# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kopo_kopo', '0003_transactions_refactor'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kopo_kopo_message',
            name='extra_field_2',
        ),
        migrations.RemoveField(
            model_name='kopo_kopo_message',
            name='extra_field_3',
        ),
        migrations.RemoveField(
            model_name='kopo_kopo_message',
            name='extra_field_4',
        ),
        migrations.RemoveField(
            model_name='kopo_kopo_message',
            name='extra_field_5',
        ),
    ]
