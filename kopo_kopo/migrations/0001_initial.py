# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kopo_Kopo_Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_message', models.CharField(max_length=1000, verbose_name=b'Raw Message')),
                ('amount', models.DecimalField(verbose_name=b'Payment Amount', max_digits=14, decimal_places=2)),
                ('timestamp', models.DateTimeField()),
                ('reference', models.CharField(unique=True, max_length=50)),
                ('extra_field_2', models.CharField(max_length=100, blank=True)),
                ('extra_field_3', models.CharField(max_length=100, blank=True)),
                ('extra_field_4', models.CharField(max_length=100, blank=True)),
                ('extra_field_5', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'ordering': ['-timestamp'],
                'verbose_name': 'Kopo Kopo Message',
            },
        ),
    ]
