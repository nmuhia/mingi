# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    """
    Makes changes to the Kopo_Kopo_Message model to support the new transaction
    system.

    Added to support STS-194.
    """

    dependencies = [
        ('kopo_kopo', '0002_kopo_kopo_message_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='kopo_kopo_message',
            name='category',
            field=models.CharField(default=b'PAY', max_length=3,
                                   choices=[(b'PAY', b'Payment')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='kopo_kopo_message',
            name='provider',
            field=models.CharField(default=b'K2', max_length=3, blank=True,
                                   choices=[(b'K2', b'Kopo Kopo')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='kopo_kopo_message',
            name='provider',
            field=models.CharField(max_length=3, blank=True,
                                   choices=[(b'K2', b'Kopo Kopo')]),
        ),
        migrations.AlterField(
            model_name='kopo_kopo_message',
            name='reference',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='kopo_kopo_message',
            name='raw_message',
            field=models.CharField(max_length=1000, verbose_name=b'Raw Message',
                                   blank=True),
        ),
        migrations.AlterIndexTogether(
            name='kopo_kopo_message',
            index_together=set([('user', 'timestamp'),
                                ('provider', 'reference')]),
        ),
    ]
