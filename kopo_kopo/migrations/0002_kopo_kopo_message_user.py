# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0001_initial'),
        ('kopo_kopo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='kopo_kopo_message',
            name='user',
            field=models.ForeignKey(default=None, blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
    ]
