import datetime
import mock
import unittest

from django_dynamic_fixture import G

from django.core.urlresolvers import reverse
from django.core import mail
from django.test.testcases import TestCase
from django.test.utils import override_settings

from africas_talking.models import Africas_Talking_Output
from mingi_django.models import Error_Log
from mingi_django.tests import SteamaDBTestCase
from mingi_site.models import Bit_Harvester, Mingi_Site
from mingi_user.models import Mingi_User

from ..handlePayment import resetLowBalance, turnOnLine, getBundleEq
from ..models import Kopo_Kopo_Message
from ..views import k2_handler, process_at_sms


@override_settings(CELERY_ALWAYS_EAGER=True)
class SmssyncPaymentViewTests(TestCase):
    @unittest.skip("broken test")
    def test_smssync_messages(self):
        # wrong secrete from smssync
        post_data = {'message': 'This is a test for a customer',
                     'message_id': '3aee7b94-ad76-400c-9aeqew',
                     'secret': '***78',
                     'sent_timestamp': datetime.datetime.now(),
                     'from': 'Milele'
                     }
        response = self.client.post(reverse('kopokopo.smssync_payment'), post_data, follow=True)
        self.assertContains(response, '{"payload": {"success": "false"}}')
        error_log = Error_Log.objects.order_by('-pk')[0]
        self.assertEqual(error_log.problem_message, 'smssync secret does not match.')
        # wrong message - is not mpesa
        post_data = {'message': 'This is a test for a customer',
                     'message_id': '3aee7b94-ad76-400c-9aeqew',
                     'secret': '006348',
                     'sent_timestamp': datetime.datetime.now(),
                     'from': 'MPesa'
                     }
        response = self.client.post(reverse('kopokopo.smssync_payment'), post_data, follow=True)
        self.assertContains(response, '{"payload": {"success": "true"}}')
        error_log = Error_Log.objects.order_by('-pk')[0]
        self.assertEqual(error_log.problem_message, 'smssync message error.')
        self.assertEqual(mail.outbox[-1].body, 'This message: "This is a test for a customer" does not look like MPesa message.')

        # test correct message with a missing user
        message = ("FI19RU778 Confirmed."
                   "on 8/7/14 at 1:34 PM"
                   "Ksh2,000.00 received from "
                   "254727843600 NYAGONCHONG'A ABUGA."
                   "New Account balance is Ksh2,890.00")
        G(Mingi_Site, name='Limbo')
        post_data = {'message': message,
                     'message_id': 'h4erfe-23200c-9aeqew',
                     'secret': '006348',
                     'sent_timestamp': datetime.datetime.now(),
                     'from': 'MPesa'
                     }
        response = self.client.post(reverse('kopokopo.smssync_payment'), post_data, follow=True)
        self.assertContains(response, '{"payload": {"success": "true"}}')
        user = Mingi_User.objects.order_by('-pk')[0]
        self.assertEqual(user.account_balance, 2000)

        # test correct message with an existing user
        site = G(Mingi_Site)
        Mingi_User.objects.create(telephone='+254727843700', line_number='1,23,43', site=site)
        message = ("FI19RU770 Confirmed."
                   "on 8/7/14 at 1:34 PM"
                   "Ksh2,000.00 received from "
                   "254727843700 NYAGONCHONG'A ABUGA."
                   "New Account balance is Ksh2,890.00")
        G(Mingi_Site, name='Limbo')
        post_data = {'message': message,
                     'message_id': 'hgerfe-23200c-9aeqew',
                     'secret': '006348',
                     'sent_timestamp': datetime.datetime.now(),
                     'from': 'MPesa'
                     }
        response = self.client.post(reverse('kopokopo.smssync_payment'), post_data, follow=True)
        self.assertContains(response, '{"payload": {"success": "true"}}')
        user = Mingi_User.objects.order_by('-pk')[0]
        self.assertEqual(user.account_balance, 2000)


USER_TEST_PHONE = '+254726767443'
BH_TEST_PHONE = '+25400000001'


class k2_handlerTestCase(TestCase):
    def setUp(self):
        testSite = Mingi_Site.objects.create(name='Testing World',num_lines=3,
                                             equipment='stuff',
                                             date_online=datetime.datetime.now(),
                                             latitude=0.00,longitude=0.00)
        Limbo = Mingi_Site.objects.create(name='Limbo',num_lines=0,
                                          equipment='none',
                                          date_online=datetime.datetime.now(),
                                          latitude=0.00,longitude=0.00)
        user = Mingi_User.objects.create(telephone=USER_TEST_PHONE,
                                         first_name='Emily',
                                         last_name='Moder',site=testSite,
                                         line_number="1,3",energy_price=500.0)
        Bit_Harvester.objects.create(telephone=BH_TEST_PHONE,
                                     harvester_id='TEST_ID0001',site=testSite,
                                     serial_number='',bh_type='AT',
                                     approval_date=datetime.datetime.now())

        user.site_manager = user
        user.save()

        pass

    def test_resetLowBalance_setting1(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        user.account_balance = 100.0
        user.low_balance_level = 0.0
        user.low_balance_warning = 75.0
        user.save()

        resetLowBalance(user)

        self.assertEqual(user.low_balance_level,75.0)

    def test_resetLowBalance_setting2(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        user.account_balance = 60.0
        user.low_balance_level = 25.0
        user.low_balance_warning = 75.0
        user.save()

        resetLowBalance(user)

        self.assertEqual(user.low_balance_level,50.0)

    def test_resetLowBalance_setting3(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        user.account_balance = 30.0
        user.low_balance_level = 0.0
        user.low_balance_warning = 75.0
        user.save()

        resetLowBalance(user)

        self.assertEqual(user.low_balance_level,25.0)

    def test_turnOnLine_ATBH(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        bh = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        bh.bh_type = 'AT'
        bh.save()

        user.line_control_party = 'BH'
        user.bit_harvester = bh
        user.save()

        turnOnLine(user)

        msgOut = ",ON,1,3"

        Africas_Talking_Output.objects.get(raw_message__contains=msgOut,bit_harvester=bh)

    @unittest.skip("irrelevant for now")
    def test_turnOnLine_TWBH(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        bh = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        bh.bh_type = 'TW'
        bh.save()

        user.line_control_party = 'BH'
        user.bit_harvester = bh
        user.save()

        turnOnLine(user)

        msgOut = "<1256:S:1:1>"

        Africas_Talking_Output.objects.get(raw_message=msgOut,bit_harvester=bh)

    def test_getBundleEq_lowAmt(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.energy_price = 100.0
        site = user.site

        site.bundle_settings = "100:90;250:85;100:75"
        site.save()
        user.save()

        amount = 90.0

        newAmt = getBundleEq(amount,user)

        self.assertEqual(amount,newAmt)


class processATSmsTestCase(SteamaDBTestCase):
    """
    Tests the process_at_sms function, which handles SMSes containing
    (potentailly) payment info from Kopo Kopo.
    """
    def test_empty_message(self):
        """
        Tests that process_at_sms creates an Error_Log when an empty message is
        provided.
        """
        self.assertEqual(
            process_at_sms('', '', '', '', True),
            [False, 'process_at_sms received empty message']
        )

        self.assertEqual(Error_Log.objects.all().count(), 1)

    @mock.patch("africas_talking.views.callPayPlanFunction")
    def test_payment_message(self, mock_call_pay_plan_function):
        """
        Tests that process_at_sms processes a Kopo Kopo payment message if
        provided.
        """
        payment_ref = 'abcdef'
        payment_amount = 100
        message = '%s from %s Ksh%d' % (
            payment_ref,
            self.all_user_customer.telephone.strip('+'),
            payment_amount)

        self.assertEqual(
            process_at_sms('', '', '', message, True),
            [True, None]
        )

        # Test that Kopo_Kopo_Message was created
        self.assertEqual(Kopo_Kopo_Message.objects.count(), 1)
        new_message = Kopo_Kopo_Message.objects.get()
        self.assertEqual(new_message.user, self.all_user_customer)
        self.assertEqual(new_message.amount, payment_amount)
        self.assertEqual(new_message.category, Kopo_Kopo_Message.PAYMENT)
        self.assertEqual(new_message.provider, Kopo_Kopo_Message.KOPO_KOPO)
        self.assertEqual(new_message.reference, payment_ref)
        self.assertEqual(new_message.raw_message, message)

        # Test that info was passed on to the callPayPlanFunction
        mock_call_pay_plan_function.assert_called_once_with(
            self.all_user_customer,
            payment_amount
        )

    @mock.patch("kopo_kopo.views.process_sms_info")
    def test_steama_command(self, mock_process_sms_info):
        """
        Tests that process_at_sms processes a Steama command.
        """
        sender = '+100200300400'
        message = 'hi guys'

        self.assertEqual(
            process_at_sms(sender, '', '', 'M %s' % message, True),
            [True, None]
        )

        # Test that info was passed on to the process_sms_info function
        mock_process_sms_info.assert_called_once_with(sender, message)


class k2HandlerViewTestCase(SteamaDBTestCase):
    """
    Tests the k2_handler view, which handles POST requests from Kopo Kopo.
    """

    def test_empty_post_request(self):
        """
        Tests that k2_handler creates an Error_Log when the POST request
        contains no body.
        """
        response = self.client.post(reverse(k2_handler))

        self.assertEqual(response.status_code, 200)

        self.assertEqual(Error_Log.objects.all().count(), 1)

    @mock.patch("africas_talking.views.callPayPlanFunction")
    def test_payment_request(self, mock_call_pay_plan_function):
        """
        Tests that k2_handler processes a Kopo Kopo payment message if
        provided.
        """
        payment_ref = 'abcdef'
        payment_amount = 100
        body_dict = {
            'amount': payment_amount,
            'sender_phone': self.all_user_customer.telephone,
            'transaction_reference': payment_ref
        }

        response = self.client.post(reverse(k2_handler), body_dict)

        # Test that response is OK
        self.assertEqual(response.status_code, 200)

        # Test that Kopo_Kopo_Message was created
        self.assertEqual(Kopo_Kopo_Message.objects.count(), 1)
        new_message = Kopo_Kopo_Message.objects.get()
        self.assertEqual(new_message.user, self.all_user_customer)
        self.assertEqual(new_message.amount, payment_amount)
        self.assertEqual(new_message.category, Kopo_Kopo_Message.PAYMENT)
        self.assertEqual(new_message.provider, Kopo_Kopo_Message.KOPO_KOPO)
        self.assertEqual(new_message.reference, payment_ref)

        # Test that info was passed on to the callPayPlanFunction
        mock_call_pay_plan_function.assert_called_once_with(
            self.all_user_customer,
            payment_amount
        )
