import datetime
import json
import re

from decimal import Decimal, InvalidOperation

from django.conf import settings
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from africas_talking.handleSMS import respondToBH, respondToUser
from africas_talking.models import Africas_Talking_Input
from africas_talking.ReportV4 import sms_unpack_as_v3
from africas_talking.sendSMS import locateUser, UserNotFound
from africas_talking.tasks import send_admin_notification
from africas_talking.views import process_kopo_kopo_payment_info,\
    PROCESS_PAYMENT_ERRORS
from mingi_django.mingi_settings.custom import OPERATOR_MAPPER
from mingi_django.models import Error_Log
from mingi_django.timezones import zone_aware
from mingi_site.models import Bit_Harvester
from mingi_user.models import Mingi_User

from .forms import PaymentMessageForm


class SmssyncPaymentView(View):
    form_class = PaymentMessageForm
    template_name = 'payment_form.html'
    initial = {'key': 'value'}

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SmssyncPaymentView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            sender = request.POST['from']
            message_id = request.POST['message_id']
            secret = request.POST['secret']
            sent_timestamp = request.POST['sent_timestamp']
            message = \
                request.POST['message'].replace('\n', '').replace('\r', '')
            success = True

            try:
                send_no = OPERATOR_MAPPER[sender]
            except Exception as e:
                Error_Log.objects.create(
                    problem_type="DEBUGGING",
                    problem_message="Payment from invalid sender flagged. "
                                    "Details (sender,secret,message): " +
                                    str(sender) + ", " + str(secret) +
                                    ", " + str(message))

            if (secret == settings.SMSSYNC_SECRET) or \
                    (secret == settings.SMSSYNC_SECRET_VULCAN):
                [success, error] = process_at_sms(sender, message_id,
                                                  sent_timestamp, message,
                                                  success)
            else:
                Error_Log.objects.create(
                    problem_type='INTERNAL',
                    problem_message='smssync secret does not match.')
                success = False
                error = 'smssync secret does not match.'

            return HttpResponse(json.dumps({"payload": {'success': success,
                                                        'error': error}}),
                                content_type="application/json")
        return render(request, self.template_name, {'form': form})


@csrf_exempt
def process_at_sms(sender, message_id, sent_timestamp, message, success):

    if (message is None) or (message == ''):
        error = 'process_at_sms received empty message'
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message=error)
        return [False, error]

    error = None
    # regex to extract amount, payment_ref
    try:
        amount = re.findall(r'Ksh([\d,]+)', message)[0]
        sender_number = '+' + re.findall(r'from\s(\d{12})', message)[0]
        ref = re.findall(r'^[\w]+', message)[0]
        amount = Decimal(amount.replace(',', ''))

        process_result = process_kopo_kopo_payment_info(amount, sender_number,
                                                        message, ref)

        if process_result in (
                None,
                PROCESS_PAYMENT_ERRORS['USER_NOT_FOUND'],
                PROCESS_PAYMENT_ERRORS['DUPLICATE_REF']):
            success = True
        else:
            success = False
            error = process_result

    except IndexError:
        # assume it's an SMS, and process through AT functions;
        # special case for main menu
        if message == "m" or message == "M":
            process_sms_info(sender, "")

        # first remove leading "M" for consist formatting
        elif message[0] == 'M' and message[1] == ' ':
            message = message.lstrip("M").lstrip(' ')
            process_sms_info(sender, message)
        elif message[0] == 'm' and message[1] == ' ':
            message = message.lstrip('m').lstrip(' ')
            process_sms_info(sender, message)

    except Exception as e:
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message="exception: "+str(e))
        send_admin_notification.delay('smssync message error',
                                      'This message: "' + message +
                                      '" does not look like MPesa message.')

    return [success, error]


@csrf_exempt
def k2_handler(request):
    """
    Handles an API call from Kopo Kopo
    """
    try:
        sender_no = request.POST['sender_phone']
        amount_info = request.POST['amount'].split()
        ref = request.POST['transaction_reference']
        amount = Decimal(amount_info[-1])

        # This produces an ugly string, but it's just for record-keeping
        msg_string = str(request.POST)

    except (MultiValueDictKeyError, IndexError) as e:
        ref = request.POST.get('transaction_reference', 'none')
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="Payment with transaction reference: %s not "
                            "processed because it does not look like a "
                            "kopokopo message: %s" % (ref, str(e)))
        return HttpResponse("200 OK")

    process_result = process_kopo_kopo_payment_info(amount, sender_no,
                                                    msg_string, ref)

    if process_result is not None:
        return HttpResponse(process_result)

    return HttpResponse("200 OK")


def process_sms_info(senderNo, msgIn):
    """
    process a received message based on sending phone number and message
    content
    """

    try:
        sender = locateUser(senderNo)

    except UserNotFound as e:
        """
        if 'BH' in msgIn:
            createNewUser(senderNo)
            sender = locateUser(senderNo)
        else:
        """
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message=e.msg)
        raise Http404

    now = zone_aware(sender, datetime.datetime.now())

    if type(sender) == Mingi_User:
        Africas_Talking_Input.objects.create(raw_message=msgIn,
                                             user=sender,
                                             timestamp=now)

        respondToUser(sender, msgIn, now)

    elif type(sender) == Bit_Harvester:

        # begin V4 integration; returns at a different endpoint from V3
        if sender.version == 'V4':
            # process as V3 if it's a PING message
            if is_formatted(msgIn):
                process_v3(msgIn, sender, now)
                return

            messages = sms_unpack_as_v3(str(msgIn))

            if messages is None:
                Error_Log.objects.create(
                    problem_type="INTERNAL",
                    problem_message="V4 message could not be decoded. "
                                    "Returned None. BH: " + str(sender) +
                                    "MSG: " + str(msgIn))
                return

            process_v4(messages, sender, now)
            # don't double-process
            return

        process_v3(msgIn, sender, now)

    else:
        Error_Log.objects.create(
            problem_type="OTHER",
            problem_message="SMS received from unknown number: %s" % senderNo)


def process_v4(messages, sender, now):

    message_list = list()

    # process STATUS messages first
    for message in messages:
        if "STATUS" in message:
            message_list.append(message)

    # process METERS messages next
    for message in messages:
        if "METERS" in message:
            message_list.append(message)

    # process CREDIT messages after
    for message in messages:
        if "CREDIT" in message:
            message_list.append(message)

    # process pulses
    for message in messages:
        if "PULSES" in message:
            message_list.append(message)

    # process everything else
    for message in messages:
        if ("STATUS" not in message) and ("METERS" not in message) and \
                ("CREDIT" not in message) and ("PULSES" not in message):
            message_list.append(message)

    for message in message_list:
        msgElements = message.split(',')

        if sender.telephone[0:4] == '+266':
            # Lesotho is 1 hour behind Kenya
            tz_hours = settings.V4_UNIX_HOURS_OFFSET + 1
            bh_timestamp = \
                datetime.datetime.fromtimestamp(int(msgElements[1])) - \
                datetime.timedelta(hours=tz_hours)
        elif sender.telephone[0:4] == '+229':
            # Benin is 2 hours behind Kenya
            tz_hours = settings.V4_UNIX_HOURS_OFFSET + 2
            bh_timestamp = \
                datetime.datetime.fromtimestamp(int(msgElements[1])) - \
                datetime.timedelta(hours=tz_hours)
        else:
            bh_timestamp = \
                datetime.datetime.fromtimestamp(int(msgElements[1])) - \
                datetime.timedelta(hours=settings.V4_UNIX_HOURS_OFFSET)
        Africas_Talking_Input.objects.create(
            raw_message=message, bit_harvester=sender, timestamp=now,
            third_party_timestamp=bh_timestamp)
        respondToBH(sender, message, now, bh_timestamp)


def process_v3(msgIn, sender, now):
    msgElements = msgIn.split(',')

    try:
        # see how to handle this
        bh_timestamp = datetime.datetime.fromtimestamp(int(msgElements[1])) - \
                       datetime.timedelta(hours=settings.UNIX_HOURS_OFFSET)

        # *** QUICK FIX: TZ BH RESPONDING AN EXTRA 12.5 HRS AHEAD ***
        if sender.site.name == "Komolo-I":
            bh_timestamp = bh_timestamp - datetime.timedelta(hours=12.5)
        elif "Nepal" in sender.site.name:
            bh_timestamp = bh_timestamp - datetime.timedelta(hours=13.8)

        Africas_Talking_Input.objects.create(
            raw_message=msgIn, bit_harvester=sender, timestamp=now,
            third_party_timestamp=bh_timestamp)

    except IndexError:
        Africas_Talking_Input.objects.create(raw_message=msgIn,
                                             bit_harvester=sender,
                                             timestamp=now)
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="Message not processed due to missing unix "
                            "timestamp: %s" % msgIn)
        return

    except InvalidOperation as e:
        Africas_Talking_Input.objects.create(raw_message=msgIn,
                                             bit_harvester=sender,
                                             timestamp=now)
        Error_Log.objects.create(
            "Message not processed due to error, %s: %s" % (str(e), msgIn))
        return


def is_formatted(msg):
    if 'apt' in msg or 'ext' in msg:
        return True
    return False
