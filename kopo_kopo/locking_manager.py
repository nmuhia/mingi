"""
A custom database manager for locking tables.

Adapted from https://djangosnippets.org/snippets/833/
"""

from django.db import connection, models


class LockingManager(models.Manager):
    """ Add lock/unlock functionality to manager.

    Example::

        class Job(models.Model):

            objects = LockingManager()

            counter = models.IntegerField(null=True, default=0)

            @staticmethod
            def do_atomic_update(job_id)
                ''' Updates job integer, keeping it below 5 '''
                try:
                    # Ensure only one HTTP request can do this update at once.
                    Job.objects.lock()

                    job = Job.objects.get(id=job_id)
                    # If we don't lock the tables two simultaneous
                    # requests might both increase the counter
                    # going over 5
                    if job.counter < 5:
                        job.counter += 1
                        job.save()

                finally:
                    Job.objects.unlock()


    """

    MYSQL_VENDOR = 'mysql'

    def lock(self):
        """
        Locks the table.

        Locks the object model table so that atomic update is possible.
        Simultaneous database access requests pend until unlock() is called.

        If the database manager is not mysql this function has no effect.

        Note: If you need to lock multiple tables, you need to do lock them
        all in one SQL clause and this function is not enough. To avoid
        dead lock, all tables must be locked in the same order.

        See http://dev.mysql.com/doc/refman/5.0/en/lock-tables.html
        """
        cursor = connection.cursor()
        if connection.vendor == self.MYSQL_VENDOR:
            table = self.model._meta.db_table
            cursor.execute("LOCK TABLES %s WRITE" % table)
        row = cursor.fetchone()
        return row

    def unlock(self):
        """
        Unlocks the table.

        If the database manager is not mysql this function has no effect.
        """
        cursor = connection.cursor()
        if connection.vendor == self.MYSQL_VENDOR:
            cursor.execute("UNLOCK TABLES")
        row = cursor.fetchone()
        return row
