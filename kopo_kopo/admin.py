from django.contrib import admin
from models import Kopo_Kopo_Message


class KopoAdmin(admin.ModelAdmin):
    list_display = ('user', 'amount', 'timestamp', 'category', 'provider')

admin.site.register(Kopo_Kopo_Message, KopoAdmin)
