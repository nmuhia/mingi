import calendar
import datetime

from decimal import Decimal, InvalidOperation

from django.utils import timezone

from africas_talking.methods import process_trigger_commands
from africas_talking.models import InvalidAttributeValue
from africas_talking.sendSMS import sendSMS
from africas_talking.userResponse import get_last_update_time,\
    handleLowBalance
from mingi_django.models import Error_Log, Custom_Timer_Command
from mingi_django.timezones import zone_aware
from mingi_site.models import Mingi_Site, Line_State, Line
from mingi_user.models import Mingi_User

from .models import Kopo_Kopo_Message


def handleStdPayment(user, amount, timestamp, planName=None):
    """
    take the appropriate action in response to a K2 payment
    """

    if planName:
        [newBalance, top_up_amt] = creditAccount(user, amount, planName)
    else:
        [newBalance, top_up_amt] = creditAccount(user, amount)

    if user.bit_harvester and user.bit_harvester.version == "V4":
        # check whether the bitHarvester thinks lines are AUTO
        lines = Line.objects.filter(user=user)
        auto_line = False
        for line in lines:
            if line.line_status != '3' and line.line_status != '6':
                auto_line = True

        if (user.control_type == "AUTOL") or (auto_line is True):
            try:
                credit_v4_auto_line(user, top_up_amt)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))
    elif newBalance > 0:
        if user.line_is_on is False:
            try:
                turnOnLine(user)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))
        
        resetLowBalance(user)
    else:
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="Account balance not incremented correctly: " +
                            str(user) + " deposited " + str(amount) + " - " +
                            str(timestamp))
        
    now = datetime.datetime.now()
    try:
        lineNos = user.line_number.split(",")
    except:
        lineNos = list()
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="User  " + str(user) +
                            " tried to to make a payment " + str(amount) +
                            " but is not registerred with a line on " +
                            str(timestamp))

    for line in lineNos:
        ls = Line_State.objects.create(
            site=user.site, number=line, timestamp=now,
            account_balance=newBalance, user=user)

    # check for relevant account balance triggers
    triggers = Custom_Timer_Command.objects.filter(
        user=user,
        trigger_type='account_balance')
    if len(triggers) > 0:
        process_trigger_commands(triggers, ls)


def handleHighUserPayment(user, amount):
    """
    performs the same functions as creditAccount but for a high-energy user
    """

    # get variable values
    msgAmt = float(amount)
    amount = getBundleEq(amount, user)
    subBalance = Decimal(user.sub_balance)
    newSubBalance = Decimal(subBalance + amount)
    paymentDetails = user.payment_plan.split(',')

    # credit account and compile confirmation message
    try:
        threshold = Decimal(paymentDetails[1].strip(' '))
    except IndexError:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Invalid syntax for payment plan details in user: " + user.telephone)
        threshold = 0
        
    # handle normally if payment volume is enough
    if (newSubBalance >= threshold):
        user.account_balance = Decimal(user.account_balance) + Decimal(newSubBalance)
        user.sub_balance = 0.00
        user.save()
        
        # if (user.language == "ENG"): # (AFTER WE HAVE THE SWAHILI TRANSLATION)
        if (user.site_manager) and (user.site_manager.last_name == "Customer Care"):
            msgOut = "Thanks %s! I have received your payment of KSh %.2f. Your energy balance is now  %.2f energy credits" % (user.first_name,msgAmt,user.account_balance)

        else:
            msgOut = "Thanks %s! I have received your payment of KSh %.2f. Your energy balance is now KSh %.2f" % (user.first_name,msgAmt,user.account_balance)
         
    # otherwise hold payment in sub-account
    else:
        user.sub_balance = newSubBalance
        user.save()
        remaining = float(threshold - newSubBalance)

        #if (user.language == "ENG"): # (AFTER WE HAVE THE SWAHILI TRANSLATION)
        msgOut = "Thanks %s! I have received your payment of KSh %.2f. You have %.2f left before your account is credited." % (user.first_name,msgAmt,remaining)

    # create record
    now = datetime.datetime.now()
    lineNos = user.line_number.split(",")
    
    for line in lineNos:
        Line_State.objects.create(site=user.site,number=line,
                                  timestamp=now,user=user,
                                  account_balance=user.account_balance)
    
    resetLowBalance(user)

    # turn line back on if appropriate
    if (user.line_is_on == False):

        if (user.account_balance >= threshold):
            try:
                turnOnLine(user)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))
                
        else:
            #if (user.language == "ENG"): (AFTER WE HAVE A SWAHILI TRANSLATION)
            msgOut += " Your line will be turned on when your balance is " + str(threshold)
      
    try:
        sendSMS(user.telephone,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="SMS failed to send: " + str(e))       


def handle_subscription_user(user, amount, timestamp):
    """
    take the appropriate action in response to a K2 payment
    """

    # get plan details and credit account
    plan_name = "Subscription Plan"
    [newBalance, top_up_amt] = creditAccount(user,amount,plan_name)
    try:
        plan_elements = user.payment_plan.split(',')
        threshold = float(plan_elements[1])
        plan_days = int(plan_elements[2])
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Subscription Plan Error:" + str(e))

    # calculate new plan expiration date
    old_exp_date = user.credit_exp_date
    old_exp_date = datetime.datetime(old_exp_date.year,old_exp_date.month,old_exp_date.day,8,0,0,0)
    today = datetime.datetime.today()

    # only recalculate if line has just been topped up past threshold
    if today > old_exp_date and newBalance >= 0:
        new_exp_date = (today + datetime.timedelta(days=plan_days)).replace(hour=0,minute=0,second=0,microsecond=0)
        user.credit_exp_date = new_exp_date
        user.save()
        
    if user.bit_harvester and user.bit_harvester.version == "V4":
        if user.control_type == "AUTOL":

            # *** FIX THIS ***
            try:
                credit_v4_auto_line(user, top_up_amt)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))

    elif (float(newBalance) >= 0):
        if (user.line_is_on == False):
            try:
                turnOnLine(user)
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))
        
    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Account balance not incremented correctly: " + str(user) + " deposited " + str(amount) + " - " + str(timestamp))
        
    now = datetime.datetime.now()
    try:
        lineNos = user.line_number.split(",")
    except:
        lineNos = list()
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="User  " + str(user) + " tried to to make a payment "+str(amount)+" but is not registerred with a line on " + str(timestamp))


    for line in lineNos:
        Line_State.objects.create(site=user.site,number=line,
                                  timestamp=now,account_balance=newBalance,
                                  user=user)


def handlePGUser(user, amount):
    """
    respond to a payment (connection fee) received from a PowerGen user
    """

    newBalance = user.account_balance + Decimal(amount)

    if (newBalance >= 0):
        user.payment_plan = ""

        if (newBalance > 0):
            amountEq = getBundleEq(newBalance,user)
            user.account_balance = amountEq
            turnOnLine(user)
        else:
            user.account_balance = newBalance

        user.save()

        if user.language == "ENG":
            msgOut = "Thanks %s! I have received your payment of KSh %.2f. You have completed your fee and your energy balance is %d" % (user.first_name,amount,int(user.account_balance))
        else:
            msgOut = "Asante, %s! Tumepokea malipo ya %.2f. Umelipa ada, na akaunti ya mingi sasa ni %d credit ya stima" % (user.first_name,amount,int(user.account_balance))

    else:
        user.account_balance = newBalance
        user.save()

        if user.language == "ENG":
            msgOut = "Thanks %s! I have received your payment of KSh %.2f. You have KSh %.2f remaining to pay your fee" % (user.first_name,amount,user.account_balance*-1)
        else:
            msgOut = msgOut = "Asante, %s! Tumepokea malipo ya KSh %.2f. Inabaki KSh %.2f kwa ada" % (user.first_name,amount,user.account_balance*-1)
        
    try:
        sendSMS(user.telephone,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="SMS failed to send: " + str(e))    


def create_preregistered_user(senderNo, amount, reference):
    """
    Creates a user in response to a payment received from an unregistered
    number. The user belongs to the "limbo" site and has an account balance
    equal to the payment amount.

    :returns: The newly-created user object.
    """
    site = Mingi_Site.objects.get(name="Limbo")

    user = Mingi_User.objects.create(telephone=senderNo, first_name="PRE-REG",
                                     last_name="USER", site=site,
                                     account_balance=amount)

    if senderNo[0:4] == "+255":
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="Payment received from unrecognized TZ number:"
                            " %s for amount: %.2f with reference: %s" %
                            (senderNo,
                             float(amount),
                             reference),
            site=Mingi_Site.objects.get(name="Komolo-I"))

    return user


def creditAccount(user, amount, planName=None):
    """
    respond to a Kopo Kopo deposit by crediting the user's account
    and confirming payment receipt
    """

    msgAmt = amount
    
    # if the bundles are active for their site, call the bundles function
    if (user.energy_price > 0.00):
        if not planName:
            amount = getBundleEq(amount,user)
    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Tried to credit user account: " + user.telephone + " with energy_price = 0; account will be credited with the original payment amount")
            
    # calculate new balance
    oldBalance = user.account_balance
            
    if (oldBalance < 0) and planName != "Subscription Plan":
        user.account_balance = Decimal(amount)
    else:
        user.account_balance = Decimal(oldBalance) + Decimal(amount)

    user.save()

    # send confirmation message
    currency = user.site.currency # *** integrate this soon ***
    if msgAmt > 0:
        ratio = float(amount) / float(msgAmt)
    else:
        ratio = 0.0

    if (user.language == "ENG"):
        if (user.site_manager) and (user.site_manager.last_name == "Customer Care"):
            msgOut = "Thanks %s! We received %s %.2f. For this bundle size, each shilling bought %.1f energy credits. Your energy credit balance is %d" % (user.first_name,currency,msgAmt,ratio,int(user.account_balance))
        elif (user.site.company_name == "E.ON"):
            msgOut = "Thanks %s! I have received your payment of %s %.2f. You now have %.1f Rafiki Credits." % (user.first_name, currency, msgAmt, user.account_balance)
        elif (user.bit_harvester and user.bit_harvester.version == "V4"):
            timestamp = str(get_last_update_time(user))
            msgOut = "Thanks %s! I have received your payment of %s %.2f. Your energy balance is now KSh %.2f" % (user.first_name,currency,msgAmt,user.account_balance)
        else:
            msgOut = "Thanks %s! I have received your payment of %s %.2f. Your energy balance is now KSh %.2f" % (user.first_name,currency,msgAmt,user.account_balance)
            
    else:
        if (user.site_manager) and (user.site_manager.last_name == "Customer Care"):
            msgOut = "Asante, %s! Tumepokea shilingi %.2f. Kwa hiki kifungu, kila shilingi hununua %.1f credit ya stima. Akaunti yako sasa ina %d credit ya stima" % (user.first_name,msgAmt,ratio,int(user.account_balance))
        elif (user.site.company_name == "E.ON"):
            msgOut = "Asante, %s! Tumepokea malipo ya %.2f %s. Akaunti yako sasa ina %.1f Rafiki kredit" % (user.first_name,msgAmt,currency,user.account_balance)
        elif (user.bit_harvester and user.bit_harvester.version == "V4"):
            timestamp = str(get_last_update_time(user))
            msgOut = "Asante, %s! Tumepokea malipo ya %.2f. Akaunti yako ni KSh %.2f katika %s" % (user.first_name,msgAmt,user.account_balance,timestamp)
        else:
            msgOut = "Asante, %s! Tumepokea malipo ya %.2f. Akaunti yako sasa ni KSh %.2f" % (user.first_name,msgAmt,user.account_balance)

    try:
        sendSMS(user.telephone,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="SMS failed to send: " + str(e))

    return [user.account_balance, amount]


def turnOnLine(user):
    """
    send the command to turn user's line back on (***V3 bitHarvester only***)
    """

    userLineNos = user.line_number.split(",")

    if (user.line_control_party == "SM"):
        if user.site_manager:
            SMSTo = user.site_manager.telephone

            for line_number in userLineNos:
                msgOut = "User has topped up their balance! Please turn on Line %s" % line_number
                try:
                    sendSMS(SMSTo,msgOut,user.site_manager)
                except Exception as e:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="SMS failed to send: " + str(e))

    elif (user.line_control_party == "BH"):
        thisBH = user.bit_harvester
        SMSTo = thisBH.telephone
        # if (thisBH.bh_type == "AT"):
        
        ts = datetime.datetime.now()
        id_num = str(calendar.timegm(ts.utctimetuple()))[3:]
        msgOut = "*1" + id_num + ",ON"

        for line_number in userLineNos:
            msgOut += "," + str(line_number)
     
        try:
            sendSMS(SMSTo,msgOut,thisBH)
            
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="SMS failed to send: " + str(e))

    else:
        raise InvalidAttributeValue("Mingi_User", "line_control_party",
                                    user.line_control_party)


def credit_v4_auto_line(user, amount):
    """
    send a CREDIT-ADD message for v4 users under AUTO control, 
    otherwise send normal line ON command
    """

    # get chargeable Wh-equivalent
    add_amount = int((float(amount) / float(user.energy_price))*1000)

    num_string = str(datetime.datetime.now().microsecond)
    msg_id = "3" + num_string[2:]

    msgOut = "*%s,CREDIT-ADD,%d,%s" % (msg_id,add_amount,user.line_number)

    try:
        sendSMS(user.bit_harvester.telephone,msgOut,user.bit_harvester)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="SMS failed to send: " + str(e))    


def resetLowBalance(user):
    """
    reset the low balance settings according to the user's new balance
    """
    
    setting1 = user.low_balance_warning
    setting2 = setting1 * 2/3
    setting3 = setting2 / 2
    
    if (user.account_balance > setting1):
        user.low_balance_level = setting1
        user.save()

    elif (user.account_balance > setting2):
        user.low_balance_level = setting2
        user.save()

    elif (user.account_balance > setting3):
        user.low_balance_level = setting3
        user.save()

    else:
        handleLowBalance(user,int(user.line_array[-1]))


def getBundleEq(amount, user):
    """
    If payment bundles are active, calculate the amount that should
    be credited to a paying user's account
    """

    proportion = 1.0

    if (user.site.bundle_settings):
        bundleSettings = user.site.bundle_settings.rstrip(';').split(";")
    
        for setting in bundleSettings:
            
            settingData = setting.split(":")
            try:
                threshold = Decimal(settingData[0].strip())
            except InvalidOperation:
                Error_Log.objects.create(problem_type="K2_PROCESS",
                                         problem_message="Invalid bundle settings in site: %s" % user.site.name)

            if (amount >= threshold):
                proportion = setting.split(":")[1].strip()

    return (Decimal(amount) * Decimal(proportion))


def resetExpDate(user, amount):
    """
    push back the user's credit expiration date 
    according to their deposit amount
    """

    # this will be implemented after Christmas - fill in later
    raise NotImplementedError()


def callPayPlanFunction(user, amount):
    """
    call the correct function to handle user's payment based on their pay plan
    """

    timestamp = timezone.now()

    # handle users on a special payment plan differently
    if user.payment_plan:

        planName = user.payment_plan.split(',')[0]

        # expected format: "High Energy User,XXX"
        if planName == "High Energy User":
            handleHighUserPayment(user, amount)
            return

        # expected format: "PowerGen Default,XXX"
        elif planName == "PowerGen Default":
            handlePGUser(user, amount)
            return

        # expected format: "Commercial User"
        elif planName == "Commercial User":
            handleStdPayment(user, amount, timestamp, planName)
            return

        # expected format: "Monthly Rate,XXX"
        elif planName == "Monthly Rate":
            handle_subscription_user(user, amount, timestamp)
            return

        # expected format: "Subscription Plan,XXX,YYY,ZZZ"
        elif planName == "Subscription Plan":
            handle_subscription_user(user, amount, timestamp)
            return

        else:
            Error_Log.objects.create(
                problem_type="INTERNAL",
                problem_message="Payment plan detected with invalid name "
                                "for user: %s. The account will be credited "
                                "as a standard account." % user.telephone)

    handleStdPayment(user, amount, timestamp)
    return
