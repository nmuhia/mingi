from django import forms


class PaymentMessageForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea)
    message_id = forms.CharField(required=True)
    secret = forms.CharField(required=True)
    sent_timestamp = forms.CharField(required=True)
