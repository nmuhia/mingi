from django.db import models

from .locking_manager import LockingManager


# spell out the currency name e.g. "Kenyan Shillings"???
PAYMENT_PLANS = (
    ('DS','Default Settings'), 
    ('CU','Commercial User'),      
    ('HE','High Energy User'),
    #('WR','Weekly Rate'),
    ('MR','Monthly Rate'),
    ('PGD','PowerGen Default'),
    ('SCP','Subscription Plan'),
    )


class Kopo_Kopo_Message(models.Model):
    """
    Represents a payment message received from Kopo Kopo.
    """

    # Locking manager needed to avoid duplicate payment processing.
    # TODO: STS-208: Replace this with a Redis-based system
    objects = LockingManager()

    # Category choices
    PAYMENT = 'PAY'
    CATEGORIES = (
        (PAYMENT, 'Payment'),
    )

    # Provider choices
    KOPO_KOPO = 'K2'
    PROVIDERS = (
        (KOPO_KOPO, 'Kopo Kopo'),
    )

    raw_message = models.CharField(max_length=1000, verbose_name="Raw Message",
                                   blank=True)

    amount = models.DecimalField(max_digits=14, decimal_places=2,
                                 verbose_name="Payment Amount")

    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True,
                             default=None)

    timestamp = models.DateTimeField()

    reference = models.CharField(max_length=50, blank=True)

    category = models.CharField(max_length=3, choices=CATEGORIES)

    provider = models.CharField(max_length=3, choices=PROVIDERS, blank=True)

    def __unicode__(self):
        return str(self.user) + " " + str(self.amount) + " " + \
               str(self.timestamp)
    
    class Meta:
        verbose_name = "Kopo Kopo Message"
        ordering = ['-timestamp']

        index_together = [
            # To support efficient checking for duplicate Kopo Kopo messages
            ["provider", "reference"],
            # To support efficient in-order listing of a customer's transactions
            ["user", "timestamp"],
        ]
