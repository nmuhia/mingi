from django.db import models

# Create your models here.

"""

class Design_Project(models.Model):
    
    # The specs for an energy system for an a:e client used in design

    # give choices for design personnel?

    project_code = models.CharField(max_length=30)
    project_title = models.CharField(max_length=30)
    stage = models.IntegerField(default=1)

    date_last_updated = models.DateTimeField(auto_now_add=True)
    last_updater = models.CharField(max_length=30)

    latitude = models.DecimalField()
    longitude = models.DecimalField()

    budget = models.DecimalField()
    
    wind_gen_coeff = models.CharField(max_length=20)
    solar_gen_coeff = models.CharField(max_length=20)

    battery_depletion_allowed = models.IntegerField(default=50)
    latency_days = models.IntegerField(default=2)

    director_salary = models.DecimalField()
    manager_salary = models.DecimalField()
    technician_salary = models.DecimalField()

    

class Project_Record(models.Model):
    # The details about an a:e project that are only needed in records

    project = models.ForeignKey('design_project.Design_Project')

    created = models.DateTimeField(auto_now_add=True)
    creator = models.CharField(max_length=30)
    creator_rating = models.IntegerField(blank=True, null=True)
    sales_manager = models.CharField(max_length=30, blank=True, null=True)

    project_overview = models.CharField(max_length=200, blank=True, null=True)

    customer_name = models.CharField(max_length=20, blank=True, null=True)
    customer_phone = models.CharField(max_length=20, blank=True, null=True)
    customer_email = models.EmailField(max_length=75, blank=True, null=True)
    
    site_manager_name = models.CharField(max_length=20, blank=True, null=True)
    site_manager_phone = models.CharField(max_length=20, blank=True, null=True)
    site_manager_email = models.EmailField(max_length=75, blank=True, null=True)

    contact_notes = models.CharField(max_length=100, blank=True, null=True)

    site_name = models.CharField(max_length=30)

    legal_address = models.CharField(max_length=75, blank=True, null=True)
    registration_number = models.CharField(max_length=30, blank=True, null=True)

    ideal_install_date = models.DateTimeField(blank=True, null=True)
    planned_install_date = models.DateTimeField(blank=True, null=True)
    actual_install_date = models.DateTimeField(blank=True, null=True)

    contract_start_date = models.DateTimeField(blank=True, null=True)
    contract_length = models.CharField(max_length=20, blank=True, null=True)
    contract_renewal = models.CharField(max_length=30, blank=True, null=True)

    win_chance = models.IntegerField(blank=True, null=True)


"""
