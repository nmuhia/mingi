import json as simplejson
import stripe

from dateutil import rrule
from dateutil.relativedelta import relativedelta
import datetime
from datetime import date, timedelta, datetime, time
from dateutil import tz
from django.utils import timezone

from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.context_processors import csrf
from django.core import serializers
from django.forms.models import model_to_dict

from django.http import HttpResponse
from django.core.management import call_command

from mingi_user.models import Mingi_User
from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output, Gateway
from africas_talking.bhResponse import map_line_number_to_bh
from africas_talking.BBOXX_API import bboxx_switch_trigger
from mingi_site.models import Mingi_Site, Bit_Harvester, Hourly_Site_Power, Hourly_Site_Revenue, Daily_Power, Line, VERSION_CHOICES, CURRENCY_CHOICES
from mingi_django.models import Error_Log, Timer_Command, Custom_Timer_Command, Utility, App_Permissions, Website_User_Profile, TRIGGER_LEVELS, TRIGGER_TYPE, USER_TYPE, MULTIMEDIA_TYPES, MSG_TYPE, Menu
from kopo_kopo.models import Kopo_Kopo_Message, PAYMENT_PLANS

from mingi_user.views import *
from mingi_site.views import *
from mingi_django.views import *

from mingi_user.methods import get_user_utility_lists, get_site_utility_lists

from mingi_django.models import Error_Log, Timer_Command, App_Permissions, TRIGGER_LEVELS, TRIGGER_TYPE, USER_TYPE, FAQ, FAQCategory, Multimedia

from mingi_django.tasks import diagnostics, power_rollups, revenue_rollups, daily_power_rollups, timer_commands, email_finance_report, email_kpi_report, email_energy_report, generate_csv_data, email_pdf_report, bboxx_api_data_collection
from mingi_django.management.commands.get_kpi_report import *
from mingi_django.management.commands.get_pdf_report import *
from mingi_site.site_energy_usage import *

from django.contrib.auth.models import User

# import load time for messages
from mingi_django.mingi_settings.custom import MESSAGE_LOAD_TIME, MAX_ALL_SITES_GRAPH_DAYS, MAX_SINGLE_SITE_GRAPH_DAYS, API_SECRET_KEY, LOAD_STATIC_URL
from africas_talking.ReportV4 import pack_v3

import sys
import pprint
from django.conf import settings

# import regex match
import re
# Import celery tasks
from celery import current_app
from africas_talking.userResponse import *
import arrow

# Get geo location
#from django.contrib.gis.utils import GeoIP
from mingi_django.mingi_settings.prod import STRIPE_API_KEY_PK

import pytz
from mingi_django.timezones import get_user_mode_timezone
from mingi_django.timezones import zone_aware, gen_zone_aware, off_set

from africas_talking.BBOXX_API import collect_BBOXX_data, bboxx_switch_trigger
from django.core.paginator import Paginator

from jfu.http import upload_receive
import csv

CUSTOMER_SUPPORT_EMAIL = "support@steama.co"
MAX_ALL_SITES_GRAPH_DAYS = 60
MAX_SINGLE_SITE_GRAPH_DAYS = 180

@login_required
def permission_denied_card(request):
    return render(request, "permission_denied.html", {})


@login_required
def new_steama_page(request):

    template_context = {}
    all_users = ""
    sites = ""
    bitharvesters = ""

    show_users_list_permission = App_Permissions.objects.filter(
        feature_name="new_steama_page", enabled=True)
    if show_users_list_permission:
        user_permitted = show_users_list_permission.filter(
            enabled_users=request.user)
        if user_permitted:
            no_user_lines = Line.objects.filter(user=None)

            all_users = Mingi_User.objects.all().order_by('first_name')
            template_context['user_details_allowed'] = True
            if not request.user.is_staff:
                template_context['is_staff'] = False
                all_users = all_users.filter(site__owners=request.user)
                no_user_lines = no_user_lines.filter(site__owners=request.user)

            else:
                template_context['is_staff'] = True

        else:
            template_context['user_details_allowed'] = False

    else:
        no_user_lines = Line.objects.filter(user=None)
        all_users = Mingi_User.objects.all().order_by('first_name')
        template_context['user_details_allowed'] = True

        if not request.user.is_staff:
            template_context['is_staff'] = False
            all_users = all_users.filter(site__owners=request.user)
            no_user_lines = no_user_lines.filter(site__owners=request.user)
        else:
            template_context['is_staff'] = True

    template_context['no_user_lines'] = no_user_lines
    template_context['all_users'] = all_users
    template_context['user_account_days'] = (
        timezone.now() - request.user.date_joined).days

    show_sites_list_permission = App_Permissions.objects.filter(
        feature_name="show_sites_list", enabled=True)
    if show_sites_list_permission:
        user_permitted = show_sites_list_permission.filter(
            enabled_users=request.user)
        if user_permitted:
            sites = Mingi_Site.objects.all().order_by('name')
            bitharvesters = Bit_Harvester.objects.all().order_by('harvester_id')
            if not request.user.is_staff:
                template_context['is_staff'] = False
                sites = sites.filter(owners=request.user)
                bitharvesters = bitharvesters.filter(site__owners=request.user)

            else:
                template_context['is_staff'] = True

    else:
        sites = Mingi_Site.objects.all().order_by('name')
        bitharvesters = Bit_Harvester.objects.all().order_by('harvester_id')
        if not request.user.is_staff:
            template_context['is_staff'] = False
            sites = sites.filter(owners=request.user)
            bitharvesters = bitharvesters.filter(site__owners=request.user)
        else:
            template_context['is_staff'] = True

    template_context['sites'] = sites
    template_context['bitharvesters'] = bitharvesters
    try:
        template_context['first_site_id'] = str(sites[0].id)
    except:
        template_context['first_site_id'] = None
        # Create error log
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="User : %s error log :" % (str(request.user)))

    # Get all users for that site
    all_website_users = list()
    if request.user.is_staff:
        website_users = User.objects.all()
        for w_user in website_users:
            all_website_users.append(w_user.username)
    else:
        # user can only edit their profile
        all_website_users.append(request.user.username)

    template_context['all_website_users'] = all_website_users

    # Pass maximum days allowed for graphs to main page
    template_context['max_all_sites_graph_days'] = MAX_ALL_SITES_GRAPH_DAYS
    template_context['max_single_site_graph_days'] = MAX_SINGLE_SITE_GRAPH_DAYS

    if(request.user.website_user_profile.utility_management):
        template_context['utility_management'] = True

    if(request.user.website_user_profile.is_demo):
        template_context['is_demo'] = True

    if(request.user.website_user_profile.is_asset_finance):
        template_context['is_asset_finance'] = True

    # get menus
    template_context['print_menus'] = get_menu(request, None)

    # get gateways
    template_context['all_gateways'] = Gateway.objects.all()
    # load static url
    template_context['load_static_url'] = LOAD_STATIC_URL
    # get stripe key
    template_context['stripe_key'] = STRIPE_API_KEY_PK
    # get all common timezones
    template_context['timezones'] = pytz.common_timezones
    template_context['current_timezone'] = request.session.get(
        'current_timezone')

    return render(request, "new_steama.html", template_context)


@login_required
def change_timezone_card(request):
    template_dict = {}
    template_dict['timezones'] = pytz.common_timezones
    template_dict['user_timezone'] = request.user.website_user_profile.timezone
    return render(request, "cards/change_timezone_card.html", template_dict)


@login_required
def save_timezone(request):
    template_dict = {}

    user_timezone = request.POST['timezone']

    request.user.website_user_profile.timezone = user_timezone
    request.user.website_user_profile.save()

    template_dict['result'] = "Saved successfully"

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def choose_timezone(request):
    template_dict = {}
    template_dict['timezone'] = get_user_mode_timezone(request)
    # user_timezone = request.POST['timezone']

    # request.user.website_user_profile.timezone = user_timezone
    # request.user.website_user_profile.save()

    template_dict['result'] = "Saved successfully"

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def comms_center(request):
    if request.user.last_name == "Guest":
        raise Http404

    template_context = {}

    if (request.user.is_staff):
        template_context['sites'] = Mingi_Site.objects.all().order_by('name')
        template_context['bit_harvesters'] = Bit_Harvester.objects.all(
        ).order_by('site__name')
        template_context['all_users'] = Mingi_User.objects.all(
        ).order_by('first_name')
        template_context['all_commands'] = Timer_Command.objects.all()
    else:
        template_context['sites'] = Mingi_Site.objects.filter(
            owners=request.user).order_by('name')
        template_context['bit_harvesters'] = Bit_Harvester.objects.filter(
            site__owners=request.user).order_by('site__name')
        template_context['all_users'] = Mingi_User.objects.filter(
            site__owners=request.user).order_by('first_name')
        template_context['all_commands'] = Timer_Command.objects.filter(
            site__owners=request.user)

    return template_context


@login_required
def customer_sms_card_open(request, user_id):
    """ Pre-open users card"""
    return customer_sms_card(request, user_id)


@login_required
def bh_sms_card_open(request, bh_id):
    """ Pre-open users card"""
    return bh_controls_card(request, bh_id)


@login_required
def customer_sms_card(request, pre_open_user=False):
    """ Card for sending SMS to customers """
    template_context = comms_center(request)

    if pre_open_user is not False:
        selected_user = Mingi_User.objects.get(id=pre_open_user)
        template_context['pre_open_user'] = str(selected_user.telephone)

    permission = App_Permissions.objects.filter(
        feature_name="customer_sms_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.website_user_profile.user_type == "CRO":
                template_context['is_cro'] = True
            return render(request, "cards/customer_sms.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.website_user_profile.user_type == "CRO":
            template_context['is_cro'] = True
        return render(request, "cards/customer_sms.html", template_context)


@csrf_exempt
def newsteama_send_customer_sms(request):
    """handle a request to send an SMS to a customer"""
    sms_type = request.POST['sms_type']
    message = request.POST['message_text'].encode('utf-8')
    response = "Message send successfully"

    if (message == ""):
        response = "Please enter a message"

    else:
        if (sms_type == "multiple_users"):
            site_name = request.POST['site']
            users = Mingi_User.objects.filter(site__name=site_name)
            # add on filter statements when new send options are added
            if (request.POST['filter'] == "line_owners"):
                users = users.filter(is_user=True)

            for user in users:
                response = send_front_end_sms(user.telephone, message)

        else:
            user_phone = request.POST['user_phone']
            if (user_phone == ""):
                response = "Please specify a recipient!"
            else:
                response = send_front_end_sms(user_phone, message)

    return HttpResponse(response)


@login_required
def bh_controls_card(request, pre_open_bh=False):
    """ Card for sending SMS to bitharvesters """
    template_context = comms_center(request)

    if pre_open_bh is not False:
        selected_bh = Bit_Harvester.objects.get(harvester_id=pre_open_bh)
        template_context['pre_open_bh'] = str(selected_bh.telephone)

    permission = App_Permissions.objects.filter(
        feature_name="bh_controls_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user.id)
        if user_permitted:
            if request.user.website_user_profile.user_type == "CRO":
                template_context['is_cro'] = True
            return render(request, "cards/bh_controls.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.website_user_profile.user_type == "CRO":
            template_context['is_cro'] = True
        return render(request, "cards/bh_controls.html", template_context)


@csrf_exempt
def newsteama_send_bh_sms(request):
    """handle a request to send SMS to a bitharvester"""

    sms_type = request.POST['sms_type'].encode("utf-8")

    try:
        harvester_phone = request.POST['bit_harvester_phone'].encode("utf-8")
        bh = Bit_Harvester.objects.get(telephone=harvester_phone)
        now = zone_aware(bh, datetime.now())

        if 'line_number' in request.POST and len(request.POST['line_number']) > 0:
            line_numbers = request.POST[
                'line_number'].encode("utf-8").split(',')
            line_number = get_adj_line_numbers(line_numbers, bh)

        if(sms_type == "send_command"):
            command = request.POST['command-option'].encode("utf-8")
            num_string = str(now.microsecond)

            # handle first the LITE BH. All communications pass through BBOXX
            # API & Site repsesents a single line
            if bh.version == "LITE":
                response = "Error in sending command"
                if str(command) == "all_on" or str(command) == "all_off" or str(command) == "status":
                    bh_imei = bh.serial_number
                    if command == "all_on":
                        command = "ON"
                        response = bboxx_switch_trigger(command, bh_imei)
                    elif command == "all_off":
                        command = "OFF"
                        response = bboxx_switch_trigger(command, bh_imei)
                    elif command == "status":
                        try:
                            start_date = zone_aware(bh, datetime.now())
                            end_date = start_date - timedelta(hours=4)
                            response = collect_BBOXX_data(
                                str(start_date), str(end_date))
                        except Exception as e:
                            Error_Log.objects.create(
                                problem_type="DEBUGGING", problem_message="BBOXX collect data error: " + str(e))
                            response = "Error in sending command. Kindly retry or contact the administrator for more details"
                    else:
                        Error_Log.objects.create(
                            problem_type="DEBUGGING", problem_message="unknown command received fro bitharvester" + str(command) + " BH: " + str(bh))
                        response = "unknown command received fro bitharvester"
                else:
                    response = "Invalid option for the selected Bit_Harvester. Kindly select the All On, All Off or Get Status Update options."
            
            # add another case for BH SOLO, which has different switching syntax
            elif bh.version == "SOLO":
                response = "Error in sending command"
                if str(command) == "all_on" or str(command) == "all_off" or str(command) == "status":

                    # password for commands is stored in the serial_number field
                    password = bh.serial_number
                    if command == "all_on":
                        command = "ON"
                    elif command == "all_off":
                        command = "OFF"
                    elif command == "status":
                        command = "CHECK"
                    else:
                        Error_Log.objects.create(
                            problem_type="EXTERNAL", problem_message="unknown command received from bitHarvester" + str(command) + " BH: " + str(bh))
                        response = "Unknown command received from bitHarvester"

                    msgOut = "SN" + password + command
                    response = send_front_end_sms(bh.telephone, msgOut)
                    
                else:
                    response = "Invalid option for the selected Bit_Harvester. Kindly select the All On, All Off or Get Status Update options."
                    
            else:

                if (command == "status"):
                    sms_id = "*4" + num_string[2:]
                    if bh.version == "V3":
                        msgOut = sms_id + ",status"
                    else:
                        msgOut = "PING"
                    response = send_front_end_sms(bh.telephone, msgOut)

                elif (command == "all_on"):
                    sms_id = "*1" + num_string[2:]
                    response = send_front_end_sms(
                        bh.telephone, sms_id + ",ON,ALL")
                    if bh.version == "V4":
                        response += " Please ensure selected users are registered with Manual line control."

                elif (command == "all_off"):
                    sms_id = "*1" + num_string[2:]
                    response = send_front_end_sms(
                        bh.telephone, sms_id + ",OFF,ALL")
                    if bh.version == "V4":
                        response += " Please ensure selected users are registered with Manual line control."

                elif (command == "all_auto"):
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        sms_id = "*1" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",AUTO,ALL")
                        response += " Please ensure selected users are registered with Automatic line control."

                elif (command == "single_on"):
                    sms_id = "*1" + num_string[2:]
                    response = send_front_end_sms(
                        bh.telephone, sms_id + ",ON," + str(line_number))
                    if bh.version == "V4":
                        response += " Please ensure selected users are registered with Manual line control."

                elif (command == "single_off"):
                    sms_id = "*1" + num_string[2:]
                    response = send_front_end_sms(
                        bh.telephone, sms_id + ",OFF," + str(line_number))
                    if bh.version == "V4":
                        response += " Please ensure selected users are registered with Manual line control."

                elif (command == "single_auto"):
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        line_number = request.POST[
                            'line_number'].encode("utf-8")
                        sms_id = "*1" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",AUTO," + str(line_number))
                        response += " Please ensure selected users are registered with Automatic line control."

                elif (command == "set_line_balance"):
                    # Set line balance
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        line_numbers = str(
                            request.POST['line_number'].encode("utf-8")).split(',')

                        # alter credit_amount to report to BH in chargeable-Wh
                        for line_number in line_numbers:
                            credit_amount = float(request.POST['extra_input'])
                            try:
                                user = findUserByLine(
                                    int(line_number), bh.site.name)
                                credit_amount = int(
                                    credit_amount / float(user.energy_price) * 1000)
                                response = ""
                            except Mingi_User.DoesNotExist:
                                # if there is an error retrieving the user or
                                # with energy price = 0, just send the
                                # unaltered amount
                                response = "No matching user found for this line. "
                            except ZeroDivisionError:
                                response = "Energy Price = 0, so account will be credited with unscaled amount."
                            sms_id = "*3" + num_string[2:]
                            if credit_amount < 0:
                                credit_amount = 0
                            response += send_front_end_sms(bh.telephone, sms_id + ",CREDIT-SET," + str(
                                credit_amount) + "," + str(line_number))

                elif (command == "set_line_price"):
                    # Set line price
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        line_number = request.POST[
                            'line_number'].encode("utf-8")
                        tariff = request.POST['extra_input'].encode("utf-8")
                        sms_id = "*3" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",TARIFF," + str(tariff) + "," + str(line_number))

                elif (command == "set_line_priority"):
                    # Set line priority
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        line_number = request.POST[
                            'line_number'].encode("utf-8")
                        priority = request.POST['extra_input'].encode("utf-8")
                        sms_id = "*3" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",PRIORITY," + str(priority) + "," + str(line_number))
                elif (command == "get_config_param"):
                    # Get configuration parameters
                    sms_id = "*2" + num_string[2:]
                    response = send_front_end_sms(
                        bh.telephone, sms_id + ",getcfg")

                elif (command == "set_log_interval"):
                    # Set data log interval
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        log_interval = request.POST[
                            'extra_input'].encode("utf-8")
                        sms_id = "*3" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",setcfg,polltime=" + str(log_interval))

                elif (command == "set_reporting_interval"):
                    # Set reporting interval
                    response = "This function is only available for bitHarvester V4"
                    if bh.version == "V4":
                        reporting_interval = request.POST[
                            'extra_input'].encode("utf-8")
                        sms_id = "*3" + num_string[2:]
                        response = send_front_end_sms(
                            bh.telephone, sms_id + ",setcfg,records=" + reporting_interval)
                        # update bh logging innterval
                        bh.logging_hours = reporting_interval
                        bh.save()

        else:
            if bh.version == "LITE":
                response = "Unknown command for bitharvester type"
            else:
                command = str(request.POST['command-manual'].encode("utf-8"))

                # check for setcfg
                if "setcfg" in command:
                    interval = get_interval(command)
                    if interval is not None:
                        bh.logging_hours = interval
                        bh.save()
                response = send_front_end_sms(bh.telephone, command)

    except Bit_Harvester.DoesNotExist as e:
        Error_Log.objects.create(
            problem_type="INTERNAL", problem_message="Comms center displayed invalid bitHarvester option: " + str(e))
        response = "Comms center displayed invalid bitHarvester option: " + \
            str(e)

    return HttpResponse(response)


def get_adj_line_numbers(line_numbers, sender):
    """
    turn a CSV list of line numbers to the correct format for the BH
    """

    return_string = ""
    line_labels = ["A", "B", "C", "D"]

    for line_number in line_numbers:
        line_number = int(line_number)
        line_number = str(map_line_number_to_bh(line_number, sender))

        return_string += line_number + ","

    return_string = return_string.rstrip(',')

    return return_string


def get_interval(command):
    """
    get BH logging interval in a user command
    """
    interval = None
    command_parts = command.split(",")
    for part in command_parts:
        if "records" in part:
            log_parts = part.split("=")
            try:
                interval_ = log_parts[1]  # get log interval
                interval = interval_.replace(" ", "")  # eat spaces

            except:
                pass
            break
    return interval


@login_required
def timer_commands_card(request):
    """ Card for adding timer commands """
    template_context = comms_center(request)

    permission = App_Permissions.objects.filter(
        feature_name="timer_commands_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            return render(request, "cards/timer_commands.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        return render(request, "cards/timer_commands.html", template_context)


@csrf_exempt
def newsteama_add_timer_commands(request):
    """Add a new timer command"""
    try:
        phone = request.POST['destination_phone']
        user = locateUser(phone)
        site = user.site
        message = request.POST['message']
        hours = request.POST['hours']
        timer_command = Timer_Command.objects.create(destination_phone=user.telephone,
                                                     message=message,
                                                     site=site,
                                                     hours=hours)
        command_id = timer_command.id
        message = str(command_id) + ":" + str(site.name) + ":" + \
            str(phone) + ":" + str(message) + ":" + str(hours)

    except UserNotFound as e:
        message = "error"

    return HttpResponse(message)


def newsteama_delete_timer_commands(request):
    """Delete a timer command"""
    command_id = request.POST.get('command_id', None)
    try:
        to_delete = Timer_Command.objects.get(id=command_id)
        to_delete.delete()
        message = "deleted"
    except Timer_Command.DoesNotExist, e:
        message = ""
        raise Http404
    return HttpResponse(message)


@login_required
def custom_trigger_commands_card(request):
    """ Card for adding timer commands """
    template_context = comms_center(request)

    permission = App_Permissions.objects.filter(
        feature_name="custom_trigger_commands_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context['all_sites'] = Mingi_Site.objects.filter(
                    is_combined=False)
                template_context['all_users'] = Mingi_User.objects.all()
                template_context['all_lines'] = Line.objects.filter(user=None)
                template_context[
                    'all_trigger_commands'] = Custom_Timer_Command.objects.all()
            else:
                template_context['all_sites'] = Mingi_Site.objects.filter(
                    owners=request.user)
                template_context['all_users'] = Mingi_User.objects.filter(
                    site__owners=request.user)
                template_context['all_lines'] = Line.objects.filter(
                    user=None, site__owners=request.user)
                # adjust this
                template_context['all_trigger_commands'] = Custom_Timer_Command.objects.filter(
                    destination=request.user.email)

            template_context['trigger_levels'] = dict(TRIGGER_LEVELS)
            template_context['trigger_types'] = dict(TRIGGER_TYPE)
            template_context['user_types'] = dict(USER_TYPE)
            template_context['msg_types'] = dict(MSG_TYPE)

            if(request.user.website_user_profile.utility_management):
                template_context['utility_management'] = True
                template_context[
                    'all_utilities'] = request.user.website_user_profile.utilities.all()

            return render(request, "cards/custom_trigger_commands.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_context['all_sites'] = Mingi_Site.objects.filter(
                is_combined=False)
            template_context['all_users'] = Mingi_User.objects.all()
            template_context['all_lines'] = Line.objects.filter(user=None)
            template_context[
                'all_trigger_commands'] = Custom_Timer_Command.objects.all()
        else:
            template_context['all_sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_context['all_users'] = Mingi_User.objects.filter(
                site__owners=request.user)
            template_context['all_lines'] = Line.objects.filter(
                user=None, site__owners=request.user)
            # adjust this
            template_context['all_trigger_commands'] = Custom_Timer_Command.objects.filter(
                destination=request.user.email)

        template_context['trigger_levels'] = dict(TRIGGER_LEVELS)
        template_context['trigger_types'] = dict(TRIGGER_TYPE)
        template_context['user_types'] = dict(USER_TYPE)
        template_context['msg_types'] = dict(MSG_TYPE)

        if(request.user.website_user_profile.utility_management):
            template_context['utility_management'] = True
            template_context[
                'all_utilities'] = request.user.website_user_profile.utilities.all()

        return render(request, "cards/custom_trigger_commands.html", template_context)


@csrf_exempt
def newsteama_add_custom_trigger_commands(request):
    """ Add a new trigger command """
    try:
        post = request.POST
        trigger_type = post['trigger_type']
        trigger_level = post['trigger_level']
        user_type = post['user_type']
        user_type_id = post['user_type_list']
        threshold = post['threshold']
        msg_type = post['msg_type']
        destination = post['destination']
        message = post['message']
        hours = post['hours']

        this_user = None
        this_line = None
        this_site = None
        user = None
        site = None
        line = None

        if msg_type == "sms":
            destination = validatePhoneNo(destination)

        if(user_type == "user"):
            user = Mingi_User.objects.get(id=user_type_id)
            this_user = str(user.first_name) + " " + str(user.last_name)

        elif(user_type == "line"):
            line = Line.objects.get(id=user_type_id)
            this_line = line.name

        elif(user_type == "site"):
            site = Mingi_Site.objects.get(id=user_type_id)
            this_site = site.name

        timer_command = Custom_Timer_Command.objects.create(destination=destination, site=site, user=user, line=line,
                                                            trigger_type=trigger_type, trigger_level=trigger_level, message=message,
                                                            message_type=msg_type, threshold=threshold, hours=hours)

        command_id = timer_command.id

        if hours == "":
            hours = "--"

        message = str(command_id) + "::" + str(this_site) + "::" + str(this_user) + "::" + str(this_line) + "::" + str(dict(TRIGGER_TYPE)[trigger_type]) + "::" + str(
            dict(TRIGGER_LEVELS)[trigger_level]) + "::" + str(destination) + "::" + str(message) + "::" + str(msg_type) + "::" + str(threshold) + "::" + str(hours)
        return HttpResponse(message)

    except Exception as e:
        Error_Log.objects.create(
            problem_type="INTERNAL", problem_message=str(e))
        return HttpResponse("0" + "::" + str(e))


@csrf_exempt
def newsteama_delete_custom_trigger_commands(request):
    """Delete a timer command"""
    command_id = request.POST['command_id']
    try:
        to_delete = Custom_Timer_Command.objects.get(id=command_id)
        to_delete.delete()
        message = "deleted"
    except Timer_Command.DoesNotExist, e:
        message = ""
        raise Http404
    return HttpResponse(message)


@login_required
def payments_bilevel_card(request, site_id=None):
    """ Card for payments bilevel chart"""
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="payments_bilevel_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if(request.user.website_user_profile.utility_management):
                template_dict['utility_management'] = True
            if site_id is not None:
                template_dict['site_name'] = Mingi_Site.objects.get(
                    id=site_id).name

            return render(request, "cards/payment_bilevel.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if(request.user.website_user_profile.utility_management):
            template_dict['utility_management'] = True
        if site_id is not None:
            template_dict['site_name'] = Mingi_Site.objects.get(
                id=site_id).name

        return render(request, "cards/payment_bilevel.html", template_dict)


@login_required
def energy_bilevel_card(request):
    """ Card for energy bilevel chart"""
    template_dict = {}
    template_dict[
        'utilities'] = request.user.website_user_profile.utilities.all()

    permission = App_Permissions.objects.filter(
        feature_name="energy_bilevel_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            return render(request, "cards/energy_bilevel.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        return render(request, "cards/energy_bilevel.html", template_dict)


@login_required
def summary_analysis_card(request):
    """ Card for payments bilevel chart"""
    template_dict = {}

    template_dict[
        'utilities'] = request.user.website_user_profile.utilities.all()

    permission = App_Permissions.objects.filter(
        feature_name="summary_analysis_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
            return render(request, "cards/summary_analysis.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.website_user_profile.utility_management:
            template_dict['utility_management'] = True
        return render(request, "cards/summary_analysis.html", template_dict)


@login_required
def comparison_graph_card(request):
    template_context = {}

    template_context[
        'utilities'] = request.user.website_user_profile.utilities.all()
    sites = Mingi_Site.objects.all().order_by('name')

    permission = App_Permissions.objects.filter(
        feature_name="comparison_graph_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if not request.user.is_staff:
                sites = sites.filter(owners=request.user)
            template_context['sites'] = sites
            return render(request, "cards/comparison_graph.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if not request.user.is_staff:
            sites = sites.filter(owners=request.user)
        template_context['sites'] = sites
        return render(request, "cards/comparison_graph.html", template_context)


@login_required
def get_comparison_graph_data(request):
    chart_type = request.POST['chart_type']
    data_type = request.POST['data_type']
    start_date = request.POST['start_date']
    end_date = request.POST['end_date']
    utility_type = request.POST['utility_type']
    sites = request.POST.getlist('sites[]')
    lines = request.POST.getlist('lines[]')
    scope = request.POST['scope']

    MULTIPLE_CURRENCIES = False
    TZSUSD_EXCHANGE_RATE = 1
    KESUSD_EXCHANGE_RATE = 1
    XOFUSD_EXCHANGE_RATE = 1

    sites_dict = {
        'name': [],
        'data': []
    }

    if(data_type == "revenue_totals"):
        sites_list = list()
        for site_id in sites:
            site = Mingi_Site.objects.get(id=site_id)
            sites_list.append(site)

        no_of_currencies = number_of_currencies(sites_list)
        if no_of_currencies > 1:
            MULTIPLE_CURRENCIES = True
            TZSUSD_EXCHANGE_RATE = convert_tzs_usd()
            KESUSD_EXCHANGE_RATE = convert_kes_usd()
            XOFUSD_EXCHANGE_RATE = convert_xof_usd()

        else:
            MULTIPLE_CURRENCIES = False

    if scope == "sites":
        for site_id in sites:
            site = Mingi_Site.objects.get(id=site_id)

            sites_dict['name'].append(site.name)
            if data_type == "revenue_totals":
                if MULTIPLE_CURRENCIES == True:
                    if site.currency == "TSH":
                        sites_dict['data'].append(get_site_financial_chart(
                            site, float(TZSUSD_EXCHANGE_RATE), start_date, end_date))
                    elif site.currency == "KSH":
                        sites_dict['data'].append(get_site_financial_chart(
                            site, float(KESUSD_EXCHANGE_RATE), start_date, end_date))
                    elif site.currency == "XOF":
                        sites_dict['data'].append(get_site_financial_chart(
                            site, float(XOFUSD_EXCHANGE_RATE), start_date, end_date))

                    sites_dict['currency'] = "USD"
                else:
                    sites_dict['data'].append(
                        get_site_financial_chart(site, 1, start_date, end_date))
                    sites_dict['currency'] = site.currency

            elif data_type == "utility_use_totals":
                sites_dict['data'].append(get_site_technical_chart(
                    site, utility_type, start_date, end_date))

            elif data_type == "utility_use":
                sites_dict['data'].append(get_site_energy_usage_rollup(
                    site, utility_type, start_date, end_date))

            elif data_type == "temperature":
                sites_dict['data'].append(get_sites_temperature_rollup(
                    request, start_date, end_date, site_id))

            elif data_type == "voltage":
                sites_dict['data'].append(get_sites_voltage_rollup(
                    request, start_date, end_date, site_id))

    else:  # lines data
        for line_id in lines:
            line = Line.objects.get(id=line_id)
            if data_type == "utility_use":
                sites_dict['data'].append(get_user_lines_energy_usage_rollup(
                    line, line.utility_type, "line", start_date, end_date))

    return HttpResponse(json.dumps(sites_dict, default=encode_datetime))


def get_site_lines(request):
    template_context = {}
    lines_list = list()
    site_id = request.POST['id']

    lines = Line.objects.filter(site__id=site_id)

    for line in lines:
        if line.user is None:
            lines_list.append(
                {"id": str(line.id), "name": str(line.name), "user": None})
        else:
            lines_list.append({"id": str(line.id), "name": str(line.name), "user": str(
                line.user.first_name) + " " + str(line.user.last_name) + " - Line " + str(line.number)})

    return HttpResponse(json.dumps(lines_list))


@login_required
def newsteama_summary_financial_chart(request):
    template_dict = {}
    template_dict['chart_data'] = {}
    template_dict['chart_data'][
        'financial'] = get_financial_chart_data(request)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_summary_technical_chart(request):

    template_dict = {}
    template_dict['chart_data'] = {}

    utility_type = request.POST['utility_type']

    template_dict['chart_data'][
        'technical'] = get_technical_chart_data(request, utility_type)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_summary_comms_chart(request):
    template_dict = {}
    template_dict['chart_data'] = {}
    template_dict['chart_data'][
        'communication'] = get_communication_chart_data(request)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def pending_alerts(request):
    template_context = {}

    all_alerts = Error_Log.objects.all()

    if request.user.is_staff:
        template_context['is_staff'] = True
        all_alerts = all_alerts.filter(is_resolved=False)
    else:
        template_context['is_staff'] = False
        all_alerts = all_alerts.filter(is_resolved=False, site__isnull=False, site__owners=request.user)

    try:
        page = int(request.GET.get('page',1))
    except ValueError:
        page = 1

    if page < 1:
        page = 1

    paginator = Paginator(all_alerts, 100)

    all_alerts = paginator.page(page)

    template_context['alerts_list'] = serializers.serialize('json',all_alerts)
    template_context['page_num'] = page

    return HttpResponse(json.dumps(template_context), content_type="application/json")


@login_required
def pending_alerts_card(request):
    """ Card for pending alerts """
    template_context = {}

    permission = App_Permissions.objects.filter(
        feature_name="pending_alerts_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            return render(request, "cards/pending_alerts.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        return render(request, "cards/pending_alerts.html", template_context)


def newsteama_resolve_alerts(request):
    """ Mark alerts as resolved """
    alert_id = request.GET['alert_id']
    try:
        resolved_alert = Error_Log.objects.get(id=alert_id)
        resolved_alert.is_resolved = True
        resolved_alert.save()
        response = "success"
    except Error_Log.DoesNotExist, e:
        raise Http404
        response = "error"
    return HttpResponse(response)


@login_required
def sites_card(request):
    """Cards for all sites"""
    template_dict = {}
    if request.user.is_staff:
        template_dict['all_sites'] = Mingi_Site.objects.all()
    else:
        template_dict['all_sites'] = Mingi_Site.objects.filter(
            owners=request.user)
    return render(request, "cards/sites.html", template_dict)


@login_required
def reports_card(request):
    """
    Card for generating report
    """
    template_context = {}

    permission = App_Permissions.objects.filter(
        feature_name="reports_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context['all_sites'] = Mingi_Site.objects.all()
                template_context['is_staff'] = True
            else:
                template_context['all_sites'] = Mingi_Site.objects.filter(
                    owners=request.user)
                template_context['is_staff'] = False
            return render(request, "cards/reports.html", template_context)

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if request.user.is_staff:
            template_context['all_sites'] = Mingi_Site.objects.all()
            template_context['is_staff'] = True
        else:
            template_context['all_sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_context['is_staff'] = False
        return render(request, "cards/reports.html", template_context)


@login_required
def report_builder_card(request):
    """
    Card for building customized CSV and PDF reports
    """
    template_context = {}
    if request.user.is_staff:
        template_context['sites'] = Mingi_Site.objects.all()
    else:
        template_context['sites'] = Mingi_Site.objects.filter(
            owners=request.user)

    return render(request, "cards/report_builder.html", template_context)


@login_required
def kpis_card(request):
    """
    Card for generating report
    """
    template_context = {}
    template_context['breadcrumb'] = request.breadcrumb

    permission = App_Permissions.objects.filter(
        feature_name="kpis_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context['all_sites'] = Mingi_Site.objects.all()
                template_context['is_staff'] = True
            else:
                template_context['all_sites'] = Mingi_Site.objects.filter(
                    owners=request.user)
                template_context['is_staff'] = False

            if request.user.website_user_profile.utility_management:
                template_context['utility_management'] = True
                template_context[
                    'utilities'] = request.user.website_user_profile.utilities.all()

            return render(request, "cards/kpis.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_context['all_sites'] = Mingi_Site.objects.all()
            template_context['is_staff'] = True
        else:
            template_context['all_sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_context['is_staff'] = False

        if request.user.website_user_profile.utility_management:
            template_context['utility_management'] = True
            template_context[
                'utilities'] = request.user.website_user_profile.utilities.all()

        return render(request, "cards/kpis.html", template_context)


@login_required
def newsteama_generate_kpis(request):
    today = timezone.now()

    request_data = request.POST['request_data']

    if request.user.website_user_profile.utility_management:
        data_type = request.POST['data_type']

        current_end_day = today.replace(
            hour=0, minute=0, second=0, microsecond=0)
        if(today < current_end_day):
            current_end_day = today.replace(
                hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)

        if request_data == "day":
            current_start_day = current_end_day - timedelta(days=1)
            previous_end_day = current_start_day
            previous_start_day = previous_end_day - timedelta(days=1)

        elif request_data == "week":
            current_start_day = current_end_day - timedelta(days=7)
            previous_end_day = current_start_day
            previous_start_day = previous_end_day - timedelta(days=7)

        elif request_data == "month":
            current_start_day = current_end_day - timedelta(days=31)
            previous_end_day = current_start_day
            previous_start_day = previous_end_day - timedelta(days=31)

        return get_bm_kpi(request, current_start_day, current_end_day, previous_start_day, previous_end_day, data_type)

    else:
        current_seven_days_start_date = today.replace(
            hour=0, minute=0, second=0, microsecond=0) - timedelta(days=7)
        current_seven_days_end_date = today

        previous_seven_days_start_date = current_seven_days_start_date - \
            timedelta(days=7)
        previous_seven_days_end_date = current_seven_days_start_date

        if request_data == "aggregate_total_revenue":
            return get_aggregate_revenues(request, previous_seven_days_start_date, previous_seven_days_end_date, current_seven_days_start_date, current_seven_days_end_date)

        elif request_data == "by_site_total_revenue":
            return get_by_site_revenues(request, previous_seven_days_start_date, previous_seven_days_end_date, current_seven_days_start_date, current_seven_days_end_date)

        elif request_data == "by_site_total_energy_use":
            return get_by_site_energy_use(request, previous_seven_days_start_date, previous_seven_days_end_date, current_seven_days_start_date, current_seven_days_end_date)

        elif request_data == "aggregate_total_energy_use":
            return get_aggregate_energy_use(request, previous_seven_days_start_date, previous_seven_days_end_date, current_seven_days_start_date, current_seven_days_end_date)

        elif request_data == "aggregate_line_states":
            return get_aggregate_line_states(request)

        elif request_data == "by_site_line_states":
            return get_by_site_line_states(request)


@login_required
def get_bm_kpi(request, current_start_day, current_end_day, previous_start_day, previous_end_day, data_type):
    current_day = 0.0
    previous_day = 0.0
    for site in Mingi_Site.objects.filter(owners=request.user):
        for user in Mingi_User.objects.filter(site=site):
            if (user.first_name == data_type):
                current_day += user.power_used(current_start_day,
                                               current_end_day)
                previous_day += user.power_used(previous_start_day,
                                                previous_end_day)

    return HttpResponse(json.dumps({"current": "{0:.2f}".format(current_day), "previous": "{0:.2f}".format(previous_day)}))


@login_required
def get_aggregate_line_states(request):
    lines_on = 0
    lines_off = 0
    users = Mingi_User.objects.all()

    if not request.user.is_staff:
        users = users.filter(site__owners=request.user)

    for user in users:
        if user.line_is_on == True:
            lines_on += 1
        else:
            lines_off += 1

    return HttpResponse(json.dumps({"lines_on": lines_on, "lines_off": lines_off}))


@login_required
def get_by_site_line_states(request):
    lines_on = 0
    lines_off = 0

    site_id = request.POST['site_id']
    site = Mingi_Site.objects.get(id=site_id)
    for user in Mingi_User.objects.filter(site=site):
        if user.line_is_on == True:
            lines_on += 1
        else:
            lines_off += 1

    return HttpResponse(json.dumps({"lines_on": lines_on, "lines_off": lines_off, "site": site.name}))


@login_required
def get_aggregate_revenues(request, last_week_start_date, last_week_end_date, this_week_start_date, this_week_end_date):
    last_week_total_revenue = 0.0
    this_week_total_revenue = 0.0
    default_currency = "KSH"

    sites = Mingi_Site.objects.all()
    if not request.user.is_staff:
        sites = Mingi_Site.objects.filter(owners=request.user)

    no_of_currencies = number_of_currencies(sites)
    if no_of_currencies > 1:
        MULTIPLE_CURRENCIES = True
        TZSKES_EXCHANGE_RATE = convert_tzs_kes()

        USDKES_EXCHANGE_RATE = convert_usd_kes()

    else:
        MULTIPLE_CURRENCIES = False

    for site in sites:
        if MULTIPLE_CURRENCIES:
            if(site.currency == "TSH"):
                last_week_total_revenue += site.revenue_range(
                    last_week_start_date, last_week_end_date) * float(TZSKES_EXCHANGE_RATE)
                this_week_total_revenue += site.revenue_range(
                    this_week_start_date, this_week_end_date) * float(TZSKES_EXCHANGE_RATE)

            elif(site.currency == "USD"):
                last_week_total_revenue += site.revenue_range(
                    last_week_start_date, last_week_end_date) * float(USDKES_EXCHANGE_RATE)
                this_week_total_revenue += site.revenue_range(
                    this_week_start_date, this_week_end_date) * float(USDKES_EXCHANGE_RATE)

            else:
                last_week_total_revenue += site.revenue_range(
                    last_week_start_date, last_week_end_date)
                this_week_total_revenue += site.revenue_range(
                    this_week_start_date, this_week_end_date)
            currency = default_currency

        else:
            last_week_total_revenue += site.revenue_range(
                last_week_start_date, last_week_end_date)
            this_week_total_revenue += site.revenue_range(
                this_week_start_date, this_week_end_date)
            currency = site.currency

    this_week_total_revenue = "{0:.2f}".format(this_week_total_revenue)
    last_week_total_revenue = "{0:.2f}".format(last_week_total_revenue)

    return HttpResponse(json.dumps({"total_revenue_this_week": this_week_total_revenue, "total_revenue_last_week": last_week_total_revenue, "currency": currency}))


@login_required
def get_by_site_revenues(request, last_week_start_date, last_week_end_date, this_week_start_date, this_week_end_date):
    last_week_total_revenue = 0.0
    this_week_total_revenue = 0.0

    site_id = request.POST['site_id']
    site = Mingi_Site.objects.get(id=site_id)

    last_week_total_revenue = site.revenue_range(
        last_week_start_date, last_week_end_date)
    this_week_total_revenue = site.revenue_range(
        this_week_start_date, this_week_end_date)

    this_week_total_revenue = "{0:.2f}".format(this_week_total_revenue)
    last_week_total_revenue = "{0:.2f}".format(last_week_total_revenue)

    return HttpResponse(json.dumps({"total_revenue_this_week": this_week_total_revenue, "total_revenue_last_week": last_week_total_revenue, "currency": site.currency, "site": site.name}))


@login_required
def get_aggregate_energy_use(request, last_week_start_date, last_week_end_date, this_week_start_date, this_week_end_date):
    last_week_total_energy_use = 0.0
    this_week_total_energy_use = 0.0

    sites = Mingi_Site.objects.all()
    if not request.user.is_staff:
        sites = Mingi_Site.objects.filter(owners=request.user)

    for site in sites:
        # DEPRECATED, instead get energy from daily rollups
        # last_week_total_energy_use += site.energy_range(last_week_start_date,last_week_end_date,False)
        # this_week_total_energy_use += site.energy_range(this_week_start_date,this_week_end_date,False)
        [this_week_energy, this_faulty_energy_list] = site.get_site_energy_total(
            this_week_start_date, this_week_end_date)
        [last_week_energy, last_faulty_energy_list] = site.get_site_energy_total(
            last_week_start_date, last_week_end_date)

        # Log the invalid data warning
        if this_faulty_energy_list:
            Error_Log.objects.create(problem_type="FAULTY_DATA",
                                     problem_message="Faulty data on total energy agregation. Description %s" % (str(this_faulty_energy_list)))
        # Log the invalid data warning
        if last_faulty_energy_list:
            Error_Log.objects.create(problem_type="FAULTY_DATA",
                                     problem_message="Faulty data on total energy agregation. Description %s" % (str(last_faulty_energy_list)))

        this_week_total_energy_use += this_week_energy
        last_week_total_energy_use += last_week_energy

    this_week_total_energy_use = "{0:.2f}".format(this_week_total_energy_use)
    last_week_total_energy_use = "{0:.2f}".format(last_week_total_energy_use)

    return HttpResponse(json.dumps({"total_energy_use_this_week": this_week_total_energy_use, "total_energy_use_last_week": last_week_total_energy_use, 'error_this_week': this_faulty_energy_list, 'error_last_week': last_faulty_energy_list}))


@login_required
def get_by_site_energy_use(request, last_week_start_date, last_week_end_date, this_week_start_date, this_week_end_date):
    last_week_total_energy_use = 0.0
    this_week_total_energy_use = 0.0

    site_id = request.POST['site_id']
    site = Mingi_Site.objects.get(id=site_id)

    # DEPRECATED, instead get energy from daily rollups
    # last_week_total_energy_use = site.energy_range(last_week_start_date,last_week_end_date,False)
    # this_week_total_energy_use = site.energy_range(this_week_start_date,this_week_end_date,False)

    [last_week_total_energy_use, last_faulty_energy_list] = site.get_user_energy_total(
        last_week_start_date, last_week_end_date)
    [this_week_total_energy_use, this_faulty_energy_list] = site.get_user_energy_total(
        this_week_start_date, this_week_end_date)

    # Log the invalid data warning
    if this_faulty_energy_list:
        Error_Log.objects.create(problem_type="FAULTY_DATA",
                                 problem_message="Faulty data on total energy agregation. Description %s: " % (str(this_faulty_energy_list)))
    # Log the invalid data warning
    if last_faulty_energy_list:
        Error_Log.objects.create(problem_type="FAULTY_DATA",
                                 problem_message="Faulty data on total energy agregation. Description %s" % (str(last_faulty_energy_list)))

    this_week_total_energy_use = "{0:.2f}".format(this_week_total_energy_use)
    last_week_total_energy_use = "{0:.2f}".format(last_week_total_energy_use)

    return HttpResponse(json.dumps({"total_energy_use_this_week": this_week_total_energy_use, "total_energy_use_last_week": last_week_total_energy_use, "site": site.name, 'error_this_week': this_faulty_energy_list, 'error_last_week': last_faulty_energy_list}))


@login_required
def site_report_card(request):
    """
    Card for generating and downloading reports
    """

    if not request.user.is_staff:
        raise Http404

    template_context = {}

    if request.user.is_staff:
        template_context['all_sites'] = Mingi_Site.objects.all()
    else:
        template_context['all_sites'] = Mingi_Site.objects.filter(
            owners=request.user)

    return render(request, "cards/site_report.html", template_context)


@login_required
def newsteama_generate_site_report(request):
    # generate report for that data
    params = CSVDateSiteFilterForm(request.GET)
    if params.is_valid():
        error_check = checkParamFields(
            params.cleaned_data, request.user, request.GET['line_numbers'])

        if (error_check == "pass"):
            message = "Your report has been generated successfully."
            compilePDFReport(params.cleaned_data)

        else:
            # error message
            message = error_check
        return HttpResponse(message)


@login_required
def finance_report_card(request):
    """
    Card for generating finance report
    """

    if not request.user.is_staff:
        raise Http404

    template_context = {}
    return render(request, "cards/finance_report.html", template_context)


@login_required
def newsteama_generate_finance_report(request):
    if request.method == "POST":
        month = request.POST['month']
        email = request.POST['email_address']
        year = request.POST['year']
        file_format = request.POST['file_format']
        # combine email & file format
        email_fileformat = [email, file_format]

        try:
            email_finance_report.delay(
                email_fileformat, year, month, request.user.username)
            return HttpResponse("success")
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Except block entered: " + str(e))
            message = "failed"
            return HttpResponse("failed: " + str(e))


@login_required
def energy_report_card(request):
    """
    Card for generating energy report
    """
    if not request.user.is_staff:
        raise Http404

    template_context = {}
    return render(request, "cards/energy_report.html", template_context)


@login_required
def newsteama_generate_energy_report(request):
    if request.method == "POST":
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        email = request.POST['email_address']
        file_format = request.POST['file_format']
        # combine  email & file format
        email_fileformat = [email, file_format]
        # Check date range
        if (datetime.strptime(end_date, '%d-%m-%y') - datetime.strptime(start_date, '%d-%m-%y')).days > MAX_ALL_SITES_GRAPH_DAYS:
            return HttpResponse("large-range")

        try:
            email_energy_report.delay(
                start_date, end_date, email_fileformat, request.user.username)
            return HttpResponse("success")
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Except block entered: " + str(e))
            message = "failed"
            return HttpResponse("failed: " + str(e))


@login_required
def newsteama_generate_kpi_report(request):
    if request.method == "POST":
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        email = request.POST['email_address']
        file_format = request.POST['file_format']
        # combine email & file format
        email_fileformat = [email, file_format]

        # Check date range
        if (datetime.strptime(end_date, '%d-%m-%y') - datetime.strptime(start_date, '%d-%m-%y')).days > MAX_ALL_SITES_GRAPH_DAYS and file_format != "zip":
            return HttpResponse("large-range")

        try:
            email_kpi_report.delay(start_date, end_date,
                                   email_fileformat, request.user.username)
            return HttpResponse("success")
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Except block entered: " + str(e))
            message = "failed"
            return HttpResponse("failed: " + str(e))


@login_required
def csv_report_card(request):
    """
    Card for generating csv report
    """

    template_context = {}
    if request.user.is_staff:
        template_context['all_sites'] = Mingi_Site.objects.all()
    else:
        template_context['all_sites'] = Mingi_Site.objects.filter(
            owners=request.user)

    return render(request, "cards/csv_report.html", template_context)


@login_required
def newsteama_generate_csv_report(request):
    if request.method == "POST":
        email = request.POST['email']
        site = request.POST['site']
        min_date = request.POST['min_date']
        max_date = request.POST['max_date']
        object_name = request.POST['object_name']
        file_format = request.POST['file_format']
        # combine email & file format to in sending
        email_fileformat = [email, file_format]

        try:
            generate_csv_data.delay(
                object_name, email_fileformat, site, min_date, max_date)
            return HttpResponse("success")
        except Exception as e:
            return HttpResponse("failed: " + str(e))


@login_required
def newsteama_site_details(request, site_id=None):
    template_dict = {}
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)
            template_dict = (get_site_info(request, this_site))

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/site_details.html", template_dict)


@login_required
def newsteama_bh_details(request, harvester_id=None):
    template_dict = {}
    if harvester_id is not None:
        try:
            if request.user.is_staff:
                this_bit_harvester = Bit_Harvester.objects.get(
                    harvester_id=harvester_id
                )
            else:
                this_bit_harvester = Bit_Harvester.objects.get(
                    harvester_id=harvester_id,
                    site__owners=request.user
                )
            template_dict.update(get_bit_harvester_info(this_bit_harvester))
            template_dict['harvester_users'] = Mingi_User.objects.filter(
                bit_harvester=this_bit_harvester
            )
        except Bit_Harvester.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/bh_details.html", template_dict)


@login_required
def newsteama_site_users(request, site_id=None):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="newsteama_site_users", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if site_id is not None:
                try:
                    if request.user.is_staff:
                        this_site = Mingi_Site.objects.get(id=site_id)

                    else:
                        this_site = Mingi_Site.objects.get(id=site_id,
                                                           owners=request.user)

                    if request.user.website_user_profile.utility_management:
                        template_dict['utility_management'] = True
                    try:
                        bh_version = Bit_Harvester.objects.filter(site=this_site)[
                            0].version
                        template_dict['bh_version'] = bh_version
                    except Exception as e:
                        Error_Log.objects.create(
                            problem_type="EXTERNAL", problem_message=str(e))
                        template_dict['bh_version'] = "V3"

                    template_dict['site_users'] = Mingi_User.objects.filter(site=this_site).exclude(
                        line_number='').exclude(line_number__isnull=True).order_by('first_name')

                    template_dict['no_user_lines'] = Line.objects.filter(
                        site=this_site, user=None)
                    template_dict['site_lines'] = Line.objects.filter(
                        site=this_site)
                    template_dict['site_name'] = this_site.name
                    template_dict['site_id'] = site_id

                except Mingi_Site.DoesNotExist:
                    raise Http404
                except ValueError:
                    raise Http404

            return render(request, "cards/site_users.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if site_id is not None:
            try:
                if request.user.is_staff:
                    this_site = Mingi_Site.objects.get(id=site_id)

                else:
                    this_site = Mingi_Site.objects.get(id=site_id,
                                                       owners=request.user)

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                try:
                    bh_version = Bit_Harvester.objects.filter(site=this_site)[
                        0].version
                    template_dict['bh_version'] = bh_version
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))
                    template_dict['bh_version'] = "V3"

                template_dict['site_users'] = Mingi_User.objects.filter(
                    site=this_site).exclude(line_number='').order_by('first_name')

                template_dict['no_user_lines'] = Line.objects.filter(
                    site=this_site, user=None)

                template_dict['site_lines'] = Line.objects.filter(
                    site=this_site)
                template_dict['site_name'] = this_site.name
                template_dict['site_id'] = site_id

            except Mingi_Site.DoesNotExist:
                raise Http404
            except ValueError:
                raise Http404

        return render(request, "cards/site_users.html", template_dict)


@login_required
def newsteama_site_users_refresh(request, site_id):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="newsteama_site_users", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if site_id is not None:
                try:
                    if request.user.is_staff:
                        this_site = Mingi_Site.objects.get(id=site_id)

                    else:
                        this_site = Mingi_Site.objects.get(id=site_id,
                                                           owners=request.user)
                    datalist = list()

                    template_dict['site_users'] = Mingi_User.objects.filter(site=this_site).exclude(
                        line_number='').exclude(line_number__isnull=True).order_by('first_name')
                    template_dict['no_user_lines'] = Line.objects.filter(
                        site=this_site, user=None)
                    template_dict['site_lines'] = Line.objects.filter(
                        site=this_site)
                    template_dict['line_status_options'] = Line.LINE_STATUS_CHOICES

                except Mingi_Site.DoesNotExist:
                    raise Http404
                except ValueError:
                    raise Http404

            site_users_ = serializers.serialize(
                'json', template_dict['site_users'])
            no_user_lines_ = serializers.serialize(
                'json', template_dict['no_user_lines'])
            site_lines_ = serializers.serialize(
                'json', template_dict['site_lines'])
            # line_status_options = serializers.serialize('json', template_dict['line_status_options'])

            return HttpResponse(json.dumps({'errors': False, "site_users": site_users_, 'no_user_lines': no_user_lines_, 'site_lines': site_lines_, 'line_status_options': dict(Line.LINE_STATUS_CHOICES)}, default=encode_datetime))

        else:
            return HttpResponse({'errors': True})
    else:
        if site_id is not None:
            try:
                if request.user.is_staff:
                    this_site = Mingi_Site.objects.get(id=site_id)

                else:
                    this_site = Mingi_Site.objects.get(id=site_id,
                                                       owners=request.user)

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                try:
                    bh_version = Bit_Harvester.objects.filter(site=this_site)[
                        0].version
                    template_dict['bh_version'] = bh_version
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))
                    template_dict['bh_version'] = "V3"

                template_dict['site_users'] = list(Mingi_User.objects.filter(
                    site=this_site).exclude(line_number='').order_by('first_name'))

                template_dict['no_user_lines'] = list(
                    Line.objects.filter(site=this_site, user=None))

                template_dict['site_lines'] = list(
                    Line.objects.filter(site=this_site))
                template_dict['site_name'] = this_site.name

            except Mingi_Site.DoesNotExist:
                raise Http404
            except ValueError:
                raise Http404

            site_users_ = serializers.serialize(
                'json', template_dict['site_users'])
            no_user_lines_ = serializers.serialize(
                'json', template_dict['no_user_lines'])
            site_lines_ = serializers.serialize(
                'json', template_dict['site_lines'])

        return HttpResponse(json.dumps({'errors': False, "site_users": site_users_, 'no_user_lines': no_user_lines_, 'site_lines': site_lines_}, default=encode_datetime))


@login_required
def newsteama_site_daily_totals(request, site_id=None):
    template_dict = {}
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)

            template_dict['site_name'] = this_site.name
            template_dict['chart_data'] = {}
            template_dict['chart_data'][
                'financial'] = get_site_financial_chart(this_site)
            template_dict['chart_data'][
                'technical'] = get_site_technical_chart(this_site)
            template_dict['chart_data'][
                'communication'] = get_site_communication_chart(this_site)

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/site_daily_totals.html", template_dict)


@login_required
def newsteama_site_revenues_daily_totals(request, site_id=None):
    template_dict = {
        'chart_data': {},
        'random_number': random.randint(1, 999999)
    }
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)
                template_dict['sites'] = Mingi_Site.objects.all()

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
                template_dict['chart_data']['financial'] = get_site_financial_chart(
                    this_site, 1, None, None)

            else:
                template_dict['chart_data']['financial'] = get_site_financial_chart(
                    this_site, 1, None, None)
                template_dict['chart_data'][
                    'communication'] = get_site_communication_chart(this_site, None, None)

            template_dict['site_id'] = site_id
            template_dict['site_name'] = this_site.name
            template_dict['currency'] = this_site.currency
            template_dict['is_staff'] = request.user.is_staff
            template_dict['timezone'] = this_site.timezone

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/site_revenues_daily_totals.html", template_dict)


@login_required
def newsteama_adjust_site_revenue_daily_totals(request):
    action = request.POST['action']
    site_id = request.POST['site_id']

    template_dict = {}
    template_dict['chart_data'] = {}
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)

            time_now = zone_aware(this_site, datetime.combine(date.today(), time.min))

            if action == "default":
                min_date = None
                max_date = None

            elif action == "last_3_day":
                max_date = time_now
                min_date = max_date - timedelta(days=2)

            elif action == "last_7_day":
                max_date = time_now
                min_date = max_date - timedelta(days=6)

            elif action == "last_30_day":
                max_date = time_now
                min_date = max_date - timedelta(days=29)

            elif action == "last_90_day":
                max_date = time_now
                min_date = max_date - timedelta(days=89)

            elif action == "last_dates":
                max_date = zone_aware(this_site,datetime.strptime(request.POST['min_date'], "%Y-%m-%d"))
                min_date = max_date - timedelta(days=int(request.POST['max_date']))

            elif action == "next_dates":
                min_date = zone_aware(this_site,datetime.strptime(request.POST['min_date'], "%Y-%m-%d"))
                max_date = min_date + timedelta(days=int(request.POST['max_date']))
                if max_date > time_now:
                    max_date = time_now
                    min_date = max_date - timedelta(days=int(request.POST['max_date']))

            else:
                min_date = zone_aware(this_site,datetime.strptime(request.POST['min_date'],"%Y-%m-%d"))
                max_date = zone_aware(this_site,datetime.strptime(request.POST['max_date'],"%Y-%m-%d"))

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
                template_dict['chart_data']['financial'] = get_site_financial_chart(
                    this_site, 1, min_date, max_date)

            else:
                template_dict['chart_data']['financial'] = get_site_financial_chart(
                    this_site, 1, min_date, max_date)
                template_dict['chart_data']['communication'] = get_site_communication_chart(
                    this_site, min_date, max_date)

            template_dict['site_id'] = site_id
            template_dict['site_name'] = this_site.name
            template_dict['currency'] = this_site.currency

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_site_energyuse_daily_totals(request, site_id=None):
    template_dict = {}
    if site_id is not None:
        this_site = None

        try:
            template_dict['chart_data'] = {}

            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)
                template_dict['sites'] = Mingi_Site.objects.all()

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)

            template_dict['site_id'] = site_id
            template_dict['site_name'] = this_site.name
            template_dict['is_staff'] = request.user.is_staff
            template_dict['site_timezone'] = this_site.timezone
            template_dict['utility_lists'] = get_site_utility_lists(this_site)

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/site_energyuse_daily_totals.html", template_dict)


@login_required
def newsteama_user_energyuse_daily_totals(request, user_id=None):
    template_dict = {}
    if user_id is not None:
        this_user = None

        template_dict['chart_data'] = {}

        # check whether this is line or user
        explode_user = user_id.split("_")

        if request.user.is_staff:
            if explode_user[0] != 'line':
                this_user = Mingi_User.objects.get(id=user_id)
                template_dict['user_name'] = this_user.first_name + \
                    " " + this_user.last_name
                template_dict['user_id'] = user_id
                template_dict['user_type'] = "user"
            else:
                this_user = Line.objects.get(id=explode_user[1])
                template_dict['user_name'] = this_user.name
                template_dict['user_id'] = explode_user[1]
                template_dict['user_type'] = "line"
            template_dict['users'] = Mingi_User.objects.all()

        else:
            if explode_user[0] != 'line':
                this_user = Mingi_User.objects.get(id=user_id,
                                                   site__owners=request.user)
                template_dict['user_name'] = this_user.first_name + \
                    " " + this_user.last_name
                template_dict['user_id'] = user_id
                template_dict['user_type'] = "user"
            else:
                this_user = Line.objects.get(
                    id=explode_user[1], site__owners=request.user)
                template_dict['user_name'] = this_user.name
                template_dict['user_id'] = explode_user[1]
                template_dict['user_type'] = "line"
            template_dict['users'] = Mingi_User.objects.filter(
                site__owners=request.user)

        try:
            template_dict['utility_lists'] = get_user_utility_lists(this_user)
        except Exception as e:
            raise Http404

        return render(request, "cards/user_energyuse_daily_totals.html", template_dict)
    else:
        raise Http404


@login_required
def newsteama_load_site_energyuse_daily_totals(request):
    template_dict = {}
    site_id = request.POST['site_id']
    utility_type = request.POST['utility_type']

    this_site = Mingi_Site.objects.get(id=site_id)

    template_dict = get_site_technical_chart(this_site, utility_type)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_load_user_energyuse_daily_totals(request):
    template_dict = {}
    user_id = request.POST['user_id']
    user_type = request.POST['user_type']
    utility_type = request.POST['utility_type']

    try:
        if user_type == "user":
            this_user = Mingi_User.objects.get(id=user_id)
        else:
            this_user = Line.objects.get(id=user_id)
    except Exception as e:
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))

    template_dict = get_user_technical_chart(this_user, utility_type)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_adjust_site_energyuse_daily_totals(request):
    template_dict = {}
    site_id = request.POST['site_id']
    utility_type = request.POST['utility_type']
    min_date = request.POST['min_date']
    max_date = request.POST['max_date']

    this_site = Mingi_Site.objects.get(id=site_id)

    template_dict = get_site_technical_chart(
        this_site, utility_type, min_date, max_date)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_adjust_user_energyuse_daily_totals(request):
    template_dict = {}
    user_id = request.POST['user_id']
    user_type = request.POST['user_type']
    utility_type = request.POST['utility_type']
    min_date = request.POST['min_date']
    max_date = request.POST['max_date']
    try:
        if user_type == "user":
            this_user = Mingi_User.objects.get(id=user_id)
        else:
            this_user = Line.objects.get(id=user_id)
    except:
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))

    template_dict = get_user_technical_chart(
        this_user, utility_type, min_date, max_date)

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def site_utilities(request):
    template_dict = {}

    try:
        site = Mingi_Site.objects.get(id=request.POST['site_id'])
        template_dict['utility_lists'] = get_site_utility_lists(site)
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))
    except Exception as e:
        Error_Log.objects.create(
            problem_type="INTERNAL", problem_message="Site Utilites: " + str(e))
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def user_utilities(request):
    template_dict = {}

    try:
        user = Mingi_User.objects.get(id=request.POST['user_id'])
        template_dict['utility_lists'] = get_user_utility_lists(user)
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))
    except Exception as e:
        Error_Log.objects.create(
            problem_type="INTERNAL", problem_message="User Utilites: " + str(e))
        return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_site_comms_daily_totals(request, site_id=None):
    template_dict = {}
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)
                template_dict['sites'] = Mingi_Site.objects.all()

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)

            template_dict['site_id'] = site_id
            template_dict['site_name'] = this_site.name
            template_dict['timezone'] = this_site.timezone
            template_dict['chart_data'] = {}
            template_dict['chart_data'][
                'communication'] = get_site_communication_chart(this_site)

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return render(request, "cards/site_comms_daily_totals.html", template_dict)


@login_required
def newsteama_adjust_site_comms_daily_totals(request):
    action = request.POST['action']
    site_id = request.POST['site_id']

    template_dict = {}
    template_dict['chart_data'] = {}
    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)

            else:
                this_site = Mingi_Site.objects.get(id=site_id,
                                                   owners=request.user)

            if action == "default":
                min_date = None
                max_date = None

            else:
                min_date = request.POST['min_date']
                max_date = request.POST['max_date']

            template_dict['site_id'] = site_id
            template_dict['site_name'] = this_site.name
            template_dict['chart_data'] = {}
            template_dict['chart_data']['communication'] = get_site_communication_chart(
                this_site, min_date, max_date)

        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    return HttpResponse(json.dumps(template_dict, default=encode_datetime))


@login_required
def newsteama_site_recent_activity(request, site_id=None):
    template_dict = {}

    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)
                template_dict['sites'] = Mingi_Site.objects.all()

            else:
                this_site = Mingi_Site.objects.get(
                    id=site_id, owners=request.user)
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)

        except Exception as e:
            Error_Log(problem_type="EXTERNAL", problem_message=str(e))

        template_dict['utility_lists'] = get_site_utility_lists(this_site)

    template_dict['site'] = this_site
    template_dict.update(get_site_info(request, this_site))

    if request.user.website_user_profile.utility_management:
        template_dict['utility_management'] = True

    return render(request, "cards/site_recent_activity.html", template_dict)


@login_required
def newsteama_load_site_recent_activity(request):
    template_dict = {}
    try:
        site_id = request.POST['site_id']
    except KeyError:
        Error_Log.objects.create(problem_type="DEBUGGING",
                                 problem_message=str(request.POST))
    utility_type = request.POST['utility_type']

    this_site = Mingi_Site.objects.get(id=site_id)

    rollup = get_site_energy_usage_rollup(this_site, utility_type, None, None)

    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def newsteama_adjust_site_recent_activity(request):
    template_dict = {}
    site_id = request.POST['site_id']
    max_date = request.POST['max_date']
    min_date = request.POST['min_date']
    utility_type = request.POST['utility_type']

    this_site = Mingi_Site.objects.get(id=site_id)

    rollup = get_site_energy_usage_rollup(
        this_site, utility_type, min_date, max_date)

    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def newsteama_site_meter_reading(request, site_id=None):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="newsteama_site_meter_reading", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted and request.user.is_active:
            if site_id is not None:
                try:
                    selected_utility = 'none'
                    template_dict['utility_management'] = False
                    if request.user.website_user_profile.utility_management:
                        template_dict['utility_management'] = True
                        template_dict[
                            'utilities'] = request.user.website_user_profile.utilities.all()
                        selected_utility = request.user.website_user_profile.utilities.all()[
                            0].name
                    else:
                        selected_utility = 'none'

                    site = Mingi_Site.objects.filter(id=site_id)[0]
                    template_dict['timezone'] = site.timezone
                    template_dict['site_data'] = get_site_users_info(
                        request, site_id, selected_utility, 'default')

                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e))
                    return render(request, "cards/permission_denied.html", {})

            return render(request, "cards/site_meter_reading.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if site_id is not None and request.user.is_active:
            try:
                selected_utility = 'none'
                template_dict['utility_management'] = False
                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                    template_dict[
                        'utilities'] = request.user.website_user_profile.utilities.all()
                    selected_utility = request.user.website_user_profile.utilities.all()[
                        0].name
                else:
                    selected_utility = 'none'

                site = Mingi_Site.objects.filter(id=site_id)[0]
                template_dict['site'] = site.name
                template_dict['timezone'] = site.timezone
                template_dict['site_data'] = get_site_users_info(
                    request, site_id, selected_utility, 'default')

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e))
                return render(request, "cards/permission_denied.html", {})
            return render(request, "cards/site_meter_reading.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})


@login_required
def sites_voltage_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="sites_voltage_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['sites_voltage'] = get_sites_voltage_rollup(
                request, None, None)
            return render(request, "cards/sites_voltage.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['sites_voltage'] = get_sites_voltage_rollup(
            request, None, None)
        return render(request, "cards/sites_voltage.html", template_dict)


@login_required
def newsteama_adjust_sites_voltage(request):
    action = request.POST['action']

    if action == "default":
        start_date = None
        end_date = None
    else:
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
    rollup = get_sites_voltage_rollup(request, start_date, end_date)
    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def sites_temperature_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="sites_temperature_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['sites_temperature'] = get_sites_temperature_rollup(
                request, None, None)
            return render(request, "cards/sites_temperature.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['sites_temperature'] = get_sites_temperature_rollup(
            request, None, None)
        return render(request, "cards/sites_temperature.html", template_dict)


@login_required
def newsteama_adjust_sites_temperature(request):
    action = request.POST['action']

    if action == "default":
        start_date = None
        end_date = None
    else:
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
    rollup = get_sites_temperature_rollup(request, start_date, end_date)
    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def sites_utility_draw_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="sites_utility_draw_card", enabled=True)

    utilities = request.user.website_user_profile.utilities.all()

    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['utilities'] = utilities
            template_dict['sites_utility_draw'] = get_sites_utility_draw_rollup(
                request, utilities[0], None, None)
            return render(request, "cards/sites_power.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['utilities'] = utilities
        template_dict['sites_utility_draw'] = get_sites_utility_draw_rollup(
            request, utilities[0], None, None)
        return render(request, "cards/sites_power.html", template_dict)


@login_required
def newsteama_adjust_sites_utility_draw(request):
    action = request.POST['action']
    utility = request.POST['utility']

    utility_type = Utility.objects.get(name=utility)

    if action == "default":
        start_date = None
        end_date = None
    else:
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
    rollup = get_sites_utility_draw_rollup(
        request, utility_type, start_date, end_date)
    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def pdf(request):
    # FOR TESTING PURPOSES
    try:
        user = User.objects.get(username="emily")
        sites = Mingi_Site.objects.all()
        year = "2015"
        if not user.is_staff:
            sites = sites.filter(owners=user)
        today = gen_zone_aware(TIME_ZONE, datetime.today())
        first = today.replace(year=2015, month=10, day=1,
                              hour=0, minute=0, second=0, microsecond=0)
        end_date = first - timedelta(days=1)
        start_date = end_date.replace(day=1)

        year = start_date.strftime("%Y")
        month = start_date.strftime("%B")

        #template_context = annual_report(sites, year, user)
        template_context = monthly_report(
            sites, month, year, start_date, end_date)
        print "================================="
        return render(request, "monthly_pdf_template.html", template_context)
    except Exception as e:
        Error_Log.objects.create(
            problem_type="INTERNAL", problem_message="PDF: " + str(e))
        return HttpResponse(e)


@login_required
def adjust_meter_reading(request):
    post = request.POST

    site_id = post['site_id']
    graph_period = post['type']

    if post['selected_utility'] != 'none':
        selected_utility = post['selected_utility']
    else:
        selected_utility = 'none'

    data = get_site_users_info(
        request, site_id, selected_utility, graph_period)

    return HttpResponse(json.dumps({'data': data}, default=encode_datetime))


@login_required
def newsteama_user_details(request, user_id=None):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="newsteama_user_details", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if user_id is not None:
                try:
                    if request.user.is_staff:
                        this_mingi_user = Mingi_User.objects.get(id=user_id)
                        template_dict['this_user'] = this_mingi_user
                        template_dict['all_users'] = Mingi_User.objects.all()
                        template_dict['sites'] = Mingi_Site.objects.all()
                        template_dict[
                            'bit_harvesters'] = Bit_Harvester.objects.all()
                    else:
                        this_mingi_user = Mingi_User.objects.get(
                            id=user_id, site__owners=request.user)
                        template_dict['this_user'] = this_mingi_user
                        template_dict['all_users'] = Mingi_User.objects.filter(
                            site__owners=request.user)
                        template_dict['sites'] = Mingi_Site.objects.filter(
                            owners=request.user)
                        template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                            site__owners=request.user)

                    try:
                        bh_version = Bit_Harvester.objects.filter(
                            site=this_mingi_user.site)[0].version
                        template_dict['bh_version'] = bh_version
                    except Exception as e:
                        Error_Log.objects.create(
                            problem_type="EXTERNAL", problem_message=str(e))
                        template_dict['bh_version'] = "V3"

                    template_dict.update(
                        get_mingi_user_info(request, this_mingi_user))
                    template_dict['user_lines'] = Line.objects.filter(
                        user=this_mingi_user)

                    if request.user.website_user_profile.user_type == "CRO":
                        template_dict['is_cro'] = True

                    if request.user.website_user_profile.utility_management:
                        template_dict['utility_management'] = True

                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))

            return render(request, "cards/user_details.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if user_id is not None:
            try:
                if request.user.is_staff:
                    this_mingi_user = Mingi_User.objects.get(id=user_id)
                    template_dict['this_user'] = this_mingi_user
                    template_dict['all_users'] = Mingi_User.objects.all()
                    template_dict['sites'] = Mingi_Site.objects.all()
                    template_dict[
                        'bit_harvesters'] = Bit_Harvester.objects.all()
                else:
                    this_mingi_user = Mingi_User.objects.get(
                        id=user_id, site__owners=request.user)
                    template_dict['this_user'] = this_mingi_user
                    template_dict['all_users'] = Mingi_User.objects.filter(
                        site__owners=request.user)
                    template_dict['sites'] = Mingi_Site.objects.filter(
                        owners=request.user)
                    template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                        site__owners=request.user)

                try:
                    bh_version = Bit_Harvester.objects.filter(
                        site=this_mingi_user.site)[0].version
                    template_dict['bh_version'] = bh_version
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))
                    template_dict['bh_version'] = "V3"

                template_dict.update(
                    get_mingi_user_info(request, this_mingi_user))
                template_dict['user_lines'] = Line.objects.filter(
                    user=this_mingi_user)

                if request.user.website_user_profile.user_type == "CRO":
                    template_dict['is_cro'] = True

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="EXTERNAL", problem_message=str(e))

        return render(request, "cards/user_details.html", template_dict)


@login_required
def newsteama_line_details(request, line_id=None):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="newsteama_line_details", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if line_id is not None:
                line_id_explode = line_id.split("_")[1]
                try:
                    if request.user.is_staff:
                        this_line = Line.objects.get(id=line_id_explode)
                        template_dict['this_line'] = this_line
                    else:
                        this_line = Line.objects.get(
                            id=line_id_explode, site__owners=request.user)
                        template_dict['this_line'] = this_line

                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))

            return render(request, "cards/line_details.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if line_id is not None:
            line_id_explode = line_id.split("_")[1]
            try:
                if request.user.is_staff:
                    this_line = Line.objects.get(id=line_id_explode)
                    template_dict['this_line'] = this_line
                else:
                    this_line = Line.objects.get(
                        id=line_id_explode, site__owners=request.user)
                    template_dict['this_line'] = this_line

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="EXTERNAL", problem_message=str(e))

        return render(request, "cards/line_details.html", template_dict)


@login_required
def newsteama_user_recent_activity(request, user_id, user_type):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="newsteama_user_recent_activity", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if user_id is not None and user_type == "user" and user_id != '':
                try:
                    if request.user.is_staff:
                        this_mingi_user = Mingi_User.objects.get(id=user_id)
                        template_dict['users'] = Mingi_User.objects.all()
                    else:
                        this_mingi_user = Mingi_User.objects.get(
                            id=user_id,
                            site__owners=request.user
                        )
                        template_dict['users'] = Mingi_User.objects.filter(
                            site__owners=request.user)

                    utility_lists = get_user_utility_lists(this_mingi_user)
                    if len(utility_lists) > 0:
                        template_dict['utility_lists'] = get_user_utility_lists(
                            this_mingi_user)

                    template_dict['this_user'] = this_mingi_user
                    template_dict['type'] = "user"

                    if(request.user.website_user_profile.utility_management):
                        template_dict['utility_management'] = True

                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e))
            elif user_id is not None and user_type == "line" and user_id != '':
                user_explode = user_id.split("_")
                try:
                    if request.user.is_staff:
                        this_line = Line.objects.get(id=user_explode[1])
                        template_dict['users'] = Mingi_User.objects.filter(
                            site__owners=request.user)
                    else:
                        this_line = Line.objects.get(
                            id=user_explode[1], site__owners=request.user)
                    template_dict['this_line'] = this_line
                    template_dict['type'] = "line"

                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e))
            else:
                return render(request, "cards/permission_denied.html", {})

            return render(request, "cards/user_recent_activity.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        # for user
        if user_id is not None and user_type == "user" and user_id != '':
            try:
                if request.user.is_staff:
                    this_mingi_user = Mingi_User.objects.get(id=user_id)
                    template_dict['users'] = Mingi_User.objects.all()
                else:
                    this_mingi_user = Mingi_User.objects.get(
                        id=user_id,
                        site__owners=request.user
                    )
                    template_dict['users'] = Mingi_User.objects.filter(
                        site__owners=request.user)

                utility_lists = get_user_utility_lists(this_mingi_user)
                if len(utility_lists) > 0:
                    template_dict['utility_lists'] = get_user_utility_lists(
                        this_mingi_user)

                template_dict['this_user'] = this_mingi_user
                template_dict['type'] = "user"

                if(request.user.website_user_profile.utility_management):
                    template_dict['utility_management'] = True

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e))

        elif user_id is not None and user_type == "line" and user_id != '':
            user_explode = user_id.split("_")
            try:
                if request.user.is_staff:
                    this_line = Line.objects.get(id=user_explode[1])
                else:
                    this_line = Line.objects.get(
                        id=user_explode[1], site__owners=request.user)
                template_dict['this_line'] = this_line
                template_dict['type'] = "line"
            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e))
        else:
            return render(request, "cards/permission_denied.html", {})

        return render(request, "cards/user_recent_activity.html", template_dict)


@login_required
def newsteama_load_user_recent_activity(request):
    user_id = request.POST['user_id']
    utility_type = request.POST['utility_type']
    u_type = request.POST['type']

    if u_type == "user":
        this_mingi_user = Mingi_User.objects.get(id=user_id)
        rollup = get_user_lines_energy_usage_rollup(
            this_mingi_user, utility_type, u_type, None, None)
    else:
        this_line = Line.objects.get(id=user_id)
        rollup = get_user_lines_energy_usage_rollup(
            this_line, utility_type, u_type, None, None)

    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def newsteama_adjust_user_recent_activity(request):
    utility_type = request.POST['utility_type']
    user_id = request.POST['user_id']
    start_date = request.POST['from_date']
    end_date = request.POST['to_date']
    u_type = request.POST['type']

    if u_type == "user":
        this_mingi_user = Mingi_User.objects.get(id=user_id)
        rollup = get_user_lines_energy_usage_rollup(
            this_mingi_user, utility_type, u_type, start_date, end_date)
    else:
        this_line = Line.objects.get(id=user_id)
        rollup = get_user_lines_energy_usage_rollup(
            this_line, utility_type, u_type, start_date, end_date)

    return HttpResponse(json.dumps(rollup, default=encode_datetime))


@login_required
def search_user_card(request):
    template_dict = {}
    if request.user.is_staff:
        template_dict['all_users'] = Mingi_User.objects.all()
    else:
        template_dict['all_users'] = Mingi_User.objects.filter(
            site__owners=request.user
        )

    return render(request, "cards/search_user.html", template_dict)


@login_required
def newsteama_search_user(request):
    if request.method == "POST":
        search_result = newsteama_mingi_user_search(request)
        if search_result == "search_failed":
            return HttpResponse(json.dumps({'response': 'failed'}))
        else:
            user_id = search_result
            this_mingi_user = Mingi_User.objects.get(id=user_id)
            user_name = this_mingi_user.first_name + " " + \
                this_mingi_user.last_name + " " + this_mingi_user.telephone
            user_data = json.dumps(
                {'response': 'success', 'user_id': this_mingi_user.id, 'user_name': user_name})
            return HttpResponse(user_data)


@login_required
def edit_user_card(request, user_id):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="edit_user_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if user_id is not None:
                try:
                    if request.user.is_staff:
                        this_mingi_user = Mingi_User.objects.get(id=user_id)
                        template_dict['this_user'] = this_mingi_user
                        template_dict['all_users'] = Mingi_User.objects.all()
                        template_dict['sites'] = Mingi_Site.objects.all()
                        template_dict[
                            'bit_harvesters'] = Bit_Harvester.objects.all()
                    else:
                        this_mingi_user = Mingi_User.objects.get(
                            id=user_id,
                            site__owners=request.user
                        )
                        template_dict['this_user'] = this_mingi_user
                        template_dict['all_users'] = Mingi_User.objects.filter(
                            site__owners=request.user
                        )
                        template_dict['sites'] = Mingi_Site.objects.filter(
                            owners=request.user
                        )
                        template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                            site__owners=request.user
                        )

                    user_site = this_mingi_user.site
                    if this_mingi_user.bit_harvester:
                        bh_version = this_mingi_user.bit_harvester.version
                    else:
                        bh_version = "V3"
                    template_dict['bh_version'] = bh_version
                    template_dict['line_status'] = this_mingi_user.line_status
                    template_dict['line_status_choices'] = Line.LINE_STATUS_CHOICES
                    template_dict['payment_plans'] = PAYMENT_PLANS
                    template_dict[
                        'control_type'] = Mingi_User.CONTROL_TYPE_CHOICES

                    template_dict.update(
                        get_mingi_user_info(request, this_mingi_user))

                    if request.user.website_user_profile.user_type == "CRO":
                        template_dict['is_cro'] = True

                    if request.user.website_user_profile.utility_management:
                        template_dict['utility_management'] = True
                        template_dict[
                            'utilities'] = request.user.website_user_profile.utilities.all()

                except Mingi_User.DoesNotExist:
                    raise Http404
                except ValueError:
                    raise Http404
        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if user_id is not None:
            try:
                if request.user.is_staff:
                    this_mingi_user = Mingi_User.objects.get(id=user_id)
                    template_dict['this_user'] = this_mingi_user
                    template_dict['all_users'] = Mingi_User.objects.all()
                    template_dict['sites'] = Mingi_Site.objects.all()
                    template_dict[
                        'bit_harvesters'] = Bit_Harvester.objects.all()
                else:
                    this_mingi_user = Mingi_User.objects.get(
                        id=user_id,
                        site__owners=request.user
                    )
                    template_dict['this_user'] = this_mingi_user
                    template_dict['all_users'] = Mingi_User.objects.filter(
                        site__owners=request.user
                    )
                    template_dict['sites'] = Mingi_Site.objects.filter(
                        owners=request.user
                    )
                    template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                        site__owners=request.user
                    )

                try:
                    bh_version = Bit_Harvester.objects.filter(
                        site=this_mingi_user.site)[0].version
                    template_dict['bh_version'] = bh_version
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="EXTERNAL", problem_message=str(e))
                    template_dict['bh_version'] = "V3"

                template_dict['line_status'] = this_mingi_user.line_status
                template_dict['line_status_choices'] = Line.LINE_STATUS_CHOICES
                template_dict['payment_plans'] = PAYMENT_PLANS
                template_dict[
                    'control_types'] = Mingi_User.CONTROL_TYPE_CHOICES

                template_dict.update(
                    get_mingi_user_info(request, this_mingi_user))

                if request.user.website_user_profile.user_type == "CRO":
                    template_dict['is_cro'] = True

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                    template_dict[
                        'utilities'] = request.user.website_user_profile.utilities.all()

            except Mingi_User.DoesNotExist:
                raise Http404
            except ValueError:
                raise Http404

    return render(request, "cards/edit_user.html", template_dict)


@login_required
def edit_site_card(request, site_id):
    template_dict = {}

    template_dict['timezones'] = pytz.common_timezones
    permission = App_Permissions.objects.filter(
        feature_name="edit_site_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if site_id is not None:
                try:
                    if request.user.is_staff:
                        site = Mingi_Site.objects.get(id=site_id)
                        template_dict['site'] = site
                        template_dict['tou'] = Mingi_User.objects.filter(site=site)[
                            0].TOU_hours
                    else:
                        site = Mingi_Site.objects.get(
                            id=site_id, owners=request.user)
                        template_dict['site'] = site
                        template_dict['tou'] = Mingi_User.objects.filter(site=site)[
                            0].TOU_hours
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e)[0:250])

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if site_id is not None:
            try:
                if request.user.is_staff:
                    site = Mingi_Site.objects.get(id=site_id)
                    template_dict['site'] = site
                    template_dict['tou'] = Mingi_User.objects.filter(site=site)[
                        0].TOU_hours
                else:
                    site = Mingi_Site.objects.get(
                        id=site_id, owners=request.user)
                    template_dict['site'] = site
                    template_dict['tou'] = Mingi_User.objects.filter(site=site)[
                        0].TOU_hours

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e)[0:250])
    return render(request, "cards/edit_site.html", template_dict)


@login_required
def edit_bh_card(request, bh_id):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="edit_bh_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if bh_id is not None:
                try:
                    if request.user.is_staff:
                        template_dict['bh'] = Bit_Harvester.objects.get(
                            harvester_id=bh_id)
                    else:
                        template_dict['bh'] = Bit_Harvester.objects.get(
                            harvester_id=bh_id, site__owners=request.user)
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e))

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if bh_id is not None:
            try:
                if request.user.is_staff:
                    template_dict['bh'] = Bit_Harvester.objects.get(
                        harvester_id=bh_id)
                else:
                    template_dict['bh'] = Bit_Harvester.objects.get(
                        harvester_id=bh_id, site__owners=request.user)

            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e))

    template_dict['bh_versions'] = VERSION_CHOICES
    return render(request, "cards/edit_bh.html", template_dict)


@login_required
def edit_line_card(request, line_id):
    template_dict = {}

    line_id_explode = line_id.split("_")[1]

    permission = App_Permissions.objects.filter(
        feature_name="edit_line_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if line_id is not None:
                try:
                    if request.user.is_staff:
                        template_dict['this_line'] = Line.objects.get(
                            id=line_id_explode)
                        template_dict['all_users'] = Mingi_User.objects.all()
                    else:
                        template_dict['this_line'] = Line.objects.get(
                            id=line_id_explode, site__owners=request.user)
                        template_dict['all_users'] = Mingi_User.objects.filter(
                            site__owners=request.user)

                    template_dict['utilities'] = Utility.objects.all()
                    template_dict['line_status'] = Line.LINE_STATUS_CHOICES
                except Exception as e:
                    Error_Log.objects.create(
                        problem_type="INTERNAL", problem_message=str(e))

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if line_id is not None:
            try:
                if request.user.is_staff:
                    template_dict['this_line'] = Line.objects.get(
                        id=line_id_explode)
                    template_dict['all_users'] = Mingi_User.objects.all()
                else:
                    template_dict['this_line'] = Line.objects.get(
                        id=line_id_explode, site__owners=request.user)
                    template_dict['all_users'] = Mingi_User.objects.filter(
                        site__owners=request.user)

                template_dict['utilities'] = Utility.objects.all()
                template_dict['line_status'] = Line.LINE_STATUS_CHOICES
            except Exception as e:
                Error_Log.objects.create(
                    problem_type="INTERNAL", problem_message=str(e))
    return render(request, "cards/edit_line.html", template_dict)


@login_required
def edit_website_user_card(request, user_id):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="edit_website_user_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            user = User.objects.get(username=user_id)
            profile = Website_User_Profile.objects.get(user=user)
            template_dict['this_user'] = user
            template_dict['this_user_profile'] = profile
            template_dict['logged_user'] = request.user
            if user.is_staff:
                template_dict[
                    'user_types'] = Website_User_Profile.USER_TYPE_CHOICES
            else:
                user_types = list()
                for u_type in Website_User_Profile.USER_TYPE_CHOICES:
                    if str(u_type[0]) != "AAD":
                        user_types.append(u_type)
                template_dict['user_types'] = user_types

            permissions = App_Permissions.objects.all()
            assigned_permissions = permissions.filter(enabled_users=user)
            unassigned_permissions = permissions.filter(
                enabled_users=request.user)
            unassigned_permissions = unassigned_permissions.exclude(
                enabled_users=user)
            template_dict['assigned_permissions'] = assigned_permissions
            template_dict['unassigned_permissions'] = unassigned_permissions

            sites = Mingi_Site.objects.all()
            assigned_sites = sites.filter(owners=user)
            unassigned_sites = sites.filter(owners=request.user)
            unassigned_sites = unassigned_sites.exclude(owners=user)
            template_dict['assigned_sites'] = assigned_sites
            template_dict['unassigned_sites'] = unassigned_sites

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True

                utilities = Utility.objects.all()
                assigned_utilities = profile.utilities.all()
                template_dict['utilities'] = utilities
                template_dict['assigned_utilities'] = assigned_utilities

            return render(request, "cards/edit_website_user.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:

        user = User.objects.get(username=user_id)
        profile = Website_User_Profile.objects.get(user=user)
        template_dict['this_user'] = user
        template_dict['this_user_profile'] = profile
        template_dict['logged_user'] = request.user
        if user.is_staff:
            template_dict[
                'user_types'] = Website_User_Profile.USER_TYPE_CHOICES
        else:
            user_types = list()
            for u_type in Website_User_Profile.USER_TYPE_CHOICES:
                if str(u_type[0]) != "AAD":
                    user_types.append(u_type)
            template_dict['user_types'] = user_types

        permissions = App_Permissions.objects.all()
        assigned_permissions = permissions.filter(enabled_users=user)
        unassigned_permissions = permissions.filter(enabled_users=request.user)
        unassigned_permissions = unassigned_permissions.exclude(
            enabled_users=user)
        template_dict['assigned_permissions'] = assigned_permissions
        template_dict['unassigned_permissions'] = unassigned_permissions

        sites = Mingi_Site.objects.all()
        assigned_sites = sites.filter(owners=user)
        unassigned_sites = sites.filter(owners=request.user)
        unassigned_sites = unassigned_sites.exclude(owners=user)
        template_dict['assigned_sites'] = assigned_sites
        template_dict['unassigned_sites'] = unassigned_sites

        if request.user.website_user_profile.utility_management:
            template_dict['utility_management'] = True

            utilities = Utility.objects.all()
            assigned_utilities = profile.utilities.all()
            template_dict['utilities'] = utilities
            template_dict['assigned_utilities'] = assigned_utilities

        return render(request, "cards/edit_website_user.html", template_dict)


@login_required
def edit_gateway_card(request, gateway_name):
    template_dict = {}

    permission = App_Permissions.objects.filter(
        feature_name="edit_gateway_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:

            template_dict['gateway'] = Gateway.objects.get(name=gateway_name)

            return render(request, "cards/edit_gateway.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['gateway'] = Gateway.objects.get(name=gateway_name)

        return render(request, "cards/edit_gateway.html", template_dict)


@login_required
def newsteama_edit_user(request, user_id):
    user_det = {}
    if request.method == "GET":
        response = check_customer_data(request.user, request.GET, user_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_edit_site(request, site_id):
    if request.method == "GET":
        response = edit_site(request, site_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_edit_bh(request, bh_id):
    if request.method == "POST":
        response = edit_bh(request, bh_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_edit_line(request, line_id):
    if request.method == "GET":
        response = manual_edit_line(request, line_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_edit_website_user(request, user_id):
    if request.method == "POST":
        response = manual_register_website_user(request, user_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_delete_user(request, user_id):
    if request.method == "GET":
        response = delete_user(user_id)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def register_user_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_user_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_dict['all_users'] = Mingi_User.objects.all()
                template_dict['sites'] = Mingi_Site.objects.all()
                template_dict['bit_harvesters'] = Bit_Harvester.objects.all()

            else:
                template_dict['all_users'] = Mingi_User.objects.filter(
                    site__owners=request.user)
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)
                template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                    site__owners=request.user)

            if request.user.website_user_profile.user_type == "CRO":
                template_dict['is_cro'] = True

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
                template_dict[
                    'utilities'] = request.user.website_user_profile.utilities.all()

            template_dict['payment_plans'] = PAYMENT_PLANS
            template_dict['control_type'] = Mingi_User.CONTROL_TYPE_CHOICES

            return render(request, "cards/register_user.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_dict['all_users'] = Mingi_User.objects.all()
            template_dict['sites'] = Mingi_Site.objects.all()
            template_dict['bit_harvesters'] = Bit_Harvester.objects.all()

        else:
            template_dict['all_users'] = Mingi_User.objects.filter(
                site__owners=request.user)
            template_dict['sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_dict['bit_harvesters'] = Bit_Harvester.objects.filter(
                site__owners=request.user)

        if request.user.website_user_profile.user_type == "CRO":
            template_dict['is_cro'] = True

        if request.user.website_user_profile.utility_management:
            template_dict['utility_management'] = True
            template_dict[
                'utilities'] = request.user.website_user_profile.utilities.all()

        template_dict['payment_plans'] = PAYMENT_PLANS
        template_dict['control_type'] = Mingi_User.CONTROL_TYPE_CHOICES

        return render(request, "cards/register_user.html", template_dict)


@login_required
def register_website_user_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_website_user_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_dict[
                    'user_type'] = request.user.website_user_profile.user_type
                template_dict['user_permissions'] = App_Permissions.objects.filter(
                    enabled=True)  # All permissions
                template_dict['user_sites'] = Mingi_Site.objects.filter(
                    is_active=True)  # all sites
                template_dict['user_details'] = request.user

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                    # Get all utilities
                    template_dict['utilities'] = Utility.objects.all()
            else:
                template_dict[
                    'user_type'] = request.user.website_user_profile.user_type
                template_dict['user_permissions'] = App_Permissions.objects.filter(
                    enabled_users=request.user, enabled=True)
                template_dict['user_sites'] = Mingi_Site.objects.filter(
                    owners=request.user, is_active=True)
                template_dict['user_details'] = request.user

                if request.user.website_user_profile.utility_management:
                    template_dict['utility_management'] = True
                    template_dict[
                        'utilities'] = request.user.website_user_profile.utilities.all()

            return render(request, "cards/register_website_user.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_dict[
                'user_type'] = request.user.website_user_profile.user_type
            template_dict['user_permissions'] = App_Permissions.objects.filter(
                enabled=True)  # all permissions
            template_dict['user_sites'] = Mingi_Site.objects.filter(
                is_active=True)  # all sites
            template_dict['user_details'] = request.user

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
                template_dict['utilities'] = Utility.objects.all()
        else:
            template_dict[
                'user_type'] = request.user.website_user_profile.user_type
            template_dict['user_permissions'] = App_Permissions.objects.filter(
                enabled_users=request.user, enabled=True)
            template_dict['user_sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_dict['user_details'] = request.user

            if request.user.website_user_profile.utility_management:
                template_dict['utility_management'] = True
                template_dict[
                    'utilities'] = request.user.website_user_profile.utilities.all()

    return render(request, "cards/register_website_user.html", template_dict)


@login_required
def register_faq_category_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_faq_category_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            return render(request, "cards/register_faq_category.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            return render(request, "cards/register_faq_category.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})


@login_required
def register_faq_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_faq_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['all_categories'] = FAQCategory.objects.all()
            template_dict['all_multimedia'] = Multimedia.objects.all()

            return render(request, "cards/register_faq.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_dict['all_categories'] = FAQCategory.objects.all()
            template_dict['all_multimedia'] = Multimedia.objects.all()
            return render(request, "cards/register_faq.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})


@login_required
def register_multimedia_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_multimedia_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['multimedia_types'] = MULTIMEDIA_TYPES
            return render(request, "cards/register_multimedia.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['multimedia_types'] = MULTIMEDIA_TYPES
        return render(request, "cards/register_multimedia.html", template_dict)


@login_required
def register_gateway_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_gateway_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['multimedia_types'] = MULTIMEDIA_TYPES
            return render(request, "cards/register_gateway.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['multimedia_types'] = MULTIMEDIA_TYPES
        return render(request, "cards/register_gateway.html", template_dict)


@login_required
def activate_website_user_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="activate_website_user_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_dict['all_users'] = User.objects.all()

            return render(request, "cards/activate_website_user.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_dict['all_users'] = User.objects.all()

    return render(request, "cards/activate_website_user.html", template_dict)


@csrf_exempt
def newsteama_register_website_user_api(request):
    if request.POST.get('secret_key', None) != API_SECRET_KEY:
        return HttpResponse("Request from this device cannot be identified. Contact the adminstrator for mode details.")

    if request.method == "POST":
        response = manual_register_website_user(request, "REGISTER", False)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_user(request):
    user_det = {}
    if request.method == "GET":
        response = check_customer_data(request.user, request.GET)
        return HttpResponse(response)
    elif request.method == "POST":
        file = upload_receive(request)

        reader = csv.reader(file)

        # loop through uploaded file and create users
        error_list = list()
        counter = 1
        for row in reader:
            if len(row) != 12:
                error_list.append(["Row "+str(counter),
                                   "Incorrect number of fields"])

            # customer_info add data here
            customer_info = request.POST
            customer_info['first_name'] = row[0]
            customer_info['last_name'] = row[1]
            customer_info['telephone'] = row[2]
            customer_info['site'] = row[3]
            customer_info['bit_harvester'] = row[4]
            customer_info['site_manager'] = row[5]
            customer_info['low_balance_warning'] = row[6]
            customer_info['energy_price'] = row[7]
            customer_info['is_user'] = row[8]
            customer_info['language'] = row[9]
            customer_info['line_number'] = row[10]
            customer_info['payment_plan'] = row[11]

            response = check_customer_data(request.user, customer_info)
            if "Fatal Error" in response:
                error_list.append(["Row "+str(counter),response])
            counter += 1

        return HttpResponse(json.dumps({"error_list": error_list}))

    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_website_user(request):
    if request.method == "POST":
        response = manual_register_website_user(request)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_faq_category(request):
    if request.method == "POST":
        post = request.POST
        # Check availability
        category = FAQCategory.objects.filter(name=post['faq_category'])
        if len(category) > 0:
            return HttpResponse("Category name already exist")
        else:
            # insert
            FAQCategory.objects.create(name=post['faq_category'])
            return HttpResponse("Category created successfully")
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_faq(request):
    if request.method == "POST":
        post = request.POST
        # Chreate FAQ
        faq_category = FAQCategory.objects.get(id=post['category'])
        faq = FAQ.objects.create(category=faq_category, question=post[
                                 'question'], answer=post['answer'], keywords=post['keywords'])
        if post.get('has_multimedia', False):
            faq.has_multimedia = True
            faq.multimedia = Multimedia.objects.get(id=post['multimedia'])
        else:
            faq.has_multimedia = False
            faq.multimedia = None
        faq.save()

        return HttpResponse("FAQ created successfully.")

    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_multimedia(request):
    if request.method == "POST":
        post = request.POST
        if post['multimedia_type'] == '':
            return HttpResponse("Select multimedia type")

        try:
            multimedia = Multimedia.objects.create(title=post['title'], multimedia_type=post['multimedia_type'], content=post[
                                                   'content'], thumbnail=post['thumbnail'], is_featured=post.get('is_featured', False), order=post['order'])
            return HttpResponse("Multimedia created successfully.")
        except:
            return HttpResponse("Error in creatinf multimedia.")
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def newsteama_register_gateway(request):
    if request.method == "POST":
        post = request.POST
        name = post['name']
        telephone = post['telephone']
        device_id = post['device_id']
        username = post['username']
        password = post['password']
        message_format = post['message_format']
        send_url = post['send_url']
        receive_url = post['receive_url']
        edit_register = post.get('btn_edit', 'register')

        if len(name) < 2:
            return HttpResponse("Enter valid gateway name")
        if len(telephone) < 2:
            return HttpResponse("Enter valid gateway telephone")
        if len(device_id) < 2:
            return HttpResponse("Enter valid gateway device ID")
        if len(username) < 2:
            return HttpResponse("Enter valid gateway username")
        if len(password) < 2:
            return HttpResponse("Enter valid gateway password")
        if len(send_url) < 2:
            return HttpResponse("Enter valid gateway send URL")
        if len(receive_url) < 2:
            return HttpResponse("Enter valid gateway  steama endpoint")

        if edit_register == 'register':
            try:
                Gateway.objects.get(name=name)
                return HttpResponse("Gateway name already exists. Enter a different name")
            except:
                pass

            try:
                Gateway.objects.get(telephone=telephone)
                return HttpResponse("Gateway telephone number already exists. Enter a different telephone. If you feel you are seeign this by mistake, kindly contact the administrator")
            except:
                pass
            try:
                Gateway.objects.get(username=username)
                return HttpResponse("Gateway username already exists. Enter a different username")
            except:
                pass

            try:
                gateway = Gateway.objects.create(name=name, telephone=telephone, device_id=device_id,
                                                 username=username, sms_type=message_format, send_url=send_url, receive_url=receive_url)
                return HttpResponse("Gateway created successfully.")
            except:
                return HttpResponse("Error in creating gateway. Kindly re-try or contact the administrator.")
        else:
            # edit gateway
            selected_gateway = post['selected_gateway']
            s_gateway = Gateway.objects.get(name=selected_gateway)
            s_gateway.name = name
            s_gateway.telephone = telephone
            s_gateway.device_id = device_id
            s_gateway.username = username
            s_gateway.password = password
            s_gateway.sms_type = message_format
            s_gateway.send_url = send_url
            s_gateway.receive_url = receive_url
            s_gateway.save()

            return HttpResponse("Gateway edited successfully.")

    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def activate_website_user_action(request):
    if request.method == "POST":
        response = manual_activate_website_user(request)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def bulk_edit_users_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="bulk_edit_users_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_dict[
                    'sites'] = Mingi_Site.objects.all().order_by('name')
                template_dict['bitharvesters'] = Bit_Harvester.objects.all(
                ).order_by('harvester_id')
                template_dict['users'] = Mingi_User.objects.all(
                ).order_by('first_name')

            else:
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user).order_by('name')
                template_dict['bitharvesters'] = Bit_Harvester.objects.filter(
                    site__owners=request.user).order_by('harvester_id')
                template_dict['users'] = Mingi_User.objects.filter(
                    site__owners=request.user).order_by('first_name')
            return render(request, "cards/bulk_edit_users.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", template_dict)
    else:
        if request.user.is_staff:
            template_dict['sites'] = Mingi_Site.objects.all().order_by('name')
            template_dict['bitharvesters'] = Bit_Harvester.objects.all(
            ).order_by('harvester_id')
            template_dict['users'] = Mingi_User.objects.all(
            ).order_by('first_name')

        else:
            template_dict['sites'] = Mingi_Site.objects.filter(
                owners=request.user).order_by('name')
            template_dict['bitharvesters'] = Bit_Harvester.objects.filter(
                site__owners=request.user).order_by('harvester_id')
            template_dict['users'] = Mingi_User.objects.filter(
                site__owners=request.user).order_by('first_name')
        return render(request, "cards/bulk_edit_users.html", template_dict)


@login_required
def register_site_card(request):
    template_dict = {}
    users_list = list()

    # get all timezones
    template_dict['timezones'] = pytz.common_timezones

    if request.user.is_staff:
        template_dict['website_users'] = User.objects.all()
    else:
        sites = Mingi_Site.objects.filter(owners=request.user)
        for site in sites:
            for owner in site.owners.all():
                if owner not in users_list and owner.is_staff == False:
                    users_list.append(owner)
        template_dict['website_users'] = users_list

    permission = App_Permissions.objects.filter(
        feature_name="register_site_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            template_dict['currencies'] = CURRENCY_CHOICES
            return render(request, "cards/register_site.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        template_dict['currencies'] = CURRENCY_CHOICES
        return render(request, "cards/register_site.html", template_dict)


@login_required
def newsteama_register_site(request):
    if request.method == "POST":
        response = register_site(request)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def register_bh_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="register_bh_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_dict['sites'] = Mingi_Site.objects.all()
                template_dict['gateway'] = Gateway.objects.all()
                template_dict['bh_versions'] = VERSION_CHOICES
            else:
                template_dict['sites'] = Mingi_Site.objects.filter(
                    owners=request.user)
                template_dict['gateway'] = Gateway.objects.all()
                template_dict['bh_versions'] = VERSION_CHOICES
            return render(request, "cards/register_bh.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_dict['sites'] = Mingi_Site.objects.all()
            template_dict['gateway'] = Gateway.objects.all()
            template_dict['bh_versions'] = VERSION_CHOICES
        else:
            template_dict['sites'] = Mingi_Site.objects.filter(
                owners=request.user)
            template_dict['gateway'] = Gateway.objects.all()
            template_dict['bh_versions'] = VERSION_CHOICES
        return render(request, "cards/register_bh.html", template_dict)


@login_required
def newsteama_register_bh(request):
    if request.method == "POST":
        response = register_bh(request)
        return HttpResponse(response)
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


@login_required
def transactions_csv_card(request):
    template_context = {}

    t = date.today()
    filter_form = TransactionDateTimeFilterForm({
        'min_date': "%s/%s/%s" % (t.month, t.day, t.year)
    })

    template_context['filter_form'] = filter_form

    return render(request, "cards/transactions_csv.html", template_context)


@login_required
def all_messages_card(request):

    template_context = {}

    all_users = Mingi_User.objects.all()

    permission = App_Permissions.objects.filter(
        feature_name="all_messages_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context[
                    'all_bitharvesters'] = Bit_Harvester.objects.all()

            else:
                template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                    site__owners=request.user)
                all_users = all_users.filter(site__owners=request.user)
            template_context['all_users'] = all_users

            if request.user.website_user_profile.utility_management:
                template_context['utility_management'] = True

            return render(request, "cards/all_messages_log.html", template_context)
        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.all()

        else:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                site__owners=request.user)
            all_users = all_users.filter(site__owners=request.user)

        if request.user.website_user_profile.utility_management:
            template_context['utility_management'] = True

        template_context['all_users'] = all_users

        return render(request, "cards/all_messages_log.html", template_context)


@login_required
def newsteama_all_messages_list(request):
    "Filter all messages data"""
    if request.method == "POST":
        params = request.POST

     # Query messages in the last 30 minutes
    MINUTES = MESSAGE_LOAD_TIME
    now_time = gen_zone_aware(request.session.current_timezone, datetime.now(
    ).replace(microsecond=0))  # trim microsends
    query_time = (now_time) - timedelta(minutes=MINUTES)

    min_date = params['min_date']
    max_date = params['max_date']
    user_id = params['user_id']
    bh_id = params['bh_id']
    digest = params['digest']
    interval = params['interval']

    atInQuery = Africas_Talking_Input.objects.all()
    atOutQuery = Africas_Talking_Output.objects.all()
    k2Query = Kopo_Kopo_Message.objects.all()

    # Evaluate the query period
    if interval == 'hour_1':
        query_time = (now_time) - timedelta(hours=1)
    elif interval == 'hour_3':
        query_time = (now_time) - timedelta(hours=3)
    elif interval == 'day_1':
        query_time = (now_time) - timedelta(days=1)
    elif interval == 'week_1':
        query_time = (now_time) - timedelta(days=7)
    elif interval == 'month_1':
        query_time = (now_time) - timedelta(days=30)
    elif interval == 'custom':
        if min_date != '':
            query_time = gen_zone_aware(
                request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
        if max_date != '':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                max_date, "%Y-%m-%d") + timedelta(days=1))
    else:
        query_time = (now_time) - timedelta(minutes=MINUTES)

    if not request.user.is_staff:
        atInQuery = atInQuery.filter(Q(user__site__owners=request.user) | Q(
            bit_harvester__site__owners=request.user))
        atOutQuery = atOutQuery.filter(Q(user__site__owners=request.user) | Q(
            bit_harvester__site__owners=request.user))
        k2Query = k2Query.filter(user__site__owners=request.user)

    if 'load_more' in params:
        now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
            request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
        query_time = (now_time) - timedelta(minutes=MINUTES)
        # Evaluate the query period, with load more
        if interval == 'hour_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=1)
        elif interval == 'hour_3':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=3)
        elif interval == 'day_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=1)
        elif interval == 'week_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=7)
        elif interval == 'month_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=30)
        elif interval == 'custom':
            if min_date != '':
                query_time = gen_zone_aware(
                    request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
            if max_date != '':
                now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                    max_date, "%Y-%m-%d") + timedelta(days=1))
        else:
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(minutes=MINUTES)

    if(digest != ""):
        atInQuery = atInQuery.filter(Q(raw_message__contains=digest))
        atOutQuery = atOutQuery.filter(Q(raw_message__contains=digest) | Q(text_message__contains=digest))
        k2Query = k2Query.filter(Q(amount__contains=digest))

    if(user_id != ""):
        atInQuery = atInQuery.filter(Q(user_id=user_id))
        atOutQuery = atOutQuery.filter(Q(user_id=user_id))
        k2Query = k2Query.filter(Q(user_id=user_id))

    if(bh_id != ""):
        atInQuery = atInQuery.filter(Q(bit_harvester_id=bh_id))
        atOutQuery = atOutQuery.filter(Q(bit_harvester_id=bh_id))
        k2Query = Kopo_Kopo_Message.objects.none()

    # atInQuery = atInQuery.order_by('-id')[:30]
    # atOutQuery = atOutQuery.order_by('-id')[:30]
    # k2Query = k2Query.order_by('-id')[:30]
    atInQuery = atInQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)
    atOutQuery = atOutQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)
    k2Query = k2Query.filter(timestamp__gte=query_time,
                             timestamp__lte=now_time)

    transactions_list = combine_newsteama_transactions(
        atInQuery, atOutQuery, k2Query)
    transactions_list.sort(key=itemgetter('timestamp'), reverse=True)
    # Add last date
    row = {}
    row['query_time'] = str(query_time.replace(tzinfo=None))
    transactions_list.append(row)

    return HttpResponse(json.dumps({"transactions_list": transactions_list}))


@login_required
def newsteama_all_messages(request):
    """
    Allows for viewing, filtering, and exporting of transactions.
    """
    return HttpResponse(newsteama_all_messages_list(request))


@login_required
def payments_messages_card(request):
    template_context = {}

    all_users = Mingi_User.objects.all()

    permission = App_Permissions.objects.filter(
        feature_name="payments_messages_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if not request.user.is_staff:
                all_users = all_users.filter(site__owners=request.user)

            template_context['all_users'] = all_users

            return render(request, "cards/payments_messages_log.html", template_context)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if not request.user.is_staff:
            all_users = all_users.filter(site__owners=request.user)

        template_context['all_users'] = all_users

        return render(request, "cards/payments_messages_log.html", template_context)


@login_required
def newsteama_payments_messages_list(request):
    "Filter payments messages data"""

    if request.method == "POST":
        params = request.POST

    min_date = params['min_date']
    max_date = params['max_date']
    user_id = params['other_party_id']
    digest = params['digest']

    atInQuery = Africas_Talking_Input.objects.none()
    atOutQuery = Africas_Talking_Output.objects.none()

    k2Query = Kopo_Kopo_Message.objects.all()

    if not request.user.is_staff:
        k2Query = k2Query.filter(user__site__owners=request.user)

    if 'load_more' in params:
        k2_last_message_id = params['last_message_id']
        k2Query = k2Query.filter(id__lt=k2_last_message_id)

    if(min_date != ""):
        min_date = gen_zone_aware(
            request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
        k2Query = k2Query.filter(Q(timestamp__gte=min_date))

    if(max_date != ""):
        max_date = gen_zone_aware(request.session.current_timezone, datetime.strptime(
            max_date, "%Y-%m-%d") + timedelta(days=1))
        k2Query = k2Query.filter(Q(timestamp__lte=max_date))

    if(digest != ""):
        k2Query = k2Query.filter(Q(amount__contains=digest))

    if(user_id != ""):
        k2Query = k2Query.filter(Q(user_id=user_id))

    k2Query = k2Query.order_by('-id')[:30]

    transactions_list = combine_newsteama_transactions(
        atInQuery, atOutQuery, k2Query)
    transactions_list.sort(key=itemgetter('timestamp'), reverse=True)
    return HttpResponse(json.dumps({"transactions_list": transactions_list}))


@login_required
def newsteama_payments_messages(request):
    return HttpResponse(newsteama_payments_messages_list(request))


@login_required
def all_sms_card(request):
    template_context = {}

    all_users = Mingi_User.objects.all()

    permission = App_Permissions.objects.filter(
        feature_name="all_sms_card", enabled=True)

    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context[
                    'all_bitharvesters'] = Bit_Harvester.objects.all()
            else:
                template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                    site__owners=request.user)
                all_users = all_users.filter(site__owners=request.user)

            if(request.user.website_user_profile.utility_management):
                template_context['utility_management'] = True

            template_context['all_users'] = all_users

            return render(request, "cards/all_sms_messages_log.html", template_context)

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if request.user.is_staff:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.all()
        else:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                site__owners=request.user)
            all_users = all_users.filter(site__owners=request.user)

        if(request.user.website_user_profile.utility_management):
            template_context['utility_management'] = True

        template_context['all_users'] = all_users

        return render(request, "cards/all_sms_messages_log.html", template_context)


@login_required
def newsteama_all_sms_messages_list(request):
    "Filter all messages data"""
    if request.method == "POST":
        params = request.POST

    # Load messages within this time
    MINUTES = MESSAGE_LOAD_TIME
    # Now time
    now_time = gen_zone_aware(
        request.session.current_timezone, datetime.now()).replace(microsecond=0)
    query_time = (now_time) - timedelta(minutes=MINUTES)

    min_date = params['min_date']
    max_date = params['max_date']
    user_id = params['user_id']
    bh_id = params['bh_id']
    digest = params['digest']
    interval = params['interval']

    atInQuery = Africas_Talking_Input.objects.all()
    atOutQuery = Africas_Talking_Output.objects.all()
    k2Query = Kopo_Kopo_Message.objects.none()

    if interval == 'hour_1':
        query_time = (now_time) - timedelta(hours=1)
    elif interval == 'hour_3':
        query_time = (now_time) - timedelta(hours=3)
    elif interval == 'day_1':
        query_time = (now_time) - timedelta(days=1)
    elif interval == 'week_1':
        query_time = (now_time) - timedelta(days=7)
    elif interval == 'month_1':
        query_time = (now_time) - timedelta(days=30)
    elif interval == 'custom':
        if min_date != '':
            query_time = gen_zone_aware(
                request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
        if max_date != '':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                max_date, "%Y-%m-%d") + timedelta(days=1))
    else:
        query_time = (now_time) - timedelta(minutes=MINUTES)

    if not request.user.is_staff:
        atInQuery = atInQuery.filter(Q(user__site__owners=request.user) | Q(
            bit_harvester__site__owners=request.user))
        atOutQuery = atOutQuery.filter(Q(user__site__owners=request.user) | Q(
            bit_harvester__site__owners=request.user))

    if 'load_more' in params:
        if interval == 'hour_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=1)
        elif interval == 'hour_3':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=3)
        elif interval == 'day_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=1)
        elif interval == 'week_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=7)
        elif interval == 'month_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=30)
        elif interval == 'custom':
            if min_date != '':
                query_time = gen_zone_aware(
                    request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
            if max_date != '':
                now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                    max_date, "%Y-%m-%d") + timedelta(days=1))
        else:
            query_time = (now_time) - timedelta(minutes=MINUTES)

    if(digest != ""):
        atInQuery = atInQuery.filter(Q(raw_message__contains=digest))
        atOutQuery = atOutQuery.filter(Q(raw_message__contains=digest) | Q(text_message__contains=digest))

    if(user_id != ""):
        atInQuery = atInQuery.filter(Q(user_id=user_id))
        atOutQuery = atOutQuery.filter(Q(user_id=user_id))

    if(bh_id != ""):
        atInQuery = atInQuery.filter(Q(bit_harvester_id=bh_id))
        atOutQuery = atOutQuery.filter(Q(bit_harvester_id=bh_id))

    # atInQuery = atInQuery.order_by('-id')[:30]
    # atOutQuery = atOutQuery.order_by('-id')[:30]
    atInQuery = atInQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)
    atOutQuery = atOutQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)

    transactions_list = combine_newsteama_transactions(
        atInQuery, atOutQuery, k2Query)
    transactions_list.sort(key=itemgetter('timestamp'), reverse=True)

    # Add last date
    row = {}
    row['query_time'] = str(query_time.replace(tzinfo=None))
    transactions_list.append(row)

    return HttpResponse(json.dumps({"transactions_list": transactions_list}))


@login_required
def newsteama_all_sms_messages(request):
    """
    Allows for viewing, filtering, and exporting of transactions.
    """
    return HttpResponse(newsteama_all_sms_messages_list(request))


@login_required
def bh_sms_card(request):
    template_context = {}

    all_users = Mingi_User.objects.all()

    permission = App_Permissions.objects.filter(
        feature_name="bh_sms_card", enabled=True)

    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff:
                template_context[
                    'all_bitharvesters'] = Bit_Harvester.objects.all()
            else:
                template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                    site__owners=request.user)
            return render(request, "cards/bh_sms_log.html", template_context)

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if request.user.is_staff:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.all()
        else:
            template_context['all_bitharvesters'] = Bit_Harvester.objects.filter(
                site__owners=request.user)

        return render(request, "cards/bh_sms_log.html", template_context)


@login_required
def newsteama_bh_sms_messages_list(request):
    "Filter all messages data"""
    if request.method == "POST":
        params = request.POST

    # Load messages within this time
    MINUTES = MESSAGE_LOAD_TIME
    # Now time
    now_time = gen_zone_aware(
        request.session.current_timezone, datetime.now()).replace(microsecond=0)
    query_time = (now_time) - timedelta(minutes=MINUTES)

    min_date = params['min_date']
    max_date = params['max_date']
    bh_id = params['other_party_id']
    digest = params['digest']
    interval = params['interval']

    if interval == 'hour_1':
        query_time = (now_time) - timedelta(hours=1)
    elif interval == 'hour_3':
        query_time = (now_time) - timedelta(hours=3)
    elif interval == 'day_1':
        query_time = (now_time) - timedelta(days=1)
    elif interval == 'week_1':
        query_time = (now_time) - timedelta(days=7)
    elif interval == 'month_1':
        query_time = (now_time) - timedelta(days=30)
    elif interval == 'custom':
        if min_date != '':
            query_time = gen_zone_aware(
                request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
        if max_date != '':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                max_date, "%Y-%m-%d") + timedelta(days=1))
    else:
        query_time = (now_time) - timedelta(minutes=MINUTES)

    atInQuery = Africas_Talking_Input.objects.exclude(user_id__isnull=False)
    atOutQuery = Africas_Talking_Output.objects.exclude(user_id__isnull=False)
    k2Query = Kopo_Kopo_Message.objects.none()

    if not request.user.is_staff:
        atInQuery = atInQuery.filter(
            Q(bit_harvester__site__owners=request.user))
        atOutQuery = atOutQuery.filter(
            Q(bit_harvester__site__owners=request.user))

    if 'load_more' in params:
        if interval == 'hour_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=1)
        elif interval == 'hour_3':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=3)
        elif interval == 'day_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=1)
        elif interval == 'week_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=7)
        elif interval == 'month_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=30)
        elif interval == 'custom':
            if min_date != '':
                query_time = gen_zone_aware(
                    request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
            if max_date != '':
                now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                    max_date, "%Y-%m-%d") + timedelta(days=1))
        else:
            query_time = (now_time) - timedelta(minutes=MINUTES)

    if digest != "":
        atInQuery = atInQuery.filter(Q(raw_message__contains=digest))
        atOutQuery = atOutQuery.filter(Q(raw_message__contains=digest) | Q(text_message__contains=digest))

    if bh_id != "":
        atInQuery = atInQuery.filter(Q(bit_harvester_id=bh_id))
        atOutQuery = atOutQuery.filter(Q(bit_harvester_id=bh_id))

    atInQuery = atInQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)
    atOutQuery = atOutQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)

    transactions_list = combine_newsteama_transactions(
        atInQuery, atOutQuery, k2Query)
    transactions_list.sort(key=itemgetter('timestamp'), reverse=True)

    # Add last date
    row = {}
    row['query_time'] = str(query_time.replace(tzinfo=None))
    transactions_list.append(row)

    return HttpResponse(json.dumps({"transactions_list": transactions_list}))


@login_required
def newsteama_bh_sms_messages(request):
    """
    Allows for viewing, filtering, and exporting of transactions.
    """
    return HttpResponse(newsteama_bh_sms_messages_list(request))


@login_required
def customer_sms_log_card(request):
    template_context = {}

    permission = App_Permissions.objects.filter(
        feature_name="customer_sms_log_card", enabled=True)

    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if (request.user.is_staff):
                template_context['all_users'] = Mingi_User.objects.all()
            else:
                template_context['all_users'] = Mingi_User.objects.filter(
                    site__owners=request.user)

            return render(request, "cards/customer_sms_log.html", template_context)

        else:
            return render(request, "cards/permission_denied.html", {})

    else:
        if (request.user.is_staff):
            template_context['all_users'] = Mingi_User.objects.all()
        else:
            template_context['all_users'] = Mingi_User.objects.filter(
                site__owners=request.user)

        return render(request, "cards/customer_sms_log.html", template_context)


@login_required
def newsteama_customer_sms_messages(request):
    """
    Allows for viewing, filtering, and exporting of transactions.
    """
    return HttpResponse(newsteama_customer_sms_messages_list(request))


@login_required
def newsteama_customer_sms_messages_list(request):
    "Filter all messages data"""
    if request.method == "POST":
        params = request.POST
    # Load messages within this time
    MINUTES = MESSAGE_LOAD_TIME
    # Now time
    now_time = gen_zone_aware(
        request.session.current_timezone, datetime.now()).replace(microsecond=0)
    query_time = (now_time) - timedelta(minutes=MINUTES)

    min_date = params['min_date']
    max_date = params['max_date']
    user_id = params['other_party_id']
    digest = params['digest']
    interval = params['interval']

    atInQuery = Africas_Talking_Input.objects.exclude(
        bit_harvester_id__isnull=False)
    atOutQuery = Africas_Talking_Output.objects.exclude(
        bit_harvester_id__isnull=False)
    k2Query = Kopo_Kopo_Message.objects.none()

    if interval == 'hour_1':
        query_time = (now_time) - timedelta(hours=1)
    elif interval == 'hour_3':
        query_time = (now_time) - timedelta(hours=3)
    elif interval == 'day_1':
        query_time = (now_time) - timedelta(days=1)
    elif interval == 'week_1':
        query_time = (now_time) - timedelta(days=7)
    elif interval == 'month_1':
        query_time = (now_time) - timedelta(days=30)
    elif interval == 'custom':
        if min_date != '':
            query_time = gen_zone_aware(
                request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
        if max_date != '':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                max_date, "%Y-%m-%d") + timedelta(days=1))
    else:
        query_time = (now_time) - timedelta(minutes=MINUTES)

    if not request.user.is_staff:
        atInQuery = atInQuery.filter(Q(user__site__owners=request.user))
        atOutQuery = atOutQuery.filter(Q(user__site__owners=request.user))

    if 'load_more' in params:
        if interval == 'hour_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=1)
        elif interval == 'hour_3':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(hours=3)
        elif interval == 'day_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=1)
        elif interval == 'week_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=7)
        elif interval == 'month_1':
            now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                request.POST.get('query_time', str(now_time)), '%Y-%m-%d %H:%M:%S'))
            query_time = (now_time) - timedelta(days=30)
        elif interval == 'custom':
            if min_date != '':
                query_time = gen_zone_aware(
                    request.session.current_timezone, datetime.strptime(min_date, "%Y-%m-%d"))
            if max_date != '':
                now_time = gen_zone_aware(request.session.current_timezone, datetime.strptime(
                    max_date, "%Y-%m-%d") + timedelta(days=1))
        else:
            query_time = (now_time) - timedelta(minutes=MINUTES)

    if(digest != ""):
        atInQuery = atInQuery.filter(Q(raw_message__contains=digest))
        atOutQuery = atOutQuery.filter(Q(raw_message__contains=digest))

    if(user_id != ""):
        atInQuery = atInQuery.filter(Q(user_id=user_id))
        atOutQuery = atOutQuery.filter(Q(user_id=user_id))

    # atInQuery = atInQuery.order_by('-id')[:30]
    # atOutQuery = atOutQuery.order_by('-id')[:30]
    atInQuery = atInQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)
    atOutQuery = atOutQuery.filter(
        timestamp__gte=query_time, timestamp__lte=now_time)

    transactions_list = combine_newsteama_transactions(
        atInQuery, atOutQuery, k2Query)
    transactions_list.sort(key=itemgetter('timestamp'), reverse=True)

    # Add last date
    row = {}
    row['query_time'] = str(query_time.replace(tzinfo=None))
    transactions_list.append(row)

    return HttpResponse(json.dumps({"transactions_list": transactions_list}))


@login_required
def faqs_card(request, action):
    template_dict = {}

    featured_video = Multimedia.objects.filter(
        is_featured=True, multimedia_type='youtube')
    template_dict['featured_video'] = featured_video
    if len(featured_video) > 0:
        template_dict['has_featured_video'] = True
    else:
        template_dict['has_featured_video'] = False

    if 'email' in action:
        template_dict['action'] = "email"

    else:
        template_dict['action'] = "help"

    template_dict['faqs_categories'] = FAQCategory.objects.all()
    return render(request, "cards/faqs.html", template_dict)


@login_required
def faqs_category(request):
    template_dict = {}
    category_id = request.POST['category_id']
    category_FAQs = FAQ.objects.filter(category__id=category_id)
    data = serializers.serialize('json', category_FAQs)
    return HttpResponse(data, content_type="application/json")


@login_required
def newsteama_email_support(request):
    try:
        from_email = request.user.email
        subject = request.POST['subject']
        message = request.POST['message']
        email_message = EmailMessage(subject, message, from_email, to=[
                                     CUSTOMER_SUPPORT_EMAIL])
        email_message.send()
        return HttpResponse("sent")
    except Exception as e:
        return HttpResponse("failed" + str(e))


@login_required
def email_users_card(request):
    """ Card for sending emails to users """

    template_context = {}
    template_context['all_users'] = User.objects.all()

    for user in template_context['all_users']:
        print user.username

    if request.user.is_staff == True:
        return render(request, "cards/user_email.html", template_context)
    else:
        return render(request, "cards/permission_denied.html", template_context)


@csrf_exempt
def send_user_email(request):
    """handle a request to send an email to a user"""

    recipient = request.POST['recipient']
    message = str(request.POST['message'])
    try:
        subject = request.POST['subject']
    except:
        subject = "Steama Dashboard Update"

    if (message == ""):
        response = "Please enter a message"

    elif (recipient == ""):
        response = "Please select a recipient"

    else:
        if (recipient == "ALL"):
            call_command('send_user_email', subject.encode(
                'utf-8'), message.encode('utf-8'))
            response = "Message(s) sent successfully!"

        else:
            call_command('send_user_email', subject.encode(
                'utf-8'), message.encode('utf-8'), o=[recipient])
            response = "Message(s) sent successfully!"

    return HttpResponse(response)


@login_required
def faqs_search(request):
    query_string = ''
    found_entries = None
    if ('q' in request.POST) and request.POST['q'].strip():
        query_string = request.POST['q']

        entry_query = get_query(
            query_string, ['question', 'answer', 'keywords', ])

        found_entries = FAQ.objects.filter(entry_query).order_by('-id')

    data = serializers.serialize('json', found_entries)
    return HttpResponse(data, content_type="application/json")


@login_required
def faqs_multimedia(request):
    post = request.POST
    multimedia_id = post.get('multimedia_id', False)
    # print multimedia_id
    multimedia = Multimedia.objects.filter(id=multimedia_id)

    data = serializers.serialize('json', multimedia)
    return HttpResponse(data, content_type="application/json")
    # return
    # HttpResponse(json.dumps({'title':multimedia.title,'multimedia_type':multimedia.multimedia_type,'content':multimedia.content,'thumbnail':multimedia.thumbnail,'is_featured':multimedia.is_featured}))


def normalize_query(query_string, findterms=re.compile(r'"([^"]+)"|(\S+)').findall, normspace=re.compile(r'\s{2,}').sub):
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    query = None
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


def encode_datetime(obj):
    if isinstance(obj, datetime):
        return obj.astimezone(tz.tzutc()).strftime('%Y-%m-%d')
    raise TypeError(repr(obj) + " is not JSON serializable")


@login_required
def newsteama_logout(request):
    logout(request)
    return redirect("/")


@login_required
def management_command_card(request):
    template_dict = {}
    permission = App_Permissions.objects.filter(
        feature_name="management_command_card", enabled=True)
    if permission:
        user_permitted = permission.filter(enabled_users=request.user)
        if user_permitted:
            if request.user.is_staff and request.user.is_active:
                template_dict['all_task_names'] = current_app.tasks.keys()
                template_dict['all_sites'] = Mingi_Site.objects.all()
                return render(request, "cards/management_command.html", template_dict)

        else:
            return render(request, "cards/permission_denied.html", {})
    else:
        if request.user.is_staff and request.user.is_active:
            template_dict['all_task_names'] = current_app.tasks.keys()
            template_dict['all_sites'] = Mingi_Site.objects.all()
            return render(request, "cards/management_command.html", template_dict)
        else:
            return render(request, "cards/permission_denied.html", {})


@login_required
def management_command(request):
    if request.method == "POST":
        post = request.POST
        if post.get('command', '') == "power_rollups":
            power_rollups.delay()
        elif post.get('command', '') == "daily_power_rollups":
            # Get buffer days
            parameters1 = post.get('parameters1', -1)
            if parameters1 == '':
                parameters1 = -1
            else:
                try:
                    parameters1 = int(parameters1)
                except:
                    parameters1 = -1
            # Get date
            parameters2 = post.get('parameters2', None)
            if parameters2 == '':
                parameters2 = None
            else:
                try:
                    parameters2 = datetime.strptime(parameters2, "%Y-%m-%d")
                except:
                    parameters2 = None

            daily_power_rollups.delay(parameters1, parameters2)

        elif post.get('command', '') == "diagnostics":
            diagnostics.delay()
        elif post.get('command', '') == "timer_commands":
            timer_commands.delay()
        elif post.get('command', '') == "custom_timer_commands":
            custom_timer_commands.delay()
        elif post.get('command', '') == "revenue_rollups":
            revenue_rollups.delay()
        elif post.get('command', '') == "transfer_data":
            parameters1 = post.get('parameters1', None)
            if parameters1 is None or parameters1 == "":
                transfer_data.delay(None)  # excecute with defaults
            else:
                transfer_data.delay(parameters1)
        elif post.get('command', '') == "pdf_report":
            username = post.get('arg')
            email_pdf_report.delay(username)

        elif post.get('command', '') == "change_timezone":
            u_timezone = post.get('arg')
            change_to_timezone(u_timezone)

        elif post.get('command', '') == "delete_revenue_rollups":
            # delete hourly revenue rollups within specified period
            s_site = post.get('site')
            if s_site == "all":
                sites = Mingi_Site.objects.all()
            else:
                sites = [Mingi_Site.objects.get(name=post.get('site'))]

            for site in sites:
                g_timestamp = zone_aware(site, datetime.strptime(
                    post.get('parameters1'), "%Y-%m-%d"))
                l_timestamp = zone_aware(site, datetime.strptime(
                    post.get('parameters2'), "%Y-%m-%d"))
                Hourly_Site_Revenue.objects.filter(
                    site=site, timestamp__gte=g_timestamp, timestamp__lte=l_timestamp).delete()

        else:
            return HttpResponse("Unknown command")

        return HttpResponse("Command initiated successfully")
    else:
        return HttpResponse("Your request could not be completed. Please try again.")


def change_to_timezone(u_timezone):
    """
    Change all sites and users to the selected timezone
    """
    # change sites
    for site in Mingi_Site.objects.all():
        site.timezone = u_timezone
        site.save()
    # change for all users
    for user in User.objects.all():
        user.website_user_profile.timezone = u_timezone
        user.website_user_profile.save()

##########################################################################
# GET MENU ITEMS
##########################################################################


def get_menu(request, menu_id):
    """
    Get menus and sub menus or only one menu id
    """
    menus = get_sub_menu(request, menu_id)

    menu_html = ''
    menu_html += '<ul>'

    for menu in menus:
        if menu.has_menu:
            # get sub menus
            menu_html += '<li class="has-sub">'
            # check for is_staff
            if menu.is_staff:
                if request.user.is_staff:
                    menu_html += '<a href="#" id="' + str(menu.menu_id) + '" title="' + str(
                        menu.menu_title) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(menu.menu_name) + '</span></a>'
            else:
                if menu.menu_type == "utility":
                    if request.user.website_user_profile.utility_management:
                        menu_html += '<a href="#" id="' + str(menu.menu_id) + '" title="' + str(
                            menu.menu_title) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(menu.menu_name) + '</span></a>'
                else:
                    menu_html += '<a href="#" id="' + str(menu.menu_id) + '" title="' + str(
                        menu.menu_title) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(menu.menu_name) + '</span></a>'

            menu_html += get_menu(request, menu)
            menu_html += '</li>'

        else:
            # print menu
            menu_html += '<li class="">'
            if menu.has_text_field:
                if menu.is_staff:
                    if request.user.is_staff:
                        menu_html += '<a href="' + str(menu.menu_action) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(
                            menu.menu_name) + '</span></a><input type="text" id="' + str(menu.menu_id) + '" title="' + str(menu.menu_title) + '" placeholder="' + str(menu.menu_title) + '" />'
                else:
                    if menu.menu_type == "utility":
                        if request.user.website_user_profile.utility_management:
                            menu_html += '<a href="' + str(menu.menu_action) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(
                                menu.menu_name) + '</span></a><input type="text" id="' + str(menu.menu_id) + '" title="' + str(menu.menu_title) + '" placeholder="' + str(menu.menu_title) + '" />'
                    else:
                        menu_html += '<a href="' + str(menu.menu_action) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(
                            menu.menu_name) + '</span></a><input type="text" id="' + str(menu.menu_id) + '" title="' + str(menu.menu_title) + '" placeholder="' + str(menu.menu_title) + '" />'
            else:
                if menu.menu_type == "utility":
                    if request.user.website_user_profile.utility_management:
                        menu_html += '<a href="' + str(menu.menu_action) + '" id="' + str(menu.menu_id) + '" title="' + str(
                            menu.menu_title) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(menu.menu_name) + '</span></a>'
                else:
                    menu_html += '<a href="' + str(menu.menu_action) + '" id="' + str(menu.menu_id) + '" title="' + str(
                        menu.menu_title) + '"><i class="menu-icon ' + str(menu.menu_icon) + '"></i><span>' + str(menu.menu_name) + '</span></a>'
            menu_html += '</li>'

    menu_html += '</ul>'

    return menu_html


def get_sub_menu(request, parent_menu=None):
    """
    Get menu immediate menus by a menu parent
    """
    menu_type = "normal"
    if request.user.website_user_profile.utility_management:
        menu_type = "utility"

    if request.user.website_user_profile.user_menu.all().count() > 1:
        menus = request.user.website_user_profile.user_menu
    else:
        menus = Menu.objects.all()

    if type(parent_menu) == Menu or parent_menu is None:
        menus = menus.filter(menu_parent=parent_menu)
    else:
        menus = menus.filter(menu_parent__id=parent_menu)

    if not request.user.is_staff:
        menus = menus.filter(is_staff=False)

    menus = menus.filter(Q(menu_type="all") | Q(menu_type=menu_type),
                         is_active=True).order_by('menu_order')
    return menus


def errorpage(request, request1):
    return HttpResponse("sorry, page not found")


@csrf_exempt
def stripe_registration(request):
    """
    register a customer on Stripe
    """

    stripe.api_key = settings.STRIPE_API_KEY_SK
    token = request.POST.get('stripeToken[id]')

    customer = stripe.Customer.create(
        description=request.user.username,
        email=request.user.email,
        source=token,
    )

    request.user.website_user_profile.token = customer.id
    request.user.website_user_profile.save()

    return HttpResponse("200 OK")


@login_required
def filter_sites(request):
    label = request.POST['label']
    sites = Mingi_Site.objects.all()

    if not request.user.is_staff:
        sites = sites.filter(owners=request.user)    

    return HttpResponse(filter_sites_by_label(sites, label))
