from django.test import RequestFactory
from mingi_django.tests import SteamaDBTestCase

from new_steama.views import check_customer_data
from django.contrib.auth.models import User
from .models import Mingi_User
from mingi_site.models import Bit_Harvester


class MingiUserTestCase(SteamaDBTestCase):
    """
    Tests for the old Steama (Mingi) platform.
    Tests removed functions and urls
    """

    def setUp(self):
        super(MingiUserTestCase, self).setUp()

        self.factory = RequestFactory()  # fake request

    def test_create_customer(self):
        """
        test creating of mingi customer
        """
        superuser = User.objects.get(username="superusername")
        customer_info = {
            'first_name': 'FileUser',
            'last_name': 'Combo',
            'telephone': '+254727513199',
            'site': 'All User Site',
            'bit_harvester': '+100100100100',
            'site_manager': 'none',
            'low_balance_warning': '75',
            'energy_price': '10.0',
            'is_user': 'Customer',
            'language': 'ENG',
            'line_number': 2,
            'payment_plan': 'Default Settings',
            'payment_plan_amount': '0.0'
        }

        response = check_customer_data(superuser, customer_info)
        # check user was created
        self.assertEqual(
            len(Mingi_User.objects.filter(telephone="+254727513199")), 1)
