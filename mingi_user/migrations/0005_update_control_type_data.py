# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def change_mingi_user_control_type(apps, schema_editor):
    """
    Change customer control types: AUTO -> AUTOL, MAN -> AUTOC
    """
    Mingi_User = apps.get_model('mingi_user', 'Mingi_User')
    for user in Mingi_User.objects.all():
        if user.control_type == "AUTO":
            user.control_type = "AUTOL"

        elif user.control_type == "MAN":
            user.control_type = "AUTOC"

        user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0004_update_control_types'),
    ]

    operations = [
        migrations.RunPython(change_mingi_user_control_type)
    ]
