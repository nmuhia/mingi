# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0002_increase_id_value'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mingi_user',
            name='extra_field_3',
        ),
        migrations.RemoveField(
            model_name='mingi_user',
            name='extra_field_4',
        ),
        migrations.RemoveField(
            model_name='mingi_user',
            name='extra_field_5',
        ),
    ]
