# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mingi_User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('telephone', models.CharField(unique=True, max_length=20)),
                ('first_name', models.CharField(max_length=100, verbose_name=b'First Name')),
                ('last_name', models.CharField(max_length=100, verbose_name=b'Last Name')),
                ('account_balance', models.DecimalField(default=0.0, verbose_name=b'Account Balance', max_digits=14, decimal_places=2)),
                ('energy_price', models.DecimalField(default=0.0, verbose_name=b'Energy Price', max_digits=14, decimal_places=2)),
                ('low_balance_warning', models.DecimalField(default=75.0, verbose_name=b'Low Balance Warning', max_digits=14, decimal_places=2)),
                ('low_balance_level', models.DecimalField(default=75.0, verbose_name=b'Current Low Balance Level', max_digits=14, decimal_places=2)),
                ('is_active', models.BooleanField(default=True)),
                ('line_control_party', models.CharField(default=b'BH', max_length=2, choices=[(b'SM', b'Site Manager'), (b'BH', b'bitHarvester')])),
                ('meter_log_party', models.CharField(default=b'BH', max_length=2, choices=[(b'SM', b'Site Manager'), (b'BH', b'bitHarvester')])),
                ('control_type', models.CharField(default=b'MAN', max_length=4, choices=[(b'MAN', b'Manual'), (b'AUTO', b'Auto')])),
                ('line_number', models.CommaSeparatedIntegerField(max_length=15, null=True, blank=True)),
                ('pulse_ratio', models.FloatField(null=True, blank=True)),
                ('is_user', models.BooleanField(default=True)),
                ('is_agent', models.BooleanField(default=False)),
                ('is_site_manager', models.BooleanField(default=False)),
                ('is_field_manager', models.BooleanField(default=False)),
                ('is_demo', models.BooleanField(default=False)),
                ('language', models.CharField(default=b'ENG', max_length=3, choices=[(b'ENG', b'English'), (b'SWA', b'Swahili')])),
                ('notifications_setting', models.CharField(default=b'OFF', max_length=3, null=True, blank=True, choices=[(b'OFF', b'Off'), (b'LO', b'Low'), (b'HI', b'High')])),
                ('request_data', models.CharField(max_length=50, null=True, blank=True)),
                ('credit_exp_date', models.DateTimeField(null=True, blank=True)),
                ('use_category', models.IntegerField(null=True, blank=True)),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('user_type', models.CharField(default=b'RES', max_length=3, choices=[(b'RES', b'Residential'), (b'BUS', b'Business')])),
                ('TOU_hours', models.CharField(default=b'', max_length=50, blank=True)),
                ('password', models.CharField(max_length=128, blank=True)),
                ('payment_plan', models.CharField(max_length=100, blank=True)),
                ('sub_balance', models.DecimalField(default=0.0, max_digits=8, decimal_places=2)),
                ('extra_field_3', models.CharField(max_length=100, blank=True)),
                ('extra_field_4', models.CharField(max_length=100, blank=True)),
                ('extra_field_5', models.CharField(max_length=100, blank=True)),
                ('bit_harvester', models.ForeignKey(verbose_name=b'BitHarvester', blank=True, to='mingi_site.Bit_Harvester', null=True)),
                ('site', models.ForeignKey(to='mingi_site.Mingi_Site')),
                ('site_manager', models.ForeignKey(verbose_name=b'Site Manager', blank=True, to='mingi_user.Mingi_User', null=True)),
            ],
            options={
                'verbose_name': 'Mingi User',
            },
        ),
        migrations.CreateModel(
            name='User_Record',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=35)),
                ('value', models.CharField(max_length=250)),
                ('user', models.ForeignKey(to='mingi_user.Mingi_User')),
            ],
            options={
                'ordering': ['-user'],
                'verbose_name': 'User Record',
            },
        ),
    ]
