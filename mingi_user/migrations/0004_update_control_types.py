# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0003_auto_20160610_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mingi_user',
            name='control_type',
            field=models.CharField(default=b'AUTOC', max_length=5, choices=[(b'MAN', b'Manual'), (b'AUTOC', b'Automatic/Cloud'), (b'AUTOL', b'Automatic/Local')]),
        ),
    ]
