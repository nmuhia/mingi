# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from mingi_django.mingi_settings.base import MEDIA_ROOT


def create_faq_category(apps, schema_editor):
    """
    Create an FAQ
    """
    FAQCategory = apps.get_model('mingi_django', 'FAQCategory')
    FAQ = apps.get_model('mingi_django', 'FAQ')
    try:
        faq_category = FAQCategory.objects.get(name="Tutorials")
    except:
        faq_category = FAQCategory.objects.create(name="Tutorials")

    question = "How do I register bulk customers through CSV upload ?"
    answer = """<div>Steama allows users to register a large number of customers quickly though CSV upload of customers hence saving time. To register bulk customers to a given site, you need:
    <ul>
        <li>To be logged in</li>
        <li>Have a valid CSV file containing customer details</li>
    </ul>
    <div>
        <p><b>Creating CSV File of Customers Bulk Upload</b></p>
        <ol type='1'>
            <ul>
                <li><b>NB/ This file doesn't require column headers. </b></li>
                <li>Click <a href='media/register-customer/sample-customer-upload.csv' download>Here</a> for a sample file</li>
            </ul>
        <li>Open an excel document (this can be through MS Excel, Google sheets, Calc etc software) </li>
        <li>Add customers details as columns as shown below</li>
        <ul type='square'>
            <li>First name</li>
            <li>Last name</li>
            <li>Customer telephone (in international format)</li>
            <li>Site name</li>
            <li>bitHarvester telephone(in international format)</li>
            <li>Site manager telephone(in international format if applicable, else 'none')</li>
            <li>Low balance warning</li>
            <li>Energy price</li>
            <li>User type ('Customer' or 'Agent')</li>
            <li>Language ('ENG' or 'SWA')</li>
            <li>Line number (field must be in quotes if commas are used)</li>
            <li>Payment plan (field must be in quotes if commas are used)</li>
        </ul>
        <li>Save this file in <b>.csv</b> format</li>
        <li>Log into Steama platform, then <b>Controls > Register > Customer</b></li>
        <li>On <b>Upload a CSV registration file</b> choose the file create above</li>
        <li>Customers in the uploaded file will be created. Any errors in the file will be displayed.</li>
        <li><i><b>Any difficulties ? You can write us for more information. </b></i></li>
        </ol>
    </div>
    </div>"""
    keywords = "csv, register user, upload"

    faq = FAQ.objects.create(category=faq_category, question=question,
                             answer=answer, keywords=keywords)


def undo_faq_category(apps, schema_editor):
    FAQCategory = apps.get_model('mingi_django', 'FAQCategory')
    FAQ = apps.get_model('mingi_django', 'FAQ')
    FAQ.objects.filter(question="How do I register customers through CSV upload ?",
                       category__name="Tutorials").delete()


class Migration(migrations.Migration):
    dependencies = [
        ('mingi_user', '0005_update_control_type_data'),
    ]

    operations = [
        migrations.RunPython(create_faq_category, undo_faq_category)
    ]
