# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import migrations


MINGI_USER_NEW_ID_VALUE = 1470654


def increase_mingi_user_id_value(apps, schema_editor):
    """
    Increase the value of the Mingi_User autoincrementing id field to
    MINGI_USER_NEW_ID_VALUE
    """
    Mingi_User = apps.get_model("mingi_user", "Mingi_User")
    Mingi_Site = apps.get_model("mingi_site", "Mingi_Site")
    dummy_site = Mingi_Site(
        num_lines=1,
        date_online=datetime.date.today()
    )
    dummy_site.save()
    dummy_user = Mingi_User(
        id=MINGI_USER_NEW_ID_VALUE,
        site=dummy_site,
        telephone="0002_increase_id_value-migration-telephone"
    )
    dummy_user.save()
    dummy_user.delete()
    dummy_site.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(increase_mingi_user_id_value)
    ]
