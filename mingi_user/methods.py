import datetime

from mingi_user.models import Mingi_User
from mingi_site.models import Line
from mingi_django.models import Utility, Error_Log

def get_user_utility_lists(user):
    """
        returns list of user lines broken down by utility
    """
    utility_list = []

    if type(user)==Mingi_User:
        for utility in Utility.objects.all():
            utility_info = {}         
            utility_info['name'] = str(utility.name)
            user_lines = list()
            for line in Line.objects.filter(user=user,utility_type=utility):
                user_lines.append(str(line.number))
            if len(user_lines) > 0:
                utility_info['lines'] = user_lines
                utility_list.append(utility_info)
    elif type(user)==Line:
        # check for utilities in lines
        for utility in Utility.objects.all():
            utility_info = {}         
            utility_info['name'] = str(utility.name)
            user_lines = list()
            for line in Line.objects.filter(number=user.number,utility_type=utility):
                user_lines.append(str(line.number))
            if len(user_lines) > 0:
                utility_info['lines'] = user_lines
                utility_list.append(utility_info)
    else:
        # do nothing
        print "do nothing"

    return utility_list
    
def get_site_utility_lists(site):
    """
    returns list of site lines broken down by utility
    """
    utility_list = []
    
    if site.is_combined:
        print "Is site combined: "+str(site.name)
    for utility in Utility.objects.all():
        utility_info = {}         
        utility_info['name'] = str(utility.name)
        site_lines = list()

        for line in Line.objects.filter(site=site,utility_type=utility):
            site_lines.append(str(line.number))
        if len(site_lines) > 0:
            utility_info['lines'] = site_lines
            utility_list.append(utility_info)

    return utility_list

def get_utility_lists(site):
    """
    return list of lines broken down by utility
    """

    users = Mingi_User.objects.filter(site=site,line_number__isnull=False)
    power_lines = list()
    water_lines = list()
    fuel_lines = list()
    non_utility_lines = list()
    power_total = 0.0
    water_total = 0.0
    fuel_total = 0.0
    for user in users:
        if user.first_name == "Electricity:":
            for line in user.line_array:
                power_lines.append(int(line))
        elif user.first_name == "Water:":
            for line in user.line_array:
                water_lines.append(int(line))
        elif user.first_name == "Fuel:":
            for line in user.line_array:
                fuel_lines.append(int(line))
        
        else:
            for line in user.line_array:
                non_utility_lines.append(int(line))
            
    if len(power_lines) > 0 or len(water_lines) > 0 or len(fuel_lines) > 0:
        if len(non_utility_lines) > 0:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="The following lines have unrecognized names for a Utility: %s" % str(non_utility_lines),
                                     site=site)
            
    return [power_lines, water_lines, fuel_lines]
        
