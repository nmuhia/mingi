import json

from models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line, Line_State
from mingi_user.models import Mingi_User
from mingi_django.models import Error_Log, Website_User_Profile, App_Permissions, Utility
from django.contrib.auth.models import User

from africas_talking.models import InvalidUserRegistration,InvalidAttributeValue

from decimal import Decimal, InvalidOperation

from user_energy_usage import get_user_energy_usage_rollup, get_user_lines_energy_usage_rollup
from africas_talking.userResponse import processPhoneNo,sendRegisterMessage
from africas_talking.sendSMS import sendSMS

from django.http import Http404, HttpResponse
from django.contrib.admin.views.decorators import staff_member_required

from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
from django.core.mail import EmailMultiAlternatives

from mingi_django.timezones import gen_zone_aware
from django.contrib.auth.views import password_reset
from mingi_django.mingi_settings.custom import EMAIL_ACTIVATION_ADDRESS
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.management.commands.subscription_plan import subscriber_maintenance

import re
import sys
import random
import string

from mingi_django.mingi_settings.custom import EMAIL_ACTIVATION_ADDRESS

from django.core import serializers
import datetime


def get_mingi_user_info(request,mingi_user):
    """
    Returns a dictionary with information about the given Mingi user.
    """
    ret_dict = mingi_user.__dict__
    ret_dict['user_found'] = True
    
    if ret_dict['site_manager_id'] is not None:
        try:
            SM = Mingi_User.objects.get(id=ret_dict['site_manager_id'])
            ret_dict['site_manager'] = str(SM)
            ret_dict['site_manager_phone'] = SM.telephone

        except Mingi_User.DoesNotExist:
            pass

    try:
        ret_dict['site_name'] = Mingi_Site.objects.get(id=
                                                     ret_dict['site_id']).name
    except Mingi_Site.DoesNotExist, e:
        pass

    try:
        BH = Bit_Harvester.objects.get(id=ret_dict['bit_harvester_id'])
        if BH and BH.version == "V4":
            ret_dict['line_status'] = mingi_user.verbose_line_status
        ret_dict['harvester_id'] = BH.harvester_id
        ret_dict['harvester_phone'] = BH.telephone

    except Bit_Harvester.DoesNotExist, e:
        pass
    
    ret_dict['line_control_party_display'] = mingi_user.get_line_control_party_display()
    ret_dict['meter_log_party_display'] = mingi_user.get_meter_log_party_display()
    ret_dict['notifications_setting_display'] = mingi_user.get_notifications_setting_display()

    payment_plan_name = None
    payment_plan = str(mingi_user.payment_plan)

    if payment_plan != "":
        payment_plan_name = str(payment_plan.split(',')[0])
        if payment_plan_name == "High Energy User" or payment_plan_name == "Monthly Rate" or payment_plan_name == "PowerGen Default":
            ret_dict['threshold'] = float(payment_plan.split(',')[1])
        elif payment_plan_name == "Subscription Plan":
            ret_dict['fee'] = float(payment_plan.split(',')[1])
            ret_dict['days'] = int(payment_plan.split(',')[2])
            ret_dict['threshold'] = payment_plan.split(',')[3]
        else:
            ret_dict['threshold'] = None
    
    ret_dict['payment_plan_name'] = payment_plan_name
    ret_dict['energy_price'] = float(mingi_user.energy_price)
    ret_dict['currency'] = mingi_user.site.currency


    tariff_bundle = None
    if mingi_user.site.bundle_settings != "" or mingi_user.site.bundle_settings is not None:
        tariff_bundle = mingi_user.site.bundle_settings
    
    print "TARIFF BUNDLE: "+str(tariff_bundle)
    ret_dict['tariff_bundle'] = tariff_bundle

    #ret_dict.update(get_user_line_state_info(request,mingi_user))

    return ret_dict

def mingi_user_search(request):
    """
    Handles searching for a Mingi user on the transactions page.
    """

    if request.method == "POST":
        params = request.POST
    else:
        params = request.GET

    if ((params.get('first_name', u'') is u'') and 
        (params.get('last_name', u'') is u'') and
        (params.get('telephone', u'') is u'')):
        raise MingiUserNotFound

    user_query = Mingi_User.objects.all()

    if not request.user.is_staff:
        user_query = user_query.filter(site__owners=request.user)

    if 'first_name' in params:
        user_query = user_query.filter(first_name__icontains=
                                       params['first_name'].strip())

    if 'last_name' in params:
        user_query = user_query.filter(last_name__icontains=
                                       params['last_name'].strip())

    if 'telephone' in params:
        user_query = user_query.filter(telephone__icontains=
                                       params['telephone'].strip())

    if len(user_query) == 0:
        raise MingiUserNotFound

    return user_query[0].id


def newsteama_mingi_user_search(request):
    """
    Handles searching for a Mingi user on the transactions page.
    """

    if request.method == "POST":
        params = request.POST 
        print params       

    user_query = Mingi_User.objects.all()

    if not request.user.is_staff:
        user_query = user_query.filter(site__owners=request.user)
          

    if 'first_name' in params:
        user_query = user_query.filter(first_name__icontains=
                                       params['first_name'].strip())

    if 'last_name' in params:
        user_query = user_query.filter(last_name__icontains=
                                       params['last_name'].strip())

    if 'telephone' in params:
        user_query = user_query.filter(telephone__icontains=
                                       params['telephone'].strip())

    if len(user_query) == 0:
        return "search_failed"

    else:
        return user_query[0].id


def get_user_line_state_info(request,mingi_user):
    """
    Gets a rollup for line state log info for the given user.
    """        

    if request.method == 'POST':
        if ('selected_period' in request.POST):	            
            try:
                start_datetime=request.POST['from_date']
                end_datetime=request.POST['to_date']
                if request.user.website_user_profile.utility_management:
                    return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,start_datetime,end_datetime,True) }
                else:
                    return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,start_datetime,end_datetime,False) }
            except Exception as e:
                print e

        if ('reset_activity' in request.POST):
            if request.user.website_user_profile.utility_management:
                return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,None,None,True) }
            else:
                return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,None,None,False) }

    else:
        if request.user.website_user_profile.utility_management:
            return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,None,None,True) }
        else:
            return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,None,None,False) }

def get_utility_line_state_info(request,mingi_user,utility_type):
    """
    Gets a rollup for line state log info for the given user.
    """        

    if request.method == 'POST':
        if ('selected_period' in request.POST):	            
            try:
                start_datetime=request.POST['from_date']
                end_datetime=request.POST['to_date']
                if request.user.website_user_profile.utility_management:
                    return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,start_datetime,end_datetime,True) }
                else:
                    return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,start_datetime,end_datetime,False) }
            except Exception as e:
                print e

        if ('reset_activity' in request.POST):
            if request.user.website_user_profile.utility_management:
                return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,None,None,True) }
            else:
                return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,None,None,False) }

    else:
        if request.user.website_user_profile.utility_management:
            return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,None,None,True) }
        else:
            return { 'energy_usage_rollup': get_user_lines_energy_usage_rollup(mingi_user,utility_type,None,None,False) }


def manual_activate_website_user(request):
    post = request.POST
    # Check whether user exists
    user = User.objects.filter(id=post['user_id'])[0]
    if user is None:
        return "no such user"
    is_active = False
    if post.get('activate_status','no')=='yes':
        is_active = True

    # Generate new passwords
    password = User.objects.make_random_password()

    # Update active status
    User.objects.select_for_update().filter(id=post['user_id']).update(is_active = is_active)
    # Change password, only if user was inactive
    if is_active:
        try:
            u_user = User.objects.filter(id=post['user_id'])[0]
            u_user.set_password(password)
            u_user.save()
        except:
            return 'error in updating'
    
    # Send user account information, if activation option is on & password has been changed
    if is_active:
        # email = EmailMessage(subject='Steama User Account',body="Welcome",to=[post.get('email','')])
        subject, from_email, to = 'Steama User Account', 'app@steama.co', u_user.email
        html_content = "<p>Dear "+u_user.first_name+",<br/></p><p>Your Steama account has been activated! You can now access Steama services by clicking <a href='https://app.steama.co'>here</a>, or enter the address http://app.steama.co in your browser. Once you have logged in, you can use the Help Center to start learning about the Steama Dashboard.</p><p>Your login credentials are:<br/>Username: "+u_user.username+"<br/>Password: "+password+"</p><p><b>Password Protection</b><br/>Please keep your passwords private, and remember to log out of your account if you are using a public computer.<br/></p><p><b>Password Change</b><br/> To change your password, click on the 'forgot password' link on the application main page.</p><p></br>If you have any questions, you can write to us at <a href='mailto:support@steama.co'>support@steama.co</a>. Thank you and welcome to Steama.</p><p><address>SteamaCo Team<br/><b>Website: </b>www.steama.co</address></p>"
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(html_content, "text/html")

        try:
            msg.send()
        except:
            return 'sucecss, but email not send'


    return 'successful'


def manual_register_website_user(request,request_type="REGISTER",api_call=True):
    """
    Run error-checking on the input website user information, register
    new user if possible, and output status message
    request_type defaults to user registration, else user edit where request_type will be user id
    """   
    if request_type=="REGISTER":
        # Get all post data
        post = request.POST

        # Check errors in input
        input_errors = ""
        if len(post['username']) < 3:
            input_errors += " invalid username, "
        if len(post['first_name'])!=0 and len(post['first_name']) < 3:
            input_errors += " invalid first name, "
        if len(post['last_name'])!=0 and len(post['last_name']) < 3:
            input_errors += " invalid username, "
        if len(post['email']) < 3 and not re.match(r"[^@]+@[^@]+\.[^@]+", post['email']):
            input_errors += " invalid email address, "
        if len(input_errors) > 0 :
            return "Kindly correct these errors and re-submit: " + input_errors

        # Check whether user exists
        register_user = User.objects.filter(username=post['username'])
        if len(register_user) > 0 :
            return "Username already exists : "+str(post['username'])

        # Allow Multiple emails, remove comments to allow unique emails
        # Ensure all email addresses are unique
        # if len(User.objects.filter(email=post['email']))>0:
        #     return "Email address already exists"


        # Auto generate password
        password = post['username']
        today = datetime.datetime.now()

        user = User.objects.create_user(username=post.get('username',''),first_name=post.get('first_name',''),last_name=post.get('last_name',''),email=post.get('email','noemail@steama.co'),password=password)
        user.is_active = False
        user.is_superuser = post.get('is_superuser',False)
        user.is_staff = post.get('is_staff',False)
        user.date_joined = today
        user.last_login = today

        

        # Save website profile information
        profile = Website_User_Profile.objects.create(user=user,user_type=post.get('user_type','CRO'),send_emails=post.get('send_emails',False))

        # if request is from API call
        if api_call == True:
            # Save data for utility management
            if request.user.website_user_profile.utility_management:
                profile.utility_management = True
                for user_utility in post.getlist('utility[]'):
                    profile.utilities.add(user_utility)

            # Save user permissions
            for per in post.getlist('user_permissions[]'):
                permission = App_Permissions.objects.filter(id=per)[0]
                permission.enabled_users.add(user)
                permission.save()

            # Add user to selected site
            for site in post.getlist('user_sites[]'):
                user_sites = Mingi_Site.objects.filter(id=site)[0]
                user_sites.owners.add(user)
                user_sites.save()

        else:            
            # fill the is_demo field
            profile.is_demo = True
            profile.send_emails = False
            try:
                demo_site = Mingi_Site.objects.get(name="Demo Site-I")
                demo_site.owners.add(user)
                demo_site.save()
            except:
                pass # assume we're in a test deployment

            # Add all permissions 
            all_permissions = App_Permissions.objects.all().exclude(feature_name__icontains='activate_website_user_card')
            for permission in all_permissions:
                permission.enabled_users.add(user)
                permission.save()

            # Add add utilities
            all_utilities = Utility.objects.all()
            for utility in all_utilities:
                user.website_user_profile.utilities.add(utility)
            user.save()


        try:
            user.save()
            profile.save()
        except:
            return "Error in creating user. Kindly re-try or contact the administrator if the problem persists"

        # Send admin emaild address concerning this user
        email = EmailMessage(subject='Activate User',body="A website user account created. User Details Username : "+str(post['username'])+"  Email : "+str(post['email'])+"  Date  :  "+str(today.date()),to=EMAIL_ACTIVATION_ADDRESS)

        try:
            email.send()
        except:
            return "User created successfully. Error in sending activation email to administrator. Kindly inform the admin through the email: app@steama.co. State the username and email used."
            pass

        # Send email to user
        email = EmailMessage(subject='Steama User Account',body="Thank you for registering with SteamaCo! Once your account has been approved, you will receive an email with your username and password",to=[post.get('email','')])

        try:
            email.send()
        except:
            pass


        return "User created successfully"
    else:      
        # Get all post data
        post = request.POST
        user_id = request_type               

        # Check whether user exists
        user = User.objects.get(id=user_id)
        profile = Website_User_Profile.objects.get(user=user)

        if not user:
            return "User details not found. Kindly retry or contact the administrator if problem persists"

        user.username = post['username']
        user.email = post['email']
        user.first_name = post['first_name']
        user.last_name = post['last_name']

        if post.get('is_staff') is not None:
            if post.get('is_staff') == "on":
                user.is_staff = True
            else:
                user.is_staff = False

        if post.get('is_superuser') is not None:
            if post.get('is_superuser') == "on":
                user.is_superuser = True
            else:
                user.is_superuser = False
        
        if post.get('send_emails') == "on":    
            profile.send_emails = True
        else:
            profile.send_emails = False

        profile.user_type = post.get('user_type')
        

        for site in Mingi_Site.objects.all(): 
            if str(site.id) in post.getlist('user_sites[]'):
                site.owners.add(user)
            else:
                site.owners.remove(user)

        for permission in App_Permissions.objects.all(): 
            if str(permission.id) in post.getlist('user_permissions[]'):
                permission.enabled_users.add(user)
            else:
                permission.enabled_users.remove(user)

        """
        for utility in Utility.objects.all():
            if str(utility.id) in post.getlist('user_utilities[]'):
                profile.utilities.add(Utility.objects.get(id=utility.id))
            else:
                profile.utilities.remove(Utility.objects.get(id=utility.id))
                """
        try:   
            user.save()
            profile.save()
            return "User details updated successfully"
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
            return "Error in updating user details:"+str(e)


def check_customer_data(login_user,
                        customer_info,
                        customer_id=None):
    """
    Performs detailed error checking on the attempted creation or update of a
    customer, then performs the create/update if everything looks good.

    :param login_user: The Django User object making the request.
    :param customer_info: A dictionary of parameters describing the new/updated
        customer.
    :param customer_id: The id of the customer being edited. This is None (or
        unspecified) if a new customer is being registered.
    :return: A string describing the result of the operation.
    """

    utility_management = False

    if login_user.website_user_profile.utility_management:
        utility_management = True

    try:    
        if customer_id is None:  # Indicates registering a new customer
            site = Mingi_Site.objects.get(name=customer_info['site'])
            user = None
        else:  # Indicates editing an existing customer
            try:
                user = Mingi_User.objects.get(id=customer_id)
                account_balance = Decimal(customer_info['account_balance'])
            except Mingi_User.DoesNotExist as e:
                return "Fatal Error: " + str(e) + ". Please contact your system administrator for assistance."
            except Mingi_User.MultipleObjectsReturned as e:
                return "Fatal Error: " + str(e) + ". Please contact your system administrator for assistance."
            except InvalidOperation:
                raise InvalidAttributeValue("Mingi_User","account balance",customer_info['account_balance'])
            site = user.site

        # raises InvalidUserRegistration if unsuccessful
        try:
            telephone = processPhoneNo(customer_info['telephone'])
            # catch invalid entry for telephone
        except InvalidUserRegistration as e:
            if  (e.error_type == 2) and (user is not None):
                if (user.telephone == customer_info['telephone']):
                    telephone = customer_info['telephone']
                else:
                    return "Fatal Error: " + e.msg + ". Please contact your system administrator for assistance."
            else:
                return "Fatal Error: " + e.msg + ". Please contact your system administrator for assistance."

        first_name = customer_info['first_name']
        last_name = customer_info['last_name']
        if (first_name == "") or (last_name == ""):
            raise InvalidAttributeValue("Mingi_User","name","[empty string]")

        try:
            language = customer_info['language']
        except:
            language = user.language
        if (language != "ENG") and (language != "SWA"):
            raise InvalidAttributeValue("Mingi_User","language",customer_info['language'])

        try:
            low_balance_warning = Decimal(customer_info['low_balance_warning'])
        except InvalidOperation:
            raise InvalidAttributeValue("Mingi_User","low balance warning",customer_info['low_balance_warning'])

        try:
            energy_price = Decimal(customer_info['energy_price'])
        except InvalidOperation:
            raise InvalidAttributeValue("Mingi_User","energy price",customer_info['energy_price'])

        lineNos = str(customer_info['line_number']) \
            if str(customer_info['line_number']).lower() != "none" else ''
        if lineNos:
            lines = lineNos.split(",")
            for lineNo in lines:
                try:
                    line = int(lineNo)
                except ValueError:
                    raise InvalidAttributeValue("Mingi_User","line number(s)",lineNos)
                occupied_lines = Line.objects.filter(number=int(line), site=site).exclude(user=user).exclude(user__isnull=True)
                if len(occupied_lines) > 0:
                    raise OccupiedLinesError()
            
        if ('user_type' in customer_info):
            user_type = customer_info['user_type']
            if user_type != "RES" and user_type != "BUS":
                raise InvalidAttributeValue("Mingi_User","user type",user_type)

        # aggregate payment plan info into a single string
        plan_name = str(customer_info['payment_plan'])
        print plan_name
        if plan_name == "Subscription Plan":
            userPaymentPlan = plan_name+","+str(customer_info['payment_plan_fee'])+","+str(customer_info['payment_plan_days'])+","+str(customer_info['payment_plan_amount'])
        else:
            userPaymentPlan = str(customer_info['payment_plan'])+","+str(customer_info['payment_plan_amount'])
        paymentPlanInfo = userPaymentPlan.split(",")
        print userPaymentPlan

        if (paymentPlanInfo[0] == "Default Settings"):
            paymentPlanInfo[0] = ""
            payment_plan = ""
        elif (paymentPlanInfo[0] == "High Energy User"):
            try:
                getFloat = float(paymentPlanInfo[1])
                payment_plan = userPaymentPlan
            except Exception:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
        elif (paymentPlanInfo[0] == "PowerGen Default"):
            try:
                getFloat = float(paymentPlanInfo[1])
                payment_plan = userPaymentPlan
            except Exception:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
        elif (paymentPlanInfo[0] == "Commercial User"):
            try:
                payment_plan = paymentPlanInfo[0]
            except Exception:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
        elif (paymentPlanInfo[0] == "Monthly Rate"):
            try:
                threshold = float(paymentPlanInfo[1])
                payment_plan = userPaymentPlan
                energy_price = 0.0 # default part of the monthly plan
            except IndexError:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
            except ValueError:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])

        elif (paymentPlanInfo[0] == "Subscription Plan"):
            print "success"
            try:
                fee = Decimal(paymentPlanInfo[1])
                plan_days = int(paymentPlanInfo[2])
                quota = int(paymentPlanInfo[3])
                energy_price = 0.0 # default part of the monthly plan
                payment_plan = userPaymentPlan
            except IndexError:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
            except ValueError:
                raise InvalidAttributeValue("Mingi_User","payment plan",
                                            customer_info['payment_plan'])
            
        else:
            raise InvalidAttributeValue("Mingi_User","payment plan",
                                        customer_info['payment_plan'])

        SM_phone = customer_info['site_manager']
        
        if (SM_phone.lower() != "none"):
            site_manager = Mingi_User.objects.get(telephone=SM_phone)
        else:
            site_manager = None

        BH_phone = customer_info['bit_harvester']
        
        if (BH_phone.lower() != "none"):
            bit_harvester = Bit_Harvester.objects.get(telephone=BH_phone,site=site)
        else:
            bit_harvester = None

        if customer_id is not None:  # Indicates editing an existing customer
            control_type = customer_info['control_type']
            if bit_harvester and bit_harvester.version != "V4":
                if (control_type != "AUTOC") and (control_type != "AUTOL"):
                    raise InvalidAttributeValue("Mingi_User","control_type",customer_info['control_type'])
    
    except InvalidAttributeValue as e:
        if e.attribute and e.value:
            if utility_management == True:
                return "Fatal Error: " + e.value + " is not a valid value for a utility's " + e.attribute + ". Please enter a different value, or contact your system administrator for assistance."
            else:
                return "Fatal Error: " + e.value + " is not a valid value for a customer's " + e.attribute + ". Please enter a different value, or contact your system administrator for assistance."
        else:
            if utility_management == True:
                return "Fatal Error: You entered an invalid value for a utility attribute. Please contact your system administrator for assistance."
            else:
                return "Fatal Error: You entered an invalid value for a customer attribute. Please contact your system administrator for assistance."
        
    except Bit_Harvester.DoesNotExist:
        if utility_management == True:
            return "Fatal Error: the bitHarvester specified does not exist in the utility's site! Please contact your system administrator for assistance."
        else:
            return "Fatal Error: the bitHarvester specified does not exist in the customer's site! Please contact your system administrator for assistance."

    except Mingi_User.DoesNotExist:
        if utility_management == True:
            return "Fatal Error: the site manager specified does not exist in the utility's site! Please contact your system administrator for assistance."
        else:
            return "Fatal Error: the site manager specified does not exist in the customer's site! Please contact your system administrator for assistance."

    except Bit_Harvester.MultipleObjectsReturned:
        return "Fatal Error: system encountered a problem with the requested bitHarvester!  Please contact your system administrator for assistance."

    except Mingi_User.MultipleObjectsReturned:
        return "Fatal Error: system encountered a problem with the requested site manager!  Please contact your system administrator for assistance."

    except Mingi_Site.DoesNotExist:
        return "Fatal Error: the site you requested could not be found! Please contact your system administrator for assistance."
    
    except Mingi_Site.MultipleObjectsReturned:
        return "Fatal Error: system encountered a problem with the requested site! Please contact your system administrator for assistance."

    except OccupiedLinesError:
        return "Fatal Error: the customer's intended lines are already assigned to a different customer."

    is_user = True
    is_agent = False
    if customer_id is None:  # Indicates registering a new customer
        create_user_from_manual_registration(telephone,first_name,last_name,site,site_manager,bit_harvester,low_balance_warning,lineNos,energy_price,is_user,is_agent,language,paymentPlanInfo)
                
        if utility_management == True:
            return "Utility: " + last_name + " (" + telephone + ") has been registered successfully!"
        else:
            return "Customer: " + first_name + " " + last_name + " (" + telephone + ") has been registered successfully!"

    else:  # Indicates editing an existing customer
        edit_user_from_manual_edit(login_user,user,telephone,first_name,last_name,site_manager,bit_harvester,low_balance_warning,lineNos,energy_price,user_type,language,payment_plan,control_type,account_balance)


        # update V4 bitHarvester if necessary (account balance is edited)
        if bit_harvester and bit_harvester.version == "V4":
            if user.account_balance != account_balance:
                try:
                    num_string = (datetime.datetime.now().microsecond)
                    sendSMS(bit_harvester.telephone,"*5%s,CREDIT-SET,%f,%s" % (num_string[4:], account_balance,lineNos),bit_harvester)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))

        if utility_management == True:
            return "Utility: " + last_name + " (" + telephone + ") has been updated successfully!"
        else:
            return "Customer: " + first_name + " " + last_name + " (" + telephone + ") has been updated successfully!"


def create_user_from_manual_registration(telephone,first_name,last_name,site,site_manager,bit_harvester,low_balance_warning,lineNos,energy_price,is_user,is_agent,language,paymentPlanInfo):
    """
    create a new Mingi_User object from manual-registration form
    """

    if (bit_harvester is not None):
        line_control_party = "BH"
        meter_log_party = "BH"
        version = bit_harvester.version
    else:
        line_control_party = "SM"
        meter_log_party = "SM"
        version = "V3"

    if lineNos:
        is_user = True

    if (paymentPlanInfo[0] == "PowerGen Default"):
        starting_balance = -1 * float(paymentPlanInfo[1])
        payment_plan = paymentPlanInfo[0] + "," + paymentPlanInfo[1]
    elif (paymentPlanInfo[0] == "High Energy User"):
        payment_plan = paymentPlanInfo[0] + "," + paymentPlanInfo[1]
        starting_balance = 0.00
    elif paymentPlanInfo[0] == "Subscription Plan":
        payment_plan = paymentPlanInfo[0] + "," + paymentPlanInfo[1] + "," + paymentPlanInfo[2] + "," + paymentPlanInfo[3]
        starting_balance = 0.00
    else:
        payment_plan = paymentPlanInfo[0]
        starting_balance = 0.00
        
    # determine ideal line status default
    needs_manual = False

    if lineNos:
        for line in lineNos.split(','):
            if int(line) >= site.num_lines - 3:
                needs_manual = True

    if version == "V4" and needs_manual == False:
        line_status = '3' # Off - AUTO (Local)
        control_type = 'AUTOL'
    else:
        line_status = '6' # Off - AUTO (Cloud)
        control_type = 'AUTOC'
    
    user = Mingi_User.objects.create(telephone=telephone,
                                     first_name=first_name,
                                     last_name=last_name,
                                     site=site,
                                     site_manager=site_manager,
                                     bit_harvester=bit_harvester,
                                     low_balance_warning=low_balance_warning,
                                     low_balance_level=low_balance_warning,
                                     line_number=lineNos,
                                     energy_price=energy_price,
                                     is_user=is_user,
                                     line_status=line_status,
                                     control_type=control_type,
                                     is_agent=is_agent,
                                     language=language,
                                     payment_plan=payment_plan,
                                     line_control_party=line_control_party,
                                     meter_log_party=meter_log_party,
                                     account_balance = starting_balance)

    if "Subscription" in payment_plan:
        initialize_subscription_user(user,payment_plan,bit_harvester,lineNos)
        user.account_balance = -1*float(paymentPlanInfo[1])
        user.save()

    #link user to lines
    if lineNos:
        lines = lineNos.split(",")
        for lineNo in lines:
            try:
                line = Line.objects.get(number=lineNo,site=site)
                if line.user is not None:
                    Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(line)+" already has a user assigned")
                else:
                    line.user = user
                    line.line_status = line_status
                    line.is_connected = True
                    line.save()
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))

    if site_manager is not None:
        sendRegisterMessage(site_manager.first_name+" "+site_manager.last_name,
                            first_name+" "+last_name,site.operator_name,telephone,language)


def manual_edit_line(request,line_id):
    user_id = request.GET['user']
    line_status = request.GET['line_status']
    pulse_ratio = request.GET['pulse_ratio']
    utility_id = request.GET['utility_type']
    name = request.GET['name']
    if 'is_connected' in request.GET:
        is_connected = True
    else:
        is_connected = False

    try:
        line = Line.objects.get(id=line_id)        
        utility = Utility.objects.get(id=utility_id)
        
        if pulse_ratio is not None and pulse_ratio != "":
            try:
                line.pulse_ratio = float(pulse_ratio)
            except Exception as e:
                return HttpResponse("Pulse ratio value is in the wrong format")                
                pass

        if user_id is not None:
            try:
                user = Mingi_User.objects.get(id=user_id)
                if user.site == line.site:
                    line.user = user                                         
                else:
                    return HttpResponse("User does not exist in the same site as the line")    
                    pass
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
                pass        
        
        line.name = name
        line.line_status = line_status
        line.utility_type = utility
        line.is_connected = is_connected
        line.save()
        return HttpResponse(str(line.name)+ " changes have been saved successfully.")
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
        return HttpResponse(str(e))


def edit_user_from_manual_edit(login_user,user,telephone,first_name,last_name,site_manager,bit_harvester,low_balance_warning,lineNos,energy_price,user_type,language,payment_plan,control_type,account_balance):
    """
    edit a Mingi_User object from manual-edit form
    """

    # first, update user's Line objects
    lines = Line.objects.filter(user=user)

    line_numbers = lineNos.split(',')
    initialize_subscription = False

    # remove user from Lines which they no longer own
    for line in lines:
        if line.number not in line_numbers:
            line.user = None
            line.reset_name
            line.is_connected = False
            line.save()

    # add user to Lines which they now own
    if lineNos:
        is_user = True
        for num in line_numbers:
            try:
                line = Line.objects.get(number=num,site=user.site)
                line.user = user
                line.is_connected = True
                line.save()
            except:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="Error in finding Line with number: %s at site: %s" % (str(num),user.site.name))

    # set back-end control categories
    if (bit_harvester is not None):
        line_control_party = "BH"
        meter_log_party = "BH"
    else:
        line_control_party = "SM"
        meter_log_party = "SM"

    # SUBSCRIPTION PLAN SETTINGS *****************************************************************
    # plan initializing + tariff update for Subscription payment plan, as needed
    if not "Subscription" in user.payment_plan and "Subscription" in payment_plan:
        initialize_subscription = True

        # for V4 plans with quotas, force AUTO control
        if bit_harvester.version == "V4" and int(payment_plan.split(',')[3]) > 0:
            control_type = "AUTOL"

    # ensure tariff is reset from 0 when user is taken off Subscription plan
    elif user.payment_plan != payment_plan and "Subscription" in user.payment_plan and not "Subscription" in payment_plan:
        quota = int(user.payment_plan.split(',')[3])
        if control_type == "AUTOL":
            # for auto lines, ensure account credit is synced
            id_string = str(datetime.datetime.now().microsecond)[2:]
            msg_out = "*%s,CREDIT-ADD,%d,%s" % (id_string,quota,user.line_number)
            try:
                sendSMS(bit_harvester.telephone,msg_out,bit_harvester)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                
        # ensure BH tariff is set back to 1 (default)
        if quota <= 0:
            id_string = str(datetime.datetime.now().microsecond)[2:]
            msg_out = "*%s,TARIFF,1,%s" % (id_string,lineNos)
            try:
                sendSMS(bit_harvester.telephone,msg_out,bit_harvester)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                
    # handle case of an edit to a user's existing subscription plan (e.g. adding a quota)
    elif user.payment_plan != payment_plan and "Subscription" in user.payment_plan and "Subscription" in payment_plan:
        old_quota = int(user.payment_plan.split(',')[3])
        new_quota = int(payment_plan.split(',')[3])

        if old_quota <= 0 and new_quota > 0:
            id_string = str(datetime.datetime.now().microsecond)[2:]
            msg_out = "*%s,TARIFF,1,%s" % (id_string,lineNos)
            try:
                sendSMS(bit_harvester.telephone,msg_out,bit_harvester)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                
        elif old_quota > 0 and new_quota <= 0:
            id_string = str(datetime.datetime.now().microsecond)[2:]
            msg_out = "*%s,TARIFF,0,%s" % (id_string,lineNos)
            try:
                sendSMS(bit_harvester.telephone,msg_out,bit_harvester)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

    # send out commands to update control_type on BH end
    if user.control_type != control_type and bit_harvester is not None:
        if control_type == "AUTOL" and bit_harvester.version == "V4":
            # update line status
            try:
                idnum = '1' + str(datetime.datetime.now().microsecond)[2:]
                sendSMS(bit_harvester.telephone,"*%s,AUTO,%s" % (idnum,lineNos),bit_harvester)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

            # update account balance
            try:
                bh_balance = int(float(account_balance)/float(energy_price) * 1000)
                idnum = '3' + str(datetime.datetime.now().microsecond)[2:]
                if bh_balance<0:
                    bh_balance = 0
                msg = "*%s,CREDIT-SET,%d,%s" % (idnum,bh_balance,lineNos)
                sendSMS(bit_harvester.telephone,msg)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

        elif control_type == "AUTOC" and bit_harvester.version == "V4":
            try:
                idnum = str(datetime.datetime.now().microsecond)[2:]
                if user.line_is_on:
                    msg = "*%s,ON,%s" % (idnum,lineNos)
                else:
                    msg = "*%s,ON,%s" % (idnum,lineNos)
                sendSMS(bit_harvester.telephone,msg)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

        elif bit_harvester.version == "SOLO":

            password = bit_harvester.serial_number
            try:
                if user.line_is_on:
                    msg = "SN" + password + "ON"
                else:
                    msg = "SN" + password + "OFF"
                sendSMS(bit_harvester.telephone,msg)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))
                
    else:
        # check whether AUTO user account balance should be updated
        if user.account_balance != account_balance:
            # send email alerts to E.ON only
            if user.site.company_name == 'E.ON':
                email = EmailMessage(subject='Steama Alert for '+str(user.first_name)+' '+str(user.last_name),body=str(datetime.datetime.now())+ ': Account balance for ' + str(user) + ' at ' + user.site.name + ' was edited manually from '+str(user.account_balance)+' to '+str(account_balance) +' by '+str(login_user.username)+ ' '+str(login_user.email),to=['tzops@eon-offgrid.com'])
                try:
                    email.send()
                except:
                    pass

            if bit_harvester is not None:
                if user.control_type == "AUTOL":
                     # update account balance
                    try:
                        bh_balance = int(float(account_balance)/float(energy_price) * 1000)
                        idnum = '3' + str(datetime.datetime.now().microsecond)[2:]
                        if bh_balance<0:
                            bh_balance = 0
                        msg = "*%s,CREDIT-SET,%d,%s" % (idnum,bh_balance,lineNos)
                        sendSMS(bit_harvester.telephone,msg)
                    except Exception as e:
                        Error_Log.objects.create(problem_type="SEND_SMS",
                                                 problem_message=str(e))

    try:
        user.telephone = telephone
        user.first_name = first_name
        user.account_balance = account_balance
        user.last_name = last_name
        user.site_manager = site_manager
        user.bit_harvester = bit_harvester
        user.low_balance_warning = low_balance_warning
        user.low_balance_level = low_balance_warning
        user.line_number = lineNos
        user.energy_price = energy_price
        user.user_type = user_type
        user.language = language
        user.payment_plan = payment_plan
        user.control_type = control_type
        user.line_control_party = line_control_party
        user.meter_log_party = meter_log_party
            
        user.save()
    except Exception as e:
        Error_Log.objects.create(problem_message=str(e),problem_type="INTERNAL")

    # create a line state snapshot to mark any account balance changes
    now = gen_zone_aware(TIME_ZONE,datetime.datetime.now()) - datetime.timedelta(seconds=1)
    if lineNos:
        for num in line_numbers:
            Line_State.objects.create(site=user.site,number=num,
                                      meter_reading=None,
                                      timestamp=now,
                                      user=user,
                                      account_balance=account_balance)
            
    if initialize_subscription == True:
        initialize_subscription_user(user,payment_plan,bit_harvester,lineNos)


def handle_bulk_user_edit(request):
    """
    Handles bulk edit requests for users.
    """

    edit_fields = {}
    filter_fields = {}
    users_list = ""
    count = 1

    try:
        for param in request.POST.keys():
            if 'editFields[' in param:
                edit_fields[int(param[11:-1])] = request.POST[param]
            elif 'filterFields[' in param:
                filter_fields[int(param[13:-1])] = request.POST[param]

        users_to_edit = get_users_to_edit(filter_fields, request.user)
        for user in users_to_edit:
            users_list += "<tr><td>"+str(count)+". <a  class='bulk_edit_users_link' user_id='"+str(user.id)+"'>"+str(user.first_name)+" " +str(user.last_name)+"</a></td><td>"+str(user.site.name)+"</td></tr>";
            count +=1
        return HttpResponse(users_list)
    except BulkEditParameterError, e:
        return HttpResponse("<tr><td>No users will be edited.</td></tr>")

def bulk_user_edit(request):
    """
    Handles bulk edit requests for users.
    """

    edit_fields = {}
    filter_fields = {}

    try:
        for param in request.POST.keys():
            if 'editFields[' in param:
                edit_fields[int(param[11:-1])] = request.POST[param]
            elif 'filterFields[' in param:
                filter_fields[int(param[13:-1])] = request.POST[param]

        users_to_edit = get_users_to_edit(filter_fields, request.user)
                
        apply_user_edits(users_to_edit, edit_fields)        
        return HttpResponse("Users have been edited successfully")
    except BulkEditParameterError, e:
        return HttpResponse(json.dumps({'error_message': e.message}),
                            content_type="application/json")


def get_users_to_edit(filter_fields, user):
    """
    Gets the set of users matching the given filters, to be bulk edited.
    """

    users_to_edit = Mingi_User.objects.filter(site__owners=user)

    for param in filter_fields:
        if param == 1:
            try:
                val = float(filter_fields[param])
            except ValueError:
                raise BulkEditParameterError("Error: account balance must be a number")
            users_to_edit = users_to_edit.filter(account_balance=val)
        elif param == 2:
            try:
                val = float(filter_fields[param])
            except ValueError:
                raise BulkEditParameterError("Error: energy price must be a number")
            users_to_edit = users_to_edit.filter(energy_price=val)
        elif param == 3:
            try:
                val = float(filter_fields[param])
            except ValueError:
                raise BulkEditParameterError("Error: low balance warning must be a number")
            users_to_edit = users_to_edit.filter(low_balance_warning=val)
        elif param == 4:
            try:
                val = float(filter_fields[param])
            except ValueError:
                raise BulkEditParameterError("Error: current low balance level must be a number")
            users_to_edit = users_to_edit.filter(low_balance_level=val)
        elif param == 5:
            users_to_edit = users_to_edit.filter(site_manager_id=filter_fields[param])
        elif param == 6:            
            users_to_edit = users_to_edit.filter(site_id=filter_fields[param])
        elif param == 7:
            users_to_edit = users_to_edit.filter(bit_harvester_id=filter_fields[param])
        elif param == 8:
            users_to_edit = users_to_edit.filter(line_control_party=filter_fields[param])
        elif param == 9:
            users_to_edit = users_to_edit.filter(meter_log_party=filter_fields[param])
        elif param == 10:
            line_numbers = list()
            for num in filter_fields[param].split(','):
                line_numbers.append(num)
            for user in users_to_edit:
                user_lines = list()
                for u_num in user.line_number.split(','):
                    user_lines.append(u_num)
                if not any(i in user_lines for i in line_numbers):
                    users_to_edit = users_to_edit.exclude(id=user.id)
        elif param == 11:
            users_to_edit = users_to_edit.filter(language=filter_fields[param]) 
        elif param == 12:
            users_to_edit = users_to_edit.filter(notifications_setting=filter_fields[param])
        elif param == 13:
            users_to_edit = users_to_edit.filter(payment_plan=filter_fields[param]) 
        elif param == 14:
            try:
                val = float(filter_fields[param])
            except ValueError:
                raise BulkEditParameterError("Error: sub balance must be a number")
            users_to_edit = users_to_edit.filter(sub_balance=val)

    return users_to_edit


def apply_user_edits(users_to_edit, edit_fields):
    """
    Applies the given bulk edit to the given users.
    """

    # track whether or not Lines must be updated
    site_change = False
    line_change = False
    
    for user in users_to_edit:
        for param in edit_fields:
            if param == 1:
                try:
                    val = float(edit_fields[param])
                except ValueError:
                    raise BulkEditParameterError("Error: account balance must be a number")
                user.account_balance = val
                try:
                    num_string = (datetime.datetime.now().microsecond)
                    sendSMS(bit_harvester.telephone,"*5%s,CREDIT-SET,%f,%s" % (num_string[4:],val,user.line_number),bit_harvester)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))
            elif param == 2:
                try:
                    val = float(edit_fields[param])
                except ValueError:
                    raise BulkEditParameterError("Error: energy price must be a number")
                user.energy_price = val
            elif param == 3:
                try:
                    val = float(edit_fields[param])
                except ValueError:
                    raise BulkEditParameterError("Error: low balance warning must be a number")
                user.low_balance_warning = val
            elif param == 4:
                try:
                    val = float(edit_fields[param])
                except ValueError:
                    raise BulkEditParameterError("Error: current low balance level must be a number")
                user.low_balance_level = val
            elif param == 5:
                user.site_manager_id = edit_fields[param]
            elif param == 6:
                site_change = True
                user.site_id = edit_fields[param]
            elif param == 7:
                user.bit_harvester_id = edit_fields[param]
            elif param == 8:
                user.line_control_party = edit_fields[param]
            elif param == 9:
                user.meter_log_party = edit_fields[param]
            elif param == 10:
                user.line_number = edit_fields[param]
                line_change = True
            elif param == 11:
                user.language = edit_fields[param]
            elif param == 12:
                user.notifications_setting = edit_fields[param]
            elif param == 13:
                user.payment_plan = edit_fields[param]
            elif param == 14:
                try:
                    val = float(edit_fields[param])
                except ValueError:
                    raise BulkEditParameterError("Error: sub balance must be a number")
                user.sub_balance = val

            user.save()

        
        # update Line objects if necessary
        if site_change or line_change:
            for line in Line.objects.filter(user=user):
                line.user = None
                line.save()
                line.reset_name()
            for number in user.line_array:
                try:
                    line = Line.objects.get(site=user.site,number=int(number))
                    line.user = user
                    if ':' in user.first_name:
                        line.name = user.last_name
                    line.save()
                except Line.DoesNotExist:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="No Line found for site: %s, number: %s" % (user.site.name,number))

                    
def initialize_subscription_user(user,payment_plan,bit_harvester,lineNos):
    """
    perform the necessary actions for initializing an account on subscription
    """

    today = datetime.datetime.now().replace(hour=12,minute=0,second=0,microsecond=0)
    user.credit_exp_date=today
    user.save()

    plan_elements = payment_plan.split(',')
    fee = Decimal(plan_elements[1])
    days = int(plan_elements[2])
    quota = int(plan_elements[3])

    subscriber_maintenance(user,payment_plan) # initial run of maintenance function

    # assign correct tariff based on daily quota
    id_string = str(datetime.datetime.now().microsecond)[2:]
    if quota <= 0:
        msg_out = "*%s,TARIFF,0,%s" % (id_string,lineNos)
    else:
        msg_out = "*%s,TARIFF,1,%s" % (id_string,lineNos)
        
    try:
        sendSMS(bit_harvester.telephone,msg_out,bit_harvester)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
    
    msg_out = "You have been registered on the subscription plan. Your energy bill will be %d credits every %d days." % (int(fee),days)
    if quota > 0:
        msg_out += " This plan has a limit of %d Wh / day." % (quota)

    try:
        sendSMS(user.telephone,msg_out,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
        

def delete_user(user_id):
    """
    delete the user record with matching unique phone number
    """

    # find user record
    try:
        user = Mingi_User.objects.get(id=user_id)
    except Mingi_User.DoesNotExist:
        return "No Match Found"
    except Mingi_User.MultipleObjectsReturned:
        return "Multiple Matches Found"

    # check that there are no attached payments
    if len(Kopo_Kopo_Message.objects.filter(user=user)) > 0:
        return "Error: Payments found"

    # disconnect line state snapshots and messages
    snapshots = Line_State.objects.filter(user=user)
    msgs_in = Africas_Talking_Input.objects.filter(user=user)
    msgs_out = Africas_Talking_Output.objects.filter(user=user)

    for snapshot in snapshots:
        snapshot.user = None
        snapshot.save()

    for msg in msgs_in:
        msg.user = None
        msg.save()

    for msg in msgs_out:
        msg.user = None
        msg.save()

    user.delete()
    return "User Successfully Deleted"


class BulkEditParameterError(Exception):
    """
    Thrown when a bulk edit parameter is incorrectly formatted.
    """
    pass


class MingiUserNotFound(Exception):
    """
    Thrown when a Mingi User cannot be found.
    """
    pass


class OccupiedLinesError(Exception):
    """
    Indicates an attempt to assign a user to occupied lines.
    """
    pass
