from django.utils import timezone
import datetime
import arrow
import pytz

from django.db import models
from django.utils.timezone import now
import pytz

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Line_State, Daily_Power, Line

from mingi_django.models import Error_Log
# from mingi_django.timezones import zone_aware,gen_zone_aware

# The threshold of activity for a user to be considered sending messages
SENDING_MESSAGES_PERIOD = datetime.timedelta(days=7)

# The time threshold of power use for a user to be "using power"
USING_POWER_PERIOD = datetime.timedelta(days=2)

# The power threshold for a user to be "using power"
USING_POWER_THRESHOLD = 0.005

class Mingi_User(models.Model):
    """
    Represents a user of the Mingi system; someone who has an account for 
    electricity or is a site manager.
    """

    ENGLISH = 'ENG'
    SWAHILI = 'SWA'
    LANGUAGE_CHOICES = (
        (ENGLISH, 'English'),
        (SWAHILI, 'Swahili'),
    )

    # for a:e personnel, controls error/info messages
    NOTIFY_SETTINGS = (
        ('OFF', 'Off'),
        ('LO', 'Low'),
        ('HI', 'High'),
    )

    SITE_MANAGER = 'SM'
    BIT_HARVESTER = 'BH'

    RESIDENTIAL = 'RES'
    BUSINESS = 'BUS'

    USER_TYPE_CHOICES = (
        (RESIDENTIAL, 'Residential'),
        (BUSINESS, 'Business'),
    )

    CONTROL_CHOICES = (
        (SITE_MANAGER, 'Site Manager'),
        (BIT_HARVESTER, 'bitHarvester'),
    )

    # beginning with BHv4:
    # does software defer to BH line-switching decisions or override them
    CONTROL_TYPE_CHOICES = (
        ('MAN', 'Manual'),
        ('AUTOC', 'Automatic/Cloud'),
        ('AUTOL', 'Automatic/Local'),
    )

    telephone = models.CharField(max_length=20, unique=True)
    first_name = models.CharField(max_length=100, verbose_name="First Name")
    last_name = models.CharField(max_length=100, verbose_name="Last Name")

    # Allows values up to one trillion with two decimal places precision
    account_balance = models.DecimalField(max_digits=14, decimal_places=2,
                                          verbose_name="Account Balance",
                                          default=0.00)
    energy_price = models.DecimalField(max_digits=14, decimal_places=2,
                                       verbose_name="Energy Price",
                                       default=0.00)

    low_balance_warning = models.DecimalField(max_digits=14, decimal_places=2,
                                              verbose_name="Low Balance Warning",
                                              default=75.00)

    # when should the next low balance warning be sent
    low_balance_level = models.DecimalField(max_digits=14, decimal_places=2,
                                            verbose_name="Current Low Balance Level",
                                            default=75.00)

    is_active = models.BooleanField(default=True)

    site_manager = models.ForeignKey('self', blank=True, null=True,
                                     verbose_name="Site Manager")
    site = models.ForeignKey('mingi_site.Mingi_Site')
    bit_harvester = models.ForeignKey('mingi_site.Bit_Harvester',
                                      verbose_name="BitHarvester",
                                      blank=True, null=True)

    # who is responsible for turning the user's line on/off
    line_control_party = models.CharField(max_length=2,
                                          choices=CONTROL_CHOICES,
                                          default=BIT_HARVESTER)

    # who is responsible for logging the user's power use
    meter_log_party = models.CharField(max_length=2,
                                       choices=CONTROL_CHOICES,
                                       default=BIT_HARVESTER)

    # V4: is switching Manual or Auto;
    # if auto, software will not send line-switching commands
    control_type = models.CharField(max_length=5,
                                    choices=CONTROL_TYPE_CHOICES,
                                    default="AUTOC")

    line_number = models.CommaSeparatedIntegerField(max_length=15, blank=True,
                                                    null=True)

    # if present, indicates that line meter readings should be scaled by the
    # indicated number in order to reflect the unit (e.g. 5 pulses = 1 L -->
    # pulse_ratio = 5.0) *** To Be Moved to Line Object ***
    pulse_ratio = models.FloatField(blank=True, null=True)

    # Whether the user is someone who pays for electricity
    is_user = models.BooleanField(default=True)
    # Whether the user is someone who does stuff for us (TBD)
    is_agent = models.BooleanField(default=False)
    # Whether the user is a site manager responsible for updates
    is_site_manager = models.BooleanField(default=False)
    # Whether the user is an access:energy employee in the field
    is_field_manager = models.BooleanField(default=False)
    # Indicates an account whose transactions don't refer to actual energy use
    is_demo = models.BooleanField(default=False)

    # Spelling is standardized by multiple-choice input forms
    language = models.CharField(max_length=3, choices=LANGUAGE_CHOICES,
                                default=ENGLISH)

    notifications_setting = models.CharField(max_length=3,
                                             choices=NOTIFY_SETTINGS,
                                             default='OFF', blank=True,
                                             null=True)

    # allows a:e personnel to monitor certain lines
    request_data = models.CharField(max_length=50, blank=True, null=True)

    # expiration date on a user's account credit
    credit_exp_date = models.DateTimeField(blank=True, null=True)

    # category for grouping people by quantity of energy use:
    # index of category defined in user's site
    use_category = models.IntegerField(blank=True, null=True)

    # GPS coordinates of the customer premesis (optional)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)

    user_type = models.CharField(max_length=3, choices=USER_TYPE_CHOICES,
                                 default=RESIDENTIAL)

    # used for time-of-use pricing, format: max_hour:price_ratio,etc
    TOU_hours = models.CharField(max_length=50, blank=True, default="")

    # To be used later
    password = models.CharField(max_length=128, blank=True)
    payment_plan = models.CharField(max_length=100, blank=True)
    sub_balance = models.DecimalField(max_digits=8, decimal_places=2,
                                      default=0.00)

    def revenue_range(self, start, end):
        """Gets the payments from this user in the given time range."""

        if (start is None):
            payments = Kopo_Kopo_Message.objects.filter(user=self)

        else:
            start_hour = start.replace(minute=0, second=0, microsecond=0)
            end_hour = end.replace(minute=0, second=0, microsecond=0)

            m_now = timezone.make_aware(
                datetime.datetime.now(), timezone=pytz.timezone('utc'))
            now_timestamp = m_now
            if end_hour > now_timestamp:
                end_hour = now_timestamp.replace(
                    minute=0, second=0, microsecond=0)

            payments = Kopo_Kopo_Message.objects.filter(
                user=self, timestamp__gte=start_hour, timestamp__lt=end_hour)

        total_payments = 0.0

        for payment in payments:
            total_payments += float(payment.amount)
        return total_payments

    @property
    def line_array(self):
        if self.line_number:
            return self.line_number.split(',')
        else:
            return []

    @property
    def line_is_on(self):
        lines = Line.objects.filter(user=self)

        # user has a single line
        if len(lines) == 1:
            return (int(lines[0].line_status) < 4)

        # user has no registered line
        elif len(lines) == 0:
            return False

        # user has multipe lines
        else:
            status = (int(lines[0].line_status) < 4)
            for line in lines:
                if status != (int(line.line_status) < 4):
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="User %s has inconsistent line status across multiple lines; please ensure line status is synced, and the user is on Manual control" % str(self))

            return status

    @property
    def line_status(self, verbose_format=False):
        lines = Line.objects.filter(user=self)

        # user has a single line
        if len(lines) == 1:
            status = lines[0].line_status
            return status

        # user has no registered line
        elif len(lines) == 0:
            return None

        # user has multipe lines
        else:
            status = lines[0].line_status
            for line in lines:
                if status != line.line_status:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="User %s has inconsistent line status across multiple lines; please ensure line status is synced, and the user is on Manual control" % str(self))
            return status

    @property
    def verbose_line_status(self):
        lines = Line.objects.filter(user=self)

        # user has a single line
        if len(lines) == 1:
            status = lines[0].line_status
            return Line.LINE_STATUS_CHOICES[int(status)][1]
        # user has no registered line
        elif len(lines) == 0:
            return None

        # user has multipe lines
        else:
            status = lines[0].line_status
            for line in lines:
                if status != line.line_status:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="User %s has inconsistent line status across multiple lines; please ensure line status is synced, and the user is on Manual control" % str(self))
            return Line.LINE_STATUS_CHOICES[int(status)][1]

    @property
    def is_sending_messages(self):
        """
        Whether the user has sent a message or payment in the last 
        SENDING_MESSAGES_PERIOD.
        """
        m_now = timezone.make_aware(
            datetime.datetime.now(), timezone=pytz.timezone('utc'))
        sending_messages_threshold = m_now - SENDING_MESSAGES_PERIOD

        recent_at_input = Africas_Talking_Input.objects.filter(
            user=self).order_by('-timestamp')[:1]
        if len(recent_at_input) != 0:
            if recent_at_input[0].timestamp > sending_messages_threshold:
                return True

        recent_k2_message = Kopo_Kopo_Message.objects.filter(
            user=self).order_by('-timestamp')[:1]
        if len(recent_k2_message) != 0:
            if recent_k2_message[0].timestamp > sending_messages_threshold:
                return True

        return False

    @property
    def is_using_power(self):
        """
        Whether the user has used power in the last USING_POWER_PERIOD.
        """
        for line_num in self.line_array:
            if self.line_using_power(line_num):
                return True
        return False

    def line_using_power(self, line_num):
        """
        Whether the user has used power on the given line in the last 
        USING_POWER_PERIOD.
        """
        m_now = timezone.make_aware(
            datetime.datetime.now(), timezone=pytz.timezone('utc'))
        using_power_threshold = m_now - USING_POWER_PERIOD

        all_line_states = Line_State.objects.filter(
            user=self,
            site=self.site,
            number=line_num,
            meter_reading__isnull=False
        )

        last_line_state = all_line_states.order_by('-timestamp')[:1]
        if len(last_line_state) == 0:
            return False
        last_line_state = last_line_state[0]
        if last_line_state.timestamp < using_power_threshold:
            return False

        threshold_line_state = all_line_states.filter(
            timestamp__lte=using_power_threshold
        ).order_by('-timestamp')
        if len(threshold_line_state) == 0:
            return True
        threshold_line_state = threshold_line_state[0]

        dp = last_line_state.meter_reading - threshold_line_state.meter_reading

        return dp > USING_POWER_THRESHOLD

    def _get_daily_power_used(self, timestamp, start, end):
        """Gets daily power used by this user daily
           Hidden; use daily_power_used instead"""

        try:
            daily_energy_obj = Daily_Power.objects.get(
                user=self,
                timestamp=timestamp
            )
            return daily_energy_obj.amount

        except Daily_Power.DoesNotExist:
            from mingi_django.tasks import create_user_daily_power_rollups
            create_user_daily_power_rollups.delay(timestamp, start, end)
            return 0.0

    def power_used(self, start, end, utility_type=None):
        """
        Gives the amount of power used by the user over the given period.
        """
        total_power_used = 0.0
        if utility_type is None:
            for line_num in self.line_array:
                total_power_used += self.line_power_used(line_num, start, end)
        else:
            # Get total from lines with selected utility
            for line_num in self.line_array:
                if self.first_name == utility_type:
                    total_power_used += self.line_power_used(
                        line_num, start, end)
        return total_power_used

    def daily_power_used(self, start, end, create_rollups=True):
        """
           Gets daily power use for this user in the given time range
        """
        total_daily_energy = 0.0
        # get number of days between two timestamps

        num_days = (end - start).days

        for i in range(num_days):
            start_day = start + datetime.timedelta(days=i)
            end_day = start_day + datetime.timedelta(days=1)
            this_hour = end_day + datetime.timedelta(hours=1)

            rollups = Daily_Power.objects.filter(
                user=self, timestamp=this_hour).order_by('timestamp')

            if (len(rollups) == 0 and create_rollups):
                m_now = timezone.make_aware(
                    datetime.datetime.now(), timezone=pytz.timezone('utc'))
                if(this_hour < m_now):
                    total_daily_energy += float(
                        self._get_daily_power_used(this_hour, start_day, end_day))
            else:
                for rollup in rollups:
                    total_daily_energy += float(rollup.amount)

        return total_daily_energy

    def line_power_used(self, line_num, start, end):
        """
        Gives the amount of power used by the user on the given line over the
        given period.
        """
        all_line_states = Line_State.objects.filter(
            user=self,
            site=self.site,
            number=line_num,
            meter_reading__isnull=False
        )

        last_line_state = all_line_states.filter(timestamp__gte=start,
                                                 timestamp__lte=end).order_by('-timestamp')[:1]

        if len(last_line_state) == 0:
            return 0.0
        last_line_state = last_line_state[0]

        prev_line_state = all_line_states.filter(
            timestamp__lte=start).order_by('-timestamp')[:1]

        if len(prev_line_state) == 0:
            return float(last_line_state.meter_reading)
        prev_line_state = prev_line_state[0]

        dp = last_line_state.meter_reading - prev_line_state.meter_reading
        return float(dp)

    @property
    def current_tou_ratio(self):
        """
        return info about TOU pricing in user-friendly format
        """

        arrow_datetime = arrow.get(datetime.datetime.now())
        ts = arrow_datetime.to(self.site.timezone).datetime
        current_hour = ts.hour

        # if there are no TOU settings, return 1 by default
        current_ratio = 1

        if self.TOU_hours:
            info = self.TOU_hours.split(';')

            # look through hour / ratio pairs for the match for current_hour
            for pair in info:
                [hour, ratio] = pair.split(':')

                # keep looking for the current_ratio as long as you haven't
                # passed the current hour
                if int(hour) <= current_hour:
                    current_ratio = ratio

        return float(current_ratio)

    def __unicode__(self):
        return self.first_name + " " + self.last_name + ": " + self.telephone

    class Meta:
        verbose_name = "Mingi User"


class User_Record(models.Model):
    """
    A record of information about a user which is not needed in everyday 
    automated operations
    """

    user = models.ForeignKey(Mingi_User)

    key = models.CharField(max_length=35)

    value = models.CharField(max_length=250)

    def __unicode__(self):
        return str(self.user) + ": " + self.key + ", " + self.value

    class Meta:
        verbose_name = "User Record"
        ordering = ['-user']
