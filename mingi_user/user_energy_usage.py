"""
Functions for viewing a user's energy usage.

Wesley Verne
"""

from datetime import datetime
import datetime, time


from mingi_site.models import Line_State, Line
from mingi_django.models import Error_Log, Utility
from mingi_user.models import Mingi_User

from django.utils import timezone
from django.http import Http404,HttpResponse
from mingi_django.timezones import zone_aware


def get_utility_cost(user,start, end):
    """ 
    Calculate the user's cost
    """
    total_utility_cost = 0.0
   
    line_states = Line_State.objects.filter(user=user,timestamp__gte=start,timestamp__lte=end)
    line_states = line_states.exclude(account_balance=None)

    if len(line_states) <= 1:
        return 0.0

    for i in range(len(line_states) -1):
        if i == (len(line_states) - 1):
            p1 = line_states[i]
        else:
            p1 = line_states[i+1]

        if i == 0:
            p2 = line_states[i]
        else:
            p2 = line_states[i-1]

        # if one of the relevant logs has a null meter-reading, skip it (take another look at this later)
        if (p1.account_balance is None) or (p2.account_balance is None):
            return 0.0

        total_utility_cost += float(p2.account_balance) - float(p1.account_balance)
        
    return total_utility_cost

def get_user_lines_energy_usage_rollup(user,utility_type,u_type, start_datetime = None,end_datetime = None,is_utility_management=False):  
    """
    Gets the given user's energy usage over the specified time period.  Defaults
    to last three days.
    """
    if u_type == "user":
        line_states = Line_State.objects.filter(user=user)
        line_states = line_states.exclude(account_balance=None)
    else:
        line_obj = user
        line_states = Line_State.objects.filter(site=line_obj.site, number=line_obj.number)
    
    rollup = {
        'rates': {}        
    }

    if user.site.company_name == "E.ON":
        rollup['currency'] = "Credits"
    else:
        rollup['currency'] = user.site.currency 
    try:
        utility = Utility.objects.get(name=utility_type)
        rollup['chart_label'] = utility.get_ts_label(False)
        rollup['primary_unit'] = utility.primary_user_unit
        rollup['secondary_unit'] = utility.secondary_user_unit
    except:
        utility = None
        rollup['chart_label'] = None
        rollup['primary_unit'] = None
        rollup['secondary_unit'] = None

    if start_datetime or end_datetime:

        start_datetime = datetime.datetime.strptime(start_datetime, "%Y-%m-%d")
        end_datetime = datetime.datetime.strptime(end_datetime, "%Y-%m-%d")

        start_datetime = zone_aware(user,start_datetime)
        end_datetime = zone_aware(user,end_datetime)
        
        line_states = line_states.filter(timestamp__gte=start_datetime, timestamp__lte=end_datetime)
        rollup['start_date'] = start_datetime
        rollup['end_date'] = end_datetime
        
    else:
        now = zone_aware(user,timezone.now())
        line_states = line_states.filter(timestamp__gte=(now - datetime.timedelta(days=3)))
        start_datetime = now
        end_datetime = now - datetime.timedelta(days=3) 
        rollup['start_date'] = end_datetime
        rollup['end_date'] = start_datetime    

    first_balances = True

    if u_type == "user":
        rollup['balances'] = []
        try:
            for line in Line.objects.filter(user=user):
                try:
                    line_num = int(line.number)
                except ValueError:
                    error_string = "Invalid line number for user %s %s" % (
                        user.first_name,
                        user.last_name
                    )
                    Error_Log.objects.create(problem_type="OTHER",
                                             problem_message=error_string)
                    continue

                line_num_states = line_states.filter(number=line_num)

                if str(line.utility_type) == str(utility_type):
                    rollup['rates'][line_num] = []

                for i in range(len(line_num_states)):    
                    this_state = line_num_states[i]

                    
                    if first_balances:
                        rollup['balances'].append({
                            'balance': float(this_state.account_balance),
                            'timestamp': js_timestamp(this_state.timestamp)
                        })
                    if str(line.utility_type) == str(utility_type):
                        this_rate = get_energy_usage_rate(line_num_states, i)
                        if this_rate is not None:
                            rollup['rates'][line_num].append({
                                'rate': this_rate,
                                'timestamp': js_timestamp(this_state.timestamp)
                            })
                first_balances = False            

        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
            pass

    else:
        line = user
        site = user.site
        try:
            line_num = int(line.number)
        except ValueError:
            error_string = "Invalid line number for line %s %s" % (line.name)
            Error_Log.objects.create(problem_type="OTHER", problem_message=error_string)
            pass

        line_num_states = line_states.filter(number=line_num,site=site)

        if str(line.utility_type) == str(utility_type):
            rollup['rates'][line_num] = []

        for i in range(len(line_num_states)):    
            this_state = line_num_states[i]
                        
            if str(line.utility_type) == str(utility_type):
                this_rate = get_energy_usage_rate(line_num_states, i)
                if this_rate is not None:
                    rollup['rates'][line_num].append({
                        'rate': this_rate,
                        'timestamp': js_timestamp(this_state.timestamp)
                    })        

    return rollup


def get_user_energy_usage_rollup(user,start_datetime = None,end_datetime = None,is_utility_management=False):  
    """
    Gets the given user's energy usage over the specified time period.  Defaults
    to last three days.
    """
	
    line_states = Line_State.objects.filter(user=user)
    line_states = line_states.exclude(account_balance=None)
    rollup = {
        'rates': {},
        'balances': [],      
    }  
    
    if user.site.company_name == "E.ON":
        rollup['currency'] = "Credits"
    else:
        rollup['currency'] = user.site.currency 
        
    if start_datetime or end_datetime:
        line_states = line_states.filter(timestamp__gte=start_datetime, timestamp__lte=end_datetime)        
        
        rollup['start_date'] = start_datetime
        rollup['end_date'] = end_datetime
        
    else:
        line_states = line_states.filter(timestamp__gte=(timezone.now() - datetime.timedelta(days=3)))
        start_datetime = timezone.now()
        end_datetime = timezone.now() - datetime.timedelta(days=3) 
        rollup['start_date'] = end_datetime
        rollup['end_date'] = start_datetime
    
    if not user.line_number:        
        return rollup

    first_balances = True


    for line_num in user.line_number.split(','):
        try:
            line_num = int(line_num)

        except ValueError:
            error_string = "Invalid line number for user %s %s" % (
                user.first_name,
                user.last_name
            )

            Error_Log.objects.create(problem_type="OTHER",
                                     problem_message=error_string)
            continue

        rollup['rates'][line_num] = []

        line_num_states = line_states.filter(number=line_num)

        for i in range(len(line_num_states)):
            
            this_state = line_num_states[i]

            
            if first_balances:
                rollup['balances'].append({
                    'balance': float(this_state.account_balance),
                    'timestamp': js_timestamp(this_state.timestamp)
                })

            this_rate = get_energy_usage_rate(line_num_states, i)
            if this_rate is not None:
                rollup['rates'][line_num].append({
                    'rate': this_rate,
                    'timestamp': js_timestamp(this_state.timestamp)
                })

        first_balances = False	
    return rollup


def get_energy_usage_rate(line_states, i):
    """
    Estimate the energy usage at the given index of the given line state list.
    Units of Watts (meter readings are given in kWh).
    """    
    if len(line_states) <= 1:
        return None

    if i == (len(line_states) - 1):
        p1 = line_states[i]
    else:
        p1 = line_states[i+1]

    if i == 0:
        p2 = line_states[i]
    else:
        p2 = line_states[i-1]

    # if one of the relevant logs has a null meter-reading, skip it (take another look at this later)
    if (p1.meter_reading is None) or (p2.meter_reading is None):
        return None

    energy_delta = p2.meter_reading - p1.meter_reading
    time_delta = (p2.timestamp - p1.timestamp).total_seconds() / 3600.0

    if time_delta <= 0:
        return 0.0

    return 1000.0 * float(energy_delta) / time_delta


def js_timestamp(dt):
    """
    Get the unix timestamp local to the set timezone, in ms.
    This is calculated by converting to the local timezone, removing
    timezone info (in order to stop aware vs naive complaints), then
    subtracting the unix epoch date.
    """    
    return (timezone.localtime(dt).replace(tzinfo=None) - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
