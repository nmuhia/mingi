from django.contrib import admin
from models import Mingi_User, User_Record

class MingiUserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'telephone', 'account_balance',
                    'site_manager', 'site', 'is_user', 'is_agent',
                    'is_site_manager', 'is_demo', 'language', 'created')
    list_filter = ('site',)


class UserRecordAdmin(admin.ModelAdmin):
    list_display = ('user','key','value')

admin.site.register(Mingi_User, MingiUserAdmin)
admin.site.register(User_Record, UserRecordAdmin)
