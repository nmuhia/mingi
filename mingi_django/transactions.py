import csv, datetime

from django.http import HttpResponse
from mingi_user.user_energy_usage import js_timestamp

def combineTransactions(atInQuery, atOutQuery, k2Query):
    """
    Combines transactions from the three sources into one list.
    """

    transactions_list = list()

    for message in atInQuery:
        transactions_list.append(atInToTable(message))
    for message in atOutQuery:
        transactions_list.append(atOutToTable(message))
    for message in k2Query:
        transactions_list.append(k2ToTable(message))

    return transactions_list


def atInToTable(message):
    """
    Converts the given Africa's Talking input message into a dictionary
    suitable for display in the transactions table.
    """

    row = {}
    row['timestamp'] = message.timestamp
    row['type'] = "Incoming Text"
    if message.user is not None:
        row['other_party'] = str(message.user)
        row['other_party_link'] = getUserLink(message.user)
    elif message.bit_harvester is not None:
        row['other_party'] = str(message.bit_harvester)
        row['other_party_link'] = getBitHarvesterLink(message.bit_harvester)
    else:
        row['other_party'] = '(none)'
    row['digest'] = message.message_type
    row['raw_content'] = message.raw_message

    return row

def atOutToTable(message):
    """
    Converts the given Africa's Talking output message into a dictionary
    suitable for display in the transactions table.
    """

    row = {}
    row['timestamp'] = message.timestamp
    row['type'] = "Outgoing Text"
    if message.user is not None:
        row['other_party'] = str(message.user)
        row['other_party_link'] = getUserLink(message.user)
    elif message.bit_harvester is not None:
        row['other_party'] = str(message.bit_harvester)
        row['other_party_link'] = getBitHarvesterLink(message.bit_harvester)
    else:
        row['other_party'] = '(none)'
    row['digest'] = ""

    if (message.text_message is None) or (message.text_message == ''):
        row['raw_content'] = message.raw_message
    else:
        row['raw_content'] = message.text_message

    return row

def k2ToTable(message):
    """
    Converts the given Kopo Kopo payment message into a dictionary suitable
    for display in the transactions table.
    """

    row = {}
    row['timestamp'] = message.timestamp
    row['reference'] = message.reference
    row['type'] = "Payment"
    
    if message.user is not None:
        row['other_party_link'] = getUserLink(message.user)
        row['site'] = message.user.site.name
        row['other_party'] = str(message.user)
        row['id'] = message.user.id
    else:
        row['other_party_link'] = ""
        row['site'] = ""
        row['other_party'] = message.user
        row['id'] = None

    row['digest'] = str(message.amount)
    row['raw_content'] = message.raw_message
    

    return row


def combine_newsteama_transactions(atInQuery, atOutQuery, k2Query):
    """
    Combines transactions from the three sources into one list.
    """

    transactions_list = list()

    for message in atInQuery:
        transactions_list.append(newsteama_atInToTable(message))
    
    for message in atOutQuery:
        transactions_list.append(newsteama_atOutToTable(message))

    for message in k2Query:
        transactions_list.append(newsteama_k2ToTable(message))
        

    return transactions_list


def newsteama_atInToTable(message):
    """
    Converts the given Africa's Talking input message into a dictionary
    suitable for display in the transactions table.
    """
    row = {}

    if message.user is not None:
        other_party = str(message.user)
        other_party_id = str(message.user.id)
        other_party_type = 'customer'
        if message.user.bit_harvester is not None:
            bh_version = message.user.bit_harvester.version
        else:
            bh_version = "V3"
    elif message.bit_harvester is not None:
        other_party = str(message.bit_harvester)
        other_party_id = str(message.bit_harvester.harvester_id)
        other_party_type = 'bh'
        bh_version = message.bit_harvester.version
    else:
        other_party = 'None'
        other_party_id = ''
        other_party_type = ''
        bh_version = "V3"

    row['timestamp'] = js_timestamp(message.timestamp)
    row['type'] = "Incoming Text"
    row['digest'] = message.raw_message
    row['other_party'] = other_party
    row['other_party_id'] = other_party_id
    row['other_party_type'] = other_party_type
    row['atin_message_id'] = message.id
    row['bh_version'] = bh_version
        
    return row

def newsteama_atOutToTable(message):
    """
    Converts the given Africa's Talking output message into a dictionary
    suitable for display in the transactions table.
    """

    row = {}

    if message.user is not None:
        other_party = str(message.user)
        other_party_id = str(message.user.id)
        other_party_type = 'customer'
        bh_version = message.user.bit_harvester.version
    elif message.bit_harvester is not None:
        other_party = str(message.bit_harvester)
        other_party_id = str(message.bit_harvester.harvester_id)
        other_party_type = 'bh'
        bh_version = message.bit_harvester.version
    else:
        other_party = 'None'
        other_party_id = ''
        other_party_type = ''
        bh_version = "V3"

    if (message.text_message is None) or (message.text_message == ''):
        row['digest'] = message.raw_message
    else:
        row['digest'] = message.text_message

    row['timestamp'] = js_timestamp(message.timestamp)
    row['type'] = "Outgoing Text"    
    row['other_party'] = other_party
    row['other_party_id'] = other_party_id
    row['other_party_type'] = other_party_type
    row['atout_message_id'] = message.id
    row['bh_version'] = bh_version
    
        
    return row

def newsteama_k2ToTable(message):
    """
    Converts the given Kopo Kopo payment message into a dictionary suitable
    for display in the transactions table.
    """

    row = {}

    row['timestamp'] = js_timestamp(message.timestamp)
    row['type'] = "Payment"
    row['digest'] = float(message.amount)
    row['other_party_type'] = 'customer'
    row['other_party'] = str(message.user)
    row['other_party_id'] = str(message.user.id)
    row['k2_message_id'] = message.id

    if message.user and message.user.bit_harvester is not None:
        row['bh_version'] = message.user.bit_harvester.version
    else:
        row['bh_version'] = "V3"
    
    return row



def getUserLink(user):

    # would this fix the error?
    if (user is not None):
        return "/elements/mingiuser/%s/" % user.id
    
    else:
        return "/elements/mingiuser/"

def getBitHarvesterLink(bit_harvester):
    return "/elements/bitharvester/%s/" % bit_harvester.harvester_id

def filterDateTime(params, atInQuery, atOutQuery, k2Query):
    """
    Filters the given queries by the datetime information in params.
    """
    if params['min_date'] is not None:
        min_date = params['min_date']
        if params['min_time'] is not None:
            min_date = datetime.datetime.combine(params['min_date'], 
                                                 params['min_time'])

        atInQuery = atInQuery.filter(timestamp__gte=min_date)
        atOutQuery = atOutQuery.filter(timestamp__gte=min_date)
        k2Query = k2Query.filter(timestamp__gte=min_date)

    if params['max_date'] is not None:
        max_date = params['max_date']
        if params['max_time'] is not None:
            max_date = datetime.datetime.combine(params['max_date'], 
                                                 params['max_time'])

        atInQuery = atInQuery.filter(timestamp__lte=max_date)
        atOutQuery = atOutQuery.filter(timestamp__lte=max_date)
        k2Query = k2Query.filter(timestamp__lte=max_date)

    return (atInQuery, atOutQuery, k2Query)



def filterKeywords(params, template_context, transactions_list):
    """
    Filters the transactions list based on the given set of parameters.
    """
    if params.get('type', None) is not None:
        template_context['type_in'] = params['type']
        transactions_list = filterTable(transactions_list,
                                        'type',
                                        params['type'])
    if params.get('other_party', None) is not None:
        template_context['other_party_in'] = params['other_party'].strip()
        transactions_list = filterTable(transactions_list,
                                        'other_party',
                                        params['other_party'].strip())
    if params.get('digest', None) is not None:
        template_context['digest_in'] = params['digest']
        transactions_list = filterTable(transactions_list,
                                        'digest',
                                        params['digest'])
    if params.get('raw_content', None) is not None:
        template_context['raw_content_in'] = params['raw_content']
        transactions_list = filterTable(transactions_list,
                                        'raw_content',
                                        params['raw_content'])

    return (transactions_list, template_context)

def filterTable(transactions_list, attribute, query):
    """
    Filters the transactions list by the given query on the given attribute.
    For use with the table display.
    """
    return [i for i in transactions_list if 
            query.lower() in i[attribute].lower()]

def transactions_csv_list(transactions_list):
    """
    Creates an http response containing the given transaction list as csv.
    """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="transactions.csv"'
    writer = csv.writer(response)
    writer.writerow(transactions_csv_header())
    for transaction in transactions_list:
        writer.writerow(transactions_csv_row(transaction))
    return response

def transactions_csv_header():
    """
    The header row for outputting transactions as csv.
    """
    return ['Timestamp', 'Message Type', 'Sender/Recipient', 'Digest',
            'Full Content']

def transactions_csv_row(row):
    """
    A transaction as a row for outputting as csv.
    """
    return [row['timestamp'], row['type'], row['other_party'], row['digest'],
            row['raw_content']]
