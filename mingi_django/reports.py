from datetime import date, timedelta, datetime, time

from django.http import HttpResponse

from mingi_site.models import Line_State, Mingi_Site, Site_Record
from mingi_user.models import Mingi_User
from kopo_kopo.models import Kopo_Kopo_Message

from mingi_site.site_energy_usage import get_site_energy_usage_rate
from mingi_user.user_energy_usage import js_timestamp

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.colors import white, black
# from reportlab.lib.utils import ImageReader
#import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt
#import matplotlib.dates as dates

from glob import glob
from django.db.models import Q

# from matplotlib.backends.backend_agg import FigureCanvasAgg


def checkParamFields(params, user, line_numbers):
    """
    make sure all required param fields are present
    """

    if params['min_date'] is None:
        return "parameter missing"

    if params['max_date'] is None:
        return "parameter missing"

    if params['site'] is None:
        return "parameter missing"
    elif user.is_staff:
        try:
            site_name = params['site']
            site = Mingi_Site.objects.get(name=site_name)
            return "pass"
        except Mingi_Site.DoesNotExist:
            return "invalid site parameter: no site named " + site_name
        except Mingi_Site.MultipleObjectsReturned:
            return "invalid site parameter"
    else:
        try:
            site = Mingi_Site.objects.get(name=params['site'],
                                          owners=user)  
            return "pass"
        except Mingi_Site.DoesNotExist:
            return "invalid parameter"
        except Mingi_Site.MultipleObjectsReturned:
            return "invalid parameter"
        
    if line_numbers:

        lineNos = line_numbers.split(',')

        for line in lineNos:
            try:
                check = int(line)
            except InvalidOperation:
                return "invalid parameter: " + line + " not an integer"
    
    return "pass"


def getEnergyTimeline(min_date,max_date,site,line_string):
    """
    get site energy use over time
    """

    snapshots = Line_State.objects.filter(site=site,
                                          timestamp__gte=min_date,
                                          timestamp__lte=max_date)
    for snapshot in snapshots:
        
        pass

    # generate plot and save as png

    return
    

def getPaymentData(min_date,max_date,site,bins):
    """
    return payment histogram for the site, dates, and bins specified
    """

    payments = Kopo_Kopo_Message.objects.filter(timestamp__gte=min_date,
                                                timestamp__lte=max_date)
    payments = payments.exclude(user=None).filter(user__site=site)
    num_total = len(payments)

    payment_total = 0
    amts = []

    # compile dataset
    for record in payments:
        payment_total += record.amount
        amts.append(record.amount)

    # plot data and save image --> IMPROVE ***
    f = plt.figure(figsize=(5,5))
    
    plt.hist(amts,bins,color=['#000066'])

    plt.xlabel('Payment Amount')
    plt.ylabel('Number of Payments')
    plt.title('Payment Distribution')
    f.savefig('payments',dpi=300)
    #f.replace('png','eps')
    
    filename = 'payments.png'

    return [filename, float(payment_total), num_total]
    
    """
    for index in range(0,len(bins)):
        
        bin_payments = payments.filter(amount__gte=bins[index],
                                       amount__lt=bins[index+1])
        bin_total_num = 0
        bin_total_amt = 0

        for payment in bin_payments:
            bin_total_num += 1
            bin_total_amt += payment.amount
            payment_total += payment.amount

        bin_nums.append(bin_total_num)
        bin_amts.append(bin_total_amt)

    # create plot
    num_hist = np.histogram(bin_nums,bins)
    amt_hist = np.histogram(bin_amts,bins)
    """
    
    
        
    # generate plot and save as png
    return


def getSystemPerformance(min_date,max_date,site):
    """
    get stats on system performance for the site and dates in question
    """

    MIN_VOLTS = 22.0

    # get filtered data set
    line_states = Line_State.objects.filter(site=site,timestamp__gte=min_date,
                                            timestamp__lte=max_date)
    voltage_logs = Site_Record.objects.filter(Q(site=site),
                                              Q(timestamp__gte=min_date),
                                              Q(timestamp__lte=max_date),
                                              Q(key="System Voltage") | 
                                              Q(key="BH Data Log"))

    # calculate voltage data
    volt_ts = []
    volt_vals = []
    volt_flags = 0

    for log in voltage_logs:
        value_array = log.value.split(',')
        volt_vals.append(float(value_array[0]))
        volt_ts.append(log.timestamp)
        if value_array[0] <= MIN_VOLTS:
            volt_flags += 1
        print "volts point: " + str(volt_ts) + ", " + str(volt_vals)
        

    # calculate power data
    first_line_states = line_states.filter(number=1)

    if not first_line_states:
        pass # do something about this

    energy_list = []

    for first_line_state in first_line_states:

        this_datetime = first_line_state.timestamp

        current_line_states = line_states.filter(timestamp=this_datetime)

        current_energy = 0.0

        for line_state in current_line_states:
            if (line_state.meter_reading is not None):
                current_energy += float(line_state.meter_reading)

        energy_list.append({
            'energy': current_energy,
            'timestamp': js_timestamp(this_datetime)
        })

    power_ts = []
    power_vals = []

    for i in range(len(energy_list)):
        power_ts.append(datetime.fromtimestamp(energy_list[i]['timestamp']/1000))
        power_vals.append(get_site_energy_usage_rate(energy_list,i))
        print "power point: " + str(energy_list[i]['timestamp']) + ", " + str(power_vals[i])
                
    f = plt.figure(figsize=(10,4))
    ax1 = f.add_subplot(111)
    plt.gca().xaxis.set_major_formatter(dates.DateFormatter('%d/%m/%Y'))
    plt.gca().xaxis.set_major_locator(dates.DayLocator())
    ax2 = ax1.twinx()
    # ax1.set_xlabel('Timestamp')
    ax1.set_ylabel('Power Use (kW)')
    ax2.set_ylabel('System Voltage')
    plt.legend()
    line2 = ax2.plot(volt_ts,volt_vals,color='#000066',label='System Voltage')
    line1 = ax1.plot(power_ts,power_vals,color='orange',label="Power Use (kW)")
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=60)
    lines = line1+line2
    labels = [l.get_label() for l in lines]
    ax1.legend(lines,labels)

    ax1.set_title('System Activity')

    f.savefig('site_graph',dpi=300)
    filename = "site_graph.png"

    return [filename, volt_flags]
             

def compilePDFReport(params):
    
    # call functions to generate graphs and insert into PDF

    min_date = datetime.combine(params['min_date'],datetime.min.time())
    max_date = datetime.combine(params['max_date'],datetime.max.time())
    site = Mingi_Site.objects.get(name=params['site'])
    bins = [0,50,100,150,200,500,1000,2000,5000] # edit: change based on site's bundle intervals

    # use ReportLab package to piece together PDF report
    report_filename = "TestReport.pdf"

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=' + report_filename

    report = canvas.Canvas(response)

    # draw header (*** separate out into drawHeader function ***)
    report.drawImage('ae_logo.png',1*inch,9.5*inch,1.25*inch,1.25*inch,mask=[250,255,250,255,250,255])

    report.setFillColorRGB(1.0,0.55,0.23)
    report.rect(2.5*inch,9.75*inch,5*inch,0.75*inch,stroke=False,fill=True)

    report.setFillColor(white)
    report.setFont('Helvetica',20)

    title_string = "Site Summary: " + site.name
    report.drawString(2.75*inch,10.1*inch,title_string)
    report.setFont('Helvetica',12)
    report.drawString(2.75*inch,9.85*inch,str(params['min_date']) + " - " + str(params['max_date']))

    # Payments section (*** separate out into drawPaymentsSection function ***)
    report.setFillColor(black)
    report.setFont('Helvetica',16)
    report.drawString(1*inch,8.75*inch,"Payments Summary")
    report.line(1*inch,8.7*inch,7*inch,8.7*inch)

    [add_image, payment_total, num_total] = getPaymentData(min_date,max_date,site,bins)

    report.setFont('Helvetica',12)
    report.drawString(1.25*inch,8.25*inch,"Payments Received (#):        " + str(num_total))
    report.drawString(1.25*inch,8*inch,"Payments Received (KES):  " + str(payment_total))
    fee_total = payment_total * 0.015
    report.drawString(1.25*inch,7.75*inch,"Processing Fees (KES):        " + str(fee_total))
    revenue = payment_total - fee_total
    report.drawString(1.25*inch,7.5*inch,"Total Revenue (KES):           " + str(revenue))

    # add: specify page size (A4 vs. letter?)
    report.drawImage(add_image,4.5*inch,6*inch,width=2.5*inch,height=2.5*inch)

    # site performance section (*** separate out ***)
    report.setFont('Helvetica',16)
    report.drawString(1*inch,5.5*inch,"Performance Summary")
    report.line(1*inch,5.45*inch,7*inch,5.45*inch)

    [filename, volt_stats] = getSystemPerformance(min_date,max_date,site)

    report.drawImage(filename,3.25*inch,3.75*inch,width=4*inch,height=1.5*inch)
    report.setFont('Helvetica',12)
    report.drawString(1.25*inch,5*inch,"Low Voltage Flags:  " + str(volt_stats))
    # issues flagged, voltage stats, total power generated/used, power/voltage ggraph for that time period

    # site activity section (*** separate out ***)
    report.setFont('Helvetica',16)
    report.drawString(1*inch,3.25*inch,"System Activity")
    report.line(1*inch,3.2*inch,7*inch,3.2*inch)

    # highlighted lines, other user stats

    report.showPage()
    report.save()

    #test_report.save()

    # test_report.setFont("Helvetica", 14)
    # test_report.setStrokeColorRGB(0.2,0.5,0.3)

    return response
