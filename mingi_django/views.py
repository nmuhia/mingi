import calendar
from operator import itemgetter
from datetime import date, datetime

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, StreamingHttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from transactions import *
from dashboard import *
from forms import *
from africas_talking.sendSMS import *
from reports import *
from mingi_user.views import *
from mingi_site.views import *
from mingi_site.charts import *
from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log,Timer_Command
from mingi_django.tasks import *

from mingi_django.management.commands.get_finance_report import *
from django.contrib.auth.views import login, logout, password_change
from axes.decorators import watch_login

@watch_login
def login_page(request):
    """
    Handles logging in of an administrative user.
    """

    if request.user.is_authenticated():
        return redirect(request.GET.get('next', '/'))

    if request.method == "POST":
        user = authenticate(username=request.POST.get('username', ''),
                            password=request.POST.get('password', ''))
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(request.GET.get('next', '/'))
            else:
                return render(request, "login.html",
                              {'login_error': 'Disabled Account'})
        else:
            return render(request, "login.html",
                          {'login_error': 'Login Failed'})

    return render(request, "login.html", {})

def logout_page(request):
    """
    Handles logging out of an administrative user.
    """
    
    logout(request)
    return redirect("/")

class Echo(object):
    def write(self,value):
        return value   

def not_found_page(request):
    """
    A page for handling 404 errors.
    """
    return HttpResponse("404 Not Found", status=404)

def server_error_page(request):
    """
    A page for handling 500 errors.
    """
    return HttpResponse("500 Server Error", status=500)


def send_front_end_sms(SMSTo,message):
    """
    send an sms specified from the user interface
    """

    try:
        sendSMS(SMSTo,message)
        return "Message sent successfully!"
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
        return "Error: System encountered an error and the message was not sent. Please contact your site administrator for assistance."


def send_customer_sms(request,template_context):
    """
    handle a request to send an SMS to a customer
    """

    site_name = request.POST['site']
    user_phone = request.POST['user_phone']
    message = request.POST['message_text']
    
    if (site_name) and (user_phone):
        template_context['client_response'] = "Error: Message not sent. Please either select a site OR an individual user"
        
    elif (message == ""):
        template_context['client_response'] = "Please enter a message!"
        
    elif (site_name):
        users = Mingi_User.objects.filter(site__name=site_name)
        
        # add on filter statements when new send options are added
        if (request.POST['filter'] == "line_owners"):
            users = users.filter(is_user=True)
            
        for user in users:
            template_context['client_response'] = send_front_end_sms(user.telephone,message)
                
    elif (user_phone):
        template_context['client_response'] = send_front_end_sms(user_phone,message)
    else:
        template_context['client_response'] = "Please specify a recipient!"

    return template_context	

def add_timer_command(request,template_context):
    """
    add a new Timer_Command
    """

    try:
        user = locateUser(request.POST['destination_phone'])
        site = user.site
        
        message = request.POST['message']
        if not message:
            template_context['command_response'] = "Fatal Error: Please specify a message!"
            return template_context

        hours = request.POST['hours']
        if not hours:
            template_context['command_response'] = "Fatal Error: Please specify the time(s) the command should be sent!"
            return template_context
        
        Timer_Command.objects.create(destination_phone=user.telephone,
                                     message=message,
                                     site=site,
                                     hours=request.POST['hours'])
        template_context['command_response'] = "Command registered successfully!"
        
    except UserNotFound as e:
        template_context['command_response'] = "Fatal Error: Cannot add Timer Command. Phone number specified does not match a registered recipient!"

    return template_context

def login_limit_exceeded(request):
    """
    Redirects the user on incorrect login limits
    """
    return render(request, "login_limit_message.html", {})
