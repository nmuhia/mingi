from django.conf.urls import patterns, include, url
from django.contrib import admin
from kopo_kopo.views import SmssyncPaymentView

admin.autodiscover()

urlpatterns = patterns('africas_talking.views',

    url(r'^at/$', 'at_handler'),
    url(r'^tw_msg/$', 'tw_handler'),
    url(r'^tz_msg/$', 'tz_msg_handler'),
    url(r'^np_msg/$', 'np_msg_handler'),
    url(r'^bn_msg/$', 'bn_msg_handler'),


)

urlpatterns += patterns('kopo_kopo.views',

    url(r'^k2/$', 'k2_handler'),

)

# SMSSync page
urlpatterns += patterns('kopo_kopo.views',
    url(r'^smssync/payment/$', SmssyncPaymentView.as_view(), name='kopokopo.smssync_payment'),

)

urlpatterns += patterns('mingi_django.migration_in',

    # Uncomment to allow incoming migrations (this should be done for a short
    # time only, due to security concerns)
    #url(r'migrate/in/', 'receive_row'),
                        
)


urlpatterns += patterns('',
    url(r'^payments/payment/', SmssyncPaymentView.as_view(), name='kopokopo.smssync_payment'),

)

urlpatterns += patterns('',
    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    url(r'^admin/', include('smuggler.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^utils/', include('utils.urls')),

)

handler404 = "mingi_django.views.not_found_page"
handler500 = "mingi_django.views.server_error_page"
