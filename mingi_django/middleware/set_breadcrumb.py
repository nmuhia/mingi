from mingi_django.urls import urlpatterns
from mingi_django.models import Menu

class FilterBreadcrumbMiddleware(object):
    def process_request(self, request):
    	get =  request.GET
    	menu_id = get.get('menu_id',"none")
        request.breadcrumb = "Menus > Menus > Menus"
