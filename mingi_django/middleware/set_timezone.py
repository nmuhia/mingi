import pytz

from django.utils import timezone
from mingi_django.mingi_settings.base import TIME_ZONE

class MyTimezoneMiddleware(object):
    def process_request(self, request):
        """
        This middleware activates timezone for users. 
        ** get site specific timezone, if vailable else
        ** get default user timezone -- this is a set/calculated value else
        ** get the system timezone else
        ** activate UTC timezone.
        """
        activate_timezone = TIME_ZONE
        
        try:
            # first get site specific timezone
            user_timezone = request.user.website_user_profile.timezone
            if user_timezone:
                # activate site specific timezone
                activate_timezone = user_timezone
            else:
                activate_timezone = TIME_ZONE
        except:
            activate_timezone = TIME_ZONE
            
        # activate timezone
        request.session['current_timezone'] = activate_timezone # enable gettingthe current timezone through sessions
        request.session.current_timezone = activate_timezone # enable gettingthe current timezone through sessions
        request.session['default_timezone'] = TIME_ZONE # enable gettingthe current timezone through sessions
        request.session.default_timezone = TIME_ZONE # enable gettingthe current timezone through sessions
        
        timezone.activate(pytz.timezone(activate_timezone)) # activate timezone here
