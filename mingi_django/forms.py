from django import forms

class TransactionDateTimeFilterForm(forms.Form):
    min_date = forms.DateField(required=False)
    max_date = forms.DateField(required=False)
    min_time = forms.TimeField(required=False)
    max_time = forms.TimeField(required=False)


class CSVDateSiteFilterForm(forms.Form):
    min_date = forms.DateField(required=False)
    max_date = forms.DateField(required=False)
    site = forms.CharField(required=False)

