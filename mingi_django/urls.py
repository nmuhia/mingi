from django.conf.urls import patterns, include, url
from django.contrib import admin
from kopo_kopo.views import SmssyncPaymentView
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from mingi_settings.base import MEDIA_URL, MEDIA_ROOT

admin.autodiscover()

urlpatterns = patterns('mingi_django.views',

    # Authentication
    url(r'^login/$', 'login_page'),
    url(r'^logout/$', 'logout_page', name='logout_page'),
    url(r'^admin/logout/$', 'logout_page'),
    url(r'^login/limit/$', 'login_limit_exceeded'),
    url(r'^admin/login/limit/$', 'login_limit_exceeded'),
)

urlpatterns += patterns('africas_talking.views',

    url(r'^at/$', 'at_handler'),
    url(r'^tw_msg/$', 'tw_handler'),
    url(r'^tz_msg/$', 'tz_msg_handler'),
    url(r'^np_msg/$', 'np_msg_handler'),
    url(r'^ke_msg/$', 'ke_msg_handler'),
    url(r'^bn_msg/$', 'bn_msg_handler'),
    url(r'^sga_msg/$', 'sga_msg_handler'),

)

urlpatterns += patterns('africas_talking.SmsGateway',

    url(r'^all_msg_handler/$', 'all_msg_handler'),
    url(r'^gateway/$', 'gateway'),

)

urlpatterns += patterns('africas_talking.SendMessage',

    # url(r'^send_message/$', 'sendMessage'),
    # url(r'^smssync/$', 'sendMessage'),
    url(r'^exports/', include('data_exports.urls', namespace='data_exports')), # export data
)

urlpatterns += patterns('africas_talking.SMS',
    url(r'^sms/', 'sendMessage'),  # export data
)

urlpatterns += patterns(
    url(r'^exports/', include('data_exports.urls', namespace='data_exports')),
)

urlpatterns += patterns('kopo_kopo.views',

    url(r'^k2/$', 'k2_handler', name='kopo_kopo_callback'),

)

# SMSSync page
urlpatterns += patterns('kopo_kopo.views',
    url(r'^smssync/payment/$', SmssyncPaymentView.as_view(), name='kopokopo.smssync_payment'),

)

urlpatterns += patterns('mingi_user.views',

    url(r'^bulk/user/$', 'handle_bulk_user_edit'),
    url(r'^confirm/bulk/user/$', 'bulk_user_edit'),

)

urlpatterns += patterns('mingi_site.views',

    url(r'async/site/', 'site_details_handler'),
                        
)

urlpatterns += patterns('new_steama.views',

    # Overall dashboard
    url(r'^$', 'new_steama_page'),

    # Cards
    url(r'cards/permission_denied','permission_denied_card'),
    url(r'cards/customer_sms','customer_sms_card'),
    url(r'cards/bh_user_sms/user/(.+)/$','customer_sms_card_open'),
    url(r'cards/bh_user_sms/bh/(.+)/$','bh_sms_card_open'),
    url(r'cards/bh_controls','bh_controls_card'),
    url(r'cards/timer_commands','timer_commands_card'),
    url(r'cards/custom_trigger_commands','custom_trigger_commands_card'),
    url(r'cards/payments_bilevel/$','payments_bilevel_card'),
    url(r'cards/payments_bilevel/(.*)/$','payments_bilevel_card'),
    url(r'cards/summary_analysis','summary_analysis_card'),
    url(r'cards/comparison_graph','comparison_graph_card'),
    url(r'cards/energy_bilevel','energy_bilevel_card'),
    url(r'cards/pending_alerts','pending_alerts_card'),
    url(r'load/alerts','pending_alerts'),
    url(r'cards/user_email','email_users_card'),
    # url(r'cards/sites/$','sites_card'),
    url(r'cards/sites_utility_draw','sites_utility_draw_card'),
    url(r'cards/sites_voltage','sites_voltage_card'),
    url(r'cards/sites_temperature','sites_temperature_card'),
    url(r'cards/reports','reports_card'),
    url(r'cards/report_builder','report_builder_card'),
    url(r'cards/kpis','kpis_card'),
    url(r'cards/finance_report','finance_report_card'),
    url(r'cards/energy_report','energy_report_card'),
    url(r'cards/site_report','site_report_card'),
    url(r'cards/csv_report/','csv_report_card'),
    url(r'cards/transactions/csv','transactions_csv_card'),
    url(r'cards/messages/all','all_messages_card'),
    url(r'cards/messages/payments','payments_messages_card'),
    url(r'cards/messages/sms/all','all_sms_card'),
    url(r'cards/messages/sms/bh','bh_sms_card'),
    url(r'cards/messages/sms/customer','customer_sms_log_card'),
                        url(r'register_stripe','stripe_registration'),
    
    #bh cards
    url(r'cards/bh/bh_details/(.+)/$','newsteama_bh_details'),

    #sites cards
    url(r'cards/site/site_details/(.+)/$','newsteama_site_details'),
    url(r'cards/site/site_users/(.+)/$','newsteama_site_users'),
    url(r'cards/site/site_users_refresh/(.+)/$','newsteama_site_users_refresh'),
    url(r'cards/site/site_daily_totals/(.+)/$','newsteama_site_daily_totals'),
    url(r'cards/site/site_revenues_daily_totals/(.+)/$','newsteama_site_revenues_daily_totals'), 
    url(r'cards/site/site_energyuse_daily_totals/(.+)/$','newsteama_site_energyuse_daily_totals'),
    url(r'cards/user/site_energyuse_daily_totals/(.+)/$','newsteama_user_energyuse_daily_totals'),
    url(r'cards/site/site_comms_daily_totals/(.+)/$','newsteama_site_comms_daily_totals'),   
    url(r'cards/site/site_recent_activity/(.+)/$','newsteama_site_recent_activity'),
    url(r'cards/site/site_meter_reading/(.+)/$','newsteama_site_meter_reading'),
    url(r'cards/site/register_site','register_site_card'),
    url(r'cards/site/register_bh','register_bh_card'),

    #user cards
    url(r'cards/user/user_details/(.+)/$','newsteama_user_details'),
    url(r'cards/line/line_details/(.+)/$','newsteama_line_details'),
    url(r'cards/user/user_recent_activity/(.+)/(.+)/$','newsteama_user_recent_activity'),
    url(r'cards/user/register_user','register_user_card'),
    url(r'cards/user/register_website_user','register_website_user_card'),
    url(r'cards/register/faq_category','register_faq_category_card'),
    url(r'cards/register/faq','register_faq_card'),
    url(r'cards/register/multimedia','register_multimedia_card'),
    url(r'cards/register/gateway','register_gateway_card'),
    url(r'cards/user/search_user','search_user_card'),
    url(r'cards/user/edit_user/(.+)/$','edit_user_card'),
    url(r'cards/site/edit_site/(.+)/$','edit_site_card'),
    url(r'cards/bh/edit_bh/(.+)/$','edit_bh_card'),
    url(r'cards/line/edit_line/(.+)/$','edit_line_card'),
    url(r'cards/user/edit_website_user/(.+)/$','edit_website_user_card'),
    url(r'cards/edit_gateway/(.+)/$','edit_gateway_card'),
    url(r'cards/bulk_edit_users','bulk_edit_users_card'),
    url(r'cards/user/activate_website_user','activate_website_user_card'),
    url(r'user/activate','activate_website_user_action'),

    # Management commands
    url(r'cards/management_command','management_command_card'),
    url(r'trigger/management_command','management_command'),

    #FAQs
    url(r'cards/faqs/(.+)/$','faqs_card'),
    url(r'faqs/category/$','faqs_category'), 
    url(r'faqs/search/$','faqs_search'),
    url(r'faqs/multimedia/$','faqs_multimedia'),

    url(r'customer_sms/send','newsteama_send_customer_sms'),
    url(r'user_email/send','send_user_email'),
    url(r'bh_sms/send','newsteama_send_bh_sms'),  
    url(r'timer_commands/add','newsteama_add_timer_commands'),
    url(r'timer_commands/delete','newsteama_delete_timer_commands'),
    url(r'custom_trigger_commands/add','newsteama_add_custom_trigger_commands'),
    url(r'custom_trigger_commands/delete','newsteama_delete_custom_trigger_commands'),
    url(r'alerts/resolve','newsteama_resolve_alerts'),
    url(r'site_report/generate','newsteama_generate_site_report'),    
    url(r'finance_report/generate','newsteama_generate_finance_report'),
    url(r'energy_report/generate','newsteama_generate_energy_report'),
    url(r'kpi_report/generate','newsteama_generate_kpi_report'),               
    url(r'csv_report/generate','newsteama_generate_csv_report'),
    url(r'kpis/generate','newsteama_generate_kpis'),
    url(r'register/user','newsteama_register_user'),
    url(r'api/register_website_user/','newsteama_register_website_user_api'),
    url(r'register/website_user','newsteama_register_website_user'),
    url(r'register/faq_category','newsteama_register_faq_category'),
    url(r'register/faq','newsteama_register_faq'),
    url(r'register/multimedia','newsteama_register_multimedia'),
    url(r'register/gateway','newsteama_register_gateway'),
    url(r'register/site/$','newsteama_register_site'),
    url(r'register/bh/$','newsteama_register_bh'),
    url(r'search/user','newsteama_search_user'),
    url(r'edit/user/(.+)/$','newsteama_edit_user'),
    url(r'edit/line/(.+)/$','newsteama_edit_line'),
    url(r'edit/site/(.+)/$','newsteama_edit_site'),
    url(r'edit/bh/(.+)/$','newsteama_edit_bh'),
    url(r'edit/website_user/(.+)/$','newsteama_edit_website_user'),
    url(r'delete/user/(.+)/$','newsteama_delete_user'),
    url(r'load_activity/user','newsteama_load_user_recent_activity'),
    url(r'adjust_activity/user','newsteama_adjust_user_recent_activity'),
    url(r'load_activity/site','newsteama_load_site_recent_activity'),
    url(r'adjust_activity/site','newsteama_adjust_site_recent_activity'),
    url(r'adjust/sites_voltage','newsteama_adjust_sites_voltage'),
    url(r'adjust/sites_temperature','newsteama_adjust_sites_temperature'),
    url(r'adjust/sites_utility_draw','newsteama_adjust_sites_utility_draw'),
    url(r'adjust_meter_reading/site','adjust_meter_reading'),
    url(r'comparison_graph/data', 'get_comparison_graph_data'),
    url(r'site/lines/$','get_site_lines'),
    url(r'site/utilities','site_utilities'),
    url(r'user/utilities','user_utilities'),
    url(r'messages/all/list/$','newsteama_all_messages'),
    url(r'messages/payments/list/$','newsteama_payments_messages'),
    url(r'messages/all_sms/list/$','newsteama_all_sms_messages'),
    url(r'messages/bh_sms/list/$','newsteama_bh_sms_messages'),
    url(r'messages/customer_sms/list/$','newsteama_customer_sms_messages'),
    url(r'totals/revenues/$','newsteama_adjust_site_revenue_daily_totals'),
    url(r'load/totals/energyuse/$','newsteama_load_site_energyuse_daily_totals'),
    url(r'load/totals/energyuse/user/$','newsteama_load_user_energyuse_daily_totals'),
    url(r'totals/energyuse/$','newsteama_adjust_site_energyuse_daily_totals'),
    url(r'totals/energyuse/user/$','newsteama_adjust_user_energyuse_daily_totals'),
    url(r'totals/comms/$','newsteama_adjust_site_comms_daily_totals'),
    url(r'summary/financial/$','newsteama_summary_financial_chart'),
    url(r'summary/technical/$','newsteama_summary_technical_chart'),
    url(r'summary/comms/$','newsteama_summary_comms_chart'),
    url(r'email/support/$','newsteama_email_support'),
    url(r'^newsteama_logout/$', 'newsteama_logout'),
    url(r'^pdf/$', 'pdf'),
    url(r'^cards/change_timezone/$', 'change_timezone_card'),
    url(r'^save/timezone/$', 'save_timezone'),
    url(r'^save/timezone/choose/$', 'choose_timezone'),
    url(r'^sites/filter/$', 'filter_sites'),
)

urlpatterns += patterns('mingi_django.migration_in',

    # Uncomment to allow incoming migrations (this should be done for a short
    # time only, due to security concerns)
    # url(r'migrate/in/', 'receive_row'),
                        
)


urlpatterns += patterns('',
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include('smuggler.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^payments/payment/', SmssyncPaymentView.as_view(), name='kopokopo.smssync_payment'),
    url(r'^payments/payment/vulcan/', SmssyncPaymentView.as_view(), name='kopokopo.smssync_payment'),
    url(r'^utils/', include('utils.urls')),

)



# Reset password links
urlpatterns += patterns('',   
    url(r'', include('django.contrib.auth.urls')),
    url(r'^password/change/$',auth_views.password_change,name='auth_password_change'),
    url(r'^password/change/done/$',auth_views.password_change_done,name='auth_password_change_done'),
    url(r'^password/reset/$',auth_views.password_reset,name='forgot_password1'),
    url(r'^password/reset/done/$',auth_views.password_reset_done,name='forgot_password2'),
    url(r'^password/reset/complete/$',auth_views.password_reset_complete,name='forgot_password4'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',auth_views.password_reset_confirm,name='forgot_password3'),
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',auth_views.password_reset_confirm,name='forgot_password3'),
    
)
urlpatterns +=  static(MEDIA_URL, document_root=MEDIA_ROOT)

handler404="mingi_django.views.not_found_page"
handler500="mingi_django.views.server_error_page"
