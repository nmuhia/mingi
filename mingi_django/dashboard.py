import csv, json
import json as simplejson

import requests

import datetime
from datetime import date, timedelta, datetime
from django.utils import timezone
from django.utils.timezone import now,utc

from operator import itemgetter

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.timezone import now,utc

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Hourly_Site_Revenue, Daily_Power,Line_State,Line
from mingi_django.models import Error_Log, Utility
from mingi_user.models import Mingi_User
from mingi_user import *
from mingi_user.user_energy_usage import get_utility_cost,js_timestamp

from transactions import k2ToTable, getUserLink
from mingi_django.mingi_settings.custom import *
from django.db.models import Q

from mingi_django.timezones import zone_aware,gen_zone_aware

import sys
import pytz


ACTIVITY_GRAPH_NUM_DAYS = 7
NUM_RECENT_ALERTS = 5
MAX_CSV_ELEMENTS = 1000
MULTIPLE_CURRENCIES = False
TZSKES_EXCHANGE_RATE = 1.0
XOFKES_EXCHANGE_RATE = 1.0
USDKES_EXCHANGE_RATE = 1.0
DEFAULT_CURRENCY = "KSH"


def number_of_currencies(sites):
    currencies = []    
    number_of_currencies = 1    

    for site in sites:
        if site.currency not in currencies:
            currencies.append(site.currency)
            number_of_currencies = len(currencies)
            if number_of_currencies > 1:
                break                

    return number_of_currencies

def convert_tzs_kes():
     try:
         TZSKES_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22TZSKES%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
         return TZSKES_EXCHANGE_RATE
     except Exception as e:
         Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
         return 0.0

def convert_xof_kes():
     try:
         XOFKES_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22XOFKES%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
         return XOFKES_EXCHANGE_RATE
     except Exception as e:
         Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
         return 0.0


def convert_usd_kes():
     try:
         USDKES_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22USDKES%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
         return USDKES_EXCHANGE_RATE
     except Exception as e:
         Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
         return 0.0

def convert_kes_usd():
    try:
        KESUSD_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22KESUSD%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
        return KESUSD_EXCHANGE_RATE
    except Exception as e:
        Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
        return 0.0

def convert_tzs_usd():
    try:
        TZSUSD_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22TZSUSD%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
        return TZSUSD_EXCHANGE_RATE
    except Exception as e:
        Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
        return 0.0

def convert_xof_usd():
    try:
        XOFUSD_EXCHANGE_RATE = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22XOFUSD%22%29&format=json&env=store://datatables.org/alltableswithkeys").json()['query']['results']['rate']['Rate']
        return XOFUSD_EXCHANGE_RATE
    except Exception as e:
        Error_Log.objects.create(problem_type="Internal",problem_message="Except block entered: " + str(e))
        return 0.0    


def get_payment_chart(request,start,end):
    """
        Gets data for the payment bilevel chart
    """    
    graph_total = 0.0

    ret_dict = {
        'name':'payments',
        'children':[]
    }
    sites = get_visible_site_list(request.user)
    sites = sites.filter(is_active=True)

    no_of_currencies = number_of_currencies(sites)
    if no_of_currencies > 1:
        MULTIPLE_CURRENCIES = True

        TZSKES_EXCHANGE_RATE = convert_tzs_kes()
        USDKES_EXCHANGE_RATE = convert_usd_kes()
        XOFKES_EXCHANGE_RATE = convert_xof_kes()

    else:
        MULTIPLE_CURRENCIES = False


    for site in sites: 
        site_total = 0.0              
        site_ret_dict = {
            'name':[],
            'size':[],
            'children':[]
        }          
        users = Mingi_User.objects.filter(site=site)

        for user in users:
            user_total = 0.0
            user_ret_dict = {
            'name':[],
            'size':[],
            'url':[]
            } 
            #for utility management
            if request.user.website_user_profile.utility_management:
                user_total = get_utility_cost(user,start, end)
            else:
                user_total = user.revenue_range(start,end)    
            site_total += user_total
            if request.user.website_user_profile.utility_management:
                user_ret_dict['name'] = user.last_name
            else:
                user_ret_dict['name'] = user.first_name+" "+user.last_name
            user_ret_dict['user_url'] = "/elements/mingiuser/"+str(user.id)+"/"            
            user_ret_dict['user_id'] = user.id
            if MULTIPLE_CURRENCIES == True:
                if(site.currency == "TSH"):
                    user_ret_dict['size'] = "{0:.2f}".format(float(user_total) * float(TZSKES_EXCHANGE_RATE))

                elif(site.currency == "XOF"):
                    user_ret_dict['size'] = "{0:.2f}".format(float(user_total) * float(XOFKES_EXCHANGE_RATE))
                    
                elif(site.currency == "USD"):
                    user_ret_dict['size'] = "{0:.2f}".format(float(user_total) * float(USDKES_EXCHANGE_RATE))

                else:
                    user_ret_dict['size'] = "{0:.2f}".format(float(user_total))

                user_ret_dict['unit'] = DEFAULT_CURRENCY
            else:
                user_ret_dict['size'] = "{0:.2f}".format(float(user_total))
                user_ret_dict['unit'] = user.site.currency
            
            site_ret_dict['children'].append(user_ret_dict)        
        #get all users under a site and add them to the site dict            
        site_ret_dict['site_url'] = "/elements/site/"+str(site.id)+"/" 
        site_ret_dict['site_id'] = site.id
        site_ret_dict['name'] = site.name        
        if MULTIPLE_CURRENCIES == True:
            if(site.currency == "TSH"):
                site_ret_dict['size'] = "{0:.2f}".format(float(site_total) * float(TZSKES_EXCHANGE_RATE))

            elif(site.currency == "XOF"):
                site_ret_dict['size'] = "{0:.2f}".format(float(site_total) * float(XOFKES_EXCHANGE_RATE))

            elif(site.currency == "USD"):               
                site_ret_dict['size'] = "{0:.2f}".format(float(site_total) * float(USDKES_EXCHANGE_RATE))

            else:                
                site_ret_dict['size'] = "{0:.2f}".format(float(site_total))

            site_ret_dict['unit'] = DEFAULT_CURRENCY
        else:
            site_ret_dict['size'] = "{0:.2f}".format(float(site_total))
            site_ret_dict['unit'] = site.currency
        ret_dict['children'].append(site_ret_dict)

        graph_total += float(site_total)         
        
    ret_dict['graph_total'] = graph_total
    
    json_data = ret_dict        
    return HttpResponse(simplejson.dumps(json_data))
    
    
def get_energy_usage_chart(request,start,end,utility):
    """
    Gets data for the energy usage chart
    """ 
    graph_total = 0.0  
    ret_dict = {
        'name':'energy_usage',
        'children':[]
    }
        
    # Faulty data list
    faulty_data_list = list()

    sites = get_visible_site_list(request.user)

    try:
        this_utility = Utility.objects.get(name=str(utility))
        unit = this_utility.primary_unit
    except:
        this_utility = None
        unit = None

    for site in sites: 
        site_total = 0.0    
        site_ret_dict = {
            'name':[],
            'size':[],
            'children':[]
        }

        rollups = Daily_Power.objects.filter(site__isnull=True,user__site=site,utility=this_utility,timestamp__gte=start,timestamp__lte=end)
        line_rollups = Daily_Power.objects.filter(site__isnull=True,line_obj__site=site,utility=this_utility,timestamp__gte=start,timestamp__lte=end)
        print 'STARTING %s: %d' % (site.name, len(rollups))

        users = Mingi_User.objects.filter(site=site,line_number__isnull=False).order_by('id')
        lines = Line.objects.filter(site=site,number__isnull=False,is_connected=True).order_by('id')        

        previous_valid_energy=0 # Store previous valid energy


        # create sorted array of user and rollup data
        user_list = [[] for user in users]
        index = 0
        for user in users:            
            user_list[index].append(user)
            index += 1

        # create sorted array of lines and line rollups
        line_list = [[] for line in lines]
        index = 0
        for line in lines:            
            line_list[index].append(line)
            index += 1


        for rollup in rollups:
            print "ROLLUP: "+str(rollup)
            found = False
            index = 0

            for row in user_list:
                user = row[0]
                if rollup.user == user:
                    energy=float(rollup.amount)
                    if energy < ENERGY_MIN_FAULTY_LIMIT or energy > USER_ENERGY_MAX_FAULTY_LIMIT :
                        energy=previous_valid_energy
                        # Log correction action
                        faulty_error = "faulty data %s replaced with %s : on user : %s  on date %s <br/>" % (str(rollup.amount),str(energy),str(rollup.user),str(rollup.timestamp))
                        Error_Log.objects.create(problem_type="FAULTY_DATA",
                            problem_message="Bilevel partition energy use "+faulty_error)
                        # Add data to faulty data list
                        faulty_data_list.append(faulty_error)

                    user_list[index].append(energy)
                    found = True
                else:
                    energy = previous_valid_energy
                    
                index += 1

            if found == False:
                print "%s not found in user list %s" % (str(rollup.user), str(user_list))
        
        # Add line rollups to user rollups
        for rollup in line_rollups:
            print "ROLLUP: "+str(rollup)
            found = False
            index = 0

            for row in line_list:
                line = row[0]
                if rollup.line_obj == line:
                    energy=float(rollup.amount)
                    if energy < ENERGY_MIN_FAULTY_LIMIT or energy > USER_ENERGY_MAX_FAULTY_LIMIT :
                        energy=previous_valid_energy
                        # Log correction action
                        faulty_error = "faulty data %s replaced with %s : on line : %s  on date %s <br/>" % (str(rollup.amount),str(energy),str(rollup.line_obj),str(rollup.timestamp))
                        Error_Log.objects.create(problem_type="FAULTY_DATA",
                            problem_message="Bilevel partition energy use "+faulty_error)
                        # Add data to faulty data list
                        faulty_data_list.append(faulty_error)

                    line_list[index].append(energy)
                    found = True
                else:
                    energy = previous_valid_energy
                    
                index += 1

            if found == False:
                print "%s not found in user list %s" % (str(rollup.line_obj), str(line_list))

         
        for row in user_list:
            user = row[0]
            user_ret_dict = {
            'name':[],
            'size':[]
            }    
            
            user_total = sum_power_row(row)
            print "%s total: %s" % (str(user),str(user_total))

            site_total += user_total
            #for utility_management
            if request.user.website_user_profile.utility_management:            
                user_ret_dict['name'] = user.last_name            
            else:
                user_ret_dict['name'] = user.first_name+" "+user.last_name
                
            user_ret_dict['unit'] = unit

            user_ret_dict['size'] = "%.2f" % float(user_total)
            user_ret_dict['user_url'] = "/elements/mingiuser/"+str(user.id)+"/"
            user_ret_dict['user_id'] = user.id           
            site_ret_dict['children'].append(user_ret_dict)

        for row in line_list:
            line = row[0]
            line_ret_dict = {
            'name':[],
            'size':[]
            }    
            
            line_total = sum_power_row(row)

            site_total += line_total
            #for utility_management
            line_ret_dict['name'] = line.name
                
            line_ret_dict['unit'] = unit

            line_ret_dict['size'] = "%.2f" % float(line_total)
            line_ret_dict['user_url'] = "/elements/mingiuser/"+str(line.id)+"/"
            line_ret_dict['user_id'] = line.id           
            site_ret_dict['children'].append(line_ret_dict) 


                 
        #get all users under a site and add them to the site dict   
        site_ret_dict['site_url'] = "/elements/site/"+str(site.id)+"/"   
        site_ret_dict['site_id'] = site.id  
        site_ret_dict['name'] = site.name

        # Add errors at site level
        site_ret_dict['site_faulty_error'] = str(faulty_data_list) 

        site_ret_dict['unit'] = unit

        site_ret_dict['size'] = "%.2f" % float(site_total)
        ret_dict['children'].append(site_ret_dict)       

        graph_total += float(site_total)
    ret_dict['graph_total'] = graph_total;
        
    json_data = ret_dict
    return HttpResponse(simplejson.dumps(json_data))


def sum_power_row(row):
    """
    calculate power use total from row of rollup amounts
    """
    
    total = 0.0
    for i in range(1,len(row)):
        total += float(row[i])

    return total
        

def get_activity_graph_data(user):
    """
    Loads data for the activity graph on the dashboard.
    """

    ret_dict = {
        'activity_dates': [],
        'activity_messages': [],
        'activity_errors': []
    }

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        next_day = now() - timedelta(days=i)
        ret_dict['activity_dates'].append("{0.month}/{0.day}".format(next_day.date()))
        
        if (user.is_staff):
            ret_dict['activity_messages'].append(
                Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                    ).count() +
                Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                    ).count() +
                Kopo_Kopo_Message.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                    ).count()
                )
            ret_dict['activity_errors'].append(
                Error_Log.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                    ).count()
                )
        else:
            ret_dict['activity_messages'].append(
                Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                    ).count() +
                Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                    ).count() +
                Kopo_Kopo_Message.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                    ).count()
                )

    return ret_dict


def get_recent_alerts():
    """
    Loads data for the recent alerts table on the dashboard.
    """

    return {
        'recent_alerts': Error_Log.objects.filter(
                             is_resolved=False
                         )[:NUM_RECENT_ALERTS]
    }


def snapshotToTable(snapshot):
    """
    Converts the given Line State Snapshot into a dictionary suitable
    for display
    """

    row = {}
    row['timestamp'] = snapshot.timestamp
    row['site'] = snapshot.site.name
    row['number'] = str(snapshot.number)

    if snapshot.user:
        row['user'] = str(snapshot.user.id)
    else:
        row['user'] = str(snapshot.user)

    row['meter_reading'] = str(snapshot.meter_reading)
    row['account_balance'] = str(snapshot.account_balance)

    return row

def voltageToTable(voltage):
    """
    Converts the given Site Record into a dictionary suitable for display
    """
    
    data_vals = voltage.value.split(',')

    row = {}
    row['timestamp'] = voltage.timestamp
    row['voltage'] = data_vals[0]

    if len(data_vals) > 1:
        row['temp'] = data_vals[1]
    else:
        row['temp'] = None
    
    row['site'] = voltage.site.name

    return row


def userToTable(user):
    """
    Converts the given user into a dictionary suitable for display
    """

    row = {}

    row['id'] = str(user.id)
    row['created'] = user.created
    row['first_name'] = user.first_name
    row['last_name'] = user.last_name
    row['telephone'] = user.telephone
    row['site'] = user.site.name
    row['line_number'] = str(user.line_number)
    row['account_balance'] = str(user.account_balance)
    row['line_is_on'] = user.line_is_on
    row['energy_price'] = str(user.energy_price)
    row['low_balance_warning'] = str(user.low_balance_warning)

    if (user.site_manager is not None):
        row['site_manager'] = user.site_manager.first_name + ": " + user.site_manager.telephone
    else:
        row['site_manager'] = "NA"

    if (user.bit_harvester is not None):
        row['bit_harvester'] = user.bit_harvester.harvester_id + ": " + user.bit_harvester.telephone
    else:
        row['bit_harvester'] = "NA"
        
    row['language'] = user.language
    row['payment_plan'] = user.payment_plan
    
    return row


def rollupToTable(rollup):
    """
    Converts the given power or revenue rollup into a dictionary 
    suitable for display
    """

    row = {}
    row['timestamp'] = rollup.timestamp
    row['hourly_total'] = rollup.amount
    row['site'] = rollup.site.name
    try:
        row['utility'] = rollup.utility.name
    except:
        row['utility'] = "Electricity"

    return row

def rollupToTableRevenue(rollup):
    """
    Converts the given power or revenue rollup into a dictionary 
    suitable for display
    """

    row = {}
    row['timestamp'] = rollup.timestamp
    row['hourly_total'] = rollup.amount
    row['site'] = rollup.site.name

    return row


def dailyRollupToTable(rollup):
    """
    Converts the given daily power rollup into a dictionary 
    suitable for display
    """
    print rollup
    row = {}
    row['timestamp'] = rollup.timestamp
    
    # specify the user or site, depending on which is defined
    if rollup.site is None:
        if rollup.user is not None:
            site_name = rollup.user.site.name
            user_id = rollup.user.id
        else:
            site_name = rollup.line_obj.site.name
            user_id = rollup.line_obj.name
            
        row['user'] = user_id
        row['site'] = site_name
    else:
        row['user'] = ""
        row['site'] = rollup.site.name
        
    row['amount'] = rollup.amount
    if rollup.utility is None:
        row['quantity'] = "Electricity"
    else:
        row['quantity'] = rollup.utility.name

    return row


def filterDateAndSite(params, object_type, object_list):
    """
    Filters the given queries by the datetime information in params.
    """

    if params['min_date'] is not None:
        min_date = datetime.combine(params['min_date'],datetime.min.time())

        if (object_type == "Mingi_User"):
            object_list = object_list.filter(created__gte=min_date)
        else:
            object_list = object_list.filter(timestamp__gte=min_date)

    if params['max_date'] is not None:
        max_date = datetime.combine(params['max_date'],datetime.max.time())

        if (object_type == "Mingi_User"):
            object_list = object_list.filter(created__lte=max_date)
        else:
            object_list = object_list.filter(timestamp__lte=max_date)

    if params['site'] is not None:
        site_name = params['site']

        if (object_type == "Mingi_User"):
            object_list = object_list.filter(site__name=site_name)

        elif (object_type == "Kopo_Kopo_Input"):
            object_list = object_list.filter(user__site__name=site_name)

        elif (object_type == "AE_Kopo_Kopo"):
            ae_till_str = "u'business_number': [u'934135']"
            object_list = object_list.filter(user__site__name=site_name,
                                             raw_message__contains=ae_till_str)

        elif (object_type == "Line_State"):
            object_list = object_list.filter(site__name=site_name)

        elif (object_type == "Site_Record"):
            object_list = object_list.filter(site__name=site_name)

    # prevent time-out errors
    if len(object_list) > MAX_CSV_ELEMENTS:
        object_list = object_list[0:MAX_CSV_ELEMENTS]

    return object_list


def snapshots_csv_list(snapshots_list):
    """
    Creates an http response containing the given snapshot list as csv.
    """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="snapshots.csv"'
    writer = csv.writer(response)
    writer.writerow(snapshots_csv_header())
    for snapshot in snapshots_list:
        writer.writerow(snapshots_csv_row(snapshot))
    return response


def snapshots_csv_header():
    """
    The header row for outputting snapshots as csv.
    """
    return ['Timestamp', 'Site', 'Line Number', 'User',
            'Meter Reading', 'Account Balance']


def snapshots_csv_row(row):
    """
    A snapshot as a row for outputting as csv.
    """
    return [row['timestamp'], row['site'], row['number'], row['user'],
            row['meter_reading'], row['account_balance']]


def voltage_csv_list(voltage_list):
    """
    Creates an http response containing the given voltage list as csv
    """
    
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="voltages.csv"'
    writer = csv.writer(response)
    writer.writerow(voltage_csv_header())
    for voltage in voltage_list:
        writer.writerow(voltage_csv_row(voltage))

    return response


def voltage_csv_header():
    """
    the header row for outputting voltage logs as csv
    """

    return ['Timestamp', 'Site', 'Voltage (V)', 'Temperature (C)']


def voltage_csv_row(row):
    """
    """

    return [row['timestamp'], row['site'], row['voltage'], row['temp']]


def k2_csv_list(k2_list):
    """
    Creates an http response containing the given payments list as csv.
    """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="payments.csv"'
    writer = csv.writer(response)
    writer.writerow(k2_csv_header())
    for payment in k2_list:
        writer.writerow(k2_csv_row(payment))
    return response


def k2_csv_header():
    """
    The header row for outputting payment logs as csv.
    """
    return ['Timestamp', 'Site', 'User ID', 'User', 'Amount','Reference']


def k2_csv_row(row):
    """
    A transaction as a row for outputting as csv.
    """
    return [row['timestamp'], row['site'], row['id'], row['other_party'],
            row['digest'],row['reference']]

def daily_rollup_csv_header():
    """
    The header row for outputting payment logs as csv.
    """
    return ['Timestamp', 'Site', 'User / Line ID', 'Amount', 'Quantity Type']


def daily_rollup_csv_row(row):
    """
    A transaction as a row for outputting as csv.
    """
    return [row['timestamp'], row['site'], row['user'], row['amount'],
            row['quantity']]


def user_csv_list(user_list):
    """
    Creates an http response containing the given snapshot list as csv.
    """
    
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'
    writer = csv.writer(response)
    writer.writerow(user_csv_header())
    for user in user_list:
        writer.writerow(user_csv_row(user))
    return response


def user_csv_header():
    """
    The header row for outputting user database as csv
    """

    return ['ID','Registration Date','First Name','Last Name','Phone Number',
            'Site','Line Number','Account Balance','Line On?','Energy Price',
            'Low Balance Warning','Site Manager','bitHarvester','Language',
            'Payment Plan']


def user_csv_row(row):
    """
    A transaction as a row for outputting as csv.
    """
    return [row['id'], row['created'], row['first_name'], row['last_name'], 
            row['telephone'], row['site'], row['line_number'],
            row['account_balance'], row['line_is_on'], row['energy_price'],
            row['low_balance_warning'], row['site_manager'], 
            row['bit_harvester'], row['language'], row['payment_plan']]

""" # convert later if desired
def rollups_csv_list(rollups_list):
    #Creates an http response containing the given snapshot list as csv.

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="rollups.csv"'
    writer = csv.writer(response)
    writer.writerow(snapshots_csv_header())
    for snapshot in snapshots_list:
        writer.writerow(snapshots_csv_row(snapshot))
    return response
    """

def rollups_csv_header(rollup_type):
    """
    The header row for outputting rollups as csv.
    """

    if (rollup_type == "Revenue"):
        return ['Timestamp', 'Site', 'Hourly Total (KSh)']
    elif (rollup_type == "Power"):
        return ['Timestamp', 'Site', 'Hourly Total (kWh)', 'Utility Type']

    # unknown type, still write data
    else:
        return ['Timestamp', 'Site', '']


def rollups_csv_row(row):
    """
    A snapshot as a row for outputting as csv.
    """
    return [row['timestamp'], row['site'], row['hourly_total'], row['utility']] 

def revenue_rollups_csv_row(row):
    """
    A snapshot as a row for outputting as csv.
    """
    return [row['timestamp'], row['site'], row['hourly_total']]

def get_financial_chart_data(request):
    """
    Gets data for the dashboard financial chart.
    """

    sites = get_visible_site_list(request.user)
   

    # Structures to hold individual site data (in order to get top 3)
    site_totals = {}
    site_days = {}
    for site in sites:
        site_totals[site] = 0.0
        site_days[site] = []

    # Structure to return
    ret_dict = {
        'dates': [],
        'total': []
    }

    # Get beginning of today as a datetime
    today = gen_zone_aware(TIME_ZONE,now()).replace(hour=0, minute=0, second=0, microsecond=0)

    no_of_currencies = number_of_currencies(sites)
    if no_of_currencies > 1:        
        MULTIPLE_CURRENCIES = True   
        TZSKES_EXCHANGE_RATE = convert_tzs_kes()
        XOFKES_EXCHANGE_RATE = convert_xof_kes()
        USDKES_EXCHANGE_RATE = convert_usd_kes()

    else:
        MULTIPLE_CURRENCIES = False
    

    # For each day, get daily totals
    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        next_day = today - timedelta(days=i)
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        day_total = 0.0
        for site in sites:
            # consider the sitespecific timezone
            next_day = zone_aware(site,next_day)
            
            if MULTIPLE_CURRENCIES == True:                
                if(site.currency == "TSH"):
                    site_day_total = site.revenue_range(next_day, 
                                                next_day + timedelta(days=1))  * float(TZSKES_EXCHANGE_RATE)
                elif(site.currency == "USD"):
                    site_day_total = site.revenue_range(next_day, 
                                                next_day + timedelta(days=1))  * float(USDKES_EXCHANGE_RATE)
                else:
                    site_day_total = site.revenue_range(next_day, 
                                                next_day + timedelta(days=1))
                ret_dict['currency'] = "KSH"
            else:
                site_day_total = site.revenue_range(next_day, 
                                                next_day + timedelta(days=1))
                ret_dict['currency'] = site.currency
            
            site_totals[site] += site_day_total
            site_days[site].append(site_day_total)
            day_total += site_day_total
        ret_dict['total'].append(day_total)

    # Get and add top three sites
    sites_sorted = sorted(site_totals.items(), key=itemgetter(1))
    sites_sorted.reverse()
    for i in range(3):
        if i < len(sites_sorted):
            top_site = sites_sorted[i][0]
            ret_dict[str(i+1)] = (top_site.name, site_days[top_site])

    return ret_dict

def get_technical_chart_data(request,utility_type=None):
    """
    Gets data for the dashboard technical chart.
    """

    sites = get_visible_site_list(request.user)

    # Structures to hold individual site data (in order to get top 3)
    site_totals = {}
    site_days = {}
    for site in sites:
        site_totals[site] = 0.0
        site_days[site] = []

    # Structure to return
    ret_dict = {
        'dates': [],
        'total': []
    }

    if utility_type is not None:
        utility = Utility.objects.get(name=str(utility_type))
        ret_dict['chart_label'] = utility.get_totals_label(True)  
        ret_dict['chart_units'] = utility.get_totals_unit(True)

    # Get beginning of today as a datetime
    today = now().replace(hour=0, minute=0, second=0, microsecond=0)

    # For each day, get daily totals
    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS+1)):
        next_day = today - timedelta(days=i)

        # dont show today on the graph
        if next_day==today:
            continue

        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        day_total = 0.0
        for site in sites:
            # Deprecate calculation of energy, else get energy from daily rollups
            # site_day_total = site.energy_range(next_day, 
            #                                    next_day + timedelta(days=1),
            #                                    False)

            # Get energy from daily roll ups
            [site_day_total,FAULTY_ENERGY_STATUS] = site.get_site_energy_total(next_day,next_day + timedelta(days=1),utility_type)
           
            if FAULTY_ENERGY_STATUS:
                # Log correction action
                Error_Log.objects.create(problem_type="FAULTY_DATA",
                     problem_message="Faulty data on daily rollups corrected on site : %s date : %s" % (str(site),str(next_day)))

            site_totals[site] += site_day_total
            site_days[site].append(site_day_total)
            day_total += site_day_total
        ret_dict['total'].append(day_total)

    # Get and add top three sites
    sites_sorted = sorted(site_totals.items(), key=itemgetter(1))
    sites_sorted.reverse()
    for i in range(3):
        if i < len(sites_sorted):
            top_site = sites_sorted[i][0]
            ret_dict[str(i+1)] = (top_site.name, site_days[top_site])

    return ret_dict


def get_communication_chart_data(request):
    """
    Gets data for the dashboard communication chart.
    """

    user = request.user

    ret_dict = {
        'dates': [],
        'sent': [],
        'received': [],
        'payments': []
    }
    sent_count = 0
    received_count = 0
    payments_count = 0

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        next_day = date.today() - timedelta(days=i)
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        
        if (user.is_staff):
            sent = Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                ).count()
            received = Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                ).count()
            payments = Kopo_Kopo_Message.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1)
                ).count()

            ret_dict['sent'].append(sent)
            ret_dict['received'].append(received)
            ret_dict['payments'].append(payments)

            sent_count += sent
            received_count += received
            payments_count += payments
        else:
            sent = Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                ).count() + Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    bit_harvester__site__owners=user
                ).count()

            received = Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                ).count() + Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    bit_harvester__site__owners=user
                ).count()

            payments = Kopo_Kopo_Message.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site__owners=user
                ).count()

            ret_dict['sent'].append(sent)
            ret_dict['received'].append(received)
            ret_dict['payments'].append(payments)
            
            sent_count += sent
            received_count += received
            payments_count += payments

    ret_dict['count'] = sent_count + received_count + payments_count

    return ret_dict


def get_visible_site_list(user):
    """
    Gets the list of sites visible to the given user.
    """
    site_list = Mingi_Site.objects.filter(is_active=True)
    if not user.is_staff:
        site_list = site_list.filter(owners=user,is_active=True)
    return site_list

def get_site_users_info(request,site_id,selected_utility,graph_period):

    print "Starting ..."

    post = request.POST
    site = Mingi_Site.objects.filter(id=site_id)
    now = zone_aware(site,datetime.now())

    # Get dates
    if graph_period == 'month':
        end_date = now
        start_date = end_date - timedelta(days=30)
    elif graph_period == 'week':
        end_date = now
        start_date = end_date - timedelta(days=7)
    elif graph_period == 'dates':
        start_date = zone_aware(site,datetime.strptime(post['start_date'], "%Y-%m-%d"))
        end_date = zone_aware(site,datetime.strptime(post['end_date'], "%Y-%m-%d"))
    else:
        # Default
        end_date = now
        start_date = end_date - timedelta(days=3)

    # ignore this, dates are already timezone aware
    # start_date = timezone.make_aware(start_date,timezone.utc)
    # end_date = timezone.make_aware(end_date,timezone.utc)

    datalist = list()
    rect_dict = {}

    total_reading = 0.0
    user_reading = 0.0
    initial_reading = 0.0

    

    if selected_utility == 'none':
        all_users = Mingi_User.objects.filter(site=site)
        all_lines = Line.objects.filter(site=site)

    else:
        all_users = Mingi_User.objects.filter(site=site,first_name=selected_utility)
        all_lines = Line.objects.filter(site=site,utility_type__name=selected_utility)

    all_readings = Line_State.objects.filter(timestamp__gte=start_date, timestamp__lte=end_date)

    # exclude empty line numbers
    # try:
    #     all_users = all_users.exclude(line_number__exact='')
    # except Exception as e:
    #             Error_Log.objects.create(problem_type="INTERNAL",problem_message="Cumulative meter reading - all users"+str(e))

    for user in all_users:

        for line_num in user.line_number.split(','):
            # break if user has no line(s)
            if user.line_number is None or str(user.line_number)=="":
                break

            # Display users with line numbers, if user has more than one line
            line_name = ''
            if len(user.line_number.split(','))>1:
                line_name = ' line'+str(line_num)
            

            _reading = list()
            user_reading = 0.0
            initial_reading = 0.0
            i = 0 
            # ignore users with no lines 
            try:          
                for reading in all_readings.filter(user=user,number=line_num).order_by("timestamp"):
                    if i == 0:
                        initial_reading = reading.meter_reading
                    try:
                        _reading.append({'timestamp':js_timestamp(reading.timestamp),'reading':(str(reading.meter_reading - initial_reading))})
                    except:
                        # ignore line states with no meter readings
                        pass
                    i += 1 
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Cumulative meter reading "+str(e))
                pass
            
            datalist.append({'user':str(user.first_name)+" "+str(user.last_name)+str(line_name),'user_data':_reading})

    # Add independent lines to output
    for line in all_lines:
        # ignore lines with no lie number
        if line.number is None or str(line.number)=="":
            break

        line_name = ' line'+str(line.number)
            

        _reading = list()
        user_reading = 0.0
        initial_reading = 0.0
        i = 0 
        # ignore users with no lines 
        try:          
            for reading in all_readings.filter(site=line.site,number=line.number).order_by("timestamp"):
                if i == 0:
                    initial_reading = reading.meter_reading
                try:
                    _reading.append({'timestamp':js_timestamp(reading.timestamp),'reading':(str(reading.meter_reading - initial_reading))})
                except:
                    # ignore line states with no meter readings
                    pass
                i += 1 
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="Cumulative meter reading "+str(e))
            pass
        
        datalist.append({'user':str(line.name),'user_data':_reading})


    
    rect_dict['chart_data'] = datalist
    rect_dict['selected_utility'] = selected_utility
    rect_dict['site_id'] = str(site_id)
    rect_dict['start_date'] = str(start_date.date())
    rect_dict['end_date'] = str(end_date.date())
    rect_dict['all_users'] = str(all_users)
    
    # print rect_dict
    return rect_dict
