import json
import requests
from decimal import Decimal

from mingi_site.models import Line_State
from kopo_kopo.models import Kopo_Kopo_Message


def send_line_states(url, start_time=None, end_time=None):
    """
    Send all line states in range start_time to end_time. If start_time is
    later than end_time, will send states in order of decreasing timestamps.
    :param url: The url to which to send the line states.
    :param start_time: The timestamp at which to begin. (default: latest timestamp)
    :param end_time: The timestamp at which to end. (default: earliest timestamp)
    :return: None
    """

    # Get all line states
    line_states = Line_State.objects.all()

    # Order line states according to start_time + end_time
    desc_order = (not start_time) or (not end_time) or (start_time > end_time)
    if desc_order:
        line_states = line_states.order_by('-timestamp')
    else:
        line_states = line_states.order_by('-timestamp')

    # Filter line states according to start_time + end_time
    if start_time:
        if desc_order:
            line_states = line_states.filter(timestamp__lte=start_time)
        else:
            line_states = line_states.filter(timestamp__gte=start_time)
    if end_time:
        if desc_order:
            line_states = line_states.filter(timestamp__gte=end_time)
        else:
            line_states = line_states.filter(timestamp__lte=end_time)

    # Send line states
    for line_state in line_states:
        send_line_state(url, line_state)


def send_line_state(url, line_state):
    row_dict = line_state.__dict__
    row_dict['site_name'] = line_state.site.name
    if (row_dict['account_balance'] is not None):
        row_dict['account_balance'] = float(line_state.account_balance)
    if line_state.user:
        row_dict['user_telephone'] = line_state.user.telephone
        row_dict['user_site_name'] = line_state.user.site.name
    else:
        row_dict['user_telephone'] = None
        row_dict['user_site_name'] = None
    row_dict['timestamp'] = line_state.timestamp.isoformat()
    if line_state.processed_timestamp:
        row_dict['processed_timestamp'] = line_state.processed_timestamp.isoformat()
    else:
        row_dict['processed_timestamp'] = None
    del row_dict['_state']
    del row_dict['_site_cache']
    del row_dict['_user_cache']
    del row_dict['id']
    send_row(url, 'Line_State', row_dict)


def send_row(url, row_type, row_data):
    payload = {
        'type': row_type,
        'data': json.dumps(row_data)
    }
    
    requests.post(url, data=payload)


def send_payments(url, start_time=None, end_time=None):
    """
    Send all payments in range start_time to end_time. If start_time is
    later than end_time, will send states in order of decreasing timestamps.
    :param url: The url to which to send the line states.
    :param start_time: The timestamp at which to begin. (default: latest timestamp)
    :param end_time: The timestamp at which to end. (default: earliest timestamp)
    :return: None
    """

    # Get all line states
    payments = Kopo_Kopo_Message.objects.all()

    # Order payment
    payments = payments.order_by('-timestamp')
 
    # Filter line states according to start_time + end_time
    if start_time:
        payments = payments.filter(timestamp__gte=start_time)
    if end_time:
        payments = payments.filter(timestamp__lte=end_time)

    # Send line states
    for payment in payments:
        send_payment(url, payment)


def send_payment(url, payment):
    row_dict = payment.__dict__
    row_dict['raw_message'] = payment.raw_message
    row_dict['amount'] = float(payment.amount)
    row_dict['reference'] = payment.reference

    if payment.user:
        row_dict['user_telephone'] = payment.user.telephone
    else:
        row_dict['user_telephone'] = None
    row_dict['timestamp'] = payment.timestamp.isoformat()

    del row_dict['_state']
    del row_dict['_user_cache']
    del row_dict['id']
    send_row(url, 'Payment', row_dict)
