import json
import dateutil.parser

from mingi_site.models import Mingi_Site, Line_State
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from django.http import Http404, HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def receive_row(request):
    if (not 'type' in request.POST) or (not 'data' in request.POST):
        raise Http404

    row_type = request.POST['type']
    row_data = request.POST['data']

    if row_type == "Line_State":
        receive_line_state(row_data)
    elif row_type == "Payment":
        receive_payment(row_data)
    else:
        raise Http404

    return HttpResponse("200 OK")


def receive_line_state(data_str):
    data = json.loads(data_str)

    site = Mingi_Site.objects.filter(name=data['site_name'])[0]

    if data['user_telephone']:
        user = Mingi_User.objects.filter(
            telephone=data['user_telephone'],
            site=Mingi_Site.objects.filter(name=data['user_site_name'])[0]
        )[0]
    else:
        user = None

    timestamp = dateutil.parser.parse(data['timestamp'])

    if data['processed_timestamp']:
        processed_timestamp = dateutil.parser.parse(data['processed_timestamp'])
    else:
        processed_timestamp = None

    if not Line_State.objects.filter(site=site,
                                     number=data['number'],
                                     timestamp=timestamp,
                                     processed_timestamp=processed_timestamp,
                                     meter_reading=data['meter_reading'],
                                     account_balance=data['account_balance'],
                                     user=user):
        Line_State.objects.create(
            site=site,
            number=data['number'],
            timestamp=timestamp,
            processed_timestamp=processed_timestamp,
            meter_reading=data['meter_reading'],
            account_balance=data['account_balance'],
            user=user
        )


def receive_payment(data_str):
    data = json.loads(data_str)

    if data['user_telephone']:
        user = Mingi_User.objects.filter(telephone=data['user_telephone'])[0]
    else:
        user = None

    timestamp = dateutil.parser.parse(data['timestamp'])

    """ # don't check for duplicates because of null references
    if not Kopo_Kopo_Message.objects.filter(user=user,
                                     amount=data['amount'],
                                     reference=data['reference']):
                                     """
    Kopo_Kopo_Message.objects.create(
        user=user,
        amount=data['amount'],
        timestamp=timestamp,
        raw_message=data['raw_message'],
        reference=data['reference']
        )
    
