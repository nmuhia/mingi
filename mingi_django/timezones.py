from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site,Bit_Harvester,Line,Line_State
from mingi_django.models import Error_Log

from mingi_django.mingi_settings.custom import TIMEZONE_KEY
from mingi_django.mingi_settings.base import TIME_ZONE

from django.utils import timezone
from django.contrib.auth.decorators import login_required
import pytz
import requests
import time
import json
import json as simplejson
from collections import Counter
import arrow

@login_required
def has_multi_timezones(user):
	"""
	Check whether the logged in user has sites with multiple has_multi_timezones
	"""
	user_timezones = list()
	sites = Mingi_Site.objects.filter(is_active=True,owners=user)
	for site in sites:
		user_timezones.append(site.timezone)

	return user_timezones

@login_required
def get_user_mode_timezone(request):
	"""
	Get user timezone based on user sites
	"""
	user_timezones = list()
	sites = Mingi_Site.objects.filter(is_active=True,owners=request.user)
	epoch = int(time.mktime(timezone.now().timetuple()))
	for site in sites:
		q_url = "https://maps.googleapis.com/maps/api/timezone/json?location="+str(site.latitude)+","+str(site.longitude)+"&timestamp="+str(epoch)+"&key="+TIMEZONE_KEY
		q_timezone = requests.get(q_url).json()['timeZoneId']
		# print "site: "+str(site)+ " timezone: "+q_timezone
		# print "Timezone: "+str(q_timezone)
		user_timezones.append(q_timezone)

	# get most occurances
	q_counter = Counter(user_timezones).most_common(1)
	if len(q_counter) != 0:
		return q_counter[0][0]
	else:
		return TIME_ZONE

@login_required
def get_timezone(site):
	"""
	Query timezone from google
	"""
	# epoch = int(time.mktime(mydate.timetuple())*1000)
	# q_timezone = requests.get("https://maps.googleapis.com/maps/api/timezone/json?location="+site.latitude+","+site.longitude+"&timestamp="+epoch+"&key="+TIMEZONE_KEY).json()['timeZoneId']
	# print "Timezone: "+str(q_timezone)+"\nSite: "+str(site)
	print "loop"

def zone_aware(user,mydate):
	"""
	Returns timezone aware time based on user/site timezone
	"""
	user_timezone = TIME_ZONE
	try:
		if type(user) == Mingi_User:
			user_timezone = user.site.timezone
		elif type(user) == Bit_Harvester:
			user_timezone = user.site.timezone
		elif type(user) == Mingi_Site:
			user_timezone = user.timezone
		elif type(user) == Line or type(user) == Line_State:
			user_timezone = user.site.timezone
		else:
			user_timezone = TIME_ZONE

		if user_timezone is None:
			user_timezone = TIME_ZONE
	except Exception as e:
		Error_Log.objects.create(problem_type="TIMEZONE",
                                         problem_message="User: "+str(user)+", Date: "+str(mydate)+" Error: "+str(e))
		user_timezone = TIME_ZONE
	arrow_datetime = arrow.get(mydate)
	# return mydate.replace(tzinfo=pytz.timezone(user_timezone))
	return arrow_datetime.to(user_timezone).datetime

def gen_zone_aware(zone,mytime):
	arrow_datetime = arrow.get(mytime)
	return arrow_datetime.to(zone).datetime
def off_set(zone):
	try:
		return arrow.utcnow().to(zone).format('ZZ')
	except:
		return "-00:00"
