"""
Tests for mingi_django models
"""
import datetime
import mock
import pytz
import json

from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase, RequestFactory

from africas_talking.models import Gateway, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line, Site_Record
from .models import Error_Log, Utility, Custom_Timer_Command, Menu,\
    Website_User_Profile

from africas_talking.bhResponse import receiveSystemData
from kopo_kopo.handlePayment import handleStdPayment

from .mingi_settings.base import TIME_ZONE
from .timezones import gen_zone_aware
from new_steama.views import get_menu, get_sub_menu, pending_alerts
from .management.commands.get_pdf_report import monthly_report
from .management.commands.pdf_report_functions import site_statistics
from .transactions import newsteama_atOutToTable
from httplib import HTTPResponse

USER_TEST_PHONE = '+254726767443'
BH_TEST_PHONE = '+25400000001'
CREATE_USER_PHONE = '+254000000009'


class SteamaDBTestCase(TestCase):
    """
    A parent class for all test cases involving database operations. Creates a
    common set of users, sites, gateways, bit harvesters, mingi_users, and lines
    for testing.
    """

    def setUp(self):
        # API URLs
        self.customers_url = '/customers/'
        self.transactions_url = '/customers/%s/transactions/'
        self.sites_url = '/sites/'
        self.token_url = '/get-token/'

        # Create test auth users
        self.superuser_password = 'superpassword'
        self.superuser = User.objects.create_superuser(
            username='superusername',
            email='superuser@example.com',
            password=self.superuser_password,
        )
        self.superuser_website_profile = Website_User_Profile.objects.create(
            user=self.superuser, user_type="AAD", send_emails=True)

        self.user_password = 'password'
        self.user = User.objects.create_user(
            username='username',
            email='user@example.com',
            password=self.user_password
        )
        self.user_website_profile = Website_User_Profile.objects.create(
            user=self.user, user_type="CAD", send_emails=True)

        # Create test sites
        self.all_user_site = Mingi_Site.objects.create(
            name='All User Site',
            num_lines=3,
            date_online=datetime.date(2015, 1, 1),
            latitude=1.0,
            longitude=-2.0
        )
        self.all_user_site.owners.add(self.user)
        self.all_user_site_url = \
            self.sites_url + "%d/" % self.all_user_site.id
        self.superuser_site = Mingi_Site.objects.create(
            name='Superuser Site',
            num_lines=3,
            date_online=datetime.date(2015, 1, 1),
            latitude=0.0,
            longitude=10.0
        )
        self.superuser_site_url = \
            self.sites_url + "%d/" % self.superuser_site.id

        # Create Limbo site (used for pre-registered customers)
        self.limbo_site = Mingi_Site.objects.create(
            name='Limbo',
            num_lines=0,
            date_online=datetime.date(2015, 1, 1),
            latitude=0.0,
            longitude=0.0
        )

        # Create test gateway
        self.gateway = Gateway.objects.create(
            name="Mock Gateway",
            device_id="123"
        )

        # Create test bitHarvesters
        self.all_user_bh = Bit_Harvester.objects.create(
            harvester_id='All User Harvester',
            telephone='+100100100100',
            site=self.all_user_site,
            approval_date=datetime.date(2015, 1, 1),
            output_gateway=self.gateway,
            version='V4',
            serial_number='12345'
        )
        self.all_user_bh_2 = Bit_Harvester.objects.create(
            harvester_id='All User Harvester 2',
            telephone='+100100100101',
            site=self.all_user_site,
            approval_date=datetime.date(2015, 1, 1),
            output_gateway=self.gateway,
            version='V4'
        )
        self.superuser_bh = Bit_Harvester.objects.create(
            harvester_id='Superuser Harvester',
            telephone='+100100100102',
            site=self.superuser_site,
            approval_date=datetime.date(2015, 1, 1),
            output_gateway=self.gateway,
            version='V3',
            serial_number='67890'
        )

        # Create test customers
        self.all_user_customer = Mingi_User.objects.create(
            telephone='+100100100200',
            first_name='All User',
            last_name='Customer',
            site=self.all_user_site,
            bit_harvester=self.all_user_bh,
            is_user=True,
            is_site_manager=False,
            control_type='AUTOL',
            line_number='1',
            account_balance=100,
            energy_price=10
        )
        self.all_user_customer_url = \
            self.customers_url + "%d/" % self.all_user_customer.id
        self.superuser_customer = Mingi_User.objects.create(
            telephone='+100100100201',
            first_name='Superuser',
            last_name='Customer',
            site=self.superuser_site,
            bit_harvester=self.superuser_bh,
            is_user=True,
            is_site_manager=False,
            control_type='AUTOC',
            line_number='1,2',
            account_balance=1000,
            energy_price=10
        )
        self.superuser_customer_url = \
            self.customers_url + "%d/" % self.superuser_customer.id
        self.all_user_site_manager = Mingi_User.objects.create(
            telephone='+100100100202',
            first_name='All User',
            last_name='Customer',
            site=self.all_user_site,
            bit_harvester=self.all_user_bh,
            is_user=True,
            is_site_manager=True,
            control_type='AUTOL',
            line_number='3',
            account_balance=0,
            energy_price=10
        )

        # Create test utility
        self.utility = Utility.objects.create(
            id=1,
            name="Mock Utility"
        )

        # Create test lines
        self.all_user_site_line_1 = Line.objects.create(
            site=self.all_user_site,
            number=1,
            is_connected=True,
            user=self.all_user_customer,
            line_status='1',
            utility_type=self.utility
        )
        self.all_user_site_line_2 = Line.objects.create(
            site=self.all_user_site,
            number=2,
            is_connected=False,
            user=None,
            line_status='6',
            utility_type=self.utility
        )
        self.all_user_site_line_3 = Line.objects.create(
            site=self.all_user_site,
            number=3,
            is_connected=True,
            user=self.all_user_site_manager,
            line_status='4',
            utility_type=self.utility
        )
        self.superuser_site_line_1 = Line.objects.create(
            site=self.superuser_site,
            number=1,
            is_connected=True,
            user=self.superuser_customer,
            line_status='3',
            utility_type=self.utility
        )
        self.superuser_site_line_2 = Line.objects.create(
            site=self.superuser_site,
            number=2,
            is_connected=True,
            user=self.superuser_customer,
            line_status='3',
            utility_type=self.utility
        )
        self.superuser_site_line_3 = Line.objects.create(
            site=self.superuser_site,
            number=3,
            is_connected=False,
            user=None,
            line_status='6',
            utility_type=self.utility
        )

    def loginUser(self):
        """
        Logs in the client with session auth as the regular user.
        """
        self.client.login(username=self.user.username,
                          password=self.user_password)

    def loginSuperuser(self):
        """
        Logs in the client with session auth as the superuser.
        """
        self.client.login(username=self.superuser.username,
                          password=self.superuser_password)


class Custom_Triggers_TestCase(TestCase):
    """
    tests the custom triggers
    """

    START_DATE = datetime.datetime(2000, 1, 1, tzinfo=pytz.timezone(TIME_ZONE))

    def setUp(self):
        testSite = Mingi_Site.objects.create(name='Testing World', num_lines=3,
                                             equipment='stuff',
                                             date_online=gen_zone_aware(
                                                 TIME_ZONE, datetime.datetime.now()),
                                             latitude=0.00, longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(telephone=BH_TEST_PHONE,
                                                     harvester_id='TEST_ID0001', site=testSite,
                                                     serial_number='', bh_type='AT', version="V4",
                                                     approval_date=gen_zone_aware(TIME_ZONE, datetime.datetime.now()))
        user = Mingi_User.objects.create(telephone=USER_TEST_PHONE,
                                         first_name='Emily',
                                         control_type="AUTOL",
                                         last_name='Moder', site=testSite,
                                         line_number="1", energy_price=100.00,
                                         line_status='1', bit_harvester=bit_harvester)
        utility = Utility.objects.create(name="Electricity:")
        for i in range(1, 4):
            if i == 1:
                this_user = user
            else:
                this_user = None
            line = Line.objects.create(site=testSite,
                                       number=i,
                                       user=this_user,
                                       line_status='3',
                                       is_connected=True,
                                       utility_type=utility)

        user.site_manager = user
        user.save()

    def test_temperature_trigger_no_trigger_DATA_msg(self):
        """
        Test a temperature trigger which is not activated when a DATA
        message comes in, because the threshold is not passed.
        """

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", site=BH.site,
                                                      trigger_type='temp', trigger_level="greater_than", message_type="sms", threshold=40.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,39.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 0)

    def test_temperature_trigger_triggered_DATA_msg_correct_repeats(self):
        """
        Test a temperature trigger (greater-than) which is activated by a
        DATA message, but is not repeated when a second data message is also 
        beyond the trigger's threshold.
        """

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED",
                                                      site=BH.site, trigger_type='temp', trigger_level="greater_than", message_type="sms", threshold=40.0)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,40.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

        # run again and verify that the trigger does not repeat
        update_ts = self.START_DATE + datetime.timedelta(days=2)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,40.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

        # run a third time and verify that trigger is un-triggered by low value
        update_ts = self.START_DATE + datetime.timedelta(days=3)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,39.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

    def test_voltage_trigger_no_trigger_DATA_msg(self):
        """
        Test a voltage trigger which is not activated when a DATA
        message comes in, because the threshold is not passed.
        """

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", site=BH.site,
                                                      trigger_type='voltage', trigger_level="less_than", message_type="sms", threshold=24.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,24.5,39.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 0)

    def test_voltage_trigger_triggered_DATA_msg_correct_repeats(self):
        """
        Test a voltage trigger (less-than) which is activated by a DATA message,
        but is not repeated when a second data message is also beyond
        the trigger's threshold.
        """

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED",
                                                      site=BH.site, trigger_type='voltage', trigger_level="less_than", message_type="sms", threshold=24.0)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,40.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)
        for log in Error_Log.objects.all():
            print log.problem_message + " (" + log.problem_type + ")"

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

        # run again and verify that the trigger does not repeat
        update_ts = self.START_DATE + datetime.timedelta(days=2)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,23.5,40.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

        # run a third time and verify that trigger is un-triggered by low value
        update_ts = self.START_DATE + datetime.timedelta(days=3)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,DATA,,24.5,39.5,," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(site=BH.site)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

    def test_account_balance_trigger_no_trigger_CREDIT_msg(self):
        """
        Test an account balance trigger (less-than) which is not activated
        by an incoming CREDIT message
        """

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100.0
        user.save()
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", user=user,
                                                      trigger_type='account_balance', trigger_level="less_than", message_type="sms", threshold=50.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,CREDIT,1,600,100,100," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)

        trigger = Custom_Timer_Command.objects.get(user=user)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 0)

    def test_account_balance_trigger_triggered_CREDIT_msg_no_repeat(self):
        """
        Test an account balance trigger (less-than) which is activated
        by an incoming CREDIT message. Another CREDIT process does not
        trigger a repeated message.
        """

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100.0
        user.save()
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", user=user,
                                                      trigger_type='account_balance', trigger_level="less_than", message_type="sms", threshold=50.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,CREDIT,1,40,100,100," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)
        for log in Error_Log.objects.all():
            print log.problem_message + " (" + log.problem_type + ")"

        trigger = Custom_Timer_Command.objects.get(user=user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        print str(user.account_balance)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

        # run again and verify that the trigger does not repeat
        update_ts = self.START_DATE + datetime.timedelta(days=2)
        msg_ts = update_ts.strftime("%s")

        bh_msg = "*0,%s,1,CREDIT,1,30,100,100," % (msg_ts)
        bh_msg_elements = bh_msg.split(',')

        receiveSystemData(bh_msg_elements, BH, update_ts, update_ts)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        print str(user.account_balance)

        trigger = Custom_Timer_Command.objects.get(user=user)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.all()), 1)

    def test_account_balance_trigger_no_trigger_payment(self):
        """
        Test an account balance trigger (greater-than) which is not activated
        by an incoming payment
        """

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100.0
        user.save()
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", user=user,
                                                      trigger_type='account_balance', trigger_level="greater_than", message_type="sms", threshold=150.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)

        handleStdPayment(user, 40, update_ts)

        trigger = Custom_Timer_Command.objects.get(user=user)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.filter(
            raw_message__contains="TRIGGERED")), 0)

    def test_account_balance_trigger_triggered_payment_with_reset(self):
        """
        Test an account balance trigger (greater-than) which is activated
        by an incoming payment
        """

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100.0
        user.save()
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        trigger = Custom_Timer_Command.objects.create(destination=USER_TEST_PHONE, message="TRIGGERED", user=user,
                                                      trigger_type='account_balance', trigger_level="greater_than", message_type="sms", threshold=150.0, is_triggered=False)
        update_ts = self.START_DATE + datetime.timedelta(days=1)

        handleStdPayment(user, 60, update_ts)

        trigger = Custom_Timer_Command.objects.get(user=user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        self.assertTrue(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.filter(
            raw_message__contains="TRIGGERED")), 1)

        # run again and verify that the trigger does not repeat
        update_ts = self.START_DATE + datetime.timedelta(days=2)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 0.0
        user.save()

        handleStdPayment(user, 140, update_ts)

        trigger = Custom_Timer_Command.objects.get(user=user)

        self.assertFalse(trigger.is_triggered)
        self.assertEqual(len(Africas_Talking_Output.objects.filter(
            raw_message__contains="TRIGGERED")), 1)

    def test_utility_use_trigger_no_trigger(self):
        """
        """

        pass

    def test_utility_use_trigger_triggered(self):
        """
        """

        pass


class MonthlyPDFReportTests(SteamaDBTestCase):
    """
    Tests the PDF report generation functions
    """
    today = gen_zone_aware(TIME_ZONE, datetime.datetime.today())
    first = today.replace(year=2015, month=10, day=1,
                          hour=0, minute=0, second=0, microsecond=0)
    end = first - datetime.timedelta(days=1)
    start = end.replace(day=1)

    def setUp(self):
        super(MonthlyPDFReportTests, self).setUp()

        Kopo_Kopo_Message.objects.create(
            user=self.all_user_customer,
            raw_message="Payment of 500",
            amount=500,
            timestamp=datetime.datetime(2016, 1, 2, 1, tzinfo=pytz.UTC),
            reference="XXX")

        Site_Record.objects.create(
            site=self.all_user_site,
            key="BH Data Log",
            value="20.5,25.7,3,48",
            timestamp=datetime.datetime(2016, 1, 2, 2, tzinfo=pytz.UTC))

    def test_site_statistics(self):
        site_statistics(self.all_user_site, self.start, self.end)

    @mock.patch("mingi_django.management.commands.get_pdf_report"
                ".send_pdf")
    @mock.patch("mingi_django.management.commands.get_pdf_report"
                ".render_to_response")
    @mock.patch("mingi_django.management.commands.get_pdf_report"
                ".monthly_report")
    def test_single_user(self,
                         mock_monthly_report,
                         mock_render_to_response,
                         mock_send_pdf):
        """
        Tests creating PDF reports with a single username specified.
        """
        mock_monthly_report_return = mock_monthly_report.return_value
        mock_render_return = mock_render_to_response.return_value

        call_command("get_pdf_report", self.user.username)

        mock_render_to_response.assert_called_once_with(
            'monthly_pdf_template.html',
            mock_monthly_report_return)

        self.assertEqual(len(mock_send_pdf.call_args_list), 1)
        send_pdf_call = mock_send_pdf.call_args_list[0][0]
        self.assertEqual(send_pdf_call[1], mock_render_return)

    def test_monthly_report_one_site(self):
        """
        Tests the montly_report function with a single site specified.

        Just test that it successfully runs for now.
        """
        monthly_report(
            Mingi_Site.objects.filter(owners=self.user),
            "January",
            "2016",
            datetime.datetime(2016, 1, 1, tzinfo=pytz.UTC),
            datetime.datetime(2016, 2, 1, tzinfo=pytz.UTC))

    def test_monthly_report_no_data(self):
        """
        Tests the monthly_report function for a time peroid with no data.

        Just test that it successfully runs for now.
        """
        monthly_report(
            Mingi_Site.objects.filter(owners=self.user),
            "January",
            "2020",
            datetime.datetime(2020, 1, 1, tzinfo=pytz.UTC),
            datetime.datetime(2020, 1, 31, tzinfo=pytz.UTC))


class MessageTransactions(TestCase):
    """
    A suite of tests concerning display of Africas_Talking_Output messages.

    This ensures that the text_message field is used if present, and the
    raw_message field as a fallback.
    """
    def setUp(self):
        self.atOut_1 = Africas_Talking_Output.objects.create(
            raw_message="Raw Message 1",
            text_message="Text Message 1",
            timestamp=gen_zone_aware(TIME_ZONE, datetime.datetime.now()))
        self.atOut_2 = Africas_Talking_Output.objects.create(
            raw_message="Raw Message 2",
            text_message=None,
            timestamp=gen_zone_aware(TIME_ZONE, datetime.datetime.now()))
        self.atOut_3 = Africas_Talking_Output.objects.create(
            raw_message="Raw Message 3",
            text_message="",
            timestamp=gen_zone_aware(TIME_ZONE, datetime.datetime.now()))

    def test_text_over_raw_message_preference(self):
        row = newsteama_atOutToTable(self.atOut_1)
        self.assertEqual(row['digest'], self.atOut_1.text_message)

    def test_none_text_message_fallback(self):
        row = newsteama_atOutToTable(self.atOut_2)
        self.assertEqual(row['digest'], self.atOut_2.raw_message)

    def test_empty_text_message_fallback(self):
        row = newsteama_atOutToTable(self.atOut_3)
        self.assertEqual(row['digest'], self.atOut_3.raw_message)


class MingiTestCase(SteamaDBTestCase):
    """
    Tests for the old Steama (Mingi) platform.
    Tests removed functions and urls
    """

    def test_removed_old_user_urls(self):
        self.loginUser()

        logs = self.client.get('/logs/')
        alerts = self.client.get('/alerts/')
        sites = self.client.get('/sites/')
        reports = self.client.get('/reports/')
        report_finance = self.client.get('/reports/finance_report/')
        dates_payment_graph = self.client.get(
            '/payment_graph/dates/2016-06-01/2016-06-02/')
        month_payment_graph = self.client.get('/payment_graph/month/')
        today_payment_graph = self.client.get('/payment_graph/today/')
        payment_graph = self.client.get('/payment_graph/')
        dates_energy_use = self.client.get(
            '/energy_usage_graph/dates/2016-06-01/2016-06-02/')
        month_energy_use = self.client.get('/energy_usage_graph/month/')
        today_energy_use = self.client.get('/energy_usage_graph/today/')
        energy_use = self.client.get('/energy_usage_graph/')
        element_site = self.client.get('/elements/site/')
        element_site_1 = self.client.get('/elements/site/1/')
        element_bh = self.client.get('/elements/bitharvester/')
        element_bh_1 = self.client.get('/elements/bitharvester/1/')
        element_mingi_user = self.client.get('/elements/mingiuser/')
        element_mingi_user_1 = self.client.get('/elements/mingiuser/1/')
        comms_center = self.client.get('/comms_center/')
        send_sms = self.client.get('/send_bh_sms/')
        dashboard = self.client.get('/dashboard/')

        self.assertEqual(logs.status_code, 404)
        self.assertEqual(logs.content, "404 Not Found")

        self.assertEqual(alerts.status_code, 404)
        self.assertEqual(alerts.content, "404 Not Found")

        self.assertEqual(sites.status_code, 404)
        self.assertEqual(sites.content, "404 Not Found")

        self.assertEqual(reports.status_code, 404)
        self.assertEqual(reports.content, "404 Not Found")

        self.assertEqual(report_finance.status_code, 404)
        self.assertEqual(report_finance.content, "404 Not Found")

        self.assertEqual(dates_payment_graph.status_code, 404)
        self.assertEqual(dates_payment_graph.content, "404 Not Found")

        self.assertEqual(month_payment_graph.status_code, 404)
        self.assertEqual(month_payment_graph.content, "404 Not Found")

        self.assertEqual(today_payment_graph.status_code, 404)
        self.assertEqual(today_payment_graph.content, "404 Not Found")

        self.assertEqual(payment_graph.status_code, 404)
        self.assertEqual(payment_graph.content, "404 Not Found")

        self.assertEqual(dates_energy_use.status_code, 404)
        self.assertEqual(dates_energy_use.content, "404 Not Found")

        self.assertEqual(month_energy_use.status_code, 404)
        self.assertEqual(month_energy_use.content, "404 Not Found")

        self.assertEqual(today_energy_use.status_code, 404)
        self.assertEqual(today_energy_use.content, "404 Not Found")

        self.assertEqual(energy_use.status_code, 404)
        self.assertEqual(energy_use.content, "404 Not Found")

        self.assertEqual(element_site.status_code, 404)
        self.assertEqual(element_site.content, "404 Not Found")

        self.assertEqual(element_site_1.status_code, 404)
        self.assertEqual(element_site_1.content, "404 Not Found")

        self.assertEqual(element_bh.status_code, 404)
        self.assertEqual(element_bh.content, "404 Not Found")

        self.assertEqual(element_bh_1.status_code, 404)
        self.assertEqual(element_bh_1.content, "404 Not Found")

        self.assertEqual(element_mingi_user.status_code, 404)
        self.assertEqual(element_mingi_user.content, "404 Not Found")

        self.assertEqual(element_mingi_user_1.status_code, 404)
        self.assertEqual(element_mingi_user_1.content, "404 Not Found")

        self.assertEqual(comms_center.status_code, 404)
        self.assertEqual(comms_center.content, "404 Not Found")

        self.assertEqual(send_sms.status_code, 404)
        self.assertEqual(send_sms.content, "404 Not Found")
        
        self.assertEqual(dashboard.status_code, 404)
        self.assertEqual(dashboard.content, "404 Not Found")


class MenuTestCase(SteamaDBTestCase):
    """
    Tests creation of dynamic Menus
    """

    def setUp(self):
        super(MenuTestCase, self).setUp()

        self.factory = RequestFactory()

    def test_dynamic_menu_creation_superuser(self):        

        superuser = User.objects.get(username="superusername")

        request = self.factory.get('/')
        request.user = superuser

        test_menu = Menu.objects.get(menu_name="Edit")

        # menu objects query is successful
        get_menu(request, test_menu)

        # superuser menus get generated successfully
        sub_menus = get_sub_menu(request,test_menu)
        self.assertEqual(len(sub_menus),6)

        parent_menus = get_sub_menu(request,None)
        self.assertEqual(len(parent_menus),8)

    def test_dynamic_menu_creation_normal_user(self):

        normaluser = User.objects.get(username="username")

        normaluser_request = self.factory.get('/')
        normaluser_request.user = normaluser

        test_menu = Menu.objects.get(menu_name="Edit")

        # menu objects query is successful
        get_menu(normaluser_request,test_menu)

        # normaluser menus get generated successfully
        sub_menus = get_sub_menu(normaluser_request,test_menu)
        self.assertEqual(len(sub_menus),4)

        parent_menus = get_sub_menu(normaluser_request,None)
        self.assertEqual(len(parent_menus),8)


class AlertsTestCase(SteamaDBTestCase):
    """
    Tests for the old Steama (Mingi) platform.
    Tests removed functions and urls
    """

    def setUp(self):
        super(AlertsTestCase, self).setUp()

        self.factory = RequestFactory()

        # create sample alerts
        problem_type = "INTERNAL"
        for i in range(200):
            if i % 7 == 0:
                problem_type = "DEBUGGING"
            elif i % 10 == 0:
                problem_type = "LOG_DATA"
            else:
                problem_type = "INTERNAL"

            Error_Log.objects.create(problem_message="This is sample error "+str(i),problem_type=problem_type)

    def test_alerts_generation(self):
        """
        Tests that alerts are generated successfully
        """
        superuser = User.objects.get(username="superusername")

        request = self.factory.get('/')
        request.user = superuser

        alerts_result = pending_alerts(request)

        json_alerts = json.loads(alerts_result.content)

        # print 
        json_alerts_list = json.loads(json_alerts['alerts_list'])

        self.assertEqual(len(json_alerts_list),100)
        self.assertEqual(json_alerts_list[1]['fields']['problem_type'],'INTERNAL')
        self.assertEqual(json_alerts_list[3]['fields']['problem_type'],'DEBUGGING')
        self.assertEqual(json_alerts_list[9]['fields']['problem_type'],'LOG_DATA')

