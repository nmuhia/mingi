from mingi_django.mingi_settings.custom import *
DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

import sys
DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.mysql',
        # DB name, or path to database file if using sqlite3.
        'NAME': 'mingidb',
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': 'orbi',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

CELERY_ALWAYS_EAGER = False

AFRICAS_TALKING_INPUT_VAULT_NAME = 'wes_africas_talking_input'
AFRICAS_TALKING_OUTPUT_VAULT_NAME = 'wes_africas_talking_output'
LINE_STATE_VAULT_NAME = 'wes_line_state'