from mingi_django.mingi_settings.custom import *

# Parse database configuration from $DATABASE_URL
import dj_database_url
sqlite_database_url = 'sqlite:////' + HOME_PATH + 'database/testdb'
DATABASES['default'] = dj_database_url.config(default=sqlite_database_url)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

AT_USERNAME = ""
AT_API_KEY = ""
AT_FROM = ''
