from mingi_django.mingi_settings.custom import *
DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

import sys
if 'test' in sys.argv:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ':memory:',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'database',
        }
    }

CELERY_ALWAYS_EAGER = False

AFRICAS_TALKING_INPUT_VAULT_NAME = 'wes_africas_talking_input'
AFRICAS_TALKING_OUTPUT_VAULT_NAME = 'wes_africas_talking_output'
LINE_STATE_VAULT_NAME = 'wes_line_state'
