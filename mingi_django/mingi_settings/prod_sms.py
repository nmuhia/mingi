from mingi_django.mingi_settings.prod import *

ROOT_URLCONF = 'mingi_django.urls_sms'

#==============================
# Celery task
#==============================
"""
# MOVED TO HTML SERVER
from celery.schedules import crontab
CELERYBEAT_SCHEDULE = {

    "diagnostic": {
        "task": "mingi_django.tasks.diagnostics",
        # Daily at noon
        "schedule": crontab(hour=12, minute=0),
        "args": (),
    },
    "timer_commands": {
        "task": "mingi_django.tasks.timer_commands",
        # Every hour
        "schedule": crontab(hour='*', minute=0),
        "args": (),
    },
    "daily_customer_sms": {
        "task": "mingi_django.tasks.daily_customer_sms",
        # Everyday at 8am
        "schedule": crontab(hour=8, minute=15),
        "args": (),
    },
    "revenue_rollups": {
        "task": "mingi_django.tasks.revenue_rollups",
        # Hourly
        "schedule": crontab(hour='*', minute=20),
        "args": (),
    },
    "power_rollups": {
        "task": "mingi_django.tasks.power_rollups",
        # Hourly
        "schedule": crontab(hour='*', minute=30),
        "args": (),
    },

}
"""
