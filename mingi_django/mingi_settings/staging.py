from mingi_django.mingi_settings.custom import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mingidb',
        'USER': 'aemaster',
        'PASSWORD': 'lights3000',
        'HOST': 'staging.cz5cjzgrpy0q.us-east-1.rds.amazonaws.com',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',  # Set to empty string for default.
    }
}

#=====================================
# SMS settings                       #
#=====================================

AT_USERNAME = ""
AT_API_KEY = ""
AT_FROM = ""

#=====================================
# Email settings                     #
#=====================================

EMAIL_BACKEND = 'bandit.backends.smtp.HijackSMTPBackend'
BANDIT_EMAIL = 'emily@steama.co'
