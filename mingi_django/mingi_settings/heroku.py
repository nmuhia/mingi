import os

if ("DEPLOYMENT" in os.environ) and (os.environ.get("DEPLOYMENT") == "PRODUCTION"):
    from mingi_django.mingi_settings.prod import *

    # Parse database configuration from $DATABASE_URL
    import dj_database_url
    sqlite_database_url = 'sqlite:////' + HOME_PATH + 'database/testdb'
    DATABASES['default'] = dj_database_url.config(default=sqlite_database_url)

    # Honor the 'X-Forwarded-Proto' header for request.is_secure()
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

    AT_USERNAME = "numbermonkey"
    AT_API_KEY = "c79c1bf5e4fd19a44d4e6b5d03549f76fe358e0d6bb778a20e22e072c6c62417"
    AT_FROM = '20880'

else:
    from mingi_django.mingi_settings.heroku_dev.py
