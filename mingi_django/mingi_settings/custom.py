from mingi_django.mingi_settings.base import *
from datetime import datetime, timedelta #, tzinfo
import sys


LOGIN_URL = "/login/"

# Allow all host headers
ALLOWED_HOSTS = ['.steama.co','.accessenergy.org','emoder-mingi.herokuapp.com',
                 '54.209.193.227','54.174.222.41','127.0.0.1','192.168.1.52',
                 'ec2-52-4-102-139.compute-1.amazonaws.com','ec2-54-174-222-41.compute-1.amazonaws.com','ec2-54-209-193-227.compute-1.amazonaws.com']


MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

MAX_TRANSACTIONS = 100
UNIX_HOURS_OFFSET = 14
V4_UNIX_HOURS_OFFSET = 13


PRODUCTION_DEPLOYMENT_NAME = "PRODUCTION"

if ('DEPLOYMENT' in os.environ) and (os.environ['DEPLOYMENT'] == PRODUCTION_DEPLOYMENT_NAME):
    MINGI_MODE = "PRODUCTION"
else:
    MINGI_MODE = "DEBUG"

#GOOGLE_AT_URL = "https://script.google.com/macros/s/AKfycbzIYz9XE2_6NYWI5kM__HWBm5AHtYCdB_-bc3ybNjEFSicp1muG/exec"
#GOOGLE_K2_URL = "https://script.google.com/macros/s/AKfycbwCNozIZAFpOBplsNhr_co9g8GnKwgZacBTWnobvBzH0L1YIFA/exec"

FORWARD_URLS_SMS = [
    "http://suntransfer.herokuapp.com/at/"
    ]

FORWARD_URLS_K2 = []

#======================
# Email settigngs
#======================

# Uncomment to use GOOGLE SEND EMAIL
# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_HOST_USER = 'app@steama.co'
# EMAIL_HOST_PASSWORD = 'sXZlU9YHo0t6'
# EMAIL_PORT = 587

# USE AWS SES EMAIL SENDING
SERVER_EMAIL = 'app@steama.co'
DEFAULT_FROM_EMAIL = 'app@steama.co'

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.us-east-1.amazonaws.com' 
EMAIL_HOST_USER = 'AKIAIM7H6DFOJTOBNBCQ'
EMAIL_HOST_PASSWORD = 'At9r+PAtoQ7sILUPy90kUZ5F8BHfR3K7xLOiTzfxgVAQ'
EMAIL_PORT = 587

#=========================
# Djcelery
#=========================
import djcelery
djcelery.setup_loader()
# The backend used to store task results - because we're going to be
# using RabbitMQ as a broker, this sends results back as AMQP messages
CELERY_RESULT_BACKEND = "amqp"
CELERY_ALWAYS_EAGER = False
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"

#==============================
# Rabbitmq
#==============================
BROKER_URL = 'amqp://guest:guest@localhost:5672/'


#==============================
# Celery task
#==============================
from celery.schedules import crontab
CELERYBEAT_SCHEDULE = {

    "diagnostic": {
        "task": "mingi_django.tasks.diagnostics",
        # Daily at 9am UTC
        "schedule": crontab(hour=9, minute=0),
        "args": (),
    },
    "timer_commands": {
        "task": "mingi_django.tasks.timer_commands",
        # Every hour
        "schedule": crontab(hour='*', minute=0),
        "args": (),
    },
    "timer_commands": {
        "task": "mingi_django.tasks.custom_timer_commands",
        # Every hour
        "schedule": crontab(hour='*', minute=0),
        "args": (),
    },
    "revenue_rollups": {
        "task": "mingi_django.tasks.revenue_rollups",
        # Hourly
        "schedule": crontab(hour='*', minute=20),
        "args": (),
    },
    "power_rollups": {
        "task": "mingi_django.tasks.power_rollups",
        # Hourly
        "schedule": crontab(hour='*', minute=40),
        "args": (),
    },
    "daily_power": {
        "task": "mingi_django.tasks.daily_power_rollups",
        # Daily at 4am UTC
        "schedule": crontab(hour='4', minute=0),
        "args": (),
    },
    "daily_message_count": {
        "task": "mingi_django.tasks.daily_message_count",
        # Daily at 1am UTC
        "schedule": crontab(hour='1', minute=0),
        "args": (),
    },
    "daily_message archive": {
        "task": "mingi_django.tasks.daily_message_archive",
        # Daily at 3am UTC
        "schedule": crontab(hour='3', minute=0),
        "args": (),
    },
  
    # Check bitharvester status after every 3 hours
    "bitharvester status": {
        "task": "mingi_django.tasks.check_BH",
        'schedule': timedelta(hours=3),
        "args": (),
    },
    # Check hourly power rollups after every 3 hours
    "rollups status": {
        "task": "mingi_django.tasks.check_power_rollups",
        'schedule': timedelta(hours=3),
        "args": (),
    },
    "SMS Gateway Check": {
        "task": "mingi_django.tasks.check_gateway",
        'schedule': timedelta(hours=3),
        "args": (),
    },
    "Demo Users": {
        "task": "mingi_django.tasks.maintain_demo_users",
        "schedule": crontab(hour=8, minute=0),
        "args": (),
    },
    "Subscription User Maintenance": {
        "task": "mingi_django.tasks.subscription_plan",
        "schedule": crontab(hour=5, minute=0),
        "args": (),
    },
    "BBOX Data Collection": {
        "task": "mingi_django.tasks.bboxx_api_data_collection",
        "schedule": crontab(minute='*/20'),
        "args": (),
    },
}

#===================
# smssync secret
#===================
SMSSYNC_SECRET = '006348'
SMSSYNC_SECRET_VULCAN = 'Vul4563'

#===========================
# test details
#==========================
SOUTH_TESTS_MIGRATE = False

# Django Nose
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
DDF_FILL_NULLABLE_FIELDS = False
DDF_USE_LIBRARY = True

#=================================
# SMS as admin email
#==================================
SEND_SMS_AS_ADMIN_EMAIL = True

#=====================================
# Grappelli - Jazzy admin
#=====================================
GRAPPELLI_ADMIN_TITLE = 'Mingi Admin'

AUTH_PROFILE_MODULE = 'mingi_django.Website_User_Profile'

#=====================================
# Glacier Info
#=====================================
# TODO: Wesley's credentials for now; we should replace this with mingi creds.
AWS_GLACIER_CREDENTIALS = {
    'aws_access_key_id': 'AKIAIFR36VOFG2B3HBTQ',
    'aws_secret_access_key': 'FO4n48rOHwKu23bZ1niK/UvePvyOxWOYwu3AxXGt',
    'region': 'us-east-1'
}
AFRICAS_TALKING_INPUT_VAULT_NAME = 'default_africas_talking_input'
AFRICAS_TALKING_OUTPUT_VAULT_NAME = 'default_africas_talking_output'
LINE_STATE_VAULT_NAME = 'default_line_state'


# Failed login settings
AXES_LOGIN_FAILURE_LIMIT = 5 # maximum incorrect attempts
AXES_LOCK_OUT_AT_FAILURE = True # If maximum attempts exceeded, lock out user
AXES_USE_USER_AGENT = True # Lock out baser on IP and user agent
AXES_COOLOFF_TIME = 0.5 # Time in hours, to reactivate the account
AXES_LOCKOUT_URL = 'limit/' # Redirect URL


# Energy limits faulty limits
ENERGY_MIN_FAULTY_LIMIT = 0 
USER_ENERGY_MAX_FAULTY_LIMIT = 100000
LINE_ENERGY_MAX_FAULTY_LIMIT = 100000
SITE_ENERGY_MAX_FAULTY_LIMIT = 100000


# Set time in minutes, within which to load messages
# For all messages, all sms messages
MESSAGE_LOAD_TIME = 30

# Define maximum days range for graphs & reports
# To avoid overloading server & timeouts
MAX_ALL_SITES_GRAPH_DAYS = 60
MAX_SINGLE_SITE_GRAPH_DAYS = 180

EMAIL_ACTIVATION_ADDRESS = ['support@steama.co','software@steama.co','ryan@steama.co']
CUSTOM_FROM_EMAIL = 'app@steama.co' #quick fix for errors


OPERATOR_MAPPER ={
    '67372':"mpesa_ke",
    'MPESA':"mpesa_ke",
    '+254702690718':'mpesa_ke', # smssync
    '+254702688230':'mpesa_ke', # Send SMS
    '+22962684418':'mtn_benin', # expect messages
}
# sms gateways available
SMS_GATEWAY = (
    ('AT', 'Africas Talking'),
    ('TW', 'Twilio (Any)'),
    ('TW-US', 'Twilio US'),
    ('TW-UK', 'Twilio UK'),
    ('TW-FR', 'Twilio France'),
    ('F1', 'FocusOne'),
    ('SC02-+254702688230-SEND', 'SC02-+254702688230-SEND'),
    ('SC01-+254702690718-RECEIVE', 'SC01-+254702690718-RECEIVE'),
    ('SMS-Gateway-API', 'SMS-Gateway-API'),
)

# Enable CORS for specific URLS
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
        'steama.co',
        'simon-mingi.herokuapp.com',
        'emoder-mingi.herokuapp.com',
        'herokuapp.com',
    )

# User registration API secret key
API_SECRET_KEY = '8=s$yAh45h&B3ln*d84kyxOi4cnupmWgwok6-01#xeh-9m23'

# Define charset for encoding
DEFAULT_CHARSET = 'utf-8'
DEFAULT_CONTENT_TYPE = 'text/html'

# change default encoding to utf-8
reload(sys)
sys.setdefaultencoding("utf-8")

#DOCRAPTOR SETTINGS
DOCRAPTOR_API_KEY = "kiO1WadXkGx9vU3EShD"

API_BASE_URL = "https://staging-api.steama.co/"

# load static URLS
LOAD_STATIC_URL = False


STRIPE_API_KEY_SK = "sk_test_s1qf6ajOgLduq7Sf6tMHZafN"

TIMEZONE_KEY = "AIzaSyCT3wT1UAELNNDhwrSXd99SXClaQXd1Utc"
# https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200&key=YOUR_API_KEY test URL

BBOXX_USERNAME = "emily@steama.co"
BBOXX_PASSWORD = "l4wuC9gm330p"
