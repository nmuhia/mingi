# Django settings for mingi_django project.

import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

if "FORWARD" in os.environ:
    FORWARD = os.environ.get('FORWARD')
else:
    FORWARD = "FALSE"

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
HOME_PATH = os.path.join(os.path.dirname(__file__), '../../')

ADMINS = (
    ('Emily Moder', 'emily@steama.co'),
    ('Wesley Verne', 'wesley@steama.co'),
    ('Nyambura Muhia', 'nyambura@steama.co'),
    ('Simon Muthusi', 'simon@steama.co'),
)

MANAGERS = ADMINS

# This has been overrided in the other settings files
DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.sqlite3',
        # DB name, or path to database file if using sqlite3.
        'NAME': os.path.join(HOME_PATH, 'database/testdb'),
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 3

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Static asset configuration
STATIC_ROOT = os.path.join(HOME_PATH, 'static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(HOME_PATH, 'staticfiles'),
)

MEDIA_ROOT = os.path.join(HOME_PATH, 'media')
MEDIA_URL = '/media/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '0rzpb2zu)$1)x^kwjh5p2mb8i2xnekyx@isrbb!&qg+2xgz6ui'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    'django.core.context_processors.request',
    'django.core.context_processors.static'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Track failed logins
    'axes.middleware.FailedLoginMiddleware',

    # enable CORS
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'mingi_django.middleware.set_breadcrumb.FilterBreadcrumbMiddleware',
    'mingi_django.middleware.set_timezone.MyTimezoneMiddleware',
)

AUTHENTICATION_BACKENDS = (
    #'mingi_django.backends.RecipientAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'mingi_django.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'mingi_django.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(HOME_PATH, 'templates'),
)

INSTALLED_APPS = (

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Creates token model for API authentication
    'rest_framework',
    'rest_framework.authtoken',

    'kopo_kopo',
    'africas_talking',
    'mingi_user',
    'mingi_site',
    'mingi_django',
    'utils',

    'new_steama',

    'smuggler',
    'grappelli',
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'djcelery',
    'django_nose',
    'bandit',
    #'debug_toolbar',

    # Track failed logins
    'axes',
    # export data
    'data_exports',

    # enable CORS
    'corsheaders',
    'jfu'
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
