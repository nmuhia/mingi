from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import pytz


TRIGGER_LEVELS = (
    ('equal_to','Equal To'),
    ('less_than','Less Than'),
    ('greater_than','Greater Than'),
    )
TRIGGER_TYPE = (
    ('temp','Temperature'),
    ('voltage','Voltage'),
    #('utility_use','Utility Use (since 12am)'),
    ('account_balance','Account Balance'),
    )
USER_TYPE = (
    ('user','User'),
    ('line','Line'),
    ('site','Site'),
    )
MSG_TYPE = (
    ('sms','SMS'),
    ('email','Email'),
    )
MULTIMEDIA_TYPES = (
        ('youtube','YouTube'),
        ('pdf','PDF'),
        )

TIMEZONES = list()
for pyz in pytz.all_timezones:
    TIMEZONES.append((pyz,pyz))


class Error_Log(models.Model):
    """
    Creates a record of internal or external problems with the system
    """

    ERROR_TYPES = (
        ("SEND_SMS","Send SMS Failed"),
        ("SEND_EMAIL","Send EMAIL Failed"),
        ("INTERNAL","Other Internal Error"),
        ("EQUIPMENT","Equipment Error"),
        ("EXTERNAL","Other External Error"),
        ("K2_PROCESS","Payment Processing Error"),
        ("FAULTY_DATA","Faulty Data Warning"),
        ("DEBUGGING", "Debugging Message"),
        ("LOG_DATA", "Log Data for Tracking"),
        ("PING_ERROR", "Server URL Doesn't Exist"),
        ("WARNING", "Warning / Alert"),
        ("TIMEZONE", "Timezone error alert"),
        ("OTHER","Other Error: Unknown Type"),
    )

    timestamp = models.DateTimeField(auto_now_add=True)
    problem_type = models.CharField(max_length=30, choices=ERROR_TYPES,
                                    default="OTHER")
    problem_message = models.CharField(max_length=300, blank=True, null=True)
    creator = models.CharField(max_length=50, blank=True, null=True)
    
    is_resolved = models.BooleanField(default=False)
    site = models.ForeignKey('mingi_site.Mingi_Site', blank=True, null=True)

    def __unicode__(self):
        return self.problem_type + "; " + self.problem_message
    
    class Meta:
        verbose_name = "Error Log"
        ordering = ['-timestamp']


class Diagnostic_Settings(models.Model):
    """
    store rewritable variables relevant to system diagnostics
    """

    name = models.CharField(max_length=50)
    
    last_updated = models.DateTimeField()

    k2_transactions_count = models.IntegerField(default=0)
    AT_sms_balance = models.DecimalField(max_digits=7,decimal_places=2,
                                         default=0.00)


class Timer_Command(models.Model):
    """
    An SMS to be sent on a timer
    """

    # number of message recipient
    destination_phone = models.CharField(max_length=20)
    
    message = models.CharField(max_length=150)

    hours = models.CommaSeparatedIntegerField(max_length=61, default="0")

    site = models.ForeignKey('mingi_site.Mingi_Site', blank=True, null=True)

    @property
    def hour_array(self):
        
        return self.hours.split(',')

class Custom_Timer_Command(models.Model):
    """
    An SMS to be sent on a timer
    """
    # destination of message. can be email address or phone number
    destination = models.CharField(max_length=30)
    # Site. None means all sites
    site = models.ForeignKey('mingi_site.Mingi_Site', blank=True, null=True)
    # Mingi User, None means all users
    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True)
    line = models.ForeignKey('mingi_site.Line', blank=True, null=True)
    # Utility. None means all utilities if any
    utility = models.ForeignKey('mingi_django.Utility', blank=True, null=True)
    # Trigger type. e.g. costs, energy usage, meter readings etc
    trigger_type = models.CharField(max_length=15, blank=True, null=True, choices=TRIGGER_TYPE)
    # Trigger level. e.g. when the specified values is high or low
    trigger_level = models.CharField(max_length=15, blank=True, null=True, choices=TRIGGER_LEVELS)
    # Trigger value
    message = models.CharField(max_length=300)
    message_type = models.CharField(max_length=5, choices=MSG_TYPE, default="email")
    # Hours
    hours = models.CommaSeparatedIntegerField(max_length=61, default="0", blank=True)
    threshold = models.FloatField(blank=True, null=True)

    # was the trigger most recently active? (to prevent repeated actions)
    is_triggered = models.BooleanField(default=False)


    @property
    def hour_array(self):
        
        return self.hours.split(',')


class Website_User_Profile(models.Model):

    USER_TYPE_CHOICES = (
        ('AAD','SteamaCo Admin'),
        ('CAD','Admin User'),
        ('CRO','Read-Only User'),
        )

    user = models.OneToOneField(User)
    is_demo = models.BooleanField(default=False, verbose_name="Is Demo?")
    is_asset_finance = models.BooleanField(default=False, verbose_name="Is Asset Finance?")
    send_emails = models.BooleanField(default=False, verbose_name="Send Emails")
    user_type = models.CharField(max_length=3, choices=USER_TYPE_CHOICES, default="CRO")
    utility_management = models.BooleanField(default=False, verbose_name="Utility Management")    
    utilities = models.ManyToManyField('mingi_django.Utility', blank=True, null=True)
    token = models.CharField(max_length=50,blank=True,null=True)
    timezone = models.CharField(max_length=20,blank=True,null=True)
    # enable creating of user specific menus
    user_menu  = models.ManyToManyField('mingi_django.Menu', blank=True, null=True)

    
class App_Permissions(models.Model):

    # name of the card's view in new_steama/views.py
    feature_name = models.CharField(max_length=100, verbose_name="Feature Name")    
    # list of usernames
    enabled_users = models.ManyToManyField(User, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    """@property
    def user_array(self):
        users = self.enabled_users.split(',')
        return_list = list()
        
        for user in users:
            try:
                match = User.objects.get(username=user)
                return_list.append(match)
            except:
                pass

        return return_list"""
    

    class Meta:
        verbose_name = "App Permission"

class Utility(models.Model):

    # The name of the utility
    name = models.CharField(max_length=50,default="Electricity")

    # unit in which the utility is stored (meter readings)
    primary_unit = models.CharField(max_length=20, blank=True)

    # unit in which the time-series data is displayed (site-level)
    secondary_unit = models.CharField(max_length=20, blank=True)

    # unit in which the totals data is displayed (user-level)
    primary_user_unit = models.CharField(max_length=20, blank=True)

    # unit in which the time-series data is displayed (user-level)
    secondary_user_unit = models.CharField(max_length=20, blank=True)

    # Label for time series display (e.g. "Power")
    time_series_label = models.CharField(max_length=100, blank=True, null=True)
    
    # Label for totals data (e.g. "Energy Use")
    daily_totals_label = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.name.strip(':')

    def get_ts_label(self, is_site_level=True):
        """
        return the full label of the time-series data for a chart; 
        site-level label used if is_site_level = True, otherwise user-level
        """
        
        if is_site_level == True:
            return self.time_series_label + " (" + self.secondary_unit + ")"
        else:
            return self.time_series_label + " (" + self.secondary_user_unit + ")"
        
    def get_totals_label(self, is_site_level=True):
        """
        return the full label of the daily totals data for a chart; 
        site-level label used if is_site_level = True, otherwise user-level
        """
        
        if is_site_level == True:
            return self.daily_totals_label + " (" + self.primary_unit + ")"
        else:
            return self.daily_totals_label + " (" + self.primary_user_unit + ")"

    def get_ts_unit(self, is_site_level=True):        
        
        if is_site_level == True:
            return self.secondary_unit
        else:
            return self.secondary_user_unit
        
    def get_totals_unit(self, is_site_level=True):
        
        if is_site_level == True:
            return self.primary_unit
        else:
            return self.primary_user_unit



    class Meta:
        verbose_name_plural = "Utilities"

class FAQCategory(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class FAQ(models.Model):
    category = models.ForeignKey(FAQCategory)
    question = models.TextField()
    answer = models.TextField()
    keywords = models.CharField(max_length=200)
    has_multimedia = models.BooleanField(default=False,blank=False)
    multimedia = models.ForeignKey('mingi_django.Multimedia',blank=True,null=True)

    def __unicode__(self):
        return self.question

class Multimedia(models.Model):
    # descriptive title
    title = models.CharField(max_length=100)
    # multimedia type
    multimedia_type = models.CharField(max_length=15,choices=MULTIMEDIA_TYPES,default='youtube')
    # link to the file
    content = models.CharField(max_length=1000)
    # optional thumbnail
    thumbnail = models.CharField(max_length=100,blank=True,default='define default link here')
    # set as featured or not
    is_featured = models.BooleanField(default=False)
    # order video
    order = models.IntegerField(default=0)

    def __unicode__(self):
        return self.multimedia_type + " " + self.title


class Menu(models.Model):
    HTML_TARGETS = (
        ('__blank','BLANK PAGE'),
        ('__self','SAME FRAME'),
        ('__parent','PARENT FRAME'),
        ('__top','WINDOW FULL BODY'),
        )
    MENU_TYPE = (
        ('all','Both'),
        ('normal','Normal Menu Only'),
        ('utility','Utility Management Menu Only'),
        )
    # menu parent or indication whther menu is a parent menu (0)
    menu_parent = models.ForeignKey("self",null=True,blank=True)
    # menu has submenus ?
    has_menu = models.BooleanField(default=False,verbose_name="Has sub-menu ?")
    # menu name be descriptive
    menu_name = models.CharField(max_length=50)
    # A descriptive name for a menu
    menu_title = models.CharField(max_length=50,blank=True,null=True)
    # Menu URL, if it's a terminal URL, or menu ID
    menu_action = models.CharField(max_length=200,blank=True,null=True,verbose_name="menu action URL",default="#")
    # Unique menu ID
    menu_id = models.CharField(max_length=200,blank=True,null=True,verbose_name="Menu ID")
    # Menu target, whether menu to open in new window, parent etc (see HTML options for this)
    menu_target = models.CharField(max_length=30,choices=HTML_TARGETS, default="__parent")
    # Menu position
    menu_order = models.IntegerField(blank=True,null=True)
    # menu icon, from font-awesome, etc
    menu_icon = models.CharField(max_length=30,blank=True,null=True)
    # menu type: utility, normal, all
    menu_type = models.CharField(max_length=10,choices=MENU_TYPE,default='all')
    # # utility management menu, deprecate this
    # is_utility_management = models.BooleanField(default=False,verbose_name="Is a utility management menu ?")
    # has TextField
    has_text_field = models.BooleanField(default=False,verbose_name="Has an input box ?")
    # is a staff only menu
    is_staff = models.BooleanField(default=False,verbose_name="Is a staff only menu ?")
    # menu MenuStatus
    is_active = models.BooleanField(default=True) 

    def __unicode__(self):
        return self.menu_name + '-'+str(self.menu_action)+' -'+str(self.menu_title)


# Create a Django REST Framework token for each created User
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
