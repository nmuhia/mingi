from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('mingi_django.views',

    # Authentication
    url(r'^login/$', 'login_page'),
    url(r'^logout/$', 'logout_page'),
    url(r'^admin/logout/$', 'logout_page'),
)

urlpatterns += patterns('mingi_user.views',

    url(r'^bulk/user/$', 'handle_bulk_user_edit'),

)

urlpatterns += patterns('mingi_site.views',

    url(r'async/site/', 'site_details_handler'),
                        
)

urlpatterns += patterns('',
    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    url(r'^admin/', include('smuggler.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^utils/', include('utils.urls')),

)

handler404 = "mingi_django.views.not_found_page"
handler500 = "mingi_django.views.server_error_page"
