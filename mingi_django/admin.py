from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from models import Error_Log, Timer_Command, Website_User_Profile, App_Permissions, Utility, FAQ, FAQCategory,Custom_Timer_Command,Multimedia,Menu
from django.contrib.auth.forms import UserChangeForm
from django import forms



class ErrorLogAdmin(admin.ModelAdmin):
    list_display = ('creator','timestamp','problem_type','problem_message',
                    'is_resolved')
    list_filter = ('problem_type',)

class TimerCommandAdmin(admin.ModelAdmin):
    list_display = ('destination_phone','message','hours')

class WebsiteUserInline(admin.StackedInline):
    filter_horizontal = ('utilities','user_menu')
    model = Website_User_Profile
    can_delete = False
    verbose_name_plural = 'profile'

class UserAdmin(UserAdmin):
    inlines = (WebsiteUserInline, )


class AppPermissionsAdmin(admin.ModelAdmin):
    filter_horizontal = ('enabled_users',)
    list_display = ('feature_name','enabled')

class UtilityAdmin(admin.ModelAdmin):   
    list_display = ('name','primary_unit','secondary_unit','time_series_label','daily_totals_label')

class FAQAdmin(admin.ModelAdmin):
    list_display = ('category','question','answer','keywords')

class FAQCategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
class CustomTimerCommandAdmin(admin.ModelAdmin):
    list_display = ('destination','site','line','user','trigger_type','trigger_level','message_type','message','hours',)

class MultimediaAdmin(admin.ModelAdmin):
    list_display = ('multimedia_type','title',)

class MenuAdmin(admin.ModelAdmin):
    list_display = ('menu_parent','has_menu','menu_name','menu_title','menu_action','menu_id','menu_order','menu_icon','is_active',)
    list_filter = ('menu_parent','is_active','menu_type',)

admin.autodiscover()
admin.site.register(Error_Log, ErrorLogAdmin)
admin.site.register(Timer_Command, TimerCommandAdmin)
admin.site.register(App_Permissions, AppPermissionsAdmin)
admin.site.register(Utility, UtilityAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(FAQ, FAQAdmin)
admin.site.register(FAQCategory, FAQCategoryAdmin)
admin.site.register(Custom_Timer_Command, CustomTimerCommandAdmin)
admin.site.register(Multimedia, MultimediaAdmin)
admin.site.register(Menu, MenuAdmin)
