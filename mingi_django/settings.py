import socket
import os

server = socket.gethostname()
hostname = os.environ.get('HOSTNAME')

# Heroku production server
if hostname == "HEROKU_PROD":
    from mingi_django.mingi_settings.heroku_dev import *
# AWS front-end production server
elif server == 'mingi.prod.html.1':
    os.environ["FORWARD"] = "TRUE"
    os.environ["DEPLOYMENT"] = "PRODUCTION"
    from mingi_django.mingi_settings.prod import *
# AWS back-end production server
elif server == 'mingi.prod.sms.1':
    os.environ["FORWARD"] = "TRUE"
    os.environ["DEPLOYMENT"] = "PRODUCTION"
    # os.environ["SERVER_END"] = "BACK"
    from mingi_django.mingi_settings.prod import *
# AWS staging server
elif server == 'mingi.staging.all.1':
    from mingi_django.mingi_settings.staging import *
# Heroku staging server
elif hostname == "HEROKU_DEV":
    from mingi_django.mingi_settings.heroku_dev import * 
# Local servers
elif server == "WesleyBox" or server == "debian" or server == "10":
    from mingi_django.mingi_settings.wesley import *

elif server == "simoh" or server == 'simoh-HP-EliteBook-8460p' or server == "muhia-Inspiron-N4050" or server=='10':
    from mingi_django.mingi_settings.simon import *
else:
    from mingi_django.mingi_settings.dev import *
