"""
This is meant as an example for writing future tests, so the commenting will
be more verbose than necessary.
"""

import datetime
import pytz

from django.test import TestCase

from mingi_django.models import Utility
from mingi_django.tasks import check_hourly_rollups_success,\
    create_daily_power_rollup, get_site_utility_lists, daily_power_rollups
from mingi_site.models import Hourly_Site_Power, Mingi_Site, Line_State, Line,\
    Daily_Power
from mingi_user.models import Mingi_User


class HourlySitePowerTestCase(TestCase):
    """
    Tests creation of hourly site power rollups.
    """

    TEST_USER_PHONE = '+254726767443'

    def setUp(self):
        test_utility = Utility.objects.create(
            name='Test Utility'
        )
        test_site = Mingi_Site.objects.create(
            name='Test Site',
            num_lines=1,
            equipment='stuff',
            date_online=datetime.date(2015, 1, 1),
            latitude=0.00,
            longitude=0.00)
        test_user = Mingi_User.objects.create(
            telephone=self.TEST_USER_PHONE,
            first_name='Test',
            last_name='User',
            site=test_site,
            line_number=1
        )
        test_line = Line.objects.create(
            site=test_site,
            number=1,
            name="Test Line",
            user=test_user,
            utility_type=test_utility
        )

    def test_power_rollups(self):
        """
        Tests that power rollups for a day are properly created
        """

        test_site = Mingi_Site.objects.get()
        test_user = Mingi_User.objects.get()

        test_datetime = datetime.datetime(2015, 1, 2, 0, 0, 0, 0, pytz.utc)

        # Create Line state snapshots with a day buffer around the test date
        Line_State.objects.create(
            site=test_site,
            number=1,
            timestamp=test_datetime - datetime.timedelta(days=1),
            meter_reading=0,
            user=test_user
        )
        Line_State.objects.create(
            site=test_site,
            number=1,
            timestamp=test_datetime,
            meter_reading=24,
            user=test_user
        )
        Line_State.objects.create(
            site=test_site,
            number=1,
            timestamp=test_datetime + datetime.timedelta(days=1),
            meter_reading=48,
            user=test_user
        )
        Line_State.objects.create(
            site=test_site,
            number=1,
            timestamp=test_datetime + datetime.timedelta(days=2),
            meter_reading=72,
            user=test_user
        )

        # Run power rollup creation functions
        check_hourly_rollups_success(test_site, test_datetime.date())
        create_daily_power_rollup(test_datetime.date(), test_site,
                                  get_site_utility_lists(test_site))

        # Check hourly power rollups
        # Check that the correct rollup exists for each hour in the test day
        hourly_power_rollups = Hourly_Site_Power.objects.all()
        self.assertEqual(len(hourly_power_rollups), 24)
        for this_hour in [test_datetime + datetime.timedelta(hours=n)
                          for n in range(24)]:
            hour_rollups = hourly_power_rollups.filter(timestamp=this_hour)
            self.assertEqual(len(hour_rollups), 1)
            hour_rollup = hour_rollups[0]
            self.assertEqual(hour_rollup.site.id, test_site.id)
            self.assertEqual(hour_rollup.amount, 1)

        # Check daily power rollups
        daily_power_rollups = Daily_Power.objects.all()
        self.assertEqual(len(daily_power_rollups), 2)
        for rollup in daily_power_rollups:
            self.assertEqual(rollup.timestamp, test_datetime.date())
            self.assertEqual(rollup.amount, 24)


class DailyPowerTestCase(TestCase):
    """
    Tests creation of daily site power rollups.
    """

    TEST_USER_PHONE = '+254726767443'
    TEST_DATE = datetime.date(2015, 1, 3)

    def setUp(self):
        test_utility = Utility.objects.create(
            name='Test Utility'
        )
        test_site = Mingi_Site.objects.create(
            name='Test Site',
            num_lines=1,
            equipment='stuff',
            date_online=datetime.date(2015, 1, 1),
            latitude=0.00,
            longitude=0.00)
        test_user = Mingi_User.objects.create(
            telephone=self.TEST_USER_PHONE,
            first_name='Test',
            last_name='User',
            site=test_site,
            line_number=1
        )
        for i in range(1, 5):
            if i == 1:
                user = test_user
            else:
                user = None
            test_line = Line.objects.create(
                site=test_site,
                number=i,
                name="Test Line %d" % i,
                user=user,
                utility_type=test_utility,
                is_connected=True
                )
            Line_State.objects.create(site=test_site,
                                      number=i,
                                      meter_reading=0.0,
                                      user=user,
                                      timestamp=datetime.datetime(
                                          2015, 1, 1, 20, 0, 0, 0))
            Line_State.objects.create(site=test_site,
                                      number=i,
                                      meter_reading=1.0,
                                      user=user,
                                      timestamp=datetime.datetime(
                                          2015, 1, 3, 5, 0, 0, 0))

    def test_daily_power_base_case(self):
        """
        test that daily power objects are created as expected
        """
        
        site = Mingi_Site.objects.get(name="Test Site")
        user = Mingi_User.objects.get(telephone=self.TEST_USER_PHONE)
        utility = Utility.objects.all()[0]

        daily_power_rollups(self.TEST_DATE)
        
        rollups = Daily_Power.objects.all()
        for rollup in rollups:
            print str(rollup.site) + ", " + str(rollup.amount)
        self.assertTrue(len(rollups), 2)

        site_rollup = Daily_Power.objects.get(site=site,
                                              user=None,
                                              amount=0.72727,
                                              utility=utility)
        user_rollup = Daily_Power.objects.get(site=None,
                                              user=user,
                                              amount=0.72727,
                                              utility=utility)

    def test_daily_power_nonuser_lines(self):
        """
        test daily power rollups with 
        """
        self.assertEqual(Line_State.objects.count(), 8)
        site = Mingi_Site.objects.get(name="Test Site")

        # add in lines which are active but have no users
        site.num_lines = 4
        site.save()

        user = Mingi_User.objects.get(telephone=self.TEST_USER_PHONE)
        utility = Utility.objects.all()[0]

        daily_power_rollups(self.TEST_DATE)
        
        rollups = Daily_Power.objects.all()
        for rollup in rollups:
            print str(rollup.site) + ", " + str(rollup.amount)
        self.assertTrue(len(rollups), 5)

        site_rollup = Daily_Power.objects.get(site=site,
                                              user=None,
                                              amount=2.90909,
                                              utility=utility)
        line_rollups = Daily_Power.objects.filter(site=None,
                                                  amount=0.72727,
                                                  utility=utility)
        self.assertEqual(len(line_rollups), 4)
        self.assertTrue(line_rollups[0].user == user)


class PostpaidRollupsTestCase(TestCase):
    """
    Tests rollup calculation for postpaid sites.
    """

    def setUp(self):
        """
        Creates a simple site, line, and set of line states.
        """

        self.site = Mingi_Site.objects.create(
            name='Test Site',
            num_lines=1,
            date_online=datetime.date(2015, 1, 1),
            latitude=1.0,
            longitude=-2.0
        )

        self.utility = Utility.objects.create(
            id=1,
            name="Mock Utility"
        )

        self.line = Line.objects.create(
            site=self.site,
            number=1,
            is_connected=True,
            line_status='1',
            utility_type=self.utility
        )

        self.line_state_1 = Line_State.objects.create(
            site=self.site,
            number=1,
            timestamp=datetime.datetime(2015, 2, 1),
            meter_reading=10.0
        )

        self.line_state_2 = Line_State.objects.create(
            site=self.site,
            number=1,
            timestamp=datetime.datetime(2015, 2, 3),
            meter_reading=20.0
        )

    def test_daily_power_rollup_postpaid(self):
        """
        Tests daily power rollup calculation for postpaid sites.
        """
        create_daily_power_rollup(
            datetime.datetime(2015, 2, 2),
            self.site,
            [{
                'lines': '1',
                'name': self.utility.name
            }],
            True
        )

        # Check that site rollup was created
        self.assertEqual(Daily_Power.objects.count(), 2)
        self.assertEqual(Daily_Power.objects.filter(site=self.site).count(), 1)
        site_dp = Daily_Power.objects.get(site=self.site)
        self.assertEqual(site_dp.amount, 5)

        # Check that line rollup was created
        self.assertEqual(Daily_Power.objects.filter(
            line_obj=self.line).count(), 1)
        line_dp = Daily_Power.objects.get(line_obj=self.line)
        self.assertEqual(line_dp.amount, 5)
