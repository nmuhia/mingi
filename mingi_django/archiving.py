# General utils for AWS Glacier archiving

import boto.glacier.layer2

from django.conf import settings


def update_archive(vault_name, archive_description, contents):
    """
    Archives the given content in the given vault, with the given description.
    Returns the id of the created archive.
    """
    creds = settings.AWS_GLACIER_CREDENTIALS
    
    conn = boto.glacier.layer2.Layer2(
        aws_access_key_id=creds['aws_access_key_id'],
        aws_secret_access_key=creds['aws_secret_access_key'],
        region_name=creds['region']
    )
    vault = conn.get_vault(vault_name)
    archive_writer = vault.create_archive_writer(description=archive_description)
    archive_writer.write(contents)
    archive_writer.close()
    return archive_writer.get_archive_id()


def retrieve_archive_init(vault_name, archive_id):
    """
    Begins a retrieval job for the given archive in the given vault.

    Returns the boto.glacier.job.Job object, which can be polled later for the
    archive contents using job.get_output(). The function will produce "400"
    errors if it has not yet completed.
    """
    creds = settings.AWS_GLACIER_CREDENTIALS

    conn = boto.glacier.layer2.Layer2(
        aws_access_key_id=creds['aws_access_key_id'],
        aws_secret_access_key=creds['aws_secret_access_key'],
        region_name=creds['region']
    )
    vault = conn.get_vault(vault_name)
    return vault.retrieve_archive(archive_id)


def get_job(vault_name, job_id):
    """
    Gets a retrieval job from the given vault. This function allows the Job
    object from retrieve_archive_init to be restored from the job id in the
    event that the object in memory is lost.

    Once the job is finished, its output can be obtained via job.get_output().
    The function will produce "400" errors if it has not yet completed.
    """
    creds = settings.AWS_GLACIER_CREDENTIALS

    conn = boto.glacier.layer2.Layer2(
        aws_access_key_id=creds['aws_access_key_id'],
        aws_secret_access_key=creds['aws_secret_access_key'],
        region_name=creds['region']
    )
    vault = conn.get_vault(vault_name)
    return vault.get_job(job_id)
