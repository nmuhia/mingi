from django.core.management.base import BaseCommand, CommandError

from africas_talking.models import Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line_State, Site_Record, Hourly_Site_Revenue, Hourly_Site_Power

import datetime
import csv
import math
from django.db.models import Q
from django.core.mail import EmailMessage
from africas_talking.sendSMS import sendSMS


class Command(BaseCommand):

    def handle(self, *args, **options):
	FUEL_CONTACT_EMAIL="muhia.nyambura@gmail.com"
	FUEL_CONTACT_PHONE="+254715762453"
	print "function call instructions: python manage.py log_fuel_purchase M 12, KES, LTRS"
	
	#args=args.split(",")
	
	#check message format
	if (len(args)!=3):
		try:
			sendSMS("+254715762453","To log fuel purchase, reply 'M 12 , KSh, Litres'")
			print "Arguments provided not right"
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

	#send message(email and sms) to fuel contact 
	else:
		message= "By: Nyambura Muhia\nContact: +254715762453\nFuel Cost(KES): "+args[1]+"\n Fuel Amount(Ltrs):"+args[2]
		
		#add fuel purchase to db
		#Site_Record.objects.create(site=12,key="Fuel Purchase",value="KES"+args[1]+",LTRS"+args[2], timestamp=now)
		
		try:
			#send SMS to fuel contact
			sendSMS(FUEL_CONTACT_PHONE,message)	
			print "Message sent to fuel contact"
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
		try:
			#send email to fuel contact
			subject = "Fuel Purchase Alert "				
			email = EmailMessage(subject,message,to=[FUEL_CONTACT_EMAIL])
			email.send()
			print "Email sent to fuel contact"

		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
		try:
			msgOut = "The fuel purchase of %s ltrs at KES %s level has been logged as %s, thanks!" % args[2], args[1]
			sendSMS("+254715762453",msgOut)
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))										


        """
        return csv form with financial data from a given month
        
        
        filename = "csv_data.csv"
        
        if len(args) != 2:
            print "function call instructions: python manage.py get_finance_report [Month (number)] [email address]"
            return

        month = int(args[0])
        year = 2014
        email = args[1]
            
        # get relevant transaction records
        payments = Kopo_Kopo_Message.objects.filter(timestamp__month=month,
                                                    timestamp__year=year)
        sms = Africas_Talking_Output.objects.filter(timestamp__month=month,
                                                    timestamp__year=year)


        all_sites = Mingi_Site.objects.all().order_by('id')

        # % of revenues to AE
        revenue_coeff = [0.0,1.0,1.0,0.0,1.0,1.0,0.1,0.0,0.0,0.1,0.0,0.0,0.1,0.0,0.1]
        # monthly fee for agent at each site (% of revenues)
        agent_fees = [0.0,0.05,0.05,0.0,0.05,0.05,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        # monthly fee for site managers
        sm_fees = [0.0,15000,8000,0.0,0.0,5000,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        # rent for land
        site_fees = [0.0,1200,400,0.0,0.0,400,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        
        pay_site_totals = process_payments(payments,all_sites)
        sms_site_totals = process_sms(sms,all_sites)

        with open(filename,'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['Site Name','AE Till Revenue (KSh)',
                             'PG Till Revenue (KSh)','Total Revenue (KSh)','AE Total Revenue (KSh)','Agent Fees (KSh)','Site Manager Salary (KSh)','Site Rent (KSh)','SMS Costs - Human (KSh)','SMS Costs - BH (KSh)','SMS Costs - Total (KSh)','Balance (KSh)'])

            # write data to csv
            row_num = 0
            for pay_row, sms_row in zip(pay_site_totals,sms_site_totals):
                print str(row_num)
                total_in = pay_row[3]*revenue_coeff[row_num]
                agent_cost = pay_row[3]*agent_fees[row_num]
                sm_cost = sm_fees[row_num]
                site_cost = site_fees[row_num]
                total_out = agent_cost + sm_cost + site_cost + sms_row[3]
                writer.writerow([pay_row[0],str(pay_row[1]),str(pay_row[2]),str(pay_row[3]),str(total_in),str(agent_cost),str(sm_cost),str(site_cost),str(sms_row[1]),str(sms_row[2]),str(sms_row[3]),str(total_in - total_out)])
                
                row_num += 1

        
        subject = "Finance Reporting Data: " + str(month)

        email = EmailMessage(subject,'data is attached',to=[email])
        email.attach_file(filename)
        email.send()"""


            
