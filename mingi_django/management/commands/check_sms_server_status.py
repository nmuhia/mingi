import datetime
from datetime import datetime, timedelta #, tzinfo

from mingi_site.models import Bit_Harvester
from africas_talking.models import Africas_Talking_Output,Africas_Talking_Input
from mingi_django.models import Error_Log

from django.core.management.base import BaseCommand, CommandError
from django.core.mail import EmailMessage
from mingi_django.views import send_front_end_sms
import math
import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware



class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments"
        
    def handle(self, *args, **options):
        """
        Che SMS server status and switch to secondary if primary is failing
        Check for the last 3 hours, all sites
        Report if unable to switch
        """
        HOURS = 3
        now = gen_zone_aware(TIME_ZONE,datetime.now())
        end_date = now.replace(minute=0,second=0,microsecond=0)
        start_date = end_date - timedelta(hours=HOURS)
        NO_MESSAGES_FAILURE = 0

        # Flag gateway failled
        failed_gateway = list()
        msg_ratio = 100
        MSG_RATION = 3
        # Check for BH
        all_bit_harvester = Bit_Harvester.objects.filter(site__is_active=True)
        all_out_messages = Africas_Talking_Output.objects.filter(timestamp__gte=start_date,timestamp__lte=end_date) 
        all_in_messages = Africas_Talking_Input.objects.filter(timestamp__gte=start_date,timestamp__lte=end_date)

        for bh in all_bit_harvester :
            HOURS  = bh.logging_hours
            end_date = gen_zone_aware(TIME_ZONE,datetime.now())
            start_date = end_date - timedelta(hours=HOURS)
            out_messages = len(Africas_Talking_Output.objects.filter(timestamp__gte=start_date,timestamp__lte=end_date,gateway=bh.output_gateway))
            in_messages = len(Africas_Talking_Input.objects.filter(timestamp__gte=start_date,timestamp__lte=end_date,gateway=bh.input_gateway))

            if out_messages == 0 and in_messages == 0 :
                msg_ratio = 0
            else:
                try:
                    msg_ratio = float(out_messages)/float(in_messages) 
                except ZeroDivisionError:
                    msg_ratio = 100

            if msg_ratio > MSG_RATION:
                # Log problem
                failed_gateway.append({'bh':bh,'start_date':str(start_date),'end_date':str(end_date)})
                Error_Log.objects.create(problem_type="WARNING", problem_message='Output gateway '+str(bh.output_gateway)+' & input gatway '+str(bh.input_gateway)+' Gateway on bitharvester '+str(bh)+' has a probem in receiving messages') 
        

        
        # Switch with secondary
        for f_gateway in failed_gateway:
            # Check for access energy sites only
            if 'access:energy' != f_gateway['bh'].site.company_name or 'SteamaCo' != f_gateway['bh'].site.company_name :
                continue


            primary_bh_type = f_gateway['bh'].bh_type
            primary_gateway_in = f_gateway['bh'].input_gateway
            primary_gateway_out = f_gateway['bh'].output_gateway

            secondary_bh_type = f_gateway['bh'].bh_type_secondary
            secondary_gateway_in = f_gateway['bh'].backup_gateway_in
            secondary_gateway_out = f_gateway['bh'].backup_gateway_out

            # avoid switching with a None gateway
            if primary_gateway_in is not None and primary_gateway_out is not None and primary_bh_type is not None and secondary_gateway_out is not None and secondary_bh_type is not None and secondary_gateway_in is not None:
                # Switch gateway
                said_bh =  Bit_Harvester.objects.filter(id=f_gateway['bh'].id).update(bh_type=secondary_bh_type,bh_type_secondary=primary_bh_type,input_gateway=secondary_gateway_in,output_gateway=secondary_gateway_out,backup_gateway_in=primary_gateway_in,backup_gateway_out=primary_gateway_out)
                

                # change the number to respond to in BH
                if f_gateway['bh'].version == "V4":
                    num_string = str(now.microsecond)
                    sms_id = "*3" + num_string[2:]
                    try:
                        send_front_end_sms(f_gateway['bh'].telephone, sms_id+",setcfg,server="+str(secondary_gateway_in.telephone))
                    except Exception as e:
                        Error_Log.objects.create(problem_type="SEND_SMS",problem_message="BH_SWITCH_ERROR: "+str(e))
                        pass
                


                # Report problem
                for owner in f_gateway['bh'].site.owners.all():
                    email = EmailMessage(subject="Sneaker Cloug Gateway Dignostics",body="The sending gateway "+str(primary_gateway_in)+" and receiving gateway "+str(primary_gateway_out)+" has a problem in sending or receiving messages. \nPrimary gateway switched with secondary instead.",to=[owner.email])
                    try:
                        email.send()
                    except:
                        pass

      
        return      
