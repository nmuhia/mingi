from django.core.management.base import BaseCommand

from africas_talking.models import Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line_State, Site_Record, Hourly_Site_Revenue, Hourly_Site_Power, Daily_Power
from report_functions import get_header, get_email_subject

from mingi_django.dashboard import *
from mingi_django.transactions import atOutToTable, atInToTable, transactions_csv_row, transactions_csv_header, k2ToTable, combineTransactions
from django.core.mail import EmailMessage

import datetime
from django.db.models import Q
import os, tempfile, zipfile
import StringIO
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

from itertools import chain
import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware
import collections
import types


class Command(BaseCommand):
    def add_arguments(self, parser):
        """
        arguments: objectname & email address mandatory, other variables are optional
        """
        parser.add_argument('object_name',help="sends a daily summary SMS to customers at any of the sites in SITE_LIST")
        parser.add_argument('email',help="email_address or [email_address,file_format]")
        parser.add_argument('-o',nargs=1,help='3 arguments hold optional site_name')
        parser.add_argument('-o1',nargs=2,help='4 arguments hold optional start_date and end_date')
        parser.add_argument('-o2',nargs=3,help='5 arguments hold optional site_name, start_date, end_date')

    def handle(self, *args, **options):
        """
        run timer commands if they are scheduled for this hour
        """

        objectName = options['object_name']

        objectNameMapper = {
        "Line_State_Snapshot":"Line State Snapshots",
        "Kopo_Kopo_Message":"Payments",
        "Mingi_User":"User Profiles",
        "SMS_Data":"SMS Data",
        "Voltage":"System Data",
        "Revenue Rollup":"Hourly Site Revenue",
        "Power Rollup":"Hourly Site Utility Use",
        "Daily Site Power":"Daily Site Utility Use",
        "Daily User Power":"Daily Line Utility Use",
        "Africas_Talking_Output":"a:e Payments",
        }

        try:
            email = options['email'][0]
            file_format = options['email'][1]
        except Exception as e:
            email = options['email']
            file_format = "plain"

        start_date = None
        end_date = None
        site_name = None
        # file_format = "plain"
        
        title = "Mingi CSV Data: " + objectName 
        # if there is a third argument, it is a site name
        if options['o']:
            site_name = options['o'][0]
        
        
        # if there are 4 arguments, there is no site name and 3-4 are dates
        if options['o1']:
            start_string = options['o1'][0]
            end_string = options['o1'][1]
            start_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(start_string,'%d-%m-%y'))
            end_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(end_string,'%d-%m-%y')) + datetime.timedelta(days=1)

            # only send aggregated data to steama.co addresses
            if not "steama.co" in email:
                return

        # if there are 5 arguments, there is a site name and 4-5 are dates
        if options['o2']:
            site_name = options['o2'][0]
            start_string = options['o2'][1]
            end_string = options['o2'][2]
            start_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(start_string,'%d-%m-%y'))
            end_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(end_string,'%d-%m-%y')) + datetime.timedelta(days=1)            
            start_date = start_date.replace(microsecond=int(0))

        # get file name
        file_date = datetime.datetime.now().isoformat()
        try:
            filename = str(objectNameMapper[objectName])+" - "+site_name+" - "+str(file_date)+".csv"
            zip_filename = str(objectNameMapper[objectName])+" - "+site_name+" - "+str(file_date)+".zip"
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",problem_message="Invalid object name in csv_data report. Error: "+str(e))
            return

        if (objectName == "Line_State_Snapshot"):
            
            if options['o1']:
            # if len(args) == 4:
                snapshots = Line_State.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)

                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return
                
            elif options['o2']:
            # elif len(args) == 5:
                snapshots = Line_State.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date,site__name=site_name)

            else:
                snapshots = Line_State.objects.all()

            snapshot_list = list()
            for snapshot in snapshots:
                snapshot_list.append(snapshotToTable(snapshot))
            write_snapshot_csv(snapshot_list,filename,title,start_date,end_date)

        elif (objectName == "Mingi_User"):

            if (start_date) and (end_date):
                users = Mingi_User.objects.filter(created__gte=start_date, created__lt=end_date)
            else:
                users = Mingi_User.objects.all()

            if (site_name):
                users = users.filter(site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return

            user_list = list()
            for user in users:
                user_list.append(userToTable(user))
            write_user_csv(user_list,filename,title,start_date,end_date)

        elif (objectName == "Kopo_Kopo_Message"):

            if (start_date) and (end_date):
                payments = Kopo_Kopo_Message.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:
                payments = Kopo_Kopo_Message.objects.all()

            if (site_name):
                payments = payments.filter(user__site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return

            payment_list = list()
            for payment in payments:
                payment_list.append(k2ToTable(payment))
            write_k2_csv(payment_list,filename,title,start_date,end_date)

        elif (objectName == "Voltage"):

            if (start_date) and (end_date):
                voltages = Site_Record.objects.filter(Q(timestamp__gte=start_date), Q(timestamp__lt=end_date),Q(key="System Voltage") | Q(key="BH Data Log"))
            else:
                voltages = Site_Record.objects.filter(Q(key="System Voltage") |
                                                      Q(key="BH Data Log"))
            if (site_name):
                voltages = voltages.filter(site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return
            
            voltage_list = list()
            for log in voltages:
                voltage_list.append(voltageToTable(log))
            write_voltage_csv(voltage_list,filename,title,start_date,end_date)

        elif (objectName == "Africas_Talking_Output"):
            if (start_date) and (end_date):
                msgs = Africas_Talking_Output.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:
                msgs = Africas_Talking_Output.objects.all()

            msgs_list = list()

            if (site_name):
                bh_msgs = msgs.exclude(bit_harvester=None)
                bh_msgs = bh_msgs.filter(bit_harvester__site__name=site_name).order_by('timestamp')
                user_msgs = msgs.exclude(user=None)
                user_msgs = user_msgs.filter(user__site__name=site_name)

                for msg in user_msgs:
                    msgs_list.append(atOutToTable(msg))
                for msg in bh_msgs:
                    msgs_list.append(atOutToTable(msg))

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return
                
                for msg in msgs:
                    msgs_list.append(atOutToTable(msg))
            
            write_msg_csv(msgs_list,filename,title,start_date,end_date)


        elif (objectName == "SMS_Data"):
            if (start_date) and (end_date):
                atInQuery = Africas_Talking_Input.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
                atOutQuery = Africas_Talking_Output.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:                
                atInQuery = Africas_Talking_Input.objects.all()
                atOutQuery = Africas_Talking_Output.objects.all()
                
            msgs_list = list()

            if (site_name):
                atOut_bh_msgs = atOutQuery.filter(bit_harvester__site__name=site_name).exclude(bit_harvester=None).order_by('timestamp')
                atIn_bh_msgs = atInQuery.filter(bit_harvester__site__name=site_name).exclude(bit_harvester=None).order_by('timestamp')
                atOut_user_msgs = atOutQuery.filter(user__site__name=site_name).exclude(user=None).order_by('timestamp')
                atIn_user_msgs = atInQuery.filter(user__site__name=site_name).exclude(user=None).order_by('timestamp')

                user_msgs = combineTransactions(atIn_user_msgs, atOut_user_msgs, [])
                bh_msgs = combineTransactions(atIn_bh_msgs, atOut_bh_msgs, [])

                user_msgs.sort(key=itemgetter('timestamp'), reverse=True)
                bh_msgs.sort(key=itemgetter('timestamp'), reverse=True)
                   
                msgs_list = list(chain(user_msgs, bh_msgs))
                
            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return
                
                for msg in atOutQuery:
                    msgs_list.append(atOutToTable(msg))

                for msg in atInQuery:
                    msgs_list.append(atInToTable(msg))
                        
            write_msg_csv(msgs_list,filename,title,start_date,end_date)
                 

        elif (objectName == "Power Rollup"):

            if (start_date) and (end_date):
                rollups = Hourly_Site_Power.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:
                rollups = Hourly_Site_Power.objects.all()

            if (site_name):
                rollups = rollups.filter(site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return

            rollup_list = list()
            for rollup in rollups:
                rollup_list.append(rollupToTable(rollup))
            write_rollup_csv(rollup_list,filename,"Power",title,start_date,end_date)

        elif (objectName == "Revenue Rollup"):
            if (start_date) and (end_date):
                rollups = Hourly_Site_Revenue.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:
                rollups = Hourly_Site_Revenue.objects.all()

            if (site_name):
                rollups = rollups.filter(site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return

            rollup_list = list()
            for rollup in rollups:
                rollup_list.append(rollupToTableRevenue(rollup))
            write_revenue_rollup_csv(rollup_list,filename,"Revenue",title,start_date,end_date)

        elif (objectName == "Daily User Power") or (objectName == "Daily Site Power"):

            if (start_date) and (end_date):
                rollups = Daily_Power.objects.filter(timestamp__gte=start_date, timestamp__lt=end_date)
            else:
                rollups = Daily_Power.objects.all()

            if (site_name):
                if objectName == "Daily User Power":
                    rollups1 = rollups.exclude(user__isnull=True).filter(user__site__name=site_name)
                    rollups2 = rollups.exclude(line_obj__isnull=True).filter(line_obj__site__name=site_name)
                    rollups = list()
                    for rollup in rollups1:
                        rollups.append(rollup)
                    for rollup in rollups2:
                        rollups.append(rollup)
                else:
                    rollups = rollups.exclude(site__isnull=True).filter(site__name=site_name)

            else:
                # double-check that non-site-specific info is only sent to SC
                if not 'steama.co' in email:
                    return

            rollup_list = list()
            for rollup in rollups:
                rollup_list.append(dailyRollupToTable(rollup))
            write_daily_rollup_csv(rollup_list,filename,title,start_date,end_date)

        else:
            return
        
        if start_date and end_date:
            subject = get_email_subject(title, start_date, end_date)
        else:
            subject = get_email_subject(title, datetime.datetime.today(), None)

        email_msg = EmailMessage(subject,'data is attached',to=[email])
        if file_format == "zip":
            # compress file
            # open file for writing
            zipped_file = zipfile.ZipFile(zip_filename, mode='w')
            try:
                # compress file
                zipped_file.write(filename,compress_type=compression)
            except Exception as e:
                # if error, send uncompressed file
                print e
            finally:
                zipped_file.close()
                
            email_msg.attach_file(zip_filename)
            os.remove(zip_filename)
            os.remove(filename)
        else:
            email_msg.attach_file(filename)
            os.remove(filename)


        email_msg.send()

            
def write_user_csv(user_list,fileName,title=None,start_date=None,end_date=None):
    """
    Wrtes the given user list as csv.
    """
    
    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])
        writer.writerow(user_csv_header())
        for user in user_list:
            writer.writerow(user_csv_row(user))

def write_msg_csv(msgs_list,fileName,title=None,start_date=None,end_date=None):
    """
    Writes the given user list as csv.
    """
    
    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)
               
        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])
        
        writer.writerow(transactions_csv_header())
        for msg in msgs_list:
            writer.writerow(transactions_csv_row(msg))
    

def write_snapshot_csv(snapshot_list,fileName,title=None,start_date=None,end_date=None):
    """
    Wrtes the given snapshot list as csv.
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])

        writer.writerow(snapshots_csv_header())
        for snapshot in snapshot_list:
            writer.writerow(snapshots_csv_row(snapshot))
            

def write_k2_csv(payment_list,fileName,title=None,start_date=None,end_date=None):
    """
    Writes the given payment list as csv
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)
            
        writer.writerow([""])
            
        writer.writerow(k2_csv_header())
        for payment in payment_list:
            writer.writerow(k2_csv_row(payment))


def write_voltage_csv(voltage_list,fileName,title=None,start_date=None,end_date=None):
    """
    Writes the given voltage list as csv
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)
        
        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)
            
        writer.writerow([""])
            
        writer.writerow(voltage_csv_header())
        for log in voltage_list:
            writer.writerow(voltage_csv_row(log))


def write_rollup_csv(rollup_list,fileName,rollup_type,title=None,start_date=None,end_date=None):
    """
    Writes the given rollup list as csv
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])

        writer.writerow(rollups_csv_header(rollup_type))
        for rollup in rollup_list:
            writer.writerow(rollups_csv_row(rollup))

def write_revenue_rollup_csv(rollup_list,fileName,rollup_type,title=None,start_date=None,end_date=None):
    """
    Writes the given rollup list as csv
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])

        writer.writerow(rollups_csv_header(rollup_type))
        for rollup in rollup_list:
            writer.writerow(revenue_rollups_csv_row(rollup))


def write_daily_rollup_csv(rollup_list,fileName,title=None,start_date=None,end_date=None):
    """
    Writes the given rollup list as csv
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)

        header = get_header(title,start_date,end_date)
        for row in header:
            writer.writerow(row)

        writer.writerow([""])

        writer.writerow(daily_rollup_csv_header())
        for rollup in rollup_list:
            writer.writerow(daily_rollup_csv_row(rollup))

"""
# DEPRECATED: see k2ToTable in mingi_django/transactions.py
def k2ToTable(message):
    #Converts the given Kopo Kopo payment message into a dictionary suitable
    #for display in the transactions table.

    row = {}
    row['timestamp'] = message.timestamp
    row['type'] = "Payment"
    row['reference'] = message.reference
    
    if message.user is not None:
        row['other_party_link'] = getUserLink(message.user)
        row['site'] = message.user.site.name
        row['other_party'] = str(message.user)
    else:
        row['other_party_link'] = ""
        row['site'] = ""
        row['other_party'] = message.user

    row['digest'] = str(message.amount)
    row['raw_content'] = message.raw_message
    

    return row
    """
