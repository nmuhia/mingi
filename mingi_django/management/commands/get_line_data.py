from django.core.management.base import BaseCommand, CommandError

from mingi_site.models import Line_State, Mingi_Site
from mingi_user.models import Mingi_User

from mingi_django.dashboard import snapshotToTable, snapshots_csv_row, snapshots_csv_header
from django.core.mail import EmailMessage

import datetime
from django.db.models import Q
import csv

import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here. arguments can only be three or four"
        parser.add_argument('site_name',help="callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy")
        parser.add_argument('line_number',help="callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy")
        parser.add_argument('start_string',help="callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy")
        parser.add_argument('end_string',help="callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy")
        parser.add_argument('email',help="callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy")

    def handle(self, *args, **options):
        """
        get csv output of line states for a single line
        """

        filename = "csv_data.csv"

        # print str(len(args))
        # for arg in args:
        #     print str(arg)
        # if len(args) != 5:
        #     print "callings instructions: python manage.py get_line_data [site name, line number, start date, end date, email]; date format is dd-mm-yy"
        #     return

        site_name = options['site_name']
        line_number = int(options['line_number'])

        # get timestamps
        start_string = options['start_string']+" 00:00:00"
        end_string = options['end_string']+" 23:59:59"
        start_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(start_string,'%d-%m-%y %H:%M:%S'))
        end_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(end_string,'%d-%m-%y %H:%M:%S'))
        start_date = start_date.replace(microsecond=int(0))
        end_date = end_date.replace(microsecond=int(999999))

        email = options['email']

        snapshots = Line_State.objects.filter(site__name=site_name,number=line_number,timestamp__gte=start_date,timestamp__lte=end_date)

        snapshot_list = list()
        for snapshot in snapshots:
            snapshot_list.append(snapshotToTable(snapshot))
            write_snapshot_csv(snapshot_list,filename)

        subject = "Line State Data: %s - Line %d" % (site_name, line_number)

        email = EmailMessage(subject,'data is attached',to=[email])
        email.attach_file(filename)
        email.send()
            

def write_snapshot_csv(snapshot_list,fileName):
    """
    Wrtes the given snapshot list as csv.
    """

    with open(fileName,'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(snapshots_csv_header())
        for snapshot in snapshot_list:
            writer.writerow(snapshots_csv_row(snapshot))
