from django.core.management.base import BaseCommand

from mingi_site.models import Mingi_Site, Line_State

import datetime

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('site_name',help="callings instructions: python manage.py edit_line_states [SITE NAME] [NO. OF HOURS TO ROLL BACK]")
        parser.add_argument('roll_back_hrs',help="callings instructions: python manage.py edit_line_states [SITE NAME] [NO. OF HOURS TO ROLL BACK]")

    def handle(self, *args, **options):
        """
        run timer commands if they are scheduled for this hour
        """

        # if len(args) != 2:
        #     print "callings instructions: python manage.py edit_line_states [SITE NAME] [NO. OF HOURS TO ROLL BACK]"
        #     return

        try:
            site = Mingi_Site.objects.get(name=options['site_name'])
        except:
            print "site not found"
            return 

        try:
            rollback_hrs = float(options['roll_back_hrs'])
        except:
            print "invalid integer"
            return
        
        line_states = Line_State.objects.filter(site=site)
        
        for state in line_states:
            timestamp = state.timestamp
            timestamp = timestamp - datetime.timedelta(hours=rollback_hrs)
            state.timestamp = timestamp
            state.save()

        print "COMPLETE"
