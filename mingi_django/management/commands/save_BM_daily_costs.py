import datetime
from django.db.models import Q

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from mingi_site.models import Mingi_Site, Hourly_Site_Revenue
from mingi_user.models import Mingi_User


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments"

    def handle(self, *args, **options):
        """
        looks at most recent energy use data to update customers' energy use
        category as appropriate
        """

        # test with Fargo House, then activate
        all_sites = Mingi_Site.objects.filter(is_active=True, name="Fargo House")
        
        for site in all_sites:
            
            # look for all dates where revenue rollup does not yet exist
            next_date = datetime.date.today()
            
            while next_date < datetime.date.today():
                building_revenue_date(site, next_date)


def building_revenue_date(site, date):
    """
    create an Hourly_Site_Revenue object to show the total for a site and date
    """

    all_uses = Mingi_User.objects.filter(site=site, line_number__isnull=False)
    rollups = Daily_Power.objects.filter(user__isnull=False,
                                         user__site=site, timestamp=date)
            

