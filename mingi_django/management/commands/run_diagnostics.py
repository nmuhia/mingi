from django.core.management.base import BaseCommand

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Line_State, Bit_Harvester
from mingi_django.models import Error_Log
#from mingi_django.tasks import *
from diagnostic_functions import tally_transactions, find_user_issues, site_recipients, todays_energy_usage, last_week_energy_usage, check_bit_harvester_log_issues, flag_site_params

from operator import itemgetter
from django.core.mail import EmailMessage

import datetime

import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware
# from html import HTML

ADMIN_RECIPIENTS = ['emily@steama.co', 'harrison@steama.co',
                    'gordon@steama.co','sam@steama.co',
                    'lameck@steama.co','joseph@steama.co',
                    'robert@steama.co']

class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments"

    def handle(self, *args, **options):
	"""calls helper methods and compiles a system summary to flag potential issues;
        sends diagnostic email with summary to relevant email addresses"""
        
        now = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
        string_now = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + " " + str(now.hour) + ":" + str(now.minute)
        now = str(now)

        # ADMIN SUMMARY

        # print out flags

        msgContent = "Site Flags Raised\n"
        bh_log_issues = check_bit_harvester_log_issues()
        msgContent +="\nbitHarvesters:\n"
       
        if (bh_log_issues):
            for log_issue in bh_log_issues:
                msgContent += log_issue[0]+":"+log_issue[1]+","+log_issue[2]+"\n"

        else:
            msgContent += "No Issues Found\n\n"

        # compile data values
        site_list = Mingi_Site.objects.filter(is_active = True)
        energy_totals = [[] for site in site_list]
        zero_energy_flags = [[] for site in site_list]
        top_5_users = [[] for site in site_list]
        voltage_flags = list()
        revenue_flags = list()
    
        index = 0
        for site in site_list:
            site_data = flag_site_params(site)

            # append flags
            if len(site_data[1]) > 0:
                zero_energy_flags.append(site.name)

            if site_data[3][0] == True:
                voltage_flags.append(site.name)

            if site_data[4][0] == True:
                revenue_flags.append(site.name + " (No revenue collected in past 24 hours)")

            if site_data[4][1] == True:
                revenue_flags.append(site.name + " (Revenue decreasing over 5 days)")
            #zero_energy_flags[index] = site.name
            #zero_energy_flags[index].append(site_data[1])
            top_5_users[index] = [site.name,site_data[2]]

            energy_totals[index] = [site.name, site_data[0][0], site_data[0][1]]
            
        print str(energy_totals)

        # output data values
        msgContent += "\nVoltage Flags (decreasing voltage for 3 days):\n"
        if len(voltage_flags) == 0:
            msgContent += "None\n"
        else:
            for row in voltage_flags:
                msgContent += row + "\n"
        msgContent += "\n"

        msgContent += "\nRevenue Flags:\n"
        if len(revenue_flags) == 0:
            msgContent += "None\n"
        else:
            for row in revenue_flags:
                msgContent += "- " + row + "\n"
        msgContent += "\n"

        msgContent += "\nEnergy Use Flags (zero power use for 1 hour or more):\n"
        found_issue = False
        for row in zero_energy_flags:
            if len(row) > 0:
                found_issue = True
                msgContent += str(row) + "\n"

        if not found_issue:
            msgContent += "None\n"
        msgContent += "\n"


        issues_header = [["Phone No","First Name","Last Name","Account Balance","Line Status","Flag Raised"]]
        [tech_issues, customer_issues] = (find_user_issues())

        msgContent += "The following users have been flagged for potential technical issues:\n"

        if (tech_issues):

            issues = sorted(tech_issues,key=itemgetter(0))
            
            for issue in tech_issues:
                if issue[5] == True:
                    lineStatus = "ON"
                else:
                    lineStatus = "OFF"

                msgContent += "%s: %s %s - (Account Balance = %.2f, Line Status = %s) --> Issue: %s\n" % (issue[0],issue[1],issue[2],issue[3],lineStatus,issue[6])

        else:
            msgContent += "None\n\n"

        msgContent += "The following users with no recent energy use have been flagged:\n"
        
        if (customer_issues):
            for issue in customer_issues:
                if issue[5] == True:
                    lineStatus = "ON"
                else:
                    lineStatus = "OFF"
                    msgContent += "%s: %s %s - (Account Balance = %.2f, Line Status = %s)\n" % (issue[0],issue[1],issue[2],issue[3],lineStatus)+"\n"
                    
        else:
            msgContent += "No Issues Found\n\n"

        msgContent += "\n\nSite Success Metrics\n\nTop 3 Sites By Energy Use\nTotals:\n"

        max_energy_use = [[0.0,None],[0.0,None],[0.0,None]]
        max_energy_change = [[0.0,None],[0.0,None],[0.0,None]]
        for row in energy_totals:
            if len(row) != 3:
                break

            # get top 3 energy use
            if row[2] > max_energy_use[0][0]:
                max_energy_use[0][0] = row[2]
                max_energy_use[0][1] = row[0]
            elif row[2] > max_energy_use[1][0]:
                max_energy_use[1][0] = row[2]
                max_energy_use[1][1] = row[0]
            elif row[2] > max_energy_use[2][0]:
                max_energy_use[2][0] = row[2]
                max_energy_use[2][1] = row[0]
            
            # get top 3 energy change
            try:
                change = (row[2] - row[1]) / row[1]
            except ZeroDivisionError:
                change = 0.0
            if change > max_energy_change[0][0]:
                max_energy_change[0][0] = change
                max_energy_change[0][1] = row[0]
            elif change > max_energy_change[1][0]:
                max_energy_change[1][0] = change
                max_energy_change[1][1] = row[0]
            elif change > max_energy_change[2][0]:
                max_energy_change[2][0] = change
                max_energy_change[2][1] = row[0]

        for row in max_energy_use:
            if row[1]:
                msgContent += row[1] + ": " + str(row[0]) + " kWh\n"

        msgContent += "\nPercent Change from yesterday:\n"
        
        for row in max_energy_change:
            if row[1]:
                msgContent += row[1] + ": " + str(row[0]*100) + "%\n"
        msgContent += "\nTop 5 Users By Energy Use:\n\n"
        
        for row in top_5_users:
            if len(row) > 1:
                msgContent += "- " + row[0] + ": " + str(row[1]) + "\n"
        
        for address in ADMIN_RECIPIENTS:
              email = EmailMessage('SMS Diagnostic Report',msgContent,to=[address])
              email.send()
              
        SITE_RECIPIENTS = site_recipients()

        # SEND SITE-SPECIFIC MESSAGE TO CLIENT EMAIL ADDRESSES ---------------
        for site_recipient in SITE_RECIPIENTS:           

            for site_info in site_recipient:
                siteName = site_info[0]

                counts = tally_transactions(siteName)
        
                msgContent = "%s Summary\nMingi System Status as of %s\n\nMessages Received: %d\nbitHarvester Logs: %d\nMessages Sent: %d\nPayments Received: %d\n\n" % (siteName,string_now,counts[0],counts[1],counts[2],counts[3])+"\n"

                [tech_issues, customer_issues] = find_user_issues(siteName)
                msgContent += "The following users have been flagged for potential technical issues:\n"
        
                if (tech_issues):
                    for issue in tech_issues:
                        if issue[5] == True:
                            lineStatus = "ON"
                        else:
                            lineStatus = "OFF"
                            msgContent += "%s: %s %s - (Account Balance = %.2f, Line Status = %s) --> Issue: %s\n" % (issue[0],issue[1],issue[2],issue[3],lineStatus,issue[6])+"\n"
                        
                else:
                    msgContent += "No Issues Found\n\n"

                msgContent += "The following users with no recent energy use have been flagged:\n"
        
                if (customer_issues):
                    for issue in customer_issues:
                        if issue[5] == True:
                            lineStatus = "ON"
                        else:
                            lineStatus = "OFF"
                            msgContent += "%s: %s %s - (Account Balance = %.2f, Line Status = %s)\n" % (issue[0],issue[1],issue[2],issue[3],lineStatus)+"\n"
                        
                else:
                    msgContent += "No Issues Found\n\n"

                bh_log_issues = check_bit_harvester_log_issues(siteName)
                msgContent +="The following bitharvesters have been flagged for potential issues:\n"
       
                if (bh_log_issues):
                    for log_issue in bh_log_issues:
                        msgContent += log_issue[0]+":"+log_issue[1]+","+log_issue[2]+"\n"

                else:
                    msgContent += "No Issues Found\n\n"


                for address in site_info:
                    if address == siteName:
                        pass
                    else:
                        email = EmailMessage('STEAMA Diagnostic Report',
                                             msgContent,to=[str(address)])
                        email.send()
                    
