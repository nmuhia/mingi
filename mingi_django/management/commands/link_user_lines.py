import datetime

import csv
import math
import pytz
from django.db.models import Q
from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
import sys

from mingi_user.models import Mingi_User
from africas_talking.userResponse import findUserByLine

from mingi_site.models import Bit_Harvester,Mingi_Site, Line
from mingi_django.models import Website_User_Profile, Error_Log
from django.core.management.base import BaseCommand, CommandError

from django.utils.timezone import now

from django.contrib.auth.models import User


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments "

    def handle(self, *args, **options):
        """
        Check Bitharvester status
        Format: manage.py check_BH_status [hours]
        Default hours = 3
        """

        all_lines = Line.objects.all()

        for line in all_lines:
            number = line.number
            site = line.site

            try:
                user = findUserByLine(number,site.name)
                
                # set user linked to Line
                line.user = user
                line.save()
                print "saved %s to line number %s, site %s" % (str(user),str(number),site.name) 
            except Mingi_User.DoesNotExist:
                print "No user found for line %s, site %s" % (str(number),site.name) 
            except Mingi_User.MultipleObjectsReturned:
                print "Duplicate users on line %s, site %s" % (str(number),site.name)

                
