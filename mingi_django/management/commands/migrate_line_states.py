from mingi_django.migration_out import send_line_states

from django.core.management.base import BaseCommand


class Command(BaseCommand):
	def add_arguments(self, parser):
        print "arguments here. arguments can only be three or four"
        parser.add_argument('var_1',help="usage: python manage.py migrate_line_states <receiving_url>")

    def handle(self, *args, **options):
        """
        Updates the hourly power rollups for every site for the past week.
        """

        # if len(args) != 1:
        #     print "usage: python manage.py migrate_line_states <receiving_url>"
        #     return

        send_line_states(options['var_1'])
