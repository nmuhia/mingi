import datetime
from datetime import datetime, timedelta #, tzinfo


from mingi_django.models import Error_Log,Timer_Command,Custom_Timer_Command,Utility
from africas_talking.models import Africas_Talking_Output
from mingi_site.models import Hourly_Site_Revenue,Hourly_Site_Power

from django.core.management.base import BaseCommand, CommandError
import math



class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('model_name',help="specify model name here")
        # parser.add_argument('-o',nargs=2,help="this is optional paramenters")

    def handle(self, *args, **options):
        """
        This command enhances transfer of data between tables. This can be when a new model is created, deprecating the former & data need to be moved to new model
        @params model_name
        """
        # Enable model name to have a default model
        # model_name = "default_model"
        model_name = options['model_name']
        if model_name is None:
            model_name = "assign_bh_hourly_site_power"

        if model_name == "custom_timer_commands":
            # transfer data from timer_commands model to custom_timer_command model
            from_data = Timer_Command.objects.all()

            for data in from_data:
                try:
                    new_data = Custom_Timer_Command.objects.create(destination=data.destination_phone,message=data.message,hours=data.hours,site=data.site)
                    # delete to avoid re-running data
                    data.delete()
                except:
                    Error_Log.objects.create(problem_type="INTERNAL",problem_message="Error in transfering custom timer commands. From" + str(data) + "To: "+ str(new_data))

        elif model_name == "send_sms_attribution":
            """
            Check for incorrectly tagged bitharvester
            """
            # correct error correction
            all_output = Africas_Talking_Output.objects.filter(Q(raw_message__contains=',ON,')|Q(raw_message__contains=',OFF,'),bit_harvester__null=False)
            for out in all_output:
                # loop through all data user - not none, bit - none
                if out.user is not None and out.bit_harvester is None:
                    # switch user, with user-bitharvester
                    try:
                        bit_har = out.user.bit_harvester
                        out.bit_harvester = bit_har
                        out.save()
                    except Exception as e:
                        Error_Log.objects.create(problem_type='INTERNAL',problem_message='Switching user bitharvester: bit> '+str(out.bit_harvester)+' message>'+str(out)+' user>'+bit_har)

        elif model_name == "bm_revenues":
            print "bm_revenues"
            start_date = datetime(2014,1,1,0,0,0,0)
            revenue_logs = Hourly_Site_Revenue.objects.filter(amount__gt=0, timestamp__gt=start_date)

            # delete offending rollup and recreate
            for log in revenue_logs:
                if log.timestamp.hour == 23:
                    print "Rollup found: " + str(log.timestamp) + " - " + str(log.amount)
                    timestamp = log.timestamp
                    site = log.site
                    log.delete()
                    site._get_hourly_revenue(timestamp)

        elif model_name == "daily_power":
            pass

        elif model_name == 'assign_bh_hourly_site_power':
            # Assign Electricity utility for hourly power records with no utility
            records = Hourly_Site_Power.objects.filter(utility=None)
            electricity = Utility.objects.filter(name='Electricity:')[0]
            for record in records:
                record.utility = electricity
                record.save()
                print "saved"+str(record)

            return "updated successfully. Records - "+str(records.count())

        else:
            return "unrecongnized parameter passed to this command"

        return


