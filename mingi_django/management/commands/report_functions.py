# helper methods for creating downloadable reports

import datetime
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


def get_header(title, start_date, end_date):
    """
    return the header array for a report with title, start_date and end_date
    """

    header = list()

    today = gen_zone_aware(TIME_ZONE,datetime.datetime.today())

    header.append([title])
    header.append(['report generated:',today.strftime('%Y.%m.%d')])

    if start_date and end_date:
        end_date = end_date - datetime.timedelta(days=1)
        header.append(['period start date:',start_date.strftime('%Y.%m.%d')])
        header.append(['period end date:',end_date.strftime('%Y.%m.%d')])
        days = (end_date - start_date).days
        header.append(['reporting days:',str(days)])
    else:
        header.append(['period start date:','n/a'])
        header.append(['period end date:','n/a'])
        header.append(['reporting days:','n/a'])
    return header


def get_email_subject(title, start_date, end_date):
    """
    return the formatted email subject for a report with title, 
    start_date and end_date
    """
    
    if start_date and end_date:
        end_date = end_date - datetime.timedelta(days=1)
        return start_date.strftime('%Y.%m.%d') + " - " + end_date.strftime('%Y.%m.%d') + " " + title

    # treat start_date as current date
    elif start_date:
        return start_date.strftime('%Y.%m.%d') + " " + title
