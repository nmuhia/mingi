import datetime
from decimal import Decimal
from django.utils import timezone

from mingi_user.models import Mingi_User
from mingi_django.models import Error_Log
from mingi_site.models import Line_State

from africas_talking.sendSMS import sendSMS
from africas_talking.bhResponse import send_line_switching_msgs

from django.core.management.base import BaseCommand, CommandError

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):

    HOURS = 6 # time for function to run
    
    def add_arguments(self, parser):
        print "no arguments"
        
    def handle(self, *args, **options):
        """
        run through monthly maintenance functions on the first of each month
        """

        today = gen_zone_aware(TIME_ZONE,datetime.datetime.now()).replace(hour=12,minute=0,second=0,microsecond=0)
        tomorrow = today + datetime.timedelta(days=1)
        
        subscription_users = Mingi_User.objects.filter(payment_plan__contains="Subscription",line_number__isnull=False)

        # check all users on subscription plan
        for user in subscription_users:

            exp_date = user.credit_exp_date.replace(hour=12,minute=0,second=0,microsecond=0) # date's stored in database in UTC
            
            # handle users with today as a payment due date
            if exp_date == today:
                print "EXPIRING USER FOUND..."
                subscriber_maintenance(user)

            # send out a warning message
            elif (exp_date == tomorrow):
                print "WARNING USER FOUND..."
                fee = Decimal(user.payment_plan.split(',')[1])
                balance = user.account_balance - fee
                # in the case of a negative balance, don't overreport
                if balance > fee:
                    balance = fee
                if balance > 0.0:
                    msg_out = "Your energy fee is due tomorrow. Please top up your account with an additional %.2f credits to keep your line turned on."
                    try:
                        sendSMS(BH.telephone,msg_out,BH)
                    except Exception as e:
                        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                

def subscriber_maintenance(user,plan_string=None):
    """
    perform reguarl maintenance on a user
    """

    if plan_string:
        payment_plan = plan_string
    else:
        payment_plan = user.payment_plan
    
    plan_elements = payment_plan.split(',')
    try:
        plan_days = int(plan_elements[2])
        fee = Decimal(plan_elements[1])
        quota = int(plan_elements[3])
    except Exception:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Invalid payment plan format for user " + str(user) + ": " + user.payment_plan)
        
    # charge fee from account
    user.account_balance = Decimal(user.account_balance) - fee
    user.save()

    now = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
    for line in user.line_array:
        Line_State.objects.create(timestamp=now, user=user, number=int(line), account_balance=user.account_balance, site=user.site) 
    
    if user.credit_exp_date:
        exp_date = user.credit_exp_date
        print str(exp_date)
    else:
        exp_date = gen_zone_aware(TIME_ZONE,datetime.datetime.today()).replace(hour=12,minute=0,second=0,microsecond=0)

    # if there was enough credit for charge, increment exp date and inform user
    if user.account_balance >= 0:
        new_exp_date = exp_date + datetime.timedelta(days=plan_days)
        user.credit_exp_date = new_exp_date
        user.save()

        msg_out = "Your energy fee has been charged from your account. Your remaining balance is %d. Your next fee of %d is due on %s" % (int(user.account_balance),int(fee),str(new_exp_date))

        try:
            sendSMS(user.telephone,msg_out,user)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
        
    # otherwise, switch off line and leave exp. date, inform user of balance
    else:
        # *** this could later be optimized via lists
        send_line_switching_msgs(user.bit_harvester,[],user.line_array)
        msg_balance = abs(int(user.account_balance)) + 1
        msg_out = "Your energy fee is due, but there is insufficient credit in your account. Please top up %d to turn your line back on." % msg_balance

        try:
            sendSMS(user.telephone,msg_out,user)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
        
    # maintain daily energy quota if necessary
    # negatives already checked
    if quota > 0 and user.account_balance >= 0:
        id_string = str(datetime.datetime.now().microsecond)[2:] # tgenerate a unique string based on timestamp
        msg_out = "*%s,CREDIT-SET,%d,%s" % (id_string,quota,user.line_number)
        BH = user.bit_harvester
        
        try:
            sendSMS(BH.telephone,msg_out,BH)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
