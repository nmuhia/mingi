from django.core.management.base import BaseCommand
from django.utils.timezone import make_naive

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Daily_Power, Hourly_Site_Revenue, Hourly_Site_Power, Line_State
from africas_talking.models import Africas_Talking_Input

from mingi_django.models import Error_Log
from mingi_user.user_energy_usage import js_timestamp
from mingi_user.models import Mingi_User

from operator import itemgetter
from django.core.mail import EmailMessage

import csv
import datetime
from pytz import utc


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here."
        parser.add_argument('telephone',help="")
        parser.add_argument('filter_ts',help="")
        parser.add_argument('email',help="")

    def handle(self, *args, **options):
	"""
        Run an analysis of payment history on the input customer 
        comparing metrics before and after the input date. Designed
        to measure the sucess / failure of a payment plan adaptation
        """

        telephone = options['telephone']
        filter_ts = datetime.datetime.strptime(options['filter_ts'],'%d-%m-%y')
        filter_date = datetime.date(filter_ts.year,filter_ts.month,filter_ts.day)
        email_address = options['email']

        email_content = ""

        try:
            user = Mingi_User.objects.get(telephone=telephone)
        except Mingi_User.DoesNotExist:
            print "User does not exist"

        daily_totals_before = Daily_Power.objects.filter(user=user,timestamp__lt=filter_date)
        daily_totals_after = Daily_Power.objects.filter(user=user,timestamp__gte=filter_date)

        ls_before = Line_State.objects.filter(user=user,timestamp__lt=filter_ts).order_by('timestamp')
        ls_after = Line_State.objects.filter(user=user,timestamp__gte=filter_ts).order_by('timestamp')

        revenue_before = Kopo_Kopo_Message.objects.filter(user=user,timestamp__lt=filter_ts)
        revenue_after = Kopo_Kopo_Message.objects.filter(user=user,timestamp__gte=filter_ts)

        duration_index = len(revenue_before) - 1
        before_ts = make_naive(revenue_before[duration_index].timestamp,utc)
        duration_before = (filter_ts - before_ts).days
        print str(duration_before)
        duration_after = (datetime.datetime.now() - filter_ts).days
        print str(duration_after)

        before_use_total = 0.0
        for log in daily_totals_before:
            before_use_total += float(log.amount)

        after_use_total = 0.0
        for log in daily_totals_after:
            after_use_total += float(log.amount)

        before_revenue_total = 0.0
        for log in revenue_before:
            before_revenue_total += float(log.amount)

        after_revenue_total = 0.0
        for log in revenue_after:
            after_revenue_total += float(log.amount)

        email_content += "BEFORE %s:\nDays: %d\nRevenue (KSh):%.2f\nEnergy (kWh):%.2f" % (str(filter_date), int(duration_before), float(before_revenue_total), float(before_use_total))
        email_content += "\n\nAFTER %s:\nDays: %d\nRevenue (KSh):%.2f\nEnergy (kWh):%.2f" % (str(filter_date), int(duration_after), float(after_revenue_total), float(after_use_total))
        
        email = EmailMessage("Payment History Analysis: " + str(user), email_content, to=[email_address])
	email.send()
