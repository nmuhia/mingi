from django.core.management.base import BaseCommand

from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Line_State
from africas_talking.sendSMS import sendSMS

import datetime
from mingi_django.models import Error_Log
from africas_talking.userResponse import EON_CONTACT_NO
import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('site_name',help="sends a daily summary SMS to customers at any of the sites in SITE_LIST")
    
    def handle(self, *args, **options):
        """
        sends a daily summary SMS to customers at any of the sites in SITE_LIST
        """
        siteName = options['site_name']
        # siteName = args[0]
        
        try:
            site = Mingi_Site.objects.get(name=siteName)
            send_to = Mingi_User.objects.filter(site=site).exclude(payment_plan__contains="Subscription")
            
            for user in send_to:
                # iterate through line numbers
                # msgOut = compileMessage(user)
                if user.site.company_name == "E.ON":
                    msg_out = "Habari, %s, salio la akaunti yako ni %.1f kredit. Kupata menu tuma hefuri 'M' Kwenda %s" % (user.first_name,float(user.account_balance),EON_CONTACT_NO)
                else:
                    msg_out = None
                
                try:
                    sendSMS(user.telephone,msg_out,user)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))

        except Mingi_Site.DoesNotExist:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="In timed SMS command, site: %s does not exist" % siteName)
        except Mingi_Site.MultipleObjectsReturned:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="In timed SMS command, site: %s has multiple entries" % siteName)

# this is an old version of the function that compiles more advanced information
def compileMessage(user):
    """
    put together a message for that user's SMS summary
    *** NOTE: assumes access to regular bitHarvester data ***
    """

    msgOut = "Line %s STEAMA Summary\n\nYour energy use:\n" % (user.line_number)
    
    [dayUse,weekUse,peakPower] = getEnergyUse(user)
    if user.account_balance < 0:
        kshBalance = 0.0
    else:
        kshBalance = float(user.account_balance)

    msgOut += "Last 24 hours: %.1f kWh\nLast 7 days: %.1f kWh\nRemaining balance:\n %.2f KSh" % (dayUse,weekUse,kshBalance)

    if (peakPower > 0):
        hourBalance = kshBalance / float(user.energy_price) / peakPower
        msgOut += "\nEstimated hours of peak use: %.1f" % hourBalance
    
    return msgOut


def getEnergyUse(user):
    """
    characterize the user's energy use today based on this week's data
    """
    
    lineNos = user.line_number.split(',')
    weekUse = 0
    dayUse = 0
    peakPower = 0
    
    for line in lineNos:
        nowtime = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
        start_day = nowtime - datetime.timedelta(days=1)
        start_week = nowtime - datetime.timedelta(days=7)
        
        
        weekSnapshots = Line_State.objects.filter(user=user,number=line,
                                                  timestamp__gte=start_week)
        weekSnapshots = weekSnapshots.exclude(meter_reading__isnull=True)
        daySnapshots = weekSnapshots.filter(timestamp__gte=start_day)
        
        if (len(daySnapshots) > 0):
            dayUse += daySnapshots[0].meter_reading - daySnapshots.reverse()[0].meter_reading
            weekUse += weekSnapshots[0].meter_reading - weekSnapshots.reverse()[0].meter_reading

        # get peak power from last 24 hours
        peakLinePower = 0
        for i in range(1,len(daySnapshots)):
            power = getUnitPower(daySnapshots[i],daySnapshots[i-1])
            if power > peakLinePower:
                peakLinePower = power
            
        peakPower += peakLinePower

    #dayType = getDayType()

    return [dayUse,weekUse,peakPower]


def getUnitPower(r1,r2):
    """
    calculate power use between two line state snapshots
    """
    
    energy_delta = r2.meter_reading - r1.meter_reading
    time_delta = (r2.timestamp - r1.timestamp).total_seconds() / 3600.0
    
    if time_delta <= 0:
        return 0.0
    
    return float(energy_delta) / time_delta


def getDayType(snapshots):
    """
    determine day's energy use profile based on historical data
    """

    dayType = ""
    
    return dayType
