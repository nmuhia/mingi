import datetime
import os

from operator import itemgetter

from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from django.shortcuts import render_to_response

from docraptor import DocRaptor

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.models import Error_Log
from mingi_django.timezones import gen_zone_aware
from mingi_site.models import Mingi_Site
from mingi_user.models import Mingi_User

from .pdf_report_functions import site_statistics, calculate_delta, \
    total_energy_use, year_long_site_statistics, get_top_site


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username',help="username passed here. rule: python manage.py username")
        parser.add_argument('-o',nargs=2,help='two potional aorguments. rule:  e.g. python manage.py get_pdf_report eon-admin monthly/annual (optional) ')

    def handle(self, *args, **options):
        """
        Email monthly/annual PDF reports to website users 

        e.g. python manage.py get_pdf_report eon-admin monthly/annual (optional)
        """
        username = None
        duration = "monthly"
        title = ""
        message = ""
        filename = ""
        message_content = ""
        today = gen_zone_aware(TIME_ZONE, datetime.datetime.today())
        year = today.strftime("%Y")

        username = options['username']
        if options['o']:
            duration = options['o'][0]
            year = options['o'][1]

        if username:
            users = User.objects.filter(username=username)
        else:
            self.stderr.write("You must specify a username!")
            return

        for user in users:
            sites = Mingi_Site.objects.filter(is_active=True, is_combined=False)

            if not user.is_staff:
                sites = sites.filter(owners=user)

            if duration == "monthly":
                first = today.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                end_date = first - datetime.timedelta(days=1)
                start_date = end_date.replace(day=1)

                year = start_date.strftime("%Y")
                month = start_date.strftime("%B")

                title = "Steama Report: "+str(month)+" "+str(year)
                try:
                    template_context = monthly_report(sites, month, year, start_date, end_date)
                except Exception, e:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message=str(e))
                    template_context = {}
                filename = str(user.username)+"_"+str(month)+"_"+str(year)+".pdf"
                message = render_to_response("monthly_pdf_template.html", template_context)
                message_content = "Dear %s,\n\nAttached is your %s Steama report for the sites in your portfolio.\n\nBest regards,\nthe SteamaCo Team" % (user.first_name, str(month))

            else:
                template_context = annual_report(sites, year, user)
                title = "Steama Annual Report: "+year
                message = render_to_response("annual_pdf_template.html", template_context)
                filename = str(user.username)+"_"+str(year)+".pdf"
                message_content = "Dear %s,\n\nAttached is your %s Steama report for the sites in your portfolio.\n\nBest regards,\nthe SteamaCo Team" % (user.first_name, year)

            recipient = user.email

            send_pdf(title, message, filename, message_content, recipient)


def monthly_report(sites, month, year, start_date, end_date):
    template_context = {}

    previous_end = start_date - datetime.timedelta(days=1)
    previous_start = previous_end.replace(day=1)

    current_total_revenue = 0
    previous_total_revenue = 0
    total_arpu = 0
    count = 1

    sites_list = []

    template_context['sites'] = []
    template_context['month'] = month
    template_context['year'] = year

    if len(sites) > 0:
        count = len(sites)

    for site in sites:
        site_dict = {
            'name': {},
            'currency': {},
            'data': {},
            'total_energy': {},
            'avg_hours': {},
            'time_on_percentage': {}
        }
        average_hours_before_top_up = 0
        time_off = 0
        time_on = 0
        total_time = 0
        time_on_percentage = 0

        site_dict['name'] = str(site.name)
        site_dict['currency'] = "USD"

        statistics = site_statistics(site, start_date, end_date)

        site_dict['data'] = statistics

        try:
            average_hours_before_top_up = float(statistics[13][1]) / float(statistics[13][0])
            print str(statistics[13][0])
        except ZeroDivisionError:
            average_hours_before_top_up = 0

        try:
            total_time = float((end_date - start_date).days) * 24
            print "total time: " + str(total_time)
            print str(statistics[15])
            time_off = float(statistics[13][1]) / float(statistics[15])
            print "time off: " + str(time_off)
            time_on = total_time - time_off
            print "time_on: " + str(time_on)
            time_on_percentage = int((time_on / total_time) * 100)
        except ZeroDivisionError:
            time_on_percentage = 0

        site_dict['avg_hours'] = "{0:,.2f}".format(average_hours_before_top_up)
        site_dict['time_on_percentage'] = str(time_on_percentage)
        site_dict['total_energy'] = "{0:.2f}".format(total_energy_use(site, start_date, end_date))

        current_total_revenue += float(statistics[3])
        previous_total_revenue += float(statistics[4])
        total_arpu += float(statistics[6])

        template_context['sites'].append(site_dict)

        site_info = []
        site_info.append(str(site.name))
        site_info.append(float(statistics[3]))
        site_info.append(float(statistics[3]) - float(statistics[4]))

        sites_list.append(site_info)

    top_site = sorted(sites_list, key=itemgetter(1), reverse=True)
    most_improved_site = sorted(sites_list, key=itemgetter(2), reverse=True)

    template_context['top_site'] = top_site[0][0]

    if most_improved_site[0][2] <= 0:
        template_context['most_improved'] = None
    else:
        template_context['most_improved'] = str(most_improved_site[0][0])

    template_context['current_total_revenue'] = "{0:,.2f}".format(current_total_revenue)
    print str(current_total_revenue)
    print template_context['current_total_revenue']
    template_context['previous_total_revenue'] = "{0:,.2f}".format(previous_total_revenue)
    print str(previous_total_revenue)
    print template_context['previous_total_revenue']
    print (template_context['current_total_revenue'] > template_context['previous_total_revenue'])
    template_context['average_arpu'] = "{0:,.2f}".format(float(total_arpu)/count)

    if previous_total_revenue == 0 or current_total_revenue == previous_total_revenue:
        template_context['delta_sign'] = "neutral"
    elif current_total_revenue > previous_total_revenue:
        template_context['delta_sign'] = "positive"
    else:
        template_context['delta_sign'] = "negative"

    # revenue delta
    revenue_delta = calculate_delta(current_total_revenue, previous_total_revenue)
    template_context['revenue_delta_percentage'] = "{0:,.2f}".format(revenue_delta)
    return template_context


def annual_report(sites, year, user):
    template_context = {}

    try:
        total_revenue = 0
        total_arpu = 0
        average_arpu = 0
        count = 1

        template_context = {}

        start_date = datetime.date(int(year), 1, 1)
        end_date = datetime.date(int(year), 12, 31)

        sites_list = list()
        payments_list = []
        num_customers = 0
                        
        for site in sites:
            site_dict = {}

            site_dict['name'] = str(site.name)                    
            site_dict['currency'] = "USD"

            statistics = year_long_site_statistics(site, start_date, end_date)
                    
            site_dict['data'] = statistics
            site_dict['revenue'] = float(statistics[0])
            site_dict['energy_use'] = float(statistics[2])

            total_revenue += float(statistics[0])
            num_customers += Mingi_User.objects.filter(site=site).exclude(line_number__exact="").exclude(line_number=None).count()
 
            sites_list.append(site_dict)
            
            if user.is_staff:
                payments = Kopo_Kopo_Message.objects.filter(timestamp__gte=start_date, timestamp__lte=end_date)
            else:
                payments = Kopo_Kopo_Message.objects.filter(timestamp__gte=start_date, timestamp__lte=end_date, user__site__owners=user)
            for i in range(0,31):
                if i == 30:
                    payments_list.append(payments.filter(timestamp__day=i+1).count() / 7)            
                else:
                    payments_list.append(payments.filter(timestamp__day=i+1).count() / 12)        

        sites_list.sort(key=itemgetter('revenue'), reverse=True)
        top_site = get_top_site(sites, start_date, end_date)

        template_context['year'] = year
        template_context['total_revenue'] = "{0:,.2f}".format(total_revenue)
        template_context['average_arpu'] = "{0:,.2f}".format((total_revenue / 12) / num_customers)        
        template_context['top_earning_site'] = top_site[0][0]
        template_context['best_performing_site'] = top_site[1][0]
        template_context['payments'] = payments_list
        template_context['min_axis'] = min(payments_list)
        template_context['sites'] = sites_list

        return template_context
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))
        print e

def send_pdf(title, message, filename, message_content, recipient):
    try:
        docraptor = DocRaptor(settings.DOCRAPTOR_API_KEY)
        with open(filename, "wb") as f:
            os.chmod(filename,0777)                    
            f.write(docraptor.create({
                'document_content': message,
                'javascript':True,
                'test': False,
            }).content)

        print "Recipient: " + str(recipient)
        print "title: " + str(title)
        email = EmailMessage(title,message_content,to=[recipient])
        email.attach_file(filename)
        email.send()
        print "email sent"
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))
        print e
