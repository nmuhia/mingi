from django.core.management.base import BaseCommand

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Daily_Power, Hourly_Site_Revenue, Hourly_Site_Power, Line_State
from africas_talking.models import Africas_Talking_Input

from mingi_django.models import Error_Log
from mingi_user.user_energy_usage import js_timestamp
from mingi_user.models import Mingi_User

from operator import itemgetter
from django.core.mail import EmailMessage

import csv
import datetime
import pytz
from pytz import utc

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware
from django.utils import timezone


class Command(BaseCommand):
    def handle(self, *args, **options):
	"""
        Run any of a series of statistical calculations (defined in this file)
        which are output as csv files
        """
        
        # set up email
        email = EmailMessage('Vulcan Stats Data', 'data attached', to=["emily@steama.co"])
        
        # assume (for now) that data should only come from Vulcan sites
        sites = Mingi_Site.objects.filter(company_name="Vulcan")

        # sanity check
        for site in sites:
            print site.name

        # collect data
        payments = Kopo_Kopo_Message.objects.filter(user__site__company_name="Vulcan").order_by('timestamp')
        hourly_power = Hourly_Site_Power.objects.filter(site__company_name="Vulcan").order_by('timestamp')
        hourly_revenue = Hourly_Site_Revenue.objects.filter(site__company_name="Vulcan").order_by('timestamp')
        daily_power = Daily_Power.objects.filter(user__isnull=False, user__site__company_name="Vulcan",timestamp__gte=gen_zone_aware(TIME_ZONE,datetime.datetime(2015,1,1,0,0,0,0)))
        line_states = Line_State.objects.filter(user__isnull=False, user__site__company_name="Vulcan",timestamp__gte=gen_zone_aware(TIME_ZONE,datetime.datetime(2015,3,1,0,0,0,0)),timestamp__lt=gen_zone_aware(TIME_ZONE,datetime.datetime(2015,6,1,0,0,0,0)),account_balance__isnull=False)
        sms = Africas_Talking_Input.objects.filter(user__isnull=False,user__site__company_name="Vulcan",timestamp__gte=gen_zone_aware(TIME_ZONE,datetime.datetime(2015,3,1,0,0,0,0)),timestamp__lt=gen_zone_aware(TIME_ZONE,datetime.datetime(2015,6,1,0,0,0,0)))

        # daily totals timeseries (comment/uncomment as desired)
        filename = write_total_use_timeseries(hourly_power)
        email.attach_file(filename)

        # revenue totals timeseries (comment/uncomment as desired)
        #filename = write_total_revenue_timeseries(hourly_revenue)
        #email.attach_file(filename)

        # average payment timeseries (comment/uncomment as desired)
        #filename = write_payment_amt_timeseries(payments)
        #email.attach_file(filename)

        # payment amt vs. revenues (comment/uncomment as desired)
        #filename = write_payment_comparison(payments, sites)
        #email.attach_file(filename)

        # daily power averages (commend/uncomment as desired)
        #filename = write_daily_power_analysis(daily_power, sites)
        #email.attach_file(filename)

        # site's standard deviation of energy use (overall and monthly)
        #filename = write_energy_use_std(hourly_power, sites)
        #email.attach_file(filename)

        # user comms history
        #filename = write_comms_history(sms, sites)
        #email.attach_file(filename)

        print str(len(line_states))
        print "SMS FILE ATTACHED"

        # user power-off history
        #filename = write_power_history_analysis(line_states, sites)
        #email.attach_file(filename)
        
        email.send()

        return


def write_total_use_timeseries(power_data_set):
    """
    return a csv file which can be used to plot total energy use of 
    input sites over time
    """
    
    filename = 'energy_use.csv'
    
    ts = power_data_set[0].timestamp
    this_date = datetime.date(ts.year,ts.month,ts.day+1)
    today = datetime.date.today()
    end_date = datetime.date(today.year,today.month,1) - datetime.timedelta(days=1)

    print str(this_date) + " AND " + str(end_date) + " AND " + str(power_data_set[len(power_data_set)-1].timestamp)
    
    write_data_list = list()
    
    index = 0
    
    # iterate through all possible dates
    while this_date <= end_date:
        print str(this_date)
        this_total = 0
        this_datetime = datetime.datetime(this_date.year,this_date.month,this_date.day,0,0,0,0,tzinfo=pytz.timezone(TIME_ZONE))

        # iterate through all possible entries on that date
        while power_data_set[index].timestamp < this_datetime:

            this_total += power_data_set[index].amount
            index += 1
            
        write_data_list.append([str(this_date),str(this_total)])
        this_date = this_date + datetime.timedelta(days=1)

    print "power data compiled"
        

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        writer.writerow(['Timestamp','Total (kWh)'])
        for row in write_data_list:
            writer.writerow(row)
            
    return filename


def write_total_revenue_timeseries(revenue_data_set):
    """
    return a csv file which can be used to plot total revenues of 
    input sites over time
    """
    
    filename = 'revenues.csv'
    
    ts = revenue_data_set[0].timestamp
    this_date = datetime.date(ts.year,ts.month,ts.day+1)
    end_date = datetime.date.today() - datetime.timedelta(days=1)
    print str(this_date) + " AND " + str(end_date) + " AND " + str(revenue_data_set[len(revenue_data_set)-1].timestamp)
    
    write_data_list = list()
    
    index = 0
    
    # iterate through all possible dates
    while this_date <= end_date:
        this_total = 0
        this_datetime = datetime.datetime(this_date.year,this_date.month,this_date.day,0,0,0,0,tzinfo=pytz.timezone(TIME_ZONE))
        
        # iterate through all possible entries on that date
        while revenue_data_set[index].timestamp < this_datetime:
            this_total += revenue_data_set[index].amount
            index += 1
            
        write_data_list.append([str(this_date),str(this_total)])
        this_date = this_date + datetime.timedelta(days=1)
        
    print "revenue data compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        writer.writerow(['Timestamp','Total (KES)'])
        for row in write_data_list:
            writer.writerow(row)
            
    return filename


def write_payment_amt_timeseries(payment_data_set):
    """
    return a csv file which can be used to plot average top-up amount of 
    input sites over time
    """
    
    filename = 'topups.csv'
    
    ts = payment_data_set[0].timestamp
    this_date = datetime.date(ts.year,ts.month,1)
    end_date = datetime.date(datetime.date.today().year,datetime.date.today().month,1) - datetime.timedelta(days=1)

    print str(this_date) + " AND " + str(end_date) + " AND " + str(payment_data_set[len(payment_data_set)-1].timestamp)
    
    write_data_list = list()
    
    index = 0
    print "max index: " + str(len(payment_data_set))
    
    # iterate through all possible dates
    while this_date <= end_date:
        print str(this_date) + " <= " + str(end_date)
        this_total = 0
        count = 0
        this_datetime = datetime.datetime(this_date.year,this_date.month,1,0,0,0,0,tzinfo=pytz.timezone(TIME_ZONE))
        
        
        # iterate through all possible entries on that date
        while payment_data_set[index].timestamp.month == this_datetime.month:
            this_total += payment_data_set[index].amount
            count += 1
            index += 1
            
        try:
            average = this_total / count
        except ZeroDivisionError:
            average = 0.0

        write_data_list.append([str(this_date),str(average)])
        # will fail over very long time ranges
        next_date = this_date + datetime.timedelta(days=31)
        this_date = datetime.date(next_date.year,next_date.month,1)
        
    print "payment data compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        writer.writerow(['Timestamp','Average Top-Up (KES)'])
        for row in write_data_list:
            writer.writerow(row)
            
    return filename


def write_payment_comparison(payment_data_set, sites):
    """
    return a csv file which can be used to plot average top-up amount vs.
    total revenues each month at each site
    """
    
    filename = 'topup-revenues.csv'
    
    write_data_list = [["Timestamp"]]
    site_index = 1
    
    # perform monthly iteration for each site
    for site in sites:
        write_data_list.append([site.name + ": Total"])
        write_data_list.append([site.name + ": Average"])
        site_payments = payment_data_set.filter(user__site=site)
        payment_index = 0

        ts = payment_data_set[0].timestamp
        this_date = datetime.date(ts.year,ts.month,1)
        end_date = datetime.date(datetime.date.today().year,datetime.date.today().month,1) - datetime.timedelta(days=1)
            
        # iterate through all possible dates
        while this_date <= end_date:
            pay_total = 0
            count = 0
            this_datetime = datetime.datetime(this_date.year,this_date.month,1,0,0,0,0,tzinfo=pytz.timezone(TIME_ZONE))

            if site_index == 1:
                write_data_list[0].append(str(this_date))        
        
            # iterate through all possible entries on that date
            while site_payments[payment_index].timestamp.month == this_datetime.month:
                pay_total += site_payments[payment_index].amount
                count += 1
                payment_index += 1
            
            try:
                average = pay_total / count
            except ZeroDivisionError:
                average = 0.0

            write_data_list[site_index].append(str(pay_total))
            write_data_list[site_index+1].append(str(average))
            next_date = this_date + datetime.timedelta(days=31) # inc. by month
            this_date = datetime.date(next_date.year,next_date.month,1)

        site_index += 2 # increment index of data array (2 columns / site)
        
    print "payment data compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        for row in write_data_list:
            writer.writerow(row)
            
    return filename


def write_energy_use_std(power_data_set, sites):
    """
    return a csv file which can be used to plot standard deviation of
    energy use each month at each site
    """
    
    filename = 'energy-std.csv'
    
    write_data_list = [["Timestamp"]]
    stdev_list = ["Std. Dev. of Energy Use Totals"]
    site_index = 1
    
    for site in sites:
        ts = power_data_set[0].timestamp
        this_date = datetime.date(ts.year,ts.month,ts.day+1)
        today =datetime.date.today()
    
        write_data_list.append([site.name + ": Total"])
        print str(write_data_list)
        site_power = power_data_set.filter(site=site)
        last_date = site_power[len(site_power)-1].timestamp
        end_date = datetime.date(last_date.year,last_date.month,last_date.day) - datetime.timedelta(days=1)
        power_index = 0
        this_total = 0.0
            
        # iterate through all possible dates
        while this_date <= end_date:
            print str(this_date)
            this_total = 0.0
            this_datetime = datetime.datetime(this_date.year,this_date.month,this_date.day,0,0,0,0,tzinfo=pytz.timezone(TIME_ZONE))

            if site_index == 1:
                write_data_list[0].append(str(this_date))
        
            # iterate through all possible entries on that date
            while site_power[power_index].timestamp < this_datetime:
                this_total += float(site_power[power_index].amount)
                power_index += 1
            print "Total: " + str(this_total)
            write_data_list[site_index].append(float(this_total))
            next_date = this_date + datetime.timedelta(days=1) # inc. by day
            this_date = datetime.date(next_date.year,next_date.month,next_date.day)

        # calculate and store standard deviation
        print "Getting data set..."
        std_data = write_data_list[site_index][1:]
        print str(std_data)
        mean_power = sum(std_data) / len(std_data)
        deviations_sum = sum((data - mean_power) ** 2 for data in std_data)
        std_point = (deviations_sum / len(std_data)) ** 0.5
        stdev_list.append(std_point)
        site_index += 1
        
    print "energy totals and stdev compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        for row in write_data_list:
            writer.writerow(row)
        writer.writerow(stdev_list)
            
    return filename


def write_daily_power_analysis(power_data_set, sites):
    """
    return a csv file which can be used to plot standard deviation of
    energy use each month at each site
    """
    
    filename = 'energy_avgs.csv'
    print str(len(power_data_set))
    
    write_data_list = [["Site Name","User ID","User Total","Avg. Daily Energy Use (kWh)"]]
    avg_list = ["Average Energy Use Totals"]
    # row_index = 0
    
    for site in sites:
        
        # set up write_data_list
        users = Mingi_User.objects.filter(site=site, line_number__isnull=False).exclude(line_number='')
        user_data_list = list()
        for user in users:
            user_data_list.append([user, 0.0, 0])

        # sort rollups and total data
        site_power = power_data_set.filter(user__site=site)
        print site.name + (": %d RECORDS" % len(site_power))
        for rollup in site_power:
            this_user = rollup.user

            for i in range(0,len(user_data_list)):
                if user_data_list[i][0] == this_user:
                    user_data_list[i][1] += float(rollup.amount)
                    user_data_list[i][2] += 1
                    
        # add totals to write_data_list
        for row in user_data_list:
            print "ROW: " + str(row)
            if row[2] == 0:
                write_data_list.append([row[0].site.name,str(row[0].id),str(row[1]),"0.0"])
            else:
                write_data_list.append([row[0].site.name,str(row[0].id),str(row[1]),str(row[1]/row[2])])
                print "TOTAL: " + str((row[1]/row[2]))
        
    print "writing data list..."

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write data
        for row in write_data_list:
            writer.writerow(row)
            
    return filename


def write_comms_history(sms_logs, sites):

    filename = "comms_history.csv"
    print str(len(sms_logs))
    
    write_data_list = [["Site Name","User ID","Message Total"]]
    avg_list = []
    
    for site in sites:

        print site.name
        # set up write_data_list
        users = Mingi_User.objects.filter(site=site, line_number__isnull=False).exclude(line_number='')
        user_data_list = list()
        for user in users:
            user_data_list.append([user, 0])

        # sort rollups and total data
        site_sms = sms_logs.filter(user__site=site)

        for sms in site_sms:
            for row in user_data_list:
                if row[0] == sms.user:
                    row[1] += 1


        for row in user_data_list:
            write_data_list.append([site.name,str(row[0]),str(row[1])])

    print "sms data compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
            
        for row in write_data_list:
            writer.writerow(row)

    return filename
        

def write_power_history_analysis(line_states, sites):
    
    print "GOT INTO FUNCTION"
    filename = "power_history.csv"
    print str(len(line_states))
    
    write_data_list = [["Site Name","User ID","Number of Power-Off Events","Total Power-Off Time (hrs)"]]
    avg_list = []
    
    for site in sites:
        
        # set up write_data_list
        users = Mingi_User.objects.filter(site=site, line_number__isnull=False).exclude(line_number='')
        user_data_list = list()
        index = 0
        for user in users:
            user_data_list.append([user, 0, 0.0])

            user_states = line_states.filter(user=user).order_by('timestamp')
            line_is_on = True
            power_off_time = None
            last_state = None

            # note relevant info each time a line is just turned off or just turned on
            for state in user_states:
                if state.account_balance <= 0 and line_is_on == True:
                    print str(state.timestamp) + ": " + str(state.account_balance)
                    line_is_on = False
                    power_off_time = state.timestamp
                    user_data_list[index][1] += 1

                if state.account_balance > 0 and line_is_on == False:
                    line_is_on = True
                    total_off_time = float((state.timestamp - power_off_time).days) * 24.0 + float((state.timestamp - power_off_time).seconds) / 3600.0
                    print str(state.timestamp) + ": " + str(total_off_time) + " (" + str(state.account_balance) + ")"
                    user_data_list[index][2] += total_off_time

                last_state = state


            # special case: handle last line-state during power-off interval
            if line_is_on == False:
                if power_off_time:
                    total_off_time = float((state.timestamp - power_off_time).days) * 24.0 + float((last_state.timestamp - power_off_time).seconds) / 3600.0
                    print "Special case: " + str(power_off_time)
                    print str(state.timestamp) + ": " + str(total_off_time)
                else:
                    total_off_time = 0.0
                user_data_list[index][2] += total_off_time

            print str(user_data_list[index])

            index += 1

        for row in user_data_list:
            write_data_list.append([site.name,str(row[0]),str(row[1]),str(row[2])])

    print "power history data compiled"

    # write each line of the data list into csv format
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
            
        for row in write_data_list:
            writer.writerow(row)

    return filename
