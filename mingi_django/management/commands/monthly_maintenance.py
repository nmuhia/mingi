import datetime

from mingi_user.models import Mingi_User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments here"
    
    def handle(self, *args, **options):
        """
        run through monthly maintenance functions on the first of each month
        """

        today = datetime.date.today()

        # only run functions on the first of the month
        if today.day != 1:
            return

        # zero out utility costs
        zero_utility_costs()

        # deduct monthly fee from monthly rate customers
        charge_monthly_rate()


def zero_utility_costs():
    """
    reset the account balances for the relevant utility customers
    """

    return


def charge_monthly_rate():
    """
    deduct a monthly rate from customers on a Monthly Rate payment plan
    """

    all_customers = Mingi_User.objects.filter(payment_plan__contains="Monthly Rate", line_number__isnull=False)

    for customer in customers:
        
        try:
            rate = float(customer.payment_plan.split(":")[1].strip())
            customer.account_balance = customer.account_balance - rate
            customer.save()

        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Error while charging customer %s: %s" % (str(customer), str(e)))
