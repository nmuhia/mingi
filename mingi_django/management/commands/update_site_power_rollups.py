import datetime

from mingi_site.models import Mingi_Site

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from mingi_user.methods import get_utility_lists, get_site_utility_lists

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o',nargs=1,help="python manage.py update_site_power_rollups -o DAYS")

    def handle(self, *args, **options):
        """
        Updates the hourly power rollups for every site for the past week.
        """

        if options['o']:
            DAYS = int(options['o'][0])
        else:
            DAYS = 1
        
        now_timestamp = gen_zone_aware(TIME_ZONE,now())
        days_ago_timestamp = now_timestamp - datetime.timedelta(days=DAYS)

        sites = Mingi_Site.objects.filter(is_active=True, is_combined=False)

        for site in sites:
            print "STARTING SITE: " + site.name
            rollup = get_site_utility_lists(site)
            print "created rollup..."
            print site.energy_range(days_ago_timestamp, now_timestamp, rollup,
                                    True)
            
