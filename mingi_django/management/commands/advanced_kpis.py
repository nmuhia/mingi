from django.core.management.base import BaseCommand

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Daily_Power, Hourly_Site_Revenue, Hourly_Site_Power, Line_State, Daily_Message_Count
from africas_talking.models import Africas_Talking_Input

from mingi_django.models import Error_Log
from mingi_user.user_energy_usage import js_timestamp
from mingi_user.models import Mingi_User
from mingi_django.dashboard import convert_kes_usd, convert_tzs_usd

from operator import itemgetter
from django.core.mail import EmailMessage

import csv
import datetime
import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware



class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments here"
        
    def handle(self, *args, **options):
        """
        calculate internal KPIs for all sites (investor due diligence Nov 2015)
        """

        # edit to set dates to desired range
        # *** assumes start and end dates are within the same year
        start = gen_zone_aware(TIME_ZONE,datetime.datetime(2015,1,1,0,0,0,0)) # start Jan 2015
        end = gen_zone_aware(TIME_ZONE,datetime.datetime(2015,11,1,0,0,0,0)) # through Oct 2015

        all_sites = Mingi_Site.objects.filter(is_active=True).exclude(company_name="Wells Fargo").exclude(is_combined=True)
        vulcan_sites = all_sites.filter(company_name="Vulcan")
        rw_sites = all_sites.filter(company_name="Renewable World")
        sc_sites = all_sites.filter(company_name="SteamaCo")
        other_sites = all_sites.exclude(company_name="Vulcan").exclude(company_name="Renewable World").exclude(company_name="SteamaCo")

        vulcan_impact = vulcan_sites.exclude(name__contains="Barsaloi").exclude(name__contains="Entesopia").exclude(name__contains="Enkoireroi").exclude(name__contains="Olenarau")
        all_commercial = all_sites.exclude(company_name="Renewable World")
        all_impact = list()
        print "IMPACT SITES"
        for site in rw_sites:
            all_impact.append(site)
            print site.name
        for site in vulcan_impact:
            all_commercial = all_commercial.exclude(name=site.name)
            all_impact.append(site)
            # sanity check
            print site.name
        print "COMMERCIAL SITES"
        for site in all_commercial:
            print site.name

        # get user totals
        print "getting total users"
        total_user_ts = get_user_totals(all_sites, start, end)
        print "getting user subset"
        subset_user_ts = get_user_totals(other_sites, start, end)
        print "getting sms costs"
        sms_cost_ts = get_sms_costs(all_sites, start, end)
        print "getting commercial arpu"
        commercial_arpu_ts = get_arpu(all_commercial, start, end)
        print "getting impact arpu"
        impact_arpu_ts = get_arpu(all_impact, start, end)


def get_user_totals(sites, start, end):
    """
    return user totals by month at each site
    """

    this_start = start
    
    while this_start < end:
        this_end = gen_zone_aware(TIME_ZONE,datetime.datetime(this_start.year,this_start.month+1,1,0,0,0,0))
        print str(this_start)
        total = 0

        for site in sites:
            total += len(Mingi_User.objects.filter(site=site,created__lt=this_end).exclude(line_number=""))

        print str(total)
        this_start = this_end
    

def get_sms_costs(sites, start, end):
    """
    return total sms costs by month
    """

    this_start = datetime.date(start.year,start.month,start.day)
    end = datetime.date(end.year,end.month,end.day)
    
    while this_start < end:
        this_end = datetime.date(this_start.year,this_start.month+1,1)
        print str(this_start)
        total = 0

        for site in sites:
            message_counts = Daily_Message_Count.objects.filter(site=site,date__gte=this_start,date__lt=this_end)
            for count in message_counts:
                if site.company_name == "E.ON":
                    total += (count.customers_out + count.bh_out)*1.1 + (count.bh_in)*0.1
                else:
                    total += count.customers_out + count.bh_out
        print str(total)
        this_start = this_end


def get_arpu(sites, start, end):
    """
    return ARPU for given sites by month (in USD)
    """

    this_start = start
    
    while this_start < end:
        this_end = gen_zone_aware(TIME_ZONE,datetime.datetime(this_start.year,this_start.month+1,1,0,0,0,0))
        print str(this_start)
        revenue_total = 0
        user_total = 0
        TSH_EXCHANGE = convert_tzs_usd()
        KES_EXCHANGE = convert_kes_usd()

        for site in sites:
            payments = Kopo_Kopo_Message.objects.filter(user__site=site,timestamp__gte=this_start,timestamp__lt=this_end)
            for log in payments:
                amount = log.amount
                if site.company_name=="E.ON":
                    amount = float(amount) * float(TSH_EXCHANGE)
                else:
                    amount = float(amount) * float(KES_EXCHANGE)
                revenue_total += amount

            user_total += len(Mingi_User.objects.filter(site=site,created__lt=this_end).exclude(line_number=""))
        try:
            arpu = float(revenue_total) / float(user_total)
        except ZeroDivisionError:
            arpu = 0.0
        print str(revenue_total) + " and " + str(user_total)
        print str(arpu)
        this_start = this_end
