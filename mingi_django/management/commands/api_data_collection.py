from django.core.management.base import BaseCommand

from africas_talking.BBOXX_API import collect_BBOXX_data
from mingi_django.timezones import gen_zone_aware
from mingi_django.mingi_settings.base import TIME_ZONE

import datetime
import requests


class Command(BaseCommand):
    def add_arguments(self, parser):
        """
        arguments: date range is optional
        """

        parser.add_argument('-o',nargs=2,help='optional start / end date')
        

    def handle(self, *args, **options):
        """
        collect periodic data from the BBOXX API
        """

        if options['o']:
            start_date = options['o'][0]
            end_date = options['o'][1]
                        
        else:
            now = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
            end_date = datetime.datetime(now.year,now.month,now.day,now.hour,0,0,0)
            start_date = end_date - datetime.timedelta(hours=4)

        collect_BBOXX_data(str(start_date), str(end_date))
