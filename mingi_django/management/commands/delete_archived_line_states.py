from django.core.management.base import BaseCommand

from mingi_site.models import Line_State, Line_State_Archive


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments"
    
    def handle(self, *args, **options):
        """
        Delete all Line_State objects that are already covered by a
        Line_State_Archive. This should be a one-off operation, since the
        delete normally happens when the archive is created.
        """
        for line_state_archive in Line_State_Archive.objects.order_by('date'):
            print str(line_state_archive.date) + ": delete started"
            Line_State.objects.filter(
                timestamp__year=line_state_archive.date.year,
                timestamp__month=line_state_archive.date.month,
                timestamp__day=line_state_archive.date.day
            ).delete()
            print str(line_state_archive.date) + ": delete finished"
