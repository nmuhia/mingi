from mingi_site.models import Mingi_Site

from django.core.management.base import BaseCommand, CommandError
import datetime
from django.utils.timezone import now

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o',nargs=1,help="python manage.py update_site_revenue_rollups -o DAYS")

    def handle(self, *args, **options):
        """
        Updates the hourly revenue rollups for every site. Accomplishes this
        with a slight hack: by calling revenue_to_date() on every site.
        """

        if options['o']:
            DAYS = int(options['o'][0])
        else:
            DAYS = 1

        now_timestamp = gen_zone_aware(TIME_ZONE,now())
        days_ago_timestamp = now_timestamp - datetime.timedelta(days=DAYS)

        for site in Mingi_Site.objects.all():
            print site.revenue_range(days_ago_timestamp, now_timestamp)
