# import datetime
from datetime import datetime, timedelta #, tzinfo
import datetime

import csv
import math
import pytz
from django.db.models import Q
from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
import sys


from mingi_site.models import Mingi_Site,Hourly_Site_Power,Daily_Power

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from mingi_django.tasks import daily_power_rollups,power_rollups_site

import sys
import pprint

import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

ROLLUP_STATUS_FILENAME = "rollups_status"+str(datetime.datetime.now().date())+".csv"


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here -- all optional"
        parser.add_argument('-o',nargs=1,help='Format : manage.py check_rollups_status [hours], [hours, date], [from_date,to_date]')
        parser.add_argument('-o1',nargs=1,help='Format : manage.py check_rollups_status [hours], [hours, date], [from_date,to_date]')

    def handle(self, *args, **options):
        """
        Check the state of power rollups within given period
        Default = 3 hours
        Format : manage.py check_rollups_status [hours], [hours, date], [from_date,to_date]
        """
        HOURLY_ROLLUPS_CHECK = True # Check whether to check hourly or daily rollups
        data_list = list() # Hold data for sites, hours, days when rollups are missing
        TODAY = gen_zone_aware(TIME_ZONE,now())
        title = "Mingi Rollups Status Report"
        filename = ROLLUP_STATUS_FILENAME

        if options['o'] and not options['o1']:
            try:
                HOURS = int(options['o'][0])
            except ValueError:
                print "Enter a whole number"
                sys.exit()

        if options['o1']:
            try:
                HOURS = int(options['o1'][0])
            except ValueError:
                HOURLY_ROLLUPS_CHECK = False

        # if len(args) == 1:
        #     try:
        #         HOURS = int(args[0])
        #     except ValueError:
        #         print "Enter a whole number"
        #         sys.exit()
        # if len(args) == 2:
        #     try:
        #         HOURS = int(args[0])
        #     except ValueError:
        #         HOURLY_ROLLUPS_CHECK = False

        print "Generating report ..."
        nowtime= gen_zone_aware(TIME_ZONE,datetime.datetime.now())
        # Check for hourly rollups
        if HOURLY_ROLLUPS_CHECK :            
            if options['o1']:
                HOURS = int(options['o'][0])
                DATES = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(options['o1'][0], '%d-%m-%y'))
            elif options['o']:
                HOURS = int(options['o'][0])
                DATES = nowtime.replace(minute=0,second=0,microsecond=0)
            else:
                HOURS = 3
                DATES = nowtime.replace(minute=0,second=0,microsecond=0)

            # if len(args) == 2:
            #     HOURS = int(args[0])
            #     DATES = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(args[1], '%d-%m-%y'))
            # elif len(args) == 1:
            #     HOURS = int(args[0])
            #     DATES = nowtime.replace(minute=0,second=0,microsecond=0)
            # else:
            #     HOURS = 3
            #     DATES = nowtime.replace(minute=0,second=0,microsecond=0)

            # Check hourly power rollups
            that_hour = (DATES - timedelta(hours = HOURS)).replace(minute=0,second=0,microsecond=0)

       
            # get all hourly rollups in range
            hourly_rollups = Hourly_Site_Power.objects.filter(timestamp__gte=that_hour,timestamp__lt=DATES).order_by('timestamp')

            # Get all site
            all_sites = Mingi_Site.objects.all()
            
            # Check through all rollups
            for site in all_sites:

                this_site_rollups = hourly_rollups.filter(site=site)

                loop_hour = that_hour
                # Check hourly rollups for given site
                for i in range(0,HOURS):
                    if len(this_site_rollups.filter(timestamp=loop_hour)) == 0:
                        # If no rollups for that hour, flag it
                        flag = [str(site),str(loop_hour)]
                        data_list.append(flag)
                    
                    loop_hour = loop_hour + timedelta(hours=1)
            
            

        # Check for daily rollups
        else:
            DATE1 = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(options['o'][0], '%d-%m-%y'))
            DATE2 = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(options['o1'][0], '%d-%m-%y'))

            # Fetch all daily rollups within range
            daily_rollups = Daily_Power.objects.filter(timestamp__gte=DATE1,timestamp__lt=DATE2, site__isnull=False).order_by('timestamp')
            for site in Mingi_Site.objects.all():
                this_site_rollups = daily_rollups.filter(site=site)

                loop_date1 = DATE1
                loop_date2 = DATE2


                while loop_date1 <= loop_date2 :
                    if (len(this_site_rollups.filter(timestamp=loop_date1))==0):
                        flag = [str(site),str(loop_date1)]
                        data_list.append(flag)  
                    loop_date1 = loop_date1 + timedelta(days=1)  




            
        # If no data all rollups exist, don't send reports
        if len(data_list)==0:
            sys.exit()

        if HOURLY_ROLLUPS_CHECK:
            # Create hourly rollups
            # Hourly rollups will create logs
            print "Creating hourly rollups ..."
            for sites in all_sites:
                power_rollups_site(sites.id,
                                   TODAY - datetime.timedelta(hours=HOURS),
                                   TODAY)

        else:
            # Create daily rollups
            # Daily rollups will create logs 
            print "Creating daily rollups ..."
            while DATE1 < DATE2 :
                print "Creating rollup for: " + str(DATE1)
                daily_power_rollups(DATE1)
                DATE1 = DATE1 + datetime.timedelta(days=1)
