from django.core.management.base import BaseCommand

from mingi_site.models import Mingi_Site, Daily_Power
from mingi_user.models import Mingi_User
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log

from report_functions import get_header, get_email_subject
from mingi_django.dashboard import convert_kes_usd, convert_tzs_usd

from django.core.mail import EmailMessage
from datetime import datetime, timedelta, date
import csv
from django.core.files import File

import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

SITE_USER_FILENAME = "site_user_report.csv"

class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here. arguments can only be three or four"
        parser.add_argument('start_string',help="")
        parser.add_argument('end_string',help="")
        parser.add_argument('email',help="")
        parser.add_argument('site_name',help="")

    def handle(self, *args, **options):
        """
        generate site user report and email to specified address
        """

        filename = SITE_USER_FILENAME
        title = "Site User Report"

        start_string = options['start_string']
        end_string = options['end_string']
        
        start_datetime = gen_zone_aware(TIME_ZONE,datetime.strptime(start_string, '%d-%m-%y'))
        # include end date in filter by getting data less than the day after
        end_datetime = gen_zone_aware(TIME_ZONE,datetime.strptime(end_string,'%d-%m-%y')) + timedelta(days=1)
        start_date = date(start_datetime.year,start_datetime.month,start_datetime.day)
        end_date = date(end_datetime.year,end_datetime.month,end_datetime.day)
        email = options['email']
        site_name = options['site_name']

        try:
            site = Mingi_Site.objects.get(name=site_name)
        except Mingi_Site.DoesNotExist:
            return "Error: Site Does Not Exist"
        except Mingi_Site.MultipleObjectsReturned:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Error: Multiple matches found for site name: %s" % site_name)
            return "Error: Multiple Matches Found"

        # reset start date for accurate averages
        if start_date < site.date_online:
            start_date = site.date_online

        days = (end_date - start_date).days

        # get users and store 
        users = Mingi_User.objects.filter(site__name=site_name,
                                          line_number__isnull=False)
        
        user_list = list()
        payment_list = list()

        for user in users:
            user_list.append([user])            
            
        index = 0
        for row in user_list:
            user = row[0]
            payments = Kopo_Kopo_Message.objects.filter(user=user,timestamp__gte=start_datetime,timestamp__lt=end_datetime)
            total = 0.0
            
            for payment in payments:
                total += float(payment.amount)
            
            user_list[index].append(total)
            index += 1

        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(title, start_date, end_date)

            for row in header:
                writer.writerow(row)

            writer.writerow([""])
            writer.writerow(['User','Line Number','Total Revenues','Avg Daily Revenue','Avg. Weekly Revenue'])
                
            for row in user_list:
                print str(row)
                user = row[0]
                total = row[1]
                day_avg = total / days
                week_avg = day_avg * 7
                writer.writerow([str(user),user.line_number,total,day_avg,week_avg])

        # compile email and send
        subject = get_email_subject(title, start_date, end_date)
        email = EmailMessage(subject,'Your data is attached',to=[email])
        email.attach_file(filename)
        email.send()
