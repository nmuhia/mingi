from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage
from africas_talking.sendSMS import sendSMS
from mingi_django.models import Error_Log, Timer_Command,Custom_Timer_Command, Utility
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Site_Record, Daily_Power, Line

from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives

import datetime
from datetime import date, timedelta, datetime
from africas_talking.bhResponse import lineStatusMaintenance
from mingi_django.models import TRIGGER_LEVELS,TRIGGER_TYPE,USER_TYPE, Custom_Timer_Command, Error_Log
from mingi_user.methods import get_site_utility_lists

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no add_arguments passed"

    def handle(self, *args, **options):
        """
        run timer commands if they are scheduled for this hour
        """

        # check line statuses
        # sendLineStatusMsgs()

        # # run commands
        # commands = Timer_Command.objects.all()
        # current_hour = datetime.now().hour

        # for command in commands:
        #     hours = command.hour_array
            
        #     for hour in hours:
        #         if (int(hour) == current_hour):
        #             try:
        #                 sendSMS(command.destination_phone,command.message)
        #             except Exception as e:
        #                 Error_Log.objects.create(problem_type="SEND_SMS",
        #                                          problem_message=str(e))
        
        # Run custom trigger commands
        # Default time range is 1 day i.e. Today

        """site_usage_list = list()

        # Construct email
        site_usage_list_email = ''"""

        end_date = gen_zone_aware(TIME_ZONE,datetime.now())
        start_date = end_date - timedelta(days=1)

        current_hour = end_date.hour

        all_trigger_commands = Custom_Timer_Command.objects.filter(hours__isnull=False)
        all_trigger_commands = all_trigger_commands.exclude(hours="")

        print "timercommands"
        print "timercommands"
        print "timercommands"
        return

        for command in all_trigger_commands:
            hours = command.hour_array
            for hour in hours:
                if int(hour) == current_hour:
                    if command.trigger_type == "temp":
                        checkTemperature(command,hour)

                    elif command.trigger_type == "voltage":
                        checkVoltage(command,hour)

                    elif command.trigger_type == "utility_use":
                        checkUtilityUse(command,hour)
                    elif command.trigger_type == "account_balance":
                        checkAccountBalance(command,hour)
                    else:
                        pass

        """for command in all_trigger_commands:
            hours = command.hour_array
            for hour in hours:
                print "hour"
                if int(hour) == current_hour:
                    if command.trigger_type=='utility_usage':
                        # Check   utility usage
                        print 'Utility usage'
                        message = checkTimerUtilityUsage(command,hour)

                    elif command.trigger_type=='send_sms':
                        # Ssend SMS
                        print 'Send SMS'
                        sendTimerSMS(command,hour)
                        message = False
                    elif command.trigger_type=='cost':
                        # Check cost usage
                        print 'Check cost usage'
                        message = False

                    # Send email
                    if message:
                        # Email message
                        # Send HTML Mail
                        print "sending email ... "  
                        print command.destination
                        subject, from_email, to = 'Custom Timer Command Report', 'app@steama.co', command.destination
                        html_content = message
                        msg = EmailMultiAlternatives(subject, '', from_email, [to])
                        msg.attach_alternative(html_content, "text/html") 
                        try:
                            msg.send()
                            print "send"
                        except Exception as e:
                            print e
                            pass
        return"""

def checkUtilityUse(command, hour):
    send_utilityuse_message = False

    end_date = gen_zone_aware(TIME_ZONE,datetime.now())
    start_date = end_date - timedelta(days=1)

    send_utilityuse_message = getUtilityUse(command, start_date, end_date)

    if send_utilityuse_message:
        send_trigger_alert(command,"Utility Use: ")


def getUtilityUse(command, start_datetime, end_datetime):
    sendMessage = False
    trigger_level = command.trigger_level
    threshold = command.threshold
    site = command.site

    start_datetime = start_datetime.replace(hour=0,minute=0,second=0,microsecond=0)

    if command.site is not None:
        utilities = get_site_utility_lists(site)

        for item in utilities:
            utility = Utility.objects.get(name=str(item['name']))
            energy_total = site.site_energy_range(start_datetime,end_datetime,utility)

            if trigger_level == "equal_to":
                if float(energy_total) == float(threshold):
                    sendMessage = True
                    break
            elif trigger_level == "greater_than":
                if float(energy_total) > float(threshold):
                    sendMessage = True
                    break
            elif trigger_level == "less_than":
                if float(energy_total) < float(threshold):
                    sendMessage = True
                    break
        
    elif command.user is not None:
        for num in command.user.line_number.split(','):  
            line = Line.objects.get(site=command.user.site, number=num)
            utility = line.utility_type

            try:

                daily_power = Daily_Power.objects.get(user=command.user, timestamp=start_datetime, utility=utility)
                energy_total = float(daily_power.amount)

                if trigger_level == "equal_to":
                    if float(energy_total) == float(threshold):
                        sendMessage = True
                        break
                elif trigger_level == "greater_than":
                    if float(energy_total) > float(threshold):
                        sendMessage = True
                        break
                elif trigger_level == "less_than":                    
                    if float(energy_total) < float(threshold):
                        sendMessage = True
                        break

            except Exception as e:
                pass

    elif command.line is not None:
        utility = command.line.utility_type
        try:
            daily_power = Daily_Power.objects.get(line_obj=command.line, timestamp=start_datetime, utility=utility)
            energy_total = float(daily_power.amount)

            if trigger_level == "equal_to":
                if float(energy_total) == float(threshold):
                    sendMessage = True
            elif trigger_level == "greater_than":
                if float(energy_total) > float(threshold):
                    sendMessage = True
            elif trigger_level == "less_than":
                if float(energy_total) < float(threshold):
                    sendMessage = True
        except Exception as e:
            pass

    return sendMessage

def checkVoltage(command, hour):
    send_voltage_message = False


    end_date = gen_zone_aware(TIME_ZONE,datetime.now())
    start_date = end_date - timedelta(days=1)

    if command.site is not None:
        send_voltage_message = getVoltageData(command, start_date, end_date)

    if send_voltage_message:
        send_trigger_alert(command, "Voltage: ")


def getVoltageData(command, start_datetime, end_datetime):
    sendMessage = False
    site = command.site
    trigger_level = command.trigger_level
    threshold = command.threshold
  
    voltage_logs = Site_Record.objects.filter(site=site,key="BH Data Log")
    voltage_logs.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)

    for log in voltage_logs:
        value_array = log.value.split(',')

        voltage = value_array[0]

        if trigger_level == "equal_to":
            if float(voltage) == float(threshold):
                sendMessage = True
                break
        elif trigger_level == "greater_than":
            if float(voltage) > float(threshold):
                sendMessage = True
                break
        elif trigger_level == "less_than":
            if float(voltage) < float(threshold):
                sendMessage = True
                break

    return sendMessage

def checkTemperature(command, hour):
    send_temperature_message = False


    end_date = gen_zone_aware(TIME_ZONE,datetime.now())
    start_date = end_date - timedelta(days=1)

    if command.site is not None:
        send_temperature_message = getTemperatureData(command, start_date, end_date)    

    if send_temperature_message:
        send_trigger_alert(command, "Temperature:")


def getTemperatureData(command, start_datetime, end_datetime):
    sendMessage = False
    site = command.site
    threshold = command.threshold
    trigger_level = command.trigger_level
    
  
    temperature_logs = Site_Record.objects.filter(site=site, key="BH Data Log")    
    temperature_logs.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)

    for log in temperature_logs:
        value_array = log.value.split(',')
        temperature = value_array[1]
       

        if trigger_level == "equal_to":
            if float(temperature) == float(threshold):
                sendMessage = True
                break
        elif trigger_level == "greater_than":
            if float(temperature) > float(threshold):
                sendMessage = True
                break
        elif trigger_level == "less_than":
            if float(temperature) < float(threshold):
                sendMessage = True
                break

    return sendMessage


def checkAccountBalance(command, hour):
    trigger_level = command.trigger_level
    threshold = command.threshold
    send_balance_msg = False           

    if command.user is not None:
        send_balance_msg = checkUserAccountBalance(command.user,trigger_level,threshold)

    if send_balance_msg:
        send_trigger_alert(command,"Account Balance:")


def checkUserAccountBalance(user, trigger_level, threshold):
    sendMessage = False

    if trigger_level == "equal_to":
        if float(user.account_balance) == float(threshold):
            sendMessage = True
    elif trigger_level == "greater_than":
        if float(user.account_balance) > float(threshold):
            sendMessage = True
    elif trigger_level == "less_than":
        if float(user.account_balance) < float(threshold):
            sendMessage = True

    return sendMessage


def send_trigger_alert(command,title):
    msg_type = command.message_type

    if msg_type == "email":
        try:
            send_email = EmailMessage(subject='Steama '+str(title)+' Trigger Report',body=str(command.message),to=[str(command.destination)])        
            send_email.send()
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))            

    elif msg_type == "sms":
        try:
            sendSMS(command.destination,command.message)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_EMAIL",problem_message=str(e))
                
    

def checkTimerUtilityUsage(command,hour):
    """
    Check utility usage
    """

    print "Utility usage timer commands ... "

    DAYS = 1 
    end_date = gen_zone_aware(TIME_ZONE,datetime.now())
    start_date = end_date - timedelta(days=DAYS)


    email_status = False

    site_usage_list_email = ''

    # Check whether a site usage
    if command.site is not None:
        # This is site
        site = command.site
        site_usage_list_email += '<p style="text-decoration:underline;">Command: Site: '+str(site)+' Utility Usage Hour: '+str(hour)+'</p>'
        if command.utility is None:
            print "utility none"
            energy_total = site.site_energy_range(start_date,end_date)
        else:
            print "utility ye"
            # check for specific defined utility
            energy_total = site.site_energy_range(start_date,end_date,command.utility)

    elif command.user is not None:
        # This is user
        user = command.user
        site_usage_list_email += '<p style="text-decoration:underline;">Command: User: '+str(user)+' Utility Usage Hour: '+str(hour)+'</p><ul>'
        if command.utility is None:
            energy_total = user.power_used(start_date,end_date)
        else:
            # check for specific defined utility
            energy_total = user.power_used(start_date,end_date,command.utility)

    print 'energy total ... '+ str(energy_total)
     # Look for equal to
    if command.trigger_level == 'equal_to':
        if float(energy_total) == float(command.message):
            # Email this data
            print "equal"
            email_status = True
            site_usage_list_email += '<li>Equal To : Site - ' + str(site if site is not None else user)+" &ensp; Value : "+str(command.message)+'</li>'
    # Look for equal to
    elif command.trigger_level == 'less_than':
        if float(energy_total) < float(command.message):
            # Email this data
            print 'less than'
            email_status = True
            site_usage_list_email += '<li>Less Than : Site - ' + str(site if site is not None else user)+" &ensp; Value : "+str(command.message)+'</li>'

    # Look for equal to
    elif command.trigger_level == 'greater_than':
        if float(energy_total) > float(command.message):
            # Email this data
            print 'greater than'
            email_status = True
            site_usage_list_email += '<li>Greater Than : Site - ' + str(site if site is not None else user)+" &ensp; Value : "+str(command.message)+'</li>'
    site_usage_list_email += '</ul>'
    # Email user
    if email_status:
        return site_usage_list_email
    else:
        return False

def sendTimerSMS(command,hour):
    try:
        sendSMS(command.destination,command.message)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                 problem_message=str(e))

# def checkSiteTimerUtilityUsage(command,hour):
#     """
#     Check utility usage
#     """ 
#     print "Site timer commands ... "
#     DAYS = 1 
#     end_date = datetime.now()
#     start_date = end_date - timedelta(days=DAYS)

#     site_usage_list_email = ''

#     user_details = User.objects.filter(email=command.email)[0]
#     user_sites = Mingi_Site.objects.filter(owners=user_details.id)

#     site_usage_list_email += 'Command: Site: '+str(site)+' Utility Usage Hour: '+str(hour)
#     for site in user_sites:
#         [energy_total,faulty_status] = site.get_site_energy_total(start_date,end_date)

#         # Look for equal to
#         if command.trigger_level == 'equal_to':
#             if float(energy_total) == float(command.message):
#                 # Email this data
#                 site_usage_list_email += '<br/>&emsp; Equal To : Site - ' + str(usage_list[0])+" &ensp; Value - "+str(command.message)
#         # Look for equal to
#         elif command.trigger_level == 'less_than':
#             if float(energy_total) < float(command.message):
#                 # Email this data
#                 site_usage_list_email += '<br/>&emsp; Less Than : Site - ' + str(usage_list[0])+" &ensp; Value - "+str(command.message)

#         # Look for equal to
#         elif command.trigger_level == 'greater_than':
#             if float(energy_total) > float(command.message):
#                 # Email this data
#                 site_usage_list_email += '<br/>&emsp; Greater Than : Site - ' + str(usage_list[0])+" &ensp; Value - "+str(command.message)

    
#     # Email user
#     if 'Equal To' in site_usage_list_email or 'Less Than' in site_usage_list_email or 'Greate Than' in site_usage_list_email:
#         return site_usage_list_email
#     else:
#         return False


def checkTimerCostUsage(command,hour):
    """
    Check utility usage
    """
    print 'cost usage'

def sendTimerSMS(command,hour):
    """
    Check utility usage
    """
    try:
        sendSMS(command.destination,command.message)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                                 problem_message=str(e))
    return False
    print 'send SMS'

def checkLineStatus(user):
    # make sure all user's lines are turned off or on as appropriate
    
    # return line switching commands (for Manual lines only) as necessary
    if (user.control_type == "AUTOL"):
        return "NO CHANGE"

    # regular users with no balance and line status of ON
    elif (user.line_is_on == True) and (int(user.line_status < 4)) and (user.account_balance <= 0):
   
        return "OFF"

    # Monthly Rate users 
    elif (user.payment_plan.split(',')[0] == "Monthly Rate"):
        threshold = float(user.payment_plan.split(',')[1])
        
        if user.account_balance >= threshold and user.line_is_on == False:
            return "ON"
        elif user.account_balance < threshold and user.line_is_on == True:
            return "OFF"
    
    elif (user.line_is_on == False) and (int(user.line_status > 3)) and (user.account_balance > 0):

        return "ON"
    
    else:
        return "NO CHANGE"
    

def sendLineStatusMsgs():
    """
    detect users whose line status should change and sent the relevant commands
    """

    # run line status maintenance (for BH-controlled users only)
    bit_harvesters = Bit_Harvester.objects.all()
    line_labels = ["A","B","C","D"]
    
    for BH in bit_harvesters:
        # this assumes all commands at site are sent to same BH
        
        all_users = Mingi_User.objects.filter(is_user=True,bit_harvester=BH)
        all_users = all_users.exclude(line_control_party="SM")
        all_users = all_users.exclude(line_number="")
        all_users = all_users.exclude(control_type="AUTOL")
        
        line_on = list()
        line_off = list()
        
        for user in all_users:
            result = checkLineStatus(user)
            
            
            # keep track of the on/off results
            if result == "ON":
                for line in user.line_array:
                    try:
                        diff = int(user.site.num_lines.encode('utf-8')) - int(line.encode('utf-8'))
                    except:
                        diff = 0
                    if BH.version == "V4" and diff < 4:
                        line = line_labels[3 - diff]
                    line_on.append(line)
            elif result == "OFF":
                for line in user.line_array:
                    try:
                        diff = int(user.site.num_lines.encode('utf-8')) - int(line.encode('utf-8'))
                    except:
                        diff = 0
                    if BH.version == "V4" and diff < 4:
                        line = line_labels[3 - diff]
                    line_off.append(line)
            else:
                pass

        
        if len(line_on) > 0:
            msg_id = "1" + str(datetime.now().microsecond)[2:-1]
            on_msg = "*%s,ON" % msg_id
            for line in line_on:
                on_msg += ",%s" % str(line)
            try:
                sendSMS(BH.telephone,on_msg,BH)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))
        if len(line_off) > 0:
            msg_id = "1" + str(datetime.now().microsecond)[2:-1]
            off_msg = "*%s,OFF" % msg_id
            for line in line_off:
                off_msg += ",%s" % str(line)
            try:
                sendSMS(BH.telephone,off_msg,BH)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))


