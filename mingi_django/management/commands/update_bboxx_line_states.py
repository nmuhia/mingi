import datetime

from django.core.management.base import BaseCommand

from africas_talking.BBOXX_API import update_bboxx_line_states


class Command(BaseCommand):
    """
    Gathers SOLO+ line state changes from the BBOXX API.
    """
    def handle(self, *args, **options):
        update_bboxx_line_states(
            datetime.datetime.now() - datetime.timedelta(days=1))
