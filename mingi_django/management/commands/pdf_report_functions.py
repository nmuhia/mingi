from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_django.models import Website_User_Profile, Error_Log
from mingi_site.models import Mingi_Site, Line_State, Bit_Harvester, Hourly_Site_Power, Hourly_Site_Revenue, Daily_Power, Site_Record, Line
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import datetime
from datetime import date
import time
import pytz
from django.utils import timezone
from django.utils.timezone import now,utc
from django.db.models import Q

from operator import itemgetter
from collections import Counter

from mingi_django.dashboard import convert_kes_usd, convert_tzs_usd, convert_xof_usd

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import zone_aware, gen_zone_aware


def year_long_site_statistics(site, start, end):

    financial_stats = site_financial_stats(site, start, end)
    revenue = financial_stats[0]
    total_arpu = financial_stats[1]
    month_data = financial_stats[2]
    highest_arpu = financial_stats[3]
    lowest_arpu = financial_stats[4]
    energy_use = total_energy_use(site, start, end)
    num_connections = new_connections(site, start, end)
   
    return ["{0:.2f}".format(revenue), "{0:.2f}".format(total_arpu), "{0:.2f}".format(energy_use), num_connections, month_data, highest_arpu, lowest_arpu]
    

def site_statistics(site, start, end):
    #start = timezone.make_aware(start,timezone=pytz.timezone(TIME_ZONE))
    #end = timezone.make_aware(end,timezone=pytz.timezone(TIME_ZONE))
    print site.name
    delta_sign = "neutral"
    previous_end = start - datetime.timedelta(days=1)
    previous_start = previous_end.replace(day=1)

    #site revenue
    last_month_revenue = "{0:.2f}".format(total_revenue(site, start, end))
    previous_month_revenue = "{0:.2f}".format(total_revenue(site, previous_start, previous_end))
    revenue_delta = "{0:.2f}".format(calculate_delta(float(last_month_revenue),float(previous_month_revenue)))
    revenue_change = float(last_month_revenue) - float(previous_month_revenue)

    print str(last_month_revenue) + " vs. " + str(previous_month_revenue) + " --> " + str(revenue_delta) 

    #arpu
    last_arpu = "{0:.2f}".format(arpu(site, start, end))
    previous_arpu = "{0:.2f}".format(arpu(site, previous_start, previous_end))
    arpu_delta = "{0:.2f}".format(calculate_delta(float(last_arpu),float(previous_arpu)))
    arpu_change = float(last_arpu) - float(previous_arpu)

    print str(last_arpu) + " vs. " + str(previous_arpu) + " --> " + str(arpu_delta) 

    if revenue_change < 0:
        delta_sign = "negative"
    elif revenue_change > 0:
        delta_sign = "positive"
    
    #get weekly energy use for the month (from Mon-Sun)
    final_start_date = start - datetime.timedelta(days=start.weekday())
    final_end_date = end + datetime.timedelta(days=end.weekday())

    start_date = final_start_date
    end_date = start_date + datetime.timedelta(days=6)

    totals_dict= {'dates': [],'total': []}     

    """for n in range(int ((final_end_date - final_start_date).days)):
        if end_date <= final_end_date:
            pass
            
        else:
            break;
        
        totals_dict['dates'].append("{0.month}/{0.day}".format(start_date))        
        totals_dict['total'].append(total_energy_use(site, start_date, end_date))

        start_date = end_date + datetime.timedelta(days=1)
        end_date = start_date + datetime.timedelta(days=6)"""

    #number of connected lines
    connected_users = Mingi_User.objects.filter(site=site).exclude(line_number__isnull=True).exclude(line_number__exact="").count()
    print "NUM USERS: " + str(connected_users)
    connected_lines = Line.objects.filter(site=site).exclude(user=None).count()

    #average hours before top up
    site_power_analysis = power_analysis(site, start, end)
    
    #return top revenue maker in a site 
    top_user = get_top_user(site, start, end)

    print "top user found"
    
    #return most common payment amount and hour
    most_common_payment_amount = 0
    most_common_payment_hour = 0

    amount_list = []
    hour_list = []

    payments = Kopo_Kopo_Message.objects.filter(user__site=site, timestamp__gte=start, timestamp__lte=end)    
        
    if len(payments) > 0:
        for pay in payments:
            amount_list.append(pay.amount)
            hour_list.append(zone_aware(pay.user.site, pay.timestamp).hour)

        amount_counter = Counter(amount_list)
        hour_counter = Counter(hour_list)

        most_common_payment_amount = float(amount_counter.most_common()[0][0])

        if site.currency == "TSH":
            most_common_payment_amount = float(most_common_payment_amount) * float(convert_tzs_usd())
        elif site.currency == "KSH":
            most_common_payment_amount = float(most_common_payment_amount) * float(convert_kes_usd())
        elif site.currency == "XOF":
            most_common_payment_amount = float(most_common_payment_amount) * float(convert_xof_usd())
        
        from_24hour = time.strptime("%02d" % (hour_counter.most_common()[0][0]), "%H")
        to_12hour = time.strftime( "%I:%M %p", from_24hour ).lstrip('0')
        most_common_payment_hour = to_12hour

    print "payment activity analyzed"
        
    return [
        top_user,
        "{0:.2f}".format(most_common_payment_amount),
        str(most_common_payment_hour),
        last_month_revenue,
        previous_month_revenue,
        totals_dict,
        last_arpu,
        previous_arpu,
        revenue_delta,
        arpu_delta,
        str(revenue_change),
        str(arpu_change),
        str(connected_users),
        site_power_analysis,
        minimum_voltage(site, start, end),
        str(connected_lines),
        delta_sign
    ]


def minimum_voltage(site, start, end):
    """
    Calculates the minimum voltage point for the given site in the given time
    range.

    :param site: A Mingi_Site
    :param start: The beginning of the time range.
    :param end: The end of the time range.
    :return: A string containing either the minimum voltage number or "No data"
    """
    voltage_logs = Site_Record.objects.filter(
        Q(site=site),
        Q(key="System Voltage") | Q(key="BH Data Log"))
    voltage_logs = voltage_logs.filter(timestamp__gte=start,
                                       timestamp__lte=end)

    if len(voltage_logs) == 0:
        return "No data"

    voltage_list = []

    for log in voltage_logs:
        reading = 0
        value_array = log.value.split(',')

        if log.key == "System Voltage":
            reading = log.value
        elif log.key == "BH Data Log":     
            reading = value_array[0]

        voltage_list.append(float(reading))

    min_voltage = min(voltage_list)
    return str(min_voltage)
        
            
def total_energy_use(site, start, end):
    """
    total energy use of of site over time
    """
    power_data = Hourly_Site_Power.objects.filter(site=site, timestamp__gte=start, timestamp__lte=end).order_by('timestamp')     

    total = 0

    if len(power_data) > 0:
        for data in power_data:
            if float(data.amount) < 0:
                total += 0
            else:
                total += float(data.amount)

    return total
    

def total_revenue(site, start, end):
    """
    total revenue for a site
    """
    
    revenue_data = Hourly_Site_Revenue.objects.filter(site=site, timestamp__gte=start, timestamp__lte=end).order_by('timestamp')    
    total = 0
    if len(revenue_data) > 0:
        for revenue in revenue_data:
            total += revenue.amount

    if site.currency == "TSH":
        total = float(total) * float(convert_tzs_usd())
    elif site.currency == "KSH":
        total = float(total) * float(convert_kes_usd())
    elif site.currency == "XOF":
        total = float(total) * float(convert_xof_usd())

    return total


def get_top_user(site, start, end):
    user_list = []
    top_user = None

    site_revenue = total_revenue(site, start, end)

    users = Mingi_User.objects.filter(site=site)
    if len(users) > 0:
        for user in users:
            user_info = []
            user_total = 0
            user_revenue = user.revenue_range(start,end)
            user_info.append(str(user.first_name)+" "+str(user.last_name))
            user_info.append(str(user.line_number))

            if site.currency == "TSH":
                user_revenue = float(user_revenue) * float(convert_tzs_usd())
            elif site.currency == "KSH":
                user_revenue = float(user_revenue) * float(convert_kes_usd())
            elif site.currency == "XOF":
                user_revenue = float(user_revenue) * float(convert_xof_usd())

            user_revenue_contrib = 0.0
            try:
                user_revenue_contrib = (float(user_revenue) / float(site_revenue)) * 100
            except ZeroDivisionError:
                user_revenue_contrib = 0.0
            
            user_info.append("{0:.2f}".format(user_revenue_contrib))            

            user_list.append(user_info)

        user_list = sorted(user_list, key=itemgetter(2), reverse=True)       
        top_user = user_list[0]

    return top_user


def get_top_site(sites, start, end):
    top_site = None
    sites_list = []
    

    for site in sites:
        site_info = []
        site_info.append(str(site.name))
        site_info.append(total_revenue(site, start, end))
        site_info.append(arpu(site, start, end))
        sites_list.append(site_info)

    top_earning_sites_list = sorted(sites_list, key=itemgetter(1), reverse=True) 
    best_performing_sites_list = sorted(sites_list, key=itemgetter(2), reverse=True)

    return [top_earning_sites_list[0], best_performing_sites_list[0]]


def site_financial_stats(site, start, end):
    total = 0
    average = 0    
    rev_list = []

    num_users = Mingi_User.objects.filter(site=site).exclude(line_number__exact="").exclude(line_number=None).count()

    try:
        total = total_revenue(site, start, end)
        average = float(total) / float(num_users)
    except ZeroDivisionError:
        average = 0.0
    
    for this_date in rrule.rrule(rrule.MONTHLY, dtstart=start, until=end):
        avg = 0
        rev = 0
        rev_info = []

        start_date = this_date
        end_date = start_date + relativedelta(months=1,days=-1)

        try:
            rev = total_revenue(site, start_date, end_date)
            avg = float(rev) / float(num_users)
        except ZeroDivisionError:
            avg = 0.00

        #append month and total arpu
        rev_info.append(start_date.strftime("%B"))        
        rev_info.append(float("{0:.2f}".format(avg)))
        rev_info.append(float("{0:.2f}".format(rev)))
        rev_list.append(rev_info)

    sorted_arpu_list = sorted(rev_list, key=itemgetter(2), reverse=True)

    highest_arpu = sorted_arpu_list[0]
    lowest_arpu = sorted_arpu_list[11]
    for item in reversed(sorted_arpu_list):
        if float(item[1]) != 0:            
            lowest_arpu = item
            break

    return [total, average, rev_list, highest_arpu, lowest_arpu]

def new_connections(site, start, end):
    lines = Line.objects.filter(site=site, user__created__gte=start, user__created__lte=end).count()
    return lines


def arpu(site, start, end):
    total = 0
    average = 0

    num_users = Mingi_User.objects.filter(site=site).exclude(line_number__exact="").exclude(line_number=None).count()
    print "No. USERS: " + str(num_users)    

    try:
        total = total_revenue(site, start, end)
        average = float(total) / float(num_users)
        print str(average)
    except ZeroDivisionError:
        average = 0.0
    print "(post conversion) " + str(average)
    return average

def calculate_delta(current,previous):
    delta = 0
    try:
        if current >= previous:
            delta = 100 * (current - previous) / previous
        else:
            delta = 100 * (previous - current) / previous

    except ZeroDivisionError:
        delta = 0

    return delta

#average # of hours before top-up 
def power_analysis(site, start, end):
    line_states = Line_State.objects.filter(site=site ,timestamp__gte=start,timestamp__lt=end,account_balance__isnull=False)
    
    users = Mingi_User.objects.filter(site=site, line_number__isnull=False).exclude(line_number='')
    user_data_list = list()
    index = 0
    power_off_events = 0
    total_time = 0

    for user in users:
        user_data_list.append([user, 0, 0.0])

        user_states = line_states.filter(user=user).order_by('timestamp')
        line_is_on = True
        power_off_time = None
        last_state = None

        # note relevant info each time a line is just turned off or just turned on
        for state in user_states:
            if state.account_balance <= 0 and line_is_on == True:
                line_is_on = False
                power_off_time = state.timestamp
                power_off_events += 1

            if state.account_balance > 0 and line_is_on == False:
                line_is_on = True
                total_off_time = float((state.timestamp - power_off_time).days) * 24.0 + float((state.timestamp - power_off_time).seconds) / 3600.0
                total_time += total_off_time

            last_state = state

        # special case: handle last line-state during power-off interval
        if line_is_on == False:
            if power_off_time:
                total_off_time = float((state.timestamp - power_off_time).days) * 24.0 + float((last_state.timestamp - power_off_time).seconds) / 3600.0
            else:
                total_off_time = 0.0
            total_time += total_off_time

    return [str(power_off_events), str(total_time)]
