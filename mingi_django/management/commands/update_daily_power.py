import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from mingi_django.tasks import daily_power_rollups


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-o',nargs=1,help="")

    def handle(self, *args, **options):
        """
        Updates the hourly power rollups for every site for the past week.
        """

        if options['o']:
            DAYS = int(options['o'][0])
        else:
            DAYS = 0
        
        this_date = now().date() - datetime.timedelta(days=DAYS)

        for i in range(0,DAYS):
            print "Creating rollup for: " + str(this_date)
            daily_power_rollups(-1,this_date)
            this_date = this_date + datetime.timedelta(days=1)

