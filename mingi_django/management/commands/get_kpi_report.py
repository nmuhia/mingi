from django.core.management.base import BaseCommand

from mingi_site.models import Mingi_Site, Daily_Power
from mingi_user.models import Mingi_User
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log

from report_functions import get_header, get_email_subject
from mingi_django.dashboard import convert_kes_usd, convert_tzs_usd

from django.core.mail import EmailMessage
from datetime import datetime, timedelta, date
from django.utils import timezone
from django.utils.timezone import now,utc
import csv
from django.core.files import File
import os, tempfile, zipfile
import StringIO
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware
import pytz

KPI_FILENAME = "kpi_report.csv"
ZIP_KPI_FILENAME = "kpi_report.zip"
DAILY_GENERATION_HOURS = 4


SYS_VAR_NAMES = ['','switch-on date','total deployed cost (USD)','solar capacity (kW)', 'wind capacity (kW)','generator capacity (kW)','inverter capacity (kW)','est. solar generation (kWh/day)','est. wind generation (kWh/day)']
USER_VAR_NAMES = ['total registered users','no. commercial users','no. residential users','avg. power use per commercial user (kWh/day)','avg. power use per residential user (kWh/day)','no. commercial users / total users','no. residential users / total users','proportion of power use - commercial users','proportion of power use - residential users','total energy use - commercial users (kWh)','total energy use - residential users (kWh)']
SALES_VAR_NAMES = ['total electricity sales (local currency)','ARPU - commercial users (USD)','ARPU - residential users (USD)','ARPU - avg (USD)','AIPU (USD)','avg. effective tariff (USD / kWh)','avg. commercial tariff (USD / kWh)','avg. residential tariff (USD / kWh)']
ENERGY_VAR_NAMES = ['total energy use (kWh)','avg. daily energy energy use (kWh / day)','utilisation factor','daily energy from generator (kWh)','proportion of energy from generator']

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('start_string',help="")
        parser.add_argument('end_string',help="")
        parser.add_argument('email',help="")
        parser.add_argument('-o',nargs=1,help='optional user')

    def handle(self, *args, **options):
        """
        generate KPIs report and email to specified address
        """
        title = "Mingi KPIs Report"

        # get args variables
        start_string = options['start_string']
        end_string = options['end_string']

        start_datetime = datetime.strptime(start_string, '%d-%m-%y')
        start_datetime = timezone.make_aware(start_datetime,timezone=pytz.timezone(TIME_ZONE))
        # include end date in filter by getting data less than the day after
        end_datetime = datetime.strptime(end_string,'%d-%m-%y') + timedelta(days=1)
        end_datetime = timezone.make_aware(end_datetime,timezone=pytz.timezone(TIME_ZONE))

        start_date = date(start_datetime.year,start_datetime.month,start_datetime.day)
        end_date = date(end_datetime.year,end_datetime.month,end_datetime.day)
        # extract email
        try:
            email = options['email'][0]
            file_format = options['email'][1]
        except Exception as e:
            email = options['email']
            file_format = "plain"

        filename = KPI_FILENAME
        zip_filename = ZIP_KPI_FILENAME
        
        if options['o']:
            user = options['o'][0]
        else:
            user = None
        
        # *** SOON: add filter by user ***
        if user and user.is_staff == False:
            sites = Mingi_Site.objects.filter(owners=user, is_active=True, is_combined=False)
        else:
            sites = Mingi_Site.objects.filter(is_active=True, is_combined=False)
        system_params = list()
        user_breakdown = list()
        sales_breakdown = list()
        energy_breakdown = list()

        for site in sites:
            site_system_params = get_system_parameters(site)
            gen_est = float(site_system_params[5]) + float(site_system_params[6])
            system_params.append(site_system_params)

            # generate / calculate data as it makes sense time-wise, but organize and display it as requested in template
            site_user_energy_breakdown = get_user_energy_breakdown(site,start_date,end_date,gen_est)

            user_breakdown.append(site_user_energy_breakdown[0])
            sales_breakdown.append(get_sales_breakdown(site, start_datetime, end_datetime))

            energy_breakdown.append(site_user_energy_breakdown[1])
            
        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(title, start_date, end_date)

            for row in header:
                writer.writerow(row)

            writer.writerow([""])

            # write system overview data 
            for i in range(0,len(SYS_VAR_NAMES)):
                row = list()
                row.append(SYS_VAR_NAMES[i])
                for site_row in system_params:
                    row.append(str(site_row[i]))
                    
                writer.writerow(row)

            writer.writerow([""])

            # write user overview data 
            for i in range(0,len(USER_VAR_NAMES)):
                row = [USER_VAR_NAMES[i]]
                for site_row in user_breakdown:
                    row.append(str(site_row[i]))
                    
                writer.writerow(row)

            writer.writerow([""])

            # write sales overview data 
            for i in range(0,len(SALES_VAR_NAMES)):
                row = [SALES_VAR_NAMES[i]]
                for site_row in sales_breakdown:
                    row.append(str(site_row[i]))
                    
                writer.writerow(row)

            writer.writerow([""])

            # write energy overview data 
            for i in range(0,len(ENERGY_VAR_NAMES)):
                row = [ENERGY_VAR_NAMES[i]]
                for site_row in energy_breakdown:
                    row.append(str(site_row[i]))
                    
                writer.writerow(row)
            

        # compile email and send
        subject = get_email_subject(title, start_date, end_date)
        email = EmailMessage(subject,'Your data is attached',to=[email])

        if file_format == "zip":
            # compress file
            # open file for writing
            zipped_file = zipfile.ZipFile(zip_filename, mode='w')
            try:
                # compress file
                zipped_file.write(filename,compress_type=compression)
            except Exception as e:
                # if error, send uncompressed file
                print e
            finally:
                zipped_file.close()
                
            email.attach_file(zip_filename)
        else:
            email.attach_file(filename)

        email.send()


        
def get_system_parameters(site):
    """
    return an array with system parameter values for capacity, etc
    """

    # retrieve site parameters
    capital = site.capital
    date_online = site.date_online
    name = site.name

    solar = float(site.solar_capacity)
    wind = float(site.wind_capacity)
    generator = float(site.generator_capacity)
    inverter = float(site.inverter_capacity)

    solar_output = solar * float(DAILY_GENERATION_HOURS)
    wind_output = wind * float(DAILY_GENERATION_HOURS)

    return [name,date_online,capital,solar,wind,generator,inverter,solar_output,wind_output]


def get_user_energy_breakdown(site, start_date, end_date, gen_est):
    """
    return an array with user numbers and energy breakdown statistics including energy use
    """
    
    users = Mingi_User.objects.filter(site=site,is_user=True,line_number__isnull=False)

    data_list = [[] for user in users]

    rollups = Daily_Power.objects.filter(user__isnull=False,timestamp__gte=start_date,timestamp__lt=end_date)
    rollups = rollups.filter(user__site=site)

    # initialize parameters
    comm_count = 0
    res_count = 0
    comm_use_total = 0.0
    res_use_total = 0.0
    genset_gen = 0.0
    
    # cache data in list array
    index = 0
    for user in users:
        data_list[index].append(user)
        index += 1

    for rollup in rollups:
        user = rollup.user
        
        for row in data_list:
            if row[0] == user:
                row.append(rollup)

    # calculate stats from data
    for row in data_list:
        user = row[0]
        
        # first look for a generator account
        if user.line_number in site.source_line_array:
            genset_gen += float(get_power_sum(row))
            
        # tally data for a commercial user
        elif user.user_type == "BUS":
            comm_count += 1
            comm_use_total += float(get_power_sum(row))

        elif user.user_type == "RES":
            res_count += 1
            res_use_total += float(get_power_sum(row))

        else:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Error: User has invalid type - %s, %s" % (str(user),user.user_type))
            

    # do math
    no_users = comm_count + res_count
    try:
        comm_proportion = comm_count / no_users
    except ZeroDivisionError:
        comm_proportion = 0.0
    try:
        res_proportion = res_count / no_users
    except ZeroDivisionError:
        res_proportion = 0.0

    total_use = comm_use_total + res_use_total
    days = (end_date - start_date).days
    avg_total_use = total_use / days

    try:
        avg_comm_use = comm_use_total / (days * comm_count)
    except ZeroDivisionError:
        avg_comm_use = 0.0
    try:
        avg_res_use = res_use_total / (days * res_count)
    except ZeroDivisionError:
        avg_res_use = 0.0

    try:
        comm_use_proportion = comm_use_total / total_use
        res_use_proportion = res_use_total / total_use
    except ZeroDivisionError:
        comm_use_proportion = 0.0
        res_use_proportion = 0.0

    try:
        util_factor = total_use / (gen_est + genset_gen)
    except ZeroDivisionError:
        util_factor = 0.0

    daily_genset = genset_gen / days
    
    try:
        gen_prop = genset_gen / total_use
    except ZeroDivisionError:
        gen_prop = 0.0

    return [[no_users,comm_count,res_count,avg_comm_use,avg_res_use,comm_proportion,res_proportion,comm_use_proportion,res_use_proportion,comm_use_total,res_use_total],[total_use,avg_total_use,util_factor,daily_genset,gen_prop]]


def get_sales_breakdown(site, start_datetime, end_datetime):
    """
    return an array with sales breakdown by user and tariff type
    """

    currency = site.currency
    if currency == "KSH":
        conversion = convert_kes_usd()
    elif currency == "TSH":
        conversion = convert_tzs_usd()
    elif currency == "NRS":
        conversion = 1
    else:
        conversion = 1
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Site: %s has unrecognized currency: %s" % (str(site),site.currency))
                                 
    users = Mingi_User.objects.filter(site=site,is_user=True,line_number__isnull=False)

    data_list = [[] for user in users]

    payments = Kopo_Kopo_Message.objects.filter(user__site=site,timestamp__gte=start_datetime,timestamp__lt=end_datetime)

    # initialize parameters
    comm_count = 0
    res_count = 0
    comm_pay_total = 0.0
    res_pay_total = 0.0
    comm_tariff_total = 0.0
    res_tariff_total = 0.0
    
    # cache data in list array
    index = 0
    for user in users:
        data_list[index].append(user)
        index += 1

    for payment in payments:
        user = payment.user
        
        for row in data_list:
            if row[0] == user:
                row.append(payment)
    
    for row in data_list:
        user = row[0]
        
        # tally data for a commercial user
        if user.user_type == "BUS":
            comm_count += 1
            comm_pay_total += float(get_power_sum(row))
            comm_tariff_total += float(user.energy_price)

        elif user.user_type == "RES":
            res_count += 1
            res_pay_total += float(get_power_sum(row))
            res_tariff_total += float(user.energy_price)

        else:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Error: User has invalid type - %s, %s" % (str(user),user.user_type))
            
    # do math (KES)
    total_sales = comm_pay_total + res_pay_total

    # convert values to USD
    comm_pay_total = comm_pay_total * float(conversion)
    res_pay_total = res_pay_total * float(conversion)

    try:
        arpu_comm = comm_pay_total / comm_count
    except ZeroDivisionError:
        arpu_comm = 0.0
        
    try:
        arpu_res = res_pay_total / res_count
    except ZeroDivisionError:
        arpu_res = 0.0 
    try:
        arpu_avg = (comm_pay_total + res_pay_total) / (comm_count + res_count)
    except:
        arpu_avg = 0.0

    try:
        aipu = site.capital / (comm_count + res_count)
    except TypeError:
        aipu = 0.0
    
    conv_comm_total = comm_tariff_total * float(conversion)
    conv_res_total =  res_tariff_total * float(conversion)

    try:
        avg_comm_tariff = comm_tariff_total / comm_count
    except ZeroDivisionError:
        avg_comm_tariff = 0.0

    try:
        avg_res_tariff = res_tariff_total / res_count
    except ZeroDivisionError:
        avg_res_tariff = 0.0
    try: 
        avg_tariff = (avg_comm_tariff * comm_count + avg_res_tariff * res_count) / (comm_count + res_count)
    except:
        avg_tariff = 0
    
    return [total_sales,arpu_comm,arpu_res,arpu_avg,aipu,avg_tariff,avg_comm_tariff,avg_res_tariff]
    

def get_power_sum(data_row):
    """
    sum the daily power rollups in a user's data_row
    """

    total = 0.0

    for i in range(1,len(data_row)):
        if data_row[i].amount > 0:
            total += float(data_row[i].amount)

    return total
