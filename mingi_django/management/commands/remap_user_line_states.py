from django.core.management.base import BaseCommand, CommandError

from africas_talking.models import Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line_State, Site_Record, Hourly_Site_Revenue, Hourly_Site_Power

import datetime
from django.db.models import Q

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here."
        parser.add_argument('site_name',help="Invalid Syntax: Please enter site name eg. 'python manage.py remap_user_line_states [site name] [number of lines] [BH Version]")
        parser.add_argument('no_of_lines',help="Invalid Syntax: Please enter site name eg. 'python manage.py remap_user_line_states [site name] [number of lines] [BH Version]")
        parser.add_argument('-o',nargs=1,help="Invalid Syntax: Please enter site name eg. 'python manage.py remap_user_line_states [site name] [number of lines] [BH Version]")

    def handle(self, *args, **options):
        """
        create line state snapshots to reset logs for users at a site
        """

        # if len(args) < 2:
        #     print "Invalid Syntax: Please enter site name eg. 'python manage.py remap_user_line_states [site name] [number of lines] [BH Version]'"
        #     return
        # else:
        try:
            site = Mingi_Site.objects.get(name=options['site_name'])
            new_line_num = options['no_of_lines']
        except Mingi_Site.DoesNotExist:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="Site with name: " + options['site_name'] + " could not be found!")
            return "Invalid Site Name"
        except Mingi_Site.MultipleObjectsReturned:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="Site search for name: " + options['site_name'] + " returned multiple results!")
            return "Invalid Site Name"

        if not options['o']:
            version = 3

        else:
            version = options['o'][1]

        users = Mingi_User.objects.filter(site=site,is_user=True)
        users = users.exclude(line_number="")

        for user in users:
            # find last line state snapshot for that user's line
            for line in user.line_array:
                last_state = Line_State.objects.filter(site=user.site,
                                                       number=line)[0]
                # create duplicate line state snapshot tagged to user
                nowtime = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
                Line_State.objects.create(site=user.site,number=line,timestamp=nowtime,meter_reading=last_state.meter_reading,user=user,account_balance=user.account_balance,processed_timestamp=last_state.processed_timestamp)

        return "Remap Successful"
