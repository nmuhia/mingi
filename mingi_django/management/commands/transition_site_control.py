from django.core.management.base import BaseCommand
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester

import datetime
from africas_talking.sendSMS import sendSMS


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('site_name',help="python manage.py transition_site_control [site_name]")

    def handle(self, *args, **options):
        """
        change the bitHarvester control of a site's lines from Manual to Auto
        """

        # retrieve the sites
        try:
            site = Mingi_Site.objects.get(name=options['site_name'])
        except:
            print "Unrecognized site name!"
            return

        users = Mingi_User.objects.filter(site=site, line_number__isnull=False).exclude(line_number='')
        bitHarvester = Bit_Harvester.objects.get(site=site)

        # change control type
        for user in users:
            BH = user.bit_harvester
            if not BH or BH.version != "V4":
                print "Site must have a V4 bitHarvester"
                return

            # only change for CONNECT lines
            if int(user.line_array[-1]) < site.num_lines - 3:
                user.control_type = "AUTOL"
                user.save()
                try:
                    balance = int(float(user.account_balance) / float(user.energy_price) * 1000)
                except ZeroDivisionError:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="User with energy price = 0 on site %s" % site.name)
                    balance = int(user.account_balance)

                try:
                    id_num = "3" + str(datetime.datetime.now().microsecond)[2:]
                    if balance<0:
                        balance = 0
                    sendSMS(BH.telephone,"*%s,CREDIT-SET,%s,%d" % (id_num,user.line_number,balance),BH)
                except Exception as e:
                    print str(e)

        # send a command to switch all lines
        try:
            sendSMS(BH.telephone,"*111,AUTO,ALL",BH)
        except Exception as e:
            print "SMS failed to send: " + str(e)
            
            
        
