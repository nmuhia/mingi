from django.core.management.base import BaseCommand

from mingi_site.models import Mingi_Site, Daily_Power, Line
from mingi_user.models import Mingi_User
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log

from report_functions import get_header, get_email_subject
from mingi_django.dashboard import convert_kes_usd, convert_tzs_usd
from mingi_user.methods import get_site_utility_lists
from report_functions import get_header, get_email_subject

from django.core.mail import EmailMessage
from datetime import datetime, timedelta, date
from django.utils import timezone
from django.utils.timezone import now,utc
import csv
from django.core.files import File
import os, tempfile, zipfile
import StringIO
import pytz


class Command(BaseCommand):
    def add_arguments(self, parser):
        print "arguments here. arguments can only be three or four"
        parser.add_argument('start_string',help="")
        parser.add_argument('end_string',help="")
        parser.add_argument('email',help="")
        parser.add_argument('-o',nargs=1,help='optional user')

    def handle(self, *args, **options):
        """
        generate KPIs report and email to specified address
        """

        # Initialize variables
        data_filename = "Ops_KPIs_" + str(datetime.now()) + ".csv"
        summary_filename = "Ops_KPIs_Summary_" + str(datetime.now()) + ".csv"

        # get args variables
        start_string = options['start_string']
        end_string = options['end_string']

        start_datetime = datetime.strptime(start_string, '%d-%m-%y')
        start_datetime = timezone.make_aware(start_datetime,timezone=pytz.timezone('utc'))

        # include end date in filter by getting data less than the day after
        end_datetime = datetime.strptime(end_string,'%d-%m-%y') + timedelta(days=1)
        end_datetime = timezone.make_aware(end_datetime,timezone=pytz.timezone('utc'))

        start_date = date(start_datetime.year,start_datetime.month,start_datetime.day)
        end_date = date(end_datetime.year,end_datetime.month,end_datetime.day)

        email = options['email']

        # Get list of relevant sites
        print "Collecting sites"
        
        # *** This may become an issue depending on names, to revisit ***
        sites = Mingi_Site.objects.filter(is_active=True).exclude(name__contains="Demo").exclude(name__contains="Test")

        # *** For now, this just calculates and emails the KPIs doc for each relevant month
        next_date = (start_date + timedelta(days=31)).replace(day=1)

        while next_date <= end_date:
            these_sites = sites.exclude(date_online__gte=next_date)
            print len(these_sites)
            site_list = get_kpis_data(these_sites,email,start_date,next_date)

            start_date = next_date
            next_date = (start_date + timedelta(days=31)).replace(day=1)
            
        """
        data_title = "Ops KPIs"
        with open(data_filename,'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(data_title, start_date, end_date)

            for row in header:
                writer.writerow(row)

            writer.writerow([])
            writer.writerow([])
                
            for row in site_list:
                writer.writerow(row)

        summary_data = get_summary_data(site_list)

        summary_title = "Ops KPIs Summary"
        with open(summary data_filename,'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(summary_title, start_date, end_date)

            for row in header:
                writer.writerow(row)

            writer.writerow([])
            writer.writerow([])
                
            for row in summary_data:
                writer.writerow(row)
    
        # compile and send email
        subject = get_email_subject(data_title, start_date, end_date)
        email = EmailMessage(subject,'Your data is attached',to=[email])
        
        email.attach_file(data_filename)
        #email.attach_file(summary_filename)
        email.send()
        """


def get_kpis_data(sites, email, start_date, end_date):
    """
    Get granular data for Ops KPIs
    """

    site_list = [["Name","Owner","Currency","Number of Connections","Revenue per Line","Metering Type(s)","Customer Type"]]
    
    for site in sites:
            
        site_lines = Line.objects.filter(site=site)
        num_connections = site_lines.exclude(user=None).count()
        type_string = ""
        utility_types = get_site_utility_lists(site)
        for utility in utility_types:
            type_string += utility['name']+","
            
        site_list.append([site.name,site.company_name,site.currency,str(num_connections),"Fill In Manually",type_string,"Fill In Manually"])

    data_title = "Ops KPIs"
    data_filename = "Ops_KPIs_" + str(datetime.now()) + ".csv"
    
    with open(data_filename,'w') as csvfile:
        writer = csv.writer(csvfile)
        
        # write header
        header = get_header(data_title, start_date, end_date)
        
        for row in header:
            writer.writerow(row)
            
        writer.writerow([])
        writer.writerow([])
        
        for row in site_list:
            writer.writerow(row)
            
    # compile and send email
    subject = get_email_subject(data_title, start_date, end_date)
    email = EmailMessage(subject,'Your data is attached',to=[email])
        
    email.attach_file(data_filename)
    email.send()


def get_summary_data(sites, email, start_date=None, end_date=None):
    """
    Get monthly summary data for Ops KPIs; from January 2015 until now
    """

    summary_data = list()

    if start_date and end_date:
        pass
    else:
        start_date = datetime(2015,1,1,0,0,0,0)
        end_date = datetime(2016,4,1,0,0,0,0)

    next_date = (start_date + timedelta(days=31)).replace(day=1)
    geographies = [["East Africa","West Africa","Asia Pacific","India","Latin America"]]
    sectors = ["Micro Utility","Asset Finance","On-Grid","Research"]
    summary_data = [[]]

    while next_date <= end_date:
        these_sites = sites.filter(date_online__gte=start_date, date_online__lt=end_date)
        site_list = get_kpis_data(sites)

        """
        ***Eventually, this should be automated; for now, we need to generate and 
        edit manually***
        """

        data_title = "Ops KPIs"
        data_filename = "Ops_KPIs_" + str(datetime.now()) + ".csv"

        with open(data_filename,'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(data_title, start_date, next_date)

            for row in header:
                writer.writerow(row)

            writer.writerow([])
            writer.writerow([])
                
            for row in site_list:
                writer.writerow(row)

        # compile and send email
        subject = get_email_subject(data_title, start_date, end_date)
        email = EmailMessage(subject,'Your data is attached',to=[email])
        
        email.attach_file(data_filename)

        start_date = next_date
        next_date = (start_date + timedelta(days=31)).replace(day=1)
        

    email.send()
    


    
