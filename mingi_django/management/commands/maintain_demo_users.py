from django.core.management.base import BaseCommand

from mingi_django.models import Error_Log, Website_User_Profile
from django.contrib.auth.models import User

from django.core.mail import EmailMessage

import datetime
import pytz
from django.db.models import Q

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

class Command(BaseCommand):
    def add_arguments(self, parser):
        print "no arguments here"

    def handle(self, *args, **options):
        """
        look for demo users who are at or approaching trial limit
        """

        admin_address = "emily@steama.co"
        sales_address = "ryan@steama.co, emily@steama.co"

        demo_users = User.objects.filter(website_user_profile__is_demo=True)
        exp_users = list()
        recent_users = list()

        for user in demo_users:

            created = user.date_joined # date login was registered
            send_email = None # email content null by default

            days_ago = (datetime.datetime.today().replace(tzinfo=pytz.timezone(TIME_ZONE)) - created).days

            # first check if any accounts are past expired
            if days_ago > 30:
                # initially, just report expired users;
                # eventually, start deleting them
                exp_users.append([user.username, user.email, user.date_joined, user.last_login])
                
            # if trial has just expired, reset password and send out notice
            elif days_ago == 30:
                password = User.objects.make_random_password()
                user.set_password(password)
                send_email = "Dear %s,\n\nYour Steama trial has expired. If you would like to subscribe to the service at any time, just notify us by email at support@steama.co. Thank you for trying Steama!\n\nBest wishes,\nThe SteamaCo Team" % user.first_name

            elif days_ago == 25:
                send_email = "Dear %s,\n\nYour Steama trial will expire in 5 days. If you have any questions about the service, or if you would like to subscribe, just send us an email at support@steama.co.\n\nBest wishes,\nThe SteamaCo Team" % user.first_name

            elif days_ago < 1:
                recent_users.append(user.username + " (" + user.email + ") - Created: " + str(user.date_joined) + ", Last Login: " + str(user.last_login))
                

            if send_email is not None:
                
                email = EmailMessage(subject='Your Steama Trial', body=send_email, to=[user.email])
                try:
                    email.send()
                except:
                    pass # if there is an error, continue with other accounts


        if len(exp_users) > 0:
            admin_email = EmailMessage(subject='Expired User Accounts', body=str(exp_users), to=[admin_address])
            admin_email.send()

        if len(recent_users) > 0:
            string_list = ""
            for row in recent_users:
                string_list += "%s\n" % row
            content = "The following is a list of users created in the last day:\n\n%s\n\nNote: If last login time is equal to the time created, the user may not have logged on yet." % string_list
            
            sales_email = EmailMessage(subject='New User Accounts', body=content, to=[sales_address])
            sales_email.send()
