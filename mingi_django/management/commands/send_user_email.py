import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from django.contrib.auth.models import User
from django.core.mail import EmailMessage


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('subject',help="Instructions: python manage.py [subject] [full message text (leaving out Dear ___)]")
        parser.add_argument('content',help="Instructions: python manage.py [subject] [full message text (leaving out Dear ___)]")
        parser.add_argument('-o',nargs=1,help="optional user [-o user]")

    def handle(self, *args, **options):
        """
        Updates the hourly power rollups for every site for the past week.
        """
        
        if not options['o']:
            subject = options['subject']
            message_content = str(options['content']).split('\\n')
            users = User.objects.all()

        elif options['o']:
            subject = options['subject']
            message_content = str(options['content']).split('\\n')
            try:
                users = User.objects.filter(username=options['o'][0])
            except Exception:
                return "Unrecognized username!"
        else:
            return "Instructions: python manage.py [subject] [full message text (leaving out Dear ___)]"
        
        for user in users:
            address = user.email
            message = "Dear " + user.first_name + ",\n\n"
            for row in message_content:
                if row:
                    message += row + "\n"
                else:
                    message += "\n"

            # add default footer
            message += "\n\n\n----------------------------------\nIf you would like to stop receiving these emails, please contact support@steama.co and we will remove your address."

            email = EmailMessage(subject,message,to=[address])
            try:
                email.send()
                print "email sent to address: " + address
            except:
                print "email not sent to address: " + address

        return "Message(s) sent successfully!"
        
