# import datetime
from datetime import datetime, timedelta #, tzinfo
import datetime

import csv
import math
import pytz
from django.db.models import Q
from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
import sys

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output

from mingi_site.models import Bit_Harvester,Mingi_Site
from mingi_django.models import Website_User_Profile, Error_Log
from django.core.management.base import BaseCommand, CommandError

from django.utils.timezone import now

from django.contrib.auth.models import User

import pytz
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


class Command(BaseCommand):
    
    def add_arguments(self, parser):
        print "no arguments here"


    def handle(self, *args, **options):
        """
        Check Bitharvester status
        Format: manage.py check_BH_status [hours]
        Default hours = 3
        """

        # List of bithavesters, where no communication has been received from
        data_list = list()

        all_users = User.objects.all()
        all_bhs = Bit_Harvester.objects.all().exclude(site__is_active=False)

        # check to see whether BHs are making logs as expected
        check_msg_frequency(all_bhs, all_users)
        
        # check to see whether BHs are responding to commands as expected
        check_msg_ratio(all_bhs)


def check_msg_frequency(all_bhs, all_users):
    """
    iterate through list of BHs
    send error email / alert if it fails to make logs as expected
    """
    
    TODAY = gen_zone_aware(TIME_ZONE,datetime.datetime.today())

    # Get site owner details
    # Backtrack to get user details, complexity could be exponential
    for bh in all_bhs:
        print bh.harvester_id
        HOURS = bh.logging_hours * 3
        data_list = list()
        start_hour = gen_zone_aware(TIME_ZONE,datetime.datetime.now()) - timedelta(hours=HOURS)
        all_input_messages = Africas_Talking_Input.objects.filter(timestamp__gte=start_hour, bit_harvester=bh, raw_message__icontains='METERS')
        print "MSGS: " + str(len(all_input_messages))

        if len(all_input_messages.filter(bit_harvester=bh))<1 and HOURS != 999:
            m_site = bh.site            
            Error_Log.objects.create(problem_type="WARNING",
                                     problem_message="bitHarvester: %s has not made any reports in the past %d hours" % (str(bh),HOURS),
                                     site=m_site)
            
            for owner in m_site.owners.all():                    
                for u_user in all_users.filter(username=owner,website_user_profile__send_emails=True):
                    log_data = [str(u_user.email),m_site,str(bh.telephone),str(bh)]
                    data_list.append(log_data)
                        
            # If no data all rollups exist, don't send reports
            if len(data_list) > 0:

                print "Sending email ..."
                # Send email to site owners
                # Send emails only once a day
                start_date = TODAY.replace(hour=0,minute=0,second=0,microsecond=0)
                end_date = start_date + timedelta(days=1)
                all_logs = Error_Log.objects.filter(timestamp__range=(start_date,end_date),problem_type='LOG_DATA')
                for row in data_list:
                    if len(all_logs.filter(creator=row[0],site=row[1]))==1:
                        email = EmailMessage("BitHarvester Status Report","Attention: The bitharvester ("+row[3]+") for site ("+str(row[1])+") has not made any meter readings in the last 3 hours",to=[row[0]])
                    
                        try:
                            email.send()
                        except:
                            pass
                        
    return "success"


def check_msg_ratio(all_bhs):
    """
    run through list of BHs
    create an alert if ratio of commands to responses is significantly off
    """
    
    end_timestamp = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
    end_timestamp_buffer = end_timestamp - datetime.timedelta(hours=1)
    start_timestamp = end_timestamp - datetime.timedelta(hours=6)
    
    for bh in all_bhs:
        commands_sent = len(Africas_Talking_Output.objects.filter(bit_harvester=bh,timestamp__gte=start_timestamp,timestamp__lt=end_timestamp_buffer))
        bh_responses = len(Africas_Talking_Input.objects.filter(bit_harvester=bh, raw_message__contains="OK", timestamp__gte=start_timestamp, timestamp__lt=end_timestamp))
        
        # if there are no commands or responses, msg_ratio will pass
        if commands_sent == 0 and bh_responses == 0:
            msg_ratio = 0
            
        # if there are significant commands and no responses, trigger an error
        elif commands_sent > 5:
            msg_ratio = 100

        # otherwise, calculate the exact ratio
        else:
            try:
                msg_ratio = float(commands_sent) / float(bh_responses)
            except ZeroDivisionError: 
                msg_ratio = 100
        
        if msg_ratio > 3:
            Error_Log.objects.create(problem_type="WARNING",
                                     site=bh.site,
                                     problem_message="bitHarvester: %s has been unresponsive to line-switching commands. Please check that SMS are being received by the bitHarvester" % str(bh))

    return
