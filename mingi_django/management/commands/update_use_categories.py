import datetime
from django.db.models import Q

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from mingi_site.models import Mingi_Site, Daily_Power
from mingi_user.models import Mingi_User

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

ALGORITHM_DAYS=10

class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        looks at most recent energy use data to update customers' energy use
        category as appropriate
        """

        all_sites = Mingi_Site.objects.filter(is_active=True)
        all_sites = all_sites.exclude(use_categories="")
        end_date = gen_zone_aware(TIME_ZONE,datetime.date.today())
        start_date = datetime.date.today() - datetime.timedelta(days=ALGORITHM_DAYS)

        for site in all_sites:
            
            print site
            # get use categories for that site
            categories = site.use_category_dict
            
            users = Mingi_User.objects.filter(site=site).order_by('id')
            rollups = Daily_Power.objects.filter(user__isnull=False, user__site=site, timestamp__gte=start_date, timestamp__lt=end_date)
            user_list = list()

            # collect necessary information in limited database queries
            index = 0
            for user in users:
                user_list.append([user])

                for rollup in rollups:
                    if rollup.user == user:
                        user_list[index].append(rollup)

                index += 1

            for row in user_list:
                print "next row: " + str(row)
                user = row[0]
                avg_use = get_user_avg_power(row)
                user.use_category = get_use_category(categories, avg_use)
                user.save()
                print "Saved user: %s in category: %d" % (str(user),int(user.use_category))



# ALGORITHM: ATTEMPT #1
def get_user_avg_power(row):
    """
    calculate average power use of the user in row 
    according to weighted algorithm
    """

    user = row[0]
    avg_days = len(row) - 1

    if avg_days != ALGORITHM_DAYS:
        print "Incomplete number of daily rollups found"

    use_sum = 0.0
    for i in range (1,len(row)):
        # calculate avg in Wh, while rollups are stored in kWh
        use_sum += float(row[i].amount) * 1000

    try:
        total = (use_sum / avg_days)
        print str(use_sum) + " / " + str(avg_days) + " = " + str(total)
        return total
    except ZeroDivisionError:
        return 0.0
    

def get_use_category(categories, avg_use):
    """
    return the index of the category which applies to the input avg_use
    """

    last_category_index = -1

    for row in categories:
        print "Category: " + str(row)
        if avg_use < float(row['use_floor']):
            break
        else:
            last_category_index += 1
            print row['use_floor']

    return last_category_index
            
    
def print_use_categories(site_name):
    """
    debugging helper method that prints all users and their 
    updated category names
    """

    site = Mingi_Site.objects.get(name=site_name)
    categories = site.use_category_dict

    users = Mingi_User.objects.filter(site=site,use_category__isnull=False)
    
    for user in users:
        category = user.use_category
        print "%s: Category - %s, Use Threshold - %s Wh" % (str(user),categories[category]['name'], categories[category]['use_floor'])

            
        
