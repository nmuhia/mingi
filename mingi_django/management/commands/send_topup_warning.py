from django.core.management.base import BaseCommand, CommandError

from africas_talking.models import Africas_Talking_Output, Africas_Talking_Input
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site
from mingi_django.models import Error_Log
from africas_talking.handleSMS import sendSMS

from operator import itemgetter
from django.core.mail import EmailMessage

import datetime

#SITE_LIST = ['Testing World']

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('site_name',help="Use syntax: python manage.py daily_customer_sms [Site Name] [Message Type #]")
        parser.add_argument('msg_type',help="Use syntax: python manage.py daily_customer_sms [Site Name] [Message Type #]")
        parser.add_argument('msg_name',help="Use syntax: python manage.py daily_customer_sms [Site Name] [Message Type #]")
    
    def handle(self, *args, **options):
        """
        sends a daily summary SMS to customers at any of the sites in SITE_LIST
        """

        # if len(args) != 2:
        #     print "Use syntax: python manage.py daily_customer_sms [Site Name] [Message Type #]"
        #     return

        # else:
        #     siteName = args[0]
        siteName = options['site_name']
        msg_type = options['msg_type']
        msg_name = options['msg_name']

        if sitename == "ALL":
            site_users = Mingi_User.objects.filter(is_user=True)
        else:
            site_users = Mingi_User.objects.filter(site__name=siteName,
                                                   is_user=True)

        if msg_type == "1":
            msgOut = "Dear Valued Customer, MPESA is experiencing delays in delivering payments. We recommend that you top up early in the day to have your account credited in the evening. Thanks!"

        for user in site_users:

            if msg_name == "NAME":
                if user.telephone[-3:] > 500:
                    credit_amt = 100
                else:
                    credit_amt = 50

                if user.language == "ENG":
                    msgOut = "Dear STEAMA customer:  "
                    
                else:
                    msgOut = "Jambo wateja: "

                    
            elif msg_type == "2":
                if user.language == "ENG":
                    msgOut = "Dear valued customer: There will be blackouts today as our team upgrades the system. We apologize for any inconvenience caused."
                else:
                    msgOut = "Jambo wateja: kutakuwa na ukosefu wa umeme leo. Fundi wetu wanashugulikia mitambo ikufaidi zaidi. Pole kwa leo."

                    
            try:
                sendSMS(user.telephone,msgOut,user)
            except Exception as e: 
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

