from django.core.management.base import BaseCommand, CommandError

from africas_talking.models import Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Daily_Power, Line_State, Site_Record, Hourly_Site_Revenue, Hourly_Site_Power
from report_functions import get_header, get_email_subject

from datetime import datetime, timedelta #, tzinfo
import datetime

import csv
import math
import pytz
from django.db.models import Q
from django.core.mail import EmailMessage
from django.utils import tzinfo
from django.utils import timezone
import sys
import os, tempfile, zipfile
import StringIO
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware


CURRENT=240
THIS_TZ=timezone.get_default_timezone()

ENERGY_REPORT_FILENAME = "energy_csv_data.csv"
ZIP_ENERGY_REPORT_FILENAME = "energy_csv_data.zip"

#Set the maximum limit for daily power 

ENERGY_MAX_FAULT_LIMIT=100000
ENERGY_MIN_FAULT_LIMIT=0 
ENERGY_LINE_MIN_FAULT_LIMIT=500000

FAULTY_VOLTAGE_STATUS=False
FAULTY_ENERGY_READING_STATUS=False
ERRORS_LIST = list()

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('month',help="function call instructions: python manage.py get_energy_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] -o [user*]")
        parser.add_argument('year',help="function call instructions: python manage.py get_energy_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] -o [user*]")
        parser.add_argument('email',help="function call instructions: python manage.py get_energy_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] -o [user*]")
        parser.add_argument('-o',nargs=1,help='optional user')

    def handle(self, *args, **options):
        """
        return a csv form with energy use data from a given month
        """
        title = "Mingi Energy Report"

        global FAULTY_VOLTAGE_STATUS

        # if not options['o'] and not options['o1']
        # # if len(args) != 3 and len(args) != 4:
        #     print "function call instructions: python manage.py get_energy_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] [user*]"
        #     return

        try:
            month = options['month']
            year = options['year']
            # month = int(args[0])
            # year = int(args[1])
            start_date = gen_zone_aware(TIME_ZONE,datetime(year,month,1,0,0,0,0))
            
            if month == 12:
                end_date = gen_zone_aware(TIME_ZONE,datetime(year+1,1,1,0,0,0,0))
            else:
                end_date = gen_zone_aware(TIME_ZONE,datetime(year,month+1,1,0,0,0,0))

        except:
            start_string = options['month']
            end_string = options['year']
            start_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(start_string, '%d-%m-%y'))
            # include end date in filter by getting data less than the day after
            end_date = gen_zone_aware(TIME_ZONE,datetime.datetime.strptime(end_string,'%d-%m-%y') + timedelta(days=1))

        # extract email
        try:
            email = options['email'][0]
            file_format = options['email'][1]
        except Exception as e:
            email = options['email']
            file_format = "plain"


        filename = ENERGY_REPORT_FILENAME
        zip_filename = ZIP_ENERGY_REPORT_FILENAME

        if options['o']:
        # if len(args) == 4:
            user = options['o'][0]
        else:
            user = None

        # *** add in a user filter soon ***
        if user and user.is_staff == False:
            all_sites = Mingi_Site.objects.filter(owners=user).order_by('id')
        else:
            all_sites = Mingi_Site.objects.filter(is_active=True).order_by('id')

        data_vals = list()

        for site in all_sites:
            row = get_data_values(start_date,end_date,site)
            data_vals.append(row)

        # write data to csv file and send as attachment 
        with open(filename,'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(title, start_date, end_date)
            for row in header:
                writer.writerow(row)

            # If there are any faulty readings detected, print a notification on the print out
            if FAULTY_VOLTAGE_STATUS == True or FAULTY_ENERGY_READING_STATUS == True:
                writer.writerow([""])
                writer.writerow(['There were some faults in energy reading. Kindly see Errors section for details'])
                writer.writerow([""])

            writer.writerow([""])
            # write report-specific notes and column headers
            writer.writerow(['Site Name','Total Energy Use (kWh)','Total Energy Use - Commercial Users (kWh)','Total Energy Use - Residential Users (kWh)','Total Energy Generation - Solar (kWh)','Total Energy Generation - Generator (kWh)','Average Voltage (V)'])
            
            for row in data_vals:
            
                writer.writerow([str(row[0]),str(row[1]),str(row[2]),str(row[3]),str(row[4]),str(row[5]),str(row[6])])


            # Write error correction details here
            if FAULTY_VOLTAGE_STATUS == True or FAULTY_ENERGY_READING_STATUS == True:
                writer.writerow([""])
                writer.writerow(['Errors:'])
                for error in ERRORS_LIST:
                    user = error[0]
                    amount = error[1]
                    date = error[2]
                    replaced_by = error[3]
                    writer.writerow(['Value for user at: %s on date: %s recorded as %.2f - this value was assumed incorrect and was replaced by average value: %.2f' % (user.site.name,str(date),float(amount),float(replaced_by))])
                writer.writerow([""])


        # compile and send email
        subject = get_email_subject(title, start_date, end_date)

        email = EmailMessage(subject,'data is attached',to=[email])

        if file_format == "zip":
            # compress file
            # open file for writing
            zipped_file = zipfile.ZipFile(zip_filename, mode='w')
            try:
                # compress file
                zipped_file.write(filename,compress_type=compression)
            except Exception as e:
                # if error, send uncompressed file
                print e
            finally:
                zipped_file.close()
                
            email.attach_file(zip_filename)
        else:
            email.attach_file(filename)

        email.send()
        print str(email)+":Email send successfully"


def get_data_values(start_date, end_date, site):
    """
    return energy use for site between start_date and end_date
    """

    # calculate raw from data so table can be used for error-checking
    total_use = 0.0
    res_use = 0.0
    com_use = 0.0
    total_gen = 0.0
    genset_gen = 0.0

    previous_energy=0.0 # Store last previous valid energy
    energy_used =0.0 # Enegy for a given user end point in a day

    global FAULTY_ENERGY_READING_STATUS
    global ERRORS_LIST

    # Get rollups in the given site
    rollups = Daily_Power.objects.filter(user__isnull=False,timestamp__gte=start_date,timestamp__lt=end_date, user__site=site)

    users = Mingi_User.objects.filter(site=site,is_user=True,line_number__isnull=False)

    data_list = [[] for user in users]
    # Loop through all days to identify any faulty readings
    while start_date < end_date :
        # Day roll ups
        day_rollups = rollups.filter(timestamp=start_date)
        # Loop through daily roll ups to identify user types
        for rollup in day_rollups:
            user = rollup.user
            # first look for a generator account
            if user.line_number in site.source_line_array:
                energy_used = float(rollup.amount)

                # Check faulty in data
                if energy_used < ENERGY_MIN_FAULT_LIMIT or energy_used > ENERGY_MAX_FAULT_LIMIT :
                    energy_used = previous_energy
                    FAULTY_ENERGY_READING_STATUS=True
                    ERRORS_LIST.append([user,rollup.amount,rollup.timestamp,previous_energy])

                genset_gen += energy_used
                previous_energy = energy_used
                    # tally data for a commercial user
            elif user.user_type == "BUS":
                energy_used = float(rollup.amount)

                # Check faulty in data
                if energy_used < ENERGY_MIN_FAULT_LIMIT or energy_used > ENERGY_MAX_FAULT_LIMIT :
                    energy_used = previous_energy
                    FAULTY_ENERGY_READING_STATUS=True
                    ERRORS_LIST.append([user,rollup.amount,rollup.timestamp,previous_energy])

                com_use += energy_used
                previous_energy = energy_used

            else :
                energy_used = float(rollup.amount)

                # Check faulty in data
                if energy_used < ENERGY_MIN_FAULT_LIMIT or energy_used > ENERGY_MAX_FAULT_LIMIT :
                    energy_used = previous_energy
                    FAULTY_ENERGY_READING_STATUS=True
                    ERRORS_LIST.append([user,rollup.amount,rollup.timestamp,previous_energy])

                res_use += energy_used
                previous_energy = energy_used
            

            total_use += energy_used

        start_date+=datetime.timedelta(days=1)

   

    [avg_voltage,voltage_diff] = get_voltage_values(start_date, end_date, site)

    # assume solar generation equals the total generation (total use plus change in energy stored) minus contribution from the metered power sources (genset)
    solar_gen = "computation incomplete" # total_use + voltage_diff*CURRENT - genset_gen

    return [site.name, total_use, com_use, res_use, solar_gen, genset_gen, avg_voltage]


def get_voltage_values(start_date, end_date, site):
    """
    return energy production for site between start_date and end_date
    """

    voltages = list(Site_Record.objects.filter(site=site,
                                               key="BH Data Log",
                                               timestamp__gte=start_date,
                                               timestamp__lte=end_date))
    # Record all faulty values in voltage reading
    faulty_reading=list()
    global FAULTY_VOLTAGE_STATUS
    # handle empty queryset
    if len(voltages) == 0:
        return [0.0,0.0]
    else:
        start_value = float(voltages[0].value.split(',')[0]) * site.voltage_scale

    # handle queryset with no voltage spread (length = 1)
    if len(voltages) == 1:
        # Check whether the reading is faulty
        if start_value < ENERGY_MIN_FAULT_LIMIT:
            faulty_reading.append(start_value)
            start_value=0
            FAULTY_VOLTAGE_STATUS=True

        return [start_value,0.0]
    
    total = 0.0
    
    
    for log in voltages:
        value = float(log.value.split(',')[0]) * site.voltage_scale
        # Check whether the reading is faulty
        if value < ENERGY_MIN_FAULT_LIMIT or value > ENERGY_MAX_FAULT_LIMIT :
            faulty_reading.append(value)
            value=0
            FAULTY_VOLTAGE_STATUS=True

        total += value
        last_value = value

    # if there are faulty readings found
    if len(faulty_reading) > 0:
        # get number of faulty redings and new average
        old_average = total / (len(voltages)-len(faulty_reading))
        total += old_average * len(faulty_reading)
        FAULTY_VOLTAGE_STATUS = True
        
    average = total / len(voltages)

    difference = last_value - start_value

    return [average,difference]
                                       
