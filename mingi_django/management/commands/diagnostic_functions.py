from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_django.models import Website_User_Profile, Error_Log
from mingi_site.models import Mingi_Site, Line_State, Bit_Harvester, Hourly_Site_Power, Hourly_Site_Revenue, Daily_Power, Site_Record
import datetime
from datetime import date
import pytz
from django.utils import timezone
from django.db.models import Q
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

#import html


CUSTOMER_ENERGY_USAGE_THRESHOLD = 1.0
MINIMUM_DAILY_LOGS = 8

def tally_transactions(siteName=None):
    """
    get a summary of transactions made 
    """
    
    nowtime = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
    lastAccessTS = nowtime - datetime.timedelta(hours=24)
    site_list = Mingi_Site.objects.all().exclude(name="Limbo").exclude(name__contains="Test")
    
    # if a site is specified, filter results for that site only
    if (siteName):

        msgsIn = Africas_Talking_Input.objects.filter(Q(timestamp__gt=lastAccessTS),Q(user__site__name=siteName) | Q(bit_harvester__site__name=siteName))

        BHlogs = msgsIn.filter(raw_message__contains="*0")

        msgsOut = Africas_Talking_Output.objects.filter(Q(timestamp__gt=lastAccessTS),Q(user__site__name=siteName) | Q(bit_harvester__site__name=siteName))

        payments = Kopo_Kopo_Message.objects.filter(timestamp__gt=lastAccessTS, user__site__name=siteName)

        return [len(msgsIn),len(BHlogs),len(msgsOut),len(payments)]

    # otherwise return totals
    else:

        msgsIn = Africas_Talking_Input.objects.filter(timestamp__gt=lastAccessTS)
        BHlogs = msgsIn.filter(raw_message__contains="*0")
        msgsOut = Africas_Talking_Output.objects.filter(timestamp__gt=lastAccessTS)
        payments = Kopo_Kopo_Message.objects.filter(timestamp__gt=lastAccessTS)

        return [len(msgsIn),len(BHlogs),len(msgsOut),len(payments)]


def find_user_issues(siteName=None):
    """
    run error-checking on all current Mingi_Users
    """
    
    if (siteName):
        users = Mingi_User.objects.filter(site__name=siteName, is_user=True, line_number__isnull=False).exclude(line_number='')
    else:
        users = Mingi_User.objects.filter(is_user=True, line_number__isnull=False).exclude(site__name="Limbo").exclude(site__name__contains="Test").exclude(line_number='') # later: filter by is_active (?)
    
    tech_issues = list()
    customer_issues = list()
    
    for user in users:
        #check for customer energy usage
	if(last_week_energy_usage(user) < CUSTOMER_ENERGY_USAGE_THRESHOLD):
            customer_issues.append([user.site.name,user.first_name,user.last_name,
                           user.account_balance, user.low_balance_warning,
                           user.line_is_on,"Energy usage is 0 WH in the past 14 days"])

        # check for account balance issues
        if (user.account_balance <= -1*user.low_balance_warning):
            tech_issues.append([user.site.name,user.first_name,user.last_name,
                           user.account_balance, user.low_balance_warning,
                           user.line_is_on,"Account balance is low"])
            
        # check for line status issues
        elif (user.account_balance > 0) and (user.line_is_on == False):
            tech_issues.append([user.site.name,user.first_name,user.last_name,
                           user.account_balance, user.low_balance_warning,
                           user.line_is_on,"Line should be ON"])
            
        elif (user.account_balance <= 0) and (user.line_is_on == True):
            tech_issues.append([user.site.name,user.first_name,user.last_name,
                           user.account_balance, user.low_balance_warning,
                           user.line_is_on,"Line should be OFF"])

    return [tech_issues, customer_issues]


def site_recipients():
    """
    get a list of all owner-site combinations for which diagnostic emails 
    should be generated
    """
    
    site_recipients = list()
    recipients = Website_User_Profile.objects.filter(send_emails=True)
    
    # get all sites (except for Test sites)
    sites = Mingi_Site.objects.filter(is_active=True)
    
    for site in sites:
        #get all site owners
        #for each site loop through all the recipients and check whether they are owners, if they are add them to email list 
        owners = site.owners.all()
        site_email_list = list()
        site_email_list.append(site.name)
        for owner in owners:
            for recipient in recipients:
                if (owner.is_active) and (owner == recipient.user):                                    
                    site_email_list.append(str(recipient.user.email))

        site_recipients.append([site_email_list])
    
    return site_recipients


def todays_energy_usage(user):
    """
      check energy usage for a user for current date
    """
    today=date.today()
    todays_reading=0

    todays_line_states=Line_State.objects.filter(user=user,timestamp__contains=today).order_by('processed_timestamp')
    todays_line_states=todays_line_states.exclude(meter_reading=None)

    
    if(len(todays_line_states)<=1):
        return 0.0
    else:
        last_index = len(todays_line_states) - 1
        todays_reading = 1000*(todays_line_states[last_index].meter_reading - todays_line_states[0].meter_reading)
        return todays_reading            
   

def last_week_energy_usage(user):
    """
      check energy usage for a user for previous week
    """

    today = gen_zone_aware(TIME_ZONE,date.today())
    last_14_days = gen_zone_aware(TIME_ZONE,timezone.now()) - datetime.timedelta(days=14)
    
    last_week_line_states=Line_State.objects.filter(user=user,timestamp__gte=last_14_days).order_by('processed_timestamp')

    last_week_line_states= last_week_line_states.exclude(meter_reading=None)
                 
    if(len(last_week_line_states)<=1):
        return 0.0
    else:
        last_index = len(last_week_line_states) - 1
        last_week_reading = 1000*(last_week_line_states[last_index].meter_reading - last_week_line_states[0].meter_reading)
        return last_week_reading      


def check_bit_harvester_log_issues(siteName=None):
    """
       check for bitharvester logs
    """
    nowtime = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
    last_24_hours = nowtime - datetime.timedelta(hours=24)
    last_3_hours = nowtime - datetime.timedelta(hours=3)
    
    #bit harvesters list
    if (siteName):
        bhs = Bit_Harvester.objects.filter(site__name=siteName)
    else:
        bhs = Bit_Harvester.objects.all().exclude(site__name="Limbo").exclude(site__name__contains="Test").exclude(site__is_active=False)
       
    logs_issues = list()
 
    for bh in bhs:
        bh_logs = Africas_Talking_Input.objects.filter(Q(bit_harvester=bh),Q(raw_message__contains="*0"),Q(raw_message__contains="METER"))
        if bh.version == "V4":
            bh_data_logs = Africas_Talking_Input.objects.filter(bit_harvester=bh,raw_message__contains="DATA",timestamp__gte=last_24_hours)
        else:
            bh_data_logs = list() # empty list
   
        bh_last_24hours_log=bh_logs.filter(timestamp__gte=last_24_hours)
        bh_last_3hours_log=bh_logs.filter(timestamp__gte=last_3_hours)
        logging_hours = float(bh.logging_hours)


        if(len(bh_last_24hours_log)<1):
            logs_issues.append([str(bh.site.name)," BH "+bh.harvester_id," has not made any logs in the past 24 hours"])            

        elif(len(bh_last_24hours_log) > 0 and (len(bh_last_24hours_log) < MINIMUM_DAILY_LOGS)):
            logs_issues.append([str(bh.site.name), " BH "+bh.harvester_id," has not sent logs at least 8 times in the last 24 hours"])

        elif(len(bh_last_24hours_log) > 0 and (len(bh_last_3hours_log) < 1)):
            logs_issues.append([str(bh.site.name), " BH "+bh.harvester_id," has not made any logs in the past 3 hours"])

        # check for BH logging hours less than 20 minutes
        if bh.logging_hours < 0.3:
            logs_issues.append([str(bh.site.name), " BH "+bh.harvester_id," has suspicious logging hours ("+str(logging_hours)+")"])

        # check for DATA messages that show CONNECT comms issues
        count = 0
        for msg in bh_data_logs:
            elements = msg.raw_message.split(',')
            if len(elements[4]) > 0 and elements[7] != elements[8]:
                print "MISMATCHED ELEMENTS: " + str(elements[7]) + ", " + str(elements[8])
                count += 1

        if count > 0:
            logs_issues.append([bh.site.name," BH "+bh.harvester_id," has shown signs of incomplete communication with CONNECT boards " + str(count) + " times in the last 24 hours"])

    return logs_issues
          

def flag_site_params(site):
    """
    look for flagged scenarios in site parameter logs
    """

    # define threshold date and datetimes
    today = datetime.datetime.today()

    today = today.replace(tzinfo=pytz.timezone(TIME_ZONE))
    yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
    yesterday = yesterday.replace(tzinfo=pytz.timezone(TIME_ZONE))
    two_days_ago = datetime.datetime.today() - datetime.timedelta(days=2)
    two_days_ago = two_days_ago.replace(tzinfo=pytz.timezone(TIME_ZONE))
    three_days_ago = datetime.datetime.today() - datetime.timedelta(days=3)
    three_days_ago = three_days_ago.replace(tzinfo=pytz.timezone(TIME_ZONE))
    four_days_ago = datetime.datetime.today() - datetime.timedelta(days=4)
    four_days_ago = four_days_ago.replace(tzinfo=pytz.timezone(TIME_ZONE))
    five_days_ago = datetime.datetime.today() - datetime.timedelta(days=5)
    five_days_ago = five_days_ago.replace(tzinfo=pytz.timezone(TIME_ZONE))

    # get energy usage flags
    energy_rollups = Hourly_Site_Power.objects.filter(site=site, timestamp__gt=two_days_ago).order_by('-timestamp')

    ZERO_ENERGY_FLAGS = list()
    today_total = 0.0
    yesterday_total = 0.0
    for rollup in energy_rollups:
        if rollup.timestamp >= yesterday:
            today_total += float(rollup.amount)
            if rollup.amount == 0.0 and rollup.timestamp >= yesterday:
                ZERO_ENERGY_FLAGS.append(rollup)

        else:
            yesterday_total += float(rollup.amount)

    # get best power users
    TOP_5_USERS = list()
    rollups = Daily_Power.objects.filter(user__isnull=False,user__site=site,timestamp__year=yesterday.year,timestamp__month=yesterday.month,timestamp__day=yesterday.day).order_by('-amount')
    
    if len(rollups) < 5:
        length = len(rollups)
    else:
        length = 5

    for i in range(0,length):
        TOP_5_USERS.append(rollups[i])

    # get voltage flags
    voltages = Site_Record.objects.filter(timestamp__gt=three_days_ago, key="BH Data Log", site=site).order_by('-timestamp')

    day3max = 0.0
    day2max = 0.0
    day1max = 0.0

    for log in voltages:
        reading = log.value.split(',')[0]
        if log.timestamp > yesterday and reading > day1max:
            day1max = reading
        elif log.timestamp > two_days_ago and reading > day2max:
            day2max = reading
        elif log.timestamp > three_days_ago and reading > day3max:
            day3max = reading
    print "3"
    if day3max > day2max and day2max > day1max:
        DECREASING_VOLTAGE_FLAG = True
    else:
        DECREASING_VOLTAGE_FLAG = False

    # get revenue flags
    revenues = Hourly_Site_Revenue.objects.filter(site=site, timestamp__gt=five_days_ago).order_by('-timestamp')
    print "4"
    day1total = 0.0
    day2total = 0.0
    day3total = 0.0
    day4total = 0.0
    day5total = 0.0

    for rollup in revenues:

        if rollup.timestamp > yesterday:
            day1total += float(rollup.amount)
        elif rollup.timestamp > two_days_ago:
            day2total += float(rollup.amount)
        elif rollup.timestamp > three_days_ago:
            day3total += float(rollup.amount)
        elif rollup.timestamp > four_days_ago:
            day4total += float(rollup.amount)
        elif rollup.timestamp > five_days_ago:
            day5total += float(rollup.amount)
    print "6"
    if day1total == 0.0:
        ZERO_REVENUE_FLAG = True
    else:
        ZERO_REVENUE_FLAG = False

    if day1total < day2total and day2total < day3total and day3total < day4total and day4total < day5total:
        DECREASING_REVENUE_FLAG = True
    else:
        DECREASING_REVENUE_FLAG = False
        
    return [[yesterday_total,today_total],ZERO_ENERGY_FLAGS,TOP_5_USERS,[DECREASING_VOLTAGE_FLAG],[ZERO_REVENUE_FLAG,DECREASING_REVENUE_FLAG]]
