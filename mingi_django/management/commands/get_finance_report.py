from django.core.management.base import BaseCommand, CommandError

from africas_talking.models import Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line_State, Site_Record, Hourly_Site_Revenue, Hourly_Site_Power
from report_functions import get_header, get_email_subject

import csv
import math
from django.db.models import Q
from django.core.mail import EmailMessage
from django.core.files import File

from django.utils.timezone import now,utc
import datetime
from django.utils import timezone
import sys
import os, tempfile, zipfile
import StringIO
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

from mingi_django.transactions import atOutToTable
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware
import pytz

MPESA_RATE = 0.01
FINANCE_REPORT_FILENAME = "finance_csv_data.csv"
ZIP_FINANCE_REPORT_FILENAME = "finance_csv_data.zip"

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('month',help="function call instructions: python manage.py get_finance_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] [user]")
        parser.add_argument('year',help="function call instructions: python manage.py get_finance_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] [user]")
        parser.add_argument('email',help="function call instructions: python manage.py get_finance_report [Month (number)] [Year (full)] [email address] OR [start date (DD-MM-YY)] [end date (DD-MM-YY)] [email address] [user]")
        parser.add_argument('-o',nargs=1,help='optional user')

    def handle(self, *args, **options):
        """
        return csv form with financial data from a given month
        """
        title = "Mingi Financial Report"
        try:
            month = int(options['month'])
            year = int(options['year'])
            # month = int(args[0])
            # year = int(args[1])
            start_date = datetime.datetime(year,month,1,0,0,0,0)
            
            if month == 12:
                end_date = datetime.datetime(year+1,1,1,0,0,0,0)
            else:
                end_date = datetime.datetime(year,month+1,1,0,0,0,0)

        except Exception as e:
            start_string = options['month']
            end_string = options['year']
            start_date = datetime.datetime.strptime(start_string, '%d-%m-%y')
            # include end date in filter by getting data less than the day after
            end_date = datetime.datetime.strptime(end_string,'%d-%m-%y') + datetime.timedelta(days=1)
                     
        # extract email & file format
        try:
            email = options['email'][0]
            file_format = options['email'][1]
        except Exception as e:
            email = options['email']
            file_format = "plain"

        filename = FINANCE_REPORT_FILENAME
        zip_filename = ZIP_FINANCE_REPORT_FILENAME
        
        if options['o']:
        # if len(args) == 4:
            user = options['o'][0]
        else:
            user = None

        # make timestamps timezone aware, UTC
        start_date = timezone.make_aware(start_date,timezone=pytz.timezone(TIME_ZONE))
        end_date = timezone.make_aware(end_date,timezone=pytz.timezone(TIME_ZONE))

        
        # *** add in a user filter soon ***
        if user and user.is_staff == False:
            all_sites = Mingi_Site.objects.filter(owners=user,is_active=True, is_combined=False).order_by('id') # active sites for users
            payments =  Kopo_Kopo_Message.objects.filter(timestamp__gte=start_date,timestamp__lt=end_date,user__site__owners=user,user__site__is_active=True) # payments for active sites
            sms = Africas_Talking_Output.objects.filter(Q(user__site__owners=user) | Q(bit_harvester__site__owners=user),Q(user__site__is_active=True) | Q(bit_harvester__site__is_active=True), timestamp__gte=start_date, timestamp__lt=end_date) #SMS for active site
        else:
            all_sites = Mingi_Site.objects.filter(is_active=True, is_combined=False).order_by('id') # active sites for users
            # get relevant transaction records
            payments = Kopo_Kopo_Message.objects.filter(timestamp__gte=start_date,timestamp__lt=end_date,user__site__is_active=True) # payments for active sites
            sms = Africas_Talking_Output.objects.filter(Q(user__site__is_active=True) | Q(bit_harvester__site__is_active=True),timestamp__gte=start_date,timestamp__lt=end_date) # SMS for active site

        payment_records = list()
        for payment in payments:
            payment_records.append(payment)

        sms_records = list()
        for msg in sms:
            sms_records.append(msg)
        
        # run helper function to get payment data
        pay_site_totals = process_payments(payment_records,all_sites)
 
        # run helper function to get sms data
        sms_site_totals = process_sms(sms_records,all_sites)

        with open(filename,'w') as csvfile:
            writer = csv.writer(csvfile)

            # write header
            header = get_header(title, start_date, end_date)

            for row in header:
                writer.writerow(row)

            # write report-specific notes and column labels
            writer.writerow(['all values in local currency as noted'])
            
            writer.writerow(['Site Name (local currency)','a. Total Revenue','b. AE Till Revenue','c. Client Till Revenue','d. Net Revenue to a:e','e. MPesa Fees','f. Agent Fees','g. Site Manager Salary','h. Site Rent','i. SMS Costs - Total','j. SMS Costs - Human','k. SMS Costs - BH','l. Balance'])
            writer.writerow(['','b. + c.','paid into a:e mpesa till','paid into client mpesa till','% of a. depending on a:e fee basis','1% of a.','calculated as % of a.','flat fee per month','flat fee per month','j. + k.','1 KES per SMS sent from Mingi to grid users','1 KES per SMS sent from Mingi to BH','a. - e. - f. - g. - h. - i.'])

            # write data to csv file
            row_num = 0
            for pay_row, sms_row in zip(pay_site_totals,sms_site_totals):

                try:
                    site = Mingi_Site.objects.get(name=pay_row[0])
                    
                    # only write active sites
                    if (site.is_active == True):
                        total_in = pay_row[3]*float(site.tariff_terms)
                        mpesa_cost = pay_row[3]*MPESA_RATE
                        agent_cost = pay_row[3]*float(site.agent_fees)
                        sm_cost = float(site.site_manager_fees)
                        site_cost = float(site.monthly_rent)
                        total_out = agent_cost + sm_cost + site_cost + sms_row[3] + mpesa_cost
                        
                        site_title = site.name + " (" + site.currency + ")"
                        writer.writerow([site_title,str(pay_row[3]),str(pay_row[1]),str(pay_row[2]),str(total_in),str(mpesa_cost),str(agent_cost),str(sm_cost),str(site_cost),str(sms_row[3]),str(sms_row[1]),str(sms_row[2]),str(pay_row[3] - total_out)])
                except IndexError:
                    pass
                except Exception:
                    pass
                
                row_num += 1
        # compile and send email
        subject = get_email_subject(title, start_date, end_date)
        email = EmailMessage(subject,'Your data is attached',to=[email])

        if file_format == "zip":
            # compress file
            # open file for writing
            zipped_file = zipfile.ZipFile(zip_filename, mode='w')
            try:
                # compress file
                zipped_file.write(filename,compress_type=compression)
            except Exception as e:
                # if error, send uncompressed file
                print e
            finally:
                zipped_file.close()
                
            email.attach_file(zip_filename)
        else:
            email.attach_file(filename)

        email.send()
        print "email has been sent successfully"

            
def process_payments(payments,all_sites):
    """
    calculate relevant financial data from payments and write to file
    """
    site_totals = [[] for i in range(0,all_sites[len(all_sites)-1].id)]

    ae_total = 0.0
    pg_total = 0.0
    total = 0.0

    AE_TILL = "934135" # Access Energy Till
    PG_TILL = "397266" # Power Gen Till


    # create array to store site totals
    for site in all_sites:
        index = site.id - 1
        site_totals[index] = [site.name,0.0,0.0,0.0]

    for payment in payments:
        try:
            # increment AE or PG totals (site and overall) as appropriate
            index = payment.user.site.id - 1

            if AE_TILL in payment.raw_message:
                ae_total += float(payment.amount)
                site_totals[index][1] += float(payment.amount)
            elif PG_TILL in payment.raw_message:
                pg_total += float(payment.amount)
                site_totals[index][2] += float(payment.amount)
            else:
                ae_total += float(payment.amount)
                site_totals[index][1] += float(payment.amount)
                
            site_totals[index][3] += float(payment.amount)
            total += float(payment.amount)
        except IndexError:
            pass
        except AttributeError:
            # For: users without sites or payments without users, add totals
            total += float(payment.amount)
            site_totals[index][3] += float(payment.amount)
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Finance payments report error: Attribute error on " + str(payment))
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Finance payments report error: " + str(e))
        # increment counter
        i += 1

    return site_totals


def process_sms(sms,all_sites):
    """
    calculate relevant financial data from paymets and write to file
    """
    site_totals = [[] for i in range(0,all_sites[len(all_sites)-1].id)]

    human_total = 0.0
    bh_total = 0.0
    total = 0.0

    # create array to store site totals
    for site in all_sites:
        index = site.id - 1
        try:
            site_totals[index] = [site.name,0.0,0.0,0.0]
        except IndexError as e:
            print e
    
    for msg in sms:
        cost = math.ceil(float(len(msg.raw_message)) / 150)
        
        try:
            if msg.user:
                index = msg.user.site.id - 1
                site_totals[index][1] += cost 
                human_total += cost
            elif msg.bit_harvester:
                index = msg.bit_harvester.site.id - 1
                site_totals[index][2] += cost
                bh_total += cost
            else:
                site_totals[index][1] += cost

            site_totals[index][3] += cost
            total += cost

        except IndexError as e:
            print str(e)
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Finance SMS report error: " + str(e))
            pass

    return site_totals

