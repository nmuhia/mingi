from django.core.management.base import BaseCommand

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_user.models import Mingi_User
from mingi_site.models import Hourly_Site_Power, Hourly_Site_Revenue, Line_State


class Command(BaseCommand):

    def add_arguments(self, parser):
        """
        Accepts one mandatory datatype, and optional date range
        """
        # parser.add_argument('args')
        parser.add_argument('data_type',help="This is datatype. Please enter one of the following data types: Line_State, Power_Rollup, Revenue_Rollup, Payment, Mingi_User")
        parser.add_argument('-o',nargs=2,help="rule: python manage.py check_db data_type [start_ts] [end_ts]")


    def handle(self, *args, **options):
        """
        delete duplicate entries from database
        """
        data_type = options['data_type']

        if options['o']:
            start_ts = options['o']
            end_ts = options['o']

        # if len(args) == 1:
        #     data_type = args[0]
        # elif len(args) == 3:
        #     data_type = args[0]
        #     start_ts = args[1]
        #     end_ts = args[2]
        # else:
        #     print "Please enter one of the following data types: Line_State, Power_Rollup, Revenue_Rollup, Payment, Mingi_User"
        #     return

        if data_type == "Line_State":
            objects = Line_State.objects.all()
            
            for log in objects:
                print log
                try:
                    Line_State.objects.get(site=log.site, number=log.number, timestamp=log.timestamp)
                    
                except Line_State.MultipleObjectsReturned:
                    repeat = Line_State.objects.filter(site=log.site, number=log.number, timestamp=log.timestamp)[1]
                    repeat.delete()
                    print "Deleted: %s" % str(repeat)

        elif data_type == "Power_Rollup":
            objects = Hourly_Site_Power.objects.all()

            for log in objects:
                try:
                    Hourly_Site_Power.objects.get(site=log.site, timestamp=log.timestamp)
                    
                except Hourly_Site_Power.MultipleObjectsReturned:
                    repeat = Hourly_Site_Power.objects.filter(site=log.site, timestamp=log.timestamp)[1]
                    repeat.delete()
                    print "Deleted: %s" % str(repeat)
            
        elif data_type == "Revenue_Rollup":
            objects = Hourly_Site_Revenue.objects.all()
            
            for log in objects:
                try:
                    Hourly_Site_Revenue.objects.get(site=log.site, timestamp=log.timestamp)
                    
                except Hourly_Site_Revenue.MultipleObjectsReturned:
                    repeat = Hourly_Site_Revenue.objects.filter(site=log.site, timestamp=log.timestamp)[1]
                    repeat.delete()
                    print "Deleted: %s" % str(repeat)

        elif data_type == "Mingi_User":
            objects = Mingi_User.objects.all()
            
            for user in objects:
                try:
                    Mingi_User.objects.get(telephone=user.telephone)
                    
                except Mingi_User.MultipleObjectsReturned:
                    repeats = Mingi_User.objects.filter(telephone=user.telephone)
                    found = False
                    
                    for repeat in repeats:
                        if found:
                            break
                        if repeat.first_name == "PRE-REG" and repeat.last_name=="USER":
                            repeat.delete()
                            print "Deleted: %s" % str(repeat)
                            found = True

        elif data_type == "Payment":
            objects = Kopo_Kopo_Message.objects.all()

            for log in objects:
                try:
                    Kopo_Kopo_Message.objects.get(site=log.site, timestamp=log.timestamp)
                    
                except Kopo_Kopo_Message.MultipleObjectsReturned:
                    repeat = Kopo_Kopo_Message.objects.filter(user=log.user, amount=log.amount, timestamp=log.timestamp)[1]
                    repeat.delete()
                    print "Deleted: %s" % str(repeat)
                    
        else:
            print "Please enter one of the following data types: Line_State, Power_Rollup, Energy_Rollup, Payment"
            return
