from celery import task
from .management.commands.run_timer_commands import sendLineStatusMsgs
from africas_talking.sendSMS import sendSMS
from africas_talking.BBOXX_API import collect_BBOXX_data
from .models import Timer_Command
from mingi_site.models import Daily_Power
from mingi_site.views import *

from africas_talking.userResponse import findUserByLine
from mingi_site.message_rollups import create_message_rollups
from africas_talking.archiving import update_message_archive
from mingi_site.archiving import update_line_state_archive

from mingi_django.management.commands.get_finance_report import *
from mingi_django.timezones import zone_aware
from django.core.management import call_command
from mingi_user.methods import get_site_utility_lists

import datetime
from django.utils import timezone
from django.utils.timezone import now
import pytz
from django.contrib.auth.models import User


@task
def diagnostics():
    """
    calls helper methods and compiles a system summary to flag potential issues

    sends diagnostic email with summary to relevant email addresses
    """

    call_command('run_diagnostics')


@task
def timer_commands():
    """
    run timer commands if they are scheduled for this hour
    """

    # check line statuses
    sendLineStatusMsgs()

    # run commands

    commands = Timer_Command.objects.all()
    current_hour = datetime.datetime.now().hour
    for command in commands:
        hours = command.hour_array

        for hour in hours:
            try:
                if int(hour) == current_hour:
                    sendSMS(command.destination_phone, command.message)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))
                    
@task
def custom_timer_commands():
    call_command('run_timer_commands')


@task
def maintain_demo_users():
    call_command('maintain_demo_users')

    
@task
def subscription_plan():
    call_command('subscription_plan')


@task
def daily_customer_sms(sitename):
    """
    sends a daily summary SMS to customers at any of the sites in SITE_LIST
    """

    call_command('daily_customer_sms', sitename)


@task
def revenue_rollups():
    """
    Updates the hourly revenue rollups for every site. Accomplishes this
    with a slight hack: by calling revenue_to_date() on every site.
    """

    now_timestamp = now()
    day_ago_timestamp = now_timestamp - datetime.timedelta(hours=3)

    for site in Mingi_Site.objects.all():
        print site.revenue_range(day_ago_timestamp, now_timestamp)


@task
def power_rollups():
    """
    Updates the hourly power rollups for every site for the past three
    hours.
    """

    for site in Mingi_Site.objects.filter(is_active=True,is_combined=False):
        power_rollups_site.delay(site.id)


@task
def power_rollups_site(site_id, start_timestamp=None, end_timestamp=None):
    """
    Updates the hourly power rollups for the given site in the past three
    hours.
    """

    if not end_timestamp:
        end_timestamp = now().replace(minute=0, second=0, microsecond=0)
    if not start_timestamp:
        start_timestamp = end_timestamp - datetime.timedelta(hours=24)

    site = Mingi_Site.objects.get(id=site_id)
    lines_rollup = get_site_utility_lists(site)
    print site.energy_range(start_timestamp, end_timestamp, lines_rollup, True)


@task
def daily_power_rollups(this_date=None):
    """
    Updates the daily power rollups for every site
    """

    # identify all gaps in daily rollups (30 days)
    if not this_date:
        this_date = now().date()

    # make datetime into date, facilitate creation of all missing rollups
    for site in Mingi_Site.objects.filter(is_active=True, is_combined=False):
        daily_power_rollups_site(site, this_date)


def daily_power_rollups_site(site, this_date):
    """
    Updates the daily power rollups for the given site
    """

    lines_rollup = get_site_utility_lists(site)

    # identify postpaid sites (they have no registered customers on lines)
    if Line.objects.filter(site=site, user__isnull=False).count() == 0:
        is_postpaid = True
    else:
        is_postpaid = False
    # For every site, check all hourly rollups exist before creating daily rollup
    print "Site: " + site.name
    try:
        start_date = Daily_Power.objects.filter(site=site, timestamp__lt=this_date).order_by('-timestamp')[0].timestamp + datetime.timedelta(days=1)

    # if no previous Daily_Power objects exist, start from site's date of commissioning
    except IndexError:
        start_date = site.date_online

    next_date = start_date

    while next_date < this_date:
        daily_power_rollup_site_date(site, next_date, lines_rollup, is_postpaid)

        next_date += datetime.timedelta(days=1)


def daily_power_rollup_site_date(site, this_date, lines_rollup=None,
                                 is_postpaid=None):

    if lines_rollup is None:
        lines_rollup = get_site_utility_lists(site)

    if is_postpaid is None:
        # identify postpaid sites (they have no registered customers on lines)
        if Line.objects.filter(site=site, user__isnull=False).count() == 0:
            is_postpaid = True
        else:
            is_postpaid = False

    rollup_length = check_hourly_rollups_success(site, this_date)

    if int(rollup_length) >= 24:
        create_daily_power_rollup(this_date, site, lines_rollup, is_postpaid)
    else:
        print "Only %d hourly rollups found for site %s on date %s" % (int(rollup_length), str(site), str(this_date))


@task
def create_daily_power_rollup(this_date, site, lines_rollup,
                              calculate_postpaid_costs=False):
    """
    Create rollups for all the site's users and use the total to create a
    rollup for the site.

    :param this_date: The day for which to create rollups.
    :param site: The site whose users and total will be rolled up.
    :param lines_rollup: The list of lines to roll up.
    :param calculate_postpaid_costs: Whether to create a cost rollup.
    :return:
    """

    print "creating"
    flag_num_lines = False
    site_total = 0.0
    cost_total = 0.0

    for row in lines_rollup:
        row['total'] = 0.0
    
    # get the set of line states for that site which includes states for all
    # lines starting before the current day
    try:
        this_datetime = datetime.datetime(this_date.year, this_date.month,
                                          this_date.day, 0, 0, 0, 0)
        next_datetime = this_datetime + datetime.timedelta(days=1)

        # last LS timestamp strictly before this_date, with a one-day buffer
        start_filter = (
            Line_State.objects.filter(site=site,
                                      timestamp__lt=this_datetime,
                                      meter_reading__isnull=False).order_by(
                '-timestamp')[0].timestamp -
            datetime.timedelta(days=1))

        # first LS timestamp strictly after this_date, with a one-day buffer
        end_filter = (
            Line_State.objects.filter(site=site,
                                      timestamp__gte=next_datetime,
                                      meter_reading__isnull=False).order_by(
                'timestamp')[0].timestamp +
            datetime.timedelta(days=1))

        line_states = Line_State.objects.filter(
            timestamp__gte=start_filter,
            timestamp__lte=end_filter,
            site=site,
            meter_reading__isnull=False).order_by('-timestamp')
            
    # if there is no timestamp in the assumed time range, take all of them
    except IndexError:
        line_states = Line_State.objects.filter(
            site=site, meter_reading__isnull=False).order_by('-timestamp')

    line_state_list = [[] for line in range(0, site.num_lines)]

    # divide line states into sub-lists by line number
    # *** NOTE: this does not account for user line number switches ***
    for item in line_states:
        line_num = item.number

        # create a single error log if this is flagged
        if line_num > site.num_lines:
            flag_num_lines = True

        index = line_num - 1

        try:
            line_state_list[index].append(item)
        except IndexError:
            pass

    # create rollups for each user and total sites simultaneously
    for line in range(1, site.num_lines+1):

        # either do a rollup for a user, or find the value for a line number
        # assume a user will only have one utility type on their line(s)
        try:
            user = findUserByLine(line, site.name)
            amount = create_user_daily_rollup(user, this_datetime,
                                              next_datetime, line_state_list)
            # don't create a rollup if a user rollup cannot be created
            if amount == "incomplete rollup":
                return "failed: no rollup created for user: %s" % str(user)

        except Mingi_User.DoesNotExist:
            user = None
            index = line - 1
            amount = get_daily_energy_line(this_datetime, next_datetime,
                                           line_state_list[index])
            # still create rollup if a non-user line has incomplete data
            if amount is None:
                amount = 0.0

            try:
                line_obj = Line.objects.get(site=site, number=line)
                this_date = datetime.date(this_datetime.year,
                                          this_datetime.month,
                                          this_datetime.day)
                Daily_Power.objects.create(line_obj=line_obj,
                                           amount=amount,
                                           utility=line_obj.utility_type,
                                           timestamp=this_date)
            except Line.DoesNotExist:
                Error_Log.objects.create(
                    problem_type="INTERNAL",
                    problem_message="Object for Line %d not found at site %s" %
                                    (line, site.name))

        except Mingi_User.MultipleObjectsReturned:
            user = None
            amount = 0.0
            Error_Log.objects.create(
                problem_type="INTERNAL",
                problem_message="Multiple users detected on line %d at: %s" %
                                (line, str(site)))

        # keep track of the site total
        if amount != "already counted":
            # ensure all line totals are matched with a utility
            for row in lines_rollup:
                if str(line) in row['lines']:
                    row['total'] += amount
            # line contribution to BM site costs, if applicable
            if calculate_postpaid_costs:
                if user is not None:
                    energy_price = float(user.energy_price)
                else:
                    line_obj = Line.objects.get(number=line, site=site)
                    if line_obj.is_connected and \
                            (line_obj.utility_price is not None):
                        energy_price = float(line_obj.utility_price)
                    else:
                        energy_price = 0.0

                user_cost = amount * float(energy_price)
                cost_total += user_cost

    for row in lines_rollup:
        if len(row['lines']) > 0:
            utility = Utility.objects.get(name=row['name'])
            daily_power = Daily_Power(site=site,
                                      amount=row['total'],
                                      timestamp=this_date,
                                      utility=utility)
            print "created: Daily Power Rollup"
            daily_power.save()

    # create cost rollup if applicable
    if calculate_postpaid_costs:
        this_ts = datetime.datetime(this_date.year, this_date.month,
                                    this_date.day, 23, 0, 0, 0)
        this_ts = zone_aware(site, this_ts)
        try:
            rollup = Hourly_Site_Revenue.objects.get(site=site,
                                                     timestamp=this_ts)
            rollup.amount = cost_total
            rollup.save()
        except Hourly_Site_Revenue.DoesNotExist:
            Hourly_Site_Revenue.objects.create(site=site,
                                               timestamp=this_ts,
                                               amount=cost_total)
        except Hourly_Site_Revenue.MultipleObjectsReturned:
            Error_Log.objects.create(
                problem_type="INTERNAL",
                problem_message="Duplicate revenue rollups found!")
            rollup = Hourly_Site_Revenue.objects.filter(site=site,
                                                        timestamp=this_ts)[0]
            rollup.amount = cost_total
            rollup.save()

    if flag_num_lines:
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="line number index out of bounds for site %s" %
                            str(site))

    return site_total


def create_user_daily_rollup(user, this_datetime, next_datetime, line_state_list):
    """
    create a rollup for a line with a user, plus a user rollup
    """

    this_date = datetime.date(this_datetime.year, this_datetime.month, this_datetime.day)

    # first check if a matching rollup already exists, otherwise create one
    # don't count a user multiple times if they have multiple lines
    rollup = Daily_Power.objects.filter(user=user, timestamp=this_date)
    if rollup:
        return "already counted"

    user_total = 0.0
    write_rollup = True

    for line in user.line_array:
        index = int(line) - 1
        line_total = get_daily_energy_line(this_datetime,
                                           next_datetime,
                                           line_state_list[index])
        # mark user as incomplete if not all lines are present
        if line_total is None:
            write_rollup = False
        else:
            user_total += line_total

    # only write that users's rollup if all lines were present (could result in inconsistent site / user daily rollups)
    if write_rollup:
        
        # ***SOON: add check to ensure that multiple lines don't span different utility types ***
        utility = Line.objects.get(number=int(line),site=user.site).utility_type
        new_hourly = Daily_Power(user=user,
                                 timestamp=this_date,
                                 amount=user_total,
                                 utility=utility)
        new_hourly.save()
        print "created: Daily Power Rollup %s - %s (%s)" % (str(user), str(user_total),utility.name)
        return user_total
    else:
        return "incomplete rollup"


def get_daily_energy_line(start_timestamp, end_timestamp, line_states):
    """
    Returns the energy produced by the given line of this site in the clock
    hour containing the timestamp. If the energy cannot be determined due to
    insufficient line state objects, returns None.
    """

    # track whether all required line states were found
    start_timestamp = timezone.make_aware(start_timestamp,timezone.utc)
    end_timestamp = timezone.make_aware(end_timestamp,timezone.utc)

    try:
        last_state = line_states[0]
        first_state = line_states[len(line_states) - 1]
    except IndexError:
        return None

    state_4_found = False

    # ensure there is a line state occuring after the end of the hour
    if (last_state.timestamp < end_timestamp):
        return None

    # ensure there is a line state occurring before the beginning of the hour
    if (first_state.timestamp > start_timestamp):
        return None

    # get the 4 line state objects which define this hour's energy use
    for state in line_states:

        if state.timestamp <= end_timestamp and state_4_found == False:
            line_state_4 = last_state
            line_state_3 = state
            state_4_found = True

        if state.timestamp <= start_timestamp:
            line_state_2 = last_state
            line_state_1 = state
            break

        last_state = state

    # The meter value at the end of the hour, using linear interpolation
    try:
        end_energy = (
            line_state_3.meter_reading +
            (
                (line_state_4.meter_reading - line_state_3.meter_reading) *
                (
                    (end_timestamp -
                     line_state_3.timestamp).total_seconds() /
                    (line_state_4.timestamp -
                     line_state_3.timestamp).total_seconds()
                )
            )
        )
    except ZeroDivisionError:
        end_energy = line_state_3.meter_reading

    # The meter value at the start of the hour, using linear interpolation
    try:
        start_energy = (
            line_state_1.meter_reading +
            (
                (line_state_2.meter_reading - line_state_1.meter_reading) *
                (
                    (start_timestamp -
                     line_state_1.timestamp).total_seconds() /
                    (line_state_2.timestamp -
                     line_state_1.timestamp).total_seconds()
                )
            )
        )
    except ZeroDivisionError:
        start_energy = line_state_1.meter_reading

    return end_energy - start_energy


@task
def email_finance_report(email, year, month, user=None):
    """
    send out finance report in specified format
    """
    if user is not None:
        user = User.objects.get(username=user)

    call_command('get_finance_report', month, year, email, o=[user])


@task
def email_kpi_report(start_date, end_date, email, user=None):
    """
    send out kpi report
    """
    if user is not None:
        user = User.objects.get(username=user)

    call_command('get_kpi_report', start_date, end_date, email, o=[user])


@task
def email_energy_report(start_date, end_date, email, user=None):
    """
    send out energy report in specified format
    """
    if user is not None:
        user = User.objects.get(username=user)
        
    call_command('get_energy_report', start_date, end_date, email, o=[user])

@task
def email_pdf_report(username=None):
    """
    send out pdf report in specified format
    """    

    call_command('get_pdf_report',username)


@task 
def generate_csv_data(object_name, email_fileformat, site, min_date, max_date):
    """
    send out csv report in specified format
    """

    if(min_date == "") or (max_date == ""):
        call_command('get_csv_data', object_name, email_fileformat, o=[site])
    
    else:
        call_command('get_csv_data', object_name, email_fileformat, o2=[site, min_date, max_date])


@task
def generate_site_user_summary(start_date, end_date, email, site_name):
    """
    send out site user report
    """

    call_command('get_user_report', start_date, end_date, email, site_name)


@task
def daily_message_count():
    """
    update daily message count records
    """

    create_message_rollups()


@task
def daily_message_archive():
    """
    archive the next day's worth of messages
    """

    update_message_archive()
    update_line_state_archive()


@task
def check_hourly_rollups_success(site, the_date):
    """
    Check that all hourly rollups for a given site on a given day, if not create them
    """
    
    start_ts = datetime.datetime(the_date.year, the_date.month, the_date.day,
                                 0, 0, 0, 0).replace(tzinfo=pytz.utc)
    the_date_after = the_date + datetime.timedelta(days=1)
    end_ts = datetime.datetime(the_date_after.year, the_date_after.month, the_date_after.day,
                               0, 0, 0, 0).replace(tzinfo=pytz.utc)

    def get_this_site_hourly_rollups():
        return Hourly_Site_Power.objects.filter(
            site=site,
            timestamp__gte=start_ts,
            timestamp__lt=end_ts).order_by('timestamp')

    rollups = get_this_site_hourly_rollups()

    # Check that all hourly rollups for that day exist, if yes, return true
    if len(rollups) == 24:
        return len(rollups)
    else:
        # Create hourly rollups
        power_rollups_site(site.id, start_ts, end_ts)
        # Check whether rollups were created successfully
        rollups_recheck = get_this_site_hourly_rollups()
        return len(rollups_recheck)


@task
def check_BH():
    """
    Check the status of BitHarvesters, if no communication for the past 3 hours, send a report
    """

    call_command('check_BH_status')
    
    return


@task
def check_gateway():
    """
    Check the status of Gateways, if no communication for the past 3 hours, send a report
    """

    call_command('check_sms_server_status')
    
    return


@task
def transfer_data(model_name):
    """
    Transfer data between models.
    """

    call_command('transfer_data', model_name)
    
    return


@task
def bboxx_api_data_collection(start_ts=None, end_ts=None):
    """
    Collect data from BBOXX API over the specified time range;
    (every 4 hours by default)
    """

    now = gen_zone_aware(TIME_ZONE,datetime.datetime.now())
    
    if end_ts:
        end_date = str(end_ts)
    else:
        end_date = datetime.datetime(now.year,now.month,now.day,now.hour,0,0,0)
        
    if start_ts:
        start_date = str(start_ts)
    else:
        start_date = end_date - datetime.timedelta(hours=4)

    collect_BBOXX_data(str(start_date), str(end_date))
    Error_Log.objects.create(problem_type='DEBUGGING',
                             problem_message="BBOXX Sync Ran at %s" % str(start_date))
