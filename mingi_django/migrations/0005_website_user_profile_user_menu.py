# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0004_create_initial_tokens'),
    ]

    operations = [
        migrations.AddField(
            model_name='website_user_profile',
            name='user_menu',
            field=models.ManyToManyField(to='mingi_django.Menu', null=True, blank=True),
        ),
    ]
