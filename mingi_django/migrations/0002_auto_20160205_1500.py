# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mingi_django', '0001_initial'),
        ('mingi_site', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='timer_command',
            name='site',
            field=models.ForeignKey(blank=True, to='mingi_site.Mingi_Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='menu',
            name='menu_parent',
            field=models.ForeignKey(blank=True, to='mingi_django.Menu', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='faq',
            name='category',
            field=models.ForeignKey(to='mingi_django.FAQCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='faq',
            name='multimedia',
            field=models.ForeignKey(blank=True, to='mingi_django.Multimedia', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='error_log',
            name='site',
            field=models.ForeignKey(blank=True, to='mingi_site.Mingi_Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='custom_timer_command',
            name='line',
            field=models.ForeignKey(blank=True, to='mingi_site.Line', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='custom_timer_command',
            name='site',
            field=models.ForeignKey(blank=True, to='mingi_site.Mingi_Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='custom_timer_command',
            name='user',
            field=models.ForeignKey(blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='custom_timer_command',
            name='utility',
            field=models.ForeignKey(blank=True, to='mingi_django.Utility', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='app_permissions',
            name='enabled_users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, null=True, blank=True),
            preserve_default=True,
        ),
    ]
