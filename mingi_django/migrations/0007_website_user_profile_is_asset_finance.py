# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0006_website_user_profile_user_menu'),
    ]

    operations = [
        migrations.AddField(
            model_name='website_user_profile',
            name='is_asset_finance',
            field=models.BooleanField(default=False, verbose_name=b'Is Asset Finance?'),
        ),
    ]
