# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='App_Permissions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feature_name', models.CharField(max_length=100, verbose_name=b'Feature Name')),
                ('enabled', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'App Permission',
            },
        ),
        migrations.CreateModel(
            name='Custom_Timer_Command',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('destination', models.CharField(max_length=30)),
                ('trigger_type', models.CharField(blank=True, max_length=15, null=True, choices=[(b'temp', b'Temperature'), (b'voltage', b'Voltage'), (b'account_balance', b'Account Balance')])),
                ('trigger_level', models.CharField(blank=True, max_length=15, null=True, choices=[(b'equal_to', b'Equal To'), (b'less_than', b'Less Than'), (b'greater_than', b'Greater Than')])),
                ('message', models.CharField(max_length=300)),
                ('message_type', models.CharField(default=b'email', max_length=5, choices=[(b'sms', b'SMS'), (b'email', b'Email')])),
                ('hours', models.CommaSeparatedIntegerField(default=b'0', max_length=61, blank=True)),
                ('threshold', models.FloatField(null=True, blank=True)),
                ('is_triggered', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Diagnostic_Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('last_updated', models.DateTimeField()),
                ('k2_transactions_count', models.IntegerField(default=0)),
                ('AT_sms_balance', models.DecimalField(default=0.0, max_digits=7, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='Error_Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('problem_type', models.CharField(default=b'OTHER', max_length=30, choices=[(b'SEND_SMS', b'Send SMS Failed'), (b'SEND_EMAIL', b'Send EMAIL Failed'), (b'INTERNAL', b'Other Internal Error'), (b'EQUIPMENT', b'Equipment Error'), (b'EXTERNAL', b'Other External Error'), (b'K2_PROCESS', b'Payment Processing Error'), (b'FAULTY_DATA', b'Faulty Data Warning'), (b'DEBUGGING', b'Debugging Message'), (b'LOG_DATA', b'Log Data for Tracking'), (b'PING_ERROR', b"Server URL Doesn't Exist"), (b'WARNING', b'Warning / Alert'), (b'OTHER', b'Other Error: Unknown Type')])),
                ('problem_message', models.CharField(max_length=300, null=True, blank=True)),
                ('creator', models.CharField(max_length=50, null=True, blank=True)),
                ('is_resolved', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-timestamp'],
                'verbose_name': 'Error Log',
            },
        ),
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('keywords', models.CharField(max_length=200)),
                ('has_multimedia', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='FAQCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('has_menu', models.BooleanField(default=False, verbose_name=b'Has sub-menu ?')),
                ('menu_name', models.CharField(max_length=50)),
                ('menu_title', models.CharField(max_length=50, null=True, blank=True)),
                ('menu_action', models.CharField(default=b'#', max_length=200, null=True, verbose_name=b'menu action URL', blank=True)),
                ('menu_id', models.CharField(max_length=200, null=True, verbose_name=b'Menu ID', blank=True)),
                ('menu_target', models.CharField(default=b'__parent', max_length=30, choices=[(b'__blank', b'BLANK PAGE'), (b'__self', b'SAME FRAME'), (b'__parent', b'PARENT FRAME'), (b'__top', b'WINDOW FULL BODY')])),
                ('menu_order', models.IntegerField(null=True, blank=True)),
                ('menu_icon', models.CharField(max_length=30, null=True, blank=True)),
                ('menu_type', models.CharField(default=b'all', max_length=10, choices=[(b'all', b'Both'), (b'normal', b'Normal Menu Only'), (b'utility', b'Utility Management Menu Only')])),
                ('has_text_field', models.BooleanField(default=False, verbose_name=b'Has an input box ?')),
                ('is_staff', models.BooleanField(default=False, verbose_name=b'Is a staff only menu ?')),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Multimedia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('multimedia_type', models.CharField(default=b'youtube', max_length=15, choices=[(b'youtube', b'YouTube'), (b'pdf', b'PDF')])),
                ('content', models.CharField(max_length=1000)),
                ('thumbnail', models.CharField(default=b'define default link here', max_length=100, blank=True)),
                ('is_featured', models.BooleanField(default=False)),
                ('order', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Timer_Command',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('destination_phone', models.CharField(max_length=20)),
                ('message', models.CharField(max_length=150)),
                ('hours', models.CommaSeparatedIntegerField(default=b'0', max_length=61)),
            ],
        ),
        migrations.CreateModel(
            name='Utility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'Electricity', max_length=50)),
                ('primary_unit', models.CharField(max_length=20, blank=True)),
                ('secondary_unit', models.CharField(max_length=20, blank=True)),
                ('primary_user_unit', models.CharField(max_length=20, blank=True)),
                ('secondary_user_unit', models.CharField(max_length=20, blank=True)),
                ('time_series_label', models.CharField(max_length=100, null=True, blank=True)),
                ('daily_totals_label', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Utilities',
            },
        ),
        migrations.CreateModel(
            name='Website_User_Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_demo', models.BooleanField(default=False, verbose_name=b'Is Demo?')),
                ('send_emails', models.BooleanField(default=False, verbose_name=b'Send Emails')),
                ('user_type', models.CharField(default=b'CRO', max_length=3, choices=[(b'AAD', b'SteamaCo Admin'), (b'CAD', b'Admin User'), (b'CRO', b'Read-Only User')])),
                ('utility_management', models.BooleanField(default=False, verbose_name=b'Utility Management')),
                ('token', models.CharField(max_length=50, null=True, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
                ('utilities', models.ManyToManyField(to='mingi_django.Utility', null=True, blank=True)),
            ],
        ),
    ]
