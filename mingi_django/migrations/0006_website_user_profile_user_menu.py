# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def create_menu(apps, schema_editor):
    """
    * Create Menus function
    """

    Menu = apps.get_model("mingi_django", "Menu")

    # add all parent menus
    graphs = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Graphs",
                                 menu_title="Graphs", menu_id="#", menu_order=1, menu_type="all")
    profiles = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Profiles",
                                   menu_title="Profiles", menu_id="#", menu_order=2, menu_type="all")
    logs = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Logs",
                               menu_title="Logs", menu_id="#", menu_order=3, menu_type="all")
    controls = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Controls",
                                   menu_title="Controls", menu_id="#", menu_order=4, menu_type="all", has_text_field=False,)
    comms = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Comms",
                                menu_title="Comms", menu_id="#", menu_order=5, menu_type="all")
    reports = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Reports",
                                  menu_title="Reports", menu_id="#", menu_order=6, menu_type="all")
    kpis = Menu.objects.create(menu_parent=None, menu_name="KPIs", menu_title="KPIs",
                               menu_id="kpis_card", menu_order=7, menu_type="all", has_text_field=False,)
    admin = Menu.objects.create(menu_parent=None, has_menu=True, menu_name="Admin",
                                menu_title="Admin", menu_id="#", menu_order=8, menu_type="all")

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Graph parent
    """
    # add sub-menu for Graphs >
    sum_totals = Menu.objects.create(menu_parent=graphs, menu_name="Summary Totals",
                                     menu_title="Summary Totals", menu_id="summary_analysis", menu_order=1, menu_type="all")
    daily_totals = Menu.objects.create(menu_parent=graphs, has_menu=True, menu_name="Daily Totals",
                                       menu_title="Daily Totals", menu_id="#", menu_order=2, menu_type="all")
    time_series = Menu.objects.create(menu_parent=graphs, has_menu=True, menu_name="Time Series",
                                      menu_title="Time Series", menu_id="#", menu_order=3, menu_type="all")
    bilevel = Menu.objects.create(menu_parent=graphs, has_menu=True, menu_name="Bilevel Partition",
                                  menu_title="Bilevel Partition", menu_id="#", menu_order=4, menu_type="all")
    comp_graph = Menu.objects.create(menu_parent=graphs, menu_name="Comparison Graph", menu_title="Comparison Graph",
                                     menu_id="comparison_graph", menu_order=5, is_staff=True, menu_type="all")

    # add sub-menus for Graphs > Daily Totals
    pay_rev = Menu.objects.create(menu_parent=daily_totals, menu_name="Payments & Revenues", menu_title="Site",
                                  menu_id="daily_totals_revenues_search_site", menu_order=1, menu_type="normal", has_text_field=True)
    pay_rev_utility = Menu.objects.create(menu_parent=daily_totals, menu_name="Costs", menu_title="Site",
                                          menu_id="daily_totals_revenues_search_site", menu_order=1, menu_type="utility", has_text_field=True)
    site_utility = Menu.objects.create(menu_parent=daily_totals, menu_name="Site Utility Use", menu_title="Site",
                                       menu_id="daily_totals_energyuse_search_site", menu_order=2, menu_type="all", has_text_field=True)
    user_utility = Menu.objects.create(menu_parent=daily_totals, menu_name="Customer Utility Use", menu_title="Customer",
                                       menu_id="daily_totals_energyuse_search_user", menu_order=3, menu_type="all", has_text_field=True)
    site_comms = Menu.objects.create(menu_parent=daily_totals, menu_name="Comms", menu_title="Site",
                                     menu_id="daily_totals_comms_search_site", menu_order=4, menu_type="all", has_text_field=True)

    # add sub-menus for Graph > Time Series
    by_site = Menu.objects.create(menu_parent=time_series, has_menu=True, menu_name="By Site",
                                  menu_title="By Site", menu_id="#", menu_order=1, menu_type="all")
    by_utility = Menu.objects.create(menu_parent=time_series, has_menu=True, menu_name="By Utility",
                                     menu_title="By Utility", menu_id="#", menu_order=2, menu_type="all")
    by_type = Menu.objects.create(menu_parent=time_series, has_menu=True, menu_name="By Data Type",
                                  menu_title="By Data Type", menu_id="#", menu_order=3, menu_type="all")

    # add sub-menus for Graphs > Time Series > By Site
    by_site_utility = Menu.objects.create(menu_parent=by_site, menu_name="Utility Use", menu_title="Site",
                                          menu_id="time_series_search_site", menu_order=1, menu_type="all", has_text_field=True)

    # add sub-menus for Graphs > Time Series > By Utility
    by_utility_graphs = Menu.objects.create(menu_parent=by_utility, menu_name="Account Activity", menu_title="Customer",
                                            menu_id="time_series_search_user", menu_order=1, has_text_field=True, menu_type="normal")
    by_utility_graphs_utility = Menu.objects.create(menu_parent=by_utility, menu_name="Account Activity", menu_title="Utility",
                                                    menu_id="time_series_search_utility", menu_order=1, has_text_field=True, menu_type="utility")

    # add sub-menus for Graphs > Time Series > By Type
    by_type_site_utility = Menu.objects.create(menu_parent=by_type, menu_name="Utility Draw",
                                               menu_title="Utility Draw", menu_id="sites_utility_draw", menu_order=1, menu_type="all")
    by_type_site_voltage = Menu.objects.create(
        menu_parent=by_type, menu_name="Voltage", menu_title="Voltage", menu_id="sites_voltage", menu_order=2, menu_type="all")
    by_type_temparature = Menu.objects.create(menu_parent=by_type, menu_name="Temperature",
                                              menu_title="Temperature", menu_id="sites_temperature", menu_order=3, menu_type="all")
    by_type_meter_readings = Menu.objects.create(menu_parent=by_type, menu_name="Meter Readings", menu_title="Meter Readings",
                                                 menu_id="site_meter_reading", menu_order=4, has_text_field=True, menu_type="all")

    # add sub-menus for Graphs > Bilevel Partition
    bilevel_revenues = Menu.objects.create(menu_parent=bilevel, menu_name="Revenues Breakdown",
                                           menu_title="Revenues Breakdown", menu_id="payments_bilevel_card", menu_order=1, menu_type="normal")
    bilevel_revenues_utility = Menu.objects.create(menu_parent=bilevel, menu_name="Cost Breakdown",
                                                   menu_title="Cost Breakdown", menu_id="payments_bilevel_card", menu_order=1, menu_type="utility")
    bilevel_utility_use = Menu.objects.create(menu_parent=bilevel, menu_name="Utility Use Breakdown",
                                              menu_title="Utility Use Breakdown", menu_id="energy_bilevel_card", menu_order=2, menu_type="all")

    # End of graphs menus

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Profiles parent menu
    """
    # sub-menus for Profiles menu
    profiles_site = Menu.objects.create(menu_parent=profiles, menu_name="Search for Site Profiles",
                                        menu_title="Site", menu_id="profiles_search_site", menu_order=1, has_text_field=True, menu_type="all")
    profiles_site_utility = Menu.objects.create(menu_parent=profiles, menu_name="Search for Site Utilities",
                                                menu_title="Site", menu_id="profiles_search_site_customers", menu_order=2, has_text_field=True, menu_type="all")
    profile_user_utility = Menu.objects.create(menu_parent=profiles, menu_name="Search for Customer", menu_title="Customer",
                                               menu_id="profiles_search_user", menu_order=3, has_text_field=True, menu_type="normal")
    profile_user_utility_utility = Menu.objects.create(menu_parent=profiles, menu_name="Search for Utility",
                                                       menu_title="Utility", menu_id="profiles_search_user", menu_order=3, has_text_field=True, menu_type="utility")

    # End of profiles sub-menu

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Logs parent menu
    """
    logs_messages = Menu.objects.create(menu_parent=logs, has_menu=True, menu_name="Messages",
                                        menu_title="Messages", menu_id="#", menu_order=1, menu_type="all")
    logs_alerts = Menu.objects.create(menu_parent=logs, menu_name="Alerts", menu_title="Alerts",
                                      menu_id="pending_alerts_card", menu_order=2, menu_type="all")

    # sub menus for Logs > Messages
    logs_messages_all = Menu.objects.create(menu_parent=logs_messages, menu_name="All",
                                            menu_title="All", menu_id="all_messages_card", menu_order=1, menu_type="all")
    logs_messages_payments = Menu.objects.create(menu_parent=logs_messages, menu_name="Payments",
                                                 menu_title="Payments", menu_id="payments_messages_card", menu_order=2, menu_type="normal")
    logs_messages_sms = Menu.objects.create(menu_parent=logs_messages, has_menu=True,
                                            menu_name="SMS", menu_title="SMS", menu_id="#", menu_order=3, menu_type="all")

    # add sub menus for Logs > Messages > SMS
    logs_messages_sms_all = Menu.objects.create(
        menu_parent=logs_messages_sms, menu_name="All", menu_title="All", menu_id="all_sms_card", menu_order=1, menu_type="all")
    logs_messages_sms_bh = Menu.objects.create(menu_parent=logs_messages_sms, menu_name="bitHarvester",
                                               menu_title="bitHarvester", menu_id="bh_sms_card", menu_order=2, menu_type="all")
    logs_messages_sms_customer = Menu.objects.create(
        menu_parent=logs_messages_sms, menu_name="Customer", menu_title="Customer", menu_id="cust_sms_card", menu_order=3, menu_type="normal")

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Controls parent menu
    """
    controls_register = Menu.objects.create(
        menu_parent=controls, has_menu=True, menu_name="Register", menu_title="Register", menu_id="#", menu_order=1, menu_type="all")
    controls_edit = Menu.objects.create(menu_parent=controls, has_menu=True,
                                        menu_name="Edit", menu_title="Edit", menu_id="#", menu_order=2, menu_type="all")
    controls_set = Menu.objects.create(menu_parent=controls, has_menu=True,
                                       menu_name="Set", menu_title="Set", menu_id="#", menu_order=3, menu_type="all")
    controls_bulk_edit_users = Menu.objects.create(menu_parent=controls, menu_name="Bulk Edit Customers",
                                                   menu_title="Bulk Edit Customers", menu_id="controls_bulk_edit_users", menu_order=4, menu_type="all")
    controls_management_commands = Menu.objects.create(menu_parent=controls, menu_name="Management Commands", menu_title="Management Commands",
                                                       menu_id="controls_management_command_card", menu_order=5, is_staff=True, menu_type="all")

    # sub-menus for Controls > Register
    controls_register_customer = Menu.objects.create(
        menu_parent=controls_register, menu_name="Customer", menu_title="Customer", menu_id="register_user_card", menu_order=1, menu_type="normal")
    controls_register_customer_utility = Menu.objects.create(
        menu_parent=controls_register, menu_name="Utility", menu_title="Utility", menu_id="register_user_card", menu_order=1, menu_type="utility")
    controls_register_site = Menu.objects.create(
        menu_parent=controls_register, menu_name="Site", menu_title="Site", menu_id="register_site_card", menu_order=2, menu_type="all")
    controls_register_bh = Menu.objects.create(menu_parent=controls_register, menu_name="bitHarvester",
                                               menu_title="bitHarvester", menu_id="register_bitharvester_card", menu_order=3, is_staff=True, menu_type="all")
    controls_register_website_user = Menu.objects.create(
        menu_parent=controls_register, menu_name="User", menu_title="User", menu_id="register_website_user_card", menu_order=4, menu_type="all")
    controls_register_faq_cat = Menu.objects.create(menu_parent=controls_register, menu_name="FAQ Category",
                                                    menu_title="FAQ Category", menu_id="register_faq_category_card", menu_order=5, is_staff=True, menu_type="all")
    controls_register_faq = Menu.objects.create(menu_parent=controls_register, menu_name="FAQ",
                                                menu_title="FAQ", menu_id="register_faq_card", menu_order=6, is_staff=True, menu_type="all")
    controls_register_multimedia = Menu.objects.create(menu_parent=controls_register, menu_name="Multimedia",
                                                       menu_title="Multimedia", menu_id="register_multimedia_card", menu_order=7, is_staff=True, menu_type="all")
    controls_register_register = Menu.objects.create(menu_parent=controls_register, menu_name="Gateway",
                                                     menu_title="Gateway", menu_id="register_gateway_card", menu_order=8, is_staff=True, menu_type="all")

    # sub-menus for Controls > Edit
    controls_edit_users = Menu.objects.create(menu_parent=controls_edit, menu_name="Customers", menu_title="Customers",
                                              menu_id="controls_edit_search_user", menu_order=1, has_text_field=True, menu_type="normal")
    controls_edit_users_utility = Menu.objects.create(menu_parent=controls_edit, menu_name="Utility", menu_title="Utility",
                                                      menu_id="controls_edit_search_user", menu_order=1, has_text_field=True, menu_type="utility")
    controls_edit_bh = Menu.objects.create(menu_parent=controls_edit, menu_name="bitHarvester", menu_title="bitHarvester",
                                           menu_id="controls_edit_search_bh", menu_order=2, has_text_field=True, menu_type="all")
    controls_edit_site = Menu.objects.create(menu_parent=controls_edit, menu_name="Site", menu_title="Site",
                                             menu_id="controls_edit_search_site", menu_order=3, has_text_field=True, menu_type="all")
    controls_edit_website_user = Menu.objects.create(menu_parent=controls_edit, menu_name="User", menu_title="User",
                                                     menu_id="controls_edit_search_website_user", menu_order=4, has_text_field=True, menu_type="all")
    controls_edit_gateway = Menu.objects.create(menu_parent=controls_edit, menu_name="Gateway", menu_title="Gateway",
                                                menu_id="controls_edit_search_gateway", menu_order=5, is_staff=True, has_text_field=True, menu_type="all")
    controls_edit_activate_user = Menu.objects.create(menu_parent=controls_edit, menu_name="Activate Users", menu_title="Activate Users",
                                                      menu_id="controls_activate_website_user_card", menu_order=6, is_staff=True, menu_type="all")

    # sub-menus for Controls > Set
    controls_set_custom_trigger = Menu.objects.create(menu_parent=controls_set, menu_name="Custom Triggers",
                                                      menu_title="Custom Triggers", menu_id="custom_trigger_commands_card", menu_order=1, menu_type="all")
    controls_set_timer_commands = Menu.objects.create(
        menu_parent=controls_set, menu_name="Timer Commands", menu_title="Timer Commands", menu_id="timer_commands_card", menu_order=2, menu_type="all")

    # End of Controls menu

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Comms parent menu
    """
    comms_customer_sms = Menu.objects.create(menu_parent=comms, menu_name="Customer SMS",
                                             menu_title="Customer SMS", menu_id="customer_sms_card", menu_order=1, menu_type="normal")
    comms_bh_sms = Menu.objects.create(menu_parent=comms, menu_name="bitHarvester SMS",
                                       menu_title="bitHarvester SMS", menu_id="bh_controls_card", menu_order=2, menu_type="all")
    comms_user_emails = Menu.objects.create(menu_parent=comms, menu_name="User Emails", menu_title="User Emails",
                                            menu_id="user_email_card", menu_order=3, is_staff=True, menu_type="all")

    # End of Comms menu

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Reports parent menu
    """
    reports_report_templates = Menu.objects.create(
        menu_parent=reports, menu_name="Report Templates", menu_title="Report Templates", menu_id="reports_card", menu_order=1, menu_type="all")
    reports_report_builder = Menu.objects.create(menu_parent=reports, menu_name="Report Builder",
                                                 menu_title="Report Builder", menu_id="report_builder_card", menu_order=2, is_staff=True, menu_type="all")

    # End of Reports menu

    """
     ////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Menus for Admin parent menu
    """
    admin_help_center = Menu.objects.create(menu_parent=admin, menu_name="Help Center",
                                            menu_title="Help Center", menu_id="help_center_card", menu_order=1, menu_type="all")
    admin_payment_settings = Menu.objects.create(menu_parent=admin, menu_name="Payment Settings",
                                                 menu_title="Payment Settings", menu_id="credit_card_pay", menu_order=1, menu_type="all")
    admin_timezone = Menu.objects.create(menu_parent=admin, menu_name="Change Timezone",
                                         menu_title="Change Timezone", menu_id="change_timezone", menu_order=1, menu_type="all")
    admin_admin = Menu.objects.create(menu_parent=admin, menu_name="Admin", menu_title="Admin",
                                      menu_id="admin_card", menu_order=1, is_staff=True, menu_type="all")
    admin_logout = Menu.objects.create(menu_parent=admin, menu_name="Logout", menu_action="/logout/",
                                       menu_title="Logout", menu_id="logout_card", menu_order=1, menu_type="all")


def destroy_menu(apps, schema_editor):
    """
    Destroy created menus
    """
    Menu = apps.get_model("mingi_django", "Menu")

    Menu.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0005_website_user_profile_user_menu'),
    ]

    operations = [
        migrations.RunPython(create_menu, destroy_menu),
    ]
