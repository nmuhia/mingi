# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0002_auto_20160205_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='website_user_profile',
            name='timezone',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='error_log',
            name='problem_type',
            field=models.CharField(default=b'OTHER', max_length=30, choices=[(b'SEND_SMS', b'Send SMS Failed'), (b'SEND_EMAIL', b'Send EMAIL Failed'), (b'INTERNAL', b'Other Internal Error'), (b'EQUIPMENT', b'Equipment Error'), (b'EXTERNAL', b'Other External Error'), (b'K2_PROCESS', b'Payment Processing Error'), (b'FAULTY_DATA', b'Faulty Data Warning'), (b'DEBUGGING', b'Debugging Message'), (b'LOG_DATA', b'Log Data for Tracking'), (b'PING_ERROR', b"Server URL Doesn't Exist"), (b'WARNING', b'Warning / Alert'), (b'TIMEZONE', b'Timezone error alert'), (b'OTHER', b'Other Error: Unknown Type')]),
        ),
    ]
