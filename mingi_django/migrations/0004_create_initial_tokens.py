# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import migrations
from rest_framework.authtoken.models import Token


def create_initial_tokens(apps, schema_editor):
    """
    A data migration function to create tokens for existing users, once the
    token table has been created.
    """
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0003_auto_20160211_1240'),

        # Ensure that we have created the user and token tables first
        ('auth', '0001_initial'),
        ('authtoken', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_initial_tokens),
    ]
