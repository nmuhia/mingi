from django.conf.urls import patterns, url
from .views import update_balance, update_site_power, update_site_revenue

urlpatterns = patterns('',
                       url(r'^sync/update-balance/$', update_balance, name='utils.update_balance'),
                       url(r'^sync/update-site-power/$', update_site_power, name='utils.update_site_power'),
                       url(r'^sync/update-site-revenue/$', update_site_revenue, name='utils.update_site_revenue'),
                       )
