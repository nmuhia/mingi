import json
import requests
from django.http import Http404, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from mingi_user.models import Mingi_User
from mingi_django.models import Error_Log
from mingi_site.models import Hourly_Site_Power, Hourly_Site_Revenue

from mingi_django.settings import FORWARD


def sync_hourly_site_revenue(url):
    hourly_site_powers = Hourly_Site_Revenue.objects.all()
    site_revenue = {}
    post_url = 'https://' + url + '/utils/sync/update-site-revenue/'
    for hourly_site_power in hourly_site_powers:
        site_revenue['id'] = hourly_site_power.id
        site_revenue['site_id'] = hourly_site_power.site.id
        site_revenue['timestamp'] = str(hourly_site_power.timestamp)
        site_revenue['amount'] = str(hourly_site_power.amount)
        payload = {
            'data': json.dumps(site_revenue)
        }
        response = requests.post(post_url, data=payload)
        print response.content
    return HttpResponse("Done!")


@csrf_exempt
def update_site_revenue(request):
    if 'data' not in request.POST:
        return HttpResponse('No post data. Data please')
    else:
        data = json.loads(request.POST['data'])
        try:
            hourly_site_revenue, created = Hourly_Site_Revenue.objects.get_or_create(site_id=data['site_id'], timestamp=data['timestamp'], amount=data['amount'])
        except Hourly_Site_Revenue.MultipleObjectsReturned:
            Error_Log.objects.create(problem_type="INTERNAL",problem_message="Site Revenue multiple: site: "+str(data['site_id'])+" time: "+str(data['timestamp']))
            return HttpResponse('Multiple records existed')
        else:
            if created:
                Error_Log.objects.create(problem_type="INTERNAL",problem_message="Site Revenue multiple created: site: "+str(data['id'])+" time: "+str(data['timestamp']))
                return HttpResponse(str(hourly_site_revenue) + ' id:' + str(data['id']) + ' timestamp:' + data['timestamp'] + ' has been created ')
            else:
                return HttpResponse('The record already existed.')


def sync_hourly_site_power(url):
    hourly_site_powers = Hourly_Site_Power.objects.all()
    site_power = {}
    post_url = 'https://' + url + '/utils/sync/update-site-power/'
    for hourly_site_power in hourly_site_powers:
        site_power['id'] = hourly_site_power.id
        site_power['site_id'] = hourly_site_power.site.id
        site_power['timestamp'] = str(hourly_site_power.timestamp)
        site_power['amount'] = str(hourly_site_power.amount)
        payload = {
            'data': json.dumps(site_power)
        }
        response = requests.post(post_url, data=payload)
        print response.content
    return HttpResponse("Done!")


@csrf_exempt
def update_site_power(request):
    if 'data' not in request.POST:
        return HttpResponse('No post data. Data please')
    else:
        data = json.loads(request.POST['data'])
        try:
            hourly_site_power, created = Hourly_Site_Power.objects.get_or_create(site_id=data['site_id'], timestamp=data['timestamp'], amount=data['amount'])
        except Hourly_Site_Power.MultipleObjectsReturned:
            return HttpResponse('Multiple records exist')
        else:
            if created:
                return HttpResponse(str(hourly_site_power) + ' id:' + str(data['id']) + ' timestamp:' + data['timestamp'] + ' has been created ')
            else:
                return HttpResponse('The record already existed.')


def sync_accounts_balances(url):
    mingi_users = Mingi_User.objects.all()
    user = {}
    post_url = 'https://' + url + '/utils/sync/update-balance/'
    for mingi_user in mingi_users:
        user['id'] = mingi_user.id
        user['account_balance'] = str(mingi_user.account_balance)
        payload = {
            'data': json.dumps(user)
        }
        response = requests.post(post_url, data=payload)
        print response.content
    return HttpResponse("Done!")


@csrf_exempt
def update_balance(request):
    if 'data' not in request.POST:
        raise Http404
    else:
        data = json.loads(request.POST['data'])
        old_balance = Mingi_User.objects.get(id=data['id']).account_balance
        Mingi_User.objects.filter(id=data['id']).update(account_balance=data['account_balance'])
        user = Mingi_User.objects.get(id=data['id'])
        return HttpResponse(user.first_name + ' ' + user.last_name + ' update balance from: ' + str(old_balance) + ' to: ' + str(user.account_balance))


@csrf_exempt
def forward_post_request(request, url):
    """
    forward the post request data to a new url
    """

    print FORWARD
    if (FORWARD == "TRUE"):
        r = requests.post(url,data=request.POST)

    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Settings Error: Post forwarding request for URL: %s denied" % url)
