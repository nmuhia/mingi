"""
Tests for mingi_django models
"""
import datetime

from django.db.models import Count

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Line
from mingi_django.models import Error_Log
from mingi_django.tests import SteamaDBTestCase
from mingi_django.timezones import zone_aware

from .models import Hourly_Site_Revenue
from .views import filter_sites_by_label, update_lines

COUNT = 24


class MingiSiteTestCase(SteamaDBTestCase):

    def setUp(self):

        super(MingiSiteTestCase, self).setUp()

        i = 1
        end_date = zone_aware(self.all_user_site,
                              datetime.datetime(2016, 1, 1))
        start_date = end_date - datetime.timedelta(days=1)

        while start_date < end_date:
            Kopo_Kopo_Message.objects.create(user=self.all_user_site_manager,
                                             timestamp=start_date,
                                             amount=i)
            start_date = start_date + datetime.timedelta(hours=1)
            i += 2

    def test_duplicate_revenue_rollups(self):
        """
        Test checks for more than one hourly revenue rollup for a given hourly
        """
        end_date = zone_aware(self.all_user_site,
                              datetime.datetime(2016, 1, 1))
        start_date = end_date - datetime.timedelta(days=1)

        self.all_user_site.revenue_range(start_date, end_date)

        # check whether the rollups run successfully.
        self.assertEqual(
            Hourly_Site_Revenue.objects.filter(
                site=self.all_user_site).count(),
            24)

        # check specific revenue rollups
        said_date_1 = zone_aware(
            self.all_user_site,
            datetime.datetime(2016, 1, 1, 23, 0, 0, 0)) - \
            datetime.timedelta(days=1)
        said_date_2 = zone_aware(
            self.all_user_site,
            datetime.datetime(2016, 1, 1, 20, 0, 0, 0)) - \
            datetime.timedelta(days=1)

        self.assertEqual(
            Hourly_Site_Revenue.objects.get(site=self.all_user_site,
                                            timestamp=said_date_1).amount,
            47)
        self.assertEqual(
            Hourly_Site_Revenue.objects.get(site=self.all_user_site,
                                            timestamp=said_date_2).amount,
            41)

        # check for hourly duplicates
        any_duplicate = Hourly_Site_Revenue.objects\
            .filter(site=self.all_user_site).values('timestamp')\
            .annotate(dcount=Count('timestamp'))\
            .order_by('-timestamp')[0]['dcount']
        self.assertEqual(any_duplicate, 1)

    def test_lines_created_higher_num_lines(self):
        """
        Tests that lines are created when num_lines is updated 
        with higher number
        """
        self.all_user_site.num_lines = 3
        self.all_user_site.save()
        self.utility.name = "Electricity"
        self.utility.save()

        new_lines = 4
        old_lines = self.all_user_site.num_lines

        response = update_lines(self.all_user_site, old_lines, new_lines)
        self.assertEqual(Line.objects.filter(
            site=self.all_user_site).count(), 4)
        self.assertEqual(response, None)  # no error

    def test_rejects_line_removal_user_assigned(self):
        """
        Tests that assigned lines cannot be removed
        """
        self.all_user_site.num_lines = 3
        self.all_user_site.save()
        self.utility.name = "Electricity"
        self.utility.save()

        new_lines = 1
        old_lines = self.all_user_site.num_lines

        response = update_lines(self.all_user_site, old_lines, new_lines)
        self.assertEqual(Line.objects.filter(
            site=self.all_user_site).count(), 3)
        self.assertEqual(response, "Lines Assigned")

    def test_accepts_line_removal_no_user_assigned(self):
        """
        Tests that unassigned lines can be removed
        """
        self.all_user_site.num_lines = 4
        self.all_user_site.save()
        self.utility.name = "Electricity"
        self.utility.save()

        new_lines = 3
        old_lines = self.all_user_site.num_lines

        response = update_lines(self.all_user_site, old_lines, new_lines)
        self.assertEqual(Line.objects.filter(
            site=self.all_user_site).count(), 3)
        self.assertEqual(response, None)


class FilterSitesByLabel(SteamaDBTestCase):
    """
        Tests logic for filtering sites by labels
    """
    existing_label = "test2"
    non_existing_label = "test3"

    sites = Mingi_Site.objects.all()

    def test_successful_match(self):
        """
        Tests that filter sites with matching labels
        """
        test_site = Mingi_Site.objects.get(name="Limbo")
        test_site.labels = "test1, test2"
        test_site.save()

        response = filter_sites_by_label(self.sites, self.existing_label)

        # check that JSON response returned
        self.assertJSONEqual(
            response,
            {
                "units": 1,
                "sites": [
                    {
                        "lat": str(test_site.latitude),
                        "id": str(test_site.id),
                        "long": str(test_site.longitude),
                        "name": str(test_site.name)
                    }
                ],
                "arrears": "0.00"
            })

    def test_matching_labels_no_user(self):
        """
        Tests that filter sites with matching labels, but no users
        """
        self.limbo_site.labels = "test1, test2"
        self.limbo_site.save()

        # call filter_sites_by_label with matching label
        filter_sites_by_label(self.sites, self.existing_label)

        # test that error log is created when there's a match but site has no
        # users
        self.assertEqual(
            Error_Log.objects.filter(
                problem_message=str(self.limbo_site.name) +
                " site has no users").count(),
            1)

    def test_unmatching_labels(self):
        """
        Tests that filter sites with unmatching labels
        """
        # call filter_sites_by_label with unmatching label
        response = filter_sites_by_label(self.sites, self.non_existing_label)

        # test that correct response is returned
        self.assertEqual(response, "DoesNotExist")

        # test that error log is created when label does not exist
        self.assertEqual(
            Error_Log.objects.filter(
                problem_type="INTERNAL",
                problem_message=str(self.non_existing_label) +
                " label does not exist").count(),
            1)
