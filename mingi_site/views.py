import json
import datetime

from django.http import HttpResponse, Http404

from models import Mingi_Site, Bit_Harvester, Line
from site_energy_usage import get_site_energy_usage_rollup, get_utility_site_energy_usage_rollup

from mingi_user.models import Mingi_User
from mingi_django.models import Utility, Error_Log 
from africas_talking.models import InvalidUserRegistration,InvalidAttributeValue
from africas_talking.userResponse import processPhoneNo

from django.core.mail import EmailMessage
from africas_talking.models import Gateway
from mingi_django.mingi_settings.custom import EMAIL_ACTIVATION_ADDRESS


def register_site(request):
    
    name = request.POST['site_name']    
    company_name = request.POST['site_company_name']
    operator_name = request.POST['site_operator_name']
    num_lines = request.POST['site_num_lines']
    if request.POST['site_num_modbus_lines']:
        num_modbus_lines = request.POST['site_num_modbus_lines']
    else:
        num_modbus_lines = 0
    equipment = request.POST['site_equipment']
    latitude = request.POST['site_latitude']
    longitude = request.POST['site_longitude']
    currency = request.POST['site_currency']
    rent = request.POST['site_rent']
    personnel_fees = request.POST['site_personnel_fees']
    bh_number = request.POST['site_bh_number']
    bh_logging_hours = request.POST['bh_logging_hours']
    pricing_bundles = request.POST['site_pricing_bundles']
    capacities = request.POST['site_capacities']
    user_access = request.POST['site_user_access']
    mytimezone = request.POST['timezone']
    labels = request.POST['labels']
    get_bh = None 

    try:
        check_site_name = Mingi_Site.objects.get(name=name)
        return HttpResponse("Site name already exists.")
    except Mingi_Site.DoesNotExist as e:
        if len(bh_number) > 0:
            try:
                get_bh = Bit_Harvester.objects.get(site__name="Testing World",telephone=bh_number)
                site = site = Mingi_Site.objects.create(name=name,company_name=company_name,operator_name=operator_name,num_lines=num_lines,num_modbus_lines=num_modbus_lines,latitude=float(latitude),longitude=float(longitude),currency=currency,date_online=datetime.datetime.now(),timezone=mytimezone,labels=labels)  
                get_bh.site = site
                get_bh.logging_hours = bh_logging_hours
                get_bh.save()
            except Bit_Harvester.DoesNotExist as e:
                return HttpResponse("The bitHarvester number does not exist. Please contact administrator.") 

        else:   
            site = Mingi_Site.objects.create(name=name,company_name=company_name,operator_name=operator_name,num_lines=num_lines,latitude=float(latitude),longitude=float(longitude),currency=currency,date_online=datetime.datetime.now(),timezone=mytimezone,labels=labels)

        if len(rent) > 0:
            site.monthly_rent = float(rent)
        if len(equipment) > 0:
            site.equipment = equipment
        if len(personnel_fees) > 0:
            site.agent_fees = float(personnel_fees)
        if len(pricing_bundles) > 0:
            site.bundle_settings = pricing_bundles
        if len(capacities) > 0:
            site.capacities = capacities
        if len(user_access) > 0:      
            for user in user_access.split(","):
                site.owners.add(user)        

    try:
        site.save()
        total_num_lines = int(num_lines)
        add_lines(site,1,total_num_lines)
        
        #email admin
        email_admin = EmailMessage(subject='New Site: '+str(name),body="A new site has been registered by "+str(request.user.email),to=EMAIL_ACTIVATION_ADDRESS)
        try:
            email_admin.send()
        except:
            pass
        #email creator
        email_creator = EmailMessage(subject=str(name),body="Thank you for registering a new site with SteamaCo!",to=[request.user.email])
        try:
           email_creator.send()
        except:
            pass
        return HttpResponse(str(name)+" has been created successfully")
    except Exception as e:
        return HttpResponse("An error has occured while creating site. Please try again or contact administrator."+str(e))


def edit_site(request,site_id):
    adjust_lines_error = None
    name = request.GET['name']
    latitude = request.GET['latitude']
    longitude = request.GET['longitude']
    pulse_ratio = request.GET['pulse_ratio']
    number_of_lines = request.GET['num_lines']
    bundle_settings = request.GET['bundle_settings']
    TOU_hours = request.GET['TOU_hours']
    mytimezone = request.GET['timezone']
    labels = request.GET['labels']

    if len(TOU_hours) > 0 and TOU_hours[-1] == ';':
        TOU_hours = TOU_hours.rstrip(';')

    try:
        site = Mingi_Site.objects.get(id=site_id)
        users = Mingi_User.objects.filter(site=site)        
        for user in users:
            user.TOU_hours = TOU_hours
            user.save()

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))

    if(len(name) >0):
        site.name = name

    if(len(latitude) >0):
        site.latitude = float(latitude)

    if(len(longitude) >0):
        site.longitude = float(longitude)

    if(len(pulse_ratio) >0):
        site.pulse_ratio = pulse_ratio

    if(len(number_of_lines) >0):
        new_lines = int(number_of_lines)
        old_lines = int(site.num_lines)
        adjust_lines_error = update_lines(site,old_lines,new_lines)

    site.bundle_settings = bundle_settings
    site.timezone = mytimezone
    site.labels = labels
    
    try:
        site.save()

        if adjust_lines_error is not None:
            return HttpResponse("Site edited successfully but updating lines for site has failed. Lines have users assigned.")
        else:
            return HttpResponse("Site edited successfully")

    except Exception as e:
        return HttpResponse("There was an error saving the edits. Please try again.")


def edit_bh(request,bh_id):
    name = request.POST['name']
    telephone = request.POST['telephone']
    if 'serial_number' in request.POST:
        serial_number = request.POST['serial_number']
    else:
        serial_number = None
    send_telephone = request.POST['send_telephone']
    # differentiate staff view from client view
    if 'input_gateway' in request.POST:
        input_gateway = request.POST['input_gateway']
        output_gateway = request.POST['output_gateway']
        backup_gateway_in = request.POST['backup_gateway_in']
        backup_gateway_out = request.POST['backup_gateway_out']
    else:
        input_gateway = None
        output_gateway = None
        backup_gateway_in = None
        backup_gateway_out = None
        
    version = request.POST['version']
    logging_hours = request.POST['logging_hours']

    try:
        bh = Bit_Harvester.objects.get(id=bh_id)
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))

    if(len(name) > 0):
        bh.harvester_id = name

    if(len(telephone) > 0):
        bh.telephone = telephone

    if(serial_number):
        bh.serial_number = serial_number

    if(len(send_telephone) > 0):
        bh.send_telephone = send_telephone

    if(input_gateway):
        bh.input_gateway = input_gateway

    if(output_gateway):
        bh.output_gateway = output_gateway

    if(backup_gateway_in):
        bh.backup_gateway_in = backup_gateway_in

    if(backup_gateway_out):
        bh.backup_gateway_out = backup_gateway_out

    if(len(version) > 0):
        bh.version = version

    if(len(logging_hours) > 0):
        bh.logging_hours = logging_hours

    
    try:
        bh.save()
        return HttpResponse(str(name) +" edited successfully")
    except Exception as e:
        return HttpResponse("There was an error saving the edits. Please try again.")

    

def register_bh(request):

    name = request.POST['name']
    telephone = request.POST['telephone']
    site_id = Mingi_Site.objects.get(name="Testing World").id
    post = request.POST
    if post['input_gateway']=='':
        input_gateway = None
    else:
        input_gateway = post['input_gateway']
    if post['output_gateway']=='':
        output_gateway = None
    else:
        output_gateway = post['output_gateway']
    if post['backup_gateway_in']=='':
        backup_gateway_in = None
    else:
        backup_gateway_in = post['backup_gateway_in']
    if post['backup_gateway_out']=='':
        backup_gateway_out = None
    else:
        backup_gateway_out = post['backup_gateway_out']

    # gateways = Gateway.objects.all()

    try:
        bh = None
        telephone = processPhoneNo(telephone)        
    except InvalidUserRegistration as e:
        if  (e.error_type == 2) and (bh is not None):
            if (bh.telephone == telephone):
                telephone = telephone
            else:
                return HttpResponse("Fatal Error: " + e.msg + ". Please contact your system administrator for assistance.")
        else:
            return HttpResponse("Fatal Error: " + e.msg + ". Please contact your system administrator for assistance.")

    try:
        check_bh_name = Bit_Harvester.objects.get(harvester_id=name)
        return HttpResponse("bitHarvester name already exists.")
    except Bit_Harvester.DoesNotExist as e:
        bh=Bit_Harvester.objects.create(harvester_id=name,telephone=telephone,approval_date=datetime.datetime.now(),bh_type='AT',site_id=site_id,send_telephone=post['send_telephone'],serial_number=post['serial_number'],version=post['version'],logging_hours=post['logging_hours'])
        bh.input_gateway = Gateway.objects.get(id=input_gateway)
        bh.output_gateway = Gateway.objects.get(id=output_gateway)
        bh.backup_gateway_in = Gateway.objects.get(id=backup_gateway_in)
        bh.backup_gateway_out = Gateway.objects.get(id=backup_gateway_out)
        bh.save()
        # input_gateway=input_gateway,output_gateway=output_gateway,backup_gateway_in=backup_gateway_in,backup_gateway_out=backup_gateway_out,

        #send message to admin
        email = EmailMessage(subject=str(name) +' Registration',body="A new bitHarvester: "+str(name)+" has been created by "+str(request.user.username)+"  Email : "+str(request.user.email),to=EMAIL_ACTIVATION_ADDRESS)
        try:
            email.send()
        except:
            pass

        #send email to creator
        email = EmailMessage(subject=str(name)+' Registration',body="Thank you for registering a new bitHarvester with SteamaCo! You can now add the bitHarvester to a site. If you have any questions, kindly contact us.",to=EMAIL_ACTIVATION_ADDRESS)
        try:
            email.send()
        except:
            pass
        return HttpResponse(str(name)+": bitHarvester registered successfully.")


def site_details_handler(request):
    """
    Handles async requests for site details.
    """

    ret_dict = {}

    site_id = request.GET.get('siteId')

    if site_id is not None:
        try:
            if request.user.is_staff:
                this_site = Mingi_Site.objects.get(id=site_id)
                          
            else:
                this_site = Mingi_Site.objects.get(id=site_id, 
                                                   owners=request.user)
        except Mingi_Site.DoesNotExist:
            raise Http404
        except ValueError:
            raise Http404

    ret_dict['users'] = {}
    site_users = Mingi_User.objects.filter(site=this_site, 
                                           is_user=True)
    ret_dict['users']['total'] = site_users.count()
    ret_dict['users']['active'] = 0
    ret_dict['users']['inactive'] = 0
    ret_dict['users']['sending_messages'] = 0
    ret_dict['users']['using_power'] = 0
    for user in site_users:
        if user.is_active:
            ret_dict['users']['active'] += 1
        else:
            ret_dict['users']['inactive'] += 1
        if user.is_sending_messages:
            ret_dict['users']['sending_messages'] += 1
        if user.is_using_power:
            ret_dict['users']['using_power'] += 1

    ret_dict['num_lines'] = this_site.num_lines
    ret_dict['equipment'] = this_site.equipment_list
    ret_dict['company_name'] = this_site.company_name
    ret_dict['revenue_to_date'] = this_site.revenue_to_date
    ret_dict['energy_to_date'] = this_site.energy_to_date

    return HttpResponse(json.dumps(ret_dict), content_type="application/json")


def get_bit_harvester_info(bit_harvester):
    """
    Returns a dictionary with information about the given BitHarvester.
    """
    ret_dict = bit_harvester.__dict__
    ret_dict['bit_harvester_found'] = True

    try:
        ret_dict['site'] = Mingi_Site.objects.get(id=
                                                     ret_dict['site_id'])
    except Mingi_Site.DoesNotExist, e:
        pass

    return ret_dict


def bit_harvester_search(request):
    """
    Handles searching for a BitHarvester on the transactions page.
    """
    if request.method == "POST":
        params = request.POST
    else:
        params = request.GET

    if params.get('harvester_id', u'') is not u'':
        try:
            Bit_Harvester.objects.get(harvester_id=params['harvester_id'])
            return params['harvester_id']
        except Bit_Harvester.DoesNotExist, e:
            raise BitHarvesterNotFound

    if ((params.get('serial_number', u'') is not u'') and
        (params.get('telephone', u'') is not u'')):
        raise BitHarvesterNotFound

    bit_harvester_query = Bit_Harvester.objects.all()

    if not request.user.is_staff:
        bit_harvester_query = bit_harvester_query.filter(
            site__owners=request.user
        )

    if 'serial_number' in params:
        bit_harvester_query = bit_harvester_query.filter(
                   serial_number__icontains=params['serial_number'].strip())

    if 'telephone' in params:
        bit_harvester_query = bit_harvester_query.filter(
                               telephone__icontains=params['telephone'].strip())

    if len(bit_harvester_query) == 0:
        raise BitHarvesterNotFound
    
    return bit_harvester_query[0].harvester_id

def get_site_info(request,site):
    """
    Returns a dictionary with information about the given Mingi site.
    """
    users_list = list()

    ret_dict = site.__dict__
    ret_dict['site_found'] = True

    if ret_dict['equipment'] is not u'':
        ret_dict['equipment_list'] = ret_dict['equipment'].split(',')

    bit_harvesters = Bit_Harvester.objects.filter(site=site)
    if (len(bit_harvesters) > 0):
        ret_dict['bit_harvesters'] = bit_harvesters
    
    for owner in site.owners.all(): 
        if owner not in users_list and owner.is_staff == False:
            users_list.append(owner)

    if(len(users_list) > 0):
        ret_dict['users'] = users_list

    ret_dict['id'] = site.id

    #bundle_settings = str(site.bundle_settings)
    #if bundle_settings != "" or bundle_settings is not None:
    #    if bundle_settings.endswith(';'):
    #        bundle_settings = bundle_settings[:-1]
    #    print str(bundle_settings)+" bundle settings:::"

    #    bundles_list = list()
    #    for bundle in bundle_settings.split(';'):
    #        setting = bundle.split(':')            
    #        bundle_range = float(setting[0])
    #        bundle_ratio = float(setting[1])
    #        bundles_list.append({"bundle_range":bundle_range,"bundle_ratio":bundle_ratio})
            
        
    #ret_dict['bundle_settings'] = bundle_settings


    return ret_dict


def site_search(request):
    """
    Handles searching for a Mingi site on the transactions page.
    """
    if request.method == "POST":
        params = request.POST
    else:
        params = request.GET

    if params.get('name', u'') is not u'':
        try:
            return Mingi_Site.objects.get(name=params['name']).id
        except Mingi_Site.DoesNotExist, e:
            mingi_site_query = Mingi_Site.objects.filter(name__icontains=
                                                         params['name'].strip())
            if not request.user.is_staff:
                mingi_site_query = mingi_site_query.filter(owners=request.user)

            if len(mingi_site_query) == 0:
                raise MingiSiteNotFound

            return mingi_site_query[0].id

    raise MingiSiteNotFound

def update_lines(site,old_lines,new_lines):
    if new_lines > old_lines:
        lines_to_add = new_lines - old_lines
        last_line_number = old_lines
        lines_count = lines_to_add + last_line_number
        site.num_lines = new_lines 
        site.save()  
        return add_lines(site,1,lines_count)

    elif old_lines > new_lines:
        return remove_lines(site,old_lines,new_lines)
            
    else:
        return

def add_lines(site,initial_count,last_count):
    """
    Create lines during site registration/editing
    """
    utility = Utility.objects.get(name="Electricity")

    try:
        for number in range(initial_count, last_count+1):            
            if Line.objects.filter(site=site,number=number).exists():
                continue
            else:
                line_name = "%s - Line %d" % (site.name,number)
                Line.objects.create(
                    site=site,
                    number=number,
                    name=line_name,
                    user=None,
                    line_status='6',
                    is_connected=False,
                    utility_type=utility
                )                     
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))

def remove_lines(site,old_lines,new_lines):
    try:        
        user_lines = Line.objects.filter(site=site,number__lte=old_lines,number__gt=new_lines)
        user_lines = user_lines.exclude(user=None)
        

        if(len(user_lines) > 0):
            Error_Log.objects.create(problem_type="INTERNAL",
                                    problem_message="Editing lines for site:"+str(site.name)+" has failed -- lines have users assigned")
            return "Lines Assigned"

        else:
            for num in range(old_lines,new_lines,-1):
                try:
                    line = Line.objects.get(number=num,site=site)
                    line.delete()
                except Exception as e:
                    Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))
            site.num_lines = new_lines
            site.save()

    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))

def filter_sites_by_label(sites, label):
    try:
        total_arrears = 0
        units = 0

        if label !="":
            sites = sites.filter(labels__icontains=label)

        if sites.count() > 0:
            ret_dict = {
                "sites":[]
            }

            for site in sites:
                site_dict = {
                    'id':[],
                    'name':[],
                    'long':[],
                    'lat':[]
                }
                users = Mingi_User.objects.filter(site=site)

                if users.count() > 0:
                    for user in users:
                        if float(user.account_balance) < 0:
                            total_arrears += abs(float(user.account_balance))
                else:
                    Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(site.name)+" site has no users")
                    pass

                site_dict['id'] = str(site.id)
                site_dict['name'] = str(site.name)
                site_dict['long'] = str(site.longitude)
                site_dict['lat'] = str(site.latitude)

                ret_dict['sites'].append(site_dict)

            ret_dict['units'] = len(sites)
            ret_dict['arrears'] = "{0:.2f}".format(total_arrears)
            return json.dumps(ret_dict)

        else:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(label)+" label does not exist")
            return "DoesNotExist"
                    
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(label)+" label does not exist")
        return None


class BitHarvesterNotFound(Exception):
    """
    Thrown when a BitHarvester cannot be found.
    """
    pass


class MingiSiteNotFound(Exception):
    """
    Thrown when a Mingi site cannot be found.
    """
    pass
