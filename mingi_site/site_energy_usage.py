"""
Functions for viewing a site's energy usage.

Wesley Verne
"""

import datetime
from datetime import date, timedelta, datetime

from mingi_django.models import Error_Log, Utility
from mingi_site.models import Line, Mingi_Site, Site_Record, Line_State
from mingi_user.user_energy_usage import js_timestamp
from mingi_user.methods import get_utility_lists, get_site_utility_lists

from django.utils import timezone
from django.utils.timezone import now,utc
from django.db.models import Q,Count
from mingi_django.timezones import zone_aware,gen_zone_aware


def get_utility_site_energy_usage_rollup(request,site, start_datetime=None, end_datetime=None):
    """Gets the given site's energy usage over the specified time period.  Defaults
    to last three days."""

    rollup = {}        

    subtract = 0
    count = 0

    rollup['utilities'] = {}
    utility_lists = get_utility_lists(site)

    for utility in request.user.website_user_profile.utilities.all():
        if count < 2:   
            chart_label = utility.get_ts_label(True)
            rollup['utilities'][chart_label] = {'total':[]}        
            line_states = Line_State.objects.filter(user__first_name=utility.name,site=site)            

            if site.source_line_number:
                exclude_nos = site.source_line_array
                subtract = len(exclude_nos)
                for line in exclude_nos:
                    line_states = line_states.exclude(number=line)
                
            if start_datetime:
                line_states = line_states.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)
                rollup['end_date'] = start_datetime
                rollup['start_date'] = end_datetime
            else:
                line_states = line_states.filter(timestamp__gte=(timezone.now() - timedelta(days=3)))
                rollup['end_date'] = timezone.now()
                rollup['start_date'] = timezone.now() - timedelta(days=3)

            try:
                if(utility.name == "Electricity"):
                    first_line = utility_lists[0][0]
                elif(utility.name == "Water"):
                    first_line = utility_lists[1][0]
                elif(utility.name == "Fuel"):
                    first_line = utility_lists[2][0]

            except Exception as e:
                first_line = 0                                


            first_line_states = [state for state in line_states if state.number == first_line]

            energy_list = []

            for first_line_state in first_line_states:        

                this_datetime = first_line_state.timestamp

                current_line_states = [state for state in line_states if state.timestamp == this_datetime]
                        
                current_energy = 0.0
                
                for line_state in current_line_states:
                    if line_state.meter_reading is not None:
                        current_energy += float(line_state.meter_reading)                    

                energy_list.append({
                    'energy': current_energy,
                    'timestamp': js_timestamp(this_datetime)
                })              

            for i in range(len(energy_list)):

                rollup['utilities'][chart_label]['total'].append({
                    'rate': get_site_energy_usage_rate(energy_list, i),
                    'timestamp': energy_list[i]['timestamp']
                })
        else:
            break;
        count = count + 1
        
    return rollup

def get_site_energy_usage_rollup(site, utility_type, start_datetime=None, end_datetime=None):

    """Gets the given site's energy usage over the specified time period. Defaults
    to last three days."""
    
    rollup = {}

    utility = Utility.objects.get(name=utility_type)

    chart_label = utility.get_ts_label(True)   
    rollup['secondary_unit'] = utility.secondary_unit
    rollup['chart_label'] = chart_label
    current_records = False

    #filter by site, and meter readings
    line_states = Line_State.objects.filter(site=site,meter_reading__isnull=False)   

    # filter by date
    if start_datetime:
        start_datetime = zone_aware(site,datetime.strptime(start_datetime,'%Y-%m-%d'))
        end_datetime = zone_aware(site,datetime.strptime(end_datetime,'%Y-%m-%d'))

        line_states = line_states.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)
        rollup['end_date'] = start_datetime
        rollup['start_date'] = end_datetime
    else:
        now = zone_aware(site,datetime.now())
        line_states = line_states.filter(timestamp__gte=(now - timedelta(days=3)))
        rollup['end_date'] = now
        rollup['start_date'] = now - timedelta(days=3)


    subtract = 0

    for line in Line.objects.filter(site=site):
        if str(line.utility_type.name) != str(utility.name):        
            line_states = line_states.exclude(number=line.number)
            subtract += 1

    
    # filter out lines that don't measure power use
    if site.source_line_number:
        exclude_nos = site.source_line_array
        subtract += len(exclude_nos)
        for line in exclude_nos:
            line_states = line_states.exclude(number=line)                     

               
    rollup['rates'] = {'total':[]}
    rollup['temp'] = {'total':[]}

    if utility_type == "Electricity":
        rollup['volts'] = {'total':[]}
    
    logs = Site_Record.objects.filter(site=site)
    current_logs = logs.filter(key="Current Log")

    if current_logs.count() > 0:
        rollup['current'] = {'total':[]}
        logs = logs.filter(Q(key="System Voltage") | Q(key="BH Data Log") | Q(key="Current Log"))
        current_records = True
    else:
        logs = logs.filter(Q(key="System Voltage") | Q(key="BH Data Log"))   
    
    if start_datetime:
        logs = logs.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)
    else:
        logs = logs.filter(timestamp__gte=(now - timedelta(days=3)))        

    for log in logs:
        create_log = True
        volt_reading = None
        temp_reading = None
        curr_reading = None
        value_array = log.value.split(',')

        if log.timestamp is not None:
            timestamp = log.timestamp
            
            if log.key == "System Voltage":
                volt_reading = log.value
            elif log.key == "BH Data Log":
                try:
                    volt_reading = value_array[0]
                    if value_array[1] != "-100":
                        temp_reading = value_array[1]
                except IndexError:
                    create_log = False
            elif log.key == "Current Log":
                curr_reading = log.value

        # DEPRECATE
        """
        else:
            try:
                timestamp = value_array[1]
                reading = value_array[0]
            except IndexError:
                create_log = False
        """

        if create_log:
            if utility_type == "Electricity":

                if volt_reading is not None:
                    rollup['volts']['total'].append({
                        'reading': volt_reading,
                        'timestamp': js_timestamp(timestamp),
                    })

            if temp_reading is not None:
                rollup['temp']['total'].append({
                    'reading': temp_reading,
                    'timestamp': js_timestamp(timestamp),
                })

            if current_records and curr_reading is not None:
                rollup['current']['total'].append({
                    'reading': curr_reading,
                    'timestamp': js_timestamp(timestamp)
                })

    try:
        utility_lists = get_site_utility_lists(site)
        for row in utility_lists:
            if(str(row['name']) == str(utility_type)):
                first_line = int(row['lines'][0])
                break
    except Exception as e:
        first_line = 0

    first_line_states = [state for state in line_states if int(state.number) == first_line]
    
    energy_list = []

    for first_line_state in first_line_states:
        this_datetime = first_line_state.timestamp

        current_line_states = [state for state in line_states if state.timestamp == this_datetime]
        
        if len(current_line_states) == site.num_lines - subtract:
            current_energy = 0.0
            
            for line_state in current_line_states:
                if line_state.meter_reading is not None:
                    current_energy += float(line_state.meter_reading)
                    
            energy_list.append({
                    'energy': current_energy,
                    'timestamp': js_timestamp(this_datetime)
                    })

            
    for i in range(len(energy_list)):
    
        rollup['rates']['total'].append({
            'rate': get_site_energy_usage_rate(energy_list, i),
            'timestamp': energy_list[i]['timestamp']
        })
    
    return rollup


def get_sites_utility_draw_rollup(request, utility=None, start_datetime=None, end_datetime=None, site_id=None):
  
    #Gets the given sites voltage over the specified time period.  Default to last three days.
    rollup = {
        'rates':{}
    }
    sites = Mingi_Site.objects.filter(is_active=True)

    if site_id is not None:
        sites = sites.filter(id=site_id)

    if not request.user.is_staff:
        sites = sites.filter(owners=request.user)

    if start_datetime:
        start_datetime = gen_zone_aware(request.session['current_timezone'],datetime.strptime(start_datetime, '%Y-%m-%d'))       
        end_datetime = gen_zone_aware(request.session['current_timezone'],datetime.strptime(end_datetime, '%Y-%m-%d'))      
        rollup['end_date'] = start_datetime
        rollup['start_date'] = end_datetime
    else:
        now = gen_zone_aware(request.session['current_timezone'],datetime.now())
        rollup['end_date'] = now
        rollup['start_date'] = now - timedelta(days=3)

    rollup['secondary_unit'] = utility.secondary_unit
    rollup['chart_label'] = utility.get_ts_label(True) 


    for site in sites:
        subtract = 0
        line_states = Line_State.objects.filter(site=site)

        for line in Line.objects.filter(site=site):
            if str(line.utility_type.name) != str(utility.name):        
                line_states = line_states.exclude(number=line.number)
                subtract += 1

        if site.source_line_number:
            exclude_nos = site.source_line_array
            subtract = len(exclude_nos)
            for line in exclude_nos:
                line_states = line_states.exclude(number=line)                
        

        rollup['rates'][site.name] = []
        
        if start_datetime:        
            line_states = line_states.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)            
        else:
            line_states = line_states.filter(
                timestamp__gte=(now - timedelta(days=3))
            )            

        first_line_states = [state for state in line_states if state.number == 1]        
        

        energy_list = []

        for first_line_state in first_line_states:

            this_datetime = first_line_state.timestamp

            current_line_states = [state for state in line_states if state.timestamp == this_datetime]
            
            if len(current_line_states) == site.num_lines - subtract:
                current_energy = 0.0
                
                for line_state in current_line_states:
                    if line_state.meter_reading is not None:
                        current_energy += float(line_state.meter_reading)                    

                energy_list.append({
                    'energy': current_energy,
                    'timestamp': js_timestamp(this_datetime)
                })

        for i in range(len(energy_list)):

            rollup['rates'][site.name].append({
                'rate': get_site_energy_usage_rate(energy_list, i),
                'timestamp': energy_list[i]['timestamp']
            })

    return rollup



def get_sites_power_rollup(request, start_datetime=None, end_datetime=None):
  
    #Gets the given sites voltage over the specified time period.  Default to last three days.
    rollup = {
        'rates':{}
    }
    sites = Mingi_Site.objects.filter(is_active=True)

    if not request.user.is_staff:
        sites = sites.filter(owners=request.user)

    if start_datetime:        
        rollup['end_date'] = start_datetime
        rollup['start_date'] = end_datetime
    else:
        rollup['end_date'] = timezone.now()
        rollup['start_date'] = timezone.now() - timedelta(days=3)


    for site in sites:
        subtract = 0
        line_states = Line_State.objects.filter(site=site)

        if site.source_line_number:
            exclude_nos = site.source_line_array
            subtract = len(exclude_nos)
            for line in exclude_nos:
                line_states = line_states.exclude(number=line)                
        

        rollup['rates'][site.name] = []                
        
        if start_datetime:        
            line_states = line_states.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)            
        else:
            line_states = line_states.filter(
                timestamp__gte=(timezone.now() - timedelta(days=3))
            )            

        first_line_states = [state for state in line_states if state.number == 1]        
        

        energy_list = []

        for first_line_state in first_line_states:

            this_datetime = first_line_state.timestamp

            current_line_states = [state for state in line_states if state.timestamp == this_datetime]
            
            if len(current_line_states) == site.num_lines - subtract:
                current_energy = 0.0
                
                for line_state in current_line_states:
                    if line_state.meter_reading is not None:
                        current_energy += float(line_state.meter_reading)                    

                energy_list.append({
                    'energy': current_energy,
                    'timestamp': js_timestamp(this_datetime)
                })

        for i in range(len(energy_list)):

            rollup['rates'][site.name].append({
                'rate': get_site_energy_usage_rate(energy_list, i),
                'timestamp': energy_list[i]['timestamp']
            })

    return rollup

def get_sites_voltage_rollup(request, start_datetime=None, end_datetime=None, site_id=None):
    """
    Gets the given sites voltage over the specified time period.  Defaults
    to last three days.
    """
    rollup = {
        'volts':{}
    }

    sites = Mingi_Site.objects.filter(is_active=True)

    if site_id is not None:
        sites = sites.filter(id=site_id)

    if not request.user.is_staff:
        sites = sites.filter(owners=request.user)

    if start_datetime:
        start_datetime = gen_zone_aware(request.session['current_timezone'],datetime.strptime(start_datetime, '%Y-%m-%d'))           
        end_datetime = gen_zone_aware(request.session['current_timezone'],datetime.strptime(end_datetime, '%Y-%m-%d'))           
        rollup['end_date'] = start_datetime
        rollup['start_date'] = end_datetime
    else:
        now = gen_zone_aware(request.session['current_timezone'],timezone.now())
        rollup['end_date'] = now
        rollup['start_date'] = now - timedelta(days=3)


    for site in sites:
        rollup['volts'][site.name] = []
        voltage_logs = Site_Record.objects.filter(Q(site=site),Q(key="System Voltage") | Q(key="BH Data Log"))
        if start_datetime:        
            voltage_logs = voltage_logs.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)
        else:
            voltage_logs = voltage_logs.filter(
                timestamp__gte=(now - timedelta(days=3))
            )

        for log in voltage_logs:            
            create_log = True
            value_array = log.value.split(',')

            if log.timestamp is not None:
                timestamp = log.timestamp

                if log.key == "System Voltage":
                    reading = log.value
                elif log.key == "BH Data Log":
                    try:                    
                        reading = value_array[0]                  
                    except IndexError:
                        create_log = False
            else:            
                try:
                    timestamp = value_array[1]
                    reading = value_array[0]                
                except IndexError:
                    create_log = False

            if create_log:
                rollup['volts'][site.name].append({
                    'reading': reading,
                    'timestamp': js_timestamp(timestamp)
                })            

    return rollup


def get_sites_temperature_rollup(request, start_datetime=None, end_datetime=None, site_id=None):
    """
    Gets the given sites temperature over the specified time period.  Defaults
    to last three days.
    """
    rollup = {
        'temp':{}
    }
    sites = Mingi_Site.objects.filter(is_active=True)

    if site_id is not None:
        sites = sites.filter(id=site_id)

    if not request.user.is_staff:
        sites = sites.filter(owners=request.user)

    if start_datetime:
        start_datetime = gen_zone_aware(request.session.current_timezone,datetime.strptime(start_datetime, '%Y-%m-%d'))       
        end_datetime = gen_zone_aware(request.session.current_timezone,datetime.strptime(end_datetime, '%Y-%m-%d'))

        rollup['end_date'] = start_datetime
        rollup['start_date'] = end_datetime
    else:
        now = gen_zone_aware(request.session.current_timezone,timezone.now())
        rollup['end_date'] = now
        rollup['start_date'] = now - timedelta(days=3)


    for site in sites:
        rollup['temp'][site.name] = []
        voltage_logs = Site_Record.objects.filter(Q(site=site),Q(key="BH Data Log"))
        if start_datetime:        
            voltage_logs = voltage_logs.filter(timestamp__gte=start_datetime,timestamp__lte=end_datetime)
        else:
            voltage_logs = voltage_logs.filter(
                timestamp__gte=(now - timedelta(days=3))
            )

        for log in voltage_logs:            
            create_log = True
            value_array = log.value.split(',')

            if log.timestamp is not None:
                timestamp = log.timestamp

            try:
                reading = value_array[1]
            except IndexError:
                create_log = False

            if create_log:
                rollup['temp'][site.name].append({
                    'reading': reading,
                    'timestamp': js_timestamp(timestamp)
                })            

    return rollup


def get_site_energy_usage_rate(energy_list, i):
    """
    Estimate the energy usage at the given index of the given energy usage list.
    Units of kW (meter readings are given in kWh)
    """

    if len(energy_list) <= 1:
        return 0.0

    if i == (len(energy_list) - 1):
        p1 = energy_list[i]
    else:
        p1 = energy_list[i+1]

    if i == 0:
        p2 = energy_list[i]
    else:
        p2 = energy_list[i-1]

    energy_delta = p2['energy'] - p1['energy']
    time_delta = (p2['timestamp'] - p1['timestamp']) / (3600.0 * 1000.0)

    if time_delta <= 0:
        return 0.0

    return float(energy_delta) / time_delta
