import datetime

from mingi_site.models import Line
from mingi_django.models import Utility

def get_site_utility_lists(site):
    """
    returns list of site lines broken down by utility
    """
    utility_list = []

    for utility in Utility.objects.all():
        utility_info = {}         
        utility_info['name'] = str(utility.name)
        site_lines = list()
        for line in Line.objects.filter(site=site,utility_type=utility):
            site_lines.append(line.number)
        if len(site_lines) > 0:
            utility_info['lines'] = site_lines
            utility_list.append(utility_info)
    return utility_list

    
