"""
    functions for viewing site charts
"""

from datetime import date, timedelta, datetime

from django.http import HttpResponse
from django.utils import timezone
from django.utils.timezone import now
from django.utils.timezone import now,utc

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_site.models import Mingi_Site, Hourly_Site_Revenue, Bit_Harvester, Line_State,Daily_Power
from mingi_user.models import Mingi_User
from mingi_user.user_energy_usage import get_utility_cost
from mingi_django.models import Utility
from mingi_user.methods import get_utility_lists, get_site_utility_lists,get_user_utility_lists
from mingi_django.timezones import zone_aware
import pytz


ACTIVITY_GRAPH_NUM_DAYS = 7

def get_site_financial_chart(site, conversion_rate=1, start=None, end=None):
    """
    summary analysis for site revenues
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7
    duration_total = 0.0
    if start is None:
        end = zone_aware(site,datetime.now()).replace(hour=0, minute=0, second=0, microsecond=0)
        start = end - timedelta(days=ACTIVITY_GRAPH_NUM_DAYS)

    else:
        start = zone_aware(site,start)
        end = zone_aware(site,end)
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    ret_dict= {'dates': [],'total': [],'duration_total': {}}
    

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        day_total=0.0        
        next_day = end - timedelta(days=i) 
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        site_day_total = 0.0

        #check if site is combined
        if site.is_combined:
            for single_site in site.sites.all():                       
                site_day_total += single_site.revenue_range(next_day, next_day + timedelta(days=1)) * conversion_rate
        else:                    
            site_day_total = site.revenue_range(next_day, next_day + timedelta(days=1)) * conversion_rate

        day_total += site_day_total
        duration_total += day_total                

        ret_dict['total'].append(day_total)

    ret_dict['duration_total'] = duration_total
    ret_dict['last_date'] = str(end.date())
    ret_dict['begin_date'] = str(start.date())
    ret_dict['dates_range'] = (end - start).days

    return ret_dict


#for building management
def get_building_site_cost_chart(site,start=None,end=None):
    """
    summary analysis for site costs
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7
    duration_total = 0.0

    if start is None:
        end = now().replace(hour=0, minute=0, second=0, microsecond=0)
        end = zone_aware(site,end)

    else: 
        start = datetime.strptime(start, "%Y-%m-%d")
        end = datetime.strptime(end, "%Y-%m-%d")

        start = zone_aware(site,start)
        end = zone_aware(site,end)
        # start = timezone.make_aware(start,timezone=utc)
        # end = timezone.make_aware(end,timezone=utc)
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    ret_dict= {'dates': [],'total': [],'duration_total': {}}
    

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):        
        day_total=0.0
        next_day = end - timedelta(days=i)        
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        for user in Mingi_User.objects.filter(site=site):
            if user.first_name == "Water:" or user.first_name == "Electricity:" or user.first_name == "Fuel:":
                user_day_total = get_utility_cost(user,next_day, next_day + timedelta(days=1)) 
                day_total += float(user_day_total)

                duration_total += float(user_day_total)

        ret_dict['total'].append(day_total)

    ret_dict['duration_total'] = duration_total

    return ret_dict


def get_site_technical_chart(site,utility_type,start=None,end=None):
    """
    summary analysis for site technicals
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7
    duration_total = 0.0

    if start is None:
        end = zone_aware(site,datetime.now()).replace(hour=0, minute=0, second=0, microsecond=0)

    else: 
        start = datetime.strptime(start, "%Y-%m-%d")
        end = datetime.strptime(end, "%Y-%m-%d")

        start = zone_aware(site,start)
        end = zone_aware(site,end)
        # avoid using pytz, use arrow
        # start = timezone.make_aware(start,timezone=pytz.timezone(site.timezone))
        # end = timezone.make_aware(end,timezone=pytz.timezone(site.timezone))
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    ret_dict= {'dates': [],'total': [],'duration_total': {}}

    utility = Utility.objects.get(name=str(utility_type))
    ret_dict['chart_label'] = utility.get_totals_label(True)  
    ret_dict['chart_units'] = utility.get_totals_unit(True)

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        day_total=0.0
        next_day = end - timedelta(days=i)
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))

        #check if site is combined
        if site.is_combined:
            for single_site in site.sites.all():  
                rollup = get_site_utility_lists(site)
                print "ROLLUP: "+str(rollup)
                day_total += single_site.energy_range(next_day, next_day + timedelta(days=1),rollup,False,utility_type)
        else:  
            rollup = get_site_utility_lists(site)
            day_total += site.energy_range(next_day, next_day + timedelta(days=1),rollup,False,utility_type)
    
        
        duration_total += day_total
        ret_dict['total'].append(day_total)

    ret_dict['duration_total'] = "%.2f" % float(duration_total)

    return ret_dict

# Get daily user utility usage
def get_user_technical_chart(user,utility_type,start=None,end=None):
    """
    summary analysis for site technicals
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7
    duration_total = 0.0

    if start is None:
        end = zone_aware(user,now()).replace(hour=0, minute=0, second=0, microsecond=0)

    else: 
        start = datetime.strptime(start, "%Y-%m-%d")
        end = datetime.strptime(end, "%Y-%m-%d")
        start = timezone.make_aware(start,timezone=pytz.timezone(user.site.timezone))
        end = timezone.make_aware(end,timezone=pytz.timezone(user.site.timezone))
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    ret_dict= {'dates': [],'total': [],'duration_total': {}}

    utility = Utility.objects.get(name=str(utility_type))
    ret_dict['chart_label'] = utility.get_totals_label(True)  
    ret_dict['chart_units'] = utility.get_totals_unit(True)

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        day_total=0.0
        next_day = end - timedelta(days=i)
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))
        rollup = get_user_utility_lists(user)
        try:
            utility = Utility.objects.get(name=utility_type)
        except:
            utility = None
            
        try:
            # check for Mingi_User
            if type(user)==Mingi_User:
                day_total += float(Daily_Power.objects.filter(user=user,utility=utility,timestamp=next_day)[0].amount)
            else:
                # check for independent lines total
                today = next_day.replace(hour=0,minute=0,second=0)
                day_total += float(user.total_daily_line_power(today))
        except Exception as e:
            print e
            day_total +=0.0
        # day_total += user.power_used(next_day, next_day + timedelta(days=1),utility_type)
        print "daily total :"+str(day_total)
    
        
        duration_total += day_total
        ret_dict['total'].append(day_total)

    ret_dict['duration_total'] = "%.2f" % float(duration_total)

    return ret_dict


#for building management
def get_building_site_technical_chart(request,site,start=None,end=None):
    """
    summary analysis for site technicals
    """
    ACTIVITY_GRAPH_NUM_DAYS = 7
    duration_total = 0.0

    if start is None:
        end = now().replace(hour=0, minute=0, second=0, microsecond=0)

    else: 
        start = datetime.strptime(start, "%Y-%m-%d")
        end = datetime.strptime(end, "%Y-%m-%d")
        start = timezone.make_aware(start,timezone=utc)
        end = timezone.make_aware(end,timezone=utc)
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    ret_dict = {
        'dates':[],
        'duration_total':{}
    }    

    utilities = request.user.website_user_profile.utilities.count()
    if utilities > 0:
        utility_lists = get_site_utility_lists(site)

        ret_dict['utilities'] = {}

        for utility in request.user.website_user_profile.utilities.all():
            ret_dict['utilities'][str(utility.daily_totals_label)] = {'total':[]}

        for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):                        
            next_day = end - timedelta(days=i)
            ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))                        

            for utility in request.user.website_user_profile.utilities.all():
                utility_total = 0.0
                if utility.name == "Electricity":                    
                    utility_total = site.energy_range(next_day, next_day + timedelta(days=1),utility_lists,False)
                elif utility.name == "Water":              
                    utility_total = site.energy_range(next_day, next_day + timedelta(days=1),utility_lists,False)
                elif utility.name == "Fuel":       
                    utility_total = site.energy_range(next_day, next_day + timedelta(days=1),utility_lists,False)

                ret_dict['utilities'][str(utility.daily_totals_label)]['total'].append(utility_total)   

    ret_dict['duration_total'] = "%.2f" % float(duration_total)

    return ret_dict


def get_site_communication_chart(site,start=None,end=None):
    """
    summary analysis for site communication
    """
    ret_dict = {
        'dates': [],
        'sent': [],
        'received': [],
        'payments': []
    }

    ACTIVITY_GRAPH_NUM_DAYS = 7

    if start is None:
        end = zone_aware(site,datetime.now()).replace(hour=0, minute=0, second=0, microsecond=0).date()

    else: 
        # start = datetime.strptime(start, "%Y-%m-%d")
        # end = datetime.strptime(end, "%Y-%m-%d")
        start = zone_aware(site,start).date()
        end = zone_aware(site,end).date()
        ACTIVITY_GRAPH_NUM_DAYS = (end - start).days + 1

    for i in reversed(range(ACTIVITY_GRAPH_NUM_DAYS)):
        next_day = end - timedelta(days=i)
        ret_dict['dates'].append("{0.month}/{0.day}".format(next_day))      

        #check if site is combined
        if site.is_combined:
            at_Out_Count = 0
            at_In_Count = 0
            k2_Count = 0
            for single_site in site.sites.all():
                at_Out_Count += Africas_Talking_Output.objects.filter(
                        timestamp__gte=next_day,
                        timestamp__lt=next_day + timedelta(days=1),
                        user__site=single_site
                    ).count() + Africas_Talking_Output.objects.filter(
                        timestamp__gte=next_day,
                        timestamp__lt=next_day + timedelta(days=1),
                        bit_harvester__site=single_site
                    ).count()

                at_In_Count += Africas_Talking_Input.objects.filter(
                        timestamp__gte=next_day,
                        timestamp__lt=next_day + timedelta(days=1),
                        user__site=single_site
                    ).count() + Africas_Talking_Input.objects.filter(
                        timestamp__gte=next_day,
                        timestamp__lt=next_day + timedelta(days=1),
                        bit_harvester__site=single_site
                    ).count()

                k2_Count += Kopo_Kopo_Message.objects.filter(
                        timestamp__gte=next_day,
                        timestamp__lt=next_day + timedelta(days=1),
                        user__site=single_site
                    ).count()        

            ret_dict['sent'].append(at_Out_Count)
            ret_dict['received'].append(at_In_Count)
            ret_dict['payments'].append(k2_Count)

        else:                 
            ret_dict['sent'].append(
                Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site=site
                ).count() + Africas_Talking_Output.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    bit_harvester__site=site
                ).count()
            )
            ret_dict['received'].append(
                Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site=site
                ).count() + Africas_Talking_Input.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    bit_harvester__site=site
                ).count()
            )                 
            ret_dict['payments'].append(
                Kopo_Kopo_Message.objects.filter(
                    timestamp__gte=next_day,
                    timestamp__lt=next_day + timedelta(days=1),
                    user__site=site
                ).count()
            )                 

    return ret_dict
