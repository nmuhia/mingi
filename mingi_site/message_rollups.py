import datetime

from django.db.models import Count

from africas_talking.models import Africas_Talking_Input, \
    Africas_Talking_Output
from mingi_site.models import Mingi_Site, Daily_Message_Count


def create_message_rollups():
    """
    Creates all missing message rollups for all sites, up until one week ago.
    """

    # If there are rollups, start from the date of the most recent one.
    # Otherwise, start from 2014-01-01.
    daily_message_counts = Daily_Message_Count.objects.all().order_by('-date')
    if daily_message_counts.count():
        start_date = daily_message_counts[0].date
    else:
        start_date = datetime.date(2014, 1, 1)

    # Create rollups up until a week ago
    end_date = (datetime.date.today() - datetime.timedelta(days=7))

    this_date = start_date
    while this_date < end_date:
        create_message_rollups_day(this_date)
        this_date += datetime.timedelta(days=1)


def create_message_rollups_day(date):
    """
    Creates message rollups for all sites for the given day.
    """
    # Get all AT input and output messages on the given day (will be further
    # filtered by site later)
    at_in_queryset = Africas_Talking_Input.objects.filter(
        timestamp__year=date.year,
        timestamp__month=date.month,
        timestamp__day=date.day
    )
    at_out_queryset = Africas_Talking_Output.objects.filter(
        timestamp__year=date.year,
        timestamp__month=date.month,
        timestamp__day=date.day
    )

    # Create a Daily_Message_Count for each site by further filtering and
    # counting the AT in/out querysets
    for site in Mingi_Site.objects.all():
        customers_in = at_in_queryset.filter(user__site=site).count()
        customers_out = at_out_queryset.filter(user__site=site).count()
        bh_in = at_in_queryset.filter(bit_harvester__site=site).count()
        bh_out = at_out_queryset.filter(bit_harvester__site=site).count()
        Daily_Message_Count.objects.create(
            site=site,
            date=date,
            customers_out=customers_out,
            customers_in=customers_in,
            bh_out=bh_out,
            bh_in=bh_in
        )