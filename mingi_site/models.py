from django.utils import timezone
import datetime
from datetime import date, timedelta, datetime
import json

from django.conf import settings

from mingi_django.archiving import retrieve_archive_init


from kopo_kopo.models import Kopo_Kopo_Message
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now


from datetime import timedelta
import pytz

from mingi_django.models import Error_Log, Utility
from mingi_django.mingi_settings.custom import *

import arrow
from mingi_django.mingi_settings.base import TIME_ZONE

# spell out the currency name e.g. "Kenyan Shillings"???
CURRENCY_CHOICES = (
    ('KSH','KSh'),
    ('TSH','TSh'),
    ('USD','USD'),
    ('NRS','NRs'),
    ('XOF','CFA Fr'),
    )

VERSION_CHOICES = (
    ('V3','Version 3.0'),
    ('V4','Version 4.0'),
    ('LITE','BH Lite'),
    ('SOLO','BH SOLO'),
    )

TYPE_CHOICES = SMS_GATEWAY

TIMEZONES = list()
for pyz in pytz.all_timezones:
    TIMEZONES.append((pyz, pyz))


class Mingi_Site(models.Model):
    """
    Represents an energy installation.
    """

    name = models.CharField(max_length=100, unique=True)

    num_lines = models.IntegerField(verbose_name="Number of Lines")

    equipment = models.CharField(max_length=250, blank=True)

    date_online = models.DateField(verbose_name="Date Online")

    latitude = models.FloatField(default=-1.283249)
    longitude = models.FloatField(default=36.816663)

    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, 
                                default="KSH")

    # IF there are modbus lines connected, how many in total?
    num_modbus_lines = models.IntegerField(blank=True, null=True,
                                           verbose_name="Number of Modbus (@) Lines")

    revenue_earned_total = models.DecimalField(max_digits=20, decimal_places=2,
                                          default=0.00)
    energy_sold_total = models.FloatField(default=0.00)
    
    voltage_range = models.CharField(max_length=10, blank=True, null=True)
    generator_is_on = models.NullBooleanField(blank=True, null=True)

    company_name = models.CharField(max_length=50, default="access:energy")
    operator_name = models.CharField(max_length=50, default="access:energy")

    # timezone in which the site is located
    timezone = models.CharField(max_length=20, choices=TIMEZONES, default="UTC", blank=True)

    # pricing information for special deals on buying energy
    bundle_settings = models.CharField(max_length=200, verbose_name="Bundle Settings", blank=True, null=True)
    use_categories = models.CharField(max_length=250, verbose_name="Energy Use Categories", blank=True)

    # last attempted meter log, used for error-checking
    last_attempted_log = models.CharField(max_length=1000, verbose_name="Last Attempted Meter Log", blank=True, null=True)

    # owners can view the site and its associated users/bitHarvesters/transactions
    owners = models.ManyToManyField(User, blank=True)

    # To be used later
    hub_line_number = models.IntegerField(blank=True,null=True,
                                           verbose_name="Power Hub Line Number")

    source_line_number = models.CharField(max_length=10, blank=True,
                                          verbose_name="External Power Source: Line Number(s)")
    is_active = models.BooleanField(default=True)

    # unit-to-pulse ratio - e.g. 5 L water / pulse --> pulse_ratio = 5
    pulse_ratio = models.FloatField(default=1.0)
    monthly_rent = models.DecimalField(max_digits=8, decimal_places=2, default=0.00)
    agent_fees = models.DecimalField(max_digits=8, decimal_places=2, default=0.00)
    site_manager_fees = models.DecimalField(max_digits=8, decimal_places=2, default=0.00)
    tariff_terms = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)

    voltage_scale = models.IntegerField(blank=True, null=True, default=1)
    # solar,wind,generator,inverter,OTHER (CSV)
    capacities = models.CharField(max_length=100, blank=True)
    capital = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)

    is_combined = models.BooleanField(default=False, verbose_name="Is Combined")
    sites = models.ManyToManyField('Mingi_Site', blank=True, null=True)

    labels = models.CharField(max_length=1000, blank=True,verbose_name="Identification/Trace Labels")

    # Whether customers of this site receive payment confirmation notices
    # First added to support STS-194
    receives_payment_confirmations = models.BooleanField(default=True)

    @property
    def equipment_list(self):
        return self.equipment.split(',')

    @property
    def source_line_array(self):
        return self.source_line_number.split(',')

    @property
    def solar_capacity(self):
        capacities = self.capacities.split(',')
        
        if len(capacities) >= 1 and capacities[0]:
            return float(capacities[0])
        else:
            return 0.0

    @property
    def line_is_on(self):

        if int(self.line_status) < 4:
            return True
        else:
            return False

    @property
    def last_int_line(self):
        """
        Used in software-to-BH line mapping; what is the integer value
        of the last line which maps directly to the BH-recognised integer
        """

        if self.num_modbus_lines:
            return self.num_lines - self.num_modbus_lines - 4
        else:
            return self.num_lines - 4

    @property
    def last_think_line(self):
        """
        Used in software-to-BH line mapping; what is the integer value
        of the last THINK line, called line "D" by the BH
        """

        if self.num_modbus_lines:
            return self.num_lines - self.num_modbus_lines
        else:
            return self.num_lines

    @property
    def wind_capacity(self):
        capacities = self.capacities.split(',')
        
        if len(capacities) >= 2 and capacities[1]:
            return float(capacities[1])
        else:
            return 0.0

    @property
    def generator_capacity(self):
        capacities = self.capacities.split(',')
        
        if len(capacities) >= 3 and capacities[2]:
            return float(capacities[2])
        else:
            return 0.0

    @property
    def inverter_capacity(self):
        capacities = self.capacities.split(',')
        
        if len(capacities) >= 4 and capacities[3]:
            return float(capacities[3])
        else:
            return 0.0

    @property
    def revenue_to_date(self):
        m_now = timezone.make_aware(datetime.now(),timezone=pytz.timezone('utc'))
        end_time = m_now.replace(minute=0, second=0, microsecond=0)
        start_time = end_time.replace(year=self.date_online.year,
                                      month=self.date_online.month,
                                      day=self.date_online.day)
        start_time -= timedelta(days=1)
        return self.revenue_range(start_time, end_time)

    # *** IN PROGRESS ***
    @property
    def use_category_dict(self):
        categories = self.use_categories.split(",")
        return_list = list()
        
        for row in categories:
            items = row.split(":")
            this_category = {
                'name': items[0],
                'use_floor':items[1],
                'price': items[2],
                }

            return_list.append(this_category)
            
        return return_list

    def _get_hourly_revenue(self, timestamp):
        """
        Gets the revenue from this site in the clock hour containing the
        timestamp.

        Hidden; use revenue_range instead.
        """
        # all rollups are created with default time_zone in base.py
        rollups_this_timestamp = arrow.get(timestamp).to(TIME_ZONE).datetime

        this_timestamp = timestamp.replace(minute=0,
                                           second=0,
                                           microsecond=0)
        try:
            revenue_obj = Hourly_Site_Revenue.objects.get(
                site=self,
                timestamp=this_timestamp
            )
            return revenue_obj.amount
        except Hourly_Site_Revenue.DoesNotExist:
            end_timestamp = this_timestamp + timedelta(hours=1)
            payments = Kopo_Kopo_Message.objects.filter(
                timestamp__gte=this_timestamp,
                timestamp__lt=end_timestamp,
                user__site=self
            )
            hour_total = 0.0
            for payment in payments:
                hour_total += float(payment.amount)
            # Only create a rollup if the hour is completely in the past.
            # this could be the problem, pytz conversion lag by 33 minutes, and are not accurate
            # m_now = timezone.make_aware(datetime.now(),timezone=pytz.timezone('utc')) 
            m_now = arrow.get(datetime.now()).to(TIME_ZONE).datetime

            eat_timezone = timezone.now()
            if end_timestamp < m_now:
                try:
                    new_hourly = Hourly_Site_Revenue.objects.get(site=self,
                                                     timestamp=rollups_this_timestamp)
                    # new_hourly.save()
                except Hourly_Site_Revenue.MultipleObjectsReturned:
                    Error_Log.objects.create(problem_type="DEBUGGING",problem_message="Duplicate hourly site revenues: site"+str(self.name)+" time: "+str(rollups_this_timestamp))

                except Hourly_Site_Revenue.DoesNotExist:
                    new_hourly = Hourly_Site_Revenue(site=self,
                                                     timestamp=rollups_this_timestamp,
                                                     amount=hour_total)
                    new_hourly.save()

                
                print "created new Revenue Rollup: %s %s - %s" % (self.name,str(this_timestamp),str(hour_total))
            else:
                Error_Log.objects.create(problem_type="DEBUGGING",problem_message="Greater than time:- Site:"+str(self)+" Hour: "+end_timestamp)

            return hour_total

    def get_site_energy_total(self,start_date,end_date,utility=None):
        # Get energy usage between dates with daily user rollups

        # Get total enegy from daily rollups
        total_energy=0.0 # Store total energy
        previous_valid_energy=0.0 # Store last previous stored energy for a given site, for faulty correction
        faulty_energy_list = list()

        rollups=Daily_Power.objects.filter (site__isnull=False, site=self, timestamp__gte=start_date,timestamp__lt=end_date).order_by('timestamp')
        if utility is not None:
            rollups = rollups.filter(utility__name=str(utility))

        while start_date <= end_date :

            rollups2 = rollups.filter(timestamp=start_date)

            for rollup in rollups2:
                energy = float(rollup.amount)

                # Check for faults in data
                if energy < ENERGY_MIN_FAULTY_LIMIT or energy > SITE_ENERGY_MAX_FAULTY_LIMIT :
                    energy=previous_valid_energy
                    faulty_energy_list.append(str(start_date)+" on site: "+str(self)+str("<br/>"))

                total_energy+=energy
            start_date += timedelta(days=1)


        return [total_energy,faulty_energy_list]

    def get_user_energy_total(self,start_date,end_date):
        # Get energy usage between dates with daily user user rollups

        # Get total enegy from daily rollups
        total_energy=0.0 # Store total energy
        previous_valid_energy=0.0 # Store last previous stored energy for a given site, for faulty correction
        faulty_energy_list = list()

        # Get all rollups in single database call
        rollups=Daily_Power.objects.filter (site__isnull=True, user__site=self, timestamp__gte=start_date,timestamp__lt=end_date).order_by('timestamp')
        while start_date <= end_date :
            rollups2 = rollups.filter(timestamp=start_date)
            for rollup in rollups2:
                energy = float(rollup.amount)

                # Check for faults in data
                if energy < ENERGY_MIN_FAULTY_LIMIT or energy > USER_ENERGY_MAX_FAULTY_LIMIT :
                    energy=previous_valid_energy
                    faulty_energy_list.append(str(start_date)+" on site: "+str(self)+str("<br/>"))

                previous_valid_energy=energy
                total_energy+=energy
            start_date += timedelta(days=1)

        return [total_energy,faulty_energy_list]

    def revenue_range(self, start, end):        
        """Gets the revenue from this site in the given time range (both times are
        truncated to the hour in order to facilitate rollups).

        This method cuts down on database calls by grabbing all rollup entries
        at once and calculating any missing entries."""
        
        total_revenue = 0.0

        if (start is None):
            rollups = Hourly_Site_Revenue.objects.filter(
                site=self
            ).order_by('timestamp')
            for rollup in rollups:
                total_revenue+=float(rollup.amount)

        else:
            start_hour = start.replace(minute=0,
                                   second=0,
                                   microsecond=0)
            end_hour = end.replace(minute=0,
                               second=0,
                               microsecond=0)

            m_now = timezone.make_aware(datetime.now(),timezone=pytz.timezone('utc'))
            now_timestamp = m_now
            if end_hour > now_timestamp:
                end_hour = now_timestamp.replace(minute=0,
                                             second=0,
                                             microsecond=0)            

            # Get all rollups in range
            rollups = Hourly_Site_Revenue.objects.filter(
                timestamp__gte=start_hour,
                timestamp__lt=end_hour,
                site=self
            ).order_by('timestamp')
            

            # Step through rollups and range, creating any missing rollups
        
            this_hour = start_hour
            rollup_count = len(rollups) # Not rollups.count() because we want to
                                    # iterate through the actual query set

            rollup_counter = 0
            while this_hour < end_hour:                
                if rollup_counter >= rollup_count:
                    total_revenue += float(self._get_hourly_revenue(this_hour))
                    
                else:
                    this_rollup = rollups[rollup_counter]
                    if this_rollup.timestamp == this_hour:
                        total_revenue += float(this_rollup.amount)
                        rollup_counter += 1
                    elif this_rollup.timestamp > this_hour:
                        total_revenue += float(self._get_hourly_revenue(this_hour))

                this_hour += timedelta(hours=1)

        return total_revenue
     

    @property
    def energy_to_date(self):
        total_energy = 0.0
        for line_num in range(1, self.num_lines + 1):
            try:
                latest_state = Line_State.objects.filter(
                    site=self,
                    number=line_num,
                    meter_reading__gte=0.0).order_by('-timestamp')[0]
                total_energy += latest_state.meter_reading
            except IndexError:
                pass
        return total_energy

    def _get_daily_energy(self, timestamp,start, end):
        """Gets daily energy produced by this site daily
           Hidden; use daily_energy_range instead"""
                
        try:
            daily_energy_obj = Daily_Power.objects.get(
                site=self,
                timestamp=timestamp
            )
            return daily_energy_obj.amount

        except Daily_Power.DoesNotExist:
            from mingi_django.tasks import create_site_daily_power_rollups
            create_site_daily_power_rollups.delay(timestamp, start, end)
            return 0.0            

    def _get_hourly_energy(self, timestamp, lines_rollup=None):
        """
        Gets the energy produced by this site in the clock hour containing the
        timestamp.

        Hidden; use energy_range instead.
        """

        this_timestamp = timestamp.replace(minute=0,
                                           second=0,
                                           microsecond=0)

        # set up lines_rollup to store totals
        for row in lines_rollup:
            row['total'] = 0.0

        try:
            energy_obj = Hourly_Site_Power.objects.get(
                site=self,
                timestamp=this_timestamp
            )
            return energy_obj.amount
        except Hourly_Site_Power.DoesNotExist:

            # Sum energy across all lines, remembering if any lines are missing
            hour_total = 0.0
            all_lines_present = True

            # get all relevant line states with one db call and distribute into a fixed list
            line_state_list = [[] for line in range(0, self.num_lines)]

            try:
                start_filter_ts = Hourly_Site_Power.objects.filter(site=self,timestamp__lte=this_timestamp,amount__isnull=False).order_by('-timestamp')[0].timestamp - timedelta(days=5)
                line_states = Line_State.objects.filter(timestamp__gte=start_filter_ts,site=self,meter_reading__isnull=False).order_by('-timestamp')
            
            except IndexError:
                line_states = Line_State.objects.filter(site=self).order_by('-timestamp')

            # divide line states into sub-lists
            for item in line_states:
                line_num = item.number
                index = line_num - 1
                
                try:
                    line_state_list[index].append(item)
                except IndexError:
                    print "inconsistent number of lines at this site"

            # pass relevant line states to line-specific calculation function
            for index in range(0, self.num_lines):      

                line_no = index + 1

                # don't count power source contributions
                if line_no in self.source_line_array:
                    line_total = 0.00
                    
                else:
                    line_total = self._get_hourly_energy_line(this_timestamp,
                                                              line_state_list[index])
                if line_total is None:
                    all_lines_present = False

                else:
                    # add line total to appropriate utility total
                    assigned = False # make sure every line is matched
                    for row in lines_rollup:
                        if str(line_no) in row['lines']:
                            row['total'] += line_total
                            assigned = True

                    if not assigned:
                        Error_Log.objects.create(problem_type="INTERNAL",
                                                 problem_message="Line %d at site %s could not be located in utility lists" % (line_no, self.name))

            # create rollup objects
            if (all_lines_present):
                for row in lines_rollup:
                    if len(row['lines']) > 0:
                        utility = Utility.objects.get(name=row['name'])
                        new_hourly = Hourly_Site_Power(site=self,
                                                       timestamp=this_timestamp,
                                                       amount=row['total'],
                                                       utility=utility)
                        new_hourly.save()
                        print "created new Power Rollup: %s %s - %s (%s)" % (self.name,str(this_timestamp),str(hour_total),utility.name)

            else:
                print "couldn't complete line states for timestamp: %s" % (str(this_timestamp))
            return hour_total

    def _get_hourly_energy_line(self, timestamp, line_states):
        """
        Returns the energy produced by the given line of this site in the clock
        hour containing the timestamp. If the energy cannot be determined due to
        insufficient line state objects, returns None.
        """
        # The start of the hour in question
        this_timestamp = timestamp.replace(minute=0,
                                           second=0,
                                           microsecond=0)
        # The end of the hour in question
        end_timestamp = this_timestamp + timedelta(hours=1)

        # All line states with meter readings pertaining to the line and site.
        # (because of lazy eval, all such states are not actually retrieved)
        # replaced by list passed into function directly
        # line_states = Line_State.objects.filter(site=self,number=line_num,meter_reading__gte=0.0)

        # track whether all required line states were found
        
        try:
            last_state = line_states[0]
            first_state = line_states[len(line_states) - 1]
        except IndexError:
            # print "index error"
            return None

        state_4_found = False

        # ensure there is a line state occuring after the end of the hour
        if (last_state.timestamp < end_timestamp):
            # print str(last_state.timestamp) + " < " + str(end_timestamp)
            return None

        # ensure there is a line state occurring before the beginning of the hour
        if (first_state.timestamp > this_timestamp):
            # print str(first_state.timestamp) + " > " + str(this_timestamp)
            return None
        
        # get the 4 line state objects which define this hour's energy use
        for state in line_states:
            
            if state.timestamp <= end_timestamp and state_4_found == False:
                line_state_4 = last_state
                line_state_3 = state
                state_4_found = True

            if state.timestamp <= this_timestamp:
                line_state_2 = last_state
                line_state_1 = state
                break
    
            last_state = state

        # The meter value at the end of the hour, using linear interpolation
        try:
            end_energy = (
                line_state_3.meter_reading +
                (
                    (line_state_4.meter_reading - line_state_3.meter_reading) *
                    (
                        (end_timestamp -
                         line_state_3.timestamp).total_seconds() /
                        (line_state_4.timestamp -
                         line_state_3.timestamp).total_seconds()
                    )
                )
            )
        # Catch all errors & log
        except Exception as e:
            end_energy = line_state_3.meter_reading
            Error_Log.objects.create(problem_type="INTERNAL",
                                                 problem_message=str(e))

        # The meter value at the start of the hour, using linear interpolation
        try:
            start_energy = (
                line_state_1.meter_reading +
                (
                    (line_state_2.meter_reading - line_state_1.meter_reading) *
                    (
                        (this_timestamp -
                         line_state_1.timestamp).total_seconds() /
                        (line_state_2.timestamp -
                         line_state_1.timestamp).total_seconds()
                    )
                )
            )
        # Catch all errors & log
        except Exception as e:
            start_energy = line_state_1.meter_reading
            Error_Log.objects.create(problem_type="INTERNAL",
                                                 problem_message=str(e))
        try:
            return float(end_energy) - float(start_energy)
        except:
            return None


    def daily_energy_range(self,start,end,create_rollups=True):
        """
        Gets daily energy for this site in the given time range
        """        
        total_daily_energy = 0.0        
        #get number of days between two timestamps
        num_days = (end - start).days
        
        
        for i in range(num_days):                                            
            start_day = start + timedelta(days=i)
            end_day = start_day + timedelta(hours=24) 
            this_hour = end_day + timedelta(hours=1)
 
            rollups = Daily_Power.objects.filter(site=self, timestamp=this_hour).order_by('timestamp')            
            
            if (len(rollups) == 0 and create_rollups): 
                m_now = timezone.make_aware(now(),timezone=pytz.timezone('utc'))
                if(this_hour < m_now):                    
                    total_daily_energy +=  float(self._get_daily_energy(this_hour,start_day,end_day))
            else:                 
                for rollup in rollups:                                               
                    total_daily_energy += float(rollup.amount)

        return total_daily_energy 

    def site_energy_range(self,start_date,end_date,utility_type=None):
        """
        Get site energy range by utility
        if No utility is defined, get energy range for all 
        """
        if utility_type is None:
            rollups = list(Hourly_Site_Power.objects.filter(site=self,timestamp__gte=start_date,timestamp__lte=end_date))
        else:
            rollups = list(Hourly_Site_Power.objects.filter(site=self,timestamp__gte=start_date,timestamp__lte=end_date,utility=utility_type))

        total = 0.0
        for rollup in rollups:
            if utility_type is None:
                total += float(rollup.amount)
            elif utility_type.name=='Energy:':
                total +=  float(rollup.amount)
            elif utility_type.name=='Water:':
                total +=  float(rollup.second_amount)
            elif utility_type.name=='Fuel:':
                total +=  float(rollup.third_amount)
                
        return total


        
    def energy_range(self, start, end, lines_rollup,create_rollups=True,utility=None):
        """
        Gets the energy from this site in the given time range (both times are
        truncated to the hour in order to facilitate rollups).

        This method cuts down on database calls by grabbing all rollup entries
        at once and calculating any missing entries.

        If create_rollups is true, this method will create any missing rollup
        entries, a potentially computationally-demanding operation.
        """

        print "ENERGY RANGE"

        total_use = 0.0

        if (start is None):            
            rollups = Hourly_Site_Power.objects.filter(
                site=self
            ).order_by('timestamp')            

        else:       
            start_hour = start.replace(minute=0,
                                   second=0,
                                   microsecond=0)
            end_hour = end.replace(minute=0,
                               second=0,
                               microsecond=0)
            m_now = timezone.make_aware(datetime.now(),timezone=pytz.timezone('utc'))
            now_timestamp = m_now
            if end_hour > now_timestamp:
                end_hour = now_timestamp.replace(minute=0,
                                             second=0,
                                             microsecond=0)

            # Get all rollups in range
            rollups = Hourly_Site_Power.objects.filter(
                timestamp__gte=start_hour,
                timestamp__lt=end_hour,
                site=self
            ).order_by('timestamp')

            if utility is not None:
                rollups = rollups.filter(utility__name=str(utility))
        

            # Step through rollups and range, creating any missing rollups
            # *** Make resilient to gaps for a single utility ***
            this_hour = start_hour
            rollup_count = len(rollups) # Not rollups.count() because we want to
                                    # iterate through the actual query set
            rollup_counter = 0
            while this_hour < end_hour:
                if rollup_counter >= rollup_count:
                    if create_rollups:
                        print str(this_hour)
                        total_use += float(self._get_hourly_energy(this_hour,lines_rollup))
                        
                else:
                    this_rollup = rollups[rollup_counter]
                    if this_rollup.timestamp == this_hour:
                        total_use += float(this_rollup.amount)
                        rollup_counter += 1
                    elif this_rollup.timestamp > this_hour:
                        if create_rollups:
                            total_use += float(self._get_hourly_energy(this_hour,lines_rollup))
                            
                this_hour += timedelta(hours=1)
        return total_use


    def __unicode__(self):
        return "%s: (%6.2f, %6.2f)" % (self.name, self.latitude,
                                       self.longitude)

    class Meta:
        verbose_name = "Mingi Site"


class Bit_Harvester(models.Model):
    """
    Represents a BitHarvester device
    """
    harvester_id = models.CharField(max_length=100, verbose_name="ID")

    telephone = models.CharField(max_length=20, unique=True)
    send_telephone = models.CharField(max_length=20, blank=True)

    site = models.ForeignKey('Mingi_Site')

    serial_number = models.CharField(max_length=100, 
                                     verbose_name="Serial Number")

    approval_date = models.DateField()

    # which phone network does the BH's sim card use
    bh_type = models.CharField(max_length=30, choices=TYPE_CHOICES,default="AT")

    # gateway used to receive messages from BH / customers
    input_gateway = models.ForeignKey('africas_talking.Gateway',blank=True,null=True, related_name='input_gateway')
    # gateway used to send messages to BH / customers
    output_gateway = models.ForeignKey('africas_talking.Gateway',blank=True,null=True, related_name='output_gateway')

    # backup gateways
    backup_gateway_in = models.ForeignKey('africas_talking.Gateway',blank=True,null=True, related_name='backup_input')
    backup_gateway_out = models.ForeignKey('africas_talking.Gateway',blank=True,null=True, related_name='backup_output')


    bh_type_secondary = models.CharField(max_length=30, choices=TYPE_CHOICES, default="AT") # secondary gateway to be used
    version = models.CharField(max_length=4, choices=VERSION_CHOICES, default='V3')    

    # how often the unit is expected to log (once every X hours)
    logging_hours = models.FloatField(default=999)

    def __unicode__(self):
        return self.site.name + ": " + self.harvester_id + " (" \
               + self.telephone + ")"

    class Meta:
        verbose_name = "BitHarvester"


class Site_Record(models.Model):
    """
    A record of information about a site which is not used
    in everyday automated operations
    """

    site = models.ForeignKey('mingi_site.Mingi_Site')

    key = models.CharField(max_length=35)

    value = models.CharField(max_length=200)

    timestamp = models.DateTimeField(blank=True, null=True)


    def __unicode__(self):
        return str(self.site) + ": " + self.key + ", " + self.value
    
    class Meta:
        verbose_name = "Site Record"
        ordering = ['-timestamp']


class Line_State(models.Model):
    """
    A snapshot of a line's meter reading and account balance 
    to be created every time the state changes
    """

    site = models.ForeignKey('mingi_site.Mingi_Site')
    
    number = models.IntegerField(verbose_name="Line Number")
    
    timestamp = models.DateTimeField(auto_now_add=False)
    processed_timestamp = models.DateTimeField(auto_now_add=False, blank=True,
                                               null=True)

    """
    Meter reading in kWh.
    If meter_reading or account_balance is left blank (null), it means that
    that field was not updated since the last log was made
    """
    meter_reading = models.FloatField(default=None, blank=True, null=True)

    account_balance = models.DecimalField(max_digits=14, decimal_places=2,
                                          default=None, blank=True, null=True)

    user = models.ForeignKey('mingi_user.Mingi_User', blank=True,
                             null=True, verbose_name="Current Line Owner")

    @property
    def archive_format(self):
        """
        Gets the line state in a json format for archiving.
        """

        # Get the log in dictionary form
        self_dict = self.__dict__

        # Clean the log dictonary for stringification
        if self.site:
            self_dict['site_name'] = self.site.name
        else:
            self_dict['site_name'] = None
        if self.account_balance:
            self_dict['account_balance'] = float(self.account_balance)
        else:
            self_dict['account_balance'] = None
        if self.user:
            self_dict['user_name'] = "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            self_dict['user_name'] = None
        if self.timestamp:
            self_dict['timestamp'] = str(self.timestamp.isoformat())
        else:
            self_dict['timestamp'] = None
        if self.processed_timestamp:
            self_dict['processed_timestamp'] = str(self.processed_timestamp.isoformat())
        else:
            self_dict['processed_timestamp'] = None

        del self_dict['_user_cache']
        del self_dict['_site_cache']
        del self_dict['_state']

        # Return the log dictionary as a json string
        return json.dumps(self_dict)

    def __unicode__(self):
        return str(self.site) + ", " + str(self.number)
    
    class Meta:
        verbose_name = "Line State Snapshot"
        ordering = ['-timestamp']


class Line_State_Archive(models.Model):
    """
    Stores the id of the archive of the given day's worth of Line State
    snapshots.

    Note: Because only one archive is created for each day, any Line State
    objects added 90 days after the actual message (ie, a new Line_State
    created with a timestamp from 90 days ago) will remain in the database and
    not be archived.
    """

    # The day over which the snapshots are archived (UTC)
    date = models.DateField()

    # The AWS-generated id of the Glacier archive
    archive_id = models.CharField(max_length=200)

    def retrieve_archive_init(self):
        """
        Begins a retrieval job for this archive.

        Returns the boto.glacier.job.Job object, which can be polled later for
        the archive contents using job.get_output(). The function will produce
        "400" errors if it has not yet completed.
        """
        return retrieve_archive_init(
            settings.LINE_STATE_VAULT_NAME, self.archive_id)

    class Meta:
        verbose_name = "Line State Snapshot Archive"


class Hourly_Site_Revenue(models.Model):
    """
    Records the revenue received from a given site in a given hour. This is used
    to present rollup data later. Hourly rollups (as opposed to daily) are needed
    in order to handle different time zones.
    """

    site = models.ForeignKey('mingi_site.Mingi_Site')

    # The beginning of the recorded hour. Should always be a unit hour.
    timestamp = models.DateTimeField(blank=True, null=True)

    amount = models.DecimalField(max_digits=14, decimal_places=2,
                                 verbose_name="Revenue Amount (KSh)")

    def __unicode__(self):
        return str(self.timestamp) + " " + str(self.site.name) + ": " + str(self.amount)

    class Meta:
        verbose_name = "Hourly Site Revenue"
        verbose_name_plural = "Hourly Site Revenue Records"
        ordering = ['-timestamp']

 
class Hourly_Site_Power(models.Model):
    """
    Records the power produced by  a given site in a given hour. This is used to
    present rollup data later. Hourly rollups (as opposed to daily) are needed
    in order to handle different time zones.
    """

    site = models.ForeignKey('mingi_site.Mingi_Site')

    # The beginning of the recorded hour. Should always be a unit hour.
    timestamp = models.DateTimeField(blank=True, null=True)

    amount = models.DecimalField(max_digits=19, decimal_places=5,
                                 verbose_name="Power Amount (kWh)")

    utility = models.ForeignKey('mingi_django.Utility', blank=True, null=True)
    
    # additional spaces for sites with multiple utility types; suggested units in verbose_name
    
    second_amount = models.DecimalField(max_digits=19, decimal_places=5,
                                        verbose_name="Water Amount (m^3)",
                                        default=0.00)

    third_amount = models.DecimalField(max_digits=19, decimal_places=5,
                                       verbose_name="Fuel Amount (m^3)",
                                       default=0.00)
                                       


    def __unicode__(self):
        return str(self.timestamp) + " " + str(self.site.name) + ": " + str(self.amount)
    
    class Meta:
        verbose_name = "Hourly Site Power"
        verbose_name_plural = "Hourly Site Power Records"
        ordering = ['-timestamp']


class Daily_Power(models.Model):
    """
    record of the total power use of a site or user over the day
    """
    
    # date of the power rollup
    timestamp = models.DateField(blank=True, null=True)

    amount = models.DecimalField(max_digits=19, decimal_places=5,
                                 verbose_name="Power Amount (kWh)")
    
    utility = models.ForeignKey('mingi_django.Utility', blank=True, null=True)

    site = models.ForeignKey('mingi_site.Mingi_Site', blank=True, null=True, default=None)
    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True, default=None)
    line_obj = models.ForeignKey('mingi_site.Line', blank=True, null=True, default=None)

    class Meta:
        verbose_name = "Daily Power"
        verbose_name_plural = "Daily Power Records"
        ordering = ['-timestamp']

        
class Daily_Message_Count(models.Model):
    """
    Records the number of messages sent to and received by the site in the
    given day, split between customers and bitHarvesters.
    """

    site = models.ForeignKey('mingi_site.Mingi_Site')

    # The day over which the messages are counted (UTC)
    date = models.DateField()

    customers_out = models.IntegerField()
    customers_in = models.IntegerField()
    bh_out = models.IntegerField()
    bh_in = models.IntegerField()

    @property
    def total_out(self):
        return self.customers_out + self.bh_out

    @property
    def total_in(self):
        return self.customers_in + self.bh_in

    class Meta:
        ordering = ['-date']
        verbose_name = "Daily Message Count"


class Line(models.Model):
    """
    An individual bitHarvester connection, specifying technical parameters
    and linked to an end-user (customer) as necessary
    """

    ON_SETTLING = '0'
    ON_AUTO = '1'
    ON_NO_POWER_USE = '2'
    ON_MANUAL = '3'
    OFF_AUTO = '4'
    OFF_PRIORITY = '5'
    OFF_MANUAL = '6'
    OFF_LOW_TRIP = '7'
    OFF_HIGH_TRIP = '8'

    LINE_STATUS_CHOICES = (
        (ON_SETTLING, 'On (Settling)'),
        (ON_AUTO, 'On (Auto)'),
        (ON_NO_POWER_USE, 'On (No Power Use)'),
        (ON_MANUAL, 'On (Manual)'),
        (OFF_AUTO, 'Off (Auto)'),
        (OFF_PRIORITY, 'Off (Priority)'),
        (OFF_MANUAL, 'Off (Manual)'),
        (OFF_LOW_TRIP, 'Off (Low Trip)'),
        (OFF_HIGH_TRIP, 'Off (High Trip)'),
    )

    site = models.ForeignKey('mingi_site.Mingi_Site')
    number = models.IntegerField()

    # for Modbus setups, if # of inputs > # of outputs, specify line mappings
    output_number = models.IntegerField(blank=True, null=True)

    # identify the line; esp. for use in buildings e.g. "1st Floor AC Unit"
    name = models.CharField(max_length=50, blank=True)

    # is the line active - i.e. connected to an end-use
    is_connected = models.BooleanField(default=True)

    # if an end-user is connected to that line, store a link; limit of 1
    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True)

    line_status = models.CharField(max_length=20, choices=LINE_STATUS_CHOICES,
                                   default="6")

    # pulses per small unit for the attached meter (e.g. 5 pulses / L --> 5.0)
    pulse_ratio = models.FloatField(blank=True, null=True)

    utility_type = models.ForeignKey('mingi_django.Utility')
    # used for non-user Line / post-paid cost tracking
    utility_price = models.DecimalField(max_digits=14, decimal_places=2,
                                        blank=True, null=True)

    def __unicode__(self):
        
        return self.name + " (" + str(self.user) + ")"

    def reset_name(self):
        """
        reset the line's name (for when user is removed)
        """
        
        self.name = self.site.name + ", Line " + str(self.number)
        self.save()
        return

    def total_daily_line_power(self, today_date):
        """
        Get daily line power
        """
        start_date = today_date
        end_date = start_date + timedelta(days=1)
        day_states = Line_State.objects.filter(site=self.site,number=self.number,processed_timestamp__gte=start_date,processed_timestamp__lte=end_date).order_by('processed_timestamp')
        total = 0.0
        start_reading = 0.0
        end_reading = 0.0
        i = 0
        for state in day_states:
            if i==0:
                start_reading = state.meter_reading
            end_reading = state.meter_reading
            i = i+1

        try:
            total = float(end_reading) - float(start_reading)
        except:
            total += 0.0

            
        return total
