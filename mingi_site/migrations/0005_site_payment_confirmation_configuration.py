# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    """
    Make changes to the Mingi_Site model to allow configuration of whether the
    site's customers receive payment confirmations.

    Added to support STS-194.
    """

    dependencies = [
        ('mingi_site', '0004_auto_20160330_1443'),
    ]

    operations = [
        migrations.AddField(
            model_name='mingi_site',
            name='receives_payment_confirmations',
            field=models.BooleanField(default=True),
        ),
    ]
