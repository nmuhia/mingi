# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0003_increase_id_value'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bit_harvester',
            name='version',
            field=models.CharField(default=b'V3', max_length=4, choices=[(b'V3', b'Version 3.0'), (b'V4', b'Version 4.0'), (b'LITE', b'BH Lite')]),
        ),
    ]
