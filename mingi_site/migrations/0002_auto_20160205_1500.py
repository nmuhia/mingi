# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_django', '0002_auto_20160205_1500'),
        ('mingi_user', '0001_initial'),
        ('mingi_site', '0001_initial'),
        ('africas_talking', '0002_auto_20160205_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='line_state',
            name='user',
            field=models.ForeignKey(verbose_name=b'Current Line Owner', blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='line',
            name='site',
            field=models.ForeignKey(to='mingi_site.Mingi_Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='line',
            name='user',
            field=models.ForeignKey(blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='line',
            name='utility_type',
            field=models.ForeignKey(to='mingi_django.Utility'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hourly_site_revenue',
            name='site',
            field=models.ForeignKey(to='mingi_site.Mingi_Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hourly_site_power',
            name='site',
            field=models.ForeignKey(to='mingi_site.Mingi_Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hourly_site_power',
            name='utility',
            field=models.ForeignKey(blank=True, to='mingi_django.Utility', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='daily_power',
            name='line_obj',
            field=models.ForeignKey(default=None, blank=True, to='mingi_site.Line', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='daily_power',
            name='site',
            field=models.ForeignKey(default=None, blank=True, to='mingi_site.Mingi_Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='daily_power',
            name='user',
            field=models.ForeignKey(default=None, blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='daily_power',
            name='utility',
            field=models.ForeignKey(blank=True, to='mingi_django.Utility', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='daily_message_count',
            name='site',
            field=models.ForeignKey(to='mingi_site.Mingi_Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bit_harvester',
            name='backup_gateway_in',
            field=models.ForeignKey(related_name='backup_input', blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bit_harvester',
            name='backup_gateway_out',
            field=models.ForeignKey(related_name='backup_output', blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bit_harvester',
            name='input_gateway',
            field=models.ForeignKey(related_name='input_gateway', blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bit_harvester',
            name='output_gateway',
            field=models.ForeignKey(related_name='output_gateway', blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bit_harvester',
            name='site',
            field=models.ForeignKey(to='mingi_site.Mingi_Site'),
            preserve_default=True,
        ),
    ]
