# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0005_site_payment_confirmation_configuration'),
    ]

    operations = [
        migrations.AddField(
            model_name='mingi_site',
            name='labels',
            field=models.CharField(max_length=1000, verbose_name=b'Identification/Trace Labels', blank=True),
        ),
    ]
