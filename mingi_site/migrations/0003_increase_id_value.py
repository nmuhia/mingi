# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import migrations


MINGI_SITE_NEW_ID_VALUE = 25839
BIT_HARVESTER_NEW_ID_VALUE = 99305


def increase_mingi_site_id_value(apps, schema_editor):
    """
    Increase the value of the Mingi_Site autoincrementing id field to
    MINGI_SITE_NEW_ID_VALUE
    """
    Mingi_Site = apps.get_model("mingi_site", "Mingi_Site")
    dummy_site = Mingi_Site(
        id=MINGI_SITE_NEW_ID_VALUE,
        num_lines=1,
        date_online=datetime.date.today()
    )
    dummy_site.save()
    dummy_site.delete()


def increase_bit_harvester_id_value(apps, schema_editor):
    """
    Increase the value of the Bit_Harvester autoincrementing id field to
    BIT_HARVESTER_NEW_ID_VALUE
    """
    Bit_Harvester = apps.get_model("mingi_site", "Bit_Harvester")
    Mingi_Site = apps.get_model("mingi_site", "Mingi_Site")
    dummy_site = Mingi_Site(
        num_lines=1,
        date_online=datetime.date.today()
    )
    dummy_site.save()
    dummy_bh = Bit_Harvester(
        id=BIT_HARVESTER_NEW_ID_VALUE,
        site=dummy_site,
        approval_date=datetime.date.today()
    )
    dummy_bh.save()
    dummy_bh.delete()
    dummy_site.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0002_auto_20160205_1500'),
    ]

    operations = [
        migrations.RunPython(increase_mingi_site_id_value),
        migrations.RunPython(increase_bit_harvester_id_value)
    ]
