# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0006_mingi_site_labels'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bit_harvester',
            name='extra_field_1',
        ),
        migrations.RemoveField(
            model_name='bit_harvester',
            name='extra_field_2',
        ),
        migrations.RemoveField(
            model_name='bit_harvester',
            name='extra_field_3',
        ),
        migrations.RemoveField(
            model_name='bit_harvester',
            name='extra_field_4',
        ),
        migrations.RemoveField(
            model_name='bit_harvester',
            name='extra_field_5',
        ),
        migrations.RemoveField(
            model_name='mingi_site',
            name='extra_field_4',
        ),
        migrations.RemoveField(
            model_name='mingi_site',
            name='extra_field_5',
        ),
    ]
