# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_site', '0007_auto_20160610_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bit_harvester',
            name='bh_type',
            field=models.CharField(
                default=b'AT',
                max_length=30,
                choices=[(b'AT', b'Africas Talking'),
                         (b'TW', b'Twilio (Any)'),
                         (b'TW-US', b'Twilio US'),
                         (b'TW-UK', b'Twilio UK'),
                         (b'TW-FR', b'Twilio France'),
                         (b'F1', b'FocusOne'),
                         (b'SC02-+254702688230-SEND', b'SC02-+254702688230-SEND'),
                         (b'SC01-+254702690718-RECEIVE', b'SC01-+254702690718-RECEIVE'),
                         (b'SMS-Gateway-API', b'SMS-Gateway-API')]),
        ),
        migrations.AlterField(
            model_name='bit_harvester',
            name='bh_type_secondary',
            field=models.CharField(
                default=b'AT',
                max_length=30,
                choices=[(b'AT', b'Africas Talking'),
                         (b'TW', b'Twilio (Any)'),
                         (b'TW-US', b'Twilio US'),
                         (b'TW-UK', b'Twilio UK'),
                         (b'TW-FR', b'Twilio France'),
                         (b'F1', b'FocusOne'),
                         (b'SC02-+254702688230-SEND', b'SC02-+254702688230-SEND'),
                         (b'SC01-+254702690718-RECEIVE', b'SC01-+254702690718-RECEIVE'),
                         (b'SMS-Gateway-API', b'SMS-Gateway-API')]),
        ),
        migrations.AlterField(
            model_name='bit_harvester',
            name='version',
            field=models.CharField(
                default=b'V3',
                max_length=4,
                choices=[(b'V3', b'Version 3.0'),
                         (b'V4', b'Version 4.0'),
                         (b'LITE', b'BH Lite'),
                         (b'SOLO', b'BH SOLO')]),
        ),
    ]
