import datetime

from django.conf import settings

from mingi_site.models import Line_State, Line_State_Archive

from mingi_django.archiving import update_archive


def update_line_state_archive():
    """
    Archive all Line State snapshots more than 9 months old.
    """

    # If there are archives, start from the day after the most recent one.
    # Otherwise, start from the oldest Line_State.
    line_state_archives = Line_State_Archive.objects.all().order_by('-date')
    if line_state_archives.count() > 0:
        start_date = line_state_archives[0].date + datetime.timedelta(days=1)
    else:
        line_state_snapshots = Line_State.objects.all().order_by('timestamp')
        if line_state_snapshots.count() > 0:
            start_date = line_state_snapshots[0].timestamp.date()
        else:
            return

    # Archive snapshots until 9 months ago.
    end_date = datetime.date.today() - datetime.timedelta(days=274)

    this_date = start_date
    while this_date < end_date:
        update_line_state_archive_date(this_date)
        this_date += datetime.timedelta(days=1)


def update_line_state_archive_date(date):
    # Get the line states from the given day to archive
    line_state_to_archive = Line_State.objects.filter(
        timestamp__year=date.year,
        timestamp__month=date.month,
        timestamp__day=date.day
    )

    # Don't create an archive if no line states exist from the given day
    if len(line_state_to_archive) == 0:
        return

    # Get the archive string (newline separated json)
    archive_str = "\n".join([line_state.archive_format
                             for line_state in line_state_to_archive])

    # Create the archive for the given day and save its ID
    archive_id = update_archive(
        settings.LINE_STATE_VAULT_NAME,
        date.isoformat(),
        archive_str
    )
    Line_State_Archive.objects.create(date=date, archive_id=archive_id)

    # Delete the snapshots
    # TODO: Uncomment this after testing; UPDATE: deleting activated
    line_state_to_archive.delete()
