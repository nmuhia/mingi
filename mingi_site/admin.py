from django.contrib import admin
from models import Mingi_Site, Bit_Harvester, Site_Record, Line_State, \
    Hourly_Site_Revenue, Hourly_Site_Power, Daily_Power, Daily_Message_Count, \
    Line, Line_State_Archive

class MingiSiteAdmin(admin.ModelAdmin):
    filter_horizontal = ('owners','sites',)
    list_display = ('name', 'latitude', 'longitude', 'num_lines', 'equipment',
                    'date_online','company_name')

class BitHarvesterAdmin(admin.ModelAdmin):
    list_display = ('harvester_id', 'site', 'telephone', 'serial_number',
                    'approval_date','input_gateway','output_gateway')

class SiteRecordAdmin(admin.ModelAdmin):
    list_display = ('site','key','value')

class LineStateAdmin(admin.ModelAdmin):
    list_display = ('site','number','timestamp',
                    'meter_reading','account_balance','user')

class LineStateArchiveAdmin(admin.ModelAdmin):
    list_display = ('date', 'archive_id')

class HourlySiteRevenueAdmin(admin.ModelAdmin):
    list_display = ('site', 'timestamp', 'amount')
    list_filter = ('site',)

class HourlySitePowerAdmin(admin.ModelAdmin):
    list_display = ('site', 'timestamp', 'amount', 'second_amount', 'third_amount')
    list_filter = ('site',)

class DailyPowerAdmin(admin.ModelAdmin):
    list_display = ('site', 'user', 'timestamp', 'amount','line_obj')
    list_filter = ('site','user')

class DailyMessageCountAdmin(admin.ModelAdmin):
    list_display = ('site', 'date', 'customers_in', 'customers_out', 'bh_in',
                    'bh_out')
    list_filter = ('site',)

class LineAdmin(admin.ModelAdmin):
    list_display = ('site', 'number', 'name', 'user', 'line_status', 'utility_type')
    list_filter = ('site', 'user', 'line_status', 'utility_type')

admin.site.register(Mingi_Site, MingiSiteAdmin)
admin.site.register(Bit_Harvester, BitHarvesterAdmin)
admin.site.register(Site_Record, SiteRecordAdmin)
admin.site.register(Line_State, LineStateAdmin)
admin.site.register(Line_State_Archive, LineStateArchiveAdmin)
admin.site.register(Hourly_Site_Revenue, HourlySiteRevenueAdmin)
admin.site.register(Hourly_Site_Power, HourlySitePowerAdmin)
admin.site.register(Daily_Power, DailyPowerAdmin)
admin.site.register(Daily_Message_Count, DailyMessageCountAdmin)
admin.site.register(Line, LineAdmin)
