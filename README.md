Mingi: Django Edition
=====

The Mingi server handles communication with access-energy users and hardware.

#### To Run Local Server

python manage.py runserver

#### Environment Setup

pip install -r requirements.txt

Apps
=====

mingi_django
-----

The main app for the project. Contains the urlconf and settings files, as well
as views and forms relevant to the front-end data display and manipulation
elements of the site.

mingi_site
-----

Contains models and functions pertinent to Mingi sites and BitHarvesters.

mingi_user
-----

Contains the definition of and functions pertinent to the Mingi User object, 
which encompasses both eletricity users and site managers.

africas_talking
-----

Contains models to store Africa's Talking SMS messages (both sent and received),
as well as the handler view for post requests from Africa's Talking.  This is
the primary means of communicating with users, site managers, and BitHarvesters.

kopo_kopo
-----

Contians models to store Kopo Kopo payments messages, as well as the handler
view for post requests from Kopo Kopo.  This is the primary means of processing
energy payments.