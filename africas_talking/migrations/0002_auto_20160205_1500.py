# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mingi_user', '0001_initial'),
        ('mingi_site', '0001_initial'),
        ('africas_talking', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='africas_talking_output',
            name='bit_harvester',
            field=models.ForeignKey(blank=True, to='mingi_site.Bit_Harvester', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='africas_talking_output',
            name='gateway',
            field=models.ForeignKey(blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='africas_talking_output',
            name='user',
            field=models.ForeignKey(blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='africas_talking_input',
            name='bit_harvester',
            field=models.ForeignKey(blank=True, to='mingi_site.Bit_Harvester', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='africas_talking_input',
            name='gateway',
            field=models.ForeignKey(blank=True, to='africas_talking.Gateway', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='africas_talking_input',
            name='user',
            field=models.ForeignKey(blank=True, to='mingi_user.Mingi_User', null=True),
            preserve_default=True,
        ),
    ]
