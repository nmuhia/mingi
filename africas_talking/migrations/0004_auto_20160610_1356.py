# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('africas_talking', '0003_auto_20160610_1339'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='message_argument_2',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='message_argument_3',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='message_argument_4',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='message_argument_5',
        ),
    ]
