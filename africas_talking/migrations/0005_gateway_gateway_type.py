# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('africas_talking', '0004_auto_20160610_1356'),
    ]

    operations = [
        migrations.AddField(
            model_name='gateway',
            name='gateway_type',
            field=models.CharField(
                default='AT',
                max_length=30,
                choices=[
                    (b'AT', b'Africas Talking'),
                    (b'TW', b'Twilio (Any)'),
                    (b'TW-US', b'Twilio US'),
                    (b'TW-UK', b'Twilio UK'),
                    (b'TW-FR', b'Twilio France'),
                    (b'F1', b'FocusOne'),
                    (b'SC02-+254702688230-SEND', b'SC02-+254702688230-SEND'),
                    (b'SC01-+254702690718-RECEIVE',
                     b'SC01-+254702690718-RECEIVE'),
                    (b'SMS-Gateway-API', b'SMS-Gateway-API'),
                    (b'MOCK', b'Mock Gateway')]),
            preserve_default=False,
        ),
    ]
