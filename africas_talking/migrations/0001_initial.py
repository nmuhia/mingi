# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Africas_Talking_Input',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_message', models.CharField(max_length=1000, verbose_name=b'Raw Message')),
                ('timestamp', models.DateTimeField()),
                ('message_type', models.CharField(max_length=10, verbose_name=b'Message Type')),
                ('third_party_timestamp', models.DateTimeField(null=True, verbose_name=b'Third-Party Service Timestamp', blank=True)),
                ('message_argument_2', models.CharField(max_length=100, verbose_name=b'Message Argument 2', blank=True)),
                ('message_argument_3', models.CharField(max_length=100, verbose_name=b'Message Argument 3', blank=True)),
                ('message_argument_4', models.CharField(max_length=100, verbose_name=b'Message Argument 4', blank=True)),
                ('message_argument_5', models.CharField(max_length=100, verbose_name=b'Message Argument 5', blank=True)),
                ('extra_field_1', models.CharField(max_length=100, blank=True)),
                ('extra_field_2', models.CharField(max_length=100, blank=True)),
                ('extra_field_3', models.CharField(max_length=100, blank=True)),
                ('extra_field_4', models.CharField(max_length=100, blank=True)),
                ('extra_field_5', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'ordering': ['-timestamp'],
                'verbose_name': "Africa's Talking Input",
            },
        ),
        migrations.CreateModel(
            name='Africas_Talking_Input_Archive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('archive_id', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': "Africa's Talking Input Archive",
            },
        ),
        migrations.CreateModel(
            name='Africas_Talking_Output',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raw_message', models.CharField(max_length=1000, verbose_name=b'Raw Message')),
                ('text_message', models.CharField(max_length=1000, null=True, blank=True)),
                ('telephone', models.CharField(max_length=20, blank=True)),
                ('timestamp', models.DateTimeField()),
                ('send_status', models.CharField(default=b'0', max_length=2, choices=[(b'-1', b'Delivery Failed'), (b'0', b'Delivery Success'), (b'1', b'Message send'), (b'2', b'Message not send'), (b'3', b'Message queued'), (b'4', b'Message not queued')])),
            ],
            options={
                'ordering': ['-timestamp'],
                'verbose_name': "Africa's Talking Output",
            },
        ),
        migrations.CreateModel(
            name='Africas_Talking_Output_Archive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('archive_id', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': "Africa's Talking Output Archive",
            },
        ),
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'AT', unique=True, max_length=30)),
                ('telephone', models.CharField(max_length=20, blank=True)),
                ('device_id', models.CharField(max_length=20, unique=True, null=True, verbose_name=b'Device ID', blank=True)),
                ('username', models.CharField(max_length=20, blank=True)),
                ('password', models.CharField(max_length=10, verbose_name=b'Gateway Password or Secret Code', blank=True)),
                ('sms_type', models.CharField(default=b'sms', max_length=10, verbose_name=b'Format to Send Message', choices=[(b'sms', b'SEND AS SMS'), (b'email', b'SEND AS EMAIL')])),
                ('send_url', models.CharField(max_length=300, verbose_name=b'Sneaker Cloud Send Url', blank=True)),
                ('receive_url', models.CharField(max_length=300, verbose_name=b'Steama Endpoint Url', blank=True)),
            ],
            options={
                'ordering': ['-name'],
                'verbose_name': 'Gateway',
            },
        ),
    ]
