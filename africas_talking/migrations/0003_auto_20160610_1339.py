# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('africas_talking', '0002_auto_20160205_1500'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='extra_field_1',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='extra_field_2',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='extra_field_3',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='extra_field_4',
        ),
        migrations.RemoveField(
            model_name='africas_talking_input',
            name='extra_field_5',
        ),
    ]
