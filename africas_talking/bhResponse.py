from decimal import Decimal
from mingi_site.models import Line_State, Site_Record, Line
from mingi_user.models import Mingi_User
from mingi_django.models import Error_Log, Custom_Timer_Command
from africas_talking.models import Africas_Talking_Output, InvalidAttributeValue
from sendSMS import sendSMS
from userResponse import findUserByLine, handleEnergyUse
from methods import process_trigger_commands

import calendar
from django.db.models import Q
from django.core.mail import EmailMessage

import datetime
import pytz

MSG_LINE_OFFSET = 5
VOLTAGE_BUFFER = [-5.0,-5.0,-5.0,-5.0,-5.0,-5.0,-5.0,-5.0,-5.0,-5.0,2.4,2.4,
                  2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4]
HOUR_BUFFER = 1
MAX_WATTS = 3000

def receiveSystemData(msgElements,BH,now,bh_ts):
    """
    receive a bitHarvetser status update and record it correctly in the
    BH_Data_Message log
    """

    timestamp = msgElements[1]
    keyword = msgElements[3]

    # process voltage/system data
    if keyword == "DATA":
        if BH.version == "V4":
            response = logDataValuesV4(msgElements,BH,bh_ts)
        else:
            response = logDataValues(msgElements,BH,bh_ts)

        if (response == "invalid length"):
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="Invalid length for DATA message from BH: " + str(BH))
        
        elif (response == "repeated message"):
            # ignore for now
            pass

    # process line status data (v3)
    elif keyword == "STATUS":

        processStatusUpdate(msgElements,BH,now,bh_ts)
        
    # process line status data, from a V4 bitHarvester
    elif keyword == "STATUSN":
        
        process_v4_status_update(msgElements,BH,now,bh_ts)

    # process meter-reading data
    elif keyword == "METERS":
        response = processMeterReadings(msgElements,BH,now,bh_ts)

    elif keyword == "PULSES":
        if (BH.site.num_modbus_lines is not None) and (BH.site.num_modbus_lines > 1):
            response = processMeterReadings(msgElements,BH,now,bh_ts)
        else:
            pass # don't process PULSES msgs unless a number of modbus lines are explicitly registered
        
    elif keyword == "CREDIT":
        response = process_credit_update(msgElements,BH,now,bh_ts)

    elif keyword == "POWERON":

        if BH.version == "V4":
            # process line remapping
            old_lines = BH.site.num_lines
            last_index = len(msgElements) - 1
            new_lines = int(msgElements[last_index])

            if new_lines != old_lines:
                pass
                # remap_v4_lines(BH.site,old_lines,new_lines)

        else:
            # otherwise, no handling currently done
            pass
    
    else:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Unexpected keyword : " + keyword + " received from BH: " + str(BH))
                                 

def processStatusUpdate(msgElements,BH,now,bh_ts):
    """
    process a line status (ON/OFF) message from a V3 bitharvester
    """
    
    try:
        statusString = msgElements[4]

        if (len(statusString) != BH.site.num_lines):
            
            #Error_Log.objects.create(problem_type="EXTERNAL",
            #problem_message="Status string from BH: " + str(BH) + " has incorrect length")
            #raise InvalidAttributeValue("Africas_Talking_Input","status string",statusString)
            # ignore for now; known bug creating too many error logs
            pass
            
    except IndexError:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Invalid length for a status message received from BH: " + str(BH))
        return

    except InvalidAttributeValue as e:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message=str(e))
        # return # DEPRECATED (create error log but assume first N are correct)
    
    linesOn = list()
    linesOff = list()
    
    # iterate through each line in status string
    for i in range(0,len(statusString)):
        this_status = statusString[i]
        line = i + 1

        # find V4 equivalent status
        if this_status == '1':
            v4_status = '3'
        elif this_status == '0':
            v4_status = '6'

        # ensure user's line status matches BH-reported line status
        try:
            user = findUserByLine(line,BH.site.name)
            line_command = lineStatusMaintenance(this_status,BH.site,line,user)

            if line_command == "OFF":
                if user.control_type == "AUTOC":
                    linesOff.append(line)

            elif line_command == "ON":
                if user.control_type == "AUTOC":
                    linesOn.append(line)

            # ensure database elements have updated status saved
            updateLineStatus(line,v4_status,BH.site,user)

        except Mingi_User.DoesNotExist:
            
            # switch unregistered lines as needed
            if line <= BH.site.num_lines:
                line_command = lineStatusMaintenance(this_status,BH.site,line)
            else:
                line_command = "NO CHANGE"
                
            if line_command == "ON":
                linesOn.append(line)
            elif line_command == "OFF":
                linesOff.append(line)
            # user doen't exist, set user to None
            user = None

            # ensure database elements have updated status saved
            updateLineStatus(line,v4_status,BH.site,None)

        except Mingi_User.MultipleObjectsReturned:
            # LATER: handle somehow
            pass
         
    # send line switching messages as necessary
    send_line_switching_msgs(BH, linesOn, linesOff)

def get_modbus_start_end_line(msgElements,BH):
    """
    get modbus_start, endlines
    """
    status_string = msgElements[5]
    start_char = msgElements[4]

    ad_end_line = int(BH.site.num_lines) + 4

    no_lines = len(status_string)
    modbus_no = int(start_char[1])

    end_line = ad_end_line + (no_lines * modbus_no)
    start_line = (end_line - no_lines) + 1

    return [start_line,end_line]

def process_v4_status_update(msgElements,BH,now,bh_ts):
    """
    process a line status (ON/OFF) message from a V4 bitharvester
    """
    
    try:
        status_string = msgElements[5]
        start_char = msgElements[4]

        print status_string
        
        # *** LATER: make this work even if A-D status info spans multiple messages ***
        if start_char == "A":
            # process think lines
            end_line = BH.site.num_lines
            start_line = end_line - 3
            line_labels = ["A","B","C","D"]

            # construct equivalent (manual) line_status_string for THINK lines
            line_status_string = ""
            for char in status_string:
                if char == "1":
                    line_status_string += "3"
                elif char == "0":
                    line_status_string += "6"
                else:
                    Error_Log.objects.create(problem_type="EXTERNAL",
                                             problem_message="Invalid character in BH reported status string: %s" % char)
        elif start_char[0] == "@":
            # prcessing modbus - external lines
            # [start_line,end_line] = get_modbus_start_end_line(msgElements,BH)
            # line_status_string = msgElements[5]
            line_status_string = ""
            start_line = None
            end_line = None
            return

        else:
            # process normal lines
            start_line = int(start_char)
            end_line = start_line + len(status_string)
            line_status_string = msgElements[6]
            
        """
        elif start_char[0] == "@":
            end_line = BH.site.num_lines
            start_line = end_line - 3
            line_labels = ["A","B","C","D"]

            # construct equivalent (manual) line_status_string for THINK lines
            line_status_string = ""
            for char in status_string:
                if char == "1":
                    line_status_string += "3"
                elif char == "0":
                    line_status_string += "6"
                else:
                    Error_Log.objects.create(problem_type="EXTERNAL",
                                             problem_message="Invalid character in BH reported status string: %s" % char)
        """
            
    except IndexError:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Invalid length for a status message received from BH: " + str(BH))
        return
    except TypeError:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Invalid format for a status message received from BH: " + str(BH))
        return
        
    # The status update must not have too many lines (this is a weak check, but can catch some errors)
    if start_line is not None and len(status_string) > (BH.site.num_lines - 4 - (start_line - 1)) and len(status_string) != 4 and start_char[0] != "@":
        
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Status string from BH: " + str(BH) + " has incorrect length")

    #update users line statuses
    linesOn = list()
    linesOff = list()
    
    for i in range(0,len(status_string)):
        this_status = status_string[i]
        v4_status = line_status_string[i]
        line = i + start_line

        # create an alert if line has been tripped
        if v4_status == "7" or v4_status == "8":
            Error_Log.objects.create(problem_type="WARNING",
                                     problem_message="bitHarvester report shows that line %d has been tripped at site: %s" % (line,BH.site.name),
                                     site=BH.site)
        
        try:
            user = findUserByLine(line,BH.site.name)
            control_type = user.control_type
            updateLineStatus(line,v4_status,BH.site,user)

            # only initiate line-switching if line control is AUTOMATIC/CLOUD
            if control_type == "AUTOC":
                line_command = lineStatusMaintenance(this_status,BH.site,line,user)
            else:
                line_command = "NO CHANGE"

            if line_command == "OFF":
                linesOff.append(line)
            elif line_command == "ON":
                linesOn.append(line)

        except Mingi_User.DoesNotExist:

            updateLineStatus(line,v4_status,BH.site)
            
            # switch unregistered lines as needed
            if line <= BH.site.num_lines:
                line_command = lineStatusMaintenance(this_status,BH.site,line)
            else:
                line_command = "NO CHANGE"
                
            if line_command == "ON":
                linesOn.append(line)
            elif line_command == "OFF":
                linesOff.append(line)

        except Mingi_User.MultipleObjectsReturned:
            # LATER: handle somehow
            pass  

    # send line switching messages as necessary
    send_line_switching_msgs(BH, linesOn, linesOff)
            

def processMeterReadings(msgElements,sender,now,ts_in):
    """
    receive system data from a bitHarvester and process it
    """
    
    site = sender.site
    siteName = site.name
    line_labels = None
    modbus_msg = False

    # check triggers for users at this site (site totals to be calculated at time of daily rollups)
    triggers = Custom_Timer_Command.objects.filter(site__isnull=True, user__site=site, trigger_type="utility_use")

    try:
        start_line = int(msgElements[4])
        num_lines = len(msgElements) - MSG_LINE_OFFSET
     
    except IndexError:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Invalid meter reading syntax from BH: " + str(sender))
        return
    except ValueError:
        start_line = msgElements[4]
        if start_line == "A":
            line_labels = ["A","B","C","D"]
            # A - D account for last 4 lines of a site's numbering
            start_line = site.num_lines - 3
            num_lines = 4
        elif start_line[0] =="@":
            # modbus lines
            modbus_msg = True
            last_think_line = site.last_think_line
            start_line = int(start_line.lstrip("@")) + last_think_line
            #[start_line,end_line] = get_modbus_start_end_line(msgElements,sender)
            num_lines = (len(msgElements) - MSG_LINE_OFFSET)/2

        else:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="V4 message has unexpected starting line (not 'A' or an integer)")
            return

    end_line = start_line + num_lines

    linesOn = list()  # lines that should be turned back on
    linesOff = list() # lines that should be turned back off

    #line_logs  = get_line_logs(siteName,start_line,end_line)

    for line in range(start_line,end_line):
        msgIndex = line - start_line + MSG_LINE_OFFSET
        if modbus_msg:
            # modbus message readings are every other element
            msgIndex = msgIndex + (line - start_line) 

        try:
            lineUser = findUserByLine(line,siteName)
            # uncomment when ready to switch to Line-controlled pulse ratios
            # line_obj = Line.objects.get(site=site,number=line,user=user)

            # if Line has a user, user's pulse ratio will always take priority
            if lineUser.pulse_ratio is not None:
                reading = float(msgElements[msgIndex].strip())/1000 * lineUser.pulse_ratio
            else:
                reading = float(msgElements[msgIndex].strip())/1000 * site.pulse_ratio

            # test that meter reading passes various checks
            pass_check = True

            if lineUser.line_is_on:
                status = "1"
            else:
                status = "0"
            
            try:
                # last update prior to this update's unix timestamp
                """
                previous_log = None
                last_log = None
                
                for previous in line_logs:
                    if previous.user==lineUser and previous.number == line and previous.timestamp.replace(tzinfo=pytz.UTC) < ts_in.replace(tzinfo=pytz.UTC):
                        previous_log = previous
                        break;

                for last in line_logs:
                    if last.user==lineUser and last.number == line:
                        last_log = last
                        break;
  

                # if no record, raise index error
                if previous_log is None or last_log is None:
                    raise IndexError
                    """

                previous_log = Line_State.objects.filter(user=lineUser,
                                                         number=line,
                                                         timestamp__lt=ts_in,
                                                         meter_reading__isnull=False)[0]
                # detect whether this update is non-sequential
                last_log = Line_State.objects.filter(user=lineUser,
                                                     number=line,
                                                     meter_reading__isnull=False)[0]

                last_reading = previous_log.meter_reading
                try:
                    time_delta = (now - previous_log.timestamp).total_seconds() / (3600.0)
                except Exception:
                    time_delta = (pytz.UTC.localize(now) - previous_log.timestamp).total_seconds() / (3600.0)
                power = (reading - last_reading) / time_delta
                if (power > MAX_WATTS):
                    Error_Log.objects.create(problem_type="EXTERNAL",
                                             problem_message="Power Spike detected on line %d, site: %s. No changes were made to this user's account but a line state snapshot has been recorded." % (line,siteName))
                    pass_check = False
            except IndexError:
                last_reading = reading
                last_log = None
                
            NRGused = Decimal(reading) - Decimal(last_reading)
            
            # make note of decreasing meter readings
            if (NRGused < 0):
                    Error_Log.objects.create(problem_type="EXTERNAL",
                                             problem_message="Decreasing meter reading on line %d, site: %s from %.3f to %.3f (%.3f). No changes were made to this user's account, but a line state snapshot has been recorded." % (line,siteName,float(last_reading),float(reading),float(NRGused)))
                    pass_check = False
                                         
            # ignore the log if the BH is in charge of logging use
            if (lineUser.meter_log_party == 'BH') and (pass_check):

                
                # in v4, AUTOMATIC/LOCAL lines are entirely deferred to BH control
                if sender.version == "V4" and lineUser.control_type == "AUTOL":
                    # CREDIT msg logs account balance; here just log meter reading in line state
                    try:
                        ls = Line_State.objects.get(site=lineUser.site,
                                                    number=line,
                                                    user=lineUser,
                                                    timestamp=ts_in)

                        ls.meter_reading = reading
                        # special case for AUTOL lines A-D, assume credit = 0.0
                        if line > site.num_lines - 4 and line <= site.num_lines:
                            ls.account_balance = 0.00
                        ls.save()
                        
                    # if line state does not exist, create with relevant values
                    except Line_State.DoesNotExist:
                        # 
                        if line > site.num_lines - 4 and line <= site.num_lines:
                            balance = 0.0
                        else:
                            balance = None
                        
                        Line_State.objects.create(site=lineUser.site,
                                                  number=line,
                                                  user=lineUser,
                                                  meter_reading=reading,
                                                  account_balance=balance,
                                                  timestamp=ts_in,
                                                  processed_timestamp=now)
                    except Line_State.MultipleObjectsReturned:
                        Error_Log.objects.create(problem_type="INTERNAL",
                                                 problem_message="Duplicate line states detected for line: %d, site: %s, timestamp: %s" % (line, site.name, str(ts_in)))

                    toReturn = "AUTOL v4 line processed successfully"
    
                # if message is non-sequential, create Line_State but do not edit account balances, send line-witching commands, etc; mesage is only processed to fill in energy use data
                elif last_log and last_log.timestamp > previous_log.timestamp:
                    Line_State.objects.create(site=lineUser.site,
                                              number=line,
                                              user=lineUser,
                                              meter_reading=reading,
                                              account_balance=None,
                                              timestamp=ts_in,
                                              processed_timestamp=now)
                    
                else:
                    handleEnergyUse(lineUser,NRGused,line,reading,ts_in,now)
                    toReturn = lineUser.account_balance
                
                # only send switching commands for AUTOMATIC/CLOUD lines (v3 OR v4)
                if lineUser.control_type == "AUTOC":

                    # don't count NRGused if update is non-sequential
                    if last_log and last_log.timestamp > previous_log.timestamp:
                        lineCommand = lineStatusMaintenance(status,site,line,lineUser,0)
                    else:
                        lineCommand = lineStatusMaintenance(status,site,line,lineUser,NRGused)
                else:
                    lineCommand = "NO CHANGE"

                if (lineCommand == "ON"):
                    linesOn.append(line)
                elif(lineCommand == "OFF"):
                    linesOff.append(line)

            else:
                # line_control is not BH, so don't send any commands
                Line_State.objects.create(site=site,number=line,
                                          meter_reading=reading,
                                          timestamp=ts_in,
                                          processed_timestamp=now,
                                          user=lineUser)
                
        except Mingi_User.MultipleObjectsReturned:
            reading = float(msgElements[msgIndex].strip())/1000 * site.pulse_ratio
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Multiple users registered on line %d in %s" % (line,site.name), site=site)
            Line_State.objects.create(site=site,number=line,
                                      meter_reading=reading,
                                      timestamp=ts_in,
                                      processed_timestamp=now)
            
        except Mingi_User.DoesNotExist:
            try:
                current_line = Line.objects.get(number=int(line),site=site)
                
                # if (non-user) Line has pulse ratio, takes priority over site's
                if current_line.pulse_ratio:
                    reading = float(msgElements[msgIndex].strip())/1000 * current_line.pulse_ratio
                else:
                    reading = float(msgElements[msgIndex].strip())/1000 * site.pulse_ratio
                Line_State.objects.create(site=site,number=line,
                                          meter_reading=reading,
                                          timestamp=ts_in,
                                          processed_timestamp=now)
                # uncomment when ready to activate line switching for userless lines (relevant only for account_balance tracking)
                #if current_line.is_connected:
                #lineCommand = lineStatusMaintenance()
                #else:
                #lineCommand = "NO CHANGE"
            except Line.DoesNotExist:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="No Line object found for site: %s, line: %s" % (site.name, line)) 
            
    # adjust the labels for V4 lines A-D
    send_line_switching_msgs(sender, linesOn, linesOff)

# Get previous lines states
def get_line_logs(siteName,start_line,end_line):
    """
    get a list of Line State objects relevant to a given call 
    of processMeterReadings
    """

    starts_line = int(start_line)
    ends_line = int(end_line)

    return list(Line_State.objects.filter(number__gte=starts_line,
                                          number__lte=ends_line,
                                          site__name=siteName,
                                          meter_reading__isnull=False))


def process_credit_update(msgElements,BH,now,timestamp):
    """
    verify BH-reported account balances and correct any discrepancies
    """

    # define variables
    site = BH.site
    corrections = list()
    # no credit updates for lines A-D
    try:
        next_line = int(msgElements[4])
    except TypeError:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Invalid syntax for BH credit report from BH: %s" % str(BH))

    # iterate through line balances
    for i in range(5,len(msgElements)):
        try:
            user = findUserByLine(next_line,site.name)
            line_credit = Decimal(msgElements[i])
            line_balance = line_credit * user.energy_price / 1000

            # if line is AUTOMATIC/CLOUD, correct discrepancies; if line is AUTOMATIC/LOCAL use them for primary account management
            if user.control_type == "AUTOC":
                if user.account_balance != line_balance:
                    corrections.append([next_line,user.account_balance])

            elif user.control_type == "AUTOL":
                if line_balance == user.account_balance:
                    # create Error Log dictating whether account balance is off
                    pass
                user.account_balance = line_balance
                user.save()

                try:
                    ls = Line_State.objects.get(site=user.site,
                                                number=next_line,
                                                user=user,
                                                timestamp=timestamp)
                    ls.account_balance = line_balance
                    ls.save()

                    # check for relevant account balance triggers
                    triggers = Custom_Timer_Command.objects.filter(user=user, trigger_type='account_balance')
                    if len(triggers) > 0:
                        process_trigger_commands(triggers,ls)
                    
                except Line_State.DoesNotExist:
                    ls = Line_State.objects.create(site=user.site,
                                                   number=next_line,
                                                   user=user,
                                                   account_balance=user.account_balance,
                                                   timestamp=timestamp,
                                                   processed_timestamp=now)
                    # check for relevant account balance triggers
                    triggers = Custom_Timer_Command.objects.filter(user=user, trigger_type='account_balance')
                    if len(triggers) > 0:
                        process_trigger_commands(triggers,ls)

                except Line_State.MultipleObjectsReturned:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Duplicate line states detected for line: %d, site: %s, timestamp: %s" % (next_line, site.name, str(timestamp)))
                    
            else:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="Invalid Control Type: %s registered for user: %s" % (user.control_type, str(user)))
            
            
        except Mingi_User.DoesNotExist:
            # *** for now, assume this can be ignored, verify with testing ***
            pass

        except Mingi_User.MultipleObjectsReturned:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Multiple users registered on line %s, site: %s" % (next_line,site.name), site=site)

        next_line += 1


def logDataValues(msgElements,BH,ts_in):
    """
    create Site Record for bitHarvester-reported data values
    """

    site = BH.site

    try:
        voltage = float(msgElements[6]) * site.voltage_scale
        temp = msgElements[7]
        boards = msgElements[8]
        relays = msgElements[9]
          
    except Exception:
        # temp ****
        if BH.version == 'V4':
            return "success"
        else:
            return "invalid length"

    # dont create duplicate logs
    repeat_msgs = Site_Record.objects.filter(site=site,key="BH Data Log",
                                             timestamp=ts_in)

    if (len(repeat_msgs) > 0):
        return "repeated message"
    

    # as long as the message syntax is valid, record the data log
    dataString = str(voltage)+","+temp+","+boards+","+relays
    new_log = Site_Record.objects.create(site=site,key="BH Data Log",
                                         value=dataString, timestamp=ts_in)

    # check for temp-related trigger commands
    if BH.send_telephone:
        phone_no = send_telephone
    else:
        phone_no = BH.telephone
    triggers = Custom_Timer_Command.objects.filter(site=site)
    if len(triggers) > 0:
        process_trigger_commands(triggers, new_log)
        
    return "success"


def logDataValuesV4(msgElements,BH,bh_ts):
    """
    create Site Record for bitHarvester-reported data values in a V4 message
    """

    site = BH.site
    
    # if log has firmware version, it's a Head Log
    if msgElements[4] and msgElements[4] != "":
        
        try:
            boards = msgElements[7]
            relays = msgElements[8]
            firmware_version = msgElements[4] # for the sake of being thorough
            
        except IndexError:
            return "invalid length"
        
        except ValueError:
            return "invalid length"

        # dont create duplicate logs
        repeat_msgs = Site_Record.objects.filter(site=site,key="BH Head Log",
                                                 timestamp=bh_ts)
        if (len(repeat_msgs) > 0):
            return "repeated message"
        
        # as long as the message syntax is valid, record the data log
        dataString = firmware_version+","+boards+","+relays
        new_log = Site_Record.objects.create(site=site,key="BH Head Log",
                                             value=dataString, timestamp=bh_ts)

    else:
        try:
            voltage = float(msgElements[5]) * site.voltage_scale
            temp = msgElements[6]

        except IndexError:
            return "invalid length"
        
        except ValueError:
            return "invalid length"

        # dont create duplicate logs
        repeat_msgs = Site_Record.objects.filter(site=site,key="BH Data Log",
                                                 timestamp=bh_ts)
        if (len(repeat_msgs) > 0):
            return "repeated message"
        
        # as long as the message syntax is valid, record the data log
        dataString = str(voltage)+","+temp
        new_log = Site_Record.objects.create(site=site,key="BH Data Log",
                                   value=dataString, timestamp=bh_ts)

        # check for temp-related trigger commands
        if BH.send_telephone:
            phone_no = send_telephone
        else:
            phone_no = BH.telephone
        triggers = Custom_Timer_Command.objects.filter(site=site)
        if len(triggers) > 0:
            process_trigger_commands(triggers, new_log)

    return "success"


def updateLineStatus(line,status,site,user=None):
    """
    update the line_status of the user AND Line
    (remove user update once Lines are fully integrated)
    """

    # update user line status
    if user is not None:

        # create error log if status doesn't match user's control type
        if user.control_type == "AUTOC":
            if status != '3' and status != '6':
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message="Line %s on site %s is registered with AUTOC control, but a BH-reported status of %s; correction message sent" % (line,site.name,status))
                # correct the bitHarvester
                try:
                    id_string = str(datetime.datetime.now().microsecond)[2:]
                    int_status = int(status)
                    if int_status < 4:
                        command = "ON"
                    else:
                        command = "OFF"
                    sendSMS(user.bit_harvester.telephone,"*%s,%s,%s" % (id_string,command,user.line_number))
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))     
        elif user.control_type == "AUTOL":
            if status == '3' or status == '6':
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message="Line %s on site %s is registered with AUTOL control, but a BH-reported status of %s; correction message sent" % (line,site.name,status))
                # correct the bitHarvester
                try:
                    id_string = str(datetime.datetime.now().microsecond)[2:]
                    command = "AUTO"
                    sendSMS(user.bit_harvester.telephone,"*%s,%s,%s" % (id_string,command,user.line_number))
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                                             problem_message=str(e))     
        else:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="User: %s has unrecognized control type: %s" % (str(user),user.control_type))
                

    # update Line object line status
    try:
        user_line = Line.objects.get(site=site,number=line,user=user)
        user_line.line_status = status
        user_line.save()
    except Line.DoesNotExist:
        #Error_Log.objects.create(problem_type="INTERNAL",
        #                         problem_message="No Line found for line %d at site: %s with user: %s" % (line, site.name, str(user)))
        # ignore for now
        pass
    

def lineStatusMaintenance(status,site,line,user=None,NRGused=None):
        
    # check a line attached to a user
    if user is not None:

        payment_plan_name = user.payment_plan.split(',')[0]
        
        # disable switching for BM lines
        if user.first_name == "Electricity:" or user.first_name == "Fuel:" or user.first_name == "Water:":
            return "NO CHANGE"
        
        # line status is ON
        if (status == '1'):

            # update line object if applicable
            try:
                user_line = Line.objects.get(site=site,number=line)
                user_line.line_status = '3'
                user_line.save()
            except Line.DoesNotExist:
                user_line = None

            # line should be off (message to customer handled in call to handleZeroBalance)
            if (user.account_balance <= 0) and payment_plan_name != "Subscription Plan":
                return "OFF"

        elif (status == '0'):

            # update line object if applicable
            try:
                user_line = Line.objects.get(site=site,number=line)  
                user_line.line_status = '6'
                user_line.save()
            except Line.DoesNotExist:
                user_line = None

            # line should be turned on (error-checking)
            payment_plan = user.payment_plan.split(',')[0]
            if payment_plan != "High Energy User" and payment_plan != "Subscription Plan":
                if (user.account_balance > 0):
                    return "ON"

            elif (user.account_balance <= 0) and payment_plan != "Subscription Plan":
                if (NRGused) and (NRGused > 0):
                    return "OFF"
            
            elif (user.payment_plan.split(',')[0] == "High Energy User"):
                
                try:
                    threshold = Decimal(user.payment_plan.split(',')[1])
                    if (user.account_balance >= threshold):
                        return "ON"

                except IndexError:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Invalid syntax for payment plan details in user: " + user.telephone)
                    if (user.account_balance > 0):
                        return "ON"
                    
                except Decimal.InvalidOperation:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Invalid syntax for payment plan details in user: " + user.telephone)
                    if (user.account_balance > 0):
                        return "ON"

        else:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="Invalid line status character: " + status)
            
    # check for power-stealing on unregistered AND unconnected lines
    elif (status == '1'):
        line_obj = Line.objects.get(number=line, site=site)
        
        # ***once is_connected statuses are synced, remove first two conditions
        if (line != site.source_line_number) and (line != site.hub_line_number) and not line_obj.is_connected:
            Error_Log.objects.create(problem_type="EXTERNAL", 
                                     problem_message="Unregistered line #" + str(line) + " logged as ON on site: " + site.name,
                                     site=site)
            return "OFF"

        else:
            pass # this is where any conditional off msgs could be triggered

    # if none of the above conditions are met, line status is OK as it is
    return "NO CHANGE"


def process_trigger_response(msgElements, sender):
    """
    update databases according to a trigger response from a bitHarvester
    """

    # identify the message which is being responded to
    site = sender.site
    id_num = msgElements[0].strip().lstrip('*')

    try:
        triggerMsg = Africas_Talking_Output.objects.filter(raw_message__contains=id_num, bit_harvester=sender,gateway=sender.output_gateway)[0].raw_message

    except IndexError:
        try:
            triggerMsg = Africas_Talking_Output.objects.filter(text_message__contains=id_num, bit_harvester=sender)[0].text_message
        except IndexError:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="No message found with trigger ID: %s" % id_num)
            return
        
    triggerElements = triggerMsg.split(',')
    command_type = triggerElements[1].upper()
    
    # call appropriate method depending on the command type
    if command_type == "ON" or command_type == "OFF" or command_type == "AUTO":
        logLineStatusChange(triggerElements,sender)

           
def logLineStatusChange(trigger_elements,sender):
    """
    update databases according to a line status update from a bitHarvester
    """
    
    site = sender.site
    version = sender.version
    line_labels = ["A","B","C","D"]
        
    status = trigger_elements[1].upper()
    if (status == "ON"):
        newStatus = '3'
    elif (status == "OFF"):
        newStatus = '6'
    # for v4 AUTO message, assume line status has no immediate change
    elif (status == "AUTO"):
        newStatus = None
    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Incorrect syntax for line control message: %s should be 'ON' or 'OFF'" % status)
        return
        
    # special case for "ALL" commands
    if (trigger_elements[2] == "ALL"):
        
        lines = Line.objects.filter(site=site,
                                    is_connected=True)

        for line in lines:
            # update status to AUTO
            if newStatus is None:
                # Manual On to Auto On
                if line.line_status == '3':
                    line.line_status = '1'
                    line.save()
                # Manual Off to Auto Off
                elif line.line_status == '6':
                    line.line_status = '4'
                    line.save()
                else:
                    # no change
                    pass

            # update status to Manual ON/OFF and send notice
            else:
                line.line_status = newStatus
                line.save()
                    
                if line.user and line.number == line.user.line_array[-1]:
                    sendLineStatusNotice(line.user.telephone,status,user)

    # special case for empty string line number (STS-283)
    elif trigger_elements[2] == '':
        Error_Log.objects.create(
            problem_type="INTERNAL",
            site=site,
            problem_message="A line switch command was sent with no line "
                            "numbers: '%s'" % ','.join(trigger_elements)
        )

    # set all lines from that command
    else:
        for i in range(2,len(trigger_elements)):

            # store current line number
            lineNo = trigger_elements[i]
            lineNo = map_bh_to_line_number(lineNo,sender)
            
            try:
                line = Line.objects.get(site=site,number=lineNo)
                
                # update status to AUTO
                if newStatus is None:
                    # Manual On to Auto On
                    if line.line_status == '3':
                        line.line_status = '1'
                        line.save()
                    # Manual Off to Auto Off
                    elif line.line_status == '6':
                        line.line_status = '4'
                        line.save()
                    else:
                        # no change
                        pass

                # update status to Manual ON/OFF and send notice
                else:
                    line.line_status = newStatus
                    line.save()
                    
                    if line.user and line.number == line.user.line_array[-1]:
                        sendLineStatusNotice(line.user.telephone,status,user)

            except Line.DoesNotExist:
                Error_Log.objects.create(problem_type="EXTERNAL", site=site,
                                         problem_message="A command was sent to switch Line #%d, but this line does not exist" % lineNo)
    

def sendLineStatusNotice(phoneNo,status,user=None,sender=None):
    """
    send an SMS to notify a customer of a status change to their line
    """

    if not user:
        try:
            user = locateUser(phoneNo)
        except Exception:
            return # don't send message if phone number isn't recognized

    if (status == "ON"):
        if user and user.language == 'ENG':
            msgOut = "Your power has been turned on, enjoy!"
        else:
            msgOut = "Umeme yako imefunguliwa, furahia!"
    elif (status == "OFF"):
        if user and user.language == 'ENG':        
            msgOut = "Your line has been turned off! Please top up your account to receive power"
        else:
            msgOut = "Umeme yako imeziwa! Ongeza salio lako ili upate stima"

    try:
        sendSMS(phoneNo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                 problem_message=str(e))        


def remap_v4_lines(site,old_lines,new_lines):
    """
    remap customer line numbers when a V4 CONNECT board is added or removed;
    update site.num_lines
    """
    
    users = Mingi_User.objects.filter(site=site, line_number__isnull=False)
    if old_lines > new_lines:
        ADD_LINES = False
    elif new_lines > old_lines:
        ADD_LINES = True
    else:
        return
    
    for user in users:
        new_line_nos = ""

        for line in user.line_array:

            # check for lines A-D, shift by difference in num_lines
            if line > (old_lines - 4):
                new_line_nos += str(int(line) + (new_lines - old_lines))

            # shift lines up (CONNECT board added)
            elif line > old_lines and line <= new_lines:
                new_line_nos += str(line + 4)

            # shift lines down (CONNECT board removed)
            elif line > new_lines and line < old_lines:
                new_line_nos += str(line - 4)

            # no change
            else:
                new_line_nos += str(line)

            new_line_nos += ","

        user.line_number = new_line_nos[0:-1]
        user.save()
        print "User: %s now has line numbers %s" % (str(user),user.line_number)


    site.num_lines = new_lines
    site.save()
                
    return


def send_line_switching_msgs(sender, linesOn, linesOff):
    """
    send out the relevant on/off messages to sender
    """

    if len(linesOn) > 0:
        numstring = str(calendar.timegm(datetime.datetime.now().utctimetuple()))
        id_num = numstring[6:]
        msg_out_on = "*1%s,ON" % id_num
        for num in linesOn:
            num = int(num)
            adj_num = map_line_number_to_bh(num,sender)
            msg_out_on = msg_out_on + "," + str(adj_num)
            
        try:
            sendSMS(sender.telephone,msg_out_on,sender)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
            
    if len(linesOff) > 0:
        id_num = str(calendar.timegm(datetime.datetime.now().utctimetuple()))[6:]
        msg_out_off = "*1%s,OFF" % id_num
        for num in linesOff:
            num = int(num)
            adj_num = map_line_number_to_bh(num,sender)
            msg_out_off = msg_out_off + "," + str(adj_num)
            
        try:
            sendSMS(sender.telephone,msg_out_off,sender)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
            return "Error: System encountered an error and the message was not sent. Please contact your site administrator for assistance."
            
    return "Message sent successfully!"


def map_line_number_to_bh(num, bh):
    """
    convert an integer line number in the software to BH-recognized format
    """

    line_labels = ["A","B","C","D"]
    site = bh.site
    last_int_line = site.last_int_line
    last_think_line = site.last_think_line
    
    if num > last_int_line and num > last_think_line:
        # convert to modbus number format; check if line output no. is distinct
        line_obj = Line.objects.get(number=num,site=site)
        if line_obj.output_number == 0:
            # this means the line has no output ***handle differently?***
            adj_num = ""
        elif line_obj.output_number > 0:
            print "FOUND AN OUTPUT"
            adj_num = "@" + str(line_obj.output_number - last_think_line)
        else:
            adj_num = "@" + str(num - last_think_line)
    elif (bh.version == "V4") and (num > last_int_line):
        AD_line_id = last_think_line - num
        adj_num = line_labels[3 - AD_line_id]
    else:
        adj_num = num

    return adj_num


def map_bh_to_line_number(num, bh):
    """
    convert an integer line number in the software to BH-recognized format
    """

    line_labels = ["A","B","C","D"]
    site = bh.site
    last_int_line = site.last_int_line
    last_think_line = site.last_think_line
    
    if num[0] == '@':
        # convert from modbus number format
        num = int(num[1:])
        adj_num = num - last_think_line

    elif num in line_labels:
        index = 1
        for item in line_labels:
            if num == item:
                adj_num = last_think_line - len(line_labels) + index
            index += 1
        
    else:
        adj_num = num

    return adj_num
