from django.contrib import admin
from models import Africas_Talking_Input, Africas_Talking_Output, \
    Africas_Talking_Input_Archive, Africas_Talking_Output_Archive,Gateway

class ATInAdmin(admin.ModelAdmin):
    list_display = ('user', 'bit_harvester', 'timestamp', 'raw_message','gateway')

class ATOutAdmin(admin.ModelAdmin):
    list_display = ('user', 'bit_harvester', 'timestamp', 'raw_message','text_message','gateway')

class ATInArchiveAdmin(admin.ModelAdmin):
    list_display = ('date', 'archive_id')

class ATOutArchiveAdmin(admin.ModelAdmin):
    list_display = ('date', 'archive_id')

class GatewayAdmin(admin.ModelAdmin):
    list_display = ('name', 'send_url', 'receive_url', 'telephone')


admin.site.register(Africas_Talking_Input, ATInAdmin)
admin.site.register(Africas_Talking_Output, ATOutAdmin)
admin.site.register(Africas_Talking_Input_Archive, ATInArchiveAdmin)
admin.site.register(Africas_Talking_Output_Archive, ATOutArchiveAdmin)
admin.site.register(Gateway,GatewayAdmin)
