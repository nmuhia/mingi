import datetime

from django.conf import settings

from africas_talking.models import Africas_Talking_Input, Africas_Talking_Output, \
    Africas_Talking_Input_Archive, Africas_Talking_Output_Archive

from mingi_django.archiving import update_archive


def update_message_archive():
    """
    Update all message archives (archive all messages more than 90 days old).
    """
    update_at_in_archive()
    update_at_out_archive()


def update_at_in_archive():
    """
    Archive all Africa's Talking Input messages more than 90 days old.
    """

    # If there are archives, start from the day after the most recent one.
    # Otherwise, start from the oldest Africas_Talking_Input.
    at_in_archives = Africas_Talking_Input_Archive.objects.all().order_by('-date')
    if at_in_archives.count():
        start_date = at_in_archives[0].date + datetime.timedelta(days=1)
    else:
        at_in_messages = Africas_Talking_Input.objects.all().order_by('timestamp')
        if at_in_messages.count():
            start_date = at_in_messages[0].timestamp.date()
        else:
            return

    # Archive messages until 90 days ago.
    end_date = datetime.date.today() - datetime.timedelta(days=90)

    this_date = start_date
    while this_date < end_date:
        update_at_in_archive_date(this_date)
        this_date += datetime.timedelta(days=1)


def update_at_out_archive():
    """
    Archive all Africa's Talking Output messages more than 90 days old.
    """

    # If there are archives, start from the day after the most recent one.
    # Otherwise, start from the oldest Africas_Talking_Output.
    at_out_archives = Africas_Talking_Output_Archive.objects.all().order_by('-date')
    if at_out_archives.count():
        start_date = at_out_archives[0].date + datetime.timedelta(days=1)
    else:
        at_out_messages = Africas_Talking_Output.objects.all().order_by('timestamp')
        if at_out_messages.count():
            start_date = at_out_messages[0].timestamp.date()
        else:
            return

    # Archive messages until 90 days ago.
    end_date = datetime.date.today() - datetime.timedelta(days=90)

    this_date = start_date
    while this_date < end_date:
        update_at_out_archive_date(this_date)
        this_date += datetime.timedelta(days=1)


def update_at_in_archive_date(date):
    # Get the messages from the given day to archive
    at_in_to_archive = Africas_Talking_Input.objects.filter(
        timestamp__year=date.year,
        timestamp__month=date.month,
        timestamp__day=date.day
    )

    # Don't create an archive if no messages exist from the given day
    if len(at_in_to_archive) == 0:
        return

    # Get the archive string (newline separated json)
    archive_str = "\n".join([at_in.archive_format for at_in in at_in_to_archive])

    # Create the archive for the given day and save its ID
    archive_id = update_archive(
        settings.AFRICAS_TALKING_INPUT_VAULT_NAME,
        date.isoformat(),
        archive_str
    )
    Africas_Talking_Input_Archive.objects.create(date=date, archive_id=archive_id)

    # Delete the messages
    # TODO: Uncomment this after testing
    at_in_to_archive.delete()


def update_at_out_archive_date(date):
    # Get the messages from the given day to archive
    at_out_to_archive = Africas_Talking_Output.objects.filter(
        timestamp__year=date.year,
        timestamp__month=date.month,
        timestamp__day=date.day
    )

    # Don't create an archive if no messages exist from the given day
    if len(at_out_to_archive) == 0:
        return

    # Get the archive string (newline separated json)
    archive_str = "\n".join([at_out.archive_format for at_out in at_out_to_archive])

    # Create the archive for the given day and save its ID
    archive_id = update_archive(
        settings.AFRICAS_TALKING_OUTPUT_VAULT_NAME,
        date.isoformat(),
        archive_str
    )
    Africas_Talking_Output_Archive.objects.create(date=date, archive_id=archive_id)

    # Delete the messages
    # TODO: Uncomment this after testing
    at_out_to_archive.delete()
