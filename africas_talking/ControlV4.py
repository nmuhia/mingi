#!/usr/bin/python
#
# (C) BitBox Ltd 2015
#
# $Id: ControlV4.py 1160 2015-09-01 14:38:16Z phorton $
#

import struct
import TextV4

#
# class for building text messages frame by frame
#
class PacketList:

	'build text mesaages frame by frame'

	_TAG_FRAME_SIZE = 3

	#
	# set starting tag value
	#
	def __init__(self, tag):
		'initialise instance'
		self.tag = tag
		self._clear()

	#
	# clear everything except tag value
	#
	def _clear(self):
		'clear all except tag'
		self.sms = [ ]
		self.pkt = ''

	#
	# convert any accumulated packet data (if there is any) to text message
	#
	# we add a tag frame to the end of the packet before conversion
	#
	def _flush(self):
		'flush frames to text message'
		if len(self.pkt):
			ident = self.tag & 0xffff
			if not ident:
				self.tag = self.tag + 1
				ident = self.tag & 0xffff
			tag = struct.pack('<BH', 0x6d, ident)
			self.sms.append(TextV4.sms_pack(self.pkt + tag))
			self.pkt = ''
			self.tag = self.tag + 1

	#
	# add frame data to current packet
	#
	# ensures we leave enough space for tag frame
	#
	def _add_frame(self, octets):
		'add frame to packet'
		reqd = len(octets) + self._TAG_FRAME_SIZE
		if len(self.pkt) + reqd > TextV4.MAX_OUTGOING_SIZE:
				self._flush()
				if len(self.pkt) + reqd > TextV4.MAX_OUTGOING_SIZE:
					raise TypeError('frame too long')
		self.pkt = self.pkt + octets

	#
	# return tag value to be used next
	#
	def tag_next(self):
		'get next tag value'
		return self.tag

	#
	# return text messages and clear everything
	#
	def get_packets(self):
		'get text messages and reset'
		self._flush()
		packets = self.sms
		self._clear()
		return packets

	#
	# add status request control frame
	#
	def add_status_request(self, rtype = 0):
		'add request for status'
		self._add_frame(struct.pack('2B', 0x29, rtype))

	#
	# add time setting control frame
	#
	def add_time(self, time, limit):
		'add time setting frame'
		self._add_frame(struct.pack('<BIH', 0xde, time, limit))

	#
	# add line control frame
	#
	def add_circuit(self, circuit, count, mask, group = None, tariff = None, state = None, credit_inc = None, credit_dec = None, credit_set = None):
		'add line control frame'
		octets = ''
		items = 0
		# add 'group'
		if not group is None:
			octets = octets + struct.pack('2B', 0x49, group)
			items = items + 1
		# add 'tariff'
		if not tariff is None:
			octets = octets + struct.pack('<BI', 0x93, int(tariff * 65536 + 0.5))
			items = items + 1
		# add 'state'
		if not state is None:
			octets = octets + struct.pack('2B', 0xe7, state)
			items = items + 1
		# add 'credit inc', 'credit dec' or 'credit set'
		tag = None
		if not credit_inc is None:
			credit = credit_inc
			tag = 0x30
		elif not credit_dec is None:
			credit = credit_dec
			tag = 0xa9
		elif not credit_set is None:
			credit = credit_set
			tag = 0x36
		if tag:
			octets = octets + struct.pack('<BI', tag, credit)
			items = items + 1
		#
		self._add_frame(struct.pack('<B3HB', 0xb9, circuit, count, mask, items) + octets)

	#
	# add configuration control frame
	#
	def add_config(self, log_interval = None, report_records = None, server_sms_intl = None, server_sms_plan = None):
		'add configuration setting frame'
		octets = ''
		items = 0
		# add 'log interval'
		if not log_interval is None:
			octets = octets + struct.pack('<BH', 0xeb, log_interval)
			items = items + 1
		# add 'report records'
		if not report_records is None:
			octets = octets + struct.pack('<BH', 0x15, report_records)
			items = items + 1
		# add 'server_sms_intl' or 'server_sms_plan'
		tag = None
		if not server_sms_intl is None:
			server = server_sms_intl
			tag = 0x56
		elif not server_sms_plan is None:
			server = server_sms_plan
			tag = 0x3e
		if tag:
			octets = octets + chr(tag)
			# convert number to BCD with 0xf padding
			for index in range(0, 20, 2):
				try:
					value = int((server[index : index + 2] + 'ff')[:2], 16)
				except ValueError:
					value = 0xff;
				octets = octets + chr(value)
			items = items + 1
		#
		self._add_frame(struct.pack('2B', 0x9e, items) + octets)

#
# convert list of circuit numbers to extents
#
# numeric circuit numbers are converted to integers and decremented so that they are zero based
#
# circuit numbers 'A' through 'Z' are converted to 0xff00 through 0xff19
#
# circuit numbers preceded with '@' are converted to 0xc000 (@1) etc
#
def _circuit_extents(cct_list):
	'convert circuit number list to circuit/count extents'
	spans = [ ]
	count = 0
	for value in sorted(cct_list):
		if isinstance(value, str):
			if len(value) == 1 and value.isupper():
				cct = 0xff00 + ord(value) - ord('A')
			elif value[0] == '@':
				cct = 0xc000 + int(value[1:]) - 1
			else:
				cct = int(value) - 1
		else:
			cct = value - 1
		if cct < 0 or cct > 0xffff:
			raise ValueError('circuit number out of range')
		if count:
			diff = cct - start
			# ignore duplicates
			if diff == 0:
				continue
			# count sequentials
			if diff == count:
				count = count + 1
				continue
			spans.append((start, count))
		# start next group
		start = cct
		count = 1
	if count:
		spans.append((start, count))
	return spans

#
# unpack V3 circuit control message addings frame to packet list
#
# returns 'pkt_list' or 'None' on error
#
def _unpack_control(pkt_list, mtype, field):

	'unpack circuit control message to frames'

	try:
		# extract argument, if one is needed
		arg = None
		if mtype == 3:
			arg = float(field.pop(0))
		elif mtype >= 4:
			arg = int(field.pop(0))
		# convert circuit numbers to spans
		if field[0].upper() == 'ALL':
			if len(field) > 1:
				return None
			spans = [(0x0000, 0xffff), (0xc000, 0xffff), (0xff00, 0xffff)]
		else:
			spans = _circuit_extents(field)
	except (IndexError, ValueError):
		return None

	# multiple short spans will result in lots of control
	# frames. the protocol has an option to specify circuits
	# as an index and a 16-bit mask rather than as an index
	# and count to mitigate this but we don't currently use it

	# build packet
	for span in spans:
		cct = span[0]
		cnt = span[1]
		if mtype == 0:
			# state = ON_MANUAL
			pkt_list.add_circuit(cct, cnt, 0, state = 3)
		elif mtype == 1:
			# state = OFF_MANUAL
			pkt_list.add_circuit(cct, cnt, 0, state = 6)
		elif mtype == 2:
			# state = ON
			pkt_list.add_circuit(cct, cnt, 0, state = 1)
		elif mtype == 3:
			# tariff = n
			pkt_list.add_circuit(cct, cnt, 0, tariff = arg)
		elif mtype == 4:
			# group = n
			pkt_list.add_circuit(cct, cnt, 0, group = arg)
		elif mtype == 5:
			# credit = n
			pkt_list.add_circuit(cct, cnt, 0, credit_set = arg)
		elif mtype == 6:
			# credit = credit + n
			pkt_list.add_circuit(cct, cnt, 0, credit_inc = arg)
		else:
			# credit = credit - n
			pkt_list.add_circuit(cct, cnt, 0, credit_dec = arg)

	return pkt_list

#
# unpack V3 configuration message adding frames to packet list
#
# returns 'pkt_list'
#
def _unpack_config(pkt_list, field):

	'unpack configuration message to frames'

	# parse 'key=value' pairs
	conf = { }
	for item in field:
		split = item.split('=')
		if len(split) == 2:
			conf[split[0].upper()] = split[1]

	# parse POLLTIME value
	interval = None
	try:
		interval = int(conf['POLLTIME'])
	except (KeyError, ValueError):
		pass

	# parse RECORDS value
	records = None
	try:
		records = int(conf['RECORDS'])
	except (KeyError, ValueError):
		pass

	# parse SERVER value
	sms_plan = None
	sms_intl = None
	sms = conf.get('SERVER', '')
	if sms and sms[0] == '+':
		sms = sms[1:]
		if sms.isdigit():
			sms_intl = sms
	elif sms.isdigit():
		sms_plan = sms

	# parse TLIMIT value
	tlimit = 0
	try:
		tlimit = int(conf['TLIMIT'])
	except (KeyError, ValueError):
		pass

	# parse TIME value
	time = None
	try:
		time = int(conf['TIME'])
	except (KeyError, ValueError):
		pass

	# add time setting frame if we had time
	if not time is None:
		pkt_list.add_time(time, tlimit)

	# add configuration frame if we had any valid items
	if not interval is None or not records is None or not sms_intl is None or not sms_plan is None:
		pkt_list.add_config(
			log_interval = interval,
			report_records = records,
			server_sms_intl = sms_intl,
			server_sms_plan = sms_plan)

	return pkt_list

_msg_types = {
	'ON'				: 0,
	'OFF'				: 1,
	'AUTO'			: 2,
	'TARIFF'			: 3,
	'PRIORITY'		: 4,
	'CREDIT-SET'	: 5,
	'CREDIT-ADD'	: 6,
	'CREDIT-SUB'	: 7,
	'GETCFG'			: 8,
	'STATUS'			: 9,
	'SETCFG'			: 10,
}

#
# unpack V3 circuit control message addings frames to packet list
#
# returns PacketList instance or 'None' on error
#
def unpack_v3(msg):

	'unpack V3 message to frames in a PacketList'

	# split into fields
	field = msg.split(',')

	# parse and remove header
	try:
		# extract identifier
		tag = field.pop(0)
		if tag[0] != '*':
			return None
		tag = int(tag[1:])
		# look up message type
		mtype = _msg_types[field.pop(0).upper()]
	except (IndexError, KeyError):
		return None

	pkt_list = PacketList(tag)

	# action based on message type
	if mtype <= 7:
		return _unpack_control(pkt_list, mtype, field)
	elif mtype == 8:
		# empty configuration frame does nothing
		pkt_list.add_config()
	elif mtype == 9:
		pkt_list.add_status_request()
	else: # mtype == 10
		return _unpack_config(pkt_list, field)

	return pkt_list

# vi:set ts=3 sw=3 ai:
