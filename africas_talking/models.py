import json

from django.conf import settings

from django.db import models
from mingi_django.archiving import retrieve_archive_init
from mingi_django.mingi_settings.custom import SMS_GATEWAY

# Message delivery status
DELIVERY_TYPES = (
    ('-1','Delivery Failed'),
    ('0','Delivery Success'),
    ('1','Message send'),
    ('2','Message not send'),
    ('3','Message queued'),
    ('4','Message not queued'),
    )
SMSTYPES = (
    ('sms','SEND AS SMS'),
    ('email','SEND AS EMAIL')
    )

class Gateway(models.Model):
    """
    Defines gateway
    """

    AT_GATEWAY = 'AT'
    TW_ANY_GATEWAY = 'TW'
    TW_US_GATEWAY = 'TW-US'
    TW_UK_GATEWAY = 'TW-UK'
    TW_FR_GATEWAY = 'TW-FR'
    F1_GATEWAY = 'F1'
    SC1_GATEWAY = 'SC02-+254702688230-SEND'
    SC2_GATEWAY = 'SC01-+254702690718-RECEIVE'
    SGA_GATEWAY = 'SMS-Gateway-API'
    MOCK_GATEWAY = 'MOCK'  # Doesn't send messages, just creates logs

    GATEWAY_TYPE_CHOICES = (
        (AT_GATEWAY, 'Africas Talking'),
        (TW_ANY_GATEWAY, 'Twilio (Any)'),
        (TW_US_GATEWAY, 'Twilio US'),
        (TW_UK_GATEWAY, 'Twilio UK'),
        (TW_FR_GATEWAY, 'Twilio France'),
        (F1_GATEWAY, 'FocusOne'),
        (SC1_GATEWAY, 'SC02-+254702688230-SEND'),
        (SC2_GATEWAY, 'SC01-+254702690718-RECEIVE'),
        (SGA_GATEWAY, 'SMS-Gateway-API'),
        (MOCK_GATEWAY, 'Mock Gateway')
    )

    # name of the gateway, e.g. KE-SAF-1; AT-SAF-2
    name = models.CharField(max_length=30, default="AT",unique=True)
    
    # telephone number for the gateway
    telephone = models.CharField(max_length=20, blank=True)

    # Device ID where gateway is installed
    device_id = models.CharField(max_length=20, blank=True,null=True, verbose_name='Device ID',unique=True)

    # username and password for accessing forwarding app
    username = models.CharField(max_length=20, blank=True)
    password = models.CharField(max_length=10, blank=True,verbose_name='Gateway Password or Secret Code')

    # Format for sending out the message
    sms_type = models.CharField(max_length=10,choices=SMSTYPES,default='sms',verbose_name='Format to Send Message')

    
    # url from which gateway will send messages
    send_url = models.CharField(max_length=300, verbose_name="Sneaker Cloud Send Url",blank=True)
    # url where messages from the gateway are received by Steama
    receive_url = models.CharField(max_length=300, verbose_name="Steama Endpoint Url",blank=True)

    # Which service this Gateway uses (e.g., Africa's Talking, Sneaker Cloud)
    gateway_type = models.CharField(max_length=30,
                                    choices=GATEWAY_TYPE_CHOICES)
    
    def __unicode__(self):
        return self.name + "  ("+self.telephone+")"

    class Meta:
        verbose_name = "Gateway"
        ordering = ['-name']

class Africas_Talking_Input(models.Model):
    """
    Represents an SMS from a user through Africa's Talking
    """

    raw_message = models.CharField(max_length=1000, verbose_name="Raw Message")

    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True)

    bit_harvester = models.ForeignKey('mingi_site.Bit_Harvester', 
                                      blank=True, null=True)

    timestamp = models.DateTimeField()

    message_type = models.CharField(max_length=10,
                                    verbose_name="Message Type")
    
    # Gateway used to receive the message
    gateway = models.ForeignKey('africas_talking.Gateway',blank=True,null=True)
    # gateway_type = models.CharField(max_length=50, blank=True,null=True,default='AT',choices=SMS_GATEWAY) # primary gateway
    
    third_party_timestamp = models.DateTimeField(blank=True, null=True,
                                          verbose_name="Third-Party Service Timestamp")

    @property
    def archive_format(self):
        """
        Gets the message log in a json format for archiving.
        """

        # Get the log in dictionary form
        self_dict = self.__dict__

        # Clean the log dictonary for stringification
        if self.user:
            self_dict['user_name'] = "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            self_dict['user_name'] = None
        if self.bit_harvester:
            self_dict['bit_harvester_harvester_id'] = self.bit_harvester.harvester_id
        else:
            self_dict['bit_harvester_harvester_id'] = None
        if self.timestamp:
            self_dict['timestamp'] = self.timestamp.isoformat()
        if self.third_party_timestamp:
            self_dict['third_party_timestamp'] = self.third_party_timestamp.isoformat()

        del self_dict['_user_cache']
        del self_dict['_bit_harvester_cache']
        del self_dict['_state']

        # Return the log dictionary as a json string
        return json.dumps(self_dict)

    def __unicode__(self):
        return str(self.user) + " " + str(self.timestamp)
    
    class Meta:
        verbose_name = "Africa's Talking Input"
        ordering = ['-timestamp']

class Africas_Talking_Output(models.Model):
    """
    Represents an SMS to a user through Africa's Talking
    """

    raw_message = models.CharField(max_length=1000, verbose_name="Raw Message")
    text_message = models.CharField(max_length=1000, blank=True, null=True)
    telephone = models.CharField(max_length=20, blank=True)

    user = models.ForeignKey('mingi_user.Mingi_User', blank=True, null=True)

    bit_harvester = models.ForeignKey('mingi_site.Bit_Harvester', 
                                      blank=True, null=True)

    timestamp = models.DateTimeField()

    # Gateway used to send the message
    gateway = models.ForeignKey('africas_talking.Gateway',blank=True,null=True)
    # gateway_type = models.CharField(max_length=50, blank=True,null=True,default='AT',choices=SMS_GATEWAY) # primary gateway

    # track messages queued through SMS sync
    send_status = models.CharField(max_length=2,choices=DELIVERY_TYPES,default='0')

    @property
    def archive_format(self):
        """
        Gets the message log in a json format for archiving.
        """

        # Get the log in dictionary form
        self_dict = self.__dict__

        # Clean the log dictonary for stringification
        if self.user:
            self_dict['user_name'] = "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            self_dict['user_name'] = None
        if self.bit_harvester:
            self_dict['bit_harvester_harvester_id'] = self.bit_harvester.harvester_id
        else:
            self_dict['bit_harvester_harvester_id'] = None
        if self.timestamp:
            self_dict['timestamp'] = self.timestamp.isoformat()

        del self_dict['_user_cache']
        del self_dict['_bit_harvester_cache']
        del self_dict['_state']

        # Return the log dictionary as a json string
        return json.dumps(self_dict)

    def __unicode__(self):
        return str(self.user) + " " + str(self.timestamp)
    
    class Meta:
        verbose_name = "Africa's Talking Output"
        ordering = ['-timestamp']


class Africas_Talking_Input_Archive(models.Model):
    """
    Stores the id of the archive of the given day's worth of Africa's Talking
    input messages.

    Note: Because only one archive is created for each day, any Africa's
    Talking Message objects added 90 days after the actual message (ie, a new
    Africas_Talking_Input created with a timestamp from 90 days ago) will
    remain in the database and not be archived.
    """

    # The day over which the messages are archived (UTC)
    date = models.DateField()

    # The AWS-generated id of the Glacier archive
    archive_id = models.CharField(max_length=200)

    def retrieve_archive_init(self):
        """
        Begins a retrieval job for this archive.

        Returns the boto.glacier.job.Job object, which can be polled later for
        the archive contents using job.get_output(). The function will produce
        "400" errors if it has not yet completed.
        """
        return retrieve_archive_init(
            settings.AFRICAS_TALKING_INPUT_VAULT_NAME, self.archive_id)

    class Meta:
        verbose_name = "Africa's Talking Input Archive"


class Africas_Talking_Output_Archive(models.Model):
    """
    Stores the id of the archive of the given day's worth of Africa's Talking
    output messages.

    Note: Because only one archive is created for each day, any Africa's
    Talking Message objects added 90 days after the actual message (ie, a new
    Africas_Talking_Output created with a timestamp from 90 days ago) will
    remain in the database and not be archived.
    """

    # The day over which the messages are archived (UTC)
    date = models.DateField()

    # The AWS-generated id of the Glacier archive
    archive_id = models.CharField(max_length=200)

    def retrieve_archive_init(self):
        """
        Begins a retrieval job for this archive.

        Returns the boto.glacier.job.Job object, which can be polled later for
        the archive contents using job.get_output(). The function will produce
        "400" errors if it has not yet completed.
        """
        return retrieve_archive_init(
            settings.AFRICAS_TALKING_OUTPUT_VAULT_NAME, self.archive_id)

    class Meta:
        verbose_name = "Africa's Talking Output Archive"


class MessageNotSentException(Exception):
    """
    Flag a message that is not sent when the Africa's Talking API is called
    """

    STD_MESSAGE = "This message was not sent!"
    
    def __init__(self, msg=None):
        if msg is not None:
            self.msg = MessageNotSentException.STD_MESSAGE + '; ' + msg
        else:
            self.msg = MessageNotSentException.STD_MESSAGE

    def __str__(self):

        return self.msg

class LineOverlapException(Exception):
    """
    This exception is thrown if 2+ users are registered in the 
    same site with the same line number
    """

    STD_MESSAGE = "Multiple users are registered on the same line!"

    def __init__(self):
        self.msg = LineOverlapException.STD_MESSAGE

    def __str__(self):
        return self.msg


class InvalidUserRegistration(Exception):
    """
    This exception signals that an invalid user was created
    or their registration was attempted, either with an invalid
    number or a repeated number
    """

    STD_MESSAGE = "Invalid user registration!"

    def __init__(self,type=None):

        self.error_type = type

        if (type == 1):
            self.msg = InvalidUserRegistration.STD_MESSAGE + ": User has invalid number"
        elif (type == 2):
            self.msg = InvalidUserRegistration.STD_MESSAGE + ": User has repeated number"
        else:
            self.msg = InvalidUserRegistration.STD_MESSAGE

            
    def __str__(self):

        return self.msg


class InvalidAttributeValue(Exception):
    """
    An object has an attribute with an invalid value,
    arguments are strings describing the impermissible situation
    """

    STD_MESSAGE = "An object has an attribute with an invalid value!"

    def __init__(self,item=None,attribute=None,value=None):

        if (item == None):
            self.item = ""
            self.attribute = ""
            self.value = ""
            self.msg = InvalidAttributeValue.STD_MESSAGE
            
        elif (attribute == None):
            self.item = str(item)
            self.attribute = ""
            self.value = ""
            self.msg = InvalidAttributeValue.STD_MESSAGE + ": " + str(item)

        elif (value == None):
            self.item = str(item)
            self.attribute = str(attribute)
            self.msg = InvalidAttributeValue.STD_MESSAGE + ": " + str(item) + ", " + str(attribute)

        else:
            self.item = str(item)
            self.attribute = str(attribute)
            self.value = str(value)
            self.msg = InvalidAttributeValue.STD_MESSAGE + ": " + str(item) + ", " + str(attribute) + " = " + str(value)


    def __str__(self):

        return self.msg

