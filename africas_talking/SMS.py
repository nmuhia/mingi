import datetime
from mingi_django.models import Error_Log
from django.views.decorators.csrf import csrf_exempt
from models import Gateway, Africas_Talking_Output, Africas_Talking_Input

from kopo_kopo.views import process_at_sms
from mingi_django.mingi_settings.custom import OPERATOR_MAPPER
from mingi_django.timezones import gen_zone_aware
from mingi_django.mingi_settings.base import TIME_ZONE

from django.http import HttpResponse
import json


@csrf_exempt
def sendMessage(request):

    try:
        gateway = Gateway.objects.get(username=request.POST['username'],
                                      password=request.POST['password'])
    except:
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="unknown device detected (username,password): " +
                            str(request.POST.get('username', None)) +
                            "," + str(request.POST.get('password')))
        return HttpResponse(json.dumps({"success": False,
                                        "error": "unknown device"}))

    # retrieve messages from cloud to send
    if request.POST['task'] == 'send':
        print request

        main_message = {
            "success": False,
            "error": None,
            "messages": []
        }

        msg_list = list()

        try:
            messages = Africas_Talking_Output.objects.filter(gateway=gateway,
                                                             send_status='4')

            for msg in messages:
                # send to user
                if msg.user:
                    msg_list.append({'to': str(msg.user.telephone),
                                     'message': str(msg.raw_message),
                                     'uuid': str(msg.id)})
                elif msg.bit_harvester:
                    if (msg.bit_harvester.send_telephone is not None) and \
                            (msg.bit_harvester.send_telephone != ''):
                        msg_list.append(
                            {'to': str(msg.bit_harvester.send_telephone),
                             'message': str(msg.raw_message),
                             'uuid': str(msg.id)})
                    else:
                        msg_list.append(
                            {'to': str(msg.bit_harvester.telephone),
                             'message': str(msg.raw_message),
                             'uuid': str(msg.id)})
                else:
                    pass
        except Exception as e:
            Error_Log.objects.create(
                problem_message='Error in sending SMS for gateway:' +
                                str(gateway) + ' error: ' + str(e))

        main_message['messages'] = msg_list
        main_message['success'] = True

        return HttpResponse(json.dumps(main_message))

    # process message send statuses
    if request.POST['task'] == 'update':

        try:
            messages = request.POST['messages']
            message_ids = json.loads(messages)

            for msg in message_ids['message_ids']:
                print msg['message_status']
                print msg['message_id']
                try:
                    Africas_Talking_Output.objects\
                        .filter(id=msg['message_id'])\
                        .update(send_status=msg['message_status'])
                except Exception as e:
                    Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Error saving" +
                                                             str(e))
                    pass

            return HttpResponse(json.dumps({"success": True, "error": None}))
        except Exception as e:
            Error_Log.objects.create(
                problem_type="INTERNAL",
                problem_message="Error in update: "+str(e))
            return HttpResponse(json.dumps({"success": False,
                                            "error": str(e)}))

    # update payment messages
    if request.POST['task'] == 'post_update':

        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        task = request.POST.get('task', None)

        # error tracking
        success = True
        error = None

        messages_str = request.POST.get("messages", "[]")
        messages = json.loads(messages_str)

        failed_messages = list()

        # loop through all messages and update with cloud
        z = 0
        for msg in messages:
            # check from phone number
            try:
                send_no = OPERATOR_MAPPER[msg['from']]
            except Exception as e:
                Error_Log.objects.create(
                    problem_type="DEBUGGING",
                    problem_message="Payment from invalid sender flagged. "
                                    "Details (sender,gateway_name,message): " +
                                    str(msg['from']) + ", " + str(username) +
                                    ", " + str(msg['message']))

            message = msg['message'].replace('\n', '').replace('\r', '')

            try:
                [success, error] = process_at_sms(msg['from'], msg['time'],
                                                  msg['time'], message, True)
            except Exception as e:
                Error_Log.objects.create(problem_type="DEBUGGING",
                                         problem_message="SC Errors: "+str(e))
                [success, error] = [True, None]
            if not success:
                if z == 0:
                    failed_messages.append({"success": success})
                failed_messages.append({"time": msg['time'], "error": error})
            z = z+1

        # if no errors found, return success for all messages
        if len(failed_messages) == 0:
            failed_messages.append({"success": success})

        return HttpResponse(json.dumps(failed_messages))

    # process health status message
    if request.POST['task'] == 'status':

        status = request.POST['status']

        now = gen_zone_aware(TIME_ZONE, datetime.datetime.now())

        # save message
        Africas_Talking_Input.objects.create(raw_message=status,
                                             gateway=gateway,
                                             message_type="HEALTH_REPORT",
                                             timestamp=now)

        return HttpResponse(json.dumps({"success": True, "error": None}))
