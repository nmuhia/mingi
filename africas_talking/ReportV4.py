#!/usr/bin/python
#
# (C) BitBox Ltd 2015
#
# $Id: ReportV4.py 1183 2015-09-07 08:38:04Z phorton $
#

from struct import calcsize, unpack_from
from time import gmtime, asctime
import TextV4

_UTC_OFFSET = 13 * 60 * 60

#
# container class for energy report frame
#
class FrameEnergy:

	'container for energy report frame'

	_layout = '<IBHB'

	_cct_states = (
		'ON-SETTLING',
		'ON',
		'ON-IDLE',
		'ON-MANUAL',
		'OFF-CREDIT',
		'OFF-GROUP',
		'OFF-MANUAL',
		'OFF-TRIP-0',
		'OFF-TRIP-1'
	)

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack energy report frame'
		# check we have enough data for header
		energy_idx = index + calcsize(self._layout)
		if energy_idx > len(octets):
			return 0
		header = unpack_from(self._layout, octets, index)
		# calculate actual frame size
		cct_count = header[3]
		if not cct_count:
			return 0
		state_idx = energy_idx + cct_count * 4
		state_octets = (cct_count + 1) / 2
		tail_idx = state_idx + state_octets
		# check we have enough data for payload
		if tail_idx >  len(octets):
			return 0
		# unpack header
		self.time_stamp	= header[0]
		self.circuit		= header[2] # 0 based
		# unpack energy values
		self.energy = unpack_from('<' + 'I' * cct_count, octets, energy_idx)
		# unpack circuit states
		states = unpack_from('B' * state_octets, octets, state_idx)
		self.state = [ ]
		for state in states:
			self.state.append(state & 0xf)
			if len(self.state) < cct_count:
				self.state.append(state >> 4)
		return tail_idx - index

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		stamp = self.time_stamp + _UTC_OFFSET
		circuit = self.circuit + 1
		# METERS
		msg_list.append('*0,%u,0,METERS,%u,%s' % (stamp, circuit, ','.join(map(str, self.energy))))
		# STATUS
		# states 0, 1, 2 and 3 are ON - 4, 5, 6, 7 and 8 are OFF
		state_bool = ''.join(map(lambda x: x < 4 and '1' or '0', self.state))
		state_hex = ''.join(map(lambda x: '%x' % x, self.state))
		msg_list.append(','.join(('*0,%u,0,STATUSN,%u' % (stamp, circuit), state_bool, state_hex)))

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of energy report frame'
		info = [
			'ENERGY-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp))
		]
		# format energy
		cct = self.circuit
		for energy in self.energy:
			cct = cct + 1
			info.append('energy-%03u:      %u' % (cct, energy))
		# format circuit state
		cct = self.circuit
		for state in self.state:
			cct = cct + 1
			try:
				text = self._cct_states[state]
			except IndexError:
				text = '#%u' % state
			info.append('state-%03u:       %s' % (cct, text))
		return '\n'.join(info)

#
# container class for credit report frame
#
class FrameCredit:

	'container for credit report frame'

	_layout = '<IBHB'

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack credit report frame'
		# check we have enough data for header
		credit_idx = index + calcsize(self._layout)
		if credit_idx > len(octets):
			return 0
		header = unpack_from(self._layout, octets, index)
		# calculate actual frame size
		cct_count = header[3]
		if not cct_count:
			return 0
		tail_idx = credit_idx + cct_count * 4
		# check we have enough data for payload
		if tail_idx >  len(octets):
			return 0
		# unpack the data
		self.time_stamp	= header[0]
		self.circuit		= header[2]	# 0 based
		self.credit			= unpack_from('<' + 'I' * cct_count, octets, credit_idx)
		return tail_idx - index

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		'return credit report frame in V3 style'
		# CREDIT
		msg_list.append('*0,%u,0,CREDIT,%u,%s' % (self.time_stamp + _UTC_OFFSET, self.circuit + 1, ','.join(map(str, self.credit))),)

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of credit report frame'
		info = [
			'CREDIT-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp))
		]
		cct = self.circuit
		# format credit
		for credit in self.credit:
			cct = cct + 1
			info.append('credit-%03u:      %u' % (cct, credit))
		return '\n'.join(info)

#
# container class for status report frame
#
class FrameStatus:

	'container for status report frame'

	_layout = '<I3BH3BH2I2H'

	_report_types = (
		'SCHEDULED',
		'ACKNOWLEDGE',
		'BLACKOUT',
		'POWER-ON'
	)

	_report_names = (
		'DATA',
		'',
		'BLACKOUT',
		'POWERON'
	)

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack status report frame'
		# check we have enough data
		length = calcsize(self._layout)
		if index + length > len(octets):
			return 0
		# unpack the data
		raw_data = unpack_from(self._layout, octets, index)
		self.time_stamp		= raw_data[0]
		self.relay_state		= raw_data[2] & 0xf
		self.report_type		= raw_data[2] >> 4
		self.state_flags		= raw_data[3]
		self.control_tag		= raw_data[4]
		self.control_error	= raw_data[5]
		self.meters_config	= raw_data[6]
		self.meters_present	= raw_data[7]
		self.config_count		= raw_data[8]
		self.firmware_major	= raw_data[9] >> 24
		self.firmware_minor	= raw_data[9] >> 12 & 0xfff
		self.firmware_patch	= raw_data[9] & 0xfff
		self.serial_number	= 'IRP%c%04u' % (ord('A') + raw_data[10] / 10000, raw_data[10] % 10000)
		self.log_interval		= raw_data[11]
		self.report_records	= raw_data[12]
		return length

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		stamp = self.time_stamp + _UTC_OFFSET
		if self.report_type == 1:
			# ERROR / OK
			if self.control_error:
				name = 'ERROR#%u' % self.control_error
			else:
				name = 'OK'
			msg_list.append('*%u,%u,0,%s' % (self.control_tag, stamp, name))
		else:
			try:
				name = self._report_names[self.report_type]
			except IndexError:
				name = 'DATA'
			# DATA / POWERON / BLACKOUT
			# we don't have supply voltage or temperature as they are logged values
			msg_list.append('*0,%u,%u,%s,%u.%u.%u,,,%u,%u' % (
				stamp, self.config_count, name,
				self.firmware_major, self.firmware_minor, self.firmware_patch,
				self.meters_config, self.meters_present))
			# RELAYS
			# scheduled reports only
			if self.report_type == 0:
				relays = '*0,%u,0,STATUSN,A,' % stamp
				for index in range(4):
					relays = relays + (self.relay_state & (1 << index) and '1' or '0')
				msg_list.append(relays)

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of status report frame'
		info = [
			'STATUS-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp)),
			'state-flags:     0x%x' % self.state_flags,
			'control-tag:     %u' % self.control_tag,
			'control-error:   %u' % self.control_error,
			'meter-count:     %u / %u' % (self.meters_present, self.meters_config),
			'config-count:    %u' % self.config_count,
			'firmware-rev:    v%u.%u.%u' % (self.firmware_major, self.firmware_minor, self.firmware_patch),
			'serial-number:   %s' % self.serial_number,
			'log-interval:    %umins' % self.log_interval,
			'report-records:  %u' % self.report_records
		]
		# format relay state
		relays = [ ]
		for index in range(4):
			if self.relay_state & (1 << index):
				relays.append(str(index + 1))
		info.insert(1, 'relays-on:       %s' % ' '.join(relays))
		# format report type
		try:
			rtype = self._report_types[self.report_type]
		except IndexError:
			rtype = '#%u' % self.report_type
		info.insert(2, 'report-type:     %s' % rtype)
		return '\n'.join(info)

#
# container class for head report frame
#
class FrameHead:

	'container for head report frame'

	_layout = '<IBHh4I4H'

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack head report frame'
		# check we have enough data
		length = calcsize(self._layout)
		if index + length > len(octets):
			return 0
		# unpack the data
		raw_data = unpack_from(self._layout, octets, index)
		self.time_stamp		= raw_data[0]
		self.supply_voltage	= raw_data[2] / 256.0
		self.ext_temperature	= raw_data[3] / 256.0
		self.pulse_counts		= raw_data[4:8]
		self.pulse_ages		= raw_data[8:]
		return length

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		stamp = self.time_stamp + _UTC_OFFSET
		# DATA
		# we don't have firmware version or circuit/board count as they are non-logged values
		msg_list.append('*0,%u,0,DATA,,%.2f,%.2f,,' % (stamp, self.supply_voltage, self.ext_temperature))
		msg_list.append('*0,%u,0,METERS,A,%u,%u,%u,%u,,%u,%u,%u,%u' % (stamp,
			self.pulse_counts[0], self.pulse_counts[1], self.pulse_counts[2], self.pulse_counts[3],
			self.pulse_ages[0], self.pulse_ages[1], self.pulse_ages[2], self.pulse_ages[3]))

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of head report frame'
		info = [
			'HEAD-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp)),
			'supply-voltage:  %.2fV' % self.supply_voltage,
			"ext-temperature: %.2f'C" % self.ext_temperature
		]
		# format pulse counts and pulse ages
		for index in range(len(self.pulse_counts)):
			age = min(self.pulse_ages[index], 35999)
			info.append('pulse-count-%u:   %u' % (index + 1, self.pulse_counts[index]))
			info.append('pulse-age-%u:     %u:%02u:%02u' % (index + 1, age / 3600, age / 60 % 60, age % 60))
		return '\n'.join(info)

#
# container class for pulse report frame
#
class FramePulse:

	'container for pulse report frame'

	_layout = '<IBHB'

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack pulse report frame'
		# check we have enough data for header
		pulse_idx = index + calcsize(self._layout)
		if pulse_idx > len(octets):
			return 0
		header = unpack_from(self._layout, octets, index)
		# calculate actual frame size
		cct_count = header[3]
		if not cct_count:
			return 0
		tail_idx = pulse_idx + cct_count * calcsize('IH')
		# check we have enough data for payload
		if tail_idx >  len(octets):
			return 0
		# unpack the data
		if header[2] < 0xc000:
			return 0
		self.time_stamp	= header[0]
		self.circuit		= header[2]	- 0xc000
		self.pulse_data	= unpack_from('<' + 'IH' * cct_count, octets, pulse_idx)
		return tail_idx - index

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		'return pulse report frame in V3 style'
		# PULSES
		msg_list.append('*0,%u,0,PULSES,@%u,%s' % (self.time_stamp + _UTC_OFFSET, self.circuit + 1, ','.join(map(str, self.pulse_data))))

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of pulse report frame'
		info = [
			'PULSE-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp))
		]
		cct = self.circuit
		for pair in map(None, self.pulse_data[0::2], self.pulse_data[1::2]):
			cct = cct + 1
			age = min(pair[1], 35999)
			info.append('pulse-count-%u:   %u' % (cct, pair[0]))
			info.append('pulse-age-%u:     %u:%02u:%02u' % (cct, age / 3600, age / 60 % 60, age % 60))
		return '\n'.join(info)

#
# container class for line report frame
#
class FrameLine:

	'container for line report frame'

	_layout = '<IB2H'

	#
	# unpack binary frame
	#
	# returns frame length or 0 on error
	#
	def unpack(self, octets, index):
		'unpack line report frame'
		# check we have enough data for header
		line_idx = index + calcsize(self._layout)
		if line_idx > len(octets):
			return 0
		header = unpack_from(self._layout, octets, index)
		# calculate actual frame size
		cct_count = header[3]
		if not cct_count:
			return 0
		byte_count = (cct_count + 7) / 8
		tail_idx = line_idx + byte_count
		# check we have enough data for payload
		if tail_idx >  len(octets):
			return 0
		# unpack the data
		if header[2] < 0xc000:
			return 0
		self.time_stamp	= header[0]
		self.circuit		= header[2]	- 0xc000

		data = unpack_from(str(byte_count) + 'B', octets, line_idx)
		states = []
		for cct in range(cct_count):
			bit = cct % 8
			if bit == 0:
				val = data[cct / 8]
			states.append((val >> bit) & 1)

		self.states = states

		return tail_idx - index

	#
	# format frame data as V3 messages
	#
	def pack_v3(self, msg_list):
		'return line report frame in V3 style'
		msg_list.append('*0,%u,0,STATUSN,@%u,%s' % (self.time_stamp + _UTC_OFFSET, self.circuit + 1, ''.join(map(str, self.states))))

	#
	# format frame data as text
	#
	def __str__(self):
		'return text version of line report frame'
		info = [
			'LINE-REPORT-FRAME',
			'time-stamp:      %s' % asctime(gmtime(self.time_stamp))
		]
		for index in range(len(self.states)):
			info.append('state-%03u:       %u' % (index + 1, self.states[index]))
		return '\n'.join(info)

_frame_map = {
	'\x37' : FrameLine,
	'\x6b' : FrameEnergy,
	'\x71' : FrameCredit,
	'\x7c' : FrameStatus,
	'\xbe' : FrameHead,
	'\xe3' : FramePulse,
}

#
# unpack SMS to a list of frame objects
#
# returns 'None' on error
#
def sms_unpack(sms_body):
	'unpack report frames from text message'
	frames = [ ]
	octets = TextV4.sms_unpack(sms_body)
	if octets is None:
		return None
	# iterate through frames
	index = 0
	while len(octets) - index >= 5:
		# map frame type to class
		try:
			frame = _frame_map[octets[index + 4]]()
		except KeyError:
			return None
		# unpack frame
		length = frame.unpack(octets, index)
		if not length:
			return None
		frames.append(frame)
		# on to next frame
		index = index + length
	# we should have used all the data
	if index != len(octets):
		return None
	return frames

#
# format list of frame objects as a list of V3 messages
#
def pack_v3(frames):
	'format frames as V3 messages'
	msgs = [ ]
	for frame in frames:
		frame.pack_v3(msgs)
	return msgs
		
#
# unpack SMS and format as a list of V3 messages
#
# returns 'None' on error
#
def sms_unpack_as_v3(sms_body):
	'unpack text message and format as V3 messages'
	frames = sms_unpack(sms_body)
	if frames is None:
		return None
	return pack_v3(frames)

# vi:set ts=3 sw=3 ai:
