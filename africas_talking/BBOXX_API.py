import requests
import datetime
import json

from mingi_django.mingi_settings.custom import BBOXX_USERNAME, BBOXX_PASSWORD
from mingi_django.mingi_settings.base import TIME_ZONE
from mingi_django.timezones import gen_zone_aware

from mingi_django.models import Error_Log
from mingi_site.models import Line_State, Mingi_Site, Site_Record, Bit_Harvester, Line

BASE_URL = "http://smartapi.bboxx.co.uk/v1/"

AUTH_URL = BASE_URL + "auth/login"
STATE_URL = BASE_URL + "enable_history"

# Strings returned by the /enable_history endpoint
STATE_ENABLED = "enabled"
STATE_DISABLED = "disabled"

# Actual state changes (as opposed to commands conveying intention)
UPDATED_STATES = (STATE_ENABLED, STATE_DISABLED)

BBOXX_ENERGY_LINE_NUMBER = 1
BBOXX_PULSE_COUNT_LINE_NUMBER = 2

UNKNOWN_IMEI_ERROR_MESSAGE = \
    "Unknown bitHarvester IMEI referenced in BBOXX API: %s"


def get_bboxx_token():
    """
    return API session token for BBOXX dashboard
    """
    r = requests.post(AUTH_URL, data={'username': BBOXX_USERNAME,
                                      'password': BBOXX_PASSWORD})
    token = r.json()['message']['login_successful']['API_token']

    return token


def get_bboxx_headers():
    """
    Obtains a token from the BBOXX API and uses it to construct headers for
    BBOXX API calls.

    :return: A dictionary containing the headers for a BBOXX API call.
    """
    return {
        'Content-Type': 'application/json',
        'Authorization': 'Token token=' + get_bboxx_token()
    }


def collect_BBOXX_data(start_time='2016-03-01 00:00:00', end_time='2016-03-03 00:00:00'):
    """
    retrieve all BBOXX data and push through to server
    """
    header = get_bboxx_headers()

    update_threshold = gen_zone_aware(
        TIME_ZONE, datetime.datetime.now()) - datetime.timedelta(hours=4)

    for bh in Bit_Harvester.objects.filter(version="LITE"):

        # skip if last update less than four hours ago
        records = Site_Record.objects.filter(site=bh.site)
        if len(records) > 0:
            last_log = records[0].timestamp
            if last_log >= update_threshold:
                continue

        num = bh.serial_number

        metrics = ['current', 'voltage', 'temperature', 'energy']

        # Only ask for pulse_count if bitHarvester has more than one line
        has_pulse_count = bh.site.num_lines > 1
        if has_pulse_count:
            metrics.append('pulse_count')

        # get data for each product
        data = json.dumps({
            "start_time": start_time,
            "end_time": end_time,
            "metrics": metrics
        })

        r = requests.get(BASE_URL + "products/" + num +
                         "/rm_data", data=data, headers=header)

        if r.status_code != 200:
            Error_Log.objects.create(
                problem_type="EXTERNAL", problem_message="Error response code '%d' from BBOXX data request for IMEI '%s': '%s'" % (r.status_code, num, r.text))
            continue

        try:
            json_response = r.json()
        except ValueError:
            Error_Log.objects.create(problem_type="EXTERNAL",problem_message="Non-JSON response from BBOXX data request for IMEI '%s': '%s'" % (num, r.text))
            continue

        if "Failure" in json_response:
            Error_Log.objects.create(
                problem_type="EXTERNAL",
                problem_message="Failure response from BBOXX data request for "
                                "IMEI '%s': '%s'" % (num,
                                                     json_response["Failure"])
            )
            continue

        try:
            site = Bit_Harvester.objects.get(serial_number=num).site
        except Bit_Harvester.DoesNotExist:
            site = Mingi_Site.objects.all()[0]  # TEMP
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="BitHarvester with ID: " + num + " does not exist!")

        try:
            if len(json_response['current']) > 0:
                # if its cron task, update the last time when argument is run
                current_data = json_response['current'][0]['points']

                # store current data as Site Records with key "Current Log"
                for row in current_data:

                    # check for invalid timestamp
                    if int(row[0]) > 1000000:
                        timestamp = gen_zone_aware(
                            TIME_ZONE, datetime.datetime.fromtimestamp(int(row[0]) / 1000))

                        current_value = str(row[2])
                        try:
                            Site_Record.objects.get(
                                site=site, timestamp=timestamp, key="Current Log")
                            print "duplicate found"
                        except Site_Record.DoesNotExist:
                            log = Site_Record.objects.create(
                                site=site, timestamp=timestamp, value=current_value, key="Current Log")
                        except Site_Record.MultipleObjectsReturned:
                            print "duplicates!"
                            pass

        except IndexError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))
        except KeyError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))

        try:
            if len(json_response['voltage']) > 0:
                voltage_data = json_response['voltage'][0]['points']

                # store voltage data in Steama Site Records
                for row in voltage_data:

                    # check for invalid timestamp
                    if int(row[0]) > 1000000:
                        timestamp = gen_zone_aware(
                            TIME_ZONE, datetime.datetime.fromtimestamp(int(row[0]) / 1000))

                        voltage_value = str(row[2])
                        try:
                            Site_Record.objects.get(
                                site=site, timestamp=timestamp, key="BH Data Log")
                            print "duplicate found"
                        except Site_Record.DoesNotExist:
                            log = Site_Record.objects.create(
                                site=site, timestamp=timestamp, key="BH Data Log", value=voltage_value + ",-100")
                        except Site_Record.MultipleObjectsReturned:
                            print "duplicates!"
                            pass

        except IndexError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))
        except KeyError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))

        try:
            if len(json_response['temperature']) > 0:
                temp_data = json_response['temperature'][0]['points']

                # store voltage data in Steama Site Records
                for row in temp_data:

                    # check for invalid timestamp
                    if int(row[0]) > 1000000:
                        timestamp = gen_zone_aware(
                            TIME_ZONE, datetime.datetime.fromtimestamp(int(row[0]) / 1000))

                        temp_value = str(row[2])
                        try:
                            log = Site_Record.objects.get(site=site, timestamp=timestamp, key="BH Data Log")
                            value = log.value.split(',')[0] + ',' + str(temp_value)
                            log.value = value
                            log.save()

                        except Site_Record.DoesNotExist:
                            print "No log found"
                            pass
                        except Site_Record.MultipleObjectsReturned:
                            print "duplicates!"
                            pass

        except IndexError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))
        except KeyError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))

        try:
            if len(json_response['energy']) > 0:
                energy_data = json_response['energy'][0]['points']

                # store energy data in Steama as Line States for Line 1
                for row in energy_data:

                    # check for invalid timestamp
                    if int(row[0]) > 1000000:
                        timestamp = gen_zone_aware(
                            TIME_ZONE, datetime.datetime.fromtimestamp(int(row[0]) / 1000))

                        energy_value = float(row[2]) / 1000
                        try:
                            Line_State.objects.get(
                                site=site, timestamp=timestamp, meter_reading=energy_value, number=BBOXX_ENERGY_LINE_NUMBER)
                            print "duplicate found"
                        except Line_State.DoesNotExist:
                            if energy_value >= 0:
                                print "Creating log, meter reading: " + str(energy_value)
                                log = Line_State.objects.create(
                                    site=site, timestamp=timestamp, meter_reading=energy_value, number=BBOXX_ENERGY_LINE_NUMBER)
                        except Site_Record.MultipleObjectsReturned:
                            print "duplicates!"
                            pass

        except IndexError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))
        except KeyError as e:
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message=str(e))

        if has_pulse_count:
            try:
                if len(json_response['pulse_count']) > 0:
                    pulse_data = json_response['pulse_count'][0]['points']
                    this_line = Line.objects.get(number=2, site=site)

                    # store pulse_count data in Steama as Line States for Line 2
                    for row in pulse_data:

                        # sanity check on the timestamp
                        if int(row[0]) > 1000000:
                            timestamp = gen_zone_aware(
                                TIME_ZONE, datetime.datetime.fromtimestamp(int(row[0]) / 1000))

                            # check for a pulse ratio to convert to meaningful units
                            if this_line.pulse_ratio is not None:
                                pulse_value = float(row[2])/1000 * this_line.pulse_ratio
                            else:
                                pulse_value = float(row[2]) / 1000

                            try:
                                Line_State.objects.get(
                                    site=site, timestamp=timestamp, meter_reading=pulse_value, number=BBOXX_PULSE_COUNT_LINE_NUMBER)
                                print "duplicate found"
                            except Line_State.DoesNotExist:
                                if pulse_value >= 0:
                                    print "Creating log, meter reading: " + str(pulse_value)
                                    log = Line_State.objects.create(
                                        site=site, timestamp=timestamp, meter_reading=pulse_value, number=BBOXX_PULSE_COUNT_LINE_NUMBER)
                            except Site_Record.MultipleObjectsReturned:
                                print "duplicates!"
                                pass

            except IndexError as e:
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message=str(e))
            except KeyError as e:
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message=str(e))

            except Line.DoesNotExist as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))
            except Line.MultipleObjectsReturned as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e))


def bboxx_switch_trigger(command, imei):
    """
    Send an API call to BBOXX dashboard to switch a unit 
    on (enable) / off (disable)
    """
    if command == "ON":
        url = BASE_URL + 'products/' + imei + '/enable'

    elif command == "OFF":
        url = BASE_URL + 'products/' + imei + '/disable'

    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Invalid API command: should be 'ON' or 'OFF'")
        return "There was an error sending this message."

    r = requests.post(url, headers=get_bboxx_headers())

    try:
        status = r.json()['status']
        if status != "success":
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="API switching command sent to unit %s was unsuccessful: %s" % (imei, status))
            return "There was an error sending this message."
        else:
            return "Message sent successfully!"
    except KeyError as e:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="API switching command sent to unit %s was unsuccessful: %s" % (imei, str(e)))
        return "There was an error sending this message."


def update_bboxx_line_states(start_time):
    """
    Updates any Line objects managed by the BBOXX API whose states have changed
    since start_time.

    :param start_time: A datetime object representing the start of the period
        over which to pull state changes.
    """
    latest_line_states = _get_bboxx_line_states(start_time)

    for imei, state in latest_line_states.iteritems():
        try:
            bit_harvester = Bit_Harvester.objects.get(serial_number=imei)
        except Bit_Harvester.DoesNotExist:
            Error_Log.objects.create(
                problem_type="EXTERNAL",
                problem_message=UNKNOWN_IMEI_ERROR_MESSAGE % imei)
            continue
        lines = Line.objects.filter(site=bit_harvester.site)
        for line in lines:
            if state == STATE_ENABLED:
                line.line_status = Line.ON_MANUAL
            elif state == STATE_DISABLED:
                line.line_status = Line.OFF_MANUAL
            else:
                raise RuntimeError("Unknown BBOXX line state: %s" % state)
            line.save()


def _get_bboxx_line_states(start_time):
    """
    Gets the latest line states, if changed since start_time.

    :param start_time: A datetime object representing the start of the period
        over which to pull state changes.
    :return: A dictionary mapping IMEI numbers to states (members of
        UPDATED_STATES)
    """

    # Get list of line state objects
    params = {"q": json.dumps({"filters": [{"name": "created_at",
                                            "op": ">=",
                                            "val": start_time.isoformat()}]})}
    r = requests.get(STATE_URL, params=params, headers=get_bboxx_headers())
    line_states_list = r.json()['objects']

    # Sort to guarantee most recent last
    line_states_list.sort(key=lambda x: x['created_at'])

    # Convert to dictionary of states, most recent state replacing earlier ones
    line_states_dict = {}
    for ls in line_states_list:
        if ls['current_enable_state'] in UPDATED_STATES:
            line_states_dict[ls['product_imei']] = ls['current_enable_state']

    return line_states_dict
