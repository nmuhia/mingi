from userResponse import *
from bhResponse import *

from mingi_django.models import Error_Log
from models import Africas_Talking_Input, Africas_Talking_Output
from kopo_kopo.models import Kopo_Kopo_Message

import datetime

BH_REPEAT_EXC_ID = -999


def respondToUser(user,msgIn,now):

    """
    Trigger the appropriate response to a SMS from a user
    """

    # get command for comma or dash delimited message
    commandElements_DSVmsg = msgIn.split('-')[0].strip()
    commandElements_CSVmsg = commandElements_DSVmsg.split(',')
    command = commandElements_CSVmsg[0].strip()
    command = command.upper()

    if (command == ""):
        if (user.is_user == True) or (user.is_demo == True):
            sendMenu(user)
        else:
            commandNotRecognized(user)
            
    elif (command == "1"):
        if (user.is_user == True) or (user.is_demo == True):
            sendBalance(user)
        else:
            commandNotRecognized(user)

    elif (command == "2"):
        if (user.is_user == True) or (user.is_demo == True):
            sendBuyPower(user)
        else:
            commandNotRecognized(user)

    elif (command == "3"):
        if (user.is_user == True) or (user.is_demo == True):
            sendNumbers(user)
        else:
            commandNotRecognized(user)

    elif (command == "6"):
        if (user.is_field_manager == True) or (user.is_demo == True):
            registerNewUser(user,msgIn)
        else:
            commandNotRecognized(user)

    elif (command == "7"):
        if (user.is_site_manager == True) or (user.is_agent == True):
            print "ok"
            logMeterUpdate(user,msgIn,now)
        else:
            commandNotRecognized(user)

    elif (command == "8"):
        if (user.is_site_manager == True) or (user.is_agent == True) or (user.is_field_manager == True):
            logProblem(user,msgIn)
        else:
            commandNotRecognized(user)

    elif (command == "9"):
        if (user.is_site_manager == True) or (user.is_agent == True):
            logLineStatus(user,msgIn)
        else:
            commandNotRecognized(user)

    elif (command == "10") or (command == "11"):
        if (user.is_agent == True):
            logSystemVoltage(user,msgIn,now)
        else:
            commandNotRecognized(user)

    elif (command == "12"):
        if (user.is_site_manager == True):
            logFuelPurchase(user,msgIn,now)
	else:
	    commandNotRecognized(user)

    elif (command == "NAME" or command == "JINA"):
        if (user.is_user == True):       
            companyNamePoll(user)
	else:
	    commandNotRecognized(user)

    elif (command == "YES"):
        if (user.is_agent == True) or (user.is_site_manager == True):
            site = user.site
            logMeterUpdate(user,site.last_attempted_log,now,True)

        elif (user.is_user == True) or (user.is_demo == True):
            confirmUserRegistration(user)
            sendMenu(user)

        else:
            commandNotRecognized(user)

    elif ("BH" in command):
        bh_commands = commandElements_DSVmsg.split(',')
        #check command type initiated
        bh_number = bh_commands[1]
        command_type = bh_commands[2]
        command_type = command_type.upper()
        try:
            bh_number = validatePhoneNo(bh_number)
            bh = Bit_Harvester.objects.get(telephone=bh_number)

            if(command_type == "STATUS"):     
                sendBHStatus(user,bh)
            elif (command_type == "ON"):       
                lineNos = commandElements_CSVmsg[3:]
                turnLinesOn(user,bh,lineNos)
            elif (command_type == "OFF"):
                lineNos = commandElements_CSVmsg[3:]
                turnLinesOff(user,bh,lineNos)
                    
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message=str(e))
            pass

    # payment reporting function for cash-payment sites
    elif (command == "PAY"):
        
        # check that sender is authorized
        if not user.is_site_manager:
            message = "Message received from unauthorized number"
        
        # parse out payment information
        else:
            # Do something for manual payment processing
            pass

    else:
        commandNotRecognized(user)


def respondToBH(BH,msgIn,now,bh_ts):
    """
    Trigger the appropriate response to a SMS from a bitHarvester
    """

    msgElements = msgIn.split(',')

    if (BH.id == BH_REPEAT_EXC_ID):
        bh_ts = datetime.datetime.now()

    # message id is a number with a leading asterisk
    id_num = msgElements[0].lstrip('*')
    commandType = id_num[0]

    #respond to initiator of messages
    """
    if BH.version == "V4":
        try:
            atIn = Africas_Talking_Input.objects.filter(raw_message__contains=str(id_num),user__isnull=False).order_by('-timestamp')[0]
            user = atIn.user
            if user is not None:
                msgOut = msgIn
                try:
                    sendSMS(user.telephone,msgOut,user)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                    pass
                
        except Africas_Talking_Input.DoesNotExist as e:
            pass    
        except IndexError:
            pass
            """
    
    # respond to status update
    if (commandType == "0") or (commandType == "4"):

        if BH.version == "V3":
            repeatMsgs = Africas_Talking_Input.objects.filter(bit_harvester=BH,
                                                              raw_message=msgIn)
        else:
            # *** think of an alternate check for V4 ***
            repeatMsgs = list()
        
        if (len(repeatMsgs) > 1) and (BH.id != BH_REPEAT_EXC_ID):
            Error_Log.objects.create(problem_type="EXTERNAL",
                                     problem_message="Repeated message received from BH: " + str(BH))

        else:
            receiveSystemData(msgElements,BH,now,bh_ts)
        
    # respond to a line status switch alert
    else: 
        process_trigger_response(msgElements,BH)
        # logLineStatusChange(msgElements,BH)
       
    # DEPRECATED: process full range of potential trigger responses (see above)
    """
    elif commandType == "2":
        print "This is a response to getcfg"
        
    # respond to updated config parameters alert
    elif commandType == "3":
        
        print "This is a response to setcfg"        
        
    # note an unrecognized command
    else:
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Command %s not recognized from bitHarvester: %s" % (commandType, BH.telephone))

                           """


def respond_to_solo(sender,msgIn):
    """
    Respond to a SOLO bitHarvester message by updating the line state
    """

    elements = [sender.harvester_id]

    if msgIn == "STATE IS ON":
        elements.append("ON")
    elif msgIn == "STATE IS OFF":
        elements.append("OFF")
    else:
        return

    elements.append("ALL")

    logLineStatusChange(elements,sender)
