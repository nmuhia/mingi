"""
Tests for the Africa's Talking handler page.
"""
import datetime
import json
import mock
import pytz
import unittest

from django.test import TestCase
from django.utils import timezone

from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.models import Error_Log, Utility
from mingi_django.tests import SteamaDBTestCase
from mingi_user.models import Mingi_User
from mingi_site.models import Mingi_Site, Bit_Harvester, Line_State,\
    Site_Record, Line

from .BBOXX_API import collect_BBOXX_data, update_bboxx_line_states, \
    STATE_DISABLED, STATE_ENABLED
from .bhResponse import receiveSystemData, process_v4_status_update,\
    processMeterReadings, logDataValues, lineStatusMaintenance,\
    logLineStatusChange, updateLineStatus, send_line_switching_msgs
from .handleSMS import respondToUser, respondToBH, respond_to_solo
from .models import Africas_Talking_Input, Africas_Talking_Output,\
    InvalidUserRegistration, Gateway
from .sendSMS import locateUser, sendSMS, UserNotFound
from .sms_gateway_api import send_sms_gateway_api, SGA_ACCOUNT_EMAIL, \
    SGA_ACCOUNT_PASSWORD, SGA_CALLBACK_SECRET, SGA_SEND_MESSAGE_URL
from .userResponse import sendMenu, sendBalance, sendBuyPower, registerNewUser,\
    logMeterUpdate, logProblem, logLineStatus, logSystemVoltage, createNewUser,\
    processPhoneNo, findUserByLine, scanLogForErrors, handleEnergyUse,\
    handleLineStatusConflict, get_respond_to_number
from .views import process_kopo_kopo_payment_info, PROCESS_PAYMENT_ERRORS


USER_TEST_PHONE = '+254726767443'
BH_TEST_PHONE = '+25400000001'
CREATE_USER_PHONE = '+254000000009'
DEVICE_ID = 'AT01'


class at_handlerTestCase(TestCase):
    """
    tests
    """

    def setUp(self):
        gateway = Gateway.objects.create(
            name='SC01-+25400000001',
            telephone=BH_TEST_PHONE,
            device_id=DEVICE_ID,
            username='steama',
            password='steama1221',
            sms_type='sms',
            send_url='https://app.steama.co/sms/',
            receive_url='https://app.steama.co/all_msg_handler/')

        testSite = Mingi_Site.objects.create(
            name='Testing World',
            num_lines=3,
            equipment='stuff',
            date_online=datetime.datetime.now(),
            latitude=0.00,
            longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(
            telephone=BH_TEST_PHONE,
            harvester_id='TEST_ID0001',
            site=testSite,
            serial_number='',
            bh_type='SC01-+25400000001',
            input_gateway=gateway,
            output_gateway=gateway,
            backup_gateway_in=gateway,
            backup_gateway_out=gateway,
            approval_date=datetime.datetime.now())
        user = Mingi_User.objects.create(telephone=USER_TEST_PHONE,
                                         first_name='Emily',
                                         last_name='Moder', site=testSite,
                                         line_number="1,3", energy_price=100.00,
                                         line_status='3',
                                         bit_harvester=bit_harvester)
        utility = Utility.objects.create(name="Electricity:")
        for i in range(1, 4):
            if i == 1 or i == 3:
                this_user = user
            else:
                this_user = None
            line = Line.objects.create(site=testSite,
                                       number=i,
                                       user=this_user,
                                       line_status='3',
                                       is_connected=True,
                                       utility_type=utility)

        user.site_manager = user
        user.save()

    def test_locateUser_nonexistant_user(self):

        testPhone = CREATE_USER_PHONE
        self.assertRaises(UserNotFound, locateUser, testPhone)

    def test_locateUser_present_User(self):

        testPhone = USER_TEST_PHONE
        user = locateUser(testPhone)
        self.assertTrue(user != "No Match Found")
        self.assertEqual(user.first_name, 'Emily')
        self.assertEqual(user.last_name, 'Moder')

    def test_locateUser_present_BH(self):

        testPhone = BH_TEST_PHONE
        bh = locateUser(testPhone)
        self.assertTrue(bh != "No Match Found")
        self.assertEqual(bh.harvester_id, 'TEST_ID0001')

    def test_sendSMS_creates_ATOutput(self):

        msg = "This is a test"
        SMSTo = USER_TEST_PHONE
        user = Mingi_User.objects.get(telephone=SMSTo)

        msgLog = sendSMS(SMSTo, msg, user)
        self.assertEqual(msgLog.raw_message, msg)
        self.assertEqual(msgLog.user, user)
        output = Africas_Talking_Output.objects.get(
            raw_message="This is a test")
        self.assertEqual(SMSTo, output.user.telephone)

    def test_sendMenu(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        menuMsg = "Mingi Menu: Text M and the option number, eg 'm 1'.\n" \
                  "M 1:Balance\nM 2:Buy Power\nM 3:Useful numbers\n" \
                  "To see this menu any time text M to " + str(respond_to_no)

        # assert there is no menu message log before the send command executes?
        sendMenu(user)

        msgLog = Africas_Talking_Output.objects.get(raw_message=menuMsg)
        self.assertEqual(msgLog.user, user)

    def test_sendBalance(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.account_balance = 333.33
        user.save()

        msgOut = "Hi Emily, your energy balance is KSh 333.33. " \
                 "To see the Mingi Menu text M to " + str(respond_to_no) + "."

        sendBalance(user)

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)

    def test_sendBalance_negative(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.account_balance = -33.33
        user.save()

        msgOut = "Hi Emily, your energy balance is KSh 0.00. " \
                 "To see the Mingi Menu text M to " + str(respond_to_no) + "."

        sendBalance(user)

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)
        self.assertEqual(user.account_balance, -33.33)

    def test_sendBuyPower(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        msgOut = "To buy power, select PAYMENT SERVICES -> " \
                 "BUY GOODS on the MPesa menu. Enter till number: " \
                 "934135, and follow the instructions to top up!"

        sendBuyPower(user)

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)

    def test_processPhoneNo_fullValidNum(self):

        testNum = CREATE_USER_PHONE

        formattedNo = processPhoneNo(testNum)
        self.assertEqual(formattedNo, testNum)

    def test_processPhoneNo_shortNum(self):

        testNum = '0000000009'

        formattedNo = processPhoneNo(testNum)
        self.assertEqual(formattedNo, CREATE_USER_PHONE)

    def test_processPhoneNo_multipleInvalid(self):

        tooLong = '+2540726767443'

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has invalid number",
            processPhoneNo,
            tooLong)

        tooShort = '726767443'

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has invalid number",
            processPhoneNo,
            tooShort)

        badFormat = '254 0726767443'

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has invalid number",
            processPhoneNo,
            badFormat)

    def test_processPhoneNo_repeatedNumber(self):

        # verify that the user exists
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        repeatNo = USER_TEST_PHONE

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has repeated number",
            processPhoneNo,
            repeatNo)

    def test_findUserByLine_exists(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        siteName = user.site.name
        lineNo = 1
        user.line_number = lineNo
        user.save()

        match = findUserByLine(lineNo, siteName)
        self.assertEqual(user, match)

    def test_findUserByLine_DNE(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        site = user.site
        lineNo = 2

        self.assertRaises(Mingi_User.DoesNotExist,
                          findUserByLine, lineNo, site)

    def test_handleLineStatusConflict_SM_on(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()
        user.account_balance = 100.00
        user.line_control_party = 'SM'
        user.site_manager = user
        user.save()

        lineNo = 1
        handleLineStatusConflict(user, lineNo)

        msgOut = "Line 1 is logged as OFF but appears to be using power. " \
                 "Please make sure Line 1 is turned ON"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut,
                                                    user=user)

    def test_handleLineStatusConflict_SM_off(self):
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()
        user.account_balance = 0.00
        user.line_control_party = 'SM'
        user.site_manager = user
        user.save()

        lineNo = 1
        handleLineStatusConflict(user, lineNo)

        msgOut = "Line 1 is logged as OFF but appears to be using power. " \
                 "Please make sure Line 1 is turned OFF"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut,
                                                    user=user)

    def test_logProblem_instructions(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_site_manager = True
        user.save()

        msgIn = "8 - Power ran out"

        msgOut = "To report a problem, reply with 'M 8 - type of problem - " \
                 "equipment affected"

        logProblem(user, msgIn)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_logProblem_success(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_site_manager = True
        user.save()

        msgIn = "8 - Power ran out-Batteries"

        msgOut = "Thank you for reporting this problem. An access:energy " \
                 "technician will contact you shortly."

        logProblem(user, msgIn)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

        Error_Log.objects.get(problem_type="EQUIPMENT", creator=user.telephone,
                              problem_message="Power ran out: Batteries")

    def test_createNewUser_createsUser(self):

        registeredBy = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        registeredBy.site_manager = registeredBy
        registeredBy.save()

        site = registeredBy.site
        energyPrice = registeredBy.energy_price
        lineNo = 2

        response = createNewUser('test', 'user', CREATE_USER_PHONE, lineNo,
                                 'E', registeredBy)

        created = Mingi_User.objects.get(telephone=CREATE_USER_PHONE,
                                         first_name='test', last_name='user',
                                         line_number=lineNo, site=site,
                                         language='ENG',
                                         energy_price=energyPrice,
                                         site_manager=registeredBy)

        self.assertEqual(response, created)

        msgOut = "Hi, I'm Mingi from access:energy. Emily Moder wants to " \
                 "register you with access:energy. If your name is test " \
                 "user, reply 'M yes' to confirm"
        Africas_Talking_Output.objects.get(raw_message=msgOut, user=response)

    def test_createNewUser_invalidNo(self):

        invalidNo = '+2540726767443'
        registeredBy = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lineNo = 2

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has invalid number",
            createNewUser, 'ShouldNot', 'Exist', invalidNo, lineNo, 'E',
            registeredBy)

    def test_createNewUser_repeatNo(self):

        repeatNo = USER_TEST_PHONE

        registeredBy = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lineNo = 2

        self.assertRaisesRegexp(
            InvalidUserRegistration,
            "Invalid user registration!: User has repeated number",
            createNewUser, 'ShouldNot', 'Exist', repeatNo, lineNo, 'E',
            registeredBy)

    def test_registerNewUser_successful_response(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_field_manager = True
        user.save()

        msgIn = "6,Harrison,Leaf,+254718458563,2,E"
        registerNewUser(user, msgIn)

        msgOut = "Thank you for registering Harrison Leaf. A message has " \
                 "been sent to them, and you will be alerted when they " \
                 "confirm registration"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)

        newUser = Mingi_User.objects.get(first_name='Harrison',
                                         last_name='Leaf')

    def test_registerNewUser_repeat_user(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_field_manager = True
        user.save()

        msgIn = "6,ShouldNotExist,McDonald,%s,2,E" % USER_TEST_PHONE
        registerNewUser(user, msgIn)

        msgOut = "Error: this person is already registered with access:energy"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)

        self.assertFalse(
            Mingi_User.objects.filter(first_name="ShouldNotExist").exists())

    def test_register_invalid_number(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        invalidNo = '+2540726767443'
        user.is_field_manager = True
        user.save()

        msgIn = "6,ShouldNotExist,McDonald,%s,2,E" % invalidNo
        registerNewUser(user, msgIn)

        msgOut = "Error: the phone number you entered could not be " \
                 "recognised. Please try again with number format eg. " \
                 "0712345678"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut)
        self.assertEqual(msgLog.user, user)

        self.assertFalse(
            Mingi_User.objects.filter(first_name="ShouldNotExist").exists())

    def test_handleEnergyUse_account_balance_update(self):
        """
        Tests account balance update for different user control types
        """
        balance = 500

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = balance
        user.energy_price = 100.00
        user.control_type = "AUTOL"
        user.save()

        lineNo = 1
        NRGused = 1.00
        reading = 1.00

        now = datetime.datetime.now()

        # test user under AUTOL control - balance updated
        handleEnergyUse(user, NRGused, lineNo, reading, now)
        self.assertEqual(user.account_balance, 400.00)

        # test user under AUTOC control - balance updated
        user.control_type = "AUTOC"
        user.account_balance = balance
        user.save()

        handleEnergyUse(user, NRGused, lineNo, reading, now)
        self.assertEqual(user.account_balance, 400.00)

        # test user under MANUAL control - no balance update
        user.control_type = "MAN"
        user.account_balance = balance
        user.save()

        handleEnergyUse(user, NRGused, lineNo, reading, now)
        self.assertEqual(user.account_balance, 500.00)

    def test_handleEnergyUse_success_normal(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 500.00
        user.energy_price = 100.00
        NRGused = 1.00

        reading = 1.00
        lineNo = 1

        today = datetime.datetime.today()
        Line_State.objects.create(timestamp=today,
                                  site=user.site,
                                  number=lineNo,
                                  meter_reading=reading,
                                  account_balance=user.account_balance)

        now = datetime.datetime.now()

        handleEnergyUse(user, NRGused, lineNo, reading, now)

        self.assertEqual(user.account_balance, 400.00)

        Line_State.objects.get(timestamp=now,
                               site=user.site,
                               meter_reading=reading)

    def test_handleEnergyUse_success_lowBalance(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 150.00
        user.energy_price = 100.00
        user.low_balance_level = 75.00
        user.save()
        NRGused = 1.00

        reading = 2.00
        timestamp = datetime.datetime.now()
        lineNo = 3

        today = datetime.datetime.today()
        Line_State.objects.create(timestamp=today,
                                  site=user.site,
                                  number=lineNo,
                                  meter_reading=1.0,
                                  account_balance=user.account_balance)

        handleEnergyUse(user, NRGused, lineNo, reading, timestamp)

        self.assertEqual(user.account_balance, 50.00)
        self.assertEqual(user.low_balance_level, 50)

        Line_State.objects.get(timestamp=timestamp,
                               number=lineNo,
                               meter_reading=reading)

    def test_handleEnergyUse_HighEnergyUser_invalid(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 200.00
        user.energy_price = 100.00
        user.payment_plan = "High Energy User"
        user.site_manager = user
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()
        user.save()

        NRGused = 1.00

        lineNo = 3
        reading = 1.00
        timestamp = datetime.datetime.now()

        today = datetime.datetime.today()
        Line_State.objects.create(timestamp=today,
                                  site=user.site,
                                  number=lineNo,
                                  meter_reading=0.0,
                                  account_balance=user.account_balance)

        handleEnergyUse(user, NRGused, lineNo, reading, timestamp)

        self.assertEqual(user.account_balance, 100.00)

        msgOut = "Your energy balance is zero and your power will be turned off. Please top up your account to turn your power back on!"

        msgLog = Africas_Talking_Output.objects.filter(raw_message=msgOut,
                                                       user=user)
        self.assertEqual(len(msgLog), 0)

        Line_State.objects.get(timestamp=timestamp,
                               number=lineNo,
                               meter_reading=reading)

    def test_scanLogForErrors_none(self):

        site = Mingi_Site.objects.get(name="Testing World")
        msgElements = ["7", "0", "0", "1", "0", "0"]

        today = datetime.datetime.today()
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=1,
                                  meter_reading=0.00)
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=2,
                                  meter_reading=0.00)
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=3,
                                  meter_reading=0.00)

        response = scanLogForErrors(msgElements, site, False)

        self.assertEqual(response, "success")

    def test_scanLogForErrors_invalidLength(self):

        site = Mingi_Site.objects.get(name="Testing World")
        msgElements = ["7", "0", "0", "1", "0", "0", "0"]

        response = scanLogForErrors(msgElements, site, False)

        self.assertEqual(response, "invalid length")

    def test_scanLogForErrors_negativeUse(self):

        site = Mingi_Site.objects.get(name="Testing World")

        msgElements = ["7", "0", "0", "0.5", "0.75", "0"]

        today = datetime.datetime.today()
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=1,
                                  meter_reading=1.00)
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=2,
                                  meter_reading=1.00)
        Line_State.objects.create(timestamp=today,
                                  site=site,
                                  number=3,
                                  meter_reading=0.00)

        possibleErrors = scanLogForErrors(msgElements, site, False)

        self.assertEqual(len(possibleErrors), 2)
        self.assertEqual(possibleErrors[0][0], 1)
        self.assertEqual(possibleErrors[0][1], 0.5)
        self.assertEqual(possibleErrors[1][0], 2)
        self.assertEqual(possibleErrors[1][1], 0.75)

    def test_logMeterUpdate_possibleErrors(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 500.00
        site = user.site
        site.num_lines = 3
        user.save()
        site.save()

        lineNo = 1

        Line_State.objects.create(site=site, number=lineNo,
                                  meter_reading=1.00,
                                  account_balance=500.0,
                                  timestamp=datetime.datetime.today())

        msgIn = "7-0-0-0.5-0-0"
        now = timezone.now()
        unattachedLines = logMeterUpdate(user, msgIn, now)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        site = user.site
        self.assertEqual(unattachedLines, 3)
        self.assertEqual(user.account_balance, 500)

        msgOut = "I have found possible errors in your log. You logged:\nline 1 as 0.50\nIf this is correct reply 'm yes', otherwise please send a new log"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut,
                                                    user=user)

    def test_logMeterUpdate_invalidMsgFormat(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 500.00
        site = user.site
        site.num_lines = 3
        user.save()
        site.save()

        msgIn = "7-0-0-1-0"

        now = timezone.now()

        unattachedLines = logMeterUpdate(user, msgIn, now)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        site = user.site

        self.assertEqual(unattachedLines, 3)
        self.assertEqual(user.account_balance, 500)

        msgOut = "I'm sorry, you have entered an incorrect number of items. Please try again and make sure you log every line once."

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut,
                                                    user=user)

    def test_logLineStatus_incorrectFormat(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()

        msgIn = "9-1-off"

        logLineStatus(user, msgIn)

        self.assertEqual(user.line_is_on, True)

        msgOut = "To log a user's line status, reply 'M 9 - ON/OFF - line #"

        msgLog = Africas_Talking_Output.objects.get(raw_message=msgOut,
                                                    user=user)

    def test_logLineStatus_noCustomerFound(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()

        msgIn = "9-off-10"

        logLineStatus(user, msgIn)

        self.assertEqual(user.line_is_on, True)

    def test_respondToUser_notRecognized_ENG(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        msgIn = "Nonsense"

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_notRecognized_SWA(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.language = 'SWA'
        user.save()

        msgIn = "Nonsense"

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "Pole, sielewi. Kupata menu tumia 'M' kwa " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendBalance_notAUser(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.is_user = False
        user.save()

        msgIn = "1"

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendBalance(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.account_balance = 333.33
        user.language = 'SWA'
        user.save()

        respondToUser(user, "1", datetime.datetime.now())

        msgOut = "Hi Emily, akaunti yako ni KSh 333.33. Kupata menu tumia 'M' kwa " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendBuyPower_notAUser(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.is_user = False
        user.save()

        msgIn = "2"

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendBuyPower(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.language = 'SWA'
        user.save()

        respondToUser(user, "2", datetime.datetime.now())

        msgOut = "Kununua umeme, enda kwa MPesa menu, chagua PAYMENT SERVICES, enda kwa BUYGOODS. Jaza nambari 934135. Fuata maagizo kuongeza akaunti yako!"

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendNumbers(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.language = 'SWA'
        user.site_manager = user
        user.save()

        respondToUser(user, "3", datetime.datetime.now())

        msgOut = "Mwakilishi yako ni Emily, nambari ya simu +254726767443."

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_sendNumbers_notAUser(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.is_user = False
        user.save()

        msgIn = "3"

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to " + \
            str(respond_to_no)

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_respondToUser_registerNewUser_success(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_field_manager = True
        user.save()

        msgIn = "6,New,User,%s,33,E" % CREATE_USER_PHONE

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "Thank you for registering New User. A message has been sent to them, and you will be alerted when they confirm registration"
        Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        Africas_Talking_Output.objects.get(raw_message=msgOut)

    def test_respondToUser_registerNewUser_notFM(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        respond_to_no = get_respond_to_number()

        user.is_field_manager = False
        user.save()

        msgIn = "6,New,User,%s,33,E" % CREATE_USER_PHONE

        respondToUser(user, msgIn, datetime.datetime.now())

        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to " + \
            str(respond_to_no)

        self.assertRaises(Mingi_User.DoesNotExist,
                          Mingi_User.objects.get, telephone=CREATE_USER_PHONE)

    def test_logVoltage_success(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_agent = True
        testingWorld = user.site
        user.save()

        msgIn = "11 - 22.22"

        logSystemVoltage(user, msgIn, datetime.datetime.now())

        msgOut = "The battery level has been logged as 22.22, thanks!"

        Site_Record.objects.get(site=testingWorld, key="System Voltage")
        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_logVoltage_sendInstructions(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.is_agent = True
        user.save()

        msgIn = "10"

        logSystemVoltage(user, msgIn, datetime.datetime.now())

        msgOut = "To log the system voltage, reply with 'M 10' plus the voltage in this format: M 10 - XX.XX"

        Africas_Talking_Output.objects.get(raw_message=msgOut, user=user)

    def test_output_sms_has_gateway_object_and_has_right_recipient(self):
        """
        Tests that output message has valid gateway objects
        """

        msg = "Testing output message"
        SMSTo = USER_TEST_PHONE
        user = Mingi_User.objects.get(telephone=SMSTo)

        msgLog = sendSMS(SMSTo, msg, user)
        self.assertEqual(type(msgLog.gateway), Gateway)
        self.assertEqual(type(msgLog.user), Mingi_User)

    # FINISH CREATING TESTS FOR EACH SMS FUNCTION IN USERRESPONSE


class BH_functionsTestCase(TestCase):
    """
    test the bitHarvester SMS-processing functions
    """

    def setUp(self):
        testSite = Mingi_Site.objects.create(name='Testing World', num_lines=3,
                                             equipment='stuff',
                                             date_online=datetime.datetime.now(),
                                             latitude=0.00, longitude=0.00)
        user = Mingi_User.objects.create(telephone=USER_TEST_PHONE,
                                         first_name='Emily',
                                         last_name='Moder', site=testSite,
                                         line_number="1,3", energy_price=100.00,
                                         line_status='3',
                                         line_control_party="BH",
                                         meter_log_party="BH",
                                         account_balance=100.0)
        utility = Utility.objects.create(name="Electricity:")
        line1 = Line.objects.create(site=testSite,
                                    user=user,
                                    number=1,
                                    is_connected=True,
                                    line_status='3',
                                    utility_type=utility)
        line3 = Line.objects.create(site=testSite,
                                    user=user,
                                    number=3,
                                    is_connected=True,
                                    line_status='3',
                                    utility_type=utility)
        user2 = Mingi_User.objects.create(telephone=CREATE_USER_PHONE,
                                          first_name='Other',
                                          last_name='User', site=testSite,
                                          line_number="2", energy_price=100.00,
                                          line_status='3', site_manager=user,
                                          line_control_party="BH",
                                          meter_log_party="BH",
                                          account_balance=100.0)
        line2 = Line.objects.create(site=testSite,
                                    user=user2,
                                    number=2,
                                    is_connected=True,
                                    line_status='3',
                                    utility_type=utility)
        Bit_Harvester.objects.create(telephone=BH_TEST_PHONE,
                                     harvester_id='TEST_ID0001', site=testSite,
                                     serial_number='', bh_type='AT',
                                     approval_date=datetime.datetime.now())
        # 2 Rounds of Line State snapshots
        Line_State.objects.create(site=user.site, number=1,
                                  meter_reading=1.0, user=user,
                                  timestamp=datetime.datetime.fromtimestamp(1391573605))
        Line_State.objects.create(site=user.site, number=2,
                                  meter_reading=1.0, user=user2,
                                  timestamp=datetime.datetime.fromtimestamp(1391573605))
        Line_State.objects.create(site=user.site, number=3,
                                  meter_reading=1.0, user=user,
                                  timestamp=datetime.datetime.fromtimestamp(1391573605))
        Line_State.objects.create(site=user.site, number=1,
                                  meter_reading=0.0, user=user,
                                  timestamp=datetime.datetime.fromtimestamp(1391573505))
        Line_State.objects.create(site=user.site, number=2,
                                  meter_reading=0.0, user=user2,
                                  timestamp=datetime.datetime.fromtimestamp(1391573505))
        Line_State.objects.create(site=user.site, number=3,
                                  meter_reading=0.0, user=user,
                                  timestamp=datetime.datetime.fromtimestamp(1391573505))

        user.site_manager = user
        user.save()

    def test_logDataValues_noErrors(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,DATA,1.6,23702,23.5,21.8,1,3"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)

        response = logDataValues(msgElements, BH, bh_ts)

        self.assertEqual(response, "success")
        Site_Record.objects.get(timestamp=bh_ts, site=BH.site,
                                key="BH Data Log", value="23.5,21.8,1,3")

        response2 = logDataValues(msgElements, BH, bh_ts)
        self.assertEqual(response2, "repeated message")

        # ensure a repeat message is ignored
        Site_Record.objects.get(timestamp=bh_ts)

    def test_logDataValues_invalidLength(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,DATA,1.6,23702,23.5,21.8,1"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)

        response = logDataValues(msgElements, BH, bh_ts)

        self.assertEqual(response, "invalid length")
        self.assertRaises(Site_Record.DoesNotExist,
                          Site_Record.objects.get, timestamp=bh_ts)

    def test_receiveSystemData_logDataValues_correct(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,DATA,1.6,23702,23.5,21.8,1,3"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)
        now = timezone.now()

        receiveSystemData(msgElements, BH, now, bh_ts)

        Site_Record.objects.get(timestamp=bh_ts, site=BH.site,
                                key="BH Data Log", value="23.5,21.8,1,3")

    def test_receiveSystemData_logDataValues_invalidLength(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,DATA,1.6,23702,23.5,21.8,1"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)
        now = timezone.now()

        receiveSystemData(msgElements, BH, now, bh_ts)

        Error_Log.objects.get(
            problem_message__contains="Invalid length for DATA message")

    def test_processMeterReadings_correct(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,METERS,1,2000,2000,2000"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.fromtimestamp(1391574810)
        now = timezone.now()

        processMeterReadings(msgElements, BH, now, bh_ts)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        print str(user.line_is_on)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        print str(user2.line_is_on)

        self.assertEqual(user.account_balance, -100.0)
        self.assertEqual(user2.account_balance, 0.0)

        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=0.0, timestamp=bh_ts,
                               processed_timestamp=now)
        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=-100.0, timestamp=bh_ts,
                               processed_timestamp=now)
        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=0.0, timestamp=bh_ts,
                               processed_timestamp=now)

        Africas_Talking_Output.objects.get(raw_message__contains="OFF,1,2,3")

    def test_processModbusV4Message(self):
        print "modbus tests"

    def test_processMeterReadings_invalidSyntax(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,METERS"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.fromtimestamp(1391574810)
        now = timezone.now()

        processMeterReadings(msgElements, BH, now, bh_ts)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)

        self.assertEqual(user.account_balance, 100.0)
        self.assertEqual(user2.account_balance, 100.0)

        Error_Log.objects.get(problem_type="EXTERNAL")

    def test_receiveSystemData_processMeterReadings_success(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,METERS,1,2000,2000,2000"
        msgElements = msgIn.split(',')

        bh_ts = datetime.datetime.fromtimestamp(1391574810)
        now = timezone.now()

        receiveSystemData(msgElements, BH, now, bh_ts)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)

        self.assertEqual(user.account_balance, -100.0)
        self.assertEqual(user2.account_balance, 0.0)

        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=0.0, timestamp=bh_ts,
                               processed_timestamp=now)
        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=-100.0, timestamp=bh_ts,
                               processed_timestamp=now)
        Line_State.objects.get(user=user, meter_reading=2.0,
                               account_balance=0.0, timestamp=bh_ts,
                               processed_timestamp=now)

        Africas_Talking_Output.objects.get(raw_message__contains="OFF,1,2,3")

    def test_receiveSystemData_processStatusString_success(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msgIn = "*0,1391574810,10,STATUS,010"
        msgElements = msgIn.split(',')

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()
        lines2 = Line.objects.filter(user=user2)
        for line in lines2:
            line.line_status = '6'
            line.save()

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)
        now = timezone.now()

        receiveSystemData(msgElements, BH, now, bh_ts)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)

        self.assertEqual(user.line_is_on, False)
        self.assertEqual(user2.line_is_on, True)

    def test_lineStatusMaintenance_userNoChange_manual(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100
        user.line_number = '1'
        user.save()
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()

        for line in user.line_array:
            response = lineStatusMaintenance("1", user.site, line, user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        self.assertEqual(response, "NO CHANGE")
        self.assertEqual(user.line_is_on, True)

    def test_lineStatusMaintenance_subscriptionUser(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 0.0
        user.payment_plan = "Subscription Plan,100,14,0"
        user.line_number = '1'
        user.save()
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()

        for line in user.line_array:
            response = lineStatusMaintenance("1", user.site, line, user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        self.assertEqual(response, "NO CHANGE")
        self.assertEqual(user.line_is_on, True)

    def test_lineStatusMaintenance_userZeroBalance_manual(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 0
        user.save()

        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()

        for line in user.line_array:
            response = lineStatusMaintenance("1", user.site, line, user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        self.assertEqual(response, "OFF")
        self.assertEqual(user.line_is_on, True)

    def test_lineStatusMaintenance_userLineOn_HighEnergyPP(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = 100
        user.payment_plan = "High Energy User,100"
        user.save()

        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()

        for line in user.line_array:
            response = lineStatusMaintenance("0", user.site, line, user)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        self.assertEqual(response, "ON")
        self.assertEqual(user.line_is_on, False)

    def test_lineStatusMaintenance_noUser_HubLineOn(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        site = user.site
        site.hub_line_number = 2
        site.save()

        response = lineStatusMaintenance("1", site, site.hub_line_number)

        self.assertEqual(response, "NO CHANGE")

    def test_lineStatusMaintenance_noUser_powerStealing(self):

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        site = user.site
        line = Line.objects.get(site=site, number=2)
        line.is_connected = False
        line.save()

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        response = lineStatusMaintenance("1", site, 2)

        self.assertEqual(response, "OFF")
        Error_Log.objects.get(problem_type="EXTERNAL",
                              problem_message__contains="Unregistered line")

    def test_updateLineStatus_mismatchedControl_MAN_ON(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.line_number = "2"
        user.bit_harvester = BH
        user.account_balance = 100.0
        user.save()
        site = user.site
        line = Line.objects.get(site=site, number=2)
        line.line_status = "2"
        line.user = user
        line.save()

        response = updateLineStatus("2", "2", site, user)

        for log in Africas_Talking_Output.objects.all():
            print log.raw_message

        Error_Log.objects.get(
            problem_type="EXTERNAL", problem_message__contains="correction message sent")
        Africas_Talking_Output.objects.get(raw_message__contains="ON,2")

    def test_updateLineStatus_mismatchedControl_MAN_OFF(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.line_number = "2"
        user.account_balance = 0.0
        user.bit_harvester = BH
        user.save()
        site = user.site
        line = Line.objects.get(site=site, number=2)
        line.line_status = "4"
        line.user = user
        line.save()

        response = updateLineStatus("2", "4", site, user)
        print str(user.line_is_on)

        Error_Log.objects.get(
            problem_type="EXTERNAL", problem_message__contains="correction message sent")
        for log in Africas_Talking_Output.objects.all():
            print log.raw_message
        Africas_Talking_Output.objects.get(raw_message__contains="OFF,2")

    def test_lineStatusMaintenance_mismatchedControl_AUTO(self):

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.line_number = "2"
        user.control_type = "AUTOL"
        user.account_balance = 100.0
        user.bit_harvester = BH
        user.save()
        site = user.site
        line = Line.objects.get(site=site, number=2)
        line.line_status = "3"
        line.user = user
        line.save()

        response = updateLineStatus("2", "3", site, user)

        Error_Log.objects.get(
            problem_type="EXTERNAL", problem_message__contains="correction message sent")
        Africas_Talking_Output.objects.get(raw_message__contains="AUTO,2")

    def test_logLineStatusChange_linesOff_manual(self):

        ts = datetime.datetime.now()
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        for line in Line.objects.all():
            line.line_status = 3
            line.save()

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msg1 = "*1123,OFF,1,2,3"

        triggerMsg = Africas_Talking_Output.objects.create(raw_message=msg1,
                                                           bit_harvester=BH,
                                                           timestamp=ts)
        triggerElements = triggerMsg.raw_message.split(',')

        msg2 = "*1123,123456789,2,OK"
        msgElements = msg2.split(',')

        logLineStatusChange(triggerElements, BH)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        lines = Line.objects.filter(user__isnull=False)
        for line in lines:
            print line.line_status
        self.assertEqual(user.line_is_on, False)
        self.assertEqual(user2.line_is_on, False)

    def test_logLineStatusChange_noMatchingIndex(self):

        ts = datetime.datetime.now()
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '3'
            line.save()
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        lines2 = Line.objects.filter(user=user2)
        for line in lines2:
            line.line_status = '3'
            line.save()

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msg2 = "*1123,123456789,2,OK"
        msgElements = msg2.split(',')

        logLineStatusChange(msgElements, BH)

        Error_Log.objects.get(problem_type="INTERNAL")

    def test_logLineStatusChange_allLinesOn(self):

        ts = datetime.datetime.now()
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.line_control_party = "BH"
        user.save()
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        user2.line_control_party = "BH"
        user2.save()
        lines2 = Line.objects.filter(user=user2)
        for line in lines2:
            line.line_status = '6'
            line.save()

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        msg1 = "*1456,ON,ALL"

        triggerMsg = Africas_Talking_Output.objects.create(raw_message=msg1,
                                                           bit_harvester=BH,
                                                           timestamp=ts)
        triggerElements = triggerMsg.raw_message.split(',')

        msg2 = "*1456,1234566778899,2,OK"
        msgElements = msg2.split(',')

        logLineStatusChange(triggerElements, BH)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user2 = Mingi_User.objects.get(telephone=CREATE_USER_PHONE)
        self.assertEqual(user.line_is_on, True)
        self.assertEqual(user2.line_is_on, True)

    def test_logLineStatusChange_handle_no_lines(self):
        """
        Tests that the logLineStatusChange function handles no lines (empty
        string) by creating an Error_Log and exiting successfully.
        """

        ts = datetime.datetime.now()
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)

        triggerMsg = Africas_Talking_Output.objects.create(
            raw_message="*1456,ON,",
            bit_harvester=BH,
            timestamp=ts)
        triggerElements = triggerMsg.raw_message.split(',')

        logLineStatusChange(triggerElements, BH)

        # Check that the Error_Log was created
        self.assertEqual(Error_Log.objects.count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_message,
                         "A line switch command was sent with no line "
                         "numbers: '%s'" % triggerMsg.raw_message)

    def test_respondToBH_autoStatus_DATA_correct(self):

        msgIn = "*0,1391574810,10,DATA,1.6,23702,23.5,21.8,1,3"

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        bh_ts = datetime.datetime.utcfromtimestamp(
            1391574810) - datetime.timedelta(hours=14)

        respondToBH(BH, msgIn, timezone.now(), bh_ts)

        Site_Record.objects.get(site=BH.site, key="BH Data Log",
                                value="23.5,21.8,1,3")

    def test_respondToBH_autoStatus_STATUS_correct(self):

        msgIn = "*0,1391574810,10,STATUS,111"

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        user.account_balance = -10.00
        user.save()
        lines = Line.objects.filter(user=user)
        for line in lines:
            line.line_status = '6'
            line.save()

        bh_ts = datetime.datetime.fromtimestamp(1391574810)

        respondToBH(BH, msgIn, timezone.now(), bh_ts)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        self.assertEqual(user.line_is_on, True)

    def test_respondToBH_autoStatus_METERS_correct(self):

        msgIn = "*0,1391574810,10,METERS,1,1500,1500,1500"

        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)

        bh_ts = datetime.datetime.fromtimestamp(1391574810)

        Africas_Talking_Input.objects.create(
            raw_message=msgIn, bit_harvester=BH, timestamp=timezone.now(), third_party_timestamp=bh_ts)

        respondToBH(BH, msgIn, timezone.now(), bh_ts)

        ls_1 = Line_State.objects.filter(
            meter_reading=1.50, user=user, number=1)
        ls_3 = Line_State.objects.filter(
            meter_reading=1.50, user=user, number=3)

        self.assertTrue(ls_1.count() == 1)
        self.assertTrue(ls_3.count() == 1)

        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        self.assertEqual(float(user.account_balance), 0.0)

        # test no repeats
        Africas_Talking_Input.objects.create(
            raw_message=msgIn, bit_harvester=BH, timestamp=timezone.now(), third_party_timestamp=bh_ts)

        respondToBH(BH, msgIn, timezone.now(), bh_ts)

        ls_1 = Line_State.objects.filter(timestamp=bh_ts, number=1)
        ls_2 = Line_State.objects.filter(timestamp=bh_ts, number=2)
        ls_3 = Line_State.objects.filter(timestamp=bh_ts, number=3)

        self.assertTrue(ls_1.count() == 1)
        self.assertTrue(ls_2.count() == 1)
        self.assertTrue(ls_3.count() == 1)

    def test_respond_to_solo_Line_On(self):
        
        msgIn = "STATE IS ON"
    
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        BH.version = "SOLO"
        BH.save()
        
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        
        # set all line statuses to "OFF"
        for line in Line.objects.all():
            line.line_status = '6'
            line.save()

        respond_to_solo(BH, msgIn)

        for line in Line.objects.all():
            self.assertEqual(line.line_status, '3')

    def test_respond_to_solo_Line_Off(self):
        
        msgIn = "STATE IS OFF"
    
        BH = Bit_Harvester.objects.get(telephone=BH_TEST_PHONE)
        BH.version = "SOLO"
        BH.save()
        
        user = Mingi_User.objects.get(telephone=USER_TEST_PHONE)
        
        # set all line statuses to "ON"
        for line in Line.objects.all():
            line.line_status = '3'
            line.save()

        respond_to_solo(BH, msgIn)

        for line in Line.objects.all():
            self.assertEqual(line.line_status, '6')


class BitHarvesterV4StatusUpdateTests(TestCase):
    """
    Tests the BitHarvester v4 line status update handling functions.
    """

    BIT_HARVESTER_PHONE = '+254726767440'
    USER_1_PHONE = '+254726767441'
    USER_2_PHONE = '+254726767442'
    USER_3_PHONE = '+254726767443'
    USER_4_PHONE = '+254726767444'
    USER_5_PHONE = '+254726767445'
    USER_6_PHONE = '+254726767446'
    SITE_START_DATE = datetime.datetime(2000, 1, 1, tzinfo=pytz.UTC)

    def setUp(self):
        """
        Creates a test site with 6 lines (2 normal and 4 relay), and 6 users
        (one for each line). There is one line state for each line, reporting
        0 energy at 2000/1/1, 00:00:00. All users start with an account balance
        of 100.
        """
        test_site = Mingi_Site.objects.create(
            name='Testing World',
            num_lines=6,
            equipment='stuff',
            date_online=self.SITE_START_DATE,
            latitude=0.00,
            longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(
            telephone=self.BIT_HARVESTER_PHONE,
            harvester_id='TEST_ID0001',
            site=test_site,
            serial_number='',
            bh_type='AT',
            version='V4',
            approval_date=datetime.datetime.now())
        user_phones = [
            self.USER_1_PHONE,
            self.USER_2_PHONE,
            self.USER_3_PHONE,
            self.USER_4_PHONE,
            self.USER_5_PHONE,
            self.USER_6_PHONE
        ]
        utility = Utility.objects.create(name="Electricity:")
        for i in range(6):
            user = Mingi_User.objects.create(
                telephone=user_phones[i],
                first_name='User',
                last_name=str(i + 1),
                site=test_site,
                line_number=i + 1,
                energy_price=100.00,
                line_status='3',
                bit_harvester=bit_harvester,
                line_control_party="BH",
                meter_log_party="BH",
                control_type="AUTOC",
                account_balance=100.0)
            Line.objects.create(
                site=test_site,
                number=i + 1,
                user=user,
                is_connected=True,
                utility_type=utility,
                line_status='3')
        user_list = Mingi_User.objects.order_by('line_number')
        for i in range(6):
            Line_State.objects.create(
                site=test_site,
                number=i + 1,
                timestamp=self.SITE_START_DATE,
                processed_timestamp=self.SITE_START_DATE,
                meter_reading=0.0,
                account_balance=100.0,
                user=user_list[i]
            )

    def test_process_v4_status_update_normal_lines_on(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to the normal lines, with no users' lines
        turned off.

        No messages should be produced, and the users' lines should still be
        on after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "11",  # line on/off states
                "33"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_lines_off(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to the normal lines, with both users'
        lines turned off.

        No messages should be produced, and the users' lines should still be
        off after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        user1.account_balance = 0.0
        user1.save()
        line1 = Line.objects.get(user=user1)
        line1.line_status = '6'
        line1.save()
        user2.account_balance = 0.0
        user2.save()
        line2 = Line.objects.get(user=user2)
        line2.line_status = '6'
        line2.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "00",  # line on/off states
                "66"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            if user.telephone in [self.USER_1_PHONE, self.USER_2_PHONE]:
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_line_off_error(self):
        """
        Tests the process_v4_status_update function.

        Tests a status update to the normal lines, with line 1 erroneously
        turned off.

        A message should be sent to the BitHarvester requesting that line 1 be
        turned back on, and user 1's line should be registered as off.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "01",  # line on/off states
                "63"
            ],
            bit_harvester,
            None,
            None
        )
        for log in Error_Log.objects.all():
            print "ERROR: " + log.problem_message

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",ON,1" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        for user in Mingi_User.objects.all():
            if user.telephone == self.USER_1_PHONE:
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_line_2_off_errors(self):
        """
        Tests the process_v4_status_update function.

        Tests a status update to the normal lines, with lines 1 and 2
        erroneously turned off.

        A message should be sent to the BitHarvester requesting that lines 1
        and 2 be turned back on, and users 1 and 2's lines should be registered
        as off.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "00",  # line on/off states
                "66"
            ],
            bit_harvester,
            None,
            None
        )
        for log in Error_Log.objects.all():
            print "ERROR: " + log.problem_message

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)
        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",ON,1,2" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        for user in Mingi_User.objects.all():
            if user.telephone in [self.USER_1_PHONE, self.USER_2_PHONE]:
                print user.telephone
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_line_on_error(self):
        """
        Tests the process_v4_status_update function.

        Tests a status update to the normal lines, with line 2 erroneously
        turned on.

        A message should be sent to the BitHarvester requesting that line 2 be
        turned back off, and user 2's line should be registered as on.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        lines = Line.objects.filter(user=user2)
        for line in lines:
            line.line_status = '3'
            line.save()

        user2.account_balance = 0.0
        user2.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "11",  # line on/off states
                "33"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,2" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_line_both_errors(self):
        """
        Tests the process_v4_status_update function.

        Tests a status update to the normal lines, with line 1 erroneously
        turned on and line 2 erroneously turned off.

        A message should be sent to the BitHarvester requesting that line 1 be
        turned back off, and user 1's line should be registered as on. A
        message should additionally be send requesting that line 2 be turned
        back on, and user 2's line should be registered as off. The
        BitHarvester was really having a bad day with this one, I guess!

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)

        user1.account_balance = 0.0
        user1.save()

        line = Line.objects.get(site=user1.site, user=user1, number=1)
        line.line_status = '3'
        line.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "10",  # line on/off states
                "36"
            ],
            bit_harvester,
            None,
            None
        )

        for log in Error_Log.objects.all():
            print "ERROR: " + log.problem_message

        self.assertEqual(Africas_Talking_Output.objects.count(), 2)

        ato_on = Africas_Talking_Output.objects.filter(
            raw_message__contains=",ON,2")
        self.assertEqual(len(ato_on), 1)
        self.assertEqual(ato_on[0].bit_harvester, bit_harvester)
        self.assertEqual(ato_on[0].telephone, self.BIT_HARVESTER_PHONE)

        ato_on = Africas_Talking_Output.objects.filter(
            raw_message__contains=",OFF,1")
        self.assertEqual(len(ato_on), 1)
        self.assertEqual(ato_on[0].bit_harvester, bit_harvester)
        self.assertEqual(ato_on[0].telephone, self.BIT_HARVESTER_PHONE)

        for user in Mingi_User.objects.all():
            if user.telephone == self.USER_2_PHONE:
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_lines_on_1_only(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to normal line 1, with no users' lines
        turned off.

        No messages should be produced, and the users' lines should still be
        on after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """

        line = Line.objects.get(number=1)
        line.user.control_type = "AUTOL"
        line.user.save()
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "1",  # start line
                "1",  # line on/off states
                "0"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_normal_lines_on_2_only(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to normal line 2, with no users' lines
        turned off.

        No messages should be produced, and the users' lines should still be
        on after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "2",  # start line
                "1",  # line on/off states
                "3"
            ],
            bit_harvester,
            None,
            None
        )

        for log in Error_Log.objects.all():
            print log.problem_message

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_relay_lines_on(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to the relay lines, with no users' lines
        turned off.

        No messages should be produced, and the users' lines should still be
        on after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "A",  # start line
                "1111",  # line on/off states
                "3333"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_relay_lines_mix(self):
        """
        Tests the process_v4_status_update function.

        Tests a correct status update to the relay lines, with users 3 and 5's
        lines turned off.

        No messages should be produced, and the users' lines should still have
        the same states after the status update is processed.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)

        user3.account_balance = 0.0
        user3.save()
        line3 = Line.objects.get(user=user3)
        line3.line_status = '6'
        line3.save()

        line5 = Line.objects.get(user=user5)
        line5.line_status = '6'
        line5.save()
        user5.account_balance = 0.0
        user5.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "A",  # start line
                "0101",  # line on/off states
                "6363"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        for user in Mingi_User.objects.all():
            if user.telephone in [self.USER_3_PHONE, self.USER_5_PHONE]:
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)

    def test_process_v4_status_update_relay_lines_both_errors(self):
        """
        Tests the process_v4_status_update function.

        Tests a complicated incorrect status update to the relay lines. Users
        3 and 5's lines (A and C) should be off, with the rest on. The
        BitHarvester, however, reports lines A and B off, with the rest on.

        Messages should be produced requesting the BitHarvester to turn line B
        on and line C off, and users 4 and 5's statuses should be updated to
        be off and on, respectively.

        note: The "now" and "bh_ts" arguments are not used in
        process_v4_status_update, and are therefore set to None. If they are
        used in the future, this test will have to be updated.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)

        user3.account_balance = 0.0
        user3.save()
        line3 = Line.objects.get(user=user3)
        line3.line_status = '6'
        line3.save()

        line5 = Line.objects.get(user=user5)
        line5.line_status = '6'
        line5.save()
        user5.account_balance = 0.0
        user5.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        process_v4_status_update(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "STATUSN",
                "A",  # start line
                "0011",  # line on/off states
                "6633"
            ],
            bit_harvester,
            None,
            None
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 2)

        ato_on = Africas_Talking_Output.objects.filter(
            raw_message__contains=",ON,B")
        self.assertEqual(len(ato_on), 1)
        self.assertEqual(ato_on[0].bit_harvester, bit_harvester)
        self.assertEqual(ato_on[0].telephone, self.BIT_HARVESTER_PHONE)

        ato_off = Africas_Talking_Output.objects.filter(
            raw_message__contains=",OFF,C")
        self.assertEqual(len(ato_off), 1)
        self.assertEqual(ato_on[0].bit_harvester, bit_harvester)
        self.assertEqual(ato_on[0].telephone, self.BIT_HARVESTER_PHONE)

        for user in Mingi_User.objects.all():
            if user.telephone in [self.USER_3_PHONE, self.USER_4_PHONE]:
                self.assertFalse(user.line_is_on)
            else:
                self.assertTrue(user.line_is_on)


class BitHarvesterV4MeterReadingTests(TestCase):
    """
    Tests the BitHarvester v4 meter reading handling functions.
    """

    BIT_HARVESTER_PHONE = '+254726767440'
    USER_1_PHONE = '+254726767441'
    USER_2_PHONE = '+254726767442'
    USER_3_PHONE = '+254726767443'
    USER_4_PHONE = '+254726767444'
    USER_5_PHONE = '+254726767445'
    USER_6_PHONE = '+254726767446'
    SITE_START_DATE = datetime.datetime(2000, 1, 1, tzinfo=pytz.UTC)

    def setUp(self):
        """
        Creates a test site with 6 lines (2 normal and 4 relay), and 6 users
        (one for each line). There is one line state for each line, reporting
        0 energy at 2000/1/1, 00:00:00. All users start with an account balance
        of 100.
        """
        test_site = Mingi_Site.objects.create(
            name='Testing World',
            num_lines=6,
            equipment='stuff',
            date_online=self.SITE_START_DATE,
            latitude=0.00,
            longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(
            telephone=self.BIT_HARVESTER_PHONE,
            harvester_id='TEST_ID0001',
            site=test_site,
            serial_number='',
            bh_type='AT',
            version='V4',
            approval_date=datetime.datetime.now())
        user_phones = [
            self.USER_1_PHONE,
            self.USER_2_PHONE,
            self.USER_3_PHONE,
            self.USER_4_PHONE,
            self.USER_5_PHONE,
            self.USER_6_PHONE
        ]
        electricity = Utility.objects.create(name="Electricity:")
        for i in range(6):
            user = Mingi_User.objects.create(
                telephone=user_phones[i],
                first_name='User',
                last_name=str(i + 1),
                site=test_site,
                line_number=i + 1,
                energy_price=100.00,
                line_status='3',
                bit_harvester=bit_harvester,
                line_control_party="BH",
                meter_log_party="BH",
                account_balance=100.0)
            line_obj = Line.objects.create(
                number=i + 1,
                site=test_site,
                user=user,
                line_status='3',
                utility_type=electricity)
            line_obj.reset_name()
        user_list = Mingi_User.objects.order_by('line_number')
        for i in range(6):
            Line_State.objects.create(
                site=test_site,
                number=i + 1,
                timestamp=self.SITE_START_DATE,
                processed_timestamp=self.SITE_START_DATE,
                meter_reading=0.0,
                account_balance=100.0,
                user=user_list[i]
            )

    def test_process_meter_readings_normal_small(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the normal lines with a small amount of energy
        usage. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        No messages should be produced, and the users' lines should still be
        on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "100",
                "200"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 8)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]
        ls_2 = line_states[1]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 0.1)
        self.assertEqual(ls_1.account_balance, 90.00)
        self.assertEqual(ls_1.user, user1)

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 0.2)
        self.assertEqual(ls_2.account_balance, 80.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_one_moderate(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the normal lines with line 2 having a moderate
        amount of energy usage, which brings its user's account balance below
        75. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        One message should be produced, warning the user that their balance is
        low, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "100",
                "300"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        warn_str = "Your balance is getting low! You only have KSh 70.00" \
                   " left. Top up your account now to keep your power on!"
        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.user, user2)
        self.assertEqual(ato.raw_message, warn_str)
        self.assertEqual(ato.telephone, self.USER_2_PHONE)

        self.assertEqual(Line_State.objects.count(), 8)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]
        ls_2 = line_states[1]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 0.1)
        self.assertEqual(ls_1.account_balance, 90.00)
        self.assertEqual(ls_1.user, user1)

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 0.3)
        self.assertEqual(ls_2.account_balance, 70.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_one_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the normal lines with line 1 having a large amount
        of energy usage, which brings its user's account balance below zero.
        The update is for 2000/1/2 00:00:00, or one day after the last update.

        One message should be produced, telling the BitHarvester to turn line 1
        off, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "1100",
                "200"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,1" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 8)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]
        ls_2 = line_states[1]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 1.1)
        self.assertEqual(ls_1.account_balance, -10.00)
        self.assertEqual(ls_1.user, user1)

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 0.2)
        self.assertEqual(ls_2.account_balance, 80.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_two_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the normal lines with both lines having a large
        amount of energy usage, which bring their users' account balances below
        zero. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        One message should be produced, telling the BitHarvester to turn lines
        1 and 2 off, and the users' lines should still be on after the meter
        reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "1100",
                "2000"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,1,2" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 8)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]
        ls_2 = line_states[1]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 1.1)
        self.assertEqual(ls_1.account_balance, -10.00)
        self.assertEqual(ls_1.user, user1)

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 2.0)
        self.assertEqual(ls_2.account_balance, -100.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_line_1_small(self):
        """
        Tests the processMeterReadings function.

        Tests an update to normal line 1 with a small amount of energy usage.
        The update is for 2000/1/2 00:00:00, or one day after the last update.

        No messages should be produced, and the users' lines should still be
        on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 7)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 0.1)
        self.assertEqual(ls_1.account_balance, 90.00)
        self.assertEqual(ls_1.user, user1)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_line_2_small(self):
        """
        Tests the processMeterReadings function.

        Tests an update to normal line 2 with a small amount of energy usage.
        The update is for 2000/1/2 00:00:00, or one day after the last update.

        No messages should be produced, and the users' lines should still be
        on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "2",  # start line
                "200"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 7)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_2 = line_states[0]

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 0.2)
        self.assertEqual(ls_2.account_balance, 80.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_line_1_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to just normal line 1 with a large amount of energy
        usage, which brings its user's account balance below zero. The update
        is for 2000/1/2 00:00:00, or one day after the last update.

        One message should be produced, telling the BitHarvester to turn line 1
        off, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user1 = Mingi_User.objects.get(telephone=self.USER_1_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "1",  # start line
                "1100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        print ato.raw_message
        self.assertTrue(",OFF,1" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 7)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_1 = line_states[0]

        self.assertEqual(ls_1.number, 1)
        self.assertEqual(ls_1.meter_reading, 1.1)
        self.assertEqual(ls_1.account_balance, -10.00)
        self.assertEqual(ls_1.user, user1)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_normal_line_2_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to just normal line 2 with a large amount of energy
        usage, which brings its user's account balance below zero. The update
        is for 2000/1/2 00:00:00, or one day after the last update.

        One message should be produced, telling the BitHarvester to turn line 2
        off, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user2 = Mingi_User.objects.get(telephone=self.USER_2_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "2",  # start line
                "2000"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,2" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 7)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_2 = line_states[0]

        self.assertEqual(ls_2.number, 2)
        self.assertEqual(ls_2.meter_reading, 2.0)
        self.assertEqual(ls_2.account_balance, -100.00)
        self.assertEqual(ls_2.user, user2)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_relay_small(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines with a small amount of energy
        usage. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        No messages should be produced, and the users' lines should still be
        on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "50",
                "100",
                "150",
                "200",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.05)
        self.assertEqual(ls_a.account_balance, 95.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.1)
        self.assertEqual(ls_b.account_balance, 90.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.15)
        self.assertEqual(ls_c.account_balance, 85.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.2)
        self.assertEqual(ls_d.account_balance, 80.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_relay_one_moderate(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines with line A having a moderate
        amount of energy usage, which brings its user's account balance below
        75. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        One message should be produced, warning the user that their balance is
        low, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "300",
                "100",
                "150",
                "200",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        warn_str = "Your balance is getting low! You only have KSh 70.00" \
                   " left. Top up your account now to keep your power on!"
        ato = Africas_Talking_Output.objects.get()
        self.assertEqual(ato.user, user3)
        self.assertEqual(ato.raw_message, warn_str)
        self.assertEqual(ato.telephone, self.USER_3_PHONE)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.3)
        self.assertEqual(ls_a.account_balance, 70.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.1)
        self.assertEqual(ls_b.account_balance, 90.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.15)
        self.assertEqual(ls_c.account_balance, 85.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.2)
        self.assertEqual(ls_d.account_balance, 80.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_relay_one_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines with line B having a large amount
        of energy usage, which brings its user's account balance below zero.
        The update is for 2000/1/2 00:00:00, or one day after the last update.

        One message should be produced, telling the BitHarvester to turn line B
        off, and the users' lines should still be on after the meter reading is
        processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        print "LINES: " + str(bit_harvester.site.num_lines)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "50",
                "1100",
                "150",
                "200",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        print ato.raw_message
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,B" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.05)
        self.assertEqual(ls_a.account_balance, 95.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 1.1)
        self.assertEqual(ls_b.account_balance, -10.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.15)
        self.assertEqual(ls_c.account_balance, 85.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.2)
        self.assertEqual(ls_d.account_balance, 80.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_relay_two_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines with lines A and D having a large
        amount of energy usage, which bring their users' account balances below
        zero. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        One message should be produced, telling the BitHarvester to turn lines
        B and D off, and the users' lines should still be on after the meter
        reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "1100",
                "100",
                "150",
                "2000",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 1)

        ato = Africas_Talking_Output.objects.get()
        print ato.raw_message
        self.assertEqual(ato.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,A,D" in ato.raw_message)
        self.assertEqual(ato.telephone, self.BIT_HARVESTER_PHONE)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 1.1)
        self.assertEqual(ls_a.account_balance, -10.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.1)
        self.assertEqual(ls_b.account_balance, 90.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.15)
        self.assertEqual(ls_c.account_balance, 85.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 2.0)
        self.assertEqual(ls_d.account_balance, -100.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_relay_one_moderate_one_large(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines with line A having a moderate
        amount of energy usage, which brings its user's account balance below
        75. Additionally, line C has a large amount of energy usage, which
        brings its user's account balance below zero. The update is for
        2000/1/2 00:00:00, or one day after the last update.

        Two messages should be produced, one warning user 3 that their balance
        is low, and the other instructing the BitHarvester to turn off line C.
        Users' lines should still be on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "300",
                "100",
                "1500",
                "200",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 2)

        warn_str = "Your balance is getting low! You only have KSh 70.00" \
                   " left. Top up your account now to keep your power on!"
        ato_warn = Africas_Talking_Output.objects.get(
            telephone=self.USER_3_PHONE)
        self.assertEqual(ato_warn.user, user3)
        self.assertEqual(ato_warn.raw_message, warn_str)

        ato_off = Africas_Talking_Output.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        self.assertEqual(ato_off.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,C" in ato_off.raw_message)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.3)
        self.assertEqual(ls_a.account_balance, 70.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.1)
        self.assertEqual(ls_b.account_balance, 90.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 1.5)
        self.assertEqual(ls_c.account_balance, -50.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.2)
        self.assertEqual(ls_d.account_balance, 80.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_nonsequential_updates(self):
        """
        Tests the processMeterReadings function.

        Tests an update to the relay lines where meter reading updates from
        the bitHarvester are received out-of-order: 1 then 2 then 3 where
        timestamp1 < timestamp3 < timestamp2.

        All Line_State objects should be created correctly, but account balances
        should only be edited for messages received sequentially.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        update_time_2 = self.SITE_START_DATE + datetime.timedelta(hours=12)

        # first, process the latest update_time update
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "300",
                "100",
                "1500",
                "200",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 2)

        warn_str = "Your balance is getting low! You only have KSh 70.00" \
                   " left. Top up your account now to keep your power on!"
        ato_warn = Africas_Talking_Output.objects.get(
            telephone=self.USER_3_PHONE)
        self.assertEqual(ato_warn.user, user3)
        self.assertEqual(ato_warn.raw_message, warn_str)

        ato_off = Africas_Talking_Output.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        self.assertEqual(ato_off.bit_harvester, bit_harvester)
        self.assertTrue(",OFF,C" in ato_off.raw_message)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.3)
        self.assertEqual(ls_a.account_balance, 70.00)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.1)
        self.assertEqual(ls_b.account_balance, 90.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 1.5)
        self.assertEqual(ls_c.account_balance, -50.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.2)
        self.assertEqual(ls_d.account_balance, 80.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

        # process an earlier update
        processMeterReadings(
            [
                "*0",
                update_time_2.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "200",
                "50",
                "1000",
                "150",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time_2,
            update_time_2
        )

        # all messaging and accounts should have stayed the same from before
        for msg in Africas_Talking_Output.objects.all():
            print msg.raw_message
        self.assertEqual(Africas_Talking_Output.objects.count(), 3)

        self.assertEqual(Line_State.objects.count(), 14)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        for state in line_states:
            print str(state.number) + ": " + str(state.timestamp)
        ls_a = line_states[4]
        ls_b = line_states[5]
        ls_c = line_states[6]
        ls_d = line_states[7]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 0.2)
        self.assertEqual(ls_a.account_balance, None)
        self.assertEqual(ls_a.user, user3)
        self.assertEqual(ls_a.user.account_balance, 70.0)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.05)
        self.assertEqual(ls_b.account_balance, None)
        self.assertEqual(ls_b.user, user4)
        self.assertEqual(ls_b.user.account_balance, 90.0)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 1.0)
        self.assertEqual(ls_c.account_balance, None)
        self.assertEqual(ls_c.user, user5)
        self.assertEqual(ls_c.user.account_balance, -50.00)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.15)
        self.assertEqual(ls_d.account_balance, None)
        self.assertEqual(ls_d.user, user6)
        self.assertEqual(ls_d.user.account_balance, 80.00)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    @unittest.skip("We don't handle partial relay line meter readings")
    def test_process_meter_readings_relay_lines_bc_small(self):
        """
        Tests the processMeterReadings function.

        Tests an update to relay lines B and C with a small amount of energy
        usage. The update is for 2000/1/2 00:00:00, or one day after the last
        update.

        No messages should be produced, and the users' lines should still be
        on after the meter reading is processed.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "B",  # start line
                "200",
                "100",
                "100",  # begin pulse ages (not used)
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 8)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_b = line_states[0]
        ls_c = line_states[1]

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.2)
        self.assertEqual(ls_b.account_balance, 80.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.1)
        self.assertEqual(ls_c.account_balance, 90.00)
        self.assertEqual(ls_c.user, user5)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_site_and_user_pulse_ratios(self):
        """
        Tests the processMeterReadings function.

        Tests an update to relay lines A-D with a small amount of energy
        usage. The update is for 2000/1/2 00:00:00, or one day after the last
        update. The site has a pulse ratio of 5.0, and line B has a pulse ratio
        of 1.0

        Low balance warning messages should go out to Users 5, 6 (Line C,D).
        An off command should go out for Line A. All lines should still be
        on after the meter reading is processed. Users A,C,D should have the 
        site's pulse ratio applied to the line snapshot, while User B's pulse 
        ratio should override the registered site ratio.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        # set pulse ratios
        site = bit_harvester.site
        site.pulse_ratio = 5.0
        site.save()

        user4.pulse_ratio = 1.0
        user4.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "200",
                "200",
                "100",
                "100",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 3)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 1.0)
        self.assertEqual(ls_a.account_balance, 0.0)
        self.assertEqual(ls_a.user, user3)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.2)
        self.assertEqual(ls_b.account_balance, 80.00)
        self.assertEqual(ls_b.user, user4)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.5)
        self.assertEqual(ls_c.account_balance, 50.00)
        self.assertEqual(ls_c.user, user5)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.5)
        self.assertEqual(ls_d.account_balance, 50.00)
        self.assertEqual(ls_d.user, user6)

        for user in Mingi_User.objects.all():
            self.assertTrue(user.line_is_on)

    def test_process_meter_readings_site_and_line_pulse_ratios(self):
        """
        Tests the processMeterReadings function.

        Tests an update to relay lines A-D with a small amount of energy
        usage. The update is for 2000/1/2 00:00:00, or one day after the last
        update. The site has a pulse ratio of 5.0, and line B has a pulse ratio
        of 1.0

        Line A-D have no users attached, and therefore no commands or warnings
        should be sent. All lines should still be
        on after the meter reading is processed. Lines A,C,D should have the 
        site's pulse ratio applied to the line snapshot, while Line B's pulse 
        ratio should override the registered site ratio.
        """
        bit_harvester = Bit_Harvester.objects.get(
            telephone=self.BIT_HARVESTER_PHONE)
        user3 = Mingi_User.objects.get(telephone=self.USER_3_PHONE)
        user4 = Mingi_User.objects.get(telephone=self.USER_4_PHONE)
        user5 = Mingi_User.objects.get(telephone=self.USER_5_PHONE)
        user6 = Mingi_User.objects.get(telephone=self.USER_6_PHONE)

        # set pulse ratios
        site = bit_harvester.site
        site.pulse_ratio = 5.0
        site.save()

        lineA = Line.objects.get(site=site, number=3)
        lineB = Line.objects.get(site=site, number=4)
        lineC = Line.objects.get(site=site, number=5)
        lineD = Line.objects.get(site=site, number=6)

        # remove users from lines
        user3.line_number = ''
        user3.save()
        user4.line_number = ''
        user4.save()
        user5.line_number = ''
        user5.save()
        user6.line_number = ''
        user6.save()

        lineA.user = None
        lineA.save()
        lineB.user = None
        lineB.pulse_ratio = 1.0
        lineB.save()
        lineC.user = None
        lineC.save()
        lineD.user = None
        lineD.save()

        update_time = self.SITE_START_DATE + datetime.timedelta(days=1)
        processMeterReadings(
            [
                "*0",
                update_time.strftime("%s"),
                "0",
                "METERS",
                "A",  # start line
                "200",
                "200",
                "100",
                "100",
                "100",  # begin pulse ages (not used)
                "100",
                "100",
                "100"
            ],
            bit_harvester,
            update_time,
            update_time
        )

        self.assertEqual(Africas_Talking_Output.objects.count(), 0)

        self.assertEqual(Line_State.objects.count(), 10)

        line_states = Line_State.objects.order_by('-timestamp', 'number')
        ls_a = line_states[0]
        ls_b = line_states[1]
        ls_c = line_states[2]
        ls_d = line_states[3]

        self.assertEqual(ls_a.number, 3)
        self.assertEqual(ls_a.meter_reading, 1.0)
        self.assertEqual(ls_a.user, None)

        self.assertEqual(ls_b.number, 4)
        self.assertEqual(ls_b.meter_reading, 0.2)
        self.assertEqual(ls_b.user, None)

        self.assertEqual(ls_c.number, 5)
        self.assertEqual(ls_c.meter_reading, 0.5)
        self.assertEqual(ls_c.user, None)

        self.assertEqual(ls_d.number, 6)
        self.assertEqual(ls_d.meter_reading, 0.5)
        self.assertEqual(ls_d.user, None)

        for line in Line.objects.all():
            self.assertEqual(line.line_status, '3')


class BitHarvesterV4CreditReportTests(TestCase):
    """
    Tests the BitHarvester v4 credit report handling functions.
    """

    BIT_HARVESTER_PHONE = '+254726767440'
    USER_1_PHONE = '+254726767441'
    USER_2_PHONE = '+254726767442'
    USER_3_PHONE = '+254726767443'
    USER_4_PHONE = '+254726767444'
    USER_5_PHONE = '+254726767445'
    USER_6_PHONE = '+254726767446'
    SITE_START_DATE = datetime.datetime(2000, 1, 1, tzinfo=pytz.UTC)

    def setUp(self):
        """
        Creates a test site with 6 lines (2 normal and 4 relay), and 6 users
        (one for each line). There is one line state for each line, reporting
        0 energy at 2000/1/1, 00:00:00. All users start with an account balance
        of 100.
        """
        test_site = Mingi_Site.objects.create(
            name='Testing World',
            num_lines=6,
            equipment='stuff',
            date_online=self.SITE_START_DATE,
            latitude=0.00,
            longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(
            telephone=self.BIT_HARVESTER_PHONE,
            harvester_id='TEST_ID0001',
            site=test_site,
            serial_number='',
            bh_type='AT',
            approval_date=datetime.datetime.now())
        user_phones = [
            self.USER_1_PHONE,
            self.USER_2_PHONE,
            self.USER_3_PHONE,
            self.USER_4_PHONE,
            self.USER_5_PHONE,
            self.USER_6_PHONE
        ]
        for i in range(6):
            Mingi_User.objects.create(
                telephone=user_phones[i],
                first_name='User',
                last_name=str(i + 1),
                site=test_site,
                line_number=i + 1,
                energy_price=100.00,
                line_status='3',
                bit_harvester=bit_harvester,
                line_control_party="BH",
                meter_log_party="BH",
                account_balance=100.0)
        user_list = Mingi_User.objects.order_by('line_number')
        for i in range(6):
            Line_State.objects.create(
                site=test_site,
                number=i + 1,
                timestamp=self.SITE_START_DATE,
                processed_timestamp=self.SITE_START_DATE,
                meter_reading=0.0,
                account_balance=100.0,
                user=user_list[i]
            )


class BitHarvesterV4ModbusTests(TestCase):
    """
    Tests the BitHarvester v4 credit report handling functions.
    """

    BIT_HARVESTER_PHONE = '+254726767440'
    USER_1_PHONE = '+254726767441'
    USER_2_PHONE = '+254726767442'
    USER_3_PHONE = '+254726767443'
    USER_4_PHONE = '+254726767444'
    USER_5_PHONE = '+254726767445'
    USER_6_PHONE = '+254726767446'
    SITE_START_DATE = datetime.datetime(2000, 1, 1, tzinfo=pytz.UTC)

    def setUp(self):
        """
        Creates a test site with 6 lines (2 normal and 4 relay), and 6 users
        (one for each line). There is one line state for each line, reporting
        0 energy at 2000/1/1, 00:00:00. All users start with an account balance
        of 100.
        """
        test_site = Mingi_Site.objects.create(
            name='Testing World',
            num_lines=10,
            num_modbus_lines=4,
            equipment='stuff',
            date_online=self.SITE_START_DATE,
            latitude=0.00,
            longitude=0.00)
        bit_harvester = Bit_Harvester.objects.create(
            telephone=self.BIT_HARVESTER_PHONE,
            harvester_id='TEST_ID0001',
            site=test_site,
            serial_number='',
            bh_type='AT',
            approval_date=datetime.datetime.now())
        user_phones = [
            self.USER_1_PHONE,
            self.USER_2_PHONE,
            self.USER_3_PHONE,
            self.USER_4_PHONE,
            self.USER_5_PHONE,
            self.USER_6_PHONE
        ]
        utility = Utility.objects.create(name="Electricity:")
        for i in range(6):
            Mingi_User.objects.create(
                telephone=user_phones[i],
                first_name='User',
                last_name=str(i + 1),
                site=test_site,
                line_number=i + 1,
                energy_price=100.00,
                line_status='3',
                bit_harvester=bit_harvester,
                line_control_party="BH",
                meter_log_party="BH",
                account_balance=100.0)
        user_query_list = Mingi_User.objects.order_by('line_number')
        user_list = list()
        for user in user_query_list:
            user_list.append(user)
        for i in range(4):
            user_list.append(None)
        for i in range(10):
            Line_State.objects.create(
                site=test_site,
                number=i + 1,
                timestamp=self.SITE_START_DATE,
                processed_timestamp=self.SITE_START_DATE,
                meter_reading=0.0,
                account_balance=100.0,
                user=user_list[i]
            )
            Line.objects.create(site=test_site,
                                number=i + 1,
                                user=user_list[i],
                                line_status='3',
                                is_connected=True,
                                utility_type=utility)

    def testNoneModbusLinesIgnorePULSEMsgs(self):
        """
        ensure that PULSES msgs received for a site with no modbus lines 
        registered will ignore the messages
        """

        site = Mingi_Site.objects.get(name="Testing World")
        site.num_modbus_lines = None
        site.save()

        msgIn = "*0,143756487,0,PULSES,@1,1,0,2,0,3,0,4,0"
        msgElements = msgIn.split(',')
        BH = Bit_Harvester.objects.get(site=site)
        ts = datetime.datetime.now()

        response = receiveSystemData(msgElements, BH, ts, ts)

        for ls in Line_State.objects.all():
            print str(ls)
        self.assertEqual(len(Line_State.objects.all()), 10)
        self.assertEqual(len(Line_State.objects.filter(timestamp=ts)), 0)

    def testModbusLineStateCreated_normal(self):
        """
        ensure line states are created normally if modbus lines are registered
        correctly after a PULESES message
        """

        site = Mingi_Site.objects.get(name="Testing World")

        msgIn = "*0,143756487,0,PULSES,@1,1,0,2,0,3,0,4,0"
        msgElements = msgIn.split(',')
        BH = Bit_Harvester.objects.get(site=site)
        ts = datetime.datetime.now()

        self.assertEqual(len(Line_State.objects.all()), 10)

        response = receiveSystemData(msgElements, BH, ts, ts)

        for ls in Line_State.objects.all():
            print str(ls)
        self.assertEqual(len(Line_State.objects.all()), 14)
        self.assertEqual(len(Line_State.objects.filter(timestamp=ts)), 4)

        for ls in Line_State.objects.filter(timestamp=ts):
            term1 = ls.number - 6
            term2 = ls.meter_reading * 1000
            self.assertEqual(term1, term2)

    def testModbusLineSwitching_noModbus(self):
        """
        test line switching when modbus lines are NOT present 
        (no adjustment to line numbers for BH mapping)
        """

        site = Mingi_Site.objects.get(name="Testing World")
        site.num_modbus_lines = None
        site.save()

        print str(site.num_modbus_lines)

        BH = Bit_Harvester.objects.get(site=site)
        linesOn = [1, 2, 10]
        linesOff = [3, 4, 9]

        send_line_switching_msgs(BH, linesOn, linesOff)
        msg1 = "ON,1,2,10"
        msg2 = "OFF,3,4,9"

        print str(len(Africas_Talking_Output.objects.all()))
        for msg in Africas_Talking_Output.objects.all():
            print msg.raw_message

        Africas_Talking_Output.objects.get(raw_message__contains=msg1)
        Africas_Talking_Output.objects.get(raw_message__contains=msg2)

    def testModbusLineSwitching_normal(self):
        """
        test line switching when modbus lines are present
        (adjustment made to line numbers for BH mapping)
        """

        site = Mingi_Site.objects.get(name="Testing World")
        site.num_modbus_lines = 4
        site.save()

        BH = Bit_Harvester.objects.get(site=site)
        linesOn = [1, 2, 10]
        linesOff = [3, 4, 9]

        send_line_switching_msgs(BH, linesOn, linesOff)
        msg1 = "ON,1,2,@4"
        msg2 = "OFF,3,4,@3"

        print str(len(Africas_Talking_Output.objects.all()))
        for msg in Africas_Talking_Output.objects.all():
            print msg.raw_message

        Africas_Talking_Output.objects.get(raw_message__contains=msg1)
        Africas_Talking_Output.objects.get(raw_message__contains=msg2)


class BBOXXDataCollectionTests(TestCase):
    """    
    Tests the BBOX API for data collection
    """
    BBOXX_ENERGY_LINE_NUMBER = 1
    BBOXX_PULSE_COUNT_LINE_NUMBER = 2

    start_date = "2016-05-01 00:00:00"
    end_date = "2016-05-02 00:00:00"
    imei = "013950003930105"
    seq_num = 0
    timestamp = 1462277975000
    current_log = 0.46
    volt_log = 27.5
    temp_log = 38.8
    energy_log = 1.35
    meter_recording = energy_log / 1000
    pulse_log = 5.25
    pulse_recording = pulse_log / 1000
    pulse_ratio = 5
    pulse_recording_ratio = pulse_log / 1000 * pulse_ratio

    def setUp(self):
        self.test_site = Mingi_Site.objects.create(name='Testing World', num_lines=2,
                                                   date_online=datetime.datetime.now(),
                                                   latitude=0.00, longitude=0.00)

        utility = Utility.objects.create(name="Electricity:")

        for i in range(1, 3):
            line = Line.objects.create(site=self.test_site, number=i,
                                       utility_type=utility)

        bh = Bit_Harvester.objects.create(harvester_id='testBH', site=self.test_site,
                                          serial_number=self.imei, version='LITE',
                                          approval_date=datetime.datetime.now())

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_collect_bbox_data_no_pulse(self, mock_get_bboxx_token, mock_requests_get):
        """
        Tests that the correct request is made to BBOXX for a bitHarvester with
        no pulse_count data (one line only).
        """
        # Set site to one line
        self.test_site.num_lines = 1
        self.test_site.save()

        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Set up mock data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [],
            'voltage': [],
            'temperature': [],
            'energy': []
        }

        # Call get data function
        collect_BBOXX_data(self.start_date, self.end_date)
        self.assertEqual(
            len(Site_Record.objects.filter(site=self.test_site,
                                           key="Current Log",
                                           timestamp__gte=self.start_date,
                                           timestamp__lte=self.end_date)),
            0)

        # Check token function call
        mock_get_bboxx_token.assert_called_once_with()

        # Check data get call
        mock_requests_get.assert_called_once_with(
            "http://smartapi.bboxx.co.uk/v1/products/" + self.imei + "/rm_data",
            data=json.dumps({"start_time": self.start_date,
                             "end_time": self.end_date,
                             "metrics": ['current', 'voltage',
                                         'temperature', 'energy']}),
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Token token=%s' % mock_token}
        )

        # Check no error logs created
        self.assertEqual(Error_Log.objects.count(), 0)

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_collect_bbox_data_with_pulse(self, mock_get_bboxx_token, mock_requests_get):
        """
        Tests that the correct request is made to BBOXX for a bitHarvester with
        pulse_count data (more than one line).
        """
        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Set up mock data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [],
            'voltage': [],
            'temperature': [],
            'energy': [],
            'pulse_count': []
        }

        # Call get data function
        collect_BBOXX_data(self.start_date, self.end_date)
        self.assertEqual(
            len(Site_Record.objects.filter(site=self.test_site,
                                           key="Current Log",
                                           timestamp__gte=self.start_date,
                                           timestamp__lte=self.end_date)),
            0)

        # Check token function call
        mock_get_bboxx_token.assert_called_once_with()

        # Check data get call
        mock_requests_get.assert_called_once_with(
            "http://smartapi.bboxx.co.uk/v1/products/" + self.imei + "/rm_data",
            data=json.dumps({"start_time": self.start_date,
                             "end_time": self.end_date,
                             "metrics": ['current', 'voltage',
                                         'temperature', 'energy',
                                         'pulse_count']}),
            headers={'Content-Type': 'application/json',
                     'Authorization': 'Token token=%s' % mock_token}
        )

        # Check no error logs created
        self.assertEqual(Error_Log.objects.count(), 0)

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_missing_bbox_data(self, mock_get_bboxx_token, mock_requests_get):
        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Missing data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [],
            'voltage': [],
            'temperature': []
        }
        collect_BBOXX_data(self.start_date, self.end_date)

        # Error logs created (one per missing key: 'energy', 'pulse_count')
        self.assertEqual(Error_Log.objects.count(), 2)

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_bboxx_failure_response(self, mock_get_bboxx_token, mock_requests_get):
        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Missing data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'Failure': "uh oh!"
        }
        collect_BBOXX_data(self.start_date, self.end_date)

        # Error log created
        self.assertEqual(Error_Log.objects.count(), 1)
        el = Error_Log.objects.get()
        self.assertTrue("Failure response from BBOXX" in el.problem_message)

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_line_states_creation(self, mock_get_bboxx_token, mock_requests_get):
        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Set up mock data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [{
                'points': [[self.timestamp, self.seq_num, self.current_log]]
            }],
            'voltage': [{
                'points': [[self.timestamp, self.seq_num, self.volt_log]]
            }],
            'temperature': [{
                'points': [[self.timestamp, self.seq_num, self.temp_log]]
            }],
            'energy': [{
                'points': [[self.timestamp, self.seq_num, self.energy_log]]
            }],
            'pulse_count': [{
                'points': [[self.timestamp, self.seq_num, self.pulse_log]]
            }]
        }

        collect_BBOXX_data(self.start_date, self.end_date)

        # Check line states created, for each item type
        self.assertTrue(Site_Record.objects.filter(site=self.test_site,
                                                   key="Current Log",
                                                   value=self.current_log).exists())

        self.assertTrue(Site_Record.objects.filter(site=self.test_site,
                                                   key="BH Data Log",
                                                   value=str(self.volt_log) + "," + str(self.temp_log)).exists())

        self.assertTrue(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_ENERGY_LINE_NUMBER,
                                                  meter_reading=self.meter_recording).exists())

        self.assertTrue(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_PULSE_COUNT_LINE_NUMBER,
                                                  meter_reading=self.pulse_recording).exists())

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_no_duplicate_line_states(self, mock_get_bboxx_token, mock_requests_get):
        """
        Test that the collect_BBOXX_data function will not create duplicate
        Line_State records if it is run over the same time range multiple times
        """
        # Create existing line state objects
        this_timestamp = datetime.datetime.fromtimestamp(self.timestamp / 1000)

        Line_State.objects.create(site=self.test_site, number=self.BBOXX_ENERGY_LINE_NUMBER,
                                  timestamp=this_timestamp,
                                  meter_reading=self.meter_recording)
        Line_State.objects.create(site=self.test_site, number=self.BBOXX_PULSE_COUNT_LINE_NUMBER,
                                  timestamp=this_timestamp,
                                  meter_reading=self.pulse_recording)

        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Set up mock data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [{
                'points': [[self.timestamp, self.seq_num, self.current_log]]
            }],
            'voltage': [{
                'points': [[self.timestamp, self.seq_num, self.volt_log]]
            }],
            'temperature': [{
                'points': [[self.timestamp, self.seq_num, self.temp_log]]
            }],
            'energy': [{
                'points': [[self.timestamp, self.seq_num, self.energy_log]]
            }],
            'pulse_count': [{
                'points': [[self.timestamp, self.seq_num, self.pulse_log]]
            }]
        }
        collect_BBOXX_data(self.start_date, self.end_date)

        self.assertEqual(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_ENERGY_LINE_NUMBER,
                                                   timestamp=this_timestamp,
                                                   meter_reading=self.meter_recording).count(), 1)
        self.assertEqual(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_PULSE_COUNT_LINE_NUMBER,
                                                   timestamp=this_timestamp,
                                                   meter_reading=self.pulse_recording).count(), 1)

    @mock.patch('requests.get')
    @mock.patch('africas_talking.BBOXX_API.get_bboxx_token')
    def test_line_states_creation_pulse_ratio(self, mock_get_bboxx_token, mock_requests_get):
        """
        Test that pulse_ratio is applied to stored Line_State if the relevant
        line has a pulse_ratio registered
        """

        line = Line.objects.get(site=self.test_site,
                                number=self.BBOXX_PULSE_COUNT_LINE_NUMBER)
        line.pulse_ratio = self.pulse_ratio
        line.save()

        # Set up mock token
        mock_token = 'mymocktoken'
        mock_get_bboxx_token.return_value = mock_token

        # Set up mock data response
        mock_data_response = mock_requests_get.return_value
        mock_data_response.status_code = 200
        mock_data_response.json.return_value = {
            'current': [{
                'points': [[self.timestamp, self.seq_num, self.current_log]]
            }],
            'voltage': [{
                'points': [[self.timestamp, self.seq_num, self.volt_log]]
            }],
            'temperature': [{
                'points': [[self.timestamp, self.seq_num, self.temp_log]]
            }],
            'energy': [{
                'points': [[self.timestamp, self.seq_num, self.energy_log]]
            }],
            'pulse_count': [{
                'points': [[self.timestamp, self.seq_num, self.pulse_log]]
            }]
        }
        collect_BBOXX_data(self.start_date, self.end_date)

        # Check line states created, for each item type
        self.assertTrue(Site_Record.objects.filter(site=self.test_site,
                                                   key="Current Log",
                                                   value=self.current_log).exists())

        self.assertTrue(Site_Record.objects.filter(site=self.test_site,
                                                   key="BH Data Log",
                                                   value=str(self.volt_log) + "," + str(self.temp_log)).exists())

        self.assertTrue(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_ENERGY_LINE_NUMBER,
                                                  meter_reading=self.meter_recording).exists())

        self.assertTrue(Line_State.objects.filter(site=self.test_site, number=self.BBOXX_PULSE_COUNT_LINE_NUMBER,
                                                  meter_reading=self.pulse_recording_ratio).exists())


class BBOXXLineStatusUpdateTestCase(SteamaDBTestCase):
    """
    A suite of tests covering our ability to read line state updates from the
    BBOXX API.
    """
    @mock.patch("requests.get")
    @mock.patch("requests.post")
    def test_update_state_multiple_updates_with_unknown(self,
                                                        mock_post,
                                                        mock_get):
        """
        Tests updating line states when multiple updates are returned from the
        BBOXX API, some duplicates (only the most recent states will be used)
        and one unknown IMEI number (it will be skipped and an error log
        created).
        """
        # Mock token response
        mock_token = "mockToken123"
        mock_post.return_value.json.return_value = {
            "message": {"login_successful": {"API_token": mock_token}}
        }

        # Mock line state response
        unknown_imei = "uhoh123"
        mock_get.return_value.json.return_value = {
            "objects": [
                {
                    "created_at": "2016-01-01T00:00:00",
                    "current_enable_state": STATE_DISABLED,
                    "product_imei": self.all_user_bh.serial_number
                },
                {
                    "created_at": "2016-01-03T00:00:00",
                    "current_enable_state": STATE_ENABLED,
                    "product_imei": self.all_user_bh.serial_number
                },
                {
                    "created_at": "2016-01-05T00:00:00",
                    "current_enable_state": STATE_DISABLED,
                    "product_imei": self.superuser_bh.serial_number
                },
                {
                    "created_at": "2016-01-04T00:00:00",
                    "current_enable_state": STATE_DISABLED,
                    "product_imei": unknown_imei
                },
                {
                    "created_at": "2016-01-02T00:00:00",
                    "current_enable_state": STATE_DISABLED,
                    "product_imei": self.all_user_bh.serial_number
                }
            ]
        }

        # Call update function
        start_time = datetime.datetime(2016, 01, 01)
        update_bboxx_line_states(start_time)

        # Check that the correct requests.get call was made
        mock_get.assert_called_once_with(
            "http://smartapi.bboxx.co.uk/v1/enable_history",
            params={
                "q": json.dumps({"filters": [{"name": "created_at",
                                              "op": ">=",
                                              "val": start_time.isoformat()}]})
            },
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Token token=' + mock_token
            })

        # Check that all all_user_bh lines are on and all superuser_bh lines
        # are off
        self.assertTrue(
            all([line.line_status == Line.ON_MANUAL
                 for line in Line.objects.filter(site=self.all_user_site)]))
        self.assertTrue(
            all([line.line_status == Line.OFF_MANUAL
                 for line in Line.objects.filter(site=self.superuser_site)]))

        # Check that an error log object was created for the unknown IMEI
        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertEqual(
            error_log.problem_message,
            ("Unknown bitHarvester IMEI referenced in BBOXX API: %s"
             % unknown_imei))


class ProcessPaymentInfoTestCase(SteamaDBTestCase):
    """
    Tests logic for processing payment information.
    """

    @mock.patch("africas_talking.views.callPayPlanFunction")
    def test_payment_info_existing_customer(self, mock_call_pay_plan_function):
        """
        Tests the process_kopo_kopo_payment_info function with a known customer
        telephone number.
        """
        # Call process_kopo_kopo_payment_info
        payment_amount = 100
        result = process_kopo_kopo_payment_info(
            payment_amount, self.all_user_customer.telephone,
            "Test payment message", "Test payment reference")

        # Check that the appropriate response was returned
        self.assertEqual(result, None)

        # Test that Kopo_Kopo_Message was created
        self.assertEqual(Kopo_Kopo_Message.objects.count(), 1)
        new_message = Kopo_Kopo_Message.objects.get()
        self.assertEqual(new_message.user, self.all_user_customer)
        self.assertEqual(new_message.amount, payment_amount)
        self.assertEqual(new_message.category, Kopo_Kopo_Message.PAYMENT)
        self.assertEqual(new_message.provider, Kopo_Kopo_Message.KOPO_KOPO)
        self.assertEqual(new_message.reference, "Test payment reference")
        self.assertEqual(new_message.raw_message, "Test payment message")

        # Test that info was passed on to the callPayPlanFunction
        mock_call_pay_plan_function.assert_called_once_with(
            self.all_user_customer,
            payment_amount
        )

    def test_payment_info_unknown_customer(self):
        """
        Tests the process_kopo_kopo_payment_info function with an unknown
        customer telephone number.
        """
        # Call process_kopo_kopo_payment_info
        payment_amount = 100
        unknown_telephone = '+100100100900'
        result = process_kopo_kopo_payment_info(
            payment_amount, unknown_telephone, "Test payment message",
            "Test payment reference")

        # Check that the appropriate response was returned
        self.assertEqual(result, PROCESS_PAYMENT_ERRORS['USER_NOT_FOUND'])

        # Test that pre-registered customer was created
        self.assertTrue(
            Mingi_User.objects.filter(telephone=unknown_telephone).exists())
        new_customer = Mingi_User.objects.get(telephone=unknown_telephone)

        # Test that Kopo_Kopo_Message was created
        self.assertEqual(Kopo_Kopo_Message.objects.count(), 1)
        new_message = Kopo_Kopo_Message.objects.get()
        self.assertEqual(new_message.user, new_customer)
        self.assertEqual(new_message.amount, payment_amount)
        self.assertEqual(new_message.category, Kopo_Kopo_Message.PAYMENT)
        self.assertEqual(new_message.provider, Kopo_Kopo_Message.KOPO_KOPO)
        self.assertEqual(new_message.reference, "Test payment reference")
        self.assertEqual(new_message.raw_message, "Test payment message")

    @mock.patch("africas_talking.views.callPayPlanFunction")
    def test_duplicate_payment_info(self, mock_call_pay_plan_function):
        """
        Tests that the process_kopo_kopo_payment_info function only processes a
        duplicate message once.
        """
        # Call process_kopo_kopo_payment_info twice
        payment_amount = 100
        result_1 = process_kopo_kopo_payment_info(
            payment_amount, self.all_user_customer.telephone,
            "Test payment message", "Test payment reference")
        result_2 = process_kopo_kopo_payment_info(
            payment_amount, self.all_user_customer.telephone,
            "Test payment message", "Test payment reference")

        # Check that the appropriate responses were returned
        self.assertEqual(result_1, None)
        self.assertEqual(result_2, PROCESS_PAYMENT_ERRORS['DUPLICATE_REF'])

        # Test that a single Kopo_Kopo_Message was created
        self.assertEqual(Kopo_Kopo_Message.objects.count(), 1)
        new_message = Kopo_Kopo_Message.objects.get()
        self.assertEqual(new_message.user, self.all_user_customer)
        self.assertEqual(new_message.amount, payment_amount)
        self.assertEqual(new_message.category, Kopo_Kopo_Message.PAYMENT)
        self.assertEqual(new_message.provider, Kopo_Kopo_Message.KOPO_KOPO)
        self.assertEqual(new_message.reference, "Test payment reference")
        self.assertEqual(new_message.raw_message, "Test payment message")

        # Test that info was passed on to the callPayPlanFunction once
        mock_call_pay_plan_function.assert_called_once_with(
            self.all_user_customer,
            payment_amount
        )


class SMSGatewayAPISendTestCase(SteamaDBTestCase):
    """
    A suite of tests for sending messages over the SMS Gateway API integration.
    """

    def setUp(self):
        """
        Runs the SteamaDBTestCase
        :return:
        """
        super(SMSGatewayAPISendTestCase, self).setUp()

        self.sms_gateway_api_gateway = Gateway.objects.create(
            name="SMS Gateway API",
            device_id="Test Device ID"
        )

    @mock.patch('africas_talking.sendSMS.send_sms_gateway_api')
    def test_sendSMS_function_sms_gateway(self, mock_send):
        """
        Tests calling the sendSMS function on an SMS Gateway API Gateway.
        """
        self.superuser_bh.bh_type = 'SMS-Gateway-API'
        self.superuser_bh.output_gateway = self.sms_gateway_api_gateway
        self.superuser_bh.save()

        test_message = "test message"

        # Avoid admin email settings so that the actual gateway function can be
        # called.
        with self.settings(SEND_SMS_AS_ADMIN_EMAIL=False):
            output = sendSMS(self.superuser_bh.telephone,
                             test_message,
                             self.superuser_bh)

        # Check output log created
        self.assertEqual(Africas_Talking_Output.objects.all().count(), 1)
        self.assertEqual(Africas_Talking_Output.objects.get(), output)
        self.assertEqual(output.gateway, self.sms_gateway_api_gateway)

        # Check that no Error_Log objects were created
        self.assertEqual(Error_Log.objects.count(), 0)

        # Check SMS Gateway API send function called
        mock_send.assert_called_once_with(
            self.superuser_bh.telephone,
            test_message,
            self.sms_gateway_api_gateway.device_id
        )

    @mock.patch('requests.post')
    def test_send_sms_gateway_api_success(self, mock_post):
        """
        Tests a successful SMS send over SMS Gateway API.
        """
        # Mock successful return value (no fails)
        mock_post.return_value.json.return_value = {
            'result': {'fails': []}
        }

        number = '12345'
        message = 'hi guys'
        device = 'abc123'
        send_sms_gateway_api(number, message, device)

        # Check no error logs created
        self.assertEqual(Error_Log.objects.all().count(), 0)

        # Check correct post request made
        mock_post.assert_called_once_with(
            SGA_SEND_MESSAGE_URL,
            data={
                'email': SGA_ACCOUNT_EMAIL,
                'password': SGA_ACCOUNT_PASSWORD,
                'device': device,
                'number': number,
                'message': message
            }
        )

    @mock.patch('requests.post')
    def test_send_sms_gateway_api_non_json_response(self, mock_post):
        """
        Tests handing a non-JSON response from the SMS Gateway API send call.
        """
        # Mock non-JSON return value
        mock_post_return = mock_post.return_value
        mock_post_return.json.side_effect = ValueError("uh oh")
        mock_post_return.text = "test error text"

        number = '12345'
        message = 'hi guys'
        device = 'abc123'
        send_sms_gateway_api(number, message, device)

        # Check error log created
        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertEqual(error_log.problem_message,
                         "Non-JSON response from SMS Gateway API: "
                         "test error text")

        # Check correct post request made
        mock_post.assert_called_once_with(
            SGA_SEND_MESSAGE_URL,
            data={
                'email': SGA_ACCOUNT_EMAIL,
                'password': SGA_ACCOUNT_PASSWORD,
                'device': device,
                'number': number,
                'message': message
            }
        )

    @mock.patch('requests.post')
    def test_send_sms_gateway_api_failure_response(self, mock_post):
        """
        Tests handing a failure response from the SMS Gateway API send call.
        """
        # Mock failed return value
        mock_post.return_value.json.return_value = {
            'result': {'fails': ['abc', '123']}
        }

        number = '12345'
        message = 'hi guys'
        device = 'abc123'
        send_sms_gateway_api(number, message, device)

        # Check error log created
        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertEqual(error_log.problem_message,
                         "Failure response from SMS Gateway API: "
                         "['abc', '123']")

        # Check correct post request made
        mock_post.assert_called_once_with(
            SGA_SEND_MESSAGE_URL,
            data={
                'email': SGA_ACCOUNT_EMAIL,
                'password': SGA_ACCOUNT_PASSWORD,
                'device': device,
                'number': number,
                'message': message
            }
        )


class SMSGatewayAPIReceiveTestCase(TestCase):
    """
    A suite of tests for receiving messages from the SMS Gateway API
    integration.
    """

    def setUp(self):
        self.sga_callback_url = "/sga_msg/"

    @mock.patch('africas_talking.views.process_sms_info')
    def test_receive_sms_gateway_api_success(self, mock_process_sms):
        """
        Tests successfully receiving an SMS from the SMS Gateway API.
        """
        test_number = '12345'
        test_message = 'M abc123'
        response = self.client.post(self.sga_callback_url,
                                    {
                                        'contact[number]': test_number,
                                        'message': test_message,
                                        'secret': SGA_CALLBACK_SECRET
                                    })

        self.assertEqual(response.status_code, 200)

        mock_process_sms.assert_called_once_with(test_number, test_message[2:])

    @mock.patch('africas_talking.views.process_sms_info')
    def test_receive_sms_gateway_api_no_secret(self, mock_process_sms):
        """
        Tests handling a message from the SMS Gateway API with no secret.
        """
        test_number = '12345'
        test_message = 'M abc123'
        response = self.client.post(self.sga_callback_url,
                                    {
                                        'contact[number]': test_number,
                                        'message': test_message
                                    })

        self.assertEqual(response.status_code, 400)

        mock_process_sms.assert_not_called()

        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertIn(
            "SMS Gateway API callback received with no 'secret':",
            error_log.problem_message)

    @mock.patch('africas_talking.views.process_sms_info')
    def test_receive_sms_gateway_api_bad_secret(self, mock_process_sms):
        """
        Tests handling a message from the SMS Gateway API with an incorrect
        secret.
        """
        test_number = '12345'
        test_message = 'M abc123'
        response = self.client.post(self.sga_callback_url,
                                    {
                                        'contact[number]': test_number,
                                        'message': test_message,
                                        'secret': "uh oh"
                                    })

        self.assertEqual(response.status_code, 400)

        mock_process_sms.assert_not_called()

        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertIn(
            "SMS Gateway API callback received with incorrect 'secret':",
            error_log.problem_message)

    @mock.patch('africas_talking.views.process_sms_info')
    def test_receive_sms_gateway_api_missing_parameter(self, mock_process_sms):
        """
        Tests handling a message from the SMS Gateway API with a missing
        parameter.
        """
        test_number = '12345'
        test_message = 'M abc123'
        response = self.client.post(self.sga_callback_url,
                                    {
                                        'message': test_message,
                                        'secret': SGA_CALLBACK_SECRET
                                    })

        self.assertEqual(response.status_code, 400)

        mock_process_sms.assert_not_called()

        self.assertEqual(Error_Log.objects.all().count(), 1)
        error_log = Error_Log.objects.get()
        self.assertEqual(error_log.problem_type, "EXTERNAL")
        self.assertIn(
            "SMS Gateway API callback received with missing information:",
            error_log.problem_message)
