from django.core.mail import mail_admins
from celery import task


@task
def send_admin_notification(subject, message):
    mail_admins(subject=subject, message=message)