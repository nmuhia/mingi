from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from africas_talking.models import Gateway

import requests
import re
import pprint
import sys

@csrf_exempt
def sendSneakerCloudMessage(gateway,smsTo,message):
    """
    SMS Ultimate app
    """
    try:
        selected_gateway = Gateway.objects.filter(name=gateway)[0]
    except:
        raise NoSmsGatewayUrl

    # get aunthentication
    gateway_url = selected_gateway.send_url
    username = selected_gateway.username
    password = selected_gateway.password
    sms_type = selected_gateway.sms_type

    if gateway_url =='':
        raise NoSmsGatewayUrl
    if username == '' or password == '':
        raise InvalidSmsGatewayArgument

    send_to = smsTo.replace('+','%2B')
    return requests.get(gateway_url+'send.html?smsto='+send_to+'&smsbody='+message+'&smstype='+sms_type,auth=(username, password))

@csrf_exempt
def sendSneakerCloudMessage2(gateway,smsTo,message):
    """
    Send message app
    """
    print "sending message"
    try:
        selected_gateway = gateway
    except:
        raise NoSmsGatewayUrl

    # get aunthentication
    gateway_url = selected_gateway.send_url
    password = selected_gateway.password
    sms_type = 'sms' # allows sending as sms only
    submit = ''
    msg_id ='' # blank for new messages

    if gateway_url =='':
        raise NoSmsGatewayUrl
    if password == '':
        raise InvalidSmsGatewayArgument

    send_to = smsTo.replace('+','%2B')
    # return sneaker_url+'send.html?smsto='+to+'&smsbody='+message+'&smstype='+sms_type
    return requests.get(gateway_url+'send/?pass='+password+'&number='+send_to+'&data='+message+'&submit='+submit+'&id='+msg_id)

class InvalidSmsGatewayArgument(Exception):
    """
    Return incorrect Sneaker Cloud Gateway arguments
    """
    pass

class NoSmsGatewayUrl(Exception):
    """
    Return when no sneaker cloud Gateway URL is defined
    """
    pass
    """
    Uncomment to allow sending out of SMS with syncs app
import sys
import datetime
import AfricasTalkingGateway as ATG
from django.conf import settings
from mingi_user.models import Mingi_User
from mingi_site.models import Bit_Harvester
from mingi_django.models import Error_Log
from .tasks import send_admin_notification
from django.views.decorators.csrf import csrf_exempt
from africas_talking.sendSMS import locateUser, UserNotFound

import requests
from django.http import HttpResponse

@csrf_exempt
def sendMessage(request):

    Check for SmsSync queued messages and send them



    get = request.GET
    post = request.POST

    print "print get ..."
    pprint.pprint(get)

    print "printing post ..."
    pprint.pprint(post)

    SECRET_CODE = 'simontest'
    GATEWAY_PHONE = '254735358743'


    # Send messages
    if get.get('task',False) and get.get('task',False)=='send': # and SIMCARD_SECRETCODE_MAPPER.get(get['device_id'].strip())==get['secret']:
        # Get server to send message
        # server_secret = SIMCARD_SECRETCODE_MAPPER[post['sender_id']]
        print "testing ..."
        # Get messages for the gateway
        messages =Africas_Talking_Output.objects.filter(gateway__icontains=GATEWAY_PHONE,send_status='4') 
        main_message = {
        'task':'send',
        'secret':SECRET_CODE,
        'messages':[]
        }

        msg_list = list()
        # compose messages
        for msg in messages:
            # send to user
            if msg.user:
                msg_list.append({'to':str(msg.user.telephone),'message':str(msg.raw_message),'uuid':str(msg.id)})
            elif msg.bit_harvester:
                if msg.bit_harvester.send_telephone is not None and msg.bit_harvester.send_telephone!='':
                    msg_list.append({'to':str(msg.bit_harvester.send_telephone),'message':str(msg.raw_message),'uuid':str(msg.id)})
                else:
                    msg_list.append({'to':str(msg.bit_harvester.telephone),'message':str(msg.raw_message),'uuid':str(msg.id)})
            else:
                pass

        main_message['messages'] = msg_list

        return HttpResponse(json.dumps({"payload":main_message}))

    #Report messages have been queued
    elif post.get('task',False) and post.get('task',False)=='sent':
        print "Recording queued messages ..."
        # get json for message id's
        message_str = json.loads(post)
        messages = Africas_Talking_Output.objects.filter(gateway__icontains=GATEWAY_PHONE)
        for msg in message_str['message_uuids']:
            # update status
            print "message ID :"+str(msg)
            messages.filter(id=msg).update(send_status='3')
        # Return empty
        result = list()
        return HttpResponse(json.dumps({'message_uuids':result}))

    #Request for delivery reports
    elif get.get('task',False) and get.get('task',False)=='result':
        print "Requesting delivery reports ..."
        # get json for message id's
        result = list()
        messages = Africas_Talking_Output.objects.filter(gateway__icontains=GATEWAY_PHONE,send_status='3')
        for msg in messages:
            result.append(msg.id)
        return HttpResponse(json.dumps({'message_uuids':result}))

    #Report message delivery status
    elif post.get('task',False) and post.get('task',False)=='result':
        print "Recording received status..."
        # get json for message id's
        message_str = json.loads(post)
        messages = Africas_Talking_Output.objects.filter(gateway__icontains=GATEWAY_PHONE)
        for msg in message_str['message_result']:
            # update status
            print "message ID :"+str(msg)
            messages.filter(id=msg['uuid']).update(send_status=msg['delivered_result_code'])
    
    # Check for alerts
    elif post.get('task',False) and post.get('task',False)=='alert':

        print "Check alerts from server ..."
        now = datetime.now()
        try:
            user = locateUser("+"+GATEWAY_PHONE)
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="SC Error : " + str(e))
        # Record message only if BH
        if type(user)==Bit_Harvester:
            Africas_Talking_Input.objects.create(raw_message=post['message'],user=user,timestamp=now,gateway=user.output_gateway,bit_harvester=user)

        return HttpResponse(json.dumps({'payloads':{'success':True,'error':None}}))

    else:
        print "not allowed"
        return HttpResponse(json.dumps({'payload':{'success':'false','error':'sorry you are not allowed to view this page'}}))
        """
