import datetime
import requests
import sys
import AfricasTalkingGateway as ATG

from django.conf import settings
from django.contrib.auth.models import User

from twilio.rest import TwilioRestClient
from ControlV4 import unpack_v3

from mingi_django.models import Error_Log
from mingi_django.timezones import zone_aware
from mingi_site.models import Bit_Harvester
from mingi_user.models import Mingi_User

from .models import Africas_Talking_Output, InvalidAttributeValue,\
    MessageNotSentException
from .sms_gateway_api import send_sms_gateway_api
from .BBOXX_API import bboxx_switch_trigger


def locateUser(phoneNo):
    """
    identify a user/BH by phone number
    """
        
    allUsers = Mingi_User.objects.all()
    try:
        return Mingi_User.objects.get(telephone=phoneNo)
    except Mingi_User.DoesNotExist:
        try:
            return Bit_Harvester.objects.get(telephone=phoneNo)
        except Bit_Harvester.DoesNotExist:
            raise UserNotFound(phoneNo)


def locateWebsiteUserEmail(email):
    """
    identify a user by email address
    """
    try:
        # user_detail = User.objects.get(email='simon@steama.co')
        user_detail = User.objects.filter(email=email)
        if len(user_detail)>1:
            raise Mingi_User.MultipleObjectsReturned
        return user_detail
    except:
        raise UserNotFound(email)

   
def sendSMS(SMSTo, msgOut, recipient=None):
    """
    send a request to the Africa's Talking API to send a message to SMSTo with
    contents msgOut
    """

    # Check the sender
    # if no user passed, locat user
    if recipient is None:
        try:
            toUser = locateUser(SMSTo)
            
        except UserNotFound as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message=str(e))
            raise MessageNotSentException()
        except Mingi_User.MultipleObjectsReturned as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="User Error for: %s; %s" % (SMSTo, str(e)))
            return
    else:
        if type(recipient) is not Bit_Harvester and type(recipient) is not Mingi_User:
            # treat this as a non-user system, log error
            Error_Log.objects.create(problem_type="INTERNAL",
                                             problem_message="Invalid user type for phone "+SMSTo + "params: "+str(recipient) + ". Type = " + str(type(recipient)))
            raise MessageNotSentException
            
        # initialize toUser
        toUser = recipient

    now = zone_aware(toUser, datetime.datetime.now())

    userType = type(toUser)
    
    # identify the correct gateway for sending the message
    if userType == Bit_Harvester:

        text_msgOut = str(msgOut)

        # check whether the BH specifies a different recipient number
        if (toUser.send_telephone):
            SMSTo = toUser.send_telephone
        # translate message for V4 bitHarvesters
        if (toUser.version == 'V4') and msgOut.upper() != "PING":
            try:
                messagesOut = unpack_v3(text_msgOut).get_packets()     
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=str(e) + ": generated from conversion of message " + text_msgOut)
                raise
            msgOut = messagesOut[0]
            
        # identify the correct gateway for sending the message
        gateway_type = toUser.bh_type
            
    elif userType == Mingi_User:
        # gateway of a user is the same as the gateway of their bitHarvester
        if toUser.bit_harvester: 
            gateway_type = toUser.bit_harvester.bh_type
        else:
            gateway_type = "AT"

        text_msgOut = msgOut # no V4 conversion for Mingi_Users

    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Invalid user type for phone number: %s" % SMSTo)
        return

      
    # CALL SEND MESSAGE FUNCTIONS ----------------------------------------
    # in DEBUG mode, just create the record, don't send a message
    # send message as email on staging server
    if settings.SEND_SMS_AS_ADMIN_EMAIL:
        Error_Log.objects.create(problem_type="DEBUGGING",
                                 problem_message="SMS Capabilities Turned Off")
        if 'SC' in gateway_type:
            outLog = send_sms_sneaker_cloud(toUser, msgOut, text_msgOut)
            return outLog

        if (userType == Mingi_User):
            outLog = Africas_Talking_Output.objects.create(raw_message=msgOut,
                                                           telephone=SMSTo,
                                                           user=toUser,
                                                           timestamp=now,gateway=toUser.bit_harvester.output_gateway,bit_harvester=None)

        elif (userType == Bit_Harvester):
            outLog = Africas_Talking_Output.objects.create(raw_message=text_msgOut,
                                                           telephone=SMSTo,
                                                           bit_harvester=toUser,
                                                           gateway=toUser.output_gateway,
                                                           timestamp=now)

        else:
            raise InvalidAttributeValue("Africas_Talking_Output",
                                        "SMSTo",userType)

        return outLog

    # send message through F1 gateway, if specified
    elif 'F1' in gateway_type:
        outLog = send_F1_SMS(toUser, msgOut)
        return outLog

    # send message through Twilio gateway, if specified (US default)
    elif 'TW' in gateway_type:
        # determine from which Twilio number to send the message
        try:
            country = gateway_type.split("-")[1]
        except Exception:
            country = "US"
        outLog = send_twilio_sms(toUser, msgOut, country, text_msgOut)
        return outLog
    
    # send message through Sneaker Cloud, if specified
    elif 'SC' in gateway_type:
        outLog = send_sms_sneaker_cloud(toUser, msgOut, text_msgOut)
        return outLog

    # send message through SMS Gateway API
    elif gateway_type == 'SMS-Gateway-API':
        if type(toUser) == Bit_Harvester:
            gateway = toUser.output_gateway
            bh = toUser
            user = None
        elif type(toUser) == Mingi_User:
            gateway = toUser.bit_harvester.output_gateway
            bh = None
            user = toUser
        else:
            raise NotImplementedError(
                "Unimplemented Gateway destination type: %s" %
                str(type(toUser)))

        send_sms_gateway_api(SMSTo, text_msgOut, gateway.device_id)

        return Africas_Talking_Output.objects.create(
            raw_message=text_msgOut,
            telephone=SMSTo,
            bit_harvester=bh,
            user=user,
            gateway=gateway,
            timestamp=zone_aware(toUser, datetime.datetime.now()))

    # otherwise, send message through Africas_Talking
    else:
        outLog = send_at_sms(toUser, msgOut, SMSTo, text_msgOut)
        return outLog


def send_F1_SMS(toUser, msgOut):
    """
    send an SMS via the FocusOne gateway (Nepal) and make log object
    """
    userType = type(toUser)
    if userType == Bit_Harvester and toUser.send_telephone:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut)

        SMSTo = toUser.send_telephone
        gateway_type = toUser.output_gateway
        bh = toUser
        user = None
    elif userType == Bit_Harvester:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut)

        SMSTo = toUser.telephone
        gateway_type = toUser.output_gateway
        bh = toUser
        user = None
    elif (userType == Mingi_User):
        SMSTo = toUser.telephone
        gateway_type = toUser.bit_harvester.output_gateway
        bh = None
        user = toUser
    else:
        if toUser.version=="LITE":
            raise InvalidAttributeValue("Africas_Talking_Output",
                                        "SMSTo",userType)
        SMSTo = None
        gateway_type = None
        bh = None
        user = None

    
    # now = datetime.datetime.now()
    # get now based on user timezone
    now = zone_aware(toUser,datetime.datetime.now())
    
    SMSTo = toUser.telephone

    post_url = "http://requestb.in/1m9bm8k1"
    post_url = "http://smail.smscentral.com.np/bp/ApiSms.php"
    
    payload = {
            'mobile': SMSTo,
            'user': 'ae',
            'pass': 'ae3638',
            'content': msgOut,
            #'sender': 'MINGI',
        }
    
    response = requests.post(post_url, data=payload)
    print "Response: %s" % str(response)

    outLog = Africas_Talking_Output.objects.create(raw_message=msgOut,
                                                   telephone=SMSTo,
                                                   bit_harvester=bh,
                                                   gateway=gateway_type,
                                                   timestamp=now)

    return outLog


def send_at_sms(toUser, msgOut, SMSTo, text_msgOut=None):
    """
    send an SMS via the Africas Talking gateway and make log object
    """
    userType = type(toUser)
    # get now based on user timezone
    now = zone_aware(toUser,datetime.datetime.now())
    # now = datetime.datetime.now()

    try:
        gateway = ATG.AfricasTalkingGateway(settings.AT_USERNAME,
                                            settings.AT_API_KEY)
        sent = gateway.sendMessage(SMSTo,msgOut,'20880')
        gateway_type = 'AT'
        
        if userType == Mingi_User:
            outLog = Africas_Talking_Output.objects.create(raw_message=msgOut,telephone=SMSTo,user=toUser,timestamp=now,gateway=toUser.bit_harvester.output_gateway)
            return outLog
            
        elif (userType == Bit_Harvester):
            if toUser.version=="LITE":
                return send_sms_bboxx_api(toUser,msgOut,text_msgOut)

            outLog = Africas_Talking_Output.objects.create(raw_message=text_msgOut,telephone=SMSTo,bit_harvester=toUser,timestamp=now,gateway=toUser.output_gateway)
            return outLog
            
        else:
            raise InvalidAttributeValue("Africas_Talking_Output",
                                        "SMSTo",userType)
    
    except Exception as exception:
        Error_Log.objects.create(problem_type="DEBUGGING",
                                 problem_message=str(exception))
        e = sys.exc_info()[0]
        raise MessageNotSentException(e)
    

def send_twilio_sms(toUser, msgOut, country="US", text_msgOut=None):
    """
    send an SMS via the Twilio gateway (US number) and make log object
    """

    userType = type(toUser)
    if userType == Bit_Harvester and toUser.send_telephone:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut,text_msgOut)
        SMSTo = toUser.send_telephone
        gateway_type = toUser.output_gateway
        bh = toUser
        user = None
    elif userType == Bit_Harvester:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut,text_msgOut)
        SMSTo = toUser.telephone
        gateway_type = toUser.output_gateway
        bh = toUser
        user = None
    elif (userType == Mingi_User):
        SMSTo = toUser.telephone
        gateway_type = toUser.bit_harvester.output_gateway
        bh = None
        user = toUser
    else:
        if toUser.version=="LITE":
            raise InvalidAttributeValue("Africas_Talking_Output",
                                        "SMSTo",userType)
        SMSTo = None
        gateway_type = None
        bh = None
        user = None

    # now = datetime.datetime.now()
    # get now based on user timezone
    now = zone_aware(toUser,datetime.datetime.now())

    if country == "US":
        twilio_no = "+16572014286"
    elif country == "UK":
        twilio_no = "+442003331601"
    elif country == "FR":
        twilio_no = "+33644600545"

    account_sid = "AC0d2a78f65beb7af9c5f1dd04998bb17d"
    auth_token = "eabc9f076af9fa53929d4cabcadb7770"
    #post_url = "https://%s:%s@api.twilio.com/2010-04-01/Accounts/" % (account_sid, auth_token)

    client = TwilioRestClient(account_sid, auth_token)
    
    message = client.messages.create(
        body=msgOut,
        to=SMSTo,
        from_=twilio_no)

    # if V4 message, log the non-encrypted translation
    if not text_msgOut:
        text_msgOut = msgOut

    outLog = Africas_Talking_Output.objects.create(raw_message=text_msgOut,
                                                   telephone=SMSTo,
                                                   bit_harvester=bh,
                                                   user=user,
                                                   gateway=gateway_type,
                                                   timestamp=now)
    return outLog


def send_sms_sneaker_cloud(toUser, msgOut, text_msgOut=None):
    """
    send an SMS via the SMSSync gateway - android server Sneaker Cloud
    messages are queued for sending with the app
    """
    print "Sending sms with sneaker cloud ..."
    # now = datetime.datetime.now()
    # get now based on user timezone
    now = zone_aware(toUser,datetime.datetime.now())

    userType = type(toUser)
    if userType == Bit_Harvester and toUser.send_telephone:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut,text_msgOut)
        SMSTo = toUser.send_telephone
        gateway_bit_harvester = toUser.output_gateway
        bh = toUser
        user = None
    elif userType == Bit_Harvester:
        if toUser.version=="LITE":
            return send_sms_bboxx_api(toUser,msgOut,text_msgOut)
        SMSTo = toUser.telephone
        gateway_bit_harvester = toUser.output_gateway
        bh = toUser
        user = None
    elif (userType == Mingi_User):
        SMSTo = toUser.telephone
        gateway_bit_harvester = toUser.bit_harvester.output_gateway
        bh = None
        user = toUser 

    
    outLog = Africas_Talking_Output.objects.create(raw_message=msgOut,
                                                   text_message=text_msgOut,
                                                   telephone=SMSTo,
                                                   bit_harvester=bh,
                                                   user=user,
                                                   gateway=gateway_bit_harvester,
                                                   timestamp=now,
                                                   send_status='4',)
    return outLog


def send_sms_bboxx_api(toUser, msgOut, text_msgOut):
    """
    send message through BBOXX API
    """
    now = zone_aware(toUser, datetime.datetime.now())

    m_sms = msgOut
    if text_msgOut is not None:
        m_sms = text_msgOut

    userType = type(toUser)
    if type(toUser) == Bit_Harvester:
        bh_imei = toUser.serial_number
        if "OFF" in m_sms:
            bboxx_switch_trigger("OFF", bh_imei)
        elif "ON" in m_sms:
            bboxx_switch_trigger("ON", bh_imei)
        else:
            raise InvalidAttributeValue("Africas_Talking_Output",
                                        "SMSTo", userType)
    else:
        raise InvalidAttributeValue("Africas_Talking_Output", "SMSTo", userType)
    return Africas_Talking_Output.objects.create(raw_message=msgOut,
                                                 text_message=text_msgOut,
                                                 telephone=toUser.telephone,
                                                 bit_harvester=toUser,
                                                 user=None,
                                                 gateway=toUser.output_gateway,
                                                 timestamp=now,
                                                 send_status='4',)

    
class UserNotFound(Exception):
    """
    no user (human or bitHarvester) found with matching phone number
    """

    def __init__(self, phoneNo=None):
        
        if (phoneNo == None):
            self.msg = "No matching user found"
        else:
            self.msg = "No matching user found for phone number: "+str(phoneNo)

    def __str__(self):
        return self.msg
