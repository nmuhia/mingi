#!/usr/bin/python
#
# (C) BitBox Ltd 2015
#
# $Id: TextV4.py 925 2015-01-25 14:58:48Z phorton $
#

from base64 import b64decode, b64encode
import crcmod

MAX_OUTGOING_SIZE = 160 * 6 / 8 - 1

_crc8 = crcmod.mkCrcFun(0x131, 0)

#
# unpack binary data from SMS
#
# 1. remove 'M ' gateway prefix if there is one
# 2. base-64 decode
# 3. verify CRC-8
#
# returns 'None' on error
#
def sms_unpack(sms_body):
	'unpack binary data from text message'
	# remove gateway prefix
	if sms_body[:2] == 'M ':
		sms_body = sms_body[2:]
	# calculate decoded length
	length = len(sms_body) * 6 / 8
	# add padding to make valid base-64
	runt = len(sms_body) % 4
	if runt:
		sms_body = sms_body + '=' * (4 - runt)
	# decode base-64 to binary
	try:
		octets = b64decode(sms_body)
	except TypeError:
		return None
	# check correct conversion
	if len(octets) != length:
		return None
	# check CRC
	if len(octets) == 0:
		return None
	if _crc8(octets) != 0x35:
		return None
	# remove CRC and return
	return octets[:-1]

#
# pack binary data into SMS
#
# 1. append CRC-8
# 2. base-64 encode
#
# returns 'None' on error
#
def sms_pack(octets):
	'pack binary data into text message'
	if len(octets) > MAX_OUTGOING_SIZE:
		return None
	# append CRC
	octets = octets + chr(_crc8(octets) ^ 0xff)
	# convert to base-64
	sms_body = b64encode(octets)
	# remove padding
	length = (len(octets) * 8 + 5) / 6
	return sms_body[:length]

# vi:set ts=3 sw=3 ai:
