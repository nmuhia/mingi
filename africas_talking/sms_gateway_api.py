"""
A set of methods and constants used for sending and receiving messages via the
SMS Gateway API.

https://smsgateway.me/
"""

import requests

from mingi_django.models import Error_Log


SGA_SEND_MESSAGE_URL = "http://smsgateway.me/api/v3/messages/send"

SGA_ACCOUNT_EMAIL = "emoder13@gmail.com"
SGA_ACCOUNT_PASSWORD = "WbX309kEvz"

SGA_CALLBACK_SECRET = "I36Xg62QxYO4qDvP"


def send_sms_gateway_api(telephone, message, device_id):
    """
    Sends the given message to the given telephone number via the given SMS
    Gateway API-registered device.

    :param telephone: The destination telephone number.
    :param message: The message to send.
    :param device_id: The ID of the device to send the message from.
    """
    response = requests.post(
        SGA_SEND_MESSAGE_URL,
        data={
            'email': SGA_ACCOUNT_EMAIL,
            'password': SGA_ACCOUNT_PASSWORD,
            'device': device_id,
            'number': telephone,
            'message': message
        }
    )

    try:
        response_json = response.json()
    except ValueError:
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="Non-JSON response from SMS Gateway API: %s" %
                            response.text
        )
        return

    if len(response_json['result']['fails']) != 0:
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="Failure response from SMS Gateway API: %s" %
                            str(response_json['result']['fails'])
        )
