from decimal import Decimal
import datetime

from mingi_site.models import Line_State, Site_Record, Line, Mingi_Site
from mingi_user.models import Mingi_User
from mingi_django.models import Error_Log, Custom_Timer_Command

from sendSMS import sendSMS
from django.core.mail import EmailMessage


def process_trigger_commands(triggers, new_record):
    """
    determine whether an existing command has been triggered, and perform
    the action as appropriate
    """

    for trigger in triggers:
        if trigger.trigger_type == "temp" or trigger.trigger_type == "voltage":
            process_data_trigger(trigger, new_record)

        elif trigger.trigger_type == "utility_use":
            process_utility_use_trigger(trigger, new_record)
            
        elif trigger.trigger_type == "account_balance":
            process_account_balance_trigger(trigger, new_record)


def process_data_trigger(trigger, record):
    """
    check trigger that has type set to temperature or voltage
    """

    triggered = False
    
    # get threshold value
    try:
        threshold = float(trigger.threshold)
        
        # check whether threshold triggers command
        if trigger.trigger_level == 'less_than':
            
            # compare values for temp and voltage; don't repeat alerts
            if trigger.trigger_type == 'temp':
                if float(record.value.split(',')[1]) < threshold:
                    if trigger.is_triggered == False:
                        triggered = True
                        trigger.is_triggered = True
                        trigger.save()
                else:
                    trigger.is_triggered = False
                    trigger.save()

            elif trigger.trigger_type == 'voltage':
                if float(record.value.split(',')[0]) < threshold:
                    if trigger.is_triggered == False:
                        triggered = True
                        trigger.is_triggered = True
                        trigger.save()
                        
                else:
                    trigger.is_triggered = False
                    trigger.save()

        # check whether threshold triggers command
        elif trigger.trigger_level == 'greater_than':

            if trigger.trigger_type == 'temp':
                if float(record.value.split(',')[1]) > threshold:
                    if trigger.is_triggered == False:
                        triggered = True
                    trigger.is_triggered = True
                    trigger.save()

                else:
                    trigger.is_triggered = False
                    trigger.save()
                    
            elif trigger.trigger_type == 'voltage':
                if float(record.value.split(',')[0]) > threshold:
                    if trigger.is_triggered == False:
                        triggered = True
                    trigger.is_triggered = True
                    trigger.save()

                else:
                    trigger.is_triggered = False
                    trigger.save()

        if triggered:
            # perform the action
            if '@' in trigger.destination:
                email = EmailMessage('Alert Triggered',trigger.message,to=[trigger.destination])
                email.send()
            else:
                try:
                    sendSMS(trigger.destination,trigger.message)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                        
    except TypeError:
        pass
    
    return str(triggered)
    

def process_utility_use_trigger(trigger, record):
    """
    process triggers that have type set to utility use
    """

    triggered = False

    # get threshold value
    try:
        threshold = float(trigger.threshold)

        utility_use = 0.0 # calculate this ***
        repeated_case = False
        
        # check whether threshold triggers command
        if trigger.trigger_level == 'less_than':

            # compare values and update trigger status
            if float(utility_use) > threshold:
                if trigger.is_triggered == False:
                    triggered = True
                    trigger.is_triggered = True
                    trigger.save()

            else:
                trigger.is_triggered = False
                trigger.save()
            
                        
        # check whether threshold triggers command
        elif trigger.trigger_level == 'greater_than':
            
            # compare values and update trigger status
            if float(utility_use) > threshold:
                if trigger.is_triggered == False:
                    triggered = True
                    trigger.is_triggered = True
                    trigger.save()

            else:
                trigger.is_triggered = False
                trigger.save()
            

        if triggered:
            # perform the action
            if '@' in trigger.destination:
                email = EmailMessage('Alert Triggered',trigger.message,to=[trigger.destination])
                email.send()
            else:
                try:
                    sendSMS(trigger.destination,trigger.message,trigger)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                    
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
        
    return str(triggered)
    
        
def process_account_balance_trigger(trigger, record):
    """
    process triggers that have type set to account balance
    """

    triggered = False

    # get threshold value
    try:
        threshold = float(trigger.threshold)

        account_balance = record.account_balance
        repeated_case = False
        
        # check whether threshold triggers command
        if trigger.trigger_level == 'less_than':

            # compare values and update trigger status
            if float(account_balance) < threshold:
                if trigger.is_triggered == False:
                    triggered = True
                    trigger.is_triggered = True
                    trigger.save()

            else:
                trigger.is_triggered = False
                trigger.save()
                        
        # check whether threshold triggers command
        elif trigger.trigger_level == 'greater_than':
            
            # compare values and update trigger status
            if float(account_balance) > threshold:
                if trigger.is_triggered == False:
                    triggered = True
                    trigger.is_triggered = True
                    trigger.save()

            else:
                trigger.is_triggered = False
                trigger.save()

        if triggered:
            # perform the action
            if '@' in trigger.destination:
                email = EmailMessage('Alert Triggered',trigger.message,to=[trigger.destination])
                email.send()
            else:
                try:
                    sendSMS(trigger.destination,trigger.message)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                    
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",problem_message=str(e))
                

    return str(triggered)
