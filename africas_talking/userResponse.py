import calendar
import datetime
import math
from datetime import date, datetime

from decimal import Decimal, InvalidOperation

from django.db.models import Q

from mingi_site.models import Mingi_Site, Line_State, Bit_Harvester, Site_Record
from mingi_django.models import Error_Log, Custom_Timer_Command
from mingi_user.models import Mingi_User
from africas_talking.models import InvalidUserRegistration, InvalidAttributeValue, Africas_Talking_Input
from africas_talking.methods import process_trigger_commands
from sendSMS import sendSMS
from methods import process_trigger_commands


# used in calculating energy used from a meter log; depends on message syntax
MSG_LINE_OFFSET=3
FUEL_CONTACT_PHONE="+254728632085"
FUEL_CONTACT_EMAIL="lameck@steama.co"

EON_CONTACT_NO = "0682018149"

ALPHA_LIST = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]


def commandNotRecognized(user):
    """
    default response if no defined command is recognized
    """

    SMSTo = user.telephone
    respond_to_no = get_respond_to_number()

    # E.ON specific messaging
    if (user.site.company_name == "E.ON"):
        if user.language == 'ENG':
            msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to %s" % EON_CONTACT_NO
        else:
            msgOut = "Samahani, sielewi chaguo lako. Kwa maelezo Zaidi tuma hefuri 'M' Kwenda %s" % EON_CONTACT_NO

    elif (user.language == 'ENG'):
        msgOut = "I'm sorry, I don't recognise that command. For instructions, send 'M' to "+str(respond_to_no)
    elif (user.language == 'SWA'):
        msgOut = "Pole, sielewi. Kupata menu tumia 'M' kwa "+str(respond_to_no)
    else:
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message="Invalid language setting for user: " + user.telephone)
        return

    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))


def sendMenu(user):
    """
    send the Mingi menu to a user
    """
    respond_to_no = get_respond_to_number()
    # E.ON specific messaging
    if user.site.company_name == "E.ON":
        if user.language == 'ENG':
            msgOut = "Rafiki Power Menu: Text M and the option number, eg 'm 1'.\nM 1:Balance\nM 2:Buy Power\nM 3:Customer Services\nTo see this menu any time text M to %s" % EON_CONTACT_NO

        else:
            msgOut = "Menyu ya Rafiki Power - Tuma hefuri 'M 1' kuangalia salio, 'M 2' Kununua umeme na 'M 3' Huduma kwa wateja, kwenda namba %s" % EON_CONTACT_NO

    elif (user.language == 'ENG') and (user.is_user):
        msgOut = "Mingi Menu: Text M and the option number, eg 'm 1'.\nM 1:Balance\nM 2:Buy Power\nM 3:Useful numbers\nTo see this menu any time text M to "+str(respond_to_no)	

    elif (user.language == 'SWA') and (user.is_user):
        msgOut = "Huduma Menu - Jibu kwa mfano 'M 1'\nM 1:Akaunti\nM 2: Kununua umeme\nM 3:Nambari muhimu\nKupata menu tumia 'M' kwa "+str(respond_to_no)

    elif (user.is_site_manager):
	msgOut = "Mingi Menu: To log fuel purchase, text ' M 12, [KES], [LTRS] to "+str(respond_to_no)+"'"
        
    else:
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message="Invalid language/role setting for user: " + user.telephone)
        return

    SMSTo = user.telephone

    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

#formatting, BH,BH_number,ON/OFF,ALL/line_number #BH,BH_number,status
def sendBHStatus(user,bh):
    """
    Get user BH, send a get_status_update message
    """    
    sms_id = "*4" + str(calendar.timegm(datetime.now().utctimetuple()))[3:]
    msgOut = sms_id+",status"    
    SMSTo = bh.telephone
    try:   
        Africas_Talking_Input.objects.create(raw_message=msgOut,
                                             user=user,
                                             timestamp=datetime.now(),
                                             gateway=bh.bh_type,
                                             bit_harvester=None)
        sendSMS(SMSTo, msgOut,bh)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

def turnLinesOn(user,bh,lineNos):
    """
    Get user BH, send a get_status_update message
    """
    lines = ",".join(lineNos)
    sms_id = "*1" + str(calendar.timegm(datetime.now().utctimetuple()))[6:]

    msgOut = sms_id+",ON,"+str(lines)
    SMSTo = bh.telephone
    try:
        Africas_Talking_Input.objects.create(raw_message=msgOut,
                                             user=user,
                                             timestamp=datetime.now(),
                                             gateway=bh.bh_type,
                                             bit_harvester=None)  
        sendSMS(SMSTo, msgOut,bh)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

def turnLinesOff(user,bh,lineNos):
    """
    Get user BH, send a get_status_update message
    """    
    lines = ",".join(lineNos)
    sms_id = "*1" + str(calendar.timegm(datetime.now().utctimetuple()))[6:]        
    msgOut = sms_id+",OFF,"+str(lines)
    SMSTo = bh.telephone
    try:
        Africas_Talking_Input.objects.create(raw_message=msgOut,
                                             user=user,
                                             timestamp=datetime.now(),
                                             gateway=bh.bh_type,
                                             bit_harvester=None)    
        sendSMS(SMSTo, msgOut,bh)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

def sendBalance(user):
    """
    send the user their current account balance
    """

    balance = user.account_balance

    # don't show a customer their negative balance
    if (balance < 0):
        balance = 0;

    fName = user.first_name
    SMSTo = user.telephone
    respond_to_no = get_respond_to_number()
    
    # ENGLISH OPTIONS
    if (user.language == 'ENG'):
        if (user.site_manager) and (user.site_manager.last_name == "Customer Care"):
            msgOut = "Hi %s, your energy balance is %d energy credits. To see the Mingi Menu text M to %s." % (fName,int(balance),respond_to_no)
        # E.ON specific messaging
        elif user.site.company_name == "E.ON":
            msgOut = "Hi %s, your energy balance is %.1f Rafiki Credits. To see the Rafiki Power Menu text M to %s" % (fName,balance,EON_CONTACT_NO)
        else:
            if user.bit_harvester and user.bit_harvester.version == "V4":
                timestamp = str(get_last_update_time(user))
                msgOut = "Hi %s, your energy balance was KSh %.2f at %s. To see the menu text 'M' to %s" % (fName,balance,timestamp,respond_to_no)
            else:
                msgOut = "Hi %s, your energy balance is KSh %.2f. To see the Mingi Menu text M to %s." % (fName,balance,respond_to_no)

    # SWAHILI OPTIONS
    elif (user.language == 'SWA'):
        if (user.site_manager) and (user.site_manager.last_name == "Customer Care"):
            msgOut = "Hi %s, akaunti yako ni %d credit ya stima. Kupata menu tumia 'M' kwa %s" % (fName,int(balance),respond_to_no)
        elif user.site.company_name == "E.ON":
            msgOut = "Habari, %s, salio la akaunti yako ni %.1f kredit. Kupata menu tuma hefuri 'M' Kwenda %s" % (fName,balance,EON_CONTACT_NO)
        else:
            if user.bit_harvester and user.bit_harvester.version == "V4":
                timestamp = str(get_last_update_time(user))
                msgOut = "Hi %s, akaunti yako ilikuwa KSh %.2f katika %s. Kupata menu tumia 'M' kwa %s" % (fName,balance,timestamp,respond_to_no)
            else:
                msgOut = "Hi %s, akaunti yako ni KSh %.2f. Kupata menu tumia 'M' kwa %s" % (fName,balance,respond_to_no)

            
    else:
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message="Invalid language setting for user: " + user.telephone)
        return
    
    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))


def get_last_update_time(user):
    """
    get datetime when the user's account balance was last updated (for v4)
    """

    BH = user.bit_harvester
    
    if user.control_type == "AUTOL":
        try:
            last_message = Africas_Talking_Input.objects.filter(bit_harvester=BH,raw_message__contains="CREDIT").order_by('-timestamp')[0]
        except IndexError:
            return " "
    elif user.control_type == "AUTOC":
        try:
            last_message = Africas_Talking_Input.objects.filter(bit_harvester=BH,raw_message__contains="METERS").order_by('-timestamp')[0]
        except IndexError:
            return " "
    else:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="Invalid Control Type: %s for user: %s" % (user.control_type,str(user)))
        return " "

    return datetime.strftime(last_message.timestamp,'%d-%b %H:%M')


def sendBuyPower(user):
    """
    send the user instructions for how to buy power
    """
    SMSTo = user.telephone
    # E.ON specific messaging
    

    # temporary client-specific code
    if (user.site_manager) and (user.site_manager.telephone == "+254707921163"):
        tillNo = "397266"
    elif user.site.company_name=="Vulcan":
        tillNo = "541048"
    else:
        tillNo = "934135"

    # E.ON specific messaging
    if (user.site.company_name == "E.ON"):
        if user.language == 'ENG':
            msgOut = "Hi %s, to buy more Rafiki Credits using Airtel Money please follow Airtel Money sending instructions to %s" % (user.first_name, EON_CONTACT_NO)
        else:
            msgOut = "Habari, %s, kuongeza salio kwenye akaunti yako ya Rafiki Power kwa kutumia Airtel Money, tafadhali fuata maelekozo ya kutuma pesa, kwenda namba %s" % (user.first_name, EON_CONTACT_NO)

    elif (user.language == 'ENG'):
        msgOut = "To buy power, select PAYMENT SERVICES -> BUY GOODS on the MPesa menu. Enter till number: %s, and follow the instructions to top up!" % tillNo
    elif (user.language == 'SWA'):
        msgOut = "Kununua umeme, enda kwa MPesa menu, chagua PAYMENT SERVICES, enda kwa BUYGOODS. Jaza nambari %s. Fuata maagizo kuongeza akaunti yako!" % tillNo
    else:
        Error_Log.objects.create(problem_type='INTERNAL',
                                 problem_message="Invalid language setting for user: " + user.telephone)
        return

    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

def sendNumbers(user):
    """
    send the user a list of useful phone numbers
    """
    SMSTo = user.telephone

    if user.site_manager is not None:
        siteManager = user.site_manager.first_name
        siteManagerPhone = user.site_manager.telephone

        # E.ON specific messaging
        if user.site.company_name == "E.ON":
            if user.language == 'ENG':
                msgOut = "To contact customer services please call %s" % siteManagerPhone
            else:
                msgOut = "Kuwasiliana na huduma kwa wateja, piga namba %s" % siteManagerPhone

        elif (user.language == 'ENG'):
            msgOut = "Your site manager is %s, phone number %s." % (siteManager,siteManagerPhone)
        elif (user.language == 'SWA'):
            msgOut = "Mwakilishi yako ni %s, nambari ya simu %s." % (siteManager,siteManagerPhone)
        else:
            Error_Log.objects.create(problem_type='INTERNAL',
                                     problem_message="Invalid language setting for user: " + user.telephone)
            return
    
    else:
        msgOut = "There is no site manager registered in your account."

    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
        
def registerNewUser(user,msgIn):
    """
    respond to a user's attempt to register a new customer
    """

    msgContents = msgIn.split(",")

    if (len(msgContents) != 6):
        msgOut = "To register a user, reply with: 'M 6,first name,last name,phone number,line number,language (E or S)"
        
    else:
        fName = msgContents[1].strip()
        lName = msgContents[2].strip()
        phoneNo = msgContents[3].strip()
        lineNo = msgContents[4].strip()
        lang = msgContents[5].strip()
        
        try:
            newUser = createNewUser(fName,lName,phoneNo,lineNo,lang,user)
            msgOut = "Thank you for registering %s %s. A message has been sent to them, and you will be alerted when they confirm registration" % (fName,lName)

        except Exception as e:
            if (e.msg == "Invalid user registration!: User has invalid number"):
                msgOut = "Error: the phone number you entered could not be recognised. Please try again with number format eg. 0712345678"
            elif (e.msg == "Invalid user registration!: User has repeated number"):
                msgOut = "Error: this person is already registered with access:energy"
            else:
                errorMsg = "createNewUser returned an unknown exception type"
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message=errorMsg)
                msgOut = "I'm sorry, I don't recognise this command. To see the Mingi Menu text M to Mingi"
            
        
    SMSTo = user.telephone
    
    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
            

def logMeterUpdate(user,msgIn,timestamp,forceUse=False):
    """
    respond to a meter update by updating relevant user accounts 
    if log is valid, and sending the appropriate response
    """

    msgElements = msgIn.split('-')
    site = user.site
    #timestamp = datetime.datetime.now()

    # check to make sure the message is valid
    response = scanLogForErrors(msgElements,site,forceUse)

    if (response == "success"):
        siteName = site.name
        numLines = site.num_lines

        noAccountHandled = 0 # no of lines with no account found, for diagnostics

        today = datetime.today()
        days_ago = datetime.today() - datetime.timedelta(days=30)
        line_states = Line_State.objects.filter(site=site,
                                                timestamp__gte=days_ago,
                                                timestamp__lte=today).order_by('-timestamp')
        line_states = line_states.exclude(meter_reading=None)

        last_states = []
        for line in range(1,site.num_lines+1):
            last_states.append([line,None])

        state_count = 0
        for state in line_states:
            index = state.number - 1
            if (last_states[index][1] == None) and (state.meter_reading):
                last_states[index][1] = state
                state_count += 1

            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message=str(last_states[index]))
                
            if state_count == len(last_states):
                break
            
        # log the voltage (***currently the 2nd message element***)
        voltageString = msgElements[1]
        Site_Record.objects.create(site=site,key="System Voltage",
                                   value = voltageString, timestamp=timestamp)

        line_states = Line_State.objects.filter(site=site)
        ls_num = len(line_states)
        for i in range(0,numLines):
            msgIndex = i + MSG_LINE_OFFSET
            line = int(i + 1) # lines are 1-indexed

            # scale by either a user-specific pulse ratio or site default
            if user.pulse_ratio:
                reading = float(msgElements[msgIndex].strip()) * user.pulse_ratio
            else:
                reading = float(msgElements[msgIndex].strip()) * site.pulse_ratio
            try:
                last_reading = last_states[i][1].meter_reading
            except IndexError:
                last_reading = reading
                
            NRGused = Decimal(reading - last_reading)
                
            try:
                lineUser = findUserByLine(line,siteName)
                
                # ignore the log if the BH is in charge of logging use
                if (user.meter_log_party == 'SM'):
                    response = handleEnergyUse(lineUser,NRGused,line,reading,timestamp)

                else:
                    Line_State.objects.create(site=site,number=line,
                                              meter_reading=reading,
                                              timestamp=timestamp)
                   
            except Mingi_User.MultipleObjectsReturned:
                noAccountHandled += 1
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="Multiple users registered on line %d in %s" % (line,site.name))
                Line_State.objects.create(site=site,number=line,
                                          meter_reading=reading,
                                          timestamp=timestamp)
                
            except Mingi_User.DoesNotExist:
                noAccountHandled += 1 
                Line_State.objects.create(site=site,number=line,
                                          meter_reading=reading,
                                          timestamp=timestamp)

        msgOut = "Update logged successfully, keep up the good work!"
        
    elif (response == "invalid length"):
        
        msgOut = "I'm sorry, you have entered an incorrect number of items. Please try again and make sure you log every line once."
        noAccountHandled = site.num_lines

    else:
        site.last_attempted_log = msgIn
        site.save()
        
        msgOut = "I have found possible errors in your log. You logged:\n"
        noAccountHandled = site.num_lines

        for error in response:
            msgOut += "line %d as %.2f\n" % (error[0],error[1])
            
        msgOut += "If this is correct reply 'm yes', otherwise please send a new log"

    try:
        sendSMS(user.telephone,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

    return noAccountHandled # can be used for error checking


def logProblem(user,msgIn):
    """
    create a log of a problem reported by SMS
    """

    msgContents = msgIn.split("-")
    SMSTo = user.telephone
    
    # if format is incorrect, send instructions
    if (len(msgContents) != 3):
        msgOut = "To report a problem, reply with 'M 8 - type of problem - equipment affected"

    # otherwise, create a log of the problem
    else:
        Error_Log.objects.create(problem_type="EQUIPMENT",creator=user.telephone,
                                 problem_message=msgContents[1].strip() + ": " + msgContents[2].strip())
        msgOut = "Thank you for reporting this problem. An access:energy technician will contact you shortly."

    try:
        sendSMS(SMSTo,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                 problem_message=str(e))


def logLineStatus(user,msgIn):
    """
    change the status of a line in user's site to match msgIn
    """

    site = user.site
    msgElements = msgIn.split("-")

    # if message format is incorrect, send instructions
    if (len(msgElements) != 3):
        try:
            sendSMS(user.telephone,"To log a user's line status, reply 'M 9 - ON/OFF - line #'",user)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",
                                     problem_message=str(e))
    else:
        # special case for the generator
        if msgElements[2].strip().upper() == "GEN":
            status = msgElements[1].strip().upper()
            if status == "ON":
                site.generator_is_on = True
            elif status == "OFF":
                site.generator_is_on = False
            site.save()
            try:
                sendSMS(user.telephone,
                        "Thank for logging the generator as %s" % status,user)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))
            return
        # standard case for numbered lines
        try:
            lineNo = int(msgElements[2].strip())
            status = msgElements[1].strip().upper()

            try:
                customer = findUserByLine(lineNo,site.name)

                # change the line status if applicable
                if (customer.line_control_party == 'SM'):
                    if (status == "OFF"):

                        msgOut = "Thank you for logging Line %d as OFF!" % lineNo

                        if (customer.line_is_on == True):
                            customerMsg = "Your line has been turned off! Please top up your account to receive power"
                            try:
                                sendSMS(customer.telephone,customerMsg,customer)
                            except Exception as e:
                                Error_Log.objects.create(problem_type="SEND_SMS",
                                                         problem_message=str(e))
                        
                            customer.line_is_on = False
                            customer.save()
                    
                    elif (status == "ON"):
                        
                        msgOut = "Thank you for logging Line %d as ON!" % lineNo
                        
                        if (customer.line_is_on == False):
                            customerMsg = "Your power has been turned on, enjoy!"
                            try:
                                sendSMS(customer.telephone,customerMsg,customer)
                            except Exception as e:
                                Error_Log.objects.create(problem_type="SEND_SMS",
                                                         problem_message=str(e))
                            customer.line_is_on = True
                            customer.save()
                        
                    else:
                        msgOut = "To log a user's line status, reply 'M 9 - ON/OFF - line #"
                    
                    try:
                        sendSMS(user.telephone,msgOut,user)
                    except Exception as e:
                        Error_Log.objects.create(problem_type="SEND_SMS",
                                                 problem_message=str(e))

            except:
                Error_Log.objects.create(problem_type="OTHER",
                                         problem_message="System tried to turn on an unattached line")
            
        except:
            msgOut = "To log a user's line status, reply 'M 9 - ON/OFF - line #" 
            try:
                sendSMS(user.telephone,msgOut,user)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

def logSystemVoltage(user,msgIn,now):
    """
    Record an update of the system voltage
    """

    msgElements = msgIn.split("-")
    
    if (len(msgElements) != 2):
        msgOut = "To log the system voltage, reply with 'M 10' plus the voltage in this format: M 10 - XX.XX"

    else:
        voltage = msgElements[1].strip()
        voltageRecord = voltage

        Site_Record.objects.create(site=user.site,key="System Voltage",
                                   value=voltageRecord, timestamp=now)
        msgOut = "The battery level has been logged as %s, thanks!" % voltage
        
    try:
        sendSMS(user.telephone,msgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                 problem_message=str(e))


#validate numeric input
def validate_number(value):
	try:
		float(value)
		return True
	except Exception as e:
		Error_Log.objects.create(problem_type="VALUE_ERROR",problem_message=str(e))
		return False
	

def creditAccount(user,amount):
    # calculate new balance
    oldBalance = user.account_balance
            
    if (oldBalance < 0):
        user.account_balance = Decimal(amount)
    else:
        user.account_balance = Decimal(oldBalance) + Decimal(amount)

    user.save()


def companyNamePoll(user):
	"""
        Company name suggestions from customers
	"""        
	#check whether a user already has another entry in the DB, if not credit account           
        name_poll = Africas_Talking_Input.objects.filter(Q(user=user),Q(raw_message__startswith='NAME')| Q(raw_message__startswith='JINA'))

        if(name_poll.count()>1):
            print "ALREADY CREDITED"
        else:
            credit_criteria = int(user.telephone[-3:])
            if(credit_criteria > 500):
                print "CREDIT THIS ACCOUNT WITH 100KES"
                creditAccount(user,100)
                try:
                    sendSMS(user.telephone,"Thank you for your participation.Your account has been credited with Ksh.100",user)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                             problem_message=str(e))                
                    
            else:
                print "CREDIT ACCOUNT WITH 50 KES"
                creditAccount(user,50)
                try:
                    sendSMS(user.telephone,"Thank you for your participation.Your account has been credited with Ksh.50",user)        
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",
                             problem_message=str(e))
                
       							

def logFuelPurchase(user,msgIn,now):
	"""
	Record purchase of fuel
	"""
	msgElements=msgIn.split(",")

	
	#check message format
	if (len(msgElements)!=3) or (not msgElements[1].is_number) or (validate_number(msgElements[2]) == False):
		try:
			sendSMS(user.telephone,"To log fuel purchase, reply 'M 12 , [KSh] , [Litres]'",user)
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

	#send message(email and sms) to fuel contact 
	else:
		message= "By: \n" + user.first_name + " " + user.last_name +"\nContact: " + user.telephone + "\nFuel Cost(KES): "+msgElements[1]+"\n Fuel Amount(Ltrs):"+msgElements[2]

		#add fuel purchase to db
		Site_Record.objects.create(site=user.site,key="Fuel Purchase",value="KES: "+msgElements[1]+",LTRS: "+msgElements[2], timestamp=now)
		try:
			#send SMS to fuel contact
			sendSMS(FUEL_CONTACT_PHONE,message)	
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
		try:
			#send email to fuel contact
			subject = "Fuel Purchase Alert "				
			email = EmailMessage(subject,message,to=[FUEL_CONTACT_EMAIL])
			email.send()

		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_EMAIL",problem_message=str(e))
		
		try:
			msgOut = "The fuel purchase of %s ltrs at KES %s has been logged successfully, thanks!" % (msgElements[2], msgElements[1])
			sendSMS(user.telephone,msgOut,user)
		except Exception as e:
			Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))										



def confirmUserRegistration(user):
    """
    mark a user as active when they confirm their registration
    """

    user.is_active = True;
    user.save()

    if user.site.company_name == "E.ON":
        if user.language == 'ENG':
            customerMsgOut = "Thank you for registering with %s. Please text 'M' to %s to get more information." % (user.site.operator_name, EON_CONTACT_NO)
        else:
            customerMsgOut = "Asante kwa kujisajili, mimi ni %s. Tuma hefuri 'M' Kwenda namba %s kwa maelezo zaidi." % (user.site.operator_name, EON_CONTACT_NO)
    else:
        customerMsgOut = "Thank you for registering! I'm Mingi. I'm here to help you get more energy from %s. Save my number so you can SMS me any time." % user.site.operator_name
    
    smMsgOut = "%s %s is now registered, good job!" % (user.first_name,user.last_name)

    try:
        sendSMS(user.telephone,customerMsgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))

    try:
        sendSMS(user.site_manager.telephone,smMsgOut,user)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                         problem_message=str(e))
    
                    

def createNewUser(fName,lName,phoneNo,lineNo,lang,registeredBy):
    """
    create a new user in the database with the correct default information
    """

    try:
        formattedPhoneNo = processPhoneNo(phoneNo)
        
        energyPrice = registeredBy.energy_price
        site = registeredBy.site
        siteManager = registeredBy.site_manager
        paymentPlan = registeredBy.payment_plan

        # default conditions for meter logging and line control
        bit_harvesters = Bit_Harvester.objects.filter(site=site)
        version = bit_harvesters[0].version

        if len(bit_harvesters) > 0:
            MLP = "BH"
            LCP = "BH"
            BH = bit_harvesters[0]
        else:
            MLP = "SM"
            LCP = "SM"
            BH = None
        if version == "V4" and int(lineNo) < (site.num_lines - 3):
            control_type = "AUTOL"
        else:
            control_type = "AUTOC"

        if (lang == 'E') or (lang == 'e'):
            lang = 'ENG'
        elif (lang == 'S') or (lang == 's'):
            lang = 'SWA'
        else:
            lang = 'ENG'

        if (paymentPlan) and (paymentPlan.split(',')[0] == "PowerGen Default"):
            try:
                startBalance = float(paymentPlan.split(',')[1]) * -1
            except Exception:
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message="Invalid payment plan details for registerer: %s" % paymentPlan)
                startBalance = 0.00
        else:
            startBalance = 0.00
                                       
            
        returnUser = Mingi_User.objects.create(telephone=formattedPhoneNo,
                                               first_name=fName,
                                               last_name=lName,
                                               account_balance=startBalance,
                                               energy_price=energyPrice,
                                               site=site,line_number=lineNo,
                                               site_manager=siteManager,
                                               payment_plan=paymentPlan,
                                               meter_log_party=MLP,
                                               line_control_party=LCP,
                                               control_type=control_type,
                                               bit_harvester=BH,language=lang)

        sendRegisterMessage(registeredBy.first_name+" "+registeredBy.last_name,
                            fName+" "+lName,site.operator_name,formattedPhoneNo,lang)

        return returnUser

    except InvalidUserRegistration as e:
        if (e.msg == "Invalid user registration!: User has invalid number"):
            raise InvalidUserRegistration(1)
        elif (e.msg == "Invalid user registration!: User has repeated number"):
            raise InvalidUserRegistration(2)
        else:
            raise InvalidUserRegistration()


def sendRegisterMessage(registerName,userName,operator,formattedPhoneNo,language='ENG'):
    """
    send welcome message to newly-registered client
    """
    
    if (operator == "Rafiki Power"):
        if language == 'ENG':
            msgOut = "Hi, I'm %s. We want to register you with us. If your name is %s, send 'M yes' to %s to confirm" % (operator,userName,EON_CONTACT_NO)
        else:
            msgOut = "Habari, Mimi ni %s. Karibu kwenye usajili akaunti yako. Kama jina lako ni %s, tuma neno 'M yes' kwenda namba %s" % (operator,userName,EON_CONTACT_NO)

    else:
        msgOut = "Hi, I'm Mingi from %s. %s wants to register you with %s. If your name is %s, reply 'M yes' to confirm" % (operator,registerName,operator,userName)
    
    try:
        sendSMS(formattedPhoneNo,msgOut)
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS",
                                 problem_message=str(e))


def validatePhoneNo(phoneNo,prefix="+254"):
    """
    Check phone number formatting
    """
    # convert the number to a uniform format if possible
    if (len(phoneNo) == 10) and phoneNo[0] == '0':
        phoneSubstr = phoneNo[1:10]
        toReturn = prefix + phoneSubstr

    # don't check for + to account for Nepal phone Nos
    elif (len(phoneNo) == 13):
        toReturn = phoneNo
        
    # allow Benin phone numbers
    elif phoneNo[1:4] == "229":
    	toReturn = phoneNo

    # allow all Benin phone numbers
    elif phoneNo[1:4] == "229":
        toReturn=phoneNo

    else:
        raise InvalidUserRegistration(1) 
    
    return toReturn


def processPhoneNo(phoneNo):
    """
    ensure the phone number is valid and not a repeat, and return it in the desired format
    """

    #validate phone number
    toReturn = validatePhoneNo(phoneNo)

    # ensure the number is not a repeat
    try:
        Mingi_User.objects.get(telephone=toReturn)
        raise InvalidUserRegistration(2)
    except Mingi_User.DoesNotExist:
        try:
            Bit_Harvester.objects.get(telephone=toReturn)
            raise InvalidUserRegistration(2)
        except Bit_Harvester.DoesNotExist:
            return toReturn
        except Bit_Harvester.MultipleObjectsReturned:
            raise InvalidUserRegistration(2)
    except Mingi_User.MultipleObjectsReturned:
        raise InvalidUserRegistration(2)

    else:
        return toReturn


def findUserByLine(lineNo,siteName):
    """
    return the user associated with line=lineNo in site=siteName
    """

    line = str(lineNo)
    containsString = "," + line + ","
    startsString = line + ","
    endsString = "," + line

    return Mingi_User.objects.get(Q(line_number__startswith=startsString) |
                                  Q(line_number__endswith=endsString) |
                                  Q(line_number__contains=containsString) |
                                  Q(line_number__exact=line),
                                  site__name=siteName)

def findLineUser(lineNo,site):
    return Line.objects.get(site=site,number=lineNo).user

# DEPRECATED FOR BITHARVESTERS
def scanLogForErrors(msgElements,site,forceUse=False):
    """
    analyze the meter log for possible errors and flag them
    """

    # number of lines must match
    if ((site.num_lines + MSG_LINE_OFFSET) != len(msgElements)):
        return "invalid length"

    elementIndex = MSG_LINE_OFFSET
    possibleErrors = []

    # check energy use values
    for lineNo in range(1, site.num_lines+1):

        # if one of the readings is NaN, treat as a message of incorrect length
        try:
            reading = float(msgElements[elementIndex].strip()) * site.pulse_ratio

        except InvalidOperation:
            return "invalid length"
        
        try:
            last_reading = Line_State.objects.filter(site=site,number=lineNo,
                                                     meter_reading__isnull=False)[0].meter_reading

        except IndexError:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="No matching line state found for " + str(site) + ": Line " + str(lineNo))
            last_reading = reading
 
        # energy used shouldn't be zero
        if (last_reading > reading):
            possibleErrors.append([lineNo,reading])

        elementIndex = elementIndex + 1

        # add in stats-based checks later -------------

    if (len(possibleErrors) > 0) and (forceUse == False):
        return possibleErrors

    else:
        return "success"


def handleEnergyUse(user,NRGused,lineNo,reading,timestamp,other_ts=None):
    """
    respond to a user's energy use by decrimenting their account
    and sending the appropriate response messages
    do not update account balance of user under MANUAL control type
    """
    if user.control_type == "MAN":
        pass

    else:

        if(user.first_name == "Water:" or user.first_name == "Electricity:" or user.first_name == "Fuel:"):
            user.account_balance = user.account_balance + (user.energy_price * NRGused)

            # will have to go to a different phone number
            """
            if user.account_balance >= user.low_balance_level:
                handleLowBalance(user,user.line_number)
                """

        # edit the user's account, taking into account time-of-use if necessary
        elif user.TOU_hours:
            ratio = user.current_tou_ratio
            deduction = user.energy_price * Decimal(ratio) * NRGused
            user.account_balance = user.account_balance - deduction
        else:
            user.account_balance = user.account_balance - (user.energy_price * NRGused)

        user.save()
    
    ls = Line_State.objects.create(site=user.site,number=lineNo,user=user,
                                   meter_reading=reading,
                                   account_balance=user.account_balance,
                                   timestamp=timestamp,
                                   processed_timestamp=other_ts)
    # check for relevant account balance triggers
    triggers = Custom_Timer_Command.objects.filter(Q(user=user),Q(trigger_type='account_balance') | Q(trigger_type='utility_use'))
    if len(triggers) > 0:
        process_trigger_commands(triggers,ls)
    
    # send response messages
    if (user.payment_plan):
        planDetails = user.payment_plan.split(',')
        if (planDetails[0] == "High Energy User"):
            try:
                threshold = float(user.payment_plan.split(',')[1])
            except Exception:
                Error_Log.objects.create(problem_type="INTERNAL",
                                         problem_message="Invalid threshold for High Energy User: " + str(user),
                                         site=user.site)
                threshold = 0;

        elif (planDetails[0] == "Commercial User"):
            threshold = 0;

        elif (planDetails[0] == "PowerGen Default"):
            threshold = 0
            if (NRGused > 0):
                Error_Log.objects.create(problem_type="EXTERNAL",
                                         problem_message="Alert! User: " + str(user) + " has not paid connection fee and therefore should not have energy use",
                                         site=user.site)

        elif (planDetails[0] == "Monthly Rate") or (planDetails[0] == "Subscription Plan"):
            threshold_price = planDetails[1]
            threshold = 0
            if user.account_balance < threshold_price and user.line_is_on:
                # turn line off
                pass
            elif user.account_balance >= threshold_price and not user.line_is_on:
                # turn line on
                pass

        else:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message="Invalid payment plan details for user: " + str(user),
                                     site=user.site)
            threshold = 0;
    else:
        threshold = 0;

    # for BM, return before messages can be sent
    if (user.first_name == "Water:" or user.first_name == "Electricity:" or user.first_name == "Fuel:"):
        return user.account_balance
        
    if (user.account_balance <= threshold):
        try:
            handleZeroBalance(user,lineNo)
        except InvalidAttributeValue as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message=e.msg)
            
    elif (user.account_balance < user.low_balance_level):
        handleLowBalance(user,lineNo)
        
    # check for line status errors
    if (NRGused > 0) and (user.line_is_on == False):
        try:
            handleLineStatusConflict(user,lineNo)
        except InvalidAttributeValue as e:
            Error_Log.objects.create(problem_type="INTERNAL",
                                     problem_message=e.msg)
            
    return user.account_balance


def handleZeroBalance(user,lineNo):
    """
    send alert to the user and command to turn user's line off
    """

    userLineNos = user.line_number.split(",")

    # wait until the user's last line to send all alerts
    if (int(userLineNos[-1]) == lineNo) and (user.line_is_on == True):

        """
        # send an alert to the user
        userNo = user.telephone
        userMsg = "Your energy balance is zero and your power will be turned off. Please top up your account to turn your power back on!"
        
        try:
            sendSMS(userNo,userMsg)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
            """
            
        # send line off command to correct party in correct format
        for line_number in userLineNos:
            if (user.line_control_party == 'SM'):
                controlNo = user.site_manager.telephone
                controlMsg = "The user balance for Line %s has fallen below zero! Please turn off Line %s" % (line_number,line_number)
               
                # assume for now that if line_control_party is 'BH', it will be handled separately by the BH
                """
                elif (user.line_control_party == 'BH'):
                
                controlNo = user.bit_harvester.telephone
                
                if (user.bit_harvester.bh_type == 'AT'):
                    controlMsg = "control,%d,OFF" % (int(line_number) - 1)
                elif (user.bit_harvester.bh_type == 'TW'):
                    controlMsg = "<1256:S:%s:0>" % line_number
                else:
                    raise InvalidAttributeValue("Bit_Harvester","bh_type",
                                                user.bit_harvester.bh_type)
                
                                                else:
                                                raise InvalidAttributeValue("Mingi_User","line_control_party",
                                                user.line_control_party)
                                                """
                try:
                    sendSMS(controlNo,controlMsg,user)
                except Exception as e:
                    Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
    

def handleLowBalance(user,lineNo):
    """
    send warning to the user and update their low balance setting
    """

    # only send a warning on the user's last line
    if(user.first_name == "Water:" or user.first_name == "Electricity:" or user.first_name == "Fuel:"):
        user_is_utility = True
    else:
        user_is_utility = False

    userLineNos = user.line_number.split(",")

    if (int(userLineNos[-1]) == lineNo) and not user_is_utility:
        # don't send to Monthly Rate customers
        if user.payment_plan.split(',')[0].strip() == "Monthly Rate":
            return
        # send warning message
        SMSTo = user.telephone

        # E.ON specific messaging
        if user.site.company_name == "E.ON":

            if user.language == 'ENG':
                msgOut = "Your balance is getting low! You only have %.2f Rafiki Credits left. Top up your account now to keep your power on!" % user.account_balance
            else:
                msgOut = "Salio la akaunti yako inapungua! Una %.2f kredit. Ongeza nauli ili kuendeleza matumizi yako." % user.account_balance

        elif user.language == 'ENG':
            msgOut = "Your balance is getting low! You only have KSh %.2f left. Top up your account now to keep your power on!" % user.account_balance

        else:
            msgOut = "Salio la akaunti yako inapungua! Una %.2f credit. Ongeza nauli ili kuendeleza matumizi yako." % user.account_balance
        
        try:
            sendSMS(SMSTo,msgOut,user)
        except Exception as e:
            Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
            
        # reset low balance setting
        midSetting = user.low_balance_warning * 2/3
        lowSetting = user.low_balance_warning * 1/3
            
        if (user.low_balance_level > midSetting):
            user.low_balance_level = midSetting
            user.save()
        elif (user.low_balance_level > lowSetting):
            user.low_balance_level = lowSetting
            user.save()
        else:
            user.low_balance_level = 0
            user.save()

    else:
        print "NOTIFY REQUEST.USER'S EMAIL ADDRESS"
            

def handleLineStatusConflict(user,lineNo):
    """
    alert the agent (and a:e?) if a line status may be incorrect
    """

    activeLine = user.line_is_on
    
    # ask SM to check on the user's line
    if (user.line_control_party == 'SM'):
        fromParty = user.site_manager
        
        if (activeLine == False) and (user.account_balance > 0):
            msgOut = "Line %d is logged as OFF but appears to be using power. Please make sure Line %d is turned ON" % (lineNo,lineNo)

            try:
                sendSMS(fromParty.telephone,msgOut,user)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

        elif (activeLine == False) and (user.account_balance <= 0):
            msgOut = "Line %d is logged as OFF but appears to be using power. Please make sure Line %d is turned OFF" % (lineNo,lineNo)

            try:
                sendSMS(fromParty.telephone,msgOut,user)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

        # add in case to catch inactive lines that are logged as ON
    

    # try to resolve the conflict by resetting the line with the BH
    """ # this is done elsewhere for bitHarvester logs
    elif (user.line_control_party == 'BH'):
        fromParty = user.bit_harvester
        id_num = str(datetime.datetime.now().hour) + str(datetime.datetime.now().second)
        
        if (activeLine == False) and (user.account_balance > 0):
            
            msgOut = "*1%s,ON,%d" % (id_num,lineNo)

            try:
                sendSMS(fromParty.telephone,msgOut)
                # when you come back to delete this, change id assignment***
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))

        elif (activeLine == False) and (user.account_balance <= 0):

            msgOut = "*1%s,OFF,%d" % (id_num,lineNo)
            
            if (fromParty.bh_type == 'AT'):
            msgOut = "control,%d,OFF" % (lineNo - 1)
            elif (fromParty.bh_type == 'TW'):
            msgOut = "<1256:S:%d:0>" % lineNo
            else:
            # raise InvalidAttributeValue("Bit_Harvester","bh_type",
            fromParty.bh_type)
            
            try:
                sendSMS(fromParty.telephone,msgOut)
            except Exception as e:
                Error_Log.objects.create(problem_type="SEND_SMS",problem_message=str(e))
                """
                

    # send email to a:e people? make a log?

def generate_ref_code():
    """
    Generate randomized reference code based on algorithm for tracking cash payments
    """

    digits = str(datetime.now().microsecond)[1:]
    for digit in digits:
    	pass
    digits2 = str(datetime.now().microsecond)[1:]
    
    """
    last_digit = int(digits[-1])
    string = ""
    for digit in digits:
        digit = int(digit)
        mult = int(math.ceil(digit * last_digit / 2))
        string += ALPHA_LIST[mult]
        last_digit = digit
       	"""

    return str(digits + digits2)
    


def validate_ref_code(code):
    """
    Ensure reference code is valid
    """

    # check for repeat message
    try:
        Kopo_Kopo_Message.objects.get(reference=code)
        return "repeat"

    # *** LATER: VALIDATE ALGORITHM ***
    except Kopo_Kopo_Message.DoesNotExist:
        return "success"

def get_respond_to_number():
    try:
        respond_to_no = user.bit_harvester.input_gateway.telephone if user.bit_harvester.input_gateway.telephone is not None else '20880'
    except:
        respond_to_no = '20880'
    return respond_to_no
    
    
        
