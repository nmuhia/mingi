import datetime
import json
import re
import urllib

from decimal import Decimal, InvalidOperation

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, Http404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from africas_talking.sendSMS import locateUser, UserNotFound
from africas_talking.models import Africas_Talking_Input
from kopo_kopo.handlePayment import create_preregistered_user, \
    callPayPlanFunction
from kopo_kopo.models import Kopo_Kopo_Message
from mingi_django.timezones import zone_aware
from mingi_django.models import Error_Log
from mingi_site.models import Bit_Harvester, Mingi_Site
from mingi_user.models import Mingi_User

from handleSMS import respondToBH, respondToUser, respond_to_solo
from ReportV4 import sms_unpack_as_v3

from .sms_gateway_api import SGA_CALLBACK_SECRET

# Map secret code to processor
GATEWAY_MAPPER = {
    'default':'+254727513104',
    'ke':'+254727513104',
    'tz':'+255727513104',
}

BENIN_SECRET_CODE = "224400"


@csrf_exempt
def at_handler(request):
    """
    Handles an API call from Africa's Talking
    """
    if not request.POST:
        return HttpResponse("unknown request")

    senderNo = request.POST['from']
    msgIn = str(urllib.unquote(request.POST['text']))

    # handle new short code which doesn't strip M prefix
    if (len(msgIn) > 1):
        if (msgIn[0] == "M") and (msgIn[1] == ' '):
            msgIn = msgIn.lstrip("M ")
    elif msgIn == "M":
        msgIn = ""

    process_sms_info(senderNo, msgIn)

    return HttpResponse("200 OK")


@csrf_exempt
def tw_handler(request):
    """
    Handles an API call from Twilio
    """
    if not request.POST:
        return HttpResponse("unknown request")

    senderNo = request.POST['From']
    msgIn = request.POST['Body']

    # strip out M prefix
    if (len(msgIn) > 1):
        if (msgIn[0] == "M") and (msgIn[1] == ' '):
            msgIn = msgIn.lstrip("M ")
    elif msgIn == "M":
        msgIn = ""

    process_sms_info(senderNo, msgIn)

    return HttpResponse("200 OK")


@csrf_exempt
def tz_msg_handler(request):
    """
    collect SMS from Tanzania Android server and process as messages or payments
    """
    if not request.POST:
        return HttpResponse("unknown request")

    # check if parameters fit format of a SMSSync SMS notification
    try:
        messageFound = False
        sender = request.POST['from']
        message_id = request.POST['message_id']
        secret = request.POST['secret']
        sent_timestamp = request.POST['sent_timestamp']
        message = request.POST['message'].replace('\n','').replace('\r','')
        messageFound = True
        
        # verify ID code
        if secret == settings.SMSSYNC_SECRET:
            now = datetime.datetime.now()

            # check whether message fits format of an Airtel Money payment
            try:
                [amount, sender, ref] = parse_payment_message(message)
                process_kopo_kopo_payment_info(amount, sender, message, ref)
            except IndexError as e:
                # assume it's an SMS, and process through AT functions; special case for main menu
                if message == "m" or message == "M":
                    process_sms_info(sender,"")                

                # first remove leading "M" for consist formatting
                elif message[0] == 'M' and message[1] == ' ':
                    message = message.lstrip("M").lstrip(' ')
                    process_sms_info(sender, message)
                elif message[0] == 'm' and message[1] == ' ':
                    message = message.lstrip('m').lstrip(' ')
                    process_sms_info(sender, message)

            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL", problem_message="TZ_MSG Error: " + str(e))
                
    except Exception as e:
        if messageFound == True:
            problem_message="Unidentified POST format: %s from %s" % (message,sender)
        else:
            problem_message="Unidentified POST format: %s" % str(e)
        Error_Log.objects.create(problem_type="EXTERNAL", problem_message=problem_message)

    return HttpResponse(json.dumps({"payload": {'success': 'true'}}), content_type='application/json')


@csrf_exempt
def np_msg_handler(request):
    """
    handle incoming message from F1 - Nepal
    """
    if not request.POST:
        return HttpResponse("unknown request")
  
    try:
        senderNo = request.GET['mobile']
        msgIn = str(request.GET['message'])
    except:
        return Http404

    process_sms_info(senderNo, msgIn)

    return HttpResponse("")


@csrf_exempt
def ke_msg_handler(request):
    """
    collect SMS from Kenya Android server and process as messages or payments
    """
    if not request.POST:
        return HttpResponse("unknown request")

    # check if parameters fit format of a SMSSync SMS notification
    try:
        messageFound = False
        sender = request.POST['from']
        message = request.POST['message'].replace('\n','').replace('\r','')
        messageFound = True

        # *** verify ID code (make arrangements later) ***
        # if secret == settings.SMSSYNC_SECRET:
        now = datetime.datetime.now()
        
        # check whether message fits format of an Airtel Money payment
        try:
            # *** REPLACE WITH KENYA EQUIVALENT SOON ***
            [amount, sender, ref] = parse_payment_message(message)
            process_kopo_kopo_payment_info(amount, sender, message, ref)
        except IndexError as e:
            # assume it's an SMS, and process through AT functions; special case for main menu
            if message == "m" or message == "M":
                process_sms_info(sender,"")
                
            # first remove leading "M" for consist formatting
            else:
                message = message.lstrip("M ").lstrip("m ")
                process_sms_info(sender, message)
                
        except Exception as e:
            Error_Log.objects.create(problem_type="INTERNAL", problem_message="KE_MSG Error: " + str(e))
                
    except Exception as e:
        if messageFound == True:
            problem_message="Unidentified POST format: %s" % message
        else:
            problem_message="Unidentified POST format: %s" % str(e)
        Error_Log.objects.create(problem_type="EXTERNAL", problem_message=problem_message)

    return HttpResponse(json.dumps({"payload": {'success': 'true'}}), content_type='application/json')


@csrf_exempt
def bn_msg_handler(request):
    """
    collect SMS from Benin Android server and process as messages or payments
    """
    if not request.POST:
        return HttpResponse("unknown request")

    # check if parameters fit format of a SMSSync SMS notification
    try:
        messageFound = False
        sender = request.POST['from']
        Error_Log.objects.create(problem_type="DEBUGGING",
                                 problem_message=str(sender))
        message_id = request.POST['message_id']
        secret = request.POST['secret']
        sent_timestamp = request.POST['sent_timestamp']
        message = request.POST['message'].replace('\n','').replace('\r','')
        messageFound = True
        
        # verify ID code
        if secret == BENIN_SECRET_CODE:
            now = datetime.datetime.now()
            
            Error_Log.objects.create(problem_type="DEBUGGING",
                                     problem_message=str(now))
            
            # check whether message fits format of an Airtel Money payment
            # ***THIS WILL NEED TO CHANGE WHEN MM IS INTEGRATED***
            try:
                [amount, sender, ref] = parse_payment_message(message)
                process_kopo_kopo_payment_info(amount, sender, message, ref)

            except IndexError as e:

                # assume it's an SMS, and process through AT functions; special case for main menu
                if message == "m" or message == "M":
                    process_sms_info(sender,"")                

                # first remove leading "M" for consist formatting
                elif message[0] == 'M' and message[1] == ' ':
                    message = message.lstrip("M").lstrip(' ')
                    process_sms_info(sender, message)
                elif message[0] == 'm' and message[1] == ' ':
                    message = message.lstrip('m').lstrip(' ')
                    process_sms_info(sender, message)
                    
            except Exception as e:
                Error_Log.objects.create(problem_type="INTERNAL", problem_message="BN_MSG Error: " + str(e))
                
    except Exception as e:
        if messageFound == True:
            problem_message="Unidentified POST format: %s" % message
        else:
            problem_message="Unidentified POST format: %s" % str(e)
        Error_Log.objects.create(problem_type="EXTERNAL", problem_message=problem_message)
        
    return HttpResponse(json.dumps({"payload": {'success': 'true'}}), content_type='application/json')


@csrf_exempt
@require_POST
def sga_msg_handler(request):
    """
    Handles an incoming SMS callback from the SMS Gateway API:
    https://smsgateway.me/sms-api-documentation/callbacks/http-method

    Validates the message secret and passes the SMS on to the processing
    function.
    """

    # Validate secret and message parameters
    if 'secret' not in request.POST:
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="SMS Gateway API callback received with no "
                            "'secret': %s" % str(request.POST))
        return HttpResponseBadRequest()

    if request.POST['secret'] != SGA_CALLBACK_SECRET:
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="SMS Gateway API callback received with incorrect "
                            "'secret': %s" % str(request.POST))
        return HttpResponseBadRequest()

    if (('message' not in request.POST) or
            ('contact[number]' not in request.POST)):
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="SMS Gateway API callback received with missing "
                            "information: %s" % str(request.POST))
        return HttpResponseBadRequest()

    # Remove leading M if present
    message = request.POST['message'].lstrip("M ").lstrip("m ")

    # Process message and return 200
    process_sms_info(request.POST['contact[number]'], message)

    return HttpResponse("200 OK")


def parse_payment_message(message):
    """
    parse Airtel Money TZ payment message
    """
    
    # accept money amount with "Tshs" either before or after the number
    try:
        amount = str(re.findall(r'Tshs[\.]*[\s]*([\d]+.[\d]+)', message)[0]).lstrip('Tshs([\s]*)[\.]*')

    except IndexError:
        amount = str(re.findall(r'([\d]+.[\d]*)[\s]*Tshs', message)[0]).rstrip('([\s]*)Tshs')

    # check for Eng and Swa messages
    try:
        sender_number = re.findall(r'kutoka\s(\d{9})', message)[0]
    except IndexError:
        try:
            sender_number = re.findall(r'from\s(\d{9})', message)[0]
        except IndexError:
            sender_number = re.findall(r'Kutoka\s(\d{9})', message)[0]
    sender_number = "+255" + sender_number
    
    # check for Eng and Swa messages
    try:
        ref = re.findall(r'rejea: ([\w]+.[\w]+.[\w]+)', message)[0].lstrip("rejea: ")
    except IndexError:
        try:
            ref = re.findall(r'ID: ([\w]+.[\w]+.[\w]+)', message)[0].lstrip("ID: ")
        except IndexError:
            ref = re.findall(r'Txn ID :([\w]+.[\w]+.[\w]+)', message)[0].lstrip("Txn ID :")
    
    amount = Decimal(amount.replace(',', ''))
    
    return [amount, sender_number, ref]


# Some possible error string returns from process_kopo_kopo_payment_info
# Needed for kopo_kopo.views.process_at_sms
PROCESS_PAYMENT_ERRORS = {
    'USER_NOT_FOUND': 'User Not Found',
    'DUPLICATE_REF': 'Duplicate payment reference'
}


def process_kopo_kopo_payment_info(amount, sender_no, message, ref):
    """
    Processes a Kopo Kopo payment. Responsible for checking for duplicate
    payments, applying payment plans, and crediting customer accounts. Also
    pre-registers customers if the sender telephone number is not recognized.

    :returns: An error string if the payment was not processed, and None if it
        was processed successfully.
    """

    # Remember whether this was a known user
    unknown_user = False

    # Get or create user who made the payment
    try:
        user = locateUser(sender_no)
    except UserNotFound:
        user = create_preregistered_user(sender_no, amount, ref)
        unknown_user = True
    except Mingi_User.MultipleObjectsReturned:
        Error_Log.objects.create(
            problem_type='INTERNAL',
            problem_message='Repeated phone number detected: ' +
                            sender_no)
        return "Multiple Users Found"

    except Bit_Harvester.MultipleObjectsReturned:
        Error_Log.objects.create(
            problem_type="INTERNAL",
            problem_message="Repeated phone number detected: " +
                            sender_no)
        return "Multiple Users Found"

    # Create the timestamp outside of the lock because zone_aware may write to
    # Error_Log
    timestamp = zone_aware(user, datetime.datetime.now())

    # Create the payment log, using a lock to avoid duplicate processing
    # TODO: STS-208: Use Redis (GETSET) to avoid the need for a table lock
    try:
        Kopo_Kopo_Message.objects.lock()
        if Kopo_Kopo_Message.objects.filter(
                reference=ref, provider=Kopo_Kopo_Message.KOPO_KOPO).exists():
            return PROCESS_PAYMENT_ERRORS['DUPLICATE_REF']
        Kopo_Kopo_Message.objects.create(
            raw_message=message,
            amount=amount,
            user=user,
            timestamp=timestamp,
            reference=ref,
            category=Kopo_Kopo_Message.PAYMENT,
            provider=Kopo_Kopo_Message.KOPO_KOPO)
    finally:
        # Note that even if a duplicate is found, this will be called before
        # the the error message is returned above.
        # https://docs.python.org/2/reference/compound_stmts.html#finally
        Kopo_Kopo_Message.objects.unlock()

    # Temporary check for E.ON
    if amount > 50000:
        Error_Log.objects.create(
            problem_type="EXTERNAL",
            problem_message="Payment detected with top-up amount:"
                            " %.2f, payment reference: %s" %
                            (amount, ref),
            site=user.site)

    # Increment the user's account balance + downstream operations if it is a
    # known user
    try:
        if unknown_user:
            return PROCESS_PAYMENT_ERRORS['USER_NOT_FOUND']
        else:
            callPayPlanFunction(user, amount)
            return None
    except Exception as e:
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message=str(e))
        return "Payment processing error"
                

def process_sms_info(senderNo, msgIn):
    """
    process a received message based on sending phone number and message content
    """
    
    try:
        sender = locateUser(senderNo)               
        
    except UserNotFound as e:
        """
        if 'BH' in msgIn:
            createNewUser(senderNo)
            sender = locateUser(senderNo)            
        else:
        """
        Error_Log.objects.create(problem_type="INTERNAL",
                                 problem_message="UserNotFound for sender: " + str(senderNo) + "; message: " + msgIn[0:245])
        raise Http404

    now = zone_aware(sender,datetime.datetime.now())


    
    if (type(sender) == Mingi_User):
        Africas_Talking_Input.objects.create(raw_message=msgIn,user=sender,timestamp=now)

        respondToUser(sender,msgIn,now)

    elif (type(sender) == Bit_Harvester):
        
        # begin V4 integration; returns at a different endpoint from V3
        if sender.version == 'V4':
            # process as V3 if it's a PING message
            if is_formatted(msgIn):
                process_v3(msgIn,sender,now)
                return

            messages = sms_unpack_as_v3(str(msgIn))

            if messages is None:
                Error_Log.objects.create(problem_type="INTERNAL",problem_message="V4 message could not be decoded. Returned None. BH: "+str(sender)+"MSG: "+str(msgIn))
                return

            process_v4(messages,sender,now)
            # don't double-process
            return
        elif sender.version == 'SOLO':
            process_solo(msgIn,sender,now)
        
        else:
            process_v3(msgIn,sender,now)

    else:
        Error_Log.objects.create(problem_type="OTHER",
                                 problem_message="SMS received from unknown number: %s" % senderNo)


def process_v4(messages,sender,now):

    message_list = list()
    # process STATUS messages first

    for message in messages:
        if "STATUS" in message:
            message_list.append(message)
    # process METERS messages first
    for message in messages:
        if "METERS" in message:
            message_list.append(message)
    # process CREDIT messages after
    for message in messages:
        if "CREDIT" in message:
            message_list.append(message)
    # process pulses
    for message in messages:
        if "PULSES" in message:
            message_list.append(message)
    # process everything else
    for message in messages:
        if not "STATUS" in message and not "METERS" in message and not "CREDIT" in message and not "PULSES" in message:
            message_list.append(message)

    for message in message_list:
        msgElements = message.split(',')

        if sender.telephone[0:4] == '+266':
            tz_hours = settings.V4_UNIX_HOURS_OFFSET + 1 # Lesotho is 1 hour behind Kenya
            bh_timestamp = datetime.datetime.fromtimestamp(int(msgElements[1])) - datetime.timedelta(hours=tz_hours)
        elif sender.telephone[0:4] == '+229':
            tz_hours = settings.V4_UNIX_HOURS_OFFSET + 2 # Benin is 2 hours behind Kenya
            bh_timestamp = datetime.datetime.fromtimestamp(int(msgElements[1])) - datetime.timedelta(hours=tz_hours)
        else:
            bh_timestamp = datetime.datetime.fromtimestamp(int(msgElements[1])) - datetime.timedelta(hours=settings.V4_UNIX_HOURS_OFFSET)
        Africas_Talking_Input.objects.create(raw_message=message,bit_harvester=sender,timestamp=now,third_party_timestamp=bh_timestamp)                                
        respondToBH(sender,message,now,bh_timestamp)


def process_v3(msgIn,sender,now):
    msgElements = msgIn.split(',')

    try:
        # see how to handle this
        bh_timestamp = datetime.datetime.fromtimestamp(int(msgElements[1])) - datetime.timedelta(hours=settings.UNIX_HOURS_OFFSET)
        
        # *** QUICK FIX: TZ BH RESPONDING AN EXTRA 12.5 HRS AHEAD ***
        if (sender.site.name == "Komolo-I"):
            bh_timestamp = bh_timestamp - datetime.timedelta(hours=12.5)
        elif ("Nepal" in sender.site.name):
            bh_timestamp = bh_timestamp - datetime.timedelta(hours=13.8)

        Africas_Talking_Input.objects.create(raw_message=msgIn,bit_harvester=sender,timestamp=now,third_party_timestamp=bh_timestamp)

    except IndexError:
        Africas_Talking_Input.objects.create(raw_message=msgIn,bit_harvester=sender,timestamp=now)
        Error_Log.objects.create(problem_type="EXTERNAL",
                                 problem_message="Message not processed due to missing unix timestamp: %s" % msgIn)
        return
        
    except InvalidOperation as e:
        Africas_Talking_Input.objects.create(raw_message=msgIn,bit_harvester=sender,timestamp=now)
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="Message not processed due to error, %s: %s" % (str(e),msgIn))
        return
        

    respondToBH(sender,msgIn,now,bh_timestamp)

def process_solo(msgIn,sender,now):
    """
    Process SMS coming from a SOLO bitHarvester, which can only confirm whether the line state is ON or OFF
    """
    
    if msgIn == "STATE IS ON" or msgIn == "STATE IS OFF":
        respond_to_solo(sender,msgIn)
        
    else:
        Error_Log.objects.create(problem_type="EXTERNAL", problem_message="Unrecognized message: %s from SOLO bitHarvester" % msgIn)
        return
    
    Africas_Talking_Input.objects.create(raw_message=msgIn,bit_harvester=sender,timestamp=now)

def is_formatted(msg):
    if 'apt' in msg or 'ext' in msg:
        return True
    return False


def createNewUser(senderNo):
    """
    Respond to request from unregistered user
    """
    site = Mingi_Site.objects.get(name="Limbo")

    user = Mingi_User.objects.create(telephone=senderNo, first_name="PRE-REG",
                                     last_name="USER",site=site,
                                     account_balance=0)
