import datetime
from decimal import Decimal
import re
import json
import requests

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from django.conf import settings
from handleSMS import respondToBH, respondToUser
from mingi_django.models import Error_Log
from mingi_user.models import Mingi_User
from africas_talking.sendSMS import locateUser, UserNotFound
from africas_talking.models import Africas_Talking_Input,Gateway
from mingi_site.models import Bit_Harvester

from utils.views import forward_post_request
from mingi_django.settings import FORWARD, FORWARD_URLS_SMS
import urllib

from kopo_kopo.models import Kopo_Kopo_Message

from ReportV4 import sms_unpack_as_v3
from ControlV4 import unpack_v3

from mingi_django.mingi_settings.custom import OPERATOR_MAPPER
from SendMessage import sendSneakerCloudMessage, sendSneakerCloudMessage2, InvalidSmsGatewayArgument,NoSmsGatewayUrl
from views import process_kopo_kopo_payment_info,process_sms_info,parse_payment_message
from SMS import sendMessage
from mingi_django.timezones import zone_aware


import sys
import pprint

@csrf_exempt
def all_msg_handler(request):
    """
    collect SMS from Kenya Android server and process as messages or payments
    """
    post = request.POST

    # Get gateway details
    try:
        selected_gateway = Gateway.objects.filter(device_id=post['device_id'])[0]
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS", problem_message='Unrecongnized device_id for request : '+str(post)+' Error: '+str(e)+ ' kindly check sneaker cloud settings')
        return HttpResponse(json.dumps({"payload": {'success': False,'error':'The phone used cannot be identified with the system'}}), content_type ="application/json")

    if not post.get('device_id',False) or selected_gateway.password!=request.POST['secret'] :
        return HttpResponse(json.dumps({"payload": {'success': False,'error':'The phone used cannot be identified with the system'}}), content_type ="application/json")

    try:
        messageFound = False
        sender = request.POST['from']
        sender_id = request.POST['sent_to']
        message_id = request.POST['message_id']
        device_id = request.POST['device_id']
        secret = request.POST['secret']
        sent_timestamp = request.POST['sent_timestamp']
        message = request.POST['message'].replace('\n','').replace('\r','')
        messageFound = True
        try:
            operator_processor = OPERATOR_MAPPER[sender]
        except:
            operator_processor = 'mpesa_ke'

        # verify ID code
        if selected_gateway.password==secret:
            process_gateway_message(sender,message,operator_processor)
            # now = datetime.datetime.now()
            # # check whether message fits format of an Airtel Money payment
            # try:
            #     print 'processing payment ...'
            #     if operator_processor=='mpesa_ke':
            #         [amount, sender, ref] = parse_payment_message(message)
            #         process_payment_info(amount, sender, message, ref)
            #     elif operator_processor=='airtel_money_tz':
            #         [amount, sender, ref] = parse_payment_message_airtel_tz(message)
            #         process_payment_info(amount, sender, message, ref)
            #     else:
            #         raise IndexError

            # except IndexError as e:
            #     print 'processing sms 1...'
            #     # assume it's an SMS, and process through AT functions; special case for main menu
            #     if message == "m" or message == "M":
            #         process_sms_info(sender,"")

            #     # first remove leading "M" for consist formatting
            #     else:
            #         message = message.lstrip("M ").lstrip("m ")
            #         process_sms_info(sender, message)

            # except Exception as e:
            #     print "exception caught"
            #     Error_Log.objects.create(problem_type="INTERNAL", problem_message="SNEAKER_CLOUD Error: " + str(e))
                
    except Exception as e:
        if messageFound == True:
            problem_message="Unidentified POST format message found: %s" % str(e)
        else:
            problem_message="Unidentified POST format: %s" % str(e)
        Error_Log.objects.create(problem_type="EXTERNAL", problem_message=problem_message)

    print json.dumps({"payload": {'success': True,'error':None}})
    return HttpResponse(json.dumps({"payload": {'success': True,'error':None}}), content_type ="application/json")

@csrf_exempt
def gateway(request):
    """
    Handle all SMS from SC : payments, user + BH SMSes
    """
    if not request.POST:
        return HttpResponse("unknown request")

    post = request.POST

    username = post.get("username",None)
    password = post.get("password",None)
    task = post.get("task",None)

    # check for app sending
    try:
        selected_gateway = Gateway.objects.filter(username=username,password=password)[0]
    except Exception as e:
        Error_Log.objects.create(problem_type="SEND_SMS", problem_message='Unrecongnized device_id for request : '+str(post)+' Error: '+str(e)+ ' kindly check sneaker cloud settings')
        return HttpResponse(json.dumps({"payload": {'success': False,'error':'The phone used cannot be identified with the system'}}), content_type ="application/json")

    if task == "post_update":
        # track errors
        error = None
        success = True

        # SC sends SMS in array
        messages_str = request.POST.get("messages","[]")
        messages = json.loads(messages_str)
        failed_messages = list()

        z = 0
        for msg in messages:
            # check from phone number
            try:
                operator_processor = OPERATOR_MAPPER[msg['from']]
            except:
                operator_processor = "mpesa_ke"

            sms_message = msg['message']
            sms_from = msg['from']
            try:
                [success,error] = process_gateway_message(sms_from,sms_message,operator_processor)
            except Exception as e:
                Error_Log.objects.create(problem_type="DEBUGGING",problem_message="SC Errors: "+str(e))
                [success,error] = [True,None]

            if not success:
                if z==0:
                    failed_messages.append({"success":success})
                failed_messages.append({"time":msg['time'],"error":error})
            z = z+1

        # if no errors found, return success for all messages
        if len(failed_messages) == 0:
            failed_messages.append({"success":success})

        return HttpResponse(json.dumps(failed_messages))

    elif task == "send":
        return sendMessage(request)
    elif task == "update":
        return sendMessage(request)
    elif task == "status":
        return sendMessage(request)
    else:
        return HttpResponse(json.dumps([{"error":"unknown request","success":False}]))


        



@csrf_exempt
def process_gateway_message(sender,message,operator_processor):
    now = zone_aware(sender,datetime.datetime.now())
    error = None
    success = True
    # check whether message fits format of an Airtel Money payment
    try:
        print 'processing payment ...'
        if operator_processor=='mpesa_ke':
            [amount, sender, ref] = parse_payment_message(message)
            process_kopo_kopo_payment_info(amount, sender, message, ref)
        elif operator_processor=='airtel_money_tz':
            [amount, sender, ref] = parse_payment_message_airtel_tz(message)
            process_kopo_kopo_payment_info(amount, sender, message, ref)
        else:
            raise IndexError

    except IndexError as e:
        print 'processing sms 1...'
        # assume it's an SMS, and process through AT functions; special case for main menu
        if message == "m" or message == "M":
            process_sms_info(sender,"")

        # first remove leading "M" for consist formatting
        else:
            message = message.lstrip("M ").lstrip("m ")
            process_sms_info(sender, message)

    except Exception as e:
        error = "SC Error"
        success = False
        print "exception caught"
        Error_Log.objects.create(problem_type="INTERNAL", problem_message="SNEAKER_CLOUD Error: " + str(e))
    return [success,error]


@csrf_exempt
def parse_payment_message_airtel_tz(message):
    """
    parse Airtel Money TZ payment message
    """
    
    # accept money amount with "Tshs" either before or after the number
    try:
        amount = str(re.findall(r'Tshs[\.]*[\s]*([\d,]+.[\d,]*)', message)[0]).lstrip('Tshs([\s]*)')
    except IndexError:
        amount = str(re.findall(r'([\d,]+.[\d,]*)[\s]*Tshs', message)[0]).rstrip('([\s]*)Tshs')

    # check for Eng and Swa messages
    try:
        sender_number = re.findall(r'kutoka\s(\d{9})', message)[0]
    except IndexError:
        sender_number = re.findall(r'from\s(\d{9})', message)[0]
    sender_number = "+255" + sender_number

    # check for Eng and Swa messages
    try:
        ref = re.findall(r'rejea: ([\w]+.[\w]+.[\w]+)', message)[0].lstrip("rejea: ")
    except IndexError:
        ref = re.findall(r'ID: ([\w]+.[\w]+.[\w]+)', message)[0].lstrip("ID: ")
    
    amount = Decimal(amount.replace(',', ''))
    
    return [amount, sender_number, ref]

