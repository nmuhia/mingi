var SITE_DETAILS_URL = "/async/site/";
var DEFAULT_SITE_MARKER = "https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=glyphish_zap|F14700";
var SELECTED_SITE_MARKER = "https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=glyphish_zap|009900";

var siteMarkers = {};
var loadedSiteDetails = {};
var map;

var lat = 0;
var log = 38;



/**
 * Creates a site map inside the given parent element (a jQuery selector) and
 * returns a "mapObj" object containing the map and its element.
 */
function initializeSiteMap(mapParentElement) {
    // Create element to hold map
    var mapElement = $('<div></div>');
    mapElement.css({
        'position': 'absolute',
        'top': 0,
        'bottom': 0,
        'left': 0,
        'right': 0,        
    });
    mapParentElement.append(mapElement);

    // Create map
    var mapOptions = {	
        center: new google.maps.LatLng(window.latitude, window.longitude),
	// zoom:6,
        disableDefaultUI: true, 
        mapTypeControl:false
    };
    map = new google.maps.Map(
	mapElement.get(0),	
        mapOptions
    );  

    //////////////////////////////////////////////////////////

        

        //  for (i = 0; i < my_locations.length; i++) {
        // marker = new google.maps.Marker({
        //     position: new google.maps.LatLng(my_locations[i][1], my_locations[i][2]),
        //     map: mapObj
        // });


    

    /////////////////////////////////////////////////////////  
    map.set('styles',[
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {                
                "hue":"#1874CD"
            },            
        ]
    },               
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color":"#FAFAFA"
            },          
        ]
    },                           
    {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "visibility":"on"
            }            
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }           
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }           
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }           
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [            
            {
                "visibility": "off"
            }
        ]
    },    
]);
    return {map: map, mapElement: mapElement};
}

/**
 * Adds the given site object to the given map object.
 */

function addSite(mapObj, siteObj) {
    var siteDetails = "";
    var infoWindow;
    var siteId = siteObj['siteId'];    
    var siteName = siteObj['name'];
    var siteLatitude = siteObj.coords.lat;
    var siteLongitude= siteObj.coords.lng;
    
    siteDetails = "<span style='font-weight:bold;font-size:18px;'>"+siteName+"</span><br>"+
                      "<span style='font-size:15px;'><b>Latitude:</b> "+siteLatitude+"</span><br>"+
                      "<span style='font-size:15px;'><b>Longitude:</b> "+siteLongitude+"</span><br>";

    var $siteDetailsDiv = $("<div style='padding:10px;margin-left:10px;margin-right:10px;margin-bottom:5px;overflow-y:hidden;overflow-x:hidden;'>"+siteDetails+"</div>");
    var infowindow = new google.maps.InfoWindow();
    infowindow.setContent($siteDetailsDiv[0]);

    

    
    siteMarkers[siteId] = new google.maps.Marker({
        position: new google.maps.LatLng(
            siteObj['coords']['lat'], 
            siteObj['coords']['lng']
        ),
        icon: DEFAULT_SITE_MARKER,
    });
    window.bounds.extend(siteMarkers[siteId].position);
    siteMarkers[siteId].setMap(mapObj.map);
    google.maps.event.addListener(siteMarkers[siteId], 'click', function() {  
        openDefaultCards(siteId);
        infowindow.open(map, siteMarkers[siteId]); 
    });
    google.maps.event.addListener(siteMarkers[siteId], 'mouseover', function() {	
        infowindow.open(map, siteMarkers[siteId]); 
    });
    google.maps.event.addListener(siteMarkers[siteId], 'mouseout', function() {
        infowindow.close(map, siteMarkers[siteId]); 
    });


    // marker = new google.maps.Marker({
    //         position: new google.maps.LatLng({{ site.latitude }}, {{ site.longitude }}),
    //         map: mapObj.map
    //     });

        



}

