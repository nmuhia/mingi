/*
 * new_user_tour.js
 *
 * Creates a guided walkthrough of the site for new users.
 *
 * Requires jQuery and intro.js
 */
function startTour(siteId) {
    /*
     * Starts the guided walkthrough.
     *
     * siteId: The ID of the site to use for the demo graphs.
     */

    var intro = introJs();

    // Stores the previous step number. Used for direction-dependent logic.
    var lastStep = -1;

    // Defines the tour steps
    intro.setOptions({
        steps: [
            {
                intro: '<div><b>Welcome to Steama!</b></div></br>' +
                    '<div>Click "Next" to take the guided tour. (It won\'t take a minute)</div>'
            },
            {
                intro: "Click the menu button to open and close the menu.",
                element: document.querySelector("#menu-button"),
                position: 'right'
            },
            {
                intro: "The menu is your launch pad for all Steama functions",
                element: document.querySelector("#menu-list"),
                position: 'right'
            },
            {
                intro: '<div><b>This is live data from a microgrid in Kisumu, Kenya</b></div></br>' +
                    '<div>Use the Site Activity graph to track power draw and battery voltage in real time.</div>'+
		    '<div>Respond to issues faster by setting up custom alerts. Powerful notifications and switch lines on/off based on triggers such as low battery voltage and high energy use.</div>' +
		    '</br><div><img src="/static/custom/img/demo-recent-activity.png" width="500px"/></div>',
                element: document.querySelector("#card_column_left"),
                position: 'right'
            },
            {
                intro: '<div><b>Understand the breakdown of energy demand among your customers</b></div></br>'+
		    '<div>This graph compares all your customers at a glance, so you can make decisions about setting tariffs and increasing revenues. With Steama, you can easily export the underlying data to a spreadsheet for further analysis.</div>'+
		    '<div>As your site portfolio grows, you can easily differentiate the most successful sites and customers.</div></br>'+
		    '<div><img src="/static/custom/img/demo-use-breakdown.png" height="300px"/></div>',
                element: document.querySelector("#card_column_left"),
                position: 'right'
            },
            {
                intro: '<div><b>Manage your customer accounts anywhere, anytime</b></div></br>'+
		    '<div>Steama accepts payments and controls sites automatically. However, if you want to reprogram your smart meters, switch lines on or off manually, or make other changes, you can do this remotely from the Steama Dashboard.</div>',
                element: document.querySelector("#card_column_left"),
                position: 'right'
            },
            {
                intro: "Click this button to email Steama Support at any time.",
                element: document.querySelector("#email_us_btn"),
                position: 'bottom-right-aligned'
            },
            {
                intro: '<div>To explore for yourself, click "Done" and send us an email to request a free 14-day trial.</div></br><div>Click <a href="https://calendly.com/ryan-steamaco/skype-call/" target="_blank">here</a> to schedule a meeting with our sales manager.</div>',
            }
//            {
//                intro: "This is the workspace button. It opens and closes the workspace.",
//                element: document.querySelector("#toggle_workspace > img"),
//                position: 'bottom'
//            },
//            {
//                intro: "This is the help center button. Use it to contact SteamaCo!",
//                element: document.querySelector("#open_help_center > i"),
//                position: 'bottom'
//            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: false
    });

    // Handles custom javascript between tour steps
    intro.onbeforechange(function (newStepElement) {
        var step = intro._currentStep;

        // Clear any existing cards
        $(".close_button").click();
        $("#overlay").click();

        // Perform setup for the step
        switch (step) {
            case 1:
                if (lastStep === 2) {
                    $(".show-menu").click();
                }
                break;
            case 2:
                $(".show-menu").click();
                break;
            case 3:
                create_card("/cards/site/site_recent_activity/"+siteId+"/");
                break;
	    case 4:
                create_card("/cards/energy_bilevel/");
                break;
            case 5:
                create_card("/cards/site/site_users/"+siteId+"/");
                break;
            case 6:
                break;
            case 7:
                create_faqs_card("/cards/faqs/email/");
                break;
        }

        lastStep = step;
    });

    intro.onafterchange(function (newStepElement) {
        var step = intro._currentStep;
        switch (step) {
	case 2:
	    $(".introjs-tooltip").css("width", "275px");
            break;
	case 3:
	    $(".introjs-tooltip").css("width", "600px");
            break;
	case 4:
	    $(".introjs-tooltip").css("width", "400px");
            break;
	case 5:
	    $(".introjs-tooltip").css("width", "275px");
            break;
            case 6:
                // Prevent highlight from covering button (hack)
                $(".introjs-helperLayer").css("visibility", "hidden");
                break;
        }
    });

    // Begins the tour
    intro.start();
}
