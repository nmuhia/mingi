var SITE_DETAILS_URL = "/async/site/";
var DEFAULT_SITE_MARKER = "https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=glyphish_zap|F14700s";
var SELECTED_SITE_MARKER = "https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=glyphish_zap|009900";

var siteMarkers = {};
var loadedSiteDetails = {};
var map;

/**
 * Creates a site map inside the given parent element (a jQuery selector) and
 * returns a "mapObj" object containing the map and its element.
 */
function initializeSiteMap(mapParentElement) {

    // Create element to hold map
    var mapElement = $('<div></div>');
    mapElement.css({
        'position': 'absolute',
        'top': 0,
        'bottom': 0,
        'left': 0,
        'right': 0,        
    });
    mapParentElement.append(mapElement);

    // Create map
    var mapOptions = {	
        /*center: new google.maps.LatLng(0, 37),*/
        center: new google.maps.LatLng(0, 43),
	/*zoom:7*/
	zoom:6
    };
    map = new google.maps.Map(
	mapElement.get(0),	
        mapOptions
    );    
    map.set('styles',[
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {                
                "hue":"#1874CD"
            },            
        ]
    },               
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "color":"#F14700"
            },          
        ]
    }, 
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [            
            {                
                "hue":"#F14700"
            },
            {
                "saturation": 100
            },
        ]
    },     
                  
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#EE7600"
            },            
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [            
            {
                "visibility": "off"
            }
        ]
    },    
]);


    // Create element to hold details
    var detailsElement = $('<div></div>');
    detailsElement.css({
        'position': 'absolute',
        'top': '100%',
        'bottom': 0,
        'left': 0,
        'right': '35%',
        'padding': '10px',
        'border-top': '2px solid #222222',
        /*'background-image': 'url("/static/custom/img/light_alu.png")',*/

    });
    detailsElement.hide();
    mapParentElement.append(detailsElement);

    return {map: map, mapElement: mapElement, detailsElement: detailsElement};
}

/**
 * Adds the given site object to the given map object.
 */
function addSite(mapObj, siteObj) {
    var boxText = document.createElement("div");
        boxText.style.cssText = "";
        boxText.innerHTML = siteObj['name'];
		
	var myOptions = {                 
		 content: boxText
                 //,closeBoxURL:'/static/custom/img/marker-small.png'
                 ,closeBoxURL:'//www.google.com/intl/en_us/mapfiles/close.gif'
		,zIndex: null
		,boxStyle: { 
                  maxWidth:'400px'
                  ,width:'120px'
                  ,border: '1px solid #ccc'
		  ,background: "#ffffff"
		  ,padding: "5px"
                  ,fontSize:'1.6em'
                  ,fontWeight:'bold'
		 }
		,isHidden: false
		,pane: "floatPane"
		,enableEventPropagation: false
                ,pixelOffset: new google.maps.Size(-50, -70)
	};
    var ib = new InfoBox(myOptions);  
    var siteId = siteObj['siteId'];    

    
    siteMarkers[siteId] = new google.maps.Marker({
        position: new google.maps.LatLng(
            siteObj['coords']['lat'], 
            siteObj['coords']['lng']
        ),
        icon: DEFAULT_SITE_MARKER,
    });
    siteMarkers[siteId].setMap(mapObj.map);
    google.maps.event.addListener(siteMarkers[siteId], 'click', function() {
        setSiteDetails(mapObj, siteObj);
        setDetailsPane(mapObj, true);        
    });
    google.maps.event.addListener(siteMarkers[siteId], 'mouseover', function() {	
        ib.open(map, siteMarkers[siteId]); 
    });
    google.maps.event.addListener(siteMarkers[siteId], 'mouseout', function() {
        ib.close(map, siteMarkers[siteId]); 
    });
}

function setDetailsPane(mapObj, setOpen) {
    if (setOpen) {
        mapObj.mapElement.animate({
            'bottom': '50%'
        }, 500);
        mapObj.detailsElement.animate({
            'top': '50%'
        }, 500);
        mapObj.mapElement.css({'border-bottom': '2px solid black'});
        mapObj.detailsElement.show();
    } else {
        mapObj.mapElement.animate({
            'bottom': 0,
            'border-bottom': 'none'
        }, 500, function () {
            mapObj.mapElement.css({'border-bottom': 'none'});
        });
        mapObj.detailsElement.animate({
            'top': '100%',
            'border-top': 'none'
        }, 500, function () {
            mapObj.detailsElement.hide();
            for (var aSiteId in siteMarkers)
                siteMarkers[aSiteId].setIcon(DEFAULT_SITE_MARKER);
        });
    }
}

/**
 * Populates the site details pane.
 */
function setSiteDetails(mapObj, siteObj) {
    var siteName = siteObj['name'];

    var headerRow = $('<div class="row-fluid"><h4 class="site-details-header">' +
                      siteName + ' Details</h4></div>');
    mapObj.detailsElement.html(headerRow);

    var detailsRow = $('<div class="row-fluid"></div>');
    var detailsLeftCol = $("<div class='span6'></div>");
    var locationDetails = $('<p></p>');
    locationDetails.html('<p><b>Latitude:</b> ' + siteObj.coords.lat + 
                         '<br /><b>Longitude:</b> ' + siteObj.coords.lng + 
                         '</p>');
    detailsLeftCol.append(locationDetails);
    detailsRow.append(detailsLeftCol);
    mapObj.detailsElement.append(detailsRow);

    var siteId = siteObj['siteId'];
    if (loadedSiteDetails.hasOwnProperty(siteId)) {
        thisSiteDetails = loadedSiteDetails[siteId];
        if (thisSiteDetails == "failed")
            headerRow.find('h4').html(siteName + " Details - Load Failed");
        else {
            detailsLeftCol.append(
                '<p style="margin-bottom:0px">' +
                '<b>Number of Customers:</b> ' +
                thisSiteDetails['users']['total'] + '</p><ul>' +
                '<li>active: ' + 
                thisSiteDetails['users']['active'] + '</li>' +
                '<li>inactive: ' +
                thisSiteDetails['users']['inactive'] + '</li>' +
                '<li>currently using power: ' +
                thisSiteDetails['users']['using_power'] + '</li>' +
                '<li>currently sending messages: ' +
                thisSiteDetails['users']['sending_messages'] + 
                '</ul><p>' + 
                '<b>Number of Lines:</b> ' +
                thisSiteDetails['num_lines'] + '</p>'
            );
            var detailsRightCol = $("<div class='span6'></div>");
            detailsRow.append(detailsRightCol);
            var siteEquipment = thisSiteDetails['equipment'];
            var equipmentList = '<p><b>Installed Equipment:</b> ';
            if (siteEquipment.length == 0) {
                equipmentList += 'None</p>';
            } else {
                for (var i = 0; i < siteEquipment.length; i++) {
                    equipmentList += siteEquipment[i];
                    if ((i + 1) < siteEquipment.length)
                        equipmentList += ", ";
                }
            }
            detailsRightCol.append(equipmentList);
            detailsRightCol.append(
                '<p><b>Owner:</b> ' + thisSiteDetails['company_name'] + '</p>'
            );
            detailsRightCol.append(
                '<p><b>Revenue to Date:</b> ' +
                thisSiteDetails['revenue_to_date'] + ' KSh</p>'
            );
            detailsRightCol.append(
                '<p><b>Energy to Date:</b> ' +
                parseFloat(thisSiteDetails['energy_to_date']).toFixed(3) + ' kWh</p>'
            );
        }
    } else {
        loadSiteDetails(mapObj, siteObj);
        headerRow.find('h4').html(siteName + " Details - Loading Details");
    }

    var buttonDiv = $("<div></div>");
    buttonDiv.css({
        'position': 'absolute',
        'bottom': '10px',
        'right': '10px'
    });
    mapObj.detailsElement.append(buttonDiv);
    
    var pageButton = $("<a class='btn btn-primary'>Site Page</a>");
    pageButton.attr("href", "/elements/site/" + siteId + "/");
    pageButton.css({"margin-right":"5px"});
    buttonDiv.append(pageButton);

    var closeButton = $("<button class='btn'>Hide Details</button>");
    closeButton.click(function() {
        setDetailsPane(mapObj, false);
    });
    buttonDiv.append(closeButton);

    // Change marker to selected icon (and other markers to default)
    for (var aSiteId in siteMarkers)
        siteMarkers[aSiteId].setIcon(DEFAULT_SITE_MARKER);
    siteMarkers[siteId].setIcon(SELECTED_SITE_MARKER);
}

/**
 * Loads site details from the server asynchronously.
 */
function loadSiteDetails(mapObj, siteObj) {
    var siteId = siteObj['siteId'];
    $.getJSON(
        SITE_DETAILS_URL,
        {
            siteId: siteId
        },
        function (data) {
            loadedSiteDetails[siteId] = data;
            setSiteDetails(mapObj, siteObj);
        }
    ).fail(function () {
        loadedSiteDetails[siteId] = "failed";
        setSiteDetails(mapObj, siteObj);
    });
}
