function getMonthName(month_id){
    var month_table = { '1':'Jan',
                     '01':'Jan', 
                     '2':'Feb', 
                     '02':'Feb', 
                     '3':'Mar', 
                     '03':'Mar', 
                     '4':'Apr',
                     '04':'Apr', 
                     '5': 'May',
                     '05':'May', 
                     '6': 'Jun',
                     '06':'Jun', 
                     '7':'Jul',
                     '07':'Jul', 
                     '8':'Aug',
                     '08':'Aug', 
                     '9':'Sep',
                     '09':'Sep', 
                    '10':'Oct',
                    '11':'Nov',
                    '12':'Dec'
                  };

    var monthName = month_table[month_id];
    return monthName;
}
function formatted_date(date){  
    var month = (date.getUTCMonth()+1).toString();
    var day = date.getUTCDate().toString();
    var year = date.getUTCFullYear();
    var hours = date.getUTCHours().toString();
    var minutes = date.getUTCMinutes().toString();        
    
    var monthName = getMonthName(month);

    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; 
    minutes = minutes < 10 ? '0'+minutes : minutes;

    var strTime = hours + ':' + minutes + ' ' + ampm;
    
    
    day = day.length < 2 ? '0' + day : day;
    minutes = minutes.length < 2 ? '0' + minutes : minutes;
    hours = hours.length < 2 ? '0' + hours : hours;
  
    var formatted_date = monthName+","+day+","+year+ ", "+strTime;       
    return formatted_date;
}
function dashed_formatted_date(date){  
    var month = (date.getUTCMonth()+1).toString();
    var day = date.getUTCDate().toString();
    var year = date.getUTCFullYear();    
    
    day = day.length < 2 ? '0' + day : day;
    month = month.length < 2 ? '0' + month : month;
  
    var formatted_date = year+"-"+month+"-"+day;       
    return formatted_date;
}
function simpledate(date){
    var date_arr = date.split("-");
    var year = date_arr[0];
    var month = date_arr[1];
    var day = date_arr[2];
   
    return day+"-"+getMonthName(month)+"-"+year;
}

function removedays(date, days) {
  var milliseconds = 1000 * 60 * 60 * 24;
  return new Date(date.getTime() - days * milliseconds);        
}


function validateEmail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        return false;
    }
    else{
        return true;
    }
}

function processPhone(phoneNo){
    //check if the number is numeric
    String.prototype.isNumber = function(){
        return /^\d+$/.test(this);
    }

    if(phoneNo.charAt(0) == "+"){
        new_phoneNo = phoneNo.substring(1);
        if(new_phoneNo.isNumber()){    
            return true;
        }
        else{
            return false;
        }
    }
    else{
        if(phoneNo.isNumber()){
            return true;
        }
        else{
            return false;
        }
    }
}

function toSeconds(hours) {
        // compute  and return total milliseconds
        return ((hours * 3600) * 1000);
}

function prepend_zero(number){
    if(number < 10){
        number = '0' + number;
    }
    return number;
}
