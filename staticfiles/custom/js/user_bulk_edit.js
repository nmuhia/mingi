var BULK_EDIT_CONFIRMATION_MESSAGE = "This is a potentially destructive action! Are you sure you want to submit a bulk edit?";

var FIELD_ID_MAP = {
    "0": "Select a Field to Edit",
    "1": "Account Balance",
    "2": "Energy Price",
    "3": "Low Balance Warning",
    "4": "Current Low Balance Level",
    "5": "Site Manager",
    "6": "Site",
    "7": "BitHarvester",
    "8": "Line Control Party",
    "9": "Meter Log Party",
    "10": "Line Number",
    "11": "Language",
    "12": "Notifications Setting",
    "13": "Payment Plan",
    "14": "Sub Balance"
};

var BULK_USER_EDIT_ENDPOINT = "/bulk/user/";

var editFields = {};
var filterFields = {};

function setupBulkEdits(csrf_token) {

    $('#bulk-edit-submit-button').click(function() {
        if (confirm(BULK_EDIT_CONFIRMATION_MESSAGE))
            submitBulkEdit(csrf_token);
    });

    setupEditFields();
    setupFilterFields();
};

function submitBulkEdit(csrf_token) {
    var resultSpan = $('#bulk-edit-result');
    resultSpan.removeClass();    
    if (emptyObject(editFields)) {
        resultSpan.addClass('text-error');
        resultSpan.text("- No Edit Parameters");
        return;
    }
    resultSpan.text("- Submitting Edit...");

    var editFieldsToSend = {}
    for (var field in editFields) {
        if (editFields.hasOwnProperty(field))
            editFieldsToSend[field] = editFields[field]['value']
    }

    var filterFieldsToSend = {}
    for (var field in filterFields) {
        if (filterFields.hasOwnProperty(field))
            filterFieldsToSend[field] = filterFields[field]['value']
    }

    $.post(
        BULK_USER_EDIT_ENDPOINT,
        {
            'csrfmiddlewaretoken': csrf_token,
            'editFields': editFieldsToSend,
            'filterFields': filterFieldsToSend
        },
        function (data) {
            if (data.hasOwnProperty('error_message')) {
                resultSpan.addClass('text-error');
                resultSpan.text("- " + data['error_message']);
                return;
            }
            var resultText = "- ";
            resultText += data['num_users_edited'];
            resultText += " User";
            if (data['num_users_edited'] != 1)
                resultText += "s";
            resultText += " Edited";
            resultSpan.text(resultText);
        }
    ).fail(function () {
        resultSpan.addClass('text-error');
        resultSpan.text("- Edit Failed");
    });
}

function setupEditFields() {
    var bulkEditSelect = $('#bulk-edit-select');
    var bulkEditAdd = $('#bulk-edit-add');
    var bulkEditInput = $('#bulk-edit-input');
    
    bulkEditSelect.change(function () {
        var selectedOption = $(this).find('option:selected');
        if (selectedOption.attr('value') == 0) {
            bulkEditInput.attr('disabled', 'true');
            bulkEditAdd.css('display', 'none');
        } else {
            bulkEditInput.removeAttr('disabled');
            bulkEditAdd.css('display', 'inline');
        }
    });

    bulkEditAdd.click(function () {
        var selectedOption = bulkEditSelect.find('option:selected');
        var inputValue = bulkEditInput.val();
        bulkEditInput.val("");
        editFields[selectedOption.attr('value')] = {
            'display': inputValue,
            'value': inputValue
        };
        setEditFieldsTable();
    });

    bulkEditInput.keydown(function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 13) {
            e.preventDefault();
            bulkEditAdd.click();
        }
    });

    setEditFieldsTable();
}

function setupFilterFields() {
    var bulkFilterSelect = $('#bulk-filter-select');
    var bulkFilterAdd = $('#bulk-filter-add');
    var bulkFilterInput = $('#bulk-filter-input');

    bulkFilterSelect.change(function () {
        var selectedOption = $(this).find('option:selected');
        if (selectedOption.attr('value') == 0) {
            bulkFilterInput.attr('disabled', 'true');
            bulkFilterAdd.css('display', 'none');
        } else {
            bulkFilterInput.removeAttr('disabled');
            bulkFilterAdd.css('display', 'inline');
        }
    });

    bulkFilterAdd.click(function () {
        var selectedOption = bulkFilterSelect.find('option:selected');
        var inputValue = bulkFilterInput.val();
        bulkFilterInput.val("");
        filterFields[selectedOption.attr('value')] = {
            'display': inputValue,
            'value': inputValue
        };
        setFilterFieldsTable();        
    });

    bulkFilterInput.keydown(function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 13) {
            e.preventDefault();
            bulkFilterAdd.click();
        }
    });

    setFilterFieldsTable();
}

function setEditFieldsTable() {
    var body = $('#edit-fields-table').find("tbody");
    body.html("");
    if (emptyObject(editFields)) {
        body.html('<tr><td colspan="3">No Edited Fields</td></tr>');
        return;
    }
    for (var field in editFields) {
        if (editFields.hasOwnProperty(field)) {
            var row = $('<tr></tr>');
            row.append('<td fieldid="' + field + '">' + FIELD_ID_MAP[field] + '</td>');
            row.append('<td>' + editFields[field]['display'] + '</td>');
            var removeLink = $('<a href="#" class="text-error">remove</a>');
            removeLink.attr('fieldid', field);
            var removeCell = $('<td></td>');
            removeCell.append(removeLink);
            row.append(removeCell);
            body.append(row);

            removeLink.click(function () {
                delete editFields[$(this).attr('fieldid')];
                setEditFieldsTable();
                return false;
            });
        }
    }
}

function setFilterFieldsTable() {
    var body = $('#filter-fields-table').find("tbody");
    body.html("");
    if (emptyObject(filterFields)) {
        body.append('<tr><td colspan="3">No Filtered Fields (All users will be edited)</td></tr>');
        return;
    }
    for (var field in filterFields) {
        if (filterFields.hasOwnProperty(field)) {
            var row = $('<tr></tr>');
            row.append('<td fieldid="' + field + '">' + FIELD_ID_MAP[field] + '</td>');
            row.append('<td>' + filterFields[field]['display'] + '</td>');
            var removeLink = $('<a href="#" class="text-error">remove</a>');
            removeLink.attr('fieldid', field);
            var removeCell = $('<td></td>');
            removeCell.append(removeLink);
            row.append(removeCell);
            body.append(row);

            removeLink.click(function () {
                delete filterFields[$(this).attr('fieldid')];
                setFilterFieldsTable();
                return false;
            });
        }
    }
}

function emptyObject(obj) {
    return (Object.keys(obj).length === 0);
}
